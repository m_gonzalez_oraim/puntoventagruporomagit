﻿Imports ModelPuntoVentaGrupoRoma

Public Class TestSolicitud
    Dim controlSolicitud As ControlSolicitud =
        New ControlSolicitud(Configuration.ConfigurationManager.AppSettings("DBConn"), Configuration.ConfigurationManager.AppSettings("DBConnSAP"))

    Public Function TestCrearSolicitud() As Boolean
        Return controlSolicitud.CrearSolicitud(StubSolicitudAlta())
    End Function

    Private Function StubSolicitudAlta() As Solicitud
        Dim solicitud As Solicitud = New Solicitud()
        solicitud.FechaImportado = DateTime.Now
        'Comentarios para generar commit
        solicitud.Fecha = DateTime.Now.AddDays(-1)
        solicitud.CodigoTiendaSolicita = 2
        solicitud.CodigoTiendaSurte = 3
        solicitud.Lineas = New List(Of LineaSolicitud)()

        Dim linea1 As LineaSolicitud = New LineaSolicitud()
        linea1.CodigoArticulo = "04010700007"
        linea1.Cantidad = 12
        linea1.FactorUnidadMedida = 4
        linea1.NombreArticulo = "PIJA PUNTA DE BROCA PARA LLAVE ALLEN     8 X 32    5/32 X 1 1/4"""
        linea1.Linea = 0
        linea1.UnidadMedida = "GRAMO"

        solicitud.Lineas.Add(linea1)

        Dim linea2 As LineaSolicitud = New LineaSolicitud()
        linea2.CodigoArticulo = "0117000806"
        linea2.Cantidad = 22
        linea2.FactorUnidadMedida = 1
        linea2.NombreArticulo = "PIJA PUNTA DE BROCA PARA LLAVE ALLEN  8 X 32  5/32 X 11/4"""
        linea2.Linea = 0
        linea2.UnidadMedida = "PIEZA"

        solicitud.Lineas.Add(linea2)

        Return solicitud

    End Function

End Class
