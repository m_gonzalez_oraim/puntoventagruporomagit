﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Login.aspx.vb" Inherits="PuntoVentaGrupoRoma.Login" %>
<%@ Register assembly="Ext.Net" namespace="Ext.Net" tagprefix="ext" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Acceso Denegado</title>
    <script type="text/javascript">
        if ((screen.width >= 1024) && (screen.height >= 768)) { }
        else { alert('La resolución de su monitor ' + screen.width + 'x' + screen.height + ' podria ser demasiado baja para visualizar correcamente algunas secciones del sitio');}
    </script>
</head>

<body>
    <script  src="../Scripts/GeneralLib.js" type="text/javascript"></script> 
    <ext:ResourceManager ID="ResourceManager1" runat="server" Theme="Gray"/>
    <form id="frmMain" runat="server">
    <ext:Viewport runat="server" Layout="BorderLayout">
        <Items>
            <ext:Panel ID="pnlTop" Layout="AnchorLayout" Region="North" Height="80" runat="server">
                <Items>
                    <ext:Container runat="server" StyleHtmlCls="text-align:center">
                        <Content>
                            <img id="Img2" runat="server" dir="ltr" enableviewstate="true" src="~/Content/Images/logo.PNG" onclick="parent.location='../Default.aspx'"  style="text-align:center"/>
                            
                        </Content>
                    </ext:Container>
                </Items>
            </ext:Panel>
            <ext:Panel ID="pnlCenter" Layout="AnchorLayout" Region="Center" Height="900" runat="server">
                <Items>
                    <ext:Container runat="server">
                        <Content>
                            <ext:Button 
                            ID="Button1" 
                            runat="server" 
                            Text="Identificación de Usuario" 
                            Height="30px"
                            Icon="LockOpen" >
                    <Listeners>
                        <Click Handler="#{Window1}.show();txtUsername2.focus(false, 100);" />
                    </Listeners>    
                </ext:Button> 
                                                    <asp:Table runat="server" ID="tblAccessDenied" BorderWidth="0" CellSpacing="0" 
                        CellPadding="5" Width="521" HorizontalAlign="Center" Visible="False">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" runat="server" BackColor="#74C2E1" Height="22" Wrap="false">
                <b><font style="font-family: Verdana,Arial,Helvetica; font-size: small; color: #FFFFFF;">Acceso No Permitido</font></b>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow2" runat="server">
                            <asp:TableCell BackColor="#CCCCCC">
                <p align="Justify" style="margin-left: 10; margin-right: 10; margin-top: 10;">
                    Para poder ingresar al sistema, deberá estar inscrito. Pregunte por una suscripción al <a href="mailto:<%= ConfigurationManager.AppSettings("SystemAdminName")%> <<%= ConfigurationManager.AppSettings("SystemAdminAddress")%>>">Administrador del Sistema</a>.
                </p>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table runat="server" ID="tblInactive" BorderWidth="0" CellSpacing="0" CellPadding="5" Width="521" HorizontalAlign="Center" Visible="false">
                        <asp:TableRow ID="TableRow3" runat="server">
                            <asp:TableCell ID="TableCell2" runat="server" BackColor="#000066" Height="22" Wrap="false">
                <b><font style="font-family: Verdana,Arial,Helvetica; font-size: small; color: #FFFFFF;">Cuenta Inactiva</font></b>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow ID="TableRow4" runat="server">
                            <asp:TableCell BackColor="#CCCCCC">
                <p align="Justify" style="margin-left: 10; margin-right: 10; margin-top: 10;">
                    Su cuenta ha sido inactivada. Pongase en contacto con el 
                    <a href="mailto:<%= ConfigurationManager.AppSettings("SystemAdminName")%> <<%= ConfigurationManager.AppSettings("SystemAdminAddress")%>>">Administrador del Sistema</a> 
                    para que le solicite la reactivación de la misma.
                </p>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>


                        </Content>
                    </ext:Container>
                </Items>
            </ext:Panel>

            <ext:Panel ID="pnlBottom" Layout="AnchorLayout" Region="South" Height="40" runat="server">
            <Items>
                <ext:Container runat="server">
                    <Content>
                        <div style="font-size: 7pt; color: #666666; font-family: Verdana;text-align:center"><%=ConfigurationManager.AppSettings("SystemCopyright")%></div>
                        <div style="font-size: 7pt; color: #666666; font-family: Verdana;text-align:center"> <%=ConfigurationManager.AppSettings("SystemVersion")%></div>
                    </Content>
                </ext:Container>
            </Items>
        </ext:Panel>
    </Items>
</ext:Viewport>

            <ext:Window 
                        ID="Window1" 
                        runat="server" 
                        Closable="false"
                        Resizable="false"
                        Height="150" 
                        Icon="Lock" 
                        Title="Datos de Acceso"
                        Draggable="true"
                        Width="350"
                        Modal="true"
                        Padding="5"
                        Hidden="true" 
                        Layout="form">
                        <Items>
                            <ext:TextField 
                                ID="txtUsername2" 
                                runat="server" 
                                FieldLabel="Usuario"  
                                AllowBlank="false"
                                BlankText="El nombre de usuario es requerido." 
                                AnchorHorizontal="100%"
                                />
                            <ext:TextField 
                                ID="txtPassword2" 
                                runat="server" 
                                InputType="Password" 
                                FieldLabel="Contraseña" 
                                AllowBlank="false" 
                                BlankText="La contraseña es requerida." 
                                AnchorHorizontal="100%"
                                />
                        </Items>
                        <Buttons>
                            <ext:Button ID="btnLogin" runat="server" Text="Aceptar" Icon="Accept" Type="Submit" >
                                <Listeners>
                                    <Click Handler="
                                        if (!#{txtUsername2}.validate() || !#{txtPassword2}.validate()) { 
                                            Ext.Msg.alert('Error','Nombre de usuario y contraseña son requeridos.');
                                            return false; 
                                        }" AutoPostBack="true"   />
                                </Listeners>
                                               
                                 <DirectEvents>
                                    <Click OnEvent="btnLogin_Click">
                                        <EventMask ShowMask="true" Msg="Verificando..." MinDelay="5000" />
                                    </Click>
                                </DirectEvents> 
                            </ext:Button>
                            <ext:Button ID="btnCancel" runat="server" Text="Cancelar" Icon="Decline">
                                <Listeners>
                                    <Click Handler="#{Window1}.hide();" />
                                </Listeners>
                            </ext:Button>
                        </Buttons>
                    </ext:Window>

        </form>
</body>
</html>
