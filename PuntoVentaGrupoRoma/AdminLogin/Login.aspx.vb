﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net

Public Class Login
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtUsername2.Focus()
        btnLogin.Center()
    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLogin.Click
        ' Get NT Name
        Me.Window1.Hide()

        Dim controlAcceso As ControlAcceso = New ControlAcceso()
        Dim existente As Boolean = False
        Dim activo As Boolean = False
        Dim userId As Integer = -1
        Dim nombre As String = String.Empty
        Dim menu As String = String.Empty
        Dim permiso As String = String.Empty

        controlAcceso.TieneAcceso(txtUsername2.Text, txtPassword2.Text, existente, activo, userId, nombre, menu, permiso)

        If Not existente Then
            tblAccessDenied.Visible = True
            tblInactive.Visible = False
            Return
        End If

        If Not activo Then
            tblAccessDenied.Visible = False
            tblInactive.Visible = True
            Return
        End If

        Dim guidSesion As String = Guid.NewGuid().ToString()
        Session("Guid") = guidSesion
        Dim usuarioPuntoVenta As UsuarioPuntoVentaIdentity = New UsuarioPuntoVentaIdentity(userId, nombre, permiso, menu, guidSesion)
        Dim faCookei As HttpCookie = New HttpCookie("PuntoVentaBXP", UsuarioPuntoVentaIdentity.Serialize(usuarioPuntoVenta))
        faCookei.Expires = DateTime.Now.AddDays(1)
        Response.Cookies.Add(faCookei)

        Dim redirectUrl As String = FormsAuthentication.GetRedirectUrl(txtUsername2.Text, False)
        Response.Redirect(redirectUrl)
    End Sub
End Class