﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.master" CodeBehind="Default.aspx.vb" Inherits="PuntoVentaGrupoRoma._Default" %>

<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">             
    <table align="center" width="550">
        <tr>
            <td>
                <p style="text-align: center;">
                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    </p>
                <p style="text-align: center;">
                    <ext:Store ID="Store1" runat="server">
                        <Model>
                            <ext:Model runat="server">
                                <Fields>
                                    <ext:ModelField Name="AdminStoreID" />
                                    <ext:ModelField Name="StoreName" />
                                    <ext:ModelField Name="WhsId" />
                                </Fields>
                            </ext:Model>
                        </Model>
                    </ext:Store>
                    
                     <ext:Window 
                        ID="Window1" 
                        runat="server" 
                        Closable="false"
                        Resizable="false"
                        Icon="Lock" 
                        Title="Para iniciar debe seleccionar una Tienda"
                        Draggable="true"
                        Width="265"
                        Modal="true"
                        Layout="form" 
                         Y="200">
                        <Items>
                             <ext:ComboBox 
                                ID="ComboBox1" 
                                runat="server"
                                StoreID="Store1" 
                                Width="250"
                                Editable="false" 
                                 DisplayField="StoreName" 
                                 ValueField="AdminStoreID" 
                                 TypeAhead="true" 
                                 Mode="Default" 
                                 LabelWidth="1" 
                                 ForceSelection="true" 
                                EmptyText="Seleccione una Tienda..."
                                ItemSelector="div.list-item"
                                SelectOnFocus="true">
                                 <ListConfig>
                                     <ItemTpl runat="server">
                                        <Html>
					                        
						                        <div class="list-item">
							                         <div><b>{StoreName}</b></div>
							                         {WhsId} 
						                        </div>
				                        </Html>
                                    </ItemTpl> 
                                </ListConfig>
                                 <DirectEvents>
                                     <Select OnEvent="controlarCambioTienda" />
                                 </DirectEvents>
                            </ext:ComboBox>                        
                                            </Items>
                    </ext:Window>                    
                </p>
            </td>
        </tr>
    </table>
    <script  src="../Scripts/NoIEActivate.js" type="text/javascript"></script>
</asp:Content>
