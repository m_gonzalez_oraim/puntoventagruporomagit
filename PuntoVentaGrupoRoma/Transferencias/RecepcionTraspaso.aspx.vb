﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml
Imports System.Data
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports System.IO.Path

Public Class RecepcionTraspaso
    Inherits System.Web.UI.Page

    Private mensaje As String = ""
    Private connstringWEB As String
    Private connstringSAP As String
    Private oDB As New DBMaster

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            If Session("STORETYPEID") = 3 Then
                Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
            End If
            If Session("ExitoRecepcion") = True Then
                Session("ExitoRecepcion") = False
                Dim strMesssage As String = "Operacion Realizada con Éxito"
                'ClientScript.RegisterStartupScript(Me.[GetType](), "alert", "<script>alert('" & strMesssage & "');</script>")
                ClientScript.RegisterStartupScript(Me.[GetType](), "alert", "<script>alert('" & strMesssage & "');</script>")

                Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
                msgConfig.Icon = MessageBox.Icon.INFO
                msgConfig.Title = "RESULTADO"
                msgConfig.Message = "Operacion Realizada con Éxito"
            End If
            Session("ExitoRecepcion") = False
        Else
            Dim CONSTRING As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            Dim INITIALCATALOG = CONSTRING.Split(";")(1).Split("=").Clone(1) & ".DBO.OITM"

            Dim SelectCommand As String
            SelectCommand = _
            "SELECT     ENVD.IDEnvio, ENVD.Linea as Partida, ENVD.Articulo, ART.ArticuloSBO, ENVD.TiendaOrigen," & vbCrLf & _
            "           SAPOITM.U_BXP_MARCA as LINEA, SAPOITM.U_BXP_MEDIDA AS MEDIDA, " & vbCrLf & _
            "           SAPOITM.ItemName AS MODELO, ENVD.Cantidad, ENVD.CantidadPendiente, ENVD.Status, ENVD.CantidadPendiente as RECIBIR" & vbCrLf & _
            "FROM         EnviosDetalle ENVD INNER JOIN" & vbCrLf & _
            "             Articulos ART ON ENVD.Articulo = ART.IDArticulo  inner join" & vbCrLf & _
            "             " & INITIALCATALOG & " SAPOITM ON ART.ArticuloSBO = SAPOITM.ItemCode " & vbCrLf & _
            "WHERE     (ENVD.IDEnvio = @IDEnvio AND ENVD.Status = 'O') " & vbCrLf & _
            "ORDER BY ENVD.LINEA" & vbCrLf & _
            ""

            SqlPurchaseOrderDetailSAP.SelectCommand = SelectCommand

        End If
    End Sub

    Protected Sub StorePurchaseOrderDetail_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim id As String = e.Parameters("IDEnvio").ToString
        SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
        StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
        StorePurchaseOrderDetailSAP.DataBind()
        Session("ENVIOID") = id
    End Sub

    Protected Sub StoreGridPanel1_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim id As String = e.Parameters("IDEnvio").ToString
        SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
        StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
        StorePurchaseOrderDetailSAP.DataBind()
        Session("ENVIOID") = id
    End Sub


    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            'Habilitamos el botón de Transferencias 
            'msgConfig.Buttons = MessageBox.Button.OK
            'msgConfig.Icon = MessageBox.Icon.INFO
            'msgConfig.Title = "RESULTADO"
            'msgConfig.Message = mensaje
            'Dim id As String = Session("ENVIOID")
            'SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
            'StorePurchaseOrderDetailSAP.BaseParams.Item("IDEnvio") = id
            'StorePurchaseOrderDetailSAP.DataBind()
            Response.Redirect("RecepcionTraspaso.aspx")
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Dim id As String = Session("ENVIOID")
            SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
            StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
            StorePurchaseOrderDetailSAP.DataBind()
        End If
        Ext.Net.X.Msg.Show(msgConfig)
    End Sub



    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        Dim grid2Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim grid2Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim row2
        Dim counter As Integer = 0
        Dim counterIMEI As Integer = 0
        Dim counterICC As Integer = 0
        Dim counterAccesorios As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String
        Dim sQueryInsert2 As String
        Dim Almacen As String
        Dim IDUsuario As Integer = Session("AdminUserID")
        Dim TipoEntrada As String = "EN"
        Dim IdEntradaWEB As Integer
        Dim numeroDeCancelaciones As Integer

        Dim RECIBIR As Integer
        Dim CANTPEND As Integer

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
        'Se verifica que el grid con los articulos contenga datos
        If grid1Data.Length > 0 Then
            If Session("WHSID") = "" Then
                mensaje = "Seleccione una Tienda antes de Proceder"
            Else
                'Primero Validamos que todos los codigos de barras existan en EL envío, en caso que alguno falte se regresa False y mensaje de error

                For Each row In grid1Data
                    RECIBIR = row.Item("RECIBIR")
                    CANTPEND = row.Item("CantidadPendiente")

                    If RECIBIR > CANTPEND Then
                        mensaje = "La cantidad a Recibir debe ser igual o menor a la cantidad Pendiente. Modelo: " & row.item("MODELO")
                        Return False
                    End If
                    If RECIBIR >= 1 Then
                        numeroDeCancelaciones += 1
                    End If
                Next
                'Return False
                If numeroDeCancelaciones = 0 Then
                    mensaje = "La cantidad a recibir debe ser al menos de 1. "
                    Return False
                End If

                'Creamos recepcion 
                sQueryInsert = ""
                sQueryInsert2 = ""
                counter = 0
                For Each row In grid1Data
                    RECIBIR = row.Item("RECIBIR")
                    CANTPEND = row.Item("CantidadPendiente")
                    If RECIBIR >= 1 Then
                        counter = counter + 1
                        sQueryInsert &= "INSERT INTO Recepciones (IDEnvio, LineaEnvio, Articulo, Cantidad, AlmacenOrigen, AlmacenDestino, Status, Fecha) " & vbCrLf & _
                                  "VALUES (" & row.Item("IDEnvio") & "," & row.Item("Partida") & ",'" & row.Item("Articulo") & "'," & _
                                  row.item("RECIBIR") & ",'" & row.Item("TiendaOrigen") & "','" & Session("IDSTORE") & "','O', GetDate())" & vbCrLf

                        'armamos el update a detalle de envio
                        Dim Status As String = "O"
                        If CANTPEND - RECIBIR = 0 Then
                            Status = "RE"
                        End If
                        sQueryInsert &= "UPDATE EnviosDetalle SET  CantidadPendiente = CantidadPendiente - " & row.item("RECIBIR") & ", Status = '" & Status & "' " & vbCrLf & _
                               "WHERE  IDEnvio = '" & row.Item("IDEnvio") & "' AND (Articulo = '" & row.Item("Articulo") & "')   " & vbCrLf
                        'Buscamos si el articulo existe en el inventario del almacen que recibe
                        'si no esta se crea
                        sQuery = "SELECT     IDInv, IDArticulo, Cantidad, Almacen FROM Inventarios " & vbCrLf & _
                                                "WHERE    (IDArticulo = '" & row.Item("Articulo") & "') AND (Almacen = '" & Session("IDSTORE") & "') " & vbCrLf
                        dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                        If dt2.Rows.Count > 0 Then
                            'Si encontramos registro actualizamos la cantidad
                            For Each drow2 As DataRow In dt2.Rows
                                sQueryInsert &= "UPDATE Inventarios SET Cantidad = Cantidad + " & row.item("RECIBIR") & " " & vbCrLf & _
                                 "WHERE     (IDArticulo = '" & row.Item("Articulo") & "') AND (Almacen = '" & Session("IDSTORE") & "')  " & vbCrLf
                                Exit For
                            Next
                        Else
                            Dim sQueryInsertFirstInventario As String
                            'insertamos el articulo en inventarios con el nuevo almacen
                            sQueryInsertFirstInventario = " Insert into Inventarios (IDArticulo, Cantidad, Almacen) " & vbCrLf & _
                            " values (" & row.Item("Articulo") & "," & row.item("RECIBIR") & ",'" & Session("IDSTORE") & "' )" & vbCrLf
                            dt = oDB.EjecutaQry_Tabla(sQueryInsertFirstInventario, CommandType.Text, "Inventario", connstringWEB)
                        End If
                    End If

                Next

                Dim idRecepcion As Integer
                'ejecutamos el insert a recepciones
                ' dt = oDB.EjecutaQry_Tabla(sQueryInsert, CommandType.Text, "Recepciones", connstringWEB)
                If oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB) = 2 Then
                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "Se encontro un problema al procesar la recepción por favor intente de nuevo"
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()
                    Return False
                    Exit Function
                Else
                    sQueryInsert = ""

                    'Obtenemos ID recepcion
                    sQuery = "select top 1 IDRecepcion from RECEPCIONES order by IDRECEPCION DESC "
                    dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                    If dt2.Rows.Count > 0 Then
                        For Each drow2 As DataRow In dt2.Rows
                            idRecepcion = drow2.Item("IDRECEPCION")
                        Next
                    End If

                    sQuery = "" & _
                    "SELECT IDRecepcion FROM RECEPCIONES where IDRecepcion not in" & vbCrLf & _
                    "(select IdOperacion  from LOGS where TipoOperacion='RE')"
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EntradasOCLastID", connstringWEB)
                    For Each Drow As DataRow In dt.Rows
                        InsertaLog(Tipos.RE, Drow.Item("IDRecepcion"), Session("IDSTORE"), Session("WHSID"))
                    Next

                    mensaje = "Operación realizada con Éxito. "
                    Session("ExitoRecepcion") = True

                End If


            End If
            Return True
        Else
            mensaje = "Debe agregar artículos a la recepción."
        End If
    End Function
End Class