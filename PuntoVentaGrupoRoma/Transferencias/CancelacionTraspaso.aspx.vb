﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml
Imports System.Data
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports System.IO.Path

Public Class CancelacionTraspaso
    Inherits System.Web.UI.Page

    Private mensaje As String = ""
    Private connstringWEB As String
    Private connstringSAP As String
    Private oDB As New DBMaster

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            If Session("STORETYPEID") = 3 Then
                Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
            End If
            If Session("ExitoCancelacion") = True Then
                Session("ExitoRecepcion") = False
                Dim strMesssage As String = "Operacion Realizada con Éxito"
                'ClientScript.RegisterStartupScript(Me.[GetType](), "alert", "<script>alert('" & strMesssage & "');</script>")
                ClientScript.RegisterStartupScript(Me.[GetType](), "alert", "<script>alert('" & strMesssage & "');</script>")

                Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
                msgConfig.Icon = MessageBox.Icon.INFO
                msgConfig.Title = "RESULTADO"
                msgConfig.Message = "Operacion Realizada con Éxito"
            End If
            Session("ExitoCancelacion") = False
        Else
            Dim CONSTRING As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            Dim INITIALCATALOG = CONSTRING.Split(";")(1).Split("=").Clone(1) & ".DBO.OITM"

            Dim SelectCommand As String
            SelectCommand = _
            "SELECT     ENVD.IDEnvio, ENVD.Linea as Partida, ENVD.Articulo, ART.ArticuloSBO, ENVD.TiendaOrigen, ENVD.TiendaDestino," & vbCrLf & _
            "           SAPOITM.U_BXP_MARCA as LINEA, SAPOITM.U_BXP_MEDIDA AS MEDIDA, " & vbCrLf & _
            "           SAPOITM.ItemName AS MODELO, ENVD.Cantidad, ENVD.CantidadPendiente, ENVD.Status, ENVD.CantidadPendiente as CANCELAR" & vbCrLf & _
            "FROM         EnviosDetalle ENVD INNER JOIN" & vbCrLf & _
            "             Articulos ART ON ENVD.Articulo = ART.IDArticulo  inner join" & vbCrLf & _
            "             " & INITIALCATALOG & " SAPOITM ON ART.ArticuloSBO = SAPOITM.ItemCode " & vbCrLf & _
            "WHERE     (ENVD.IDEnvio = @IDEnvio AND ENVD.Status = 'O') " & vbCrLf & _
            "ORDER BY ENVD.LINEA" & vbCrLf & _
            ""

            SqlPurchaseOrderDetailSAP.SelectCommand = SelectCommand

        End If
    End Sub

    Protected Sub StorePurchaseOrderDetail_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim id As String = e.Parameters("IDEnvio").ToString
        SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
        StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
        StorePurchaseOrderDetailSAP.DataBind()
        Session("ENVIOID") = id
    End Sub

    Protected Sub StoreGridPanel1_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim id As String = e.Parameters("IDEnvio").ToString
        SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
        StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
        StorePurchaseOrderDetailSAP.DataBind()
        Session("ENVIOID") = id
    End Sub


    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            'Habilitamos el botón de Transferencias 
            'msgConfig.Buttons = MessageBox.Button.OK
            'msgConfig.Icon = MessageBox.Icon.INFO
            'msgConfig.Title = "RESULTADO"
            'msgConfig.Message = mensaje
            'Dim id As String = Session("ENVIOID")
            'SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
            'StorePurchaseOrderDetailSAP.BaseParams.Item("IDEnvio") = id
            'StorePurchaseOrderDetailSAP.DataBind()
            Response.Redirect("CancelacionTraspaso.aspx")
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Dim id As String = Session("ENVIOID")
            SqlPurchaseOrderDetailSAP.SelectParameters("IDEnvio").DefaultValue = id
            StorePurchaseOrderDetailSAP.Parameters.Item("IDEnvio") = id
            StorePurchaseOrderDetailSAP.DataBind()
        End If
        Ext.Net.X.Msg.Show(msgConfig)
    End Sub



    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        Dim grid2Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim grid2Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim row2
        Dim counter As Integer = 0
        Dim counterIMEI As Integer = 0
        Dim counterICC As Integer = 0
        Dim counterAccesorios As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String
        Dim sQueryInsert2 As String
        Dim Almacen As String
        Dim IDUsuario As Integer = Session("AdminUserID")
        Dim TipoEntrada As String = "EN"
        Dim numeroDeCancelaciones As Integer

        Dim CANCELAR As Integer
        Dim CANTPEND As Integer

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
        'Se verifica que el grid con los articulos contenga datos
        If grid1Data.Length > 0 Then
            If Session("WHSID") = "" Then
                mensaje = "Seleccione una Tienda antes de Proceder"
            Else
                'Primero Validamos que todos los codigos de barras existan en EL envío, en caso que alguno falte se regresa False y mensaje de error

                For Each row In grid1Data
                    CANCELAR = row.Item("CANCELAR")
                    CANTPEND = row.Item("CantidadPendiente")

                    If CANCELAR > CANTPEND Then
                        mensaje = "La cantidad a Cancelar debe ser igual o menor a la cantidad Pendiente. Modelo: " & row.item("MODELO")
                        Return False
                    End If
                    If CANCELAR >= 1 Then
                        numeroDeCancelaciones += 1
                    End If
                Next
                'Return False

                If numeroDeCancelaciones = 0 Then
                    mensaje = "La cantidad debe ser al menos de 1. Modelo: " & row.item("MODELO")
                    Return False
                End If

                'Creamos recepcion 
                sQueryInsert = ""
                sQueryInsert2 = ""
                counter = 0
                For Each row In grid1Data
                    CANCELAR = row.Item("CANCELAR")
                    CANTPEND = row.Item("CantidadPendiente")

                    If CANCELAR >= 1 Then
                        counter = counter + 1

                        'armamos el update a detalle de envio
                        Dim Status As String = "O"
                        If CANTPEND - CANCELAR = 0 Then
                            Status = "C"
                        Else
                            Status = "O"
                        End If
                        sQueryInsert &= "UPDATE EnviosDetalle SET CantidadPendiente = CantidadPendiente - " & row.item("CANCELAR") & ", Status = '" & Status & "' " & vbCrLf & _
                               "WHERE  IDEnvio = '" & row.Item("IDEnvio") & "' AND (Articulo = '" & row.Item("Articulo") & "')   " & vbCrLf
                        'Buscamos si el articulo existe en el inventario del almacen que recibe
                        'si no esta se crea
                        sQueryInsert &= "UPDATE Inventarios SET Cantidad = Cantidad + " & row.item("CANCELAR") & " " & vbCrLf & _
                                 "WHERE     (IDArticulo = '" & row.Item("Articulo") & "') AND (Almacen = '" & Session("IDSTORE") & "')  " & vbCrLf

                        sQueryInsert &= "insert into CancelacionEnvios (Linea, Articulo, Cantidad, TiendaOrigen, TiendaDestino, IDUsuario, IDEnvio, FechaCancelacion) " & vbCrLf & _
                            "VALUES ('" & counter & "'," & row.Item("Articulo") & ",'" & row.item("CANCELAR") & _
                            "','" & Session("IDSTORE") & "','" & row.Item("TiendaDestino") & "','" & Session("AdminUserID") & "','" & row.Item("IDEnvio") & "',getdate())" & vbCrLf


                    End If

                Next


                'ejecutamos el insert a recepciones
                ' dt = oDB.EjecutaQry_Tabla(sQueryInsert, CommandType.Text, "Recepciones", connstringWEB)
                If oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB) = 2 Then
                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "Se encontro un problema al procesar la cancelación por favor intente de nuevo"
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()
                    Return False
                    Exit Function
                Else
                    sQueryInsert = "" & _
                    "SELECT IDCancelacion FROM CancelacionEnvios where IDCancelacion not in" & vbCrLf & _
                    "(select IdOperacion  from LOGS where TipoOperacion='CE')"
                    dt = oDB.EjecutaQry_Tabla(sQueryInsert, CommandType.Text, "EntradasOCLastID", connstringWEB)
                    For Each Drow As DataRow In dt.Rows
                        InsertaLog(Tipos.CE, Drow.Item("IDCancelacion"), Session("IDSTORE"), Session("WHSID"))
                    Next

                    mensaje = "Operación realizada con Éxito. "
                    Session("ExitoCancelacion") = True
                End If


            End If

            Return True
        Else
            mensaje = "Debe agregar artículos a la recepción."
        End If

    End Function

End Class
