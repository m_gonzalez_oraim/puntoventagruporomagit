﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml

Public Class Envios
    Inherits System.Web.UI.Page

    Private connstringWEB As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    Private connstringSAP As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
    Private oDB As New DBMaster
    Private sQuery As String
    Private dt As New DataTable
    Private tipoMensaje As String
    Private mensaje As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("AdminUserID") > 0 And Session("IDSTORE") > 0 Then
            If Not Ext.Net.X.IsAjaxRequest Then
                If Session("STORETYPEID") = 3 Then
                    Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
                End If
                Try
                    Session("POSTBACK") = ""
                    Session("IDPRINTVENTA") = 0
                    sQuery = _
                    "SELECT     UserStores.AdminStoreToSendID as AdminStoreID, AdminStore.StoreName, AdminStore.WhsId " & vbCrLf & _
                    "FROM         UserStores INNER JOIN  " & vbCrLf & _
                    "                      AdminStore ON UserStores.AdminStoreToSendID = AdminStore.AdminStoreID " & vbCrLf & _
                    "WHERE     (UserStores.AdminUserID = '" & Session("ADMINUSERID") & "') " & vbCrLf & _
                    "       AND (UserStores.AdminStoreToSendID > 0)  AND AdminStore.adminstoreid <> " & Session("IDSTORE") & " "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringWEB)
                    StoreCmb.DataSource = dt
                    StoreCmb.DataBind()
                    ComboBox1.DataBind()

                    sQuery = "select ItmsTypCod,ItmsGrpNam from OITG WHERE ItmsTypCod IN (1,2,3,4,5,6,7)"
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                    StoreArticulo.DataSource = dt
                    StoreArticulo.DataBind()
                    cmbArticulo.DataBind()
                Catch ex As Exception
                    tipoMensaje = "Error"
                    mensaje = ex.Message
                    Mensajes(tipoMensaje, mensaje)
                End Try
            End If
        Else
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If
    End Sub

    Public Sub CambiaArticulo()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If

            sQuery = "select distinct U_BXP_MARCA as CODE, U_BXP_MARCA as NAME from OITM " & vbCrLf & _
                "where QryGroup" & Value & "='Y' and frozenFor='N' order by U_BXP_MARCA"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreLinea.DataSource = dt
                StoreLinea.DataBind()
                cmbLinea.DataBind()
                'cmbLinea.SetInitValue("")

                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                Stock.Text = 0

                tipoMensaje = "Error"
                mensaje = "No existen Lineas para esta categoria de articulo '" & DirectCast(Ext.Net.ExtNet.GetCtl("CMBArticulo"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)

            Else
                StoreLinea.DataSource = dt
                StoreLinea.DataBind()
                cmbLinea.DataBind()
                'cmbLinea.SetInitValue(dt.Rows.Item(0).Item(0))
                'cmbLinea.SetInitValue("")

                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")
                Stock.Text = 0

            End If

            ArtName.Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Text
            ' cmbLinea.Render()
        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True
            Cantidad.Text = 1
            Cantidad.Disabled = True
        End Try
    End Sub

    Public Sub CambiaLinea()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If
            sQuery = "select distinct U_BXP_MEDIDA as CODE, U_BXP_MEDIDA as NAME from OITM " & vbCrLf & _
            "where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value & "='Y' and U_BXP_MARCA='" & Value & "'"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""

                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                Stock.Text = 0

                tipoMensaje = "Error"
                mensaje = "No existen medidas para esta línea '" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)
            Else
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
               ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")
                Stock.Text = 0
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True
            Cantidad.Text = 1
            Cantidad.Disabled = True
        End Try
    End Sub

    Public Sub CambiaMedida()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbMedida"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If
            sQuery = "select distinct ItemCode as CODE, ItemName as NAME from OITM " & vbCrLf & _
            "where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value & "='Y' " & vbCrLf & _
            "and U_BXP_MARCA='" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Value & "' and U_BXP_MEDIDA='" & Value & "' ORDER BY ITEMNAME"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                Stock.Text = 0

                tipoMensaje = "Error"
                mensaje = "No existen modelos para esta Medida '" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbMedida"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)
            Else
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")
                Stock.Text = 0
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True
            Cantidad.Text = 1
            Cantidad.Disabled = True
        End Try
    End Sub

    Public Sub CambiaModelo()
        Try
            dt = Store1.DataSource
            Dim itemcode As String
            Dim modelo As String
            Dim iStock As Integer

            itemcode = DirectCast(Ext.Net.ExtNet.GetCtl("cmbModelo"), ComboBox).SelectedItem.Value
            modelo = DirectCast(Ext.Net.ExtNet.GetCtl("cmbModelo"), ComboBox).SelectedItem.Text
            ItemName.Value = modelo

            sQuery = "" & _
            "SELECT     inv.Cantidad, inv.Almacen, art.ArticuloSBO" & vbCrLf & _
            "FROM         Inventarios AS inv INNER JOIN" & vbCrLf & _
            "                      Articulos AS art ON art.IDArticulo = inv.IDArticulo " & vbCrLf & _
            "WHERE     (inv.Almacen = " & Session("IDSTORE") & ") AND (inv.Cantidad > 0) and art.ArticuloSBO = '" & itemcode & "'" & vbCrLf & _
            ""
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringWEB)
            If dt.Rows.Count = 0 Then
                Stock.Text = iStock
                btnAgregar.Disabled = False
                Cantidad.Text = 1
                Cantidad.Disabled = True
                tipoMensaje = "Atención"
                mensaje = "No cuenta con stock del modelo " & modelo
                Mensajes(tipoMensaje, mensaje)

            Else
                Cantidad.Disabled = False
                Cantidad.Text = 1
                btnAgregar.Disabled = False
                ItemName.Value = modelo
                iStock = dt.Rows.Item(0).Item(0)
                Stock.Text = iStock
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try
    End Sub

    Protected Sub SelectModel(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If findModel(e) Then
            'Me.GridPanel1.LoadContent()
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
        End If

    End Sub

    Private Function findModel(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim counter As Integer = 0
        Dim IDUsuario As Integer = Session("AdminUserID")

        Try
            'Se verifica que el grid con el detalle de la orden de compra contenga datos
            If grid1Data.Length > 0 Then
                'Revisamos si los articulos estan creados en web
                For Each row In grid1Data
                    Dim codeBarCount As Integer = 0
                    Dim IDCategoria As Integer = 0
                    Dim ArticuloSBO As String = ""
                    Dim DescripcionSBO As String = ""
                    counter = counter + 1
                    ArticuloSBO = row.item("Modelo")
                    DescripcionSBO = row.item("ItemName")
                    If DirectCast(Ext.Net.ExtNet.GetCtl("cmbModelo"), ComboBox).SelectedItem.Value = ArticuloSBO Then
                        mensaje = "Modelo ya seleccionado."
                        Return False
                    End If
                Next
                CambiaModelo()
                Return True
            Else
                CambiaModelo()
                Return True
            End If
        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try


    End Function

    Private Function Mensajes(ByVal Tittle As String, ByVal Html As String) As Boolean
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-exclamation"
        nconf.Html = Html
        nconf.Title = Tittle
        nconf.HideDelay = 5000
        msg.Configure(nconf)
        msg.Show()
    End Function

    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            'Habilitamos el botón de Transferencias 
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "RESULTADO"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
            'Me.GridPanel1.LoadContent()
            Me.GridPanel1.ClearContent()
            Response.Redirect("../printControl/imprimeventa.aspx")
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
        End If

    End Sub

    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String = ""
        Dim sQueryInsert2 As String = ""
        Dim IDTienda As String
        Dim IDUsuario As Integer = Session("AdminUserID")
        Dim IdEnvioWEB As Integer
        Dim ret As Boolean
        Dim IdTiendaDestino As Integer


        Try
            If ComboBox1.SelectedItem.Value = "" Then
                IdTiendaDestino = -1
            Else
                IdTiendaDestino = ComboBox1.SelectedItem.Value
            End If

            'Se verifica que el grid con el detalle de la orden de compra contenga datos
            If grid1Data.Length > 0 Then
                If IdTiendaDestino < 1 Then
                    mensaje = "Seleccione una Tienda antes de Proceder"
                    Return False
                Else

                    IDTienda = Session("IDSTORE")

                    Dim ArticuloSBO As String = ""
                    Dim DescripcionSBO As String = ""
                    Dim cantidad As Integer
                    Dim iStock As Integer

                    'Revisamos si los modelos tienen stock suficiente
                    For Each row In grid1Data
                        ArticuloSBO = row.item("Modelo")
                        DescripcionSBO = row.item("ItemName")
                        cantidad = row.item("Cantidad")
                        'Verificamos stock por cada modelo, si alguno no es suficente se detiene el proceso y se notifica
                        sQuery = "" & _
                            "SELECT     inv.Cantidad, inv.Almacen, art.ArticuloSBO" & vbCrLf & _
                            "FROM         Inventarios AS inv INNER JOIN" & vbCrLf & _
                            "                      Articulos AS art ON art.IDArticulo = inv.IDArticulo " & vbCrLf & _
                            "WHERE     (inv.Almacen = " & Session("IDSTORE") & ") AND (inv.Cantidad > 0) and art.ArticuloSBO = '" & ArticuloSBO & "'" & vbCrLf & _
                            ""
                        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Almacen", connstringWEB)
                        If dt.Rows.Count > 0 Then
                            iStock = dt.Rows.Item(0).Item(0)
                            If cantidad > iStock Then
                                mensaje = "ERROR EN SALIDA: La cantidad disponible del modelo: " & DescripcionSBO & _
                                " es insuficiente. Stock: " & iStock & ""
                                Return False
                            End If
                        Else
                            mensaje = "Error al verificar disponibilidad del modelo: " & DescripcionSBO
                            Return False
                        End If
                    Next


                    For Each row In grid1Data
                        counter = counter + 1
                        ArticuloSBO = row.item("Modelo")
                        cantidad = row.item("Cantidad")
                        If counter = 1 Then
                            Dim fechaSalida As String
                            fechaSalida = Format(Now(), "dd/MM/yyyy").ToString
                            sQuery = "set dateformat dmy INSERT INTO EnviosEncabezado (FechaEnvio, Status, IDUsuario, TipoEnvio)" & vbCrLf & _
                            "VALUES (GETDATE(),'O','" & IDUsuario & "','EN')"
                            oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB)
                            'Obtenemos el id del documento de salida creado en web
                            sQuery = "SELECT   top 1  IDEnvio " & vbCrLf & _
                            "FROM EnviosEncabezado order by IDEnvio desc "
                            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EnviosEncabezado", connstringWEB)
                            If dt.Rows.Count > 0 Then
                                IdEnvioWEB = dt.Rows.Item(0).Item(0)
                            End If
                        End If

                        If IdEnvioWEB > 0 Then
                            sQueryInsert &= "INSERT INTO EnviosDetalle (IDEnvio, Linea, Articulo, Cantidad, TiendaOrigen, TiendaDestino, CantidadPendiente, Status) " & vbCrLf & _
                                "VALUES ('" & IdEnvioWEB & "','" & counter & "',(Select IDArticulo from articulos where ArticuloSBO ='" & ArticuloSBO & "'),'" & cantidad & _
                                "','" & IDTienda & "','" & IdTiendaDestino & "','" & cantidad & "','O')" & vbCrLf
                        End If
                    Next


                    'Si llegamos a este punto podemos crear el detalle de la salida
                    ret = oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB)

                    Dim contadorDetOC As Integer = 0
                    sQuery = "" & _
                            "SELECT      Articulo, Cantidad  " & vbCrLf & _
                            "FROM         EnviosDetalle " & vbCrLf & _
                            "where IDEnvio = " & IdEnvioWEB & " " & vbCrLf & _
                            " "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "CheckInv", connstringWEB)
                    sQuery = ""
                    sQueryInsert = ""
                    If dt.Rows.Count > 0 Then
                        For Each drow As DataRow In dt.Rows
                            contadorDetOC += 1
                            sQuery = "SELECT     IDInv, IDArticulo, Cantidad, Almacen FROM Inventarios " & vbCrLf & _
                            "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & IDTienda & "') " & vbCrLf
                            dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                            If dt2.Rows.Count > 0 Then
                                'Si encontramos registro actualizamos la cantidad
                                For Each drow2 As DataRow In dt2.Rows
                                    sQueryInsert2 &= "UPDATE Inventarios SET Cantidad = Cantidad - " & drow.Item("Cantidad") & "" & vbCrLf & _
                                     "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & IDTienda & "')  " & vbCrLf
                                    Exit For
                                Next
                            End If
                        Next
                        ret = oDB.EjecutaQry(sQueryInsert2, CommandType.Text, connstringWEB)
                        mensaje = "Operación realizada con Éxito."
                        InsertaLog(Tipos.EV, IdEnvioWEB, Session("IDSTORE"), Session("WHSID"))
                    End If
                End If
                Session("POSTBACK") = "Envios"
                Session("IDPRINTVENTA") = IdEnvioWEB
                Return True
            Else
                mensaje = "Debe capturar al menos un modelo para realizar la salida."
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try


    End Function

    Protected Sub DisableClick(ByVal sender As Object, ByVal e As DirectEventArgs)
        btnAgregar.Disabled = True
    End Sub

End Class