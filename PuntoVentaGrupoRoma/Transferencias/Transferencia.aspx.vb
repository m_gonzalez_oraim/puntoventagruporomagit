﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml
Imports System.Net

Public Class Transferencia
    Inherits System.Web.UI.Page

    Private connstringWEB As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    Private connstringSAP As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
    Private oDB As New DBMaster
    Private sQuery As String
    Private dt As New DataTable
    Private tipoMensaje As String
    Private mensaje As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Request("hdnCrear") = "1" Then
            If Not CrearSolicitud() Then
                Context.Response.StatusCode = HttpStatusCode.InternalServerError
                Context.Response.ContentType = "text/plain"
                Context.Response.StatusDescription = "Error al crear la solicitud. Contacte con su administrador"
            End If
            'LimpiarCampos();

            Return
        End If

        If Session("AdminUserID") > 0 And Session("IDSTORE") > 0 Then
            If Not Ext.Net.X.IsAjaxRequest Then
                Try
                    sQuery = _
                    "SELECT     UserStores.AdminStoreToSendID as AdminStoreID, AdminStore.StoreName, AdminStore.WhsId " & vbCrLf & _
                    "FROM         UserStores INNER JOIN  " & vbCrLf & _
                    "                      AdminStore ON UserStores.AdminStoreToSendID = AdminStore.AdminStoreID " & vbCrLf & _
                    "WHERE     (UserStores.AdminUserID = '" & Session("ADMINUSERID") & "') " & vbCrLf & _
                    "       AND (UserStores.AdminStoreToSendID > 0)  AND AdminStore.adminstoreid <> " & Session("IDSTORE") & " "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringWEB)

                    StoreCmb.DataSource = dt
                    StoreCmb.DataBind()
                    ''ComboBox1.DataBind()

                Catch ex As Exception
                    tipoMensaje = "Error"
                    mensaje = ex.Message
                    Mensajes(tipoMensaje, mensaje)
                End Try
            End If
        Else
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If
    End Sub

    Private Function CrearSolicitud() As Boolean
        Dim controlSolicitud As ControlSolicitud = New ControlSolicitud()
        Dim solicitudCrear As Solicitud = Solicitud.InformacionSolicitudFromRequest(Session("IDSTORE"), Session("AdminUserId"), Request)

        Return controlSolicitud.CrearSolicitud(solicitudCrear)

    End Function

    'Private Function LimpiarCampos(ByVal idLinea As String, ByRef request As System.Web.HttpRequest, ByVal i As Integer)

    '    request("txtNombreArticulo").Remove();
    '    Dim codigoArticulo As String = request("txtCodigoArticulo" & idLinea)
    '    Dim factor As String = request("cmbUnidadMedida" & idLinea)
    '    Dim unidadMedida As String = request("hdnUnidadMedida" & idLinea)
    '    Dim cantidad As Decimal = request("txtCantidad" & idLinea)
    '    'cambiar el valor de unidad de medida cuando cambie el valor del combo

    '    Dim linea As LineaSolicitud = New LineaSolicitud()
    '    linea.Linea = i + 1
    '    linea.CodigoArticulo = codigoArticulo
    '    linea.NombreArticulo = nombreArticulo
    '    linea.Cantidad = cantidad
    '    linea.UnidadMedida = unidadMedida
    '    linea.FactorUnidadMedida = factor



    'End Function
    Private Function Mensajes(ByVal Tittle As String, ByVal Html As String) As Boolean
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-exclamation"
        nconf.Html = Html
        nconf.Title = Tittle
        nconf.HideDelay = 5000
        msg.Configure(nconf)
        msg.Show()
    End Function

    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            'Habilitamos el botón de Transferencias 
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "RESULTADO"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
            Response.Redirect("../printControl/imprimeventa.aspx")
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
        End If

    End Sub

    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String = ""
        Dim sQueryInsert2 As String = ""
        Dim IDTienda As String
        Dim IDUsuario As Integer = Session("AdminUserID")
        Dim IdEnvioWEB As Integer
        Dim ret As Boolean
        Dim IdTiendaDestino As Integer


        Try
            'Se verifica que el grid con el detalle de la orden de compra contenga datos
            If grid1Data.Length > 0 Then
                If IdTiendaDestino < 1 Then
                    mensaje = "Seleccione una Tienda antes de Proceder"
                    Return False
                Else

                    IDTienda = Session("IDSTORE")

                    Dim ArticuloSBO As String = ""
                    Dim DescripcionSBO As String = ""
                    Dim cantidad As Integer
                    Dim iStock As Integer

                    'Revisamos si los modelos tienen stock suficiente
                    For Each row In grid1Data
                        ArticuloSBO = row.item("Modelo")
                        DescripcionSBO = row.item("ItemName")
                        cantidad = row.item("Cantidad")
                        'Verificamos stock por cada modelo, si alguno no es suficente se detiene el proceso y se notifica
                        sQuery = "" & _
                            "SELECT     inv.Cantidad, inv.Almacen, art.ArticuloSBO" & vbCrLf & _
                            "FROM         Inventarios AS inv INNER JOIN" & vbCrLf & _
                            "                      Articulos AS art ON art.IDArticulo = inv.IDArticulo " & vbCrLf & _
                            "WHERE     (inv.Almacen = " & Session("IDSTORE") & ") AND (inv.Cantidad > 0) and art.ArticuloSBO = '" & ArticuloSBO & "'" & vbCrLf & _
                            ""
                        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Almacen", connstringWEB)
                        If dt.Rows.Count > 0 Then
                            iStock = dt.Rows.Item(0).Item(0)
                            If cantidad > iStock Then
                                mensaje = "ERROR EN SALIDA: La cantidad disponible del modelo: " & DescripcionSBO & _
                                " es insuficiente. Stock: " & iStock & ""
                                Return False
                            End If
                        Else
                            mensaje = "Error al verificar disponibilidad del modelo: " & DescripcionSBO
                            Return False
                        End If
                    Next


                    For Each row In grid1Data
                        counter = counter + 1
                        ArticuloSBO = row.item("Modelo")
                        cantidad = row.item("Cantidad")
                        If counter = 1 Then
                            Dim fechaSalida As String
                            fechaSalida = Format(Now(), "dd/MM/yyyy").ToString
                            sQuery = "set dateformat dmy INSERT INTO EnviosEncabezado (FechaEnvio, Status, IDUsuario, TipoEnvio)" & vbCrLf & _
                            "VALUES (GETDATE(),'O','" & IDUsuario & "','EN')"
                            oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB)
                            'Obtenemos el id del documento de salida creado en web
                            sQuery = "SELECT   top 1  IDEnvio " & vbCrLf & _
                            "FROM EnviosEncabezado order by IDEnvio desc "
                            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EnviosEncabezado", connstringWEB)
                            If dt.Rows.Count > 0 Then
                                IdEnvioWEB = dt.Rows.Item(0).Item(0)
                            End If
                        End If

                        If IdEnvioWEB > 0 Then
                            sQueryInsert &= "INSERT INTO EnviosDetalle (IDEnvio, Linea, Articulo, Cantidad, TiendaOrigen, TiendaDestino, CantidadPendiente, Status) " & vbCrLf & _
                                "VALUES ('" & IdEnvioWEB & "','" & counter & "',(Select IDArticulo from articulos where ArticuloSBO ='" & ArticuloSBO & "'),'" & cantidad & _
                                "','" & IDTienda & "','" & IdTiendaDestino & "','" & cantidad & "','O')" & vbCrLf
                        End If
                    Next

                    'Si llegamos a este punto podemos crear el detalle de la salida
                    ret = oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB)

                    Dim contadorDetOC As Integer = 0
                    sQuery = "" & _
                            "SELECT      Articulo, Cantidad  " & vbCrLf & _
                            "FROM         EnviosDetalle " & vbCrLf & _
                            "where IDEnvio = " & IdEnvioWEB & " " & vbCrLf & _
                            " "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "CheckInv", connstringWEB)
                    sQuery = ""
                    sQueryInsert = ""
                    If dt.Rows.Count > 0 Then
                        For Each drow As DataRow In dt.Rows
                            contadorDetOC += 1
                            sQuery = "SELECT     IDInv, IDArticulo, Cantidad, Almacen FROM Inventarios " & vbCrLf & _
                            "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & IDTienda & "') " & vbCrLf
                            dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                            If dt2.Rows.Count > 0 Then
                                'Si encontramos registro actualizamos la cantidad
                                For Each drow2 As DataRow In dt2.Rows
                                    sQueryInsert2 &= "UPDATE Inventarios SET Cantidad = Cantidad - " & drow.Item("Cantidad") & "" & vbCrLf & _
                                     "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & IDTienda & "')  " & vbCrLf
                                    Exit For
                                Next
                            End If
                        Next
                        ret = oDB.EjecutaQry(sQueryInsert2, CommandType.Text, connstringWEB)
                        mensaje = "Operación realizada con Éxito."
                        InsertaLog(Tipos.EV, IdEnvioWEB, Session("IDSTORE"), Session("WHSID"))
                    End If
                End If
                Session("POSTBACK") = "Envios"
                Session("IDPRINTVENTA") = IdEnvioWEB
                Return True
            Else
                mensaje = "Debe capturar al menos un modelo para realizar la salida."
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try
    End Function

    Protected Sub DisableClick(ByVal sender As Object, ByVal e As DirectEventArgs)
        ''btnAgregar.Disabled = True
    End Sub

    Public Sub CargarInformacionArticuloSeleccionadoDeBuscador()
        Dim nombreArticulo As String = cmbBusquedaArticulo.SelectedItem.Text
        Dim codigoArticulo As String = cmbBusquedaArticulo.SelectedItem.Value

        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo

        prepararInterfazCapturaLinea(codigoArticulo)

    End Sub
    Private Sub prepararInterfazCapturaLinea(ByVal codigoArticulo As String)
        Dim controlSolicitud As ControlSolicitud = New ControlSolicitud()
        Dim unidadesMedida As List(Of UnidadMedida) = Nothing

        If Not controlSolicitud.InformacionSolicitudArticulo(codigoArticulo, unidadesMedida) Then
            cmbUnidadMedida.ReadOnly = True
            InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)
            hdnItemsComboUnidadMedida.Value = 0
            txtCantidad.ReadOnly = True
            txtCantidad.Value = 1

            BtnAgregarLinea.Disabled = True
            MessageBoxExtNet.ShowError("No se pudo obtener la informacion para el articulo seleccionado. Consulte con su administrador")
            Return
        End If
        InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)

        For i As Integer = 0 To unidadesMedida.Count - 1
            cmbUnidadMedida.InsertItem(i, unidadesMedida(i).UnidadMedida, unidadesMedida(i).Factor)
        Next
        cmbUnidadMedida.Select(0)
        hdnItemsComboUnidadMedida.Value = unidadesMedida.Count

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1
        BtnAgregarLinea.Disabled = False

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1

    End Sub

    Public Sub InicializarValoresCombo(ByRef combo As Ext.Net.ComboBox, ByVal numeroRegistros As Integer)
        For i As Integer = 0 To numeroRegistros - 1
            combo.RemoveByIndex(0)
        Next
    End Sub
#Region "Metodos Control Ventana Clientes"

    <DirectMethod()> _
    Public Sub MostrarVentanaBusquedaArticulos()

        StoreArticulos.Data = Nothing
        StoreArticulos.DataBind()

        txtCodigoArticuloBusqueda.Value = String.Empty
        txtNombreArticuloBusqueda.Value = String.Empty

        hdnUltimoCodigoBuscado.Value = "|[NONE]|"
        hdnUltimoNombreBuscado.Value = Nothing

        winArticulos.Show()
    End Sub

    <DirectMethod>
    Public Sub CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(ByVal parametrosRegistroSeleccionado As String)
        Dim valoresArticuloSeleccionado As List(Of String) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of String))(parametrosRegistroSeleccionado)
        Dim codigoArticulo As String = valoresArticuloSeleccionado(0)
        Dim nombreArticulo As String = valoresArticuloSeleccionado(1)

        cmbBusquedaArticulo.Text = nombreArticulo
        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo
        winArticulos.Close()
        prepararInterfazCapturaLinea(codigoArticulo)
    End Sub

    <DirectMethod()> _
    Public Sub AgregarLineaVenta()
        If hdnCodigoArticulo.Value = String.Empty Then
            Ext.Net.X.MessageBox.Alert("Mensaje Sistema", "No se puede agregar la linea si no ha seleccionado el numero de articulo ")
            Return
        End If
        If txtCantidad.Value <= 0 Then
            Ext.Net.X.MessageBox.Alert("Mensaje Sistema", "La cantidad debe ser mayor a cero")
            Return
        End If
        Dim lineaVenta As LineaVenta = New LineaVenta()
        lineaVenta.Id = Guid.NewGuid().ToString()
        lineaVenta.CodigoArticulo = hdnCodigoArticulo.Value
        lineaVenta.NombreArticulo = hdnNombreArticulo.Value
        lineaVenta.Cantidad = txtCantidad.Value
        lineaVenta.UnidadDeMedida = New UnidadMedida()
        lineaVenta.UnidadDeMedida.UnidadMedida = cmbUnidadMedida.Value
        lineaVenta.UnidadDeMedida.Factor = cmbUnidadMedida.Text


        Dim controlArticulos As ControlArticulos = New ControlArticulos()
        lineaVenta.UnidadesDeMedida = controlArticulos.UnidadesMedidaArticulo("Inventario", lineaVenta.CodigoArticulo)

        AgregarRegistroDetalleDocumento(lineaVenta)
        inicializarInterfazCapturaLinea()

    End Sub
    Private Sub inicializarInterfazCapturaLinea()
        InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)
        cmbUnidadMedida.Value = String.Empty
        cmbBusquedaArticulo.Value = String.Empty
        hdnCodigoArticulo.Value = String.Empty
        hdnFactorArticulo.Value = String.Empty
        hdnNombreArticulo.Value = String.Empty
        txtCantidad.SetRawValue(String.Empty)
        txtCantidad.ReadOnly = True
        BtnAgregarLinea.Enabled = False
        cmbBusquedaArticulo.Focus()
    End Sub

    Private Sub AgregarRegistroDetalleDocumento(ByVal informacionLineaVenta As LineaVenta)
        Dim idRegistro As String = Guid.NewGuid().ToString()
        Dim registroContainer As Ext.Net.Container = New Container()
        registroContainer.Layout = "HBoxLayout"
        registroContainer.Width = PanelDetalleDocumento.Width
        registroContainer.ID = "contenedor" & idRegistro
        registroContainer.ClientIDMode = UI.ClientIDMode.Static

        Dim cajaCodigoArticulo As Ext.Net.TextField = New TextField()
        cajaCodigoArticulo.ID = "txtCodigoArticulo" & idRegistro
        cajaCodigoArticulo.Name = "txtCodigoArticulo" & idRegistro
        cajaCodigoArticulo.ClientIDMode = UI.ClientIDMode.Static
        cajaCodigoArticulo.Width = HeaderGridCodigo.Width
        cajaCodigoArticulo.ReadOnly = True
        cajaCodigoArticulo.Value = informacionLineaVenta.CodigoArticulo

        Dim cajaCk As Ext.Net.Checkbox = New Checkbox()
        cajaCk.ID = "txtCk" & idRegistro
        cajaCk.ClientIDMode = UI.ClientIDMode.Static
        cajaCk.Width = HeaderGridCk.Width
        'cajaCk.ReadOnly = True
        cajaCk.Value = True
        cajaCk.Listeners.Change.Handler = "MetodoDetalleEliminarRegistro.EliminarLineaGrid('" & idRegistro & "')"

        Dim cajaNombreArticulo As Ext.Net.TextField = New TextField()
        cajaNombreArticulo.ID = "txtNombreArticulo" & idRegistro
        cajaNombreArticulo.Name = "txtNombreArticulo" & idRegistro
        cajaNombreArticulo.ClientIDMode = UI.ClientIDMode.Static
        cajaNombreArticulo.Width = HeaderGridNombre.Width
        cajaNombreArticulo.ReadOnly = True
        cajaNombreArticulo.Value = informacionLineaVenta.NombreArticulo

        Dim comboUnidadMedida As Ext.Net.ComboBox = New ComboBox()
        comboUnidadMedida.ID = "cmbUnidadMedida" & idRegistro
        comboUnidadMedida.Name = "cmbUnidadMedida" & idRegistro
        comboUnidadMedida.ClientIDMode = UI.ClientIDMode.Static
        comboUnidadMedida.Width = HeaderGridUnidad.Width
        comboUnidadMedida.Enabled = True
        For i As Integer = 0 To informacionLineaVenta.UnidadesDeMedida.Count - 1
            comboUnidadMedida.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.UnidadesDeMedida(i).UnidadMedida, informacionLineaVenta.UnidadesDeMedida(i).Factor))
        Next
        comboUnidadMedida.Value = informacionLineaVenta.UnidadDeMedida.Factor
        comboUnidadMedida.Listeners.Select.Handler = "MetodoCambioUnidadMedida.CambiarUnidadMedida('" & idRegistro & "')"

        Dim hdnUnidadMedida As Ext.Net.Hidden = New Hidden()
        hdnUnidadMedida.ID = "hdnUnidadMedida" & idRegistro
        hdnUnidadMedida.Name = "hdnUnidadMedida" & idRegistro
        hdnUnidadMedida.ClientIDMode = UI.ClientIDMode.Static
        hdnUnidadMedida.Value = informacionLineaVenta.UnidadDeMedida.UnidadMedida

        Dim cajaCantidad As Ext.Net.NumberField = New NumberField()
        cajaCantidad.ID = "txtCantidad" & idRegistro
        cajaCantidad.Name = "txtCantidad" & idRegistro
        cajaCantidad.ClientIDMode = UI.ClientIDMode.Static
        cajaCantidad.Width = HeaderGridCantidad.Width
        cajaCantidad.Enabled = True
        cajaCantidad.Value = informacionLineaVenta.Cantidad
        cajaCantidad.DecimalPrecision = 2

        Dim containerHidden As Ext.Net.Container = New Container()
        containerHidden.Html = "<input type=""hidden"" name=""hdnAuxIdLinea"" value=""" & idRegistro & """ />"

        registroContainer.Items.Add(cajaCk)
        registroContainer.Items.Add(cajaCodigoArticulo)
        registroContainer.Items.Add(cajaNombreArticulo)
        registroContainer.Items.Add(comboUnidadMedida)
        registroContainer.Items.Add(hdnUnidadMedida)
        registroContainer.Items.Add(cajaCantidad)
        registroContainer.Items.Add(containerHidden)
        PanelDetalleDocumento.Items.Add(registroContainer)
        registroContainer.Render()

    End Sub
#End Region

#Region "Metodos Control Ventana Articulos"

    <DirectMethod>
    Public Function ControlPaginacionArticulos(accion As String, extraParams As Dictionary(Of String, Object)) As Object
        If hdnUltimoCodigoBuscado.Value = "|[NONE]|" Then
            Return Nothing
        End If

        Dim obtenerTotalRegistros As Boolean = False
        If hdnNuevaBusqueda.Value = "YES" Then
            hdnNuevaBusqueda.Value = "NO"
            obtenerTotalRegistros = True
        End If

        Dim codigoBusqueda As String = hdnUltimoCodigoBuscado.Value
        Dim nombreBusqueda As String = hdnUltimoNombreBuscado.Value

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim codigoAlmaceSAP As String = controlTienda.ObtenerCodigoAlmacenSAP(Session("IDSTORE"))
        If codigoAlmaceSAP = Nothing Then
            MessageBoxExtNet.ShowError("Codigo almacen tienda no encontrado")
            Return Nothing
        End If

        Dim parametrosPaginacion As StoreRequestParameters = New StoreRequestParameters(extraParams)
        Dim totalArticulos As Integer = 0

        Dim sortParameter As String = "Codigo"
        Dim sortDirection As String = "ASC"
        If parametrosPaginacion.Sort.Length > 0 Then
            sortParameter = parametrosPaginacion.Sort(0).Property
            sortDirection = parametrosPaginacion.Sort(0).Direction.ToString()
        End If

        Dim pagina As Integer = parametrosPaginacion.Page
        If pagina = 0 Then
            pagina = 1
        End If

        Dim articulosEncontrados As List(Of Articulo) = controlArticulo.BuscarArticulo(((pagina - 1) * parametrosPaginacion.Limit) + 1, pagina * parametrosPaginacion.Limit, sortParameter, sortDirection, codigoBusqueda, nombreBusqueda, "Inventario", Session("IDSTORE"), codigoAlmaceSAP, obtenerTotalRegistros, totalArticulos)
        If obtenerTotalRegistros Then
            hdnTotalRegistrosNuevaBusqueda.Value = totalArticulos
        Else
            totalArticulos = hdnTotalRegistrosNuevaBusqueda.Value
        End If
        Return New Paging(Of Articulo)(articulosEncontrados, totalArticulos)
    End Function

    Public Sub BuscarArticulos()
        Dim codigoBusqueda As String = txtCodigoArticuloBusqueda.Value
        Dim nombreBusqueda As String = txtNombreArticuloBusqueda.Value

        If codigoBusqueda Is Nothing Then
            codigoBusqueda = String.Empty
        End If

        If nombreBusqueda Is Nothing Then
            nombreBusqueda = String.Empty
        End If

        hdnUltimoCodigoBuscado.Value = codigoBusqueda
        hdnUltimoNombreBuscado.Value = nombreBusqueda
        hdnNuevaBusqueda.Value = "YES"
        StoreArticulos.LoadProxy()
    End Sub
#End Region
End Class
