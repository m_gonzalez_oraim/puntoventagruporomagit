﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RecepcionTraspaso.aspx.vb" Inherits="PuntoVentaGrupoRoma.RecepcionTraspaso" MasterPageFile="~/admin.master"%>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <script type="text/javascript">
        var AfterEdit = function (e) {
            
            e.grid.store.commitChanges()
        };

        var validate = function (e) {
            
            e.value = null;
        };

        var addRecord = function (form, grid) {
            //            debugger;
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").CODEBARS == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "El código de barras es obligatorio",
                    title: "Error"
                });
                return false;
            }
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();

        };


    </script>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
       <%-- ****************INICIA SQL DATA SOURCES*******************--%>
    <asp:SqlDataSource ID="SqlPurchaseOrderSAP" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DBConn  %>" SelectCommand="
        SELECT DISTINCT 
                EnviosEncabezado.IDEnvio, 
                CONVERT(VARCHAR, 
                EnviosEncabezado.FechaEnvio, 103) AS FechaEnvio,  
                (AdminUser.FirstName + ' ' + AdminUser.LastName) Usuario, 
                AdminStore.StoreName Tienda,
                EnviosEncabezado.IDUsuario, 
                EnviosEncabezado.TipoEnvio
        FROM         EnviosEncabezado INNER JOIN
                     EnviosDetalle ON EnviosEncabezado.IDEnvio = EnviosDetalle.IDEnvio INNER JOIN
                     AdminUser ON EnviosEncabezado.IDUsuario = AdminUser.AdminUserID INNER JOIN
                     AdminStore ON EnviosDetalle.TiendaOrigen  = AdminStore.AdminStoreID
        WHERE 
                (Select count(IDEnvio) from EnviosDetalle where Status = 'O' and TiendaDestino = @IDSTORE and IDEnvio = EnviosEncabezado.IDEnvio) &gt; 0
                AND  (EnviosDetalle.TiendaDestino = @IDSTORE)
        ">
        <SelectParameters>
            <asp:SessionParameter Name="IDSTORE" SessionField="IDSTORE" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlPurchaseOrderDetailSAP" runat="server"  
        ConnectionString="<%$ ConnectionStrings:DBConn %>"  >
        <SelectParameters>
            <asp:Parameter Name="IDEnvio" Type="Int32" DefaultValue="0" />  
        </SelectParameters> 
    </asp:SqlDataSource>
    
   <%-- ****************TERMINA SQL DATA SOURCES*******************--%>
    <table style="width: 99%;">
        <tr>
            <td>
     <ext:Store ID="StoreCmb" runat="server">
        <Model>
            <ext:Model runat="server">
                <Fields>
                    <ext:ModelField Name="AdminStoreID" />
                    <ext:ModelField Name="StoreName" />
                    <ext:ModelField Name="WhsId" /> 
                </Fields>
            </ext:Model>
        </Model>            
    </ext:Store>
    
    <ext:GridPanel ID="gpPurchaseOrder" runat="server" Title="Envíos por Recibir" Collapsed="false" 
        Collapsible="true" Layout="fit"  AutoHeight="true"  Icon="Lorry">
        
        <Store>
            <ext:Store ID="StorePurchaseOrderSAP" runat="server" DataSourceID="SqlPurchaseOrderSAP"
                SerializationMode="Simple">
                <Model>
                    <ext:Model runat="server" IDProperty="IDEnvio">
                        <Fields>                		
                            <ext:ModelField Name="IDEnvio" />
                            <ext:ModelField Name="FechaEnvio" /> 
                            <ext:ModelField Name="Usuario" />
                            <ext:ModelField Name="Tienda" />
                            <ext:ModelField Name="IDUsuario" />
                            <ext:ModelField Name="TipoEnvio" /> 
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <View>
            <ext:GridView ID="GridView1" runat="server" AutoFill="true" />
        </View>
        <ColumnModel Sortable="True">
            <Columns>  
                <ext:Column runat="server" DataIndex="IDEnvio" Header="IdEnvío" ColumnID="IDEnvio">
                </ext:Column>
                <ext:Column runat="server" Align="Center" DataIndex="FechaEnvio" Header="Fecha de Envio"  >
                </ext:Column> 
                <ext:Column runat="server" Align="Center" DataIndex="Usuario" Header="Usuario"  >
                </ext:Column>
                <ext:Column runat="server" Align="Center" DataIndex="Tienda" Header="Tienda"  >
                </ext:Column>
                <ext:Column runat="server" Align="Center" DataIndex="IDUsuario" Header="IDUsuario"  Hidden="True" >
                </ext:Column>
                <ext:Column runat="server" Align="Center" DataIndex="TipoEnvio" Header="TipoEnvio" Hidden="True" >
                </ext:Column> 
            </Columns>
        </ColumnModel>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true">
                <Listeners>
                    <Select Handler="#{StorePurchaseOrderDetailSAP}.reload();" Buffer="250" />
                </Listeners>
            </ext:RowSelectionModel>
        </SelectionModel>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolBar1" runat="server" PageSize="6" />
        </BottomBar>
    </ext:GridPanel>
    
    
    <ext:GridPanel  ID="gpPurchaseOrderDetail" runat="server"  Title="Detalles" AutoDataBind="true" Layout="fit"
                    EnableViewState="true" Icon="LorryGo" AutoExpandColumn="IDEnvio" AutoHeight="true" Collapsible="true" AnimCollapse="true">
        
        <Store>
            <ext:Store runat="server" ID="StorePurchaseOrderDetailSAP" DataSourceID="SqlPurchaseOrderDetailSAP"
                 OnReadData="StorePurchaseOrderDetail_Refresh" AutoDataBind="true">
                <Model>
                    <ext:Model runat="server" IDProperty="Partida">
                        <Fields>  
                            <ext:ModelField Name="IDEnvio" />
                            <ext:ModelField Name="Partida" />
                            <ext:ModelField Name="Articulo" />
                            <ext:ModelField Name="ArticuloSBO" />
                            <ext:ModelField Name="TiendaOrigen" />
                            <ext:ModelField Name="MEDIDA" />
                            <ext:ModelField Name="LINEA" />
                            <ext:ModelField Name="MODELO" />                          
                            <ext:ModelField Name="Cantidad" />
                            <ext:ModelField Name="CantidadPendiente" />
                            <ext:ModelField Name="Status" />
                            <ext:ModelField Name="RECIBIR" />
                            <%--<ext:RecordField Name="Entrada" />--%>
                        </Fields>
                    </ext:Model>
                </Model>
                <Parameters>
                    <ext:StoreParameter Name="IDEnvio" Value="#{gpPurchaseOrder}.getSelectionModel().hasSelection() ? #{gpPurchaseOrder}.getSelectionModel().getSelection()[0].data.IDEnvio : -1"
                        Mode="Raw">
                    </ext:StoreParameter>
                </Parameters>
                <Listeners>
                    <Exception Handler="Ext.Msg.alert('Productos - Error de lectura.', e.message || response.statusText);" />
                </Listeners>
            </ext:Store>
        </Store>
        <ColumnModel ID="ColumnModel1" runat="server">
            <Columns> 
                <ext:Column runat="server" DataIndex="IDEnvio" Hidden="True" ColumnID="IDEnvio" Header="IDEnvio"></ext:Column>
                <ext:Column runat="server" Align="Center" Hidden="True" DataIndex="Partida" Header="Partida"></ext:Column>
                <ext:Column runat="server" Align="Center" Hidden="True" DataIndex="Articulo" Header="IDArticulo"></ext:Column>
                <ext:Column runat="server" Align="Left" Hidden="True" DataIndex="ArticuloSBO" Header="ArtículoSBO"></ext:Column>
                <ext:Column runat="server" Align="Center" Hidden="True" DataIndex="TiendaOrigen" Header="TiendaOrigen"></ext:Column>
                <ext:Column runat="server" Align="Left" DataIndex="MEDIDA" Header="Medida"></ext:Column>
                <ext:Column runat="server" Align="Left" DataIndex="LINEA" Header="Linea"></ext:Column>
                <ext:Column runat="server" Align="Left" DataIndex="MODELO" Header="Modelo"></ext:Column> 
                <ext:Column runat="server" Align="Right" DataIndex="Cantidad" Header="Cant. En Transferencia"></ext:Column> 
                <ext:Column runat="server" Align="Right" DataIndex="CantidadPendiente" Header="Pendiente de Recibir">
                    
                </ext:Column>                 
                <ext:Column runat="server" Align="Center" DataIndex="Status" Header="Estatus"></ext:Column>  
               
                <ext:Column runat="server" Align="Right" DataIndex="RECIBIR" Header="Recibir" > 
                    <Editor><ext:NumberField AllowDecimals="false" AllowNegative="false" AllowBlank="false" runat="server" ID="NumberField1"></ext:NumberField></Editor>
                </ext:Column>
            </Columns>
        </ColumnModel>
        <Features>
            <ext:GridFilters runat="server" ID="GridFilters2">
                <Filters>   
                <ext:StringFilter DataIndex="MEDIDA"  />    
                <ext:StringFilter DataIndex="LINEA"  /> 
                <ext:StringFilter DataIndex="MODELO"  /> 
                <ext:ListFilter DataIndex="DescCategoria" Options="Equipos,Tarjetas SIM,Accesorios,TEMM,No Inventariable" />
                </Filters>
            </ext:GridFilters>
        </Features>
        <View>
            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
        </View>
        <BottomBar>        
            <ext:PagingToolbar ID="PagingToolBar2" runat="server" PageSize="200" />           
        </BottomBar>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
                
            </ext:RowSelectionModel>
        </SelectionModel> 
        <TopBar>
            <ext:Toolbar ID="Toolbar2" runat="server">
                <Items>
                    <ext:Button ID="Button4" runat="server" Text="Procesar Recepción" Icon="Disk"  Type="Submit" >
                        <DirectEvents>
                            <Click 
                                OnEvent="UploadClick"
                                Before="Ext.Msg.wait('Procesando Recepción...', 'Cargando...');"
                                    
                                Failure="Ext.Msg.show({ 
                                    title   : 'Error', 
                                    msg     : 'Error durante la carga...', 
                                    minWidth: 200, 
                                    modal   : true, 
                                    icon    : Ext.Msg.ERROR, 
                                    buttons : Ext.Msg.OK 
                                });">
                                <ExtraParams>
                                    <ext:Parameter Name="Grid1" Value="Ext.encode(#{gpPurchaseOrderDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" /> 
                                </ExtraParams>                            
                            </Click>
                        </DirectEvents>
                    </ext:Button>   
                </Items>
            </ext:Toolbar>
        </TopBar>  
        
    </ext:GridPanel> 
            </td>
        </tr>
        </table>
</asp:Content>
