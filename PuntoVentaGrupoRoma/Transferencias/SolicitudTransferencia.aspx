﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SolicitudTransferencia.aspx.vb" Inherits="PuntoVentaGrupoRoma.SolicitudTransferencia" MasterPageFile="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >


    <script type="text/javascript">
        var MetodoArticuloSeleccionado = {
            javaCargarInformacionArticuloSeleccionado: function (grid) {
                //var porcentajeImpuesto = Ext.getCmp("hdnPorcentajeImpuesto").getValue();
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                    data.push(item.data.Nombre)
                    //data.push(porcentajeImpuesto)
                });
                App.direct.CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(data);
            }
        }


        function ResetearForm(form) {
            form.getForm().reset();
            form.items.items[9].setValue("");
            form.items.items[10].setValue("");
            form.items.items[11].setValue("");
            form.items.items[8].setValue("");
            if (form.items.items[8].store != undefined) {
                form.items.items[8].store.removeAll();
            }
        }

        function limpiarSolicitud(formaSolicitud, comboTienda, hiddenIdLineas, hiddenCrear, hiddenTienda) {
            formaSolicitud.reset()
            for (i = 0; i < formaSolicitud.length; i++)
                {
                    form.items.items[i].setValue("");
                    if (form.items.items[i].store != undefined) {
                        form.items.items[i].store.removeAll();
                    }
                }
            }
           
            
        
        function guardarSolicitud(formaSolicitud,comboTienda, hiddenIdLineas, hiddenCrear,hiddenTienda){
            //Validar que haya lineas capturadas
            var hdnsAuxLinea = document.getElementsByName("hdnAuxIdLinea");
            var idsLinea = "";

            if (hdnsAuxLinea.length == 0)
            {
                Ext.Msg.alert("No se puede crear la solicitud si no se ha capturado una linea")
                return false;
            }

            //alert(Ext.getCmp("cmbTienda").getValue())

            if (Ext.getCmp("cmbTienda").getValue() == null) {
                Ext.Msg.alert("Debe elegir una tienda")
                return false;
            }

            for (i = 0; i < hdnsAuxLinea.length; i++)
            {
                if (i == 0)
                    idsLinea =  hdnsAuxLinea[i].value
                else 
                    idsLinea = idsLinea + "," + hdnsAuxLinea[i].value;
            }

            
            hiddenIdLineas.setValue(idsLinea);
            hiddenCrear.setValue("1");
            hiddenTienda.setValue(comboTienda.getValue())

            formaSolicitud.getForm().submit({
                failure: function (a, b) {
                    hiddenCrear.setValue("0")
                    if (b.response.status == 200) {
                        Ext.Msg.alert("Solicitud creada con exito");
                        
                        //quitar todas las lineas
                        //limpiar la interfaz de captura
                    }
                    else {
                        Ext.Msg.alert("Error al crear la solicitud Consulte con el administrador");
                    }
                }
            });


        }

        var MetodoDetalleEliminarRegistro = {
            EliminarLineaGrid: function (idregistro) {
                var ckEliminar = Ext.getCmp("txtCk"+idregistro).getValue();
                if (ckEliminar == false)
                    return;
                var contenedor = Ext.getCmp("contenedor" + idregistro);
                contenedor.destroy();
            }
        }

        var MetodoCambioUnidadMedida = {
            CambiarUnidadMedida: function (idregistro) {
                var uMedida = Ext.getCmp("cmbUnidadMedida" + idregistro).getRawValue();
                Ext.getCmp("hdnUnidadMedida" + idregistro).setValue(uMedida);
            }
        }



    </script>
    
        <ext:Store ID="StoreCmb" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="AdminStoreID" />
                        <ext:ModelField Name="StoreName" />
                        <ext:ModelField Name="WhsId" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        <ext:Store
            ID="StoreArticulos"
            runat="server"
            RemotePaging="true"
            RemoteSort="true"
            PageSize="20">
            <Proxy>
                <ext:PageProxy DirectFn="App.direct.ControlPaginacionArticulos" />
            </Proxy>
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="ck" />
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="Nombre" />
                        <ext:ModelField Name="Existencia" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>

    <ext:Panel ID="BorderLayoutMain" runat="server" Flex="1" Layout="BorderLayout" Width="1350" Height="442" Border="false" >
        <Items>
        <ext:Panel 
            ID="pnlWest" 
            runat="server" 
            Layout="Border"
            Region="West"
            AutoScroll="true"
            Width="320">
            <Items>
            <ext:FormPanel 
                ID="FormSeleccionArticulo" 
                runat="server"
                Layout="AnchorLayout"
                LabelAlign="Right"
                AutoRender ="true" 
                Width="310"
                Region="North"
                Frame="true">
                    <Items>  
                        <ext:FieldContainer 
                            Layout="HBoxLayout" 
                            runat="server" >
                            <Items>
                                <ext:Label 
                                    runat="server"
                                    Text="Articulo"
                                    Width="65" />
                                <ext:ComboBox
                                    Id="cmbBusquedaArticulo"
                                    LabelSeparator=""
                                    runat="server"  
                                    DisplayField="Nombre"
                                    ValueField="Codigo"
                                    TypeAhead="False"
                                    Width="200"
                                    PageSize="10"
                                    HideBaseTrigger="true"
                                    MinChars="4"
                                    TriggerAction="Query" >
                                        <ListConfig 
                                            LoadingText="Buscando">
                                            <ItemTpl runat="server">
                                                <Html>
                                                    <div>
                                                        <span style="font-weight:bolder">{Codigo}</span>|{Existencia}<br />
                                                        <span style="font-size:x-small;font-style:italic">{Nombre}</span>
                                                    </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store runat="server" AutoLoad="false">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../Ajax/ListadoArticulos.ashx">
                                                        <ActionMethods Read="POST" />

                                                        <Reader>
                                                            <ext:JsonReader Root="Articulos" TotalProperty="total" />
                                                        </Reader>                                                        
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Model>
                                                    <ext:Model runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Codigo" />
                                                            <ext:ModelField Name="Nombre" />
                                                            <ext:ModelField Name="Existencia"  />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    <DirectEvents>
                                        <Select OnEvent="CargarInformacionArticuloSeleccionadoDeBuscador" />
                                    </DirectEvents>
                                    </ext:ComboBox>
                                <ext:Button runat="server" Icon="Find">
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaBusquedaArticulos" />
                                    </DirectEvents>
                                </ext:Button>
                                </Items>
                            </ext:FieldContainer>
                            <ext:Hidden
                                ID="hdnCodigoArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnFactorArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnNombreArticulo"
                                runat="server"/>
                        <ext:Hidden
                            ID="hdnSujImp"
                            runat="server" />
                    <ext:ComboBox
                        ID="cmbUnidadMedida"
                        runat="server"
                        FieldLabel="Uni. Med."
                        LabelWidth="60"
                        AutoRender="true"
                        Width="288"
                        LabelSeparator="">
                        <%--<DirectEvents>
                            <Select OnEvent="ControlarCambioUnidadMedida" />
                        </DirectEvents>--%>
                        </ext:ComboBox>
                    <ext:Hidden 
                        ID="hdnItemsComboUnidadMedida"
                        runat="server"
                        Text="0" />
                    <ext:NumberField
                        ID="txtCantidad"
                        runat="server"
                        FieldLabel="Cantidad" 
                        LabelWidth="60"
                        Width="288"
                        LabelSeparator="" 
                        HideTrigger="true"
                        DecimalPrecision="2"
                        MinValue="0">
                        <%--<DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>--%>
                    </ext:NumberField>
                    
                    <ext:Button 
                        ID="BtnAgregarLinea" 
                        runat="server"
                        Text="+ Linea"
                        Icon="Add"> 
                        <DirectEvents>
                            <Click OnEvent="AgregarLineaVenta" />
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:FormPanel>
                <ext:FormPanel 
                ID="FormSeleccionTienda" 
                runat="server"
                Layout="AnchorLayout"
                LabelAlign="Right"
                AutoRender ="true" 
                Width="310"
                Region="Center"
                Frame="true">
                    <Items>  
                        <ext:FieldContainer 
                            Layout="HBoxLayout" 
                            runat="server" >
                            <Items>
                                <ext:ComboBox 
                                ID="cmbTienda" 
                                runat="server"
                                StoreID="StoreCmb" 
                                Width="150" 
                                Editable="false"
                                DisplayField="StoreName"
                                ValueField="AdminStoreID" 
                                TypeAhead="true" 
                                Mode="Default"
                                ForceSelection="true"
                                TriggerAction="All" 
                                Title="Tiendas"                                 
                                FieldLabel="Tienda"
                                EmptyText="Tiendas..."
                                ItemSelector="div.list-item"
                                SelectOnFocus="true"
                                ClientIDMode="Static">
                                <ListConfig>
                                <ItemTpl ID="Template1" runat="server">
                                    <Html>
					                    
						                    <div class="list-item">
							                     <h3>{StoreName}</h3>
							                     {WhsId} 
						                    </div>
					                    
				                    </Html>
                                </ItemTpl>
                                    </ListConfig>  
                                   <ToolTips>
                                    <ext:ToolTip ID="ToolTip1" runat="server" Html="Solo se hace una vez" Title="Seleccione la tienda al terminar de agregar todos los articulos" Target="#{ComboBox1}"></ext:ToolTip>
                                    </ToolTips>                               
                                </ext:ComboBox>
                                <%--<ext:Button runat="server" Icon="Find">
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaBusquedaArticulos" />
                                    </DirectEvents>
                                </ext:Button>--%>
                                </Items>
                            </ext:FieldContainer>
                </Items>
            </ext:FormPanel>
        </Items>            
        </ext:Panel>
        
        <ext:Panel ID="pnlCenter" 
            runat="server" 
            AutoScroll="true"
            Layout="BorderLayout"
            Region="Center"
            Width="900">
            <Items>
         <ext:FormPanel
            runat="server" 
            Frame="true"
            Layout="TableLayout"
            PaddingSummary="5px 5px 0"
            Width="1100"
             Id="FrmSolicitud"
             ClientIDMode="Static"
            TrackResetOnLoad="true"
             >
             <LayoutConfig>
                 <ext:TableLayoutConfig Columns="3" />
             </LayoutConfig>
            <Items>
                <ext:Toolbar 
                    ColSpan="3"
                    runat="server">
                    <Items>
                        <ext:Button runat="server" Text="Guardar">
                            <Listeners>
                                <Click Handler="guardarSolicitud(#{FrmSolicitud},#{cmbTienda},#{hdnIdLinea}, #{hdnCrear},#{hdnStore})" />
                            </Listeners>
                        </ext:Button>

                        <ext:Button runat="server" Text="Limpiar">
                            <Listeners>
                                <Click Handler="limpiarSolicitud(#{FrmSolicitud},#{cmbTienda},#{hdnIdLinea}, #{hdnCrear},#{hdnStore})" />
                            </Listeners>
                        </ext:Button>
                    </Items>
                </ext:Toolbar>
                <ext:Panel         
                    runat="server"    
                    Width="1000" 
                    Layout="AnchorLayout"
                    Id="PanelEncabezado"
                    ColSpan="3">
                <Items>
                    <ext:Container runat="server" Width="750" Layout="HBoxLayout">
                        <Items>
                            <ext:Image runat="server" ID="HeaderGridCk" Width="30" Height="30" MarginSpec="-5 0 0 0"   ImageUrl="../content/images/appbar.delete.png" />
                            <ext:Label runat="server" ID="HeaderGridCodigo" Text="Codigo" Width="100" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridNombre" Text="Articulo" Width="400" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridUnidad" Text="Unidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridCantidad" Text="Cantidad" Width="60" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        </Items>

                    </ext:Container>
                </Items>
            </ext:Panel>
            <ext:Panel         
                    runat="server"    
                    Width="1000" 
                    Height="210"
                    Layout="AnchorLayout"
                    Id="PanelDetalleDocumento"
                    AutoScroll="true"
                    ColSpan="3" >
            </ext:Panel>
                <ext:Hidden ID="hdnCrear" runat="server" Name="hdnCrear"  />
                <ext:Hidden ID="hdnStore" runat="server" Name="hdnStore" />
                <ext:Hidden ID="hdnIdLinea" runat="server" Name="hdnIdLinea" />
            </Items>
        </ext:FormPanel>
            </Items>
        </ext:Panel>
        </Items>
    </ext:Panel>        

    <ext:Window 
        title="Buscar Articulos" 
            runat="server" 
            ID="winArticulos" 
            Modal="true" 
            Hidden="True" 
            Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content> 
                    <ext:FormPanel 
                        runat="server"
                        ID="FormBusquedaArticulo"
                        Layout="AnchorLayout"
                        LabelAlign="right"
                        AutoRender="true"
                        Region="North"
                        Width="1000"
                        ButtonAlign="Left"
                        PaddingSpec="10 0 10 10"
                        Frame="true">
                        <Items>
                            <ext:Hidden 
                                runat="server"
                                Id="hdnNuevaBusqueda"
                                Text="NO" />
                            <ext:Hidden
                                runat="server"
                                Id="hdnTotalRegistrosNuevaBusqueda" />
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoCodigoBuscado" 
                                Text="|[NONE]|"/>
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoNombreBuscado" />
                            <ext:FieldContainer runat="server">
                                <Items>
                                    <ext:TextField
                                        runat="server"
                                        ID="txtCodigoArticuloBusqueda"
                                        FieldLabel="Codigo"
                                        MarginSpec="0 0 0 8"
                                        LabelWidth="60"    
                                        AnchorHorizontal="200"
                                        DataIndex="CodigoArticulo"/>
                                    </Items>
                            </ext:FieldContainer>
                            <ext:FieldContainer runat="server">
                                <Items>
                            <ext:TextField
                                runat="server"
                                ID="txtNombreArticuloBusqueda"
                                FieldLabel="Nombre"
                                AnchorHorizontal="200"
                                MarginSpec="0 0 0 8"
                                LabelWidth="60"    
                                DataIndex="NombreArticulo" />
                                    </Items>
                                </ext:FieldContainer>
                        </Items>
                        <Buttons>
                            <ext:Button
                                runat="server"
                                ID="btnBuscarArticulo"
                                Text="Buscar"
                                Icon="Find">
                                <DirectEvents>
                                    <Click OnEvent="BuscarArticulos" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:GridPanel
                        runat="server"
                        Id="GridArticulo"
                        Width="1000"
                        Height="380"
                        Layout="BorderLayout"
                        Region="Center"
                        Border="false"
                        StoreID="StoreArticulos">
                        <ColumnModel>
                            <Columns>
                                <ext:Column runat="server" Header="Codigo" DataIndex="Codigo" Width="100" />
                                <ext:Column runat="server" Header="Nombre" DataIndex="Nombre" Width="350" />
                                <ext:Column runat="server" Header="Existencia" DataIndex="Existencia" Width="100" />
                            </Columns>
                        </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView2" runat="server" ForceFit="true">
                                </ext:GridView>
                            </View>
                        <Listeners>
                            <ItemDblClick Handler="MetodoArticuloSeleccionado.javaCargarInformacionArticuloSeleccionado(#{GridArticulo});" />
                        </Listeners>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" />                    
                            </SelectionModel>                
                        <BottomBar>
                            <ext:PagingToolbar
                                runat="server"
                                DisplayInfo="true"
                                DisplayMsg="Articulos {0} - {1} de {2}"
                                EmptyMsg="Sin Articulos " />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
        </Content>
        </ext:Window>
        
</asp:Content>