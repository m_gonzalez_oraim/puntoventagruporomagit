﻿Imports System.Text.RegularExpressions
Imports System.IO

Module GeneralClass

    Public Enum Tipos
        VE 'Venta
        DE 'devolucion venta
        ED 'entrada directa
        EC 'entrada por compra
        RE 'recepcion envio
        SA 'salida directa
        AB 'abono
        CO 'compra
        EV 'envio
        CE 'cancelacion envio
        FA 'factura

    End Enum

    Public Function InsertaLog(ByVal TipoOperacion As Tipos, ByVal IdOperacion As Integer, ByVal IdTienda As Integer, ByVal Almacen As String) As String

        Dim connstringWEB As String
        Dim oDB As New DBMaster
        Dim sError As String = ""

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

        Dim sQuery As String
        Dim dt As New DataTable

        sQuery = "Insert into LOGS (TipoOperacion,IdOperacion,IdTienda,Almacen,FechaOperacion) values (" & _
        "'" & TipoOperacion.ToString & "'," & _
        IdOperacion & "," & _
        IdTienda & "," & _
        "'" & Almacen & "'," & _
        "getdate())"

        If oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB, sError) = 1 Then
            Return "1"
        Else
            Return sError
        End If

    End Function

    Public Function GeneraArchivoFactura(ByVal IDventa As String, ByVal directorio As String, ByVal BaseSAP As String, ByVal ftp As String, ByVal user As String, ByVal pass As String, Optional ByVal tablaClientes As String = "Clientes") As Boolean
        Return True
    End Function

    Public Function GeneraArchivoNotaCredito(ByVal IDDevolucion As String, ByVal directorio As String, ByVal baseSAP As String, ByVal ftp As String, ByVal user As String, ByVal pass As String) As Boolean
        Return True
    End Function

    Public Function generaLineaArchivoFE(ByVal NoLinea As String, ByVal Contenido As DataTable) As String

        Dim Cadena As String = ""

        Try

            For Each dr As DataRow In Contenido.Rows

                Cadena = Cadena & NoLinea & "|"

                For i As Integer = 0 To Contenido.Columns.Count - 1

                    If i = Contenido.Columns.Count - 1 Then

                        If dr(i).GetType().Name = "DateTime" Then
                            Dim tmpFecha As String
                            tmpFecha = Format(dr(i), "aaaa-mm-ddThh:mm:ss")
                        Else
                            If NoLinea <> "06" Then
                                If Contenido.Columns.Item(i).Caption.Contains("RFC") Then
                                    If dr(i).ToString.Contains(" ") Or dr(i).ToString.Length < 11 Or dr(i).ToString.Length > 13 Or Regex.IsMatch(dr(i).ToString, "^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") Then
                                        Cadena = Cadena & dr(i)
                                    Else
                                        Cadena = Cadena & dr(i)
                                    End If
                                Else
                                    Cadena = Cadena & dr(i)
                                End If
                            Else
                                Cadena = Cadena & dr(i) & "|"
                            End If
                        End If

                    Else
                        If dr(i).GetType().Name = "DateTime" Then
                            Dim tmpFecha As String
                            tmpFecha = Format(dr(i), "yyyy-MM-ddThh:mm:ss")
                            Cadena = Cadena & tmpFecha & "|"
                        Else
                            If Contenido.Columns.Item(i).Caption.Contains("RFC") Then
                                If dr(i).ToString.Contains(" ") Or dr(i).ToString.Length < 11 Or dr(i).ToString.Length > 13 Or Regex.IsMatch(dr(i).ToString, "^([A-Z\s]{4})\d{6}([A-Z\w]{3})$") Then
                                    Cadena = Cadena & "XAXX010101000" & "|"
                                Else
                                    Cadena = Cadena & dr(i) & "|"
                                End If
                            Else
                                Cadena = Cadena & dr(i) & "|"
                            End If
                        End If
                    End If

                Next

                Cadena = Cadena & vbNewLine

            Next

            generaLineaArchivoFE = Cadena

        Catch ex As Exception

            generaLineaArchivoFE = ""

        End Try

    End Function

    Friend Function EscribeArchivo(ByVal texto As String, ByVal nombreArchivo As String)

        Dim Sw1 As StreamWriter
        Dim Sr1 As StreamReader
        Dim sPath As String

        Try

            sPath = nombreArchivo

            If File.Exists(sPath) Then
                File.Delete(sPath)
            End If

            'Sw1 = New StreamWriter(sPath, False, System.Text.Encoding.Unicode)
            Sw1 = New StreamWriter(sPath, False, System.Text.Encoding.UTF8)
            Sw1.WriteLine(texto)
            Sw1.Flush()
            Sw1.Close()

            Sr1 = New StreamReader(File.Open(nombreArchivo, FileMode.OpenOrCreate, _
                FileAccess.ReadWrite))

            Sr1.Close()
            Sr1.Dispose()
            Sr1 = Nothing

            GC.Collect()

        Catch ex As Exception

            Dim serr As String = "Bexap Service, FillLog: " & ex.Message

        End Try

    End Function


    Public Class NumLetra

        Dim UNIDADES As String() = {"", "un ", "dos ", "tres ", "cuatro ", "cinco ", "seis ", "siete ", "ocho ", "nueve "}
        Dim DECENAS As String() = {"diez ", "once ", "doce ", "trece ", "catorce ", "quince ", "dieciseis ", "diecisiete ", "dieciocho ", "diecinueve", "veinte ", "treinta ", "cuarenta ", "cincuenta ", "sesenta ", "setenta ", "ochenta ", "noventa "}
        Dim CENTENAS As String() = {"", "ciento ", "doscientos ", "trecientos ", "cuatrocientos ", "quinientos ", "seiscientos ", "setecientos ", "ochocientos ", "novecientos "}

        Dim r As Regex


        Sub NumLetra()
        End Sub

        Public Function Convertir(ByVal numero As String, ByVal mayusculas As Boolean) As String
            Dim literal As String = ""
            Dim parte_decimal As String = ""
            'si el numero utiliza (.) en lugar de (,) -> se reemplaza
            numero = Replace(numero, ".", ",")
            'si el numero no tiene parte decimal, se le agrega ,00        
            If numero.IndexOf(",") = -1 Then
                numero = numero & ",00"
            End If
            'se valida formato de entrada -> 0,00 y 999 999 999,00
            'if (Pattern.matches("\\d{1,9},\\d{1,2}", numero)) {

            r = New Regex("\d{1,9},\d{1,2}")
            Dim mc As MatchCollection = r.Matches(numero)
            If mc.Count > 0 Then
                'se divide el numero 0000000,00 -> entero y decimal
                Dim Num As String() = numero.Split(",")
                'de da formato al numero decimal
                parte_decimal = Num(1) & "/100 M.N."
                'se convierte el numero a literal            
                If Num(0) = 0 Then
                    literal = "cero "
                ElseIf Num(0) > 999999 Then
                    literal = getMillones(Num(0))
                ElseIf Num(0) > 999 Then
                    literal = getMiles(Num(0))
                ElseIf Num(0) > 99 Then
                    literal = getCentenas(Num(0))
                ElseIf Num(0) > 9 Then
                    literal = getDecenas(Num(0))
                Else
                    literal = getUnidades(Num(0))
                End If
                'devuelve el resultado en mayusculas o minusculas
                If mayusculas Then
                    Return (literal & parte_decimal).ToUpper
                Else
                    Return literal & parte_decimal
                End If
            Else
                Return ""
            End If

        End Function

        ' funciones para convertir los numeros a literales

        Private Function getUnidades(ByVal numero As String) As String '1 - 9
            'si tuviera algun 0 antes se lo quita -> 09 = 9 o 009=9
            Dim num As String = numero.Substring(numero.Length - 1)
            Return UNIDADES(num)
        End Function

        Private Function getDecenas(ByVal numero As String) As String '99
            If numero < 10 Then 'para casos como -> 01 - 09
                Return getUnidades(numero)
            ElseIf numero > 19 Then 'para 20...99
                Dim u As String = getUnidades(numero)
                If u.Equals("") Then 'para 20,30,40,50,60,70,80,90
                    Return DECENAS(numero.Substring(0, 1) + 8)
                Else
                    Return DECENAS(numero.Substring(0, 1) + 8) & "y " & u
                End If
            Else
                Return DECENAS(numero - 10)
            End If
        End Function

        Private Function getCentenas(ByVal numero As String) As String
            If numero > 99 Then 'es centena
                If numero = 100 Then 'caso especial
                    Return "cien "
                Else
                    Return CENTENAS(numero.Substring(0, 1)) & getDecenas(numero.Substring(1))
                End If
            Else 'se quita el 0 antes de convertir a decenas
                Return getDecenas(numero)
            End If
        End Function


        Private Function getMiles(ByVal numero As String) As String
            'obtiene las centenas'
            Dim c As String = CInt(numero.Substring(numero.Length - 3))
            'obtiene los miles
            Dim m As String = CInt(numero.Substring(0, numero.Length - 3))
            Dim n As String = ""
            'se comprueba que miles tenga valor entero
            If m > 0 Then
                n = getCentenas(m)
                Return n & " mil " & getCentenas(c)
            Else
                Return "" & getCentenas(c)
            End If
        End Function

        Private Function getMillones(ByVal numero As String) As String
            'se obtiene los miles
            Dim miles As String = numero.Substring(numero.Length - 6)
            'millones
            Dim millon As String = numero.Substring(0, numero.Length - 6)
            Dim n As String = ""
            If millon > 9 Then
                n = getCentenas(millon) & " millones "
            Else
                n = getUnidades(millon) & " millon "
            End If
            Return n & getMiles(miles)
        End Function
    End Class

End Module