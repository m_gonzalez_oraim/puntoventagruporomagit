﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports System.Data
Imports System.Web.Configuration

Public Class clsAdminLogin 
    Private p_UserInfo As New Hashtable
    Private p_HasAccess As Boolean = False
    Private p_IsActive As Boolean = True
    Private p_ErrorMessage As String = ""
    Sub New(ByRef DBConn As clsDBMgr, ByVal NTUserAccount As String, ByVal NTUserDomain As String)

        ' Search for the User/Domain Pair
        Dim UserQuery As String
        UserQuery = "SELECT TOP 1 * " & _
                    "FROM AdminUser " & _
                    " WHERE NTUserAccount = '" & NTUserAccount.ToLower() & "' " & _
                    " AND NTUserDomain = '" & NTUserDomain.ToLower() & "' "

        ' Get Data Reader
        Dim UserReader As SqlDataReader = Nothing
        DBConn.GetQuerydtr(UserQuery, UserReader)

        ' Check if a Record was Found
        If UserReader.Read() Then
            ' Check if User is Active
            If UserReader.Item("Status") = "A" Then
                p_UserInfo.Add("AdminUserID", UserReader.Item("AdminUserID"))
                p_UserInfo.Add("UserName", UserReader.Item("FirstName") & " " & UserReader.Item("LastName"))
                p_UserInfo.Add("AdminRoleID", UserReader.Item("AdminRoleID"))
                p_UserInfo.Add("NTUserAccount", UserReader.Item("NTUserAccount"))
                p_HasAccess = True
            Else
                p_ErrorMessage = "Inactive Account"
                p_IsActive = False
            End If
        Else
            p_ErrorMessage = "Invalid User."
        End If

        ' Close Reader
        UserReader.Close()

    End Sub
    Public ReadOnly Property UserInfo() As Hashtable
        Get
            Return p_UserInfo
        End Get
    End Property
    Public ReadOnly Property HasAccess() As Boolean
        Get
            Return p_HasAccess
        End Get
    End Property
    Public ReadOnly Property IsActive() As Boolean
        Get
            Return p_IsActive
        End Get
    End Property
    Public ReadOnly Property ErrorMessage() As String
        Get
            Return p_ErrorMessage
        End Get
    End Property
End Class
