﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Abonos.aspx.vb" Inherits="PuntoVentaGrupoRoma.Abonos" MasterPageFile="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <br />
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
    <ext:Hidden ID="UseConfirmation" runat="server" Text="false" />
    
    <ext:XScript ID="XScript1" runat="server">
    
       <script type="text/javascript" language="javascript">
           var customerSearch = function (el, event) {
               var enter = false;
               if (Ext.isIE === true) {
                   if (event.keyCode == 13) {
                       enter = true;
                   }
               }
               else {
                   if (event.button == 12) {
                       enter = true;
                   }
               }

               if (enter === true) {
                   Ext.net.Notification.show({
                       iconCls: 'icon-exclamation',
                       html: 'Enter',
                       title: 'EXCEPTION',
                       autoScroll: true,
                       hideDelay: 5000,
                       width: 300,
                       height: 200
                   })
               }
           };
       </script>
    
        <script type="text/javascript">
            Ext.data.Connection.override({ timeout: 1200000 });
            Ext.Ajax.timeout = 12000000;
        </script>
    
              
               
    </ext:XScript>

    
   
     
     
     <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="true"
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None" AutoLoad="false">
            <Proxy>
                <ext:AjaxProxy Url="ClientesAbonos.ashx" >
                        <ActionMethods READ="POST" />
                </ext:AjaxProxy>   
            </Proxy> 
            <Model>
                <ext:Model runat="server" Root="clientes" TotalProperty="Total" >
                    <Fields>
                        <ext:ModelField Name="cCliente" />
                        <ext:ModelField Name="RFC"/> 
                        <ext:ModelField Name="cIdCliente"/>                        
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
    
     
    <ext:GridPanel 
            ID="gpDevoluciones" 
            runat="server"
            Icon="Table" 
            Title="Abonos"  
            Height="500"   
            AutoExpandColumn = "Vendedor" 
            ClicksToEdit="1"    
            AutoDataBind="true"
            EnableViewState="true"        
            >   
            <Store>
                 <ext:Store 
            ID="StoreDevoluciones" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None"
            OnBeforeStoreChanged="HandleChanges">
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="IDVenta"/>
                        <ext:ModelField Name="Fecha"/>
                        <ext:ModelField Name="Vendedor"/>
                        <ext:ModelField Name="Pagado"/>
                        <ext:ModelField Name="PorPagar"/>
                        <ext:ModelField Name="Total"/>
                        <ext:ModelField Name="FormaDePago"/>
                        <ext:ModelField Name="TerminacionTarjeta"/>
                        <ext:ModelField Name="Monto"/>
                        <ext:ModelField Name="Observaciones"/>
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
            
            </Store>                    
            <ColumnModel>
                <Columns>        
                    <ext:Column runat="server" Header="Id" DataIndex="Id" Hidden="true"  />
                    <ext:Column runat="server" Header="Venta" DataIndex="IDVenta"/>                        
                    <ext:Column runat="server" Header="Fecha Venta" DataIndex="Fecha"/>                        
                    <ext:Column runat="server" Header="Vendedor" DataIndex="Vendedor"/>                        
                    <ext:Column runat="server" Header="Pagado" DataIndex="Pagado">                        
                        <Renderer Format="UsMoney" />
                        </ext:Column>
                    <ext:Column runat="server" Header="Por Pagar" DataIndex="PorPagar">                        
                        <Renderer Format="UsMoney" />
                        </ext:Column>
                    <ext:Column runat="server" Header="Total" DataIndex="Total">                        
                        <Renderer Format="UsMoney" />
                        </ext:Column>
                    <ext:Column runat="server" Header="Forma de Pago" DataIndex="FormaDePago">                        
                        <Editor>
                            <ext:ComboBox ID="cbFP" runat="server">
                                <Items>
                                    <ext:ListItem Text="Efectivo" Value="Efectivo" />
                                    <ext:ListItem Text="Tarjeta de Crédito" Value="Tarjeta de Crédito" />
                                    <ext:ListItem Text="Tarjeta de Débito" Value="Tarjeta de Débito" />
                                    <ext:ListItem Text="American Express" Value="American Express" />
                                    <ext:ListItem Text="Deposito / Cheque" Value="Deposito / Cheque" />
                                    <ext:ListItem Text="Nota de crédito" Value="Nota de crédito" />
                                    <ext:ListItem Text="Otros" Value="Otros" />
                                </Items> 
                            </ext:ComboBox>
                        </Editor> 
                    </ext:Column>
                                        
                    <ext:Column runat="server" Header="Terminacion Tarjeta" DataIndex="TerminacionTarjeta">                        
                        <Editor>
                            <ext:TextField ID="TerminacionTarjeta" runat="server">
                            </ext:TextField>
                        </Editor> 
                    </ext:Column>
                    
                    
                    <ext:Column runat="server" Header="Monto" DataIndex="Monto">
                        <Editor>
                            <ext:NumberField ID="Monto" runat="server" EnableKeyEvents="true">                                
                                <Listeners>
                                    <%--<KeyDown Fn="function(el,e) {Ext.net.Notification.show({iconCls    : 'icon-exclamation',html:String(e.getKey()),title:'EXCEPTION', autoScroll : true, hideDelay  : 5000, width      : 300, height     : 200});}"/>--%>                                    
                                    <SpecialKey Fn="function(el,e) {if (String(e.getKey())=='9') {e.stopEvent();return false;};if (String(e.getKey())=='13') {e.stopEvent();return false;};}" />
                                </Listeners>
                            </ext:NumberField>
                        </Editor> 
                    </ext:Column>

                                        
                </Columns>
            </ColumnModel>
            
                             <Features >
                                <ext:GridFilters runat="server" ID="GridFilters1">
                                    <Filters>  
                                    <ext:StringFilter DataIndex="Articulo"  /> 
                                   <%-- <ext:StringFilter DataIndex="IMEI"  /> 
                                    <ext:StringFilter DataIndex="SIM"  /> 
                                    <ext:StringFilter DataIndex="DN"  />  --%>
                                    </Filters>
                                </ext:GridFilters>
                            </Features>
            <%-- 
            <Plugins>
                <ext:RowEditor ID="RowEditor1" runat="server" SaveText="Guardar" CancelText="Cancelar"  />                                               
            </Plugins> 
            --%>            
                       
            <View>
                <ext:GridView ID="gvDevoluciones" runat="server" ForceFit="true" />
            </View>
            
            <TopBar>
                <ext:Toolbar ID="Toolbar1" runat="server" Layout="Container" >
                    <Items>
                       <%-- <ext:TableLayout runat="server" Columns="1">                        
                            <Cells>
                            <ext:Cell>--%>
                            
                            <ext:Toolbar ID="Toolbar3" runat="server" Flat="true">
                                <Items>
                                                                              
                                <ext:Label ID="Label2" runat="server" Text="Cliente: "></ext:Label>  
                                
                                <ext:ComboBox
                                runat="server" 
                                ID="cmbCliente"
                                StoreID="StoreClientes"
                                DisplayField="cCliente" 
                                ValueField="cIdCliente"                                                        
                                TypeAhead="false"
                                LoadingText="Searching..."                 
                                PageSize="10"
                                HideTrigger="true"
                                ItemSelector="div.search-item"        
                                AnchorHorizontal="100%"
                                MinChars="1" 
                                EmptyText="Seleccione Cliente"
                                AllowBlank="false"                                                       
                                Width="250"
                                >
                                <%-- AutoPostBack="true"
                                AutoPostBackEvent="Select"  
                                TriggerAction="All" --%>
                                    <ListConfig>
                                    <ItemTpl ID="Template1" runat="server">
                                       <Html>
					                      
						                      <div class="search-item">
							                     <h3><span>{cCliente}</span></h3>
							                     {RFC} 
						                      </div>
					                      
				                       </Html>
                                    </ItemTpl>
                                    </ListConfig>
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetPeriods(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox>
                                
                                <ext:Label ID="lblPeriodo" runat="server" Text="Periodo:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbFecha"
                                EmptyText="Seleccione Fecha"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <%--<Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>--%>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetDays(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblDia" runat="server" Text="Dia:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbDia"
                                EmptyText="Seleccione un dia"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblFolio" runat="server" Text="Folio:" Visible="True"></ext:Label>                       
                                
                                <ext:ComboBox
                                runat="server" 
                                ID="cmbFolio"
                                EmptyText="Seleccione Folio" Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.DoBind(el.value,false);}"/>
                                    </Listeners>
                                </ext:ComboBox>                                           
                                
                                <ext:Button runat="server" ID="btnClean" Text="Nuevo Filto" Flat="false" Icon="Erase">                                
                                <Listeners>
                                        <Click Fn="function(el) {Ext.net.DirectMethods.Clean();}"/>
                                    </Listeners>
                                </ext:Button>    
                            <%--    </ext:Cell>
                                
                            </cells>                        
                        </ext:tablelayout> --%>
                        </Items>
                        </ext:Toolbar>
                        
                        <ext:Toolbar ID="Toolbar2" runat="server" Flat="true">                        
                            <Items>
                                <ext:TextField runat="server" FieldLabel="Pedido" ID="Pedido">
                                </ext:TextField>
                                <ext:Button runat="server" Text="Buscar" Icon="Find" ID="Buscar"></ext:Button>
                            </Items>
                        </ext:Toolbar> 
                        
                        
                    </Items>
                </ext:Toolbar>
            </TopBar>
            
            <BottomBar>
                <ext:Toolbar runat="server" ID="tbBottom" >
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Tipo:" Visible="False"></ext:Label>
                        <ext:ToolbarSpacer Width="10"></ext:ToolbarSpacer> 
                        <ext:ComboBox runat="server" ID="cmbTipo" SelectedIndex="1" Disabled="true" Hidden="true">
                            <Items>
                                <ext:ListItem Text="Cancelacion (Mismo dia)" Value="Cancelacion" /> 
                                <ext:ListItem Text="Nota de credito (Devolucion)" Value="Devolucion" /> 
                            </Items> 
                        </ext:ComboBox> 
                        <ext:ToolbarFill></ext:ToolbarFill> 
                        <ext:Button ID="btnDevolucion" runat="server" Text="Realizar Abonos" Icon="Accept">

                            <DirectEvents>
                                <Click OnEvent="realizaAbono">
                                    <EventMask ShowMask="true" Msg="Realizando Abono" MinDelay="1000" />
                                </Click>
                            </DirectEvents>

                        </ext:Button>    
                    </Items> 
                </ext:Toolbar> 
            </BottomBar> 
            
           <SelectionModel>
                <ext:CheckboxSelectionModel ID="chkDevolber" runat="server" HideCheckAll="true" CheckOnly="true" >
                </ext:CheckboxSelectionModel>
            </SelectionModel>
        </ext:GridPanel>
</asp:Content>