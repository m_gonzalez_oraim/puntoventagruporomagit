﻿Imports System.Collections.Generic
Imports Ext.Net
Imports System.Data
Imports System.Xml
Imports System.IO


Partial Class EsquemaVenta_Facturacion
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private sDefaultList As String
    Private sDefaultName As String
    Private mensaje As String = ""
    Private winMsg As Ext.Net.Window
    Private oWSData As WSData

    Public Class Cliente

        Public Sub New(ByVal p_Id As String, ByVal p_Nombre As String, ByVal p_RFC As String, ByVal p_CalleNumero As String, ByVal p_Colonia As String, ByVal p_CP As String, ByVal p_TelCasa As String, ByVal p_TelOfc As String, ByVal p_Correo As String, ByVal p_Estado As String, ByVal p_DelMun As String, ByVal p_IDRef As String, ByVal p_TipoCliente As String, ByVal p_actFCHNACIMIENTO As String, ByVal p_actNOMBRE As String, ByVal p_actAPEPATERNO As String, ByVal p_actAPEMATERNO As String, ByVal p_actCALLE As String, ByVal p_actNUMEXT As String, ByVal p_actNUMINT As String, ByVal p_actCIUDAD As String, ByVal p_actCod_REGIONestado As String, ByVal p_actCod_PROVINCIAmunicipio As String, ByVal p_actCod_CIUDAD As String, ByVal p_actCod_COMUNAcolonia As String)

            Id = p_Id
            Nombre = p_Nombre
            RFC = p_RFC
            CalleNumero = p_CalleNumero
            Colonia = p_Colonia
            CP = p_CP
            TelCasa = p_TelCasa
            TelOfc = p_TelOfc
            Correo = p_Correo
            Estado = p_Estado
            DelMun = p_DelMun
            IDRef = p_IDRef
            TipoCliente = p_TipoCliente
            actFCHNACIMIENTO = p_actFCHNACIMIENTO
            actNOMBRE = p_actNOMBRE
            actAPEPATERNO = p_actAPEPATERNO
            actAPEMATERNO = p_actAPEMATERNO
            actCALLE = p_actCALLE
            actNUMEXT = p_actNUMEXT
            actNUMINT = p_actNUMINT
            actCIUDAD = p_actCIUDAD
            actCod_REGIONestado = p_actCod_REGIONestado
            actCod_PROVINCIAmunicipio = p_actCod_PROVINCIAmunicipio
            actCod_CIUDAD = p_actCod_CIUDAD
            actCod_COMUNAcolonia = p_actCod_COMUNAcolonia


        End Sub


        Public Property TipoCliente() As String
            Get
                Return m_TipoCliente
            End Get
            Set(ByVal value As String)
                m_TipoCliente = value
            End Set
        End Property
        Private m_TipoCliente As String

        Public Property actCod_REGIONestado() As String
            Get
                Return m_actCod_REGIONestado
            End Get
            Set(ByVal value As String)
                m_actCod_REGIONestado = value
            End Set
        End Property
        Private m_actCod_REGIONestado As String


        Public Property actCod_PROVINCIAmunicipio() As String
            Get
                Return m_actCod_PROVINCIAmunicipio
            End Get
            Set(ByVal value As String)
                m_actCod_PROVINCIAmunicipio = value
            End Set
        End Property
        Private m_actCod_PROVINCIAmunicipio As String

        Public Property actCod_COMUNAcolonia() As String
            Get
                Return m_actCod_COMUNAcolonia
            End Get
            Set(ByVal value As String)
                m_actCod_COMUNAcolonia = value
            End Set
        End Property
        Private m_actCod_COMUNAcolonia As String

        Public Property actCod_CIUDAD() As String
            Get
                Return m_actCod_CIUDAD
            End Get
            Set(ByVal value As String)
                m_actCod_CIUDAD = value
            End Set
        End Property
        Private m_actCod_CIUDAD As String


        Public Property Id() As Integer
            Get
                Return m_Id
            End Get
            Set(ByVal value As Integer)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer

        Public Property actFCHNACIMIENTO() As String
            Get
                Return m_actFCHNACIMIENTO
            End Get
            Set(ByVal value As String)
                m_actFCHNACIMIENTO = value
            End Set
        End Property
        Private m_actFCHNACIMIENTO As String

        Public Property actNOMBRE() As String
            Get
                Return m_actNOMBRE
            End Get
            Set(ByVal value As String)
                m_actNOMBRE = value
            End Set
        End Property
        Private m_actNOMBRE As String

        Public Property actAPEPATERNO() As String
            Get
                Return m_actAPEPATERNO
            End Get
            Set(ByVal value As String)
                m_actAPEPATERNO = value
            End Set
        End Property
        Private m_actAPEPATERNO As String

        Public Property actAPEMATERNO() As String
            Get
                Return m_actAPEMATERNO
            End Get
            Set(ByVal value As String)
                m_actAPEMATERNO = value
            End Set
        End Property
        Private m_actAPEMATERNO As String

        Public Property actCALLE() As String
            Get
                Return m_actCALLE
            End Get
            Set(ByVal value As String)
                m_actCALLE = value
            End Set
        End Property
        Private m_actCALLE As String

        Public Property actNUMEXT() As String
            Get
                Return m_actNUMEXT
            End Get
            Set(ByVal value As String)
                m_actNUMEXT = value
            End Set
        End Property
        Private m_actNUMEXT As String

        Public Property actNUMINT() As String
            Get
                Return m_actNUMINT
            End Get
            Set(ByVal value As String)
                m_actNUMINT = value
            End Set
        End Property
        Private m_actNUMINT As String

        Public Property actCIUDAD() As String
            Get
                Return m_actCIUDAD
            End Get
            Set(ByVal value As String)
                m_actCIUDAD = value
            End Set
        End Property
        Private m_actCIUDAD As String


        Public Property Nombre() As String
            Get
                Return m_Nombre
            End Get
            Set(ByVal value As String)
                m_Nombre = value
            End Set
        End Property
        Private m_Nombre As String

        Public Property RFC() As String
            Get
                Return m_RFC
            End Get
            Set(ByVal value As String)
                m_RFC = value
            End Set
        End Property
        Private m_RFC As String

        Public Property CalleNumero() As String
            Get
                Return m_CalleNumero
            End Get
            Set(ByVal value As String)
                m_CalleNumero = value
            End Set
        End Property
        Private m_CalleNumero As String


        Public Property Colonia() As String
            Get
                Return m_Colonia
            End Get
            Set(ByVal value As String)
                m_Colonia = value
            End Set
        End Property
        Private m_Colonia As String

        Public Property CP() As String
            Get
                Return m_CP
            End Get
            Set(ByVal value As String)
                m_CP = value
            End Set
        End Property
        Private m_CP As String

        Public Property TelCasa() As String
            Get
                Return m_TelCasa
            End Get
            Set(ByVal value As String)
                m_TelCasa = value
            End Set
        End Property
        Private m_TelCasa As String

        Public Property TelOfc() As String
            Get
                Return m_TelOfc
            End Get
            Set(ByVal value As String)
                m_TelOfc = value
            End Set
        End Property
        Private m_TelOfc As String

        Public Property Correo() As String
            Get
                Return m_Correo
            End Get
            Set(ByVal value As String)
                m_Correo = value
            End Set
        End Property
        Private m_Correo As String

        Public Property Estado() As String
            Get
                Return m_Estado
            End Get
            Set(ByVal value As String)
                m_Estado = value
            End Set
        End Property
        Private m_Estado As String

        Public Property DelMun() As String
            Get
                Return m_DelMun
            End Get
            Set(ByVal value As String)
                m_DelMun = value
            End Set
        End Property
        Private m_DelMun As String

        Public Property IDRef() As String
            Get
                Return m_IDRef
            End Get
            Set(ByVal value As String)
                m_IDRef = value
            End Set
        End Property
        Private m_IDRef As String

    End Class

    Public Class Venta

        Public Sub New(ByVal pId As String, _
                       ByVal pArticulo As String, _
                       ByVal pLinea As String, _
                       ByVal pMedida As String, _
                       ByVal pModelo As String, _
                       ByVal pCantidadesTienda As String, _
                       ByVal pCantidadesBodega As String, _
                       ByVal pLista As String, _
                       ByVal pPrecioUnitario As String, _
                       ByVal pIVA As String, _
                       ByVal pMonto As String, _
                       ByVal pDescuento As String, _
                       ByVal pTotal As String, _
                       ByVal pJuego As String, _
                       ByVal pAlmacenBox As String, _
                       ByVal pCantidadBox As String, _
                       ByVal pSubTotal As String _
                       )
            Id = pId
            Articulo = pArticulo
            Linea = pLinea
            Medida = pMedida
            Modelo = pModelo
            CantidadTienda = pCantidadesTienda
            CantidadBodega = pCantidadesBodega
            Lista = pLista '
            PrecioUnitario = pPrecioUnitario
            IVA = pIVA
            Monto = pMonto
            Descuento = pDescuento
            Total = pTotal
            Juego = pJuego
            AlmacenBox = pAlmacenBox
            CantidadBox = pCantidadBox
            subTotal = pSubTotal

        End Sub

        Private m_subTotal As String
        Public Property subTotal() As String
            Get
                Return m_subTotal
            End Get
            Set(ByVal value As String)
                m_subTotal = value
            End Set
        End Property

        Private m_CantidadBox As String
        Public Property CantidadBox() As String
            Get
                Return m_CantidadBox
            End Get
            Set(ByVal value As String)
                m_CantidadBox = value
            End Set
        End Property

        Private mAlmacenBox As String
        Public Property AlmacenBox() As String
            Get
                Return mAlmacenBox
            End Get
            Set(ByVal value As String)
                mAlmacenBox = value
            End Set
        End Property


        Public Property Total() As Double
            Get
                Return m_Total
            End Get
            Set(ByVal value As Double)
                m_Total = value
            End Set
        End Property
        Private m_Total As Double

        Public Property Descuento() As Double
            Get
                Return m_Cantidad
            End Get
            Set(ByVal value As Double)
                m_Cantidad = value
            End Set
        End Property
        Private m_Cantidad As Double

        Public Property Lista() As String
            Get
                Return m_Lista
            End Get
            Set(ByVal value As String)
                m_Lista = value
            End Set
        End Property
        Private m_Lista As String

        Public Property Juego() As String
            Get
                Return m_Juego
            End Get
            Set(ByVal value As String)
                m_Juego = value
            End Set
        End Property
        Private m_Juego As String

        Public Property PrecioUnitario() As String
            Get
                Return m_PrecioUnitario
            End Get
            Set(ByVal value As String)
                m_PrecioUnitario = value
            End Set
        End Property
        Private m_PrecioUnitario As String

        Public Property IVA() As String
            Get
                Return m_IVA
            End Get
            Set(ByVal value As String)
                m_IVA = value
            End Set
        End Property
        Private m_IVA As String

        Public Property Monto() As String
            Get
                Return m_Monto
            End Get
            Set(ByVal value As String)
                m_Monto = value
            End Set
        End Property
        Private m_Monto As String

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer

        Public Property Articulo() As String
            Get
                Return m_Articulo
            End Get
            Set(ByVal value As String)
                m_Articulo = value
            End Set
        End Property
        Private m_Articulo As String

        Public Property Linea() As String
            Get
                Return m_DescripcionArticulo
            End Get
            Set(ByVal value As String)
                m_DescripcionArticulo = value
            End Set
        End Property
        Private m_DescripcionArticulo As String

        Public Property Medida() As String
            Get
                Return m_Medida
            End Get
            Set(ByVal value As String)
                m_Medida = value
            End Set
        End Property
        Private m_Medida As String


        Public Property Modelo() As String
            Get
                Return m_Modelo
            End Get
            Set(ByVal value As String)
                m_Modelo = value
            End Set
        End Property
        Private m_Modelo As String

        Public Property CantidadTienda() As String
            Get
                Return m_CantidadesTienda
            End Get
            Set(ByVal value As String)
                m_CantidadesTienda = value
            End Set
        End Property
        Private m_CantidadesTienda As String

        Public Property CantidadBodega() As String
            Get
                Return m_CantidadesBodega
            End Get
            Set(ByVal value As String)
                m_CantidadesBodega = value
            End Set
        End Property
        Private m_CantidadesBodega As String
    End Class

    Public Class Pago

        Public Sub New(ByVal pId As String, ByVal pFormaPago As String, ByVal pMontoPagado As String, ByVal pNoCuenta As String)
            Id = pId
            FormaPago = pFormaPago '
            MontoPagado = pMontoPagado '
            NoCuenta = pNoCuenta
        End Sub

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer


        Public Property FormaPago() As String
            Get
                Return m_FormaPago
            End Get
            Set(ByVal value As String)
                m_FormaPago = value
            End Set
        End Property
        Private m_FormaPago As String

        Public Property MontoPagado() As Double
            Get
                Return m_MontoPagado
            End Get
            Set(ByVal value As Double)
                m_MontoPagado = value
            End Set
        End Property
        Private m_MontoPagado As Double

        Public Property NoCuenta() As String
            Get
                Return m_NoCuenta
            End Get
            Set(ByVal value As String)
                m_NoCuenta = value
            End Set
        End Property
        Private m_NoCuenta As String

    End Class

    Private ReadOnly Property Ventas() As List(Of Venta)
        Get
            Return New List(Of Venta)(New Venta() _
                {})
        End Get
    End Property

    Private ReadOnly Property TestClientes(Optional ByVal sCliente As String = "", Optional ByVal sRFC As String = "") As List(Of Cliente)

        Get

            Dim oList As New List(Of Cliente)()
            Dim dt As New DataTable

            If sCliente <> "" Or sRFC <> "" Then
                'dt = oDB.EjecutaQry_Tabla("select * from Clientes where Nombre like '" & sCliente & "%'", CommandType.Text, "Clientes", connstringWEB)

                Dim squery As String
                squery = "select Clientes,Socios   from AdminStore where AdminStoreID=" & Session("IDSTORE")
                Dim dtC As New DataTable
                dtC = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringWEB)

                squery = ""

                'If dtC.Rows(0).Item("Clientes") = True Then
                '    squery = squery & "'C'"
                'End If

                'If dtC.Rows(0).Item("Socios") = True Then
                '    If squery <> "" Then
                '        squery = squery & ",'S'"
                '    Else
                '        squery = squery & "'S'"
                '    End If
                'End If

                If squery <> "" Then
                    If sRFC = "" Then
                        'dt = oDB.EjecutaQry_Tabla("select * from Clientes where Nombre like '" & sCliente & "%' and TipoCliente in ('M'," & squery & ")", CommandType.Text, "Clientes", connstringWEB)
                        dt = oDB.EjecutaQry_Tabla("select top 1 * from Clientes where Nombre = '" & sCliente & "' and TipoCliente in ('M'," & squery & ") and id<>70", CommandType.Text, "Clientes", connstringWEB)
                    ElseIf sCliente = "" Then
                        'dt = oDB.EjecutaQry_Tabla("select * from Clientes where Nombre in (select nombre from clientes where RFC like '" & sRFC & "%') and TipoCliente in ('M'," & squery & ")", CommandType.Text, "Clientes", connstringWEB)
                        dt = oDB.EjecutaQry_Tabla("select top 1 * from Clientes where RFC = '" & sRFC & "' and TipoCliente in ('M'," & squery & ") and id<>70", CommandType.Text, "Clientes", connstringWEB)
                    Else
                        dt = oDB.EjecutaQry_Tabla("select top 1 * from Clientes where (Nombre like '" & sCliente & "%' and RFC = '" & sRFC & "') and TipoCliente in ('M'," & squery & ") and id<>70", CommandType.Text, "Clientes", connstringWEB)
                    End If
                Else

                    If Trim(sRFC) <> "" Then

                        'dt = oDB.EjecutaQry_Tabla("select top 1 * from Clientes where RFC like '" & sRFC & "%'", CommandType.Text, "Clientes", connstringWEB)
                        dt = oDB.EjecutaQry_Tabla("select top 1 * from Clientes where RFC = '" & sRFC & "' and id<>70", CommandType.Text, "Clientes", connstringWEB)


                    Else
                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "Debe escribir el RFC que desea buscar"
                        nconf.Title = "Error"
                        msg.Configure(nconf)
                        msg.Show()
                        Exit Property

                    End If


                End If

            Else
                'dt = oDB.EjecutaQry_Tabla("select * from Clientes", CommandType.Text, "Clientes", connstringWEB)

                Dim squery As String
                squery = "select Clientes,Socios   from AdminStore where AdminStoreID=" & Session("IDSTORE")
                Dim dtC As New DataTable
                dtC = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringWEB)

                squery = ""

                'If dtC.Rows(0).Item("Clientes") = True Then
                '    squery = squery & "'C'"
                'End If

                'If dtC.Rows(0).Item("Socios") = True Then
                '    If squery <> "" Then
                '        squery = squery & ",'S'"
                '    Else
                '        squery = squery & "'S'"
                '    End If
                'End If

                If squery <> "" Then
                    dt = oDB.EjecutaQry_Tabla("select * from Clientes where Nombre like '" & sCliente & "%' and TipoCliente in ('M'," & squery & ") and id<>70", CommandType.Text, "Clientes", connstringWEB)
                Else
                    dt = oDB.EjecutaQry_Tabla("select * from Clientes where Nombre like '" & sCliente & "%' and id<>70", CommandType.Text, "Clientes", connstringWEB)
                End If

            End If

            For Each Drow As DataRow In dt.Rows

                If IsDBNull(Drow.Item("IDRef")) Then
                    Drow.Item("IDRef") = ""
                End If

                If IsDBNull(Drow.Item("actFCHNACIMIENTO")) Then
                    Drow.Item("actFCHNACIMIENTO") = Now
                End If

                Dim columnas As String = ""
                Dim valores As String = ""

                For i = 0 To Drow.Table.Columns.Count - 1

                    If i <> Drow.Table.Columns.Count - 1 Then
                        If i <> 0 Then
                            columnas = columnas & Drow.Table.Columns.Item(i).ColumnName & ","
                            valores = valores & "'" & Drow.Item(Drow.Table.Columns.Item(i).ColumnName) & "',"
                        End If
                    Else
                        columnas = columnas & Drow.Table.Columns.Item(i).ColumnName
                        valores = valores & "'" & Drow.Item(Drow.Table.Columns.Item(i).ColumnName) & "'"
                    End If




                    If IsDBNull(Drow.Item(Drow.Table.Columns.Item(i).ColumnName)) And Drow.Table.Columns.Item(i).ColumnName <> "actFCHNACIMIENTO" Then
                        Select Case Drow.Table.Columns.Item(i).DataType.Name
                            Case "String"
                                Drow.Item(Drow.Table.Columns.Item(i).ColumnName) = ""
                            Case Else
tag:
                        End Select

                    End If

                Next


                'If IsNothing(Session("LCLIENTES")) = True Then
                '    Session("LCLIENTES") = ""
                'End If

                'Session("LCLIENTES") = Session("LCLIENTES") & "set dateformat dmy insert into Log_Clientes (Accion,fecha," & columnas & ") values ('Se selecciono el RFC',GETDATE()," & valores & ")" & vbNewLine


                oList.Add(New Cliente(Drow.Item("ID"), Drow.Item("Nombre"), Drow.Item("RFC"), Drow.Item("CalleNumero"), Drow.Item("Colonia"), Drow.Item("CP"), Drow.Item("TelCasa"), Drow.Item("TelOfc"), Drow.Item("Correo"), Drow.Item("Estado"), Drow.Item("DelMun"), Drow.Item("IDRef"), Drow.Item("TipoCliente"), Drow.Item("actFCHNACIMIENTO"), Drow.Item("actNOMBRE"), Drow.Item("actAPEPATERNO"), Drow.Item("actAPEMATERNO"), Drow.Item("actCALLE"), Drow.Item("actNUMEXT"), Drow.Item("actNUMINT"), Drow.Item("actCIUDAD"), Drow.Item("actCod_REGIONestado"), Drow.Item("actCod_PROVINCIAmunicipio"), Drow.Item("actCod_CIUDAD"), Drow.Item("actCod_COMUNAcolonia")))

            Next

            Return oList

        End Get

    End Property

    Private ReadOnly Property Pagos() As List(Of Pago)
        Get
            Return New List(Of Pago)(New Pago() _
                {})
        End Get
    End Property

    Private Shared curId As Integer = 7
    Private Shared lockObj As New Object()

    Private Shared curIdClient As Integer = 2
    Private Shared lockObjClient As New Object()

    Private Shared curIdPago As Integer = 1
    Private Shared lockObjPago As New Object()

    Private ReadOnly Property NewId() As Integer
        Get
            'Return System.Threading.Interlocked.Increment(curId)
            'curId = curId + 1
            'Return curId
        End Get
    End Property

    Private ReadOnly Property NewClient() As Integer
        Get
            'Return System.Threading.Interlocked.Increment(curId)
            'curId = curId + 1
            'Return curId
        End Get
    End Property

    Private ReadOnly Property NewPago() As Integer
        Get
            'Return System.Threading.Interlocked.Increment(curId)
            'curId = curId + 1
            'Return curId
        End Get
    End Property

    Private ReadOnly Property CurrentData() As List(Of Venta)
        Get
            Dim persons = Me.Session("TestPersons")

            If persons Is Nothing Then
                persons = Me.Ventas
                Me.Session("TestPersons") = persons
            End If

            Return DirectCast(persons, List(Of Venta))
        End Get
    End Property

    Private ReadOnly Property CurrentDataClientes(Optional ByVal sCliente As String = "", Optional ByVal sRFC As String = "") As List(Of Cliente)
        Get
            Dim clientes = Me.Session("clientes")

            'If clientes Is Nothing Then
            If sCliente <> "" Or sRFC <> "" Then
                Try
                    clientes = Me.TestClientes(sCliente, sRFC)
                Catch ex As Exception

                End Try
            Else
                Try
                    clientes = Me.TestClientes(sCliente, sRFC)
                Catch ex As Exception

                End Try
            End If

            Me.Session("clientes") = clientes

            'End If

            Return DirectCast(clientes, List(Of Cliente))
        End Get
    End Property

    Private ReadOnly Property CurrentDataPago() As List(Of Pago)
        Get
            Dim pagos = Me.Session("Pagos")

            If pagos Is Nothing Then
                pagos = Me.Pagos
                Me.Session("Pagos") = pagos
            End If

            Return DirectCast(pagos, List(Of Pago))
        End Get
    End Property

    Private Function AddPerson(ByVal person As Venta) As Integer
        SyncLock lockObj
            Dim persons = Me.CurrentData
            person.Id = persons.Count
            persons.Add(person)
            Me.Session("TestPersons") = persons

            Return person.Id
        End SyncLock
    End Function

    Private Function AddCliente(ByVal cliente As Cliente) As Integer
        SyncLock lockObj
            Dim clientes = Me.CurrentDataClientes
            cliente.Id = clientes.Count
            clientes.Add(cliente)
            Me.Session("clientes") = clientes

            Return cliente.Id
        End SyncLock
    End Function

    Private Function AddPago(ByVal pago As Pago) As Integer
        SyncLock lockObjPago
            Dim pagos = Me.CurrentDataPago
            pago.Id = pagos.Count
            pagos.Add(pago)
            Me.Session("Pagos") = pagos
            Return pago.Id
        End SyncLock
    End Function

    Private Sub DeletePerson(ByVal id As Integer)
        SyncLock lockObj
            Dim persons = Me.CurrentData
            Dim person As Venta = Nothing

            For Each p As Venta In persons
                If p.Id = id Then
                    person = p
                    Exit For
                End If
            Next

            'throw new Exception("TestPerson not found");
            If person Is Nothing Then
            End If

            persons.Remove(person)

            Me.Session("TestPersons") = persons
        End SyncLock
    End Sub

    Private Sub DeleteCliente(ByVal id As Integer)
        SyncLock lockObj
            Dim clientes = Me.CurrentDataClientes
            Dim cliente As Cliente = Nothing

            For Each p As Cliente In clientes
                If p.Id = id Then
                    cliente = p
                    Exit For
                End If
            Next

            If cliente Is Nothing Then
            End If

            clientes.Remove(cliente)

            Me.Session("clientes") = clientes
        End SyncLock
    End Sub

    Private Sub DeletePago(ByVal id As Integer)
        SyncLock lockObjPago
            Dim pagos = Me.CurrentDataPago
            Dim pago As Pago = Nothing

            For Each p As Pago In pagos
                If p.Id = id Then
                    pago = p
                    Exit For
                End If
            Next

            If pago Is Nothing Then
            End If

            pagos.Remove(pago)

            Me.Session("Pagos") = pagos
        End SyncLock
    End Sub

    Private Sub UpdatePerson(ByVal person As Venta)
        SyncLock lockObj

            Dim persons = Me.CurrentData
            Dim updatingPerson As Venta = Nothing

            For Each p As Venta In persons
                If p.Id = person.Id Then
                    updatingPerson = p
                    Exit For
                End If
            Next

            If updatingPerson Is Nothing Then
                Throw New Exception("TestPerson not found")
            End If

            updatingPerson.Articulo = person.Articulo
            updatingPerson.Linea = person.Linea
            updatingPerson.Medida = person.Medida
            updatingPerson.CantidadBodega = person.CantidadBodega
            updatingPerson.Modelo = person.Modelo
            updatingPerson.CantidadTienda = person.CantidadTienda

            Me.Session("TestPersons") = persons
        End SyncLock
    End Sub

    Private Sub Updatecliente(ByVal cliente As Cliente)
        SyncLock lockObj

            Dim clientes = Me.CurrentDataClientes
            Dim updatingcliente As Cliente = Nothing
            Dim squery As String
            Dim dt As New DataTable

            'squery = "select * from clientes where RFC='" & ftFindRFC.Text & "'"
            'dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente", connstringWEB)

            For Each p As Cliente In CurrentDataClientes(tfFindNombre.Text, ftFindRFC.Text)  'clientes
                If p.Id = cliente.Id Then
                    updatingcliente = p
                    Exit For
                End If
            Next

            'updatingcliente = cliente



            If updatingcliente Is Nothing Then
                'Throw New Exception("Cliente no Encontrado")
                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Cliente no Encontrado"
                nconf.Title = "Advertencia!"
                msg.Configure(nconf)
                msg.Show()
                Exit Sub
            End If

            If actNOMBRE.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo nombre es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actAPEPATERNO.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo A. Paterno es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actAPEMATERNO.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo A. Materno es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            tfNombre.Text = Trim(actNOMBRE.Text) & " " & Trim(actAPEPATERNO.Text) & " " & Trim(actAPEMATERNO.Text)

            If actCALLE.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Calle es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If tfColonia.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Colonia es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actCod_REGIONestado.SelectedItem.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Estado es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If tfDelMun.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Del/Mun es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            'If Trim(cmbTipoCliente.SelectedItem.Value) = "" Then

            '    Dim msg As New Ext.Net.Notification
            '    Dim nconf As New Ext.Net.NotificationConfig
            '    nconf.IconCls = "icon-exclamation"
            '    nconf.Html = "Debe seleccionar un tipo de cliente"
            '    nconf.Title = "Error"
            '    msg.Configure(nconf)
            '    msg.Show()

            '    Exit Sub

            'End If

            updatingcliente.Nombre = tfNombre.Text
            updatingcliente.RFC = updatingcliente.RFC
            updatingcliente.CalleNumero = tfCalleNumero.Text
            updatingcliente.Colonia = tfColonia.Text
            updatingcliente.TelCasa = tfTelCasa.Text
            updatingcliente.CP = tfCP.Text
            With updatingcliente
                .TelCasa = tfTelCasa.Text
                .TelOfc = tfTelOfc.Text
                .Correo = tfCorreo.Text
                .IDRef = tfIDRef.Text
                .actFCHNACIMIENTO = actFCHNACIMIENTO.Value.ToString.Split(" ")(0)
                .actNOMBRE = actNOMBRE.Text
                .actAPEPATERNO = actAPEPATERNO.Text
                .actAPEMATERNO = actAPEMATERNO.Text
                .actCALLE = actCALLE.Text
                .actNUMEXT = actNUMEXT.Text
                .actNUMINT = actNUMINT.Text
                .actCIUDAD = actCod_CIUDAD.SelectedItem.Text
                .actCod_REGIONestado = actCod_REGIONestado.SelectedItem.Value
                .Estado = actCod_REGIONestado.SelectedItem.Text
                .actCod_PROVINCIAmunicipio = actCod_PROVINCIAmunicipio.SelectedItem.Value
                .DelMun = tfDelMun.Text
                .actCod_CIUDAD = actCod_CIUDAD.SelectedItem.Value
                .actCod_COMUNAcolonia = actCod_COMUNAcolonia.SelectedItem.Value

            End With

            With updatingcliente
                'squery = "Update clientes set IDRef='" & .IDRef & "',RFC='" & .RFC & "',CalleNumero='" & .CalleNumero & "',Colonia='" & .Colonia & "',CP='" & .CP & "',TelCasa='" & .TelCasa & "',TelOfc='" & .TelOfc & "',Correo='" & .Correo & "',Estado='" & .Estado & "',DelMun='" & .DelMun & "',actFCHNACIMIENTO='" & .actFCHNACIMIENTO & "' where id='" & .Id & "'"

                squery = "SET DATEFORMAT DMY Update clientes set"
                squery = squery & " Nombre='" & Trim(.actNOMBRE) & " " & Trim(.actAPEPATERNO) & " " & Trim(.actAPEMATERNO) & "'" & vbNewLine
                squery = squery & " ,RFC=UPPER('" & .RFC & "')" & vbNewLine
                squery = squery & " ,CalleNumero='" & Trim(.actCALLE) & " #EXT:" & Trim(.actNUMEXT) & If(Trim(.actNUMINT) <> "", " #INT:" & Trim(.actNUMINT), "") & "'" & vbNewLine
                squery = squery & " ,Colonia='" & .Colonia & "'" & vbNewLine
                squery = squery & " ,CP='" & .CP & "'" & vbNewLine
                squery = squery & " ,TelCasa='" & .TelCasa & "'" & vbNewLine
                squery = squery & " ,TelOfc='" & .TelOfc & "'" & vbNewLine
                squery = squery & " ,Correo='" & .Correo & "'" & vbNewLine
                squery = squery & " ,Estado='" & .Estado & "'" & vbNewLine
                squery = squery & " ,DelMun='" & .DelMun & "'" & vbNewLine
                squery = squery & " ,IDRef='" & .IDRef & "'" & vbNewLine
                squery = squery & " ,TipoCliente='" & "C" & "'" & vbNewLine
                squery = squery & " ,actFCHNACIMIENTO='" & .actFCHNACIMIENTO & "'" & vbNewLine
                squery = squery & " ,actNOMBRE='" & .actNOMBRE & "'" & vbNewLine
                squery = squery & " ,actAPEPATERNO='" & .actAPEPATERNO & "'" & vbNewLine
                squery = squery & " ,actAPEMATERNO='" & .actAPEMATERNO & "'" & vbNewLine
                squery = squery & " ,actCALLE='" & .actCALLE & "'" & vbNewLine
                squery = squery & " ,actNUMEXT='" & .actNUMEXT & "'" & vbNewLine
                squery = squery & " ,actNUMINT='" & .actNUMINT & "'" & vbNewLine
                squery = squery & " ,actCIUDAD='" & .actCIUDAD & "'" & vbNewLine
                squery = squery & " ,actCod_REGIONestado='" & .actCod_REGIONestado & "'" & vbNewLine
                squery = squery & " ,actCod_PROVINCIAmunicipio='" & .actCod_PROVINCIAmunicipio & "'" & vbNewLine
                squery = squery & " ,actCod_CIUDAD='" & .actCod_CIUDAD & "'" & vbNewLine
                squery = squery & " ,actCod_COMUNAcolonia='" & .actCod_COMUNAcolonia & "'" & vbNewLine
                squery = squery & " where RFC='" & .RFC & "'" & vbNewLine & vbNewLine

                squery = squery & "set dateformat dmy insert into Log_Clientes (Accion,fecha,Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,IDRef,TipoCliente,CardCode,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,actCALLE,actNUMEXT,actNUMINT,actCod_COMUNAcolonia,actCod_REGIONestado,actCod_PROVINCIAmunicipio,actCod_CIUDAD,actCIUDAD,Referencia,Tienda,Usuario,IDCliente) values "
                squery = squery & "('Se actualizo el Cliente',GETDATE(),'" & Trim(.actNOMBRE) & " " & Trim(.actAPEPATERNO) & " " & Trim(.actAPEMATERNO) & "'" & _
                ",UPPER('" & .RFC & "')," & _
                "'" & Trim(.actCALLE) & " #EXT:" & Trim(.actNUMEXT) & If(Trim(.actNUMINT) <> "", " #INT:" & Trim(.actNUMINT), "") & "'," & _
                "'" & .Colonia & "'," & _
                "'" & .CP & "'," & _
                "'" & .TelCasa & "'," & _
                "'" & .TelOfc & "'," & _
                "'" & .Correo & "'," & _
                "'" & .Estado & "'," & _
                "'" & .DelMun & "'," & _
                "'" & .IDRef & "','C'" & _
                ",'','" & .actFCHNACIMIENTO & "','" & .actNOMBRE & "','" & .actAPEPATERNO & "','" & .actAPEMATERNO & "'," & _
                "'" & .actCALLE & "','" & .actNUMEXT & "','" & .actNUMINT & "','','12','','','','SIN REFERENCIA','" & Session("IDSTORE") & "'," & Session("AdminUserID") & "," & .Id & ")"

                oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)
            End With

            Me.Session("clientes") = clientes
            btnCleanClient.FireEvent("Click")

            Me.BindDataCliente(tfFindNombre.Text, ftFindRFC.Text)

            If tfFindNombre.Text <> "" Or ftFindRFC.Text <> "" Then
                If Session("clientes").count = 0 Then
tag:
                    tfFindNombre.Text = ""
                    ftFindRFC.Text = ""
                    Button2.FireEvent("Click")
                End If
            End If

            'StoreClientes.DataBind()

        End SyncLock
    End Sub

    Private Sub UpdatePago(ByVal pago As Pago)
        SyncLock lockObjPago

            Dim pagos = Me.CurrentDataPago
            Dim updatingPago As Pago = Nothing

            For Each p As Pago In pagos
                If p.Id = pago.Id Then
                    updatingPago = p
                    Exit For
                End If
            Next

            If updatingPago Is Nothing Then
                Throw New Exception("TestPerson not found")
            End If

            updatingPago.FormaPago = pago.FormaPago
            updatingPago.MontoPagado = pago.MontoPagado


            Me.Session("Pagos") = pagos
        End SyncLock
    End Sub

    Private Sub BindData()
        'If X.IsAjaxRequest Then
        'Return
        'End If

        Me.Store1.DataSource = Me.CurrentData
        Me.Store1.DataBind()

    End Sub

    Private Sub BindDataCliente(Optional ByVal sCliente As String = "", Optional ByVal sRFC As String = "")
        'If X.IsAjaxRequest Then
        'Return
        'End If

        If sCliente <> "" Or sRFC <> "" Then
            Try
                Me.StoreClientes.DataSource = Me.CurrentDataClientes(sCliente, sRFC)
            Catch ex As Exception

            End Try
        Else
            Try
                Me.StoreClientes.DataSource = Me.CurrentDataClientes("-1", sRFC)
            Catch ex As Exception

            End Try
        End If

        Me.StoreClientes.DataBind()


    End Sub

    Private Sub BindDataPago()
        'If X.IsAjaxRequest Then
        'Return
        'End If

        Me.StorePagos.DataSource = Me.CurrentDataPago
        Me.StorePagos.DataBind()
    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)
        Dim Ventas As ChangeRecords(Of Venta) = e.DataHandler.BatchObjectData(Of Venta)()

        grdPagos.GetStore().Each(New Ext.Net.JFunction())
        If Session("bCreated") = False Then

            For Each created As Venta In Ventas.Created

                Session("bCreated") = True

                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddPerson(created)

                'If Store1.UseIdConfirmation Then
                'e.ConfirmationList.ConfirmRecord(tempId.ToString(), newId.ToString())
                'Else
                Store1.GetById(tempId).SetId(newId)

                Dim dMonto As Double = 0

                For Each art As Venta In Session("TestPersons")
                    dMonto = dMonto + (art.Lista)
                Next

                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = FormatCurrency(dMonto, 2)

                Dim dMontoPagado As Double
                Dim dMontoEfe As Double

                For Each Pag As Pago In CurrentDataPago
                    If Pag.FormaPago = "Efectivo" Then
                        dMontoEfe = dMontoEfe + Pag.MontoPagado
                    End If
                    dMontoPagado = dMontoPagado + Pag.MontoPagado
                Next

                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dMontoPagado, 2)
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = FormatCurrency(dMontoEfe, 2)
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency(dMonto - dMontoPagado, 2)

                If ((dMonto - dMontoPagado) < 0) And (((dMontoPagado - dMontoEfe) <= (dMonto - dMontoPagado)) Or (dMontoPagado - dMontoEfe) = 0) Then
                    DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMonto - dMontoPagado) * -1)
                Else
                    If (dMontoPagado - dMonto) <= 0 Then
                        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency(0, 2)
                    Else
                        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMontoPagado - dMonto), 2)
                    End If
                End If

            Next

        Else

            Session("bCreated") = False

        End If

        Dim borrados As Integer = 0

        For Each deleted As Venta In Ventas.Deleted
            Me.DeletePerson(deleted.Id)

            'If Store1.UseIdConfirmation Then
            ' e.ConfirmationList.ConfirmRecord(deleted.Id.ToString())
            ' End If

            Dim dMonto As Double = 0

            If DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = "" Then
                dMonto = Ventas.Deleted.Item(0).Lista
            Else
                dMonto = DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text - Ventas.Deleted.Item(0).Total
            End If

            'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = dMonto
            DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = FormatCurrency(dMonto, 4)

            'Reordenar ID's

            For iid As Integer = 0 To Session("TestPersons").count - 1
                Session("TestPersons")(iid).id = iid + 1
            Next

            For id As Integer = 0 To CurrentData.Count - 1
                CurrentData.Item(id).Id = id + 1
            Next

            borrados = borrados + 1
            actualizaMontos()

        Next

        For Each updated As Venta In Ventas.Updated
            Me.UpdatePerson(updated)

            'If Store1.UseIdConfirmation Then
            'e.ConfirmationList.ConfirmRecord(updated.Id.ToString())
            'End If
        Next

        e.Cancel = True

        If borrados <> 0 Then
            Store1.CommitChanges()
            'Response.Redirect("Facturacion.aspx")
        End If

    End Sub

    Protected Sub HandleChangesClientes(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)
        Dim clientes As ChangeRecords(Of Cliente) = e.DataHandler.BatchObjectData(Of Cliente)()

        If Session("bCreated") = False Then

            For Each created As Cliente In clientes.Created

                Session("bCreated") = True
                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddCliente(created)

                'If StoreClientes.UseIdConfirmation Then
                'e.ConfirmationList.ConfirmRecord(tempId.ToString(), newId.ToString())
                'Else
                StoreClientes.GetById(tempId).SetId(newId)
                '    StoreClientes.GetAt(0).SetId( UpdateRecordId(tempId, newId)
                'End If
            Next

        Else
            Session("bCreated") = False
        End If

        For Each deleted As Cliente In clientes.Deleted
            Me.DeleteCliente(deleted.Id)

            'If StoreClientes.UseIdConfirmation Then
            'e.ConfirmationList.ConfirmRecord(deleted.Id.ToString())
            'End If
        Next

        For Each updated As Cliente In clientes.Updated
            Me.Updatecliente(updated)

            'If StoreClientes.UseIdConfirmation Then
            'e.ConfirmationList.ConfirmRecord(updated.Id.ToString())
            'End If
        Next
        e.Cancel = True
    End Sub

    Protected Sub HandleChangesPagos(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)
        Dim pagos As ChangeRecords(Of Pago) = e.DataHandler.BatchObjectData(Of Pago)()

        If Session("bCreated") = False Then
            For Each created As Pago In pagos.Created

                Session("bCreated") = True

                If created.FormaPago = "Efectivo" Then
                    created.NoCuenta = ""
                Else
                    If created.NoCuenta = "" Then
                        pagos.Created.Remove(pagos.Created.Item(0))
                        BindDataPago()
                        Session("bCreated") = False
                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "Debe teclear los ultimos 4 digitos de la tarjeta/cuenta"
                        nconf.Title = "Advertencia!"
                        msg.Configure(nconf)
                        msg.Show()
                        GoTo EndProcess
                    ElseIf created.NoCuenta.Length <> 4 Then
                        pagos.Created.Remove(pagos.Created.Item(0))
                        BindDataPago()
                        Session("bCreated") = False
                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "Debe teclear los ultimos 4 digitos de la tarjeta/cuenta"
                        nconf.Title = "Advertencia!"
                        msg.Configure(nconf)
                        msg.Show()
                        GoTo EndProcess
                    End If
                End If

                Dim dPagosAplicados As Double = 0
                Dim dPagosEfectivo As Double = 0
                Dim dOtrosPagos As Double = 0

                For Each pagoTmp As Pago In CurrentDataPago
                    dPagosAplicados = dPagosAplicados + pagoTmp.MontoPagado
                    If pagoTmp.FormaPago = "Efectivo" Then
                        dPagosEfectivo = dPagosEfectivo + pagoTmp.MontoPagado
                    Else
                        dOtrosPagos = dOtrosPagos + pagoTmp.MontoPagado
                    End If
                Next

                dPagosAplicados = dPagosAplicados + pagos.Created.Item(0).MontoPagado

                If pagos.Created.Item(0).FormaPago = "Efectivo" Then
                    dPagosEfectivo = dPagosEfectivo + pagos.Created.Item(0).MontoPagado
                Else
                    dOtrosPagos = dOtrosPagos + pagos.Created.Item(0).MontoPagado
                End If

                Dim dMontoAPagar As Double = 0

                If DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = "" Then
                    dMontoAPagar = 0
                Else
                    dMontoAPagar = CDbl(DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text)
                End If

                dMontoAPagar = 0

                For Each oVenta In CurrentData
                    dMontoAPagar = dMontoAPagar + oVenta.Total
                Next

                'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = dPagosEfectivo
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = FormatCurrency(dPagosEfectivo, 4)


                If dPagosAplicados > dMontoAPagar Then
                    'If ((dMontoAPagar - dPagosAplicados) < 0) And ((dOtrosPagos <= (dMontoAPagar - dPagosAplicados)) Or dOtrosPagos = 0) Then
                    If (dOtrosPagos < dMontoAPagar) Then
                        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = (dMontoAPagar - dPagosAplicados) * -1
                        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = dPagosAplicados
                        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = (dMontoAPagar - dPagosAplicados) '((dMontoAPagar - dOtrosPagos) - (dPagosAplicados - dOtrosPagos))
                        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMontoAPagar - dPagosAplicados) * -1, 4)
                        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dPagosAplicados, 4)
                        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency((dMontoAPagar - dPagosAplicados), 4) '((dMontoAPagar - dOtrosPagos) - (dPagosAplicados - dOtrosPagos))
                    Else
                        pagos.Created.Remove(pagos.Created.Item(0))
                        BindDataPago()
                        Session("bCreated") = False
                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "El monto del pago excede la cantidad a pagar"
                        nconf.Title = "Advertencia!"
                        msg.Configure(nconf)
                        msg.Show()
                        GoTo EndProcess
                    End If
                Else

                    'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = dPagosAplicados
                    'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = dMontoAPagar - dPagosAplicados
                    DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dPagosAplicados, 4)
                    DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency(dMontoAPagar - dPagosAplicados, 4)

                End If

                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddPago(created)

                'If StorePagos.UseIdConfirmation Then
                'e.ConfirmationList.ConfirmRecord(tempId.ToString(), newId.ToString())
                'Else
                'grdPagos.Store.Item(0).
                StorePagos.GetById(tempId).SetId(newId)
                'End If
            Next

        Else
            Session("bCreated") = False
        End If

        For Each deleted As Pago In pagos.Deleted
            Me.DeletePago(deleted.Id)

            'If StorePagos.UseIdConfirmation Then
            'e.ConfirmationList.ConfirmRecord(deleted.Id.ToString())
            'End If
        Next

        For Each updated As Pago In pagos.Updated
            Me.UpdatePago(updated)

            'If StorePagos.UseIdConfirmation Then
            'e.ConfirmationList.ConfirmRecord(updated.Id.ToString())
            'End If
        Next

        actualizaMontos()

EndProcess:

        'If lblDocType.Text <> "4" And tfTipoCliente.Text <> "M" Then
        '    cmbFolioType.SetValue("2")
        'End If

        e.Cancel = True

    End Sub

    Public Sub Procesando()

        If Ext.Net.X.IsAjaxRequest = False Then

            '        winMsg = New Window() With { _
            '  .ID = "Window1", _
            '  .Title = "My Window", _
            '  .Height = 185, _
            '  .Width = 350, _
            '  .Padding = 5, _
            '  .Html = "Por favor espere .." _
            '}

            '        winMsg.Render()

        End If

    End Sub

    Private Sub Page_Disposed(sender As Object, e As System.EventArgs) Handles Me.Disposed

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim useConfirmation__1 As Boolean
        Dim dt As New DataTable
        Dim squery As String

        'Session("WHSID") = "CEN000"

        If Ext.Net.X.IsAjaxRequest = False Then

            If Session("STORETYPEID") = 3 Then
                Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
            End If

            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString


            squery = "select COUNT(*) from StoreFolios where AdminStoreID=" & Session("IDSTORE") & " and (Prefijo<>'' and Prefijo is Not null)"

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente Default", connstringWEB)

            If dt.Rows.Item(0).Item(0) < 4 Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Antes de comenzar a vender debe definir los folios de la tienda"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Button4.Enabled = False
                Button4.Disabled = True

                btnCapturar.Enabled = False
                btnCapturar.Disabled = True

                LimpiarArticulo.Enabled = False
                LimpiarArticulo.Disabled = True

                Button11.Enabled = False
                Button11.Disabled = True

                Button3.Enabled = False
                Button3.Disabled = True

                btnCancelarFactura.Enabled = False
                btnCancelarFactura.Disabled = True

                For i As Integer = 0 To UserForm.Items.Count - 1

                    UserForm.Items.Item(i).Enabled = False
                    UserForm.Items.Item(i).Disabled = True

                Next


                For i As Integer = 0 To FormPanel1.Items.Count - 1

                    FormPanel1.Items.Item(i).Enabled = False
                    FormPanel1.Items.Item(i).Disabled = True

                Next


                GridPanel1.Enabled = False
                'UserForm.Enabled = False
                'UserForm.Disabled = True



            End If

            Session("SelCount") = 0

            cmbFP.Items.Add(New Ext.Net.ListItem("Efectivo", "Efectivo"))
            cmbFP.Items.Add(New Ext.Net.ListItem("Tarjeta de Crédito", "Tarjeta de Crédito"))
            cmbFP.Items.Add(New Ext.Net.ListItem("Tarjeta de Débito", "Tarjeta de Débito"))
            cmbFP.Items.Add(New Ext.Net.ListItem("American Express", "American Express"))
            cmbFP.Items.Add(New Ext.Net.ListItem("Deposito / Cheque", "Deposito / Cheque"))
            cmbFP.Items.Add(New Ext.Net.ListItem("Nota de crédito ", "Nota de crédito"))
            cmbFP.Items.Add(New Ext.Net.ListItem("Otros", "Otros"))
            cmbFP.SelectedItem.Index = 0


            squery = "select ItmsTypCod,ItmsGrpNam from OITG WHERE ItmsTypCod IN (1,2,3,4,5,6,7)"

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente Default", connstringSAP)

            For Each Drow As DataRow In dt.Rows
                tfArticulo.Items.Add(New Ext.Net.ListItem(Drow("ItmsGrpNam").ToString(), Drow("ItmsTypCod")))
            Next


            If Not IsNothing(Session("CurrentClient")) Then

                squery = "SELECT Clientes.Id, Clientes.Nombre, Clientes.RFC, Clientes.CalleNumero, Clientes.Colonia, Clientes.CP, Clientes.TelCasa, Clientes.TelOfc, Clientes.Correo, "
                squery = squery & " Clientes.Estado, Clientes.DelMun, Clientes.IDRef, Clientes.TipoCliente"
                squery = squery & " FROM"
                squery = squery & " Clientes"
                squery = squery & " where Clientes.Id='" & DirectCast(Session("CurrentClient"), Cliente).Id & "'"


                oDB.ConectaDBConnString(connstringWEB)
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente Default", connstringWEB)

                For Each Drow As DataRow In dt.Rows
                    tfCliente.Text = Drow("Id")
                    tfNombreCliente.Text = Drow("Nombre")
                    tfRFCCliente.Text = Drow("RFC")
                    tfTelCasaCliente.Text = Drow("TelCasa")
                    tfDireccion.Text = Drow("CalleNumero") & vbNewLine & _
                                       Drow("Colonia") & vbNewLine & _
                                       Drow("DelMun") & vbNewLine & _
                                       Drow("Estado") & vbNewLine & _
                                       "CP: " & Drow("CP") & vbNewLine
                    tfTipoCliente.Text = Drow("TipoCliente")
                    lblTipoCliente.Text = Drow("TipoCliente")
                Next


            End If


        End If

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        oDB.ConectaDBConnString(connstringWEB)

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.Store1.SuspendScripting()
            Me.Store1.ResumeScripting()
        End If

        Me.BindData()

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.StoreClientes.SuspendScripting()
            Me.StoreClientes.ResumeScripting()
        End If

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.StorePagos.SuspendScripting()
            Me.StorePagos.ResumeScripting()
        End If

        Me.BindDataPago()

        'If Session("ClientesMode") = "U" Then
        '    PanelIzquierda.Show()
        '    PanelCentro.Hide()
        'ElseIf Session("ClientesMode") = "A" Then
        '    PanelIzquierda.Hide()
        '    PanelCentro.Show()
        'End If

        If DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = "" Then
            actualizaMontos()
        End If

        'Listas de precios -----------------------------------------------
        Dim storeListas As New Store()
        storeListas.ID = "StoreListas"

        ' Create Proxy
        Dim proxy2 As New AjaxProxy()
        proxy2.ActionMethods.Read = HttpMethod.POST
        proxy2.Url = "Listas.ashx"

        ' Create Reader
        'Dim readerListas As New JsonReader()
        'readerListas.Root = "listas"
        'readerListas.TotalProperty = "Total"

        ' Add Fields
        'Dim model As New Model()
        'model.Fields.Add(New ModelField("cLista"))
        'model.Fields.Add(New ModelField("NombreLista"))

        ' Add Proxy and Reader to Store
        'storeListas.Proxy.Add(proxy2)
        ' storeListas.Reader.Add(readerListas)
        'storeListas.AutoLoad = False

        ' Add Store to Controls Collection
        'Me.ViewportPrincipal.Controls.Add(storeListas)

        'cargaListas()
        'ObtenerArticulos()


    End Sub

    Public Sub cargaListas(Optional ByVal Cambio As Boolean = False)


        Dim dt As New DataTable
        Dim squery As String

        squery = "select ListID listnum,isnull(ListName,'') listname from storelist where AdminStoreID='" & Session("IDSTORE") & "' and listid in (select ListID from StoreList where AdminStoreID='" & Session("IDSTORE") & "') union select defaultlist,(select ListName from " & Session("SAPDB") & ".OPLN where OPLN.ListNum=defaultlist) from AdminStore where AdminStoreID=" & Session("IDSTORE") & " union select listid,listname from StoreListGlobal"

        'No incluye lista default
        'squery = "select ListID listnum,isnull(ListName,'') listname from storelist where AdminStoreID='" & Session("IDSTORE") & "' and listid in (select ListID from StoreList where AdminStoreID='" & Session("IDSTORE") & "')"

        oDB.ConectaDBConnString(connstringSAP)
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Listas", connstringWEB)

        Dim cuenta As Integer = 0

        For Each Drow As DataRow In dt.Rows

            Lista.Items.Add(New Ext.Net.ListItem(Drow("ListName").ToString(), Drow("ListNum")))

            If cuenta = 0 Then
                If Lista.SelectedItem.Index = -1 Then
                    Lista.SelectedItem.Index = 0
                End If
            End If

            cuenta = cuenta + 1

        Next

        If Cambio = True Then
            'Lista.SelectedIndex = 0
            If Lista.SelectedItem.Value = ConfigurationManager.AppSettings("ListaJuego") Or Lista.SelectedItem.Value = ConfigurationManager.AppSettings("ListaJuegoFin") Then
                Lista.SelectedItem.Index = 0
            End If

            CantidadesTienda.Render()
            CantidadesBodega.Render()
            Combo.Render()
            AJuego.Render()
            Lista.Disabled = False
            Lista.Enabled = True
            Lista.Render()
            'CantidadesTienda.Render()
            'CantidadesBodega.Render()
            'PrecioUnitario.Render()
            'IVA.Render()
            PUIVA.Render()
            Monto.Render()
            Descuento.Render()
            Total.Render()
        End If

    End Sub


    <DirectMethod()> _
    Public Sub Limpiar()

tag:

        cbLinea.Items.Clear()
        tfMedida.Items.Clear()

        cbLinea.Text = ""
        tfMedida.Text = ""
        Modelo.Text = ""
        cbLinea.Render()
        tfMedida.Render()
        Modelo.Render()
        Combo.Render()
        Lista.Render()
        CantidadesTienda.Render()
        CantidadesBodega.Render()
        PrecioUnitario.Render()
        IVA.Render()
        Monto.Render()
        Descuento.Render()
        Total.Render()


    End Sub

    <DirectMethod()> _
    Public Sub redirige()

        Response.Redirect("Facturacion.aspx")

    End Sub

    <DirectMethod()> _
    Public Sub actualizaMontos()

        Dim dMonto As Double = 0
        Dim dDescuento As Double = 0

        For Each art As Venta In CurrentData
            dMonto = dMonto + (art.Total)
            dDescuento = dDescuento + art.Descuento
        Next

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = FormatCurrency(dMonto, 2)

        Dim dMontoPagado As Double
        Dim dMontoEfe As Double

        For Each Pag As Pago In CurrentDataPago
            If Pag.FormaPago = "Efectivo" Then
                dMontoEfe = dMontoEfe + Pag.MontoPagado
            End If
            dMontoPagado = dMontoPagado + Pag.MontoPagado
        Next

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dMontoPagado, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = FormatCurrency(dMontoEfe, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency(dMonto - dMontoPagado, 2)

        If dMontoPagado >= dMonto Then
            'cmbFolioType.Disabled = False


            'If Ext.Net.X.IsAjaxRequest = True Then
            '    cmbFolioType.Render()
            '    Comentario.Render()
            '    Button3.Render()
            '    btnCancelarFactura.Render()

            'End If
        Else
            cmbFolioType.SelectedItem.Index = 0
            'cmbFolioType.Disabled = True


            'If Ext.Net.X.IsAjaxRequest = True Then
            '    cmbFolioType.Render()
            '    Comentario.Render()
            '    Button3.Render()
            '    btnCancelarFactura.Render()

            'End If
        End If

        If ((dMonto - dMontoPagado) < 0) And (((dMontoPagado - dMontoEfe) <= (dMonto - dMontoPagado)) Or (dMontoPagado - dMontoEfe) = 0) Then
            DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMonto - dMontoPagado) * -1)
        Else
            If (dMontoPagado - dMonto) <= 0 Then
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency(0, 2)
            Else
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMontoPagado - dMonto), 2)
            End If
        End If

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(10), TextField).Text = FormatCurrency(dDescuento, 2)

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(12), TextField).Text = FormatCurrency(dMonto - dMontoPagado, 2)

        txtMontoP.Text = dMonto - dMontoPagado

        Try
            GridPanel1.Refresh()
        Catch ex As Exception

        End Try


    End Sub

    Public Function ObtenRestante() As Double

        Dim dMonto As Double = 0

        For Each art As Venta In CurrentData
            dMonto = dMonto + (art.Total)
        Next

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = FormatCurrency(dMonto, 2)

        Dim dMontoPagado As Double
        Dim dMontoEfe As Double

        For Each Pag As Pago In CurrentDataPago
            If Pag.FormaPago = "Efectivo" Then
                dMontoEfe = dMontoEfe + Pag.MontoPagado
            End If
            dMontoPagado = dMontoPagado + Pag.MontoPagado
        Next

        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = dMontoPagado
        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = dMontoEfe
        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = dMonto - dMontoPagado
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dMontoPagado, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = FormatCurrency(dMontoEfe, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency(dMonto - dMontoPagado, 2)

        If ((dMonto - dMontoPagado) < 0) And (((dMontoPagado - dMontoEfe) <= (dMonto - dMontoPagado)) Or (dMontoPagado - dMontoEfe) = 0) Then
            'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = (dMonto - dMontoPagado) * -1
            DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMonto - dMontoPagado) * -1)
            Return FormatCurrency((dMonto - dMontoPagado), 2)
        Else
            If (dMontoPagado - dMonto) <= 0 Then
                'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = 0
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency(0, 2)
                Return FormatCurrency((dMonto - dMontoPagado), 2)
            Else
                'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = (dMontoPagado - dMonto)
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMontoPagado - dMonto), 2)
                Return FormatCurrency((dMonto - dMontoPagado), 2)
            End If
        End If


    End Function

    <DirectMethod()> _
    Public Sub actualizaMontosPagos()

        Session("Montos") = True

        Dim dMonto As Double = 0

        For Each art As Venta In CurrentData
            dMonto = dMonto + (art.Lista)
        Next

        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(0), TextField).Text = FormatCurrency(dMonto, 2)

        Dim dMontoPagado As Double
        Dim dMontoEfe As Double

        For Each Pag As Pago In CurrentDataPago
            If Pag.FormaPago = "Efectivo" Then
                dMontoEfe = dMontoEfe + Pag.MontoPagado
            End If
            dMontoPagado = dMontoPagado + Pag.MontoPagado
        Next

        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = dMontoPagado
        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = dMontoEfe
        'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = dMonto - dMontoPagado
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(2), TextField).Text = FormatCurrency(dMontoPagado, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(4), TextField).Text = FormatCurrency(dMontoEfe, 2)
        DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(6), TextField).Text = FormatCurrency(dMonto - dMontoPagado, 2)

        If ((dMonto - dMontoPagado) < 0) And (((dMontoPagado - dMontoEfe) <= (dMonto - dMontoPagado)) Or (dMontoPagado - dMontoEfe) = 0) Then
            'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = (dMonto - dMontoPagado) * -1
            DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMonto - dMontoPagado) * -1)
        Else
            If (dMontoPagado - dMonto) <= 0 Then
                'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = 0
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency(0, 2)
            Else
                'DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = (dMontoPagado - dMonto)
                DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text = FormatCurrency((dMontoPagado - dMonto), 2)
            End If
        End If


    End Sub

    Protected Sub Button2_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Button2.DirectClick


        With winClientes
            .Title = "Agregar Cliente"
            .Width = Unit.Pixel(450)
            .Height = Unit.Pixel(500)
            .Modal = True
            .Collapsible = True
            .Maximizable = True
            .Hidden = True
            .AutoRender = True
            .LazyMode = LazyMode.Instance
        End With

        Dim squery As String
        squery = "select Clientes,Socios from AdminStore where AdminStoreID=" & Session("IDSTORE")
        Dim dt As New DataTable
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringWEB)


        tfRFC.Disabled = False
        If cmbTipoCliente.Items.Count = 0 Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "No esta autorizado para dar de alta clientes"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            winClientes.Close()

            Button2.Disabled = True
            Exit Sub
        End If

        btnInsertClient.Show()
        btnFindClient.Hide()

        DirectCast(Ext.Net.ExtNet.GetCtl("btnInsertClient"), Ext.Net.Button).Show()
        DirectCast(Ext.Net.ExtNet.GetCtl("btnFindClient"), Ext.Net.Button).Hide()
        DirectCast(Ext.Net.ExtNet.GetCtl("Button9"), Ext.Net.Button).Hide()
        DirectCast(Ext.Net.ExtNet.GetCtl("Button13"), Ext.Net.Button).Hide()
        'PanelIzquierda.Show()

        DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Hide()
        DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Hide()


        'tfID.Disabled = False
        'tfNombre.Disabled = False
        'tfRFC.Disabled = False
        'tfCalleNumero.Disabled = False
        'tfColonia.Disabled = False
        'tfCP.Disabled = False
        'tfTelCasa.Disabled = False
        'tfTelOfc.Disabled = False
        'tfCorreo.Disabled = False
        'tfEstado.Disabled = False
        'tfDelMun.Disabled = False
        'tfIDRef.Disabled = False
        'cmbTipoCliente.Disabled = False

        tfRFC.Text = DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text

        DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text = ""

        'ClientForm.Title = "Agregar Cliente"
        GridCliente.Title = "Clientes encontrados"
        PanelCentro.Hide()
        winClientes.Show()

        cmbTipoCliente.SelectedItem.Index = 1

        Dim msgb As New Ext.Net.MessageBox

        msgb.Confirm("RFC No encontrado", "RFC no encontrado debera crear un nuevo cliente", New MessageBoxButtonsConfig() With { _
  .Yes = New MessageBoxButtonConfig() With { _
   .Handler = "CompanyX.DoYes()", _
   .Text = "OK" _
 }}).Show()


        'Dim mnconf As New Ext.Net.MessageBoxConfig
        'mnconf.Message = "RFC no encontrado deber crear un nuevo cliente"
        'mnconf.Title = "Error"

        'msgb.Configure(mnconf)

        'msgb.Show()

    End Sub

    Protected Sub Button4_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Button4.DirectClick

        With winClientes
            .Title = "Buscar Cliente"
            .Width = Unit.Pixel(1000)
            .Height = Unit.Pixel(500)
            .Modal = True
            .Collapsible = True
            .Maximizable = True
            .Hidden = True
            .AutoRender = True
            .LazyMode = LazyMode.Instance
        End With

        Dim squery As String
        squery = "select Clientes,Socios   from AdminStore where AdminStoreID=" & Session("IDSTORE")
        Dim dt As New DataTable
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringWEB)

        'If dt.Rows(0).Item("Clientes") = False Then
        '    cmbTipoCliente.Items.RemoveAt(1)
        'End If

        'If dt.Rows(0).Item("Socios") = False Then
        '    cmbTipoCliente.Items.RemoveAt(0)
        'End If

        DirectCast(Ext.Net.ExtNet.GetCtl("btnInsertClient"), Ext.Net.Button).Hide()
        DirectCast(Ext.Net.ExtNet.GetCtl("btnFindClient"), Ext.Net.Button).Show()
        DirectCast(Ext.Net.ExtNet.GetCtl("Button9"), Ext.Net.Button).Show()

        DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Hide()
        DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Show()
        DirectCast(Ext.Net.ExtNet.GetCtl("Button13"), Ext.Net.Button).Show()


        tfID.Disabled = True
        tfNombre.Disabled = False
        tfRFC.Disabled = True
        tfCalleNumero.Disabled = False
        tfColonia.Disabled = False
        tfCP.Disabled = False
        tfTelCasa.Disabled = False
        tfTelOfc.Disabled = False
        tfCorreo.Disabled = False
        tfEstado.Disabled = False
        tfDelMun.Disabled = False
        tfIDRef.Disabled = False
        cmbTipoCliente.Disabled = False

        'ClientForm.Title = "Busqueda de cliente"
        GridCliente.Title = "Clientes encontrados"

        'Dim sf As StringFilter = DirectCast(GridFilters1.Filters(1), StringFilter)
        'sf.SetValue("|||||")
        'sf.SetActive(True)

        'sf = New StringFilter
        'sf = DirectCast(GridFilters1.Filters(2), StringFilter)
        'sf.SetValue("|||||")
        'sf.SetActive(True)


        DirectCast(Ext.Net.ExtNet.GetCtl("GridCliente"), Ext.Net.GridPanel).Show()
        'GridCliente

        'PanelIzquierda.Show()
        PanelCentro.Show()

        squery = "select * from ge_regiones order by DES_REGION"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente", connstringWEB)
        tfColonia.Render()
        tfDelMun.Render()

        For Each row As DataRow In dt.Rows
            Dim item As New Ext.Net.ListItem
            item.Value = row("COD_REGION")
            item.Text = row("DES_REGION")
            actCod_REGIONestado.Items.Add(item)
        Next

        actCod_REGIONestado.Render()
        actCod_PROVINCIAmunicipio.Render()
        actCod_CIUDAD.Render()
        actCod_COMUNAcolonia.Render()
        'actCALLE.Render()
        'actNUMEXT.Render()
        'actNUMINT.Render()
        tfCP.Render()
        tfTelCasa.Render()
        tfTelOfc.Render()
        tfCorreo.Render()
        cmbTipoCliente.Render()
        actFCHNACIMIENTO.Render()


        winClientes.Show()



    End Sub

    Protected Sub Button9_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Button9.DirectClick

        setClientData()

    End Sub

    Public Sub setClientData()

        Dim txtOrigen As New Ext.Net.TextField
        Dim txtDestino As New Ext.Net.TextField

        If IsNothing(Session("CurrentClient")) Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Debe seleccionar un cliente para continuar."
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If

        tfCliente.Text = DirectCast(Session("CurrentClient"), Cliente).Id
        tfNombreCliente.Text = DirectCast(Session("CurrentClient"), Cliente).Nombre
        tfRFCCliente.Text = DirectCast(Session("CurrentClient"), Cliente).RFC
        tfTelCasaCliente.Text = DirectCast(Session("CurrentClient"), Cliente).TelCasa

        Dim sDireccion As String = ""

        sDireccion = sDireccion & DirectCast(Session("CurrentClient"), Cliente).CalleNumero & vbNewLine
        sDireccion = sDireccion & DirectCast(Session("CurrentClient"), Cliente).Colonia & vbNewLine
        sDireccion = sDireccion & DirectCast(Session("CurrentClient"), Cliente).DelMun & vbNewLine
        'sDireccion = sDireccion & DirectCast(Session("CurrentClient"), Cliente).TipoCliente & vbNewLine

        tfDireccion.Text = sDireccion

        RowSelectionModel1.ClearSelection()
        btnCleanClient.FireEvent("Click")
        winClientes.Hide()
        tfTipoCliente.Text = DirectCast(Session("CurrentClient"), Cliente).TipoCliente
        lblTipoCliente.Text = DirectCast(Session("CurrentClient"), Cliente).TipoCliente

        If Trim(DirectCast(Session("CurrentClient"), Cliente).TipoCliente) = "S" Then
            frmVentaMasiva.Show()
        Else
            frmVentaMasiva.Hide()
        End If

        Dim squery As String

        With DirectCast(Session("CurrentClient"), Cliente)

            squery = "set dateformat dmy insert into Log_Clientes (Accion,fecha,Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,IDRef,TipoCliente,CardCode,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,actCALLE,actNUMEXT,actNUMINT,actCod_COMUNAcolonia,actCod_REGIONestado,actCod_PROVINCIAmunicipio,actCod_CIUDAD,actCIUDAD,Referencia,Tienda,Usuario,IDCliente) values "
            squery = squery & "('Se selecciono el Cliente',GETDATE(),'" & Trim(.actNOMBRE) & " " & Trim(.actAPEPATERNO) & " " & Trim(.actAPEMATERNO) & "'" & _
            ",UPPER('" & .RFC & "')," & _
            "'" & Trim(.actCALLE) & " #EXT:" & Trim(.actNUMEXT) & If(Trim(.actNUMINT) <> "", " #INT:" & Trim(.actNUMINT), "") & "'," & _
            "'" & .Colonia & "'," & _
            "'" & .CP & "'," & _
            "'" & .TelCasa & "'," & _
            "'" & .TelOfc & "'," & _
            "'" & .Correo & "'," & _
            "'" & .Estado & "'," & _
            "'" & .DelMun & "'," & _
            "'" & .IDRef & "','C'" & _
            ",'','" & .actFCHNACIMIENTO & "','" & .actNOMBRE & "','" & .actAPEPATERNO & "','" & .actAPEMATERNO & "'," & _
            "'" & .actCALLE & "','" & .actNUMEXT & "','" & .actNUMINT & "','','12','','','','SIN REFERENCIA','" & Session("IDSTORE") & "'," & Session("AdminUserID") & ",'" & .Id & "')"

            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

        End With


        'tfColonia.Render()
        'tfDelMun.Render()
        'tfCP.Render()
        'txt.Text

    End Sub

    Protected Sub btnInsertClient_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnInsertClient.DirectClick
        Dim squery As String

        If DirectCast(Ext.Net.ExtNet.GetCtl("btnInsertClient"), Ext.Net.Button).Text = "Buscar" Then

        Else

            If actNOMBRE.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo nombre es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actAPEPATERNO.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo A. Paterno es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actAPEMATERNO.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo A. Materno es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            tfNombre.Text = Trim(actNOMBRE.Text) & " " & Trim(actAPEPATERNO.Text) & " " & Trim(actAPEMATERNO.Text)

            If tfRFC.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo rfc es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actCALLE.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Calle es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If actNOMBRE.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo nombre es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If


            If actCod_REGIONestado.SelectedItem.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Estado es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            If tfDelMun.Text = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El campo Del/Mun es obligatorio"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            'If Trim(cmbTipoCliente.SelectedItem.Value) = "" Then

            '    Dim msg As New Ext.Net.Notification
            '    Dim nconf As New Ext.Net.NotificationConfig
            '    nconf.IconCls = "icon-exclamation"
            '    nconf.Html = "Debe seleccionar un tipo de cliente"
            '    nconf.Title = "Error"
            '    msg.Configure(nconf)
            '    msg.Show()

            '    Exit Sub

            'End If


            Dim dt As New DataTable

            squery = "SELECT COUNT(*) FROM CLIENTES WHERE RFC='" & tfRFC.Text & "'"
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente", connstringWEB)

            If dt.Rows.Item(0).Item(0) <> 0 Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Ya existe un cliente con el RFC " & tfRFC.Text
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            Session("oWSData") = New WSData
            'Session("oWSData").LlenaDatosCliente(Session("CurrentClient"))

            Dim vetado As Boolean = True
            Dim mensaje As String
            Dim RFC As String
            RFC = tfRFC.Text
            'mensaje = wsRFC(RFC, vetado)
            'mensaje.ToUpper()
            vetado = False

            If vetado = False And mensaje = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.Html = "El RFC " & tfRFC.Text & " a sido validado de forma exitosa"
                nconf.Title = "Validacion RFC"
                msg.Configure(nconf)
                msg.Show()

            ElseIf mensaje <> "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.Html = mensaje
                nconf.Title = "Validacion RFC"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            ElseIf vetado = True Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.Html = "El cliente seleccionado esta vetado"
                nconf.Title = "Validacion RFC"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            End If

            squery = "set dateformat dmy Insert into clientes (Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,IDRef,TipoCliente,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,actCALLE,actNUMEXT,actNUMINT,actCIUDAD,actCod_REGIONestado,actCod_PROVINCIAmunicipio,actCod_CIUDAD,actCod_COMUNAcolonia) values (" & vbNewLine
            squery = squery & "'" & Trim(actNOMBRE.Text) & " " & Trim(actAPEPATERNO.Text) & " " & Trim(actAPEMATERNO.Text) & "',"
            squery = squery & "UPPER('" & tfRFC.Text & "'),"
            squery = squery & "'" & Trim(actCALLE.Text) & " #EXT:" & Trim(actNUMEXT.Text) & If(Trim(actNUMINT.Text) <> "", " #INT:" & Trim(actNUMINT.Text), "") & "',"
            squery = squery & "'" & tfColonia.Text & "',"
            squery = squery & "'" & tfCP.Text & "',"
            squery = squery & "'" & tfTelCasa.Text & "',"
            squery = squery & "'" & tfTelOfc.Text & "',"
            squery = squery & "'" & tfCorreo.Text & "',"
            squery = squery & "'" & actCod_REGIONestado.SelectedItem.Text & "',"
            squery = squery & "'" & tfDelMun.Text & "',"
            squery = squery & "'" & tfIDRef.Text & "','" & "C" & "','" & actFCHNACIMIENTO.Value.ToString.Split(" ")(0) & "','" & actNOMBRE.Text & "','" & actAPEPATERNO.Text & "','" & actAPEMATERNO.Text & "','" & actCALLE.Text & "','" & actNUMEXT.Text & "','" & actNUMINT.Text & "'"
            squery = squery & ",'" & actCod_CIUDAD.SelectedItem.Text & "'"
            squery = squery & ",'" & actCod_REGIONestado.SelectedItem.Value & "'"
            squery = squery & ",'" & actCod_PROVINCIAmunicipio.SelectedItem.Value & "'"
            squery = squery & ",'" & actCod_CIUDAD.SelectedItem.Value & "'"
            squery = squery & ",'" & actCod_COMUNAcolonia.SelectedItem.Value & "'"
            squery = squery & ")"

            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

            'If IsNothing(DirectCast(Session("CurrentClient"), Cliente)) = True Then
            squery = "select top 1 * from clientes where Nombre='" & tfNombre.Text & "' and RFC='" & tfRFC.Text & "' and IDRef='" & tfIDRef.Text & "' order by ID desc"
            'Dim dt As New DataTable
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente", connstringWEB)
            'Session("CurrentClient") = New Cliente(dt.Rows.Item(0).Item("Id"), dt.Rows.Item(0).Item("Nombre"), dt.Rows.Item(0).Item("RFC"), dt.Rows.Item(0).Item("CalleNumero"), dt.Rows.Item(0).Item("Colonia"), dt.Rows.Item(0).Item("CP"), dt.Rows.Item(0).Item("TelCasa"), dt.Rows.Item(0).Item("TelOfc"), dt.Rows.Item(0).Item("Correo"), dt.Rows.Item(0).Item("Estado"), dt.Rows.Item(0).Item("DelMun"), dt.Rows.Item(0).Item("IDRef"), dt.Rows.Item(0).Item("TipoCliente"), dt.Rows.Item(0).Item("actFCHNACIMIENTO"))
            Session("CurrentClient") = New Cliente(dt.Rows.Item(0).Item("ID"), dt.Rows.Item(0).Item("Nombre"), dt.Rows.Item(0).Item("RFC"), dt.Rows.Item(0).Item("CalleNumero"), dt.Rows.Item(0).Item("Colonia"), dt.Rows.Item(0).Item("CP"), dt.Rows.Item(0).Item("TelCasa"), dt.Rows.Item(0).Item("TelOfc"), dt.Rows.Item(0).Item("Correo"), dt.Rows.Item(0).Item("Estado"), dt.Rows.Item(0).Item("DelMun"), dt.Rows.Item(0).Item("IDRef"), dt.Rows.Item(0).Item("TipoCliente"), dt.Rows.Item(0).Item("actFCHNACIMIENTO"), dt.Rows.Item(0).Item("actNOMBRE"), dt.Rows.Item(0).Item("actAPEPATERNO"), dt.Rows.Item(0).Item("actAPEMATERNO"), dt.Rows.Item(0).Item("actCALLE"), dt.Rows.Item(0).Item("actNUMEXT"), dt.Rows.Item(0).Item("actNUMINT"), dt.Rows.Item(0).Item("actCIUDAD"), dt.Rows.Item(0).Item("actCod_REGIONestado"), dt.Rows.Item(0).Item("actCod_PROVINCIAmunicipio"), dt.Rows.Item(0).Item("actCod_CIUDAD"), dt.Rows.Item(0).Item("actCod_COMUNAcolonia"))
            'Else
            'End If

            With Session("CurrentClient")

                squery = squery & "set dateformat dmy insert into Log_Clientes (Accion,fecha,Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,IDRef,TipoCliente,CardCode,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,actCALLE,actNUMEXT,actNUMINT,actCod_COMUNAcolonia,actCod_REGIONestado,actCod_PROVINCIAmunicipio,actCod_CIUDAD,actCIUDAD,Referencia,Tienda,Usuario,IDCliente) values "
                squery = squery & "('Se creo el Cliente',GETDATE(),'" & Trim(.actNOMBRE) & " " & Trim(.actAPEPATERNO) & " " & Trim(.actAPEMATERNO) & "'" & _
                ",UPPER('" & .RFC & "')," & _
                "'" & Trim(.actCALLE) & " #EXT:" & Trim(.actNUMEXT) & If(Trim(.actNUMINT) <> "", " #INT:" & Trim(.actNUMINT), "") & "'," & _
                "'" & .Colonia & "'," & _
                "'" & .CP & "'," & _
                "'" & .TelCasa & "'," & _
                "'" & .TelOfc & "'," & _
                "'" & .Correo & "'," & _
                "'" & .Estado & "'," & _
                "'" & .DelMun & "'," & _
                "'" & .IDRef & "','C'" & _
                ",'','" & .actFCHNACIMIENTO & "','" & .actNOMBRE & "','" & .actAPEPATERNO & "','" & .actAPEMATERNO & "'," & _
                "'" & .actCALLE & "','" & .actNUMEXT & "','" & .actNUMINT & "','','12','','','','SIN REFERENCIA','" & Session("IDSTORE") & "'," & Session("AdminUserID") & "," & .Id & ")"

            End With

            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

            Button9.FireEvent("Click")

        End If


    End Sub

    <DirectMethod()> _
    Public Sub ChangeABOX()


        If esCombo.Value = False Then

            Exit Sub

        End If


        Dim dt As New DataTable
        Dim squery As String

        squery = "select ItemName,itemcode from oitm where itemcode= (select cast(U_juego as varchar) from OITM where ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "')"

        oDB.ConectaDBConnString(connstringSAP)
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Combo", connstringSAP)

        Dim pArtCombo As String = dt.Rows.Item(0).Item(0)
        Dim pArtCode As String = dt.Rows.Item(0).Item(1)


        If AlmJuego.SelectedItem.Value = "T" Then

            squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & pArtCode & "') and Almacen=" & Session("IDSTORE")
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)

            Dim ctemp As Double

            If dt.Rows.Count = 0 Then
                ctemp = 0
            Else
                ctemp = dt.Rows.Item(0).Item("Cantidad")
            End If

            For Each oVenta In CurrentData

                If oVenta.CantidadTienda > 0 Then

                    If oVenta.Linea = pArtCode Then
                        ctemp = ctemp - CDbl(oVenta.CantidadTienda)
                    End If

                End If

            Next

            CantidadBox.Text = ctemp

        End If


        If AlmJuego.SelectedItem.Value = "B" Then

            squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & pArtCode & "') and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "EB", connstringWEB)

            Dim ctemp As Double

            If dt.Rows.Count = 0 Then
                ctemp = 0
            Else
                ctemp = dt.Rows.Item(0).Item("Cantidad")
            End If


            For Each oVenta In CurrentData

                If oVenta.CantidadBodega > 0 Then

                    If oVenta.Linea = pArtCode Then
                        ctemp = ctemp - CDbl(oVenta.CantidadBodega)
                    End If

                End If

            Next

            CantidadBox.Text = ctemp


        End If


    End Sub

    <DirectMethod()> _
    Public Sub ChangePrice()

        Dim dt As New DataTable
        Dim squery As String


        If DirectCast(Ext.Net.ExtNet.GetCtl("Lista"), Ext.Net.ComboBox).SelectedItem.Value = "" Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Seleccione una lista de precios"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()


        Else

            DirectCast(Ext.Net.ExtNet.GetCtl("taObservaciones"), Ext.Net.Hidden).Text = DirectCast(Ext.Net.ExtNet.GetCtl("Lista"), Ext.Net.ComboBox).SelectedItem.Text

            squery = "select isnull(price,0) as price from itm1 where itemcode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "' and pricelist='" & Lista.SelectedItem.Value & "'"
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)
            If dt.Rows.Count <> 0 Then

                'Dim dPrecioUnitario As Double = dt.Rows.Item(0).Item("price") - (dt.Rows.Item(0).Item("price") * (Session("IVA") / 100))
                Dim dPrecioUnitario As Double = dt.Rows.Item(0).Item("price") / (1 + (Session("IVA") / 100))

                Dim desc As Double

                If Descuento.Text = "" Then
                    desc = 0
                Else
                    desc = Descuento.Text
                End If

                Dim dPrecioConDescuento As Double = dPrecioUnitario * (1 - (desc / 100))
                'Dim dIvaUnitario = (dt.Rows.Item(0).Item("price") * (Session("IVA") / 100)) * (1 - (desc / 100))
                'Dim dIvaUnitario = dt.Rows.Item(0).Item("price") - dPrecioUnitario
                Dim dIvaUnitario = (dt.Rows.Item(0).Item("price") * (1 - (desc / 100)) - dPrecioConDescuento)

                Dim dCantidad As Double
                Dim cantidad As Double
                Dim cTienda As Double
                Dim cBodega As Double

                DirectCast(Ext.Net.ExtNet.GetCtl("PrecioUnitario"), Ext.Net.TextField).Text = FormatCurrency(dPrecioUnitario, 2)

                If CantidadTienda.Text = "" Then
                    cTienda = 0
                Else
                    cTienda = CantidadTienda.Text
                End If

                If CantidadBodega.Text = "" Then
                    cBodega = 0
                Else
                    cBodega = CantidadBodega.Text
                End If

                cantidad = cTienda + cBodega

                Dim dIVATotal As Double = dIvaUnitario * cantidad

                IVA.Text = FormatCurrency(dIVATotal, 2)

                Dim dMonto As Double = (dPrecioUnitario * cantidad) + dIVATotal

                Monto.Text = FormatCurrency(dMonto, 2)

                Dim dTotal = (dPrecioConDescuento * cantidad) + dIVATotal

                Total.Text = FormatCurrency(dTotal, 2)

                squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "') and Almacen=" & Session("IDSTORE")
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)

                Dim ctemp As Double

                If dt.Rows.Count = 0 Then
                    ctemp = 0
                Else
                    ctemp = dt.Rows.Item(0).Item("Cantidad")
                End If


                If cTienda > ctemp Then

                    CantidadTienda.Text = ""

                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "Existencia insuficiente"
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()

                End If

                squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "') and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "EB", connstringWEB)


                If dt.Rows.Count = 0 Then
                    ctemp = 0
                Else
                    ctemp = dt.Rows.Item(0).Item("Cantidad")
                End If

                If cBodega > ctemp Then

                    'CantidadBodega.Text = ""

                    'Dim msg As New Ext.Net.Notification
                    'Dim nconf As New Ext.Net.NotificationConfig
                    'nconf.IconCls = "icon-exclamation"
                    'nconf.Html = "Existencia insuficiente"
                    'nconf.Title = "Error"
                    'msg.Configure(nconf)
                    'msg.Show()

                End If

                Dim value As String = DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value

                squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & value & "') and Almacen=" & Session("IDSTORE")
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)


                If dt.Rows.Count = 0 Then
                    ctemp = 0
                Else
                    ctemp = dt.Rows.Item(0).Item("Cantidad")
                End If

                For Each oVenta In CurrentData

                    If oVenta.CantidadTienda > 0 Then

                        If oVenta.Linea = value Then
                            ctemp = ctemp - CDbl(oVenta.CantidadTienda)
                        End If

                    End If

                Next

                ExistenciaTienda.Text = ctemp

                squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & value & "') and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "EB", connstringWEB)

                If dt.Rows.Count = 0 Then
                    ctemp = 0
                Else
                    ctemp = dt.Rows.Item(0).Item("Cantidad")
                End If

                For Each oVenta In CurrentData

                    If oVenta.CantidadBodega > 0 Then

                        If oVenta.Linea = value Then
                            ctemp = ctemp - CDbl(oVenta.CantidadBodega)
                        End If

                    End If

                Next

                ExistenciaBodega.Text = ctemp

                AplicaCombo()
                ChangeABOX()

            End If

        End If

    End Sub

    <DirectMethod()> _
    Public Sub CambiaArticulo()

        Dim Value As String

        Value = DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Value

        If Value = "" Then
            Exit Sub
        End If

        If Value <> "" Then

            Dim dt As New DataTable
            Dim squery As String

            'squery = "select distinct ItmsGrpCod as code ,(select ItmsGrpNam from OITB where oitb.ItmsGrpCod=oitm.ItmsGrpCod)  as name from OITM where QryGroup" & Value & "='Y'"
            squery = "select distinct U_BXP_MARCA as code ,U_BXP_MARCA  as name from OITM where QryGroup" & Value & "='Y' and frozenFor='N' order by U_BXP_MARCA"

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)

            If dt.Rows.Count <> 0 And Value <> "" Then

                For Each drow As DataRow In dt.Rows

                    cbLinea.Items.Add(New Ext.Net.ListItem(drow("Name").ToString(), drow("Code")))

                Next

                cbLinea.Text = ""
                tfMedida.Text = ""
                Modelo.Text = ""
                cbLinea.Render()
                tfMedida.Render()
                Modelo.Render()
                CantidadesTienda.Render()
                CantidadesBodega.Render()
                Combo.Render()
                AJuego.Render()
                Lista.Render()
                'PrecioUnitario.Render()
                'IVA.Render()
                PUIVA.Render()
                Monto.Render()
                Descuento.Render()
                Total.Render()
            Else

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "No existen Lineas para esta categoria de articulo '" & DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Text & "'"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                UserForm.Reset()

            End If

        End If

        'CheckItem()


    End Sub

    <DirectMethod()> _
    Public Sub CambiaLinea()

        Dim Value As String

        Value = DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Value

        If Value = "" Then
            Exit Sub
        End If

        If Value <> "" Then

            Dim dt As New DataTable
            Dim squery As String

            'squery = "select distinct U_BXP_MEDIDA as code, U_BXP_MEDIDA as name from OITM where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Value & "='Y' and ItmsGrpCod='" & Value & "'"
            squery = "select distinct U_BXP_MEDIDA as code, U_BXP_MEDIDA as name from OITM where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Value & "='Y' and U_BXP_MARCA='" & Value & "'"

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)

            If dt.Rows.Count <> 0 And Value <> "" Then

                For Each drow As DataRow In dt.Rows

                    tfMedida.Items.Add(New Ext.Net.ListItem(drow("Name").ToString(), drow("Code")))

                Next

                tfMedida.Text = ""
                Modelo.Text = ""
                tfMedida.Render()
                Modelo.Render()
                CantidadesTienda.Render()
                CantidadesBodega.Render()
                Combo.Render()
                AJuego.Render()
                Lista.Render()
                'PrecioUnitario.Render()
                'IVA.Render()
                PUIVA.Render()
                Monto.Render()
                Descuento.Render()
                Total.Render()


            Else

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "No existen Medidas para esta Linea de articulo '" & DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Text & "'"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                UserForm.Reset()

            End If

        End If

        'CheckItem()


    End Sub

    <DirectMethod()> _
    Public Sub CambiaMedida()

        Dim Value As String

        Value = DirectCast(Ext.Net.ExtNet.GetCtl("tfMedida"), ComboBox).SelectedItem.Value

        If Value = "" Then
            Exit Sub
        End If

        If Value <> "" Then

            Dim dt As New DataTable
            Dim squery As String

            'squery = "select distinct ItemCode as code, ItemName as name from OITM where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Value & "='Y' and ItmsGrpCod=" & DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Value & " and U_BXP_MEDIDA='" & Value & "'"
            squery = "select distinct ItemCode as code, ItemName as name from OITM where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Value & "='Y' and U_BXP_MARCA='" & DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Value & "' and U_BXP_MEDIDA='" & Value & "' ORDER BY ITEMNAME"

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)

            If dt.Rows.Count <> 0 And Value <> "" Then

                For Each drow As DataRow In dt.Rows
                    Modelo.Items.Add(New Ext.Net.ListItem(drow("Name").ToString(), drow("Code")))
                Next

                Modelo.Text = ""
                Modelo.Render()
                CantidadesTienda.Render()
                CantidadesBodega.Render()
                Combo.Render()
                AJuego.Render()
                Lista.Render()
                'PrecioUnitario.Render()
                'IVA.Render()
                PUIVA.Render()
                Monto.Render()
                Descuento.Render()
                Total.Render()


            Else

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "No existen Modelos para esta Medida '" & DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Text & "'"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                UserForm.Reset()

            End If

        End If

        'CheckItem()


    End Sub

    <DirectMethod()> _
    Public Sub CambiaModelo()

        Dim Value As String

        Value = DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value

        If Value = "" Then
            Exit Sub
        End If


        Dim dt As New DataTable
        Dim squery As String

        'Incluye lista default
        'squery = "select ListID listnum,isnull(ListName,'') listname from storelist where AdminStoreID='" & Session("IDSTORE") & "' and listid in (select ListID from StoreList where AdminStoreID='" & Session("IDSTORE") & "') union select defaultlist,(select top 1 listname from StoreList where listid=defaultlist) from AdminStore where AdminStoreID=" & Session("IDSTORE")

        ''No incluye lista default
        ''squery = "select ListID listnum,isnull(ListName,'') listname from storelist where AdminStoreID='" & Session("IDSTORE") & "' and listid in (select ListID from StoreList where AdminStoreID='" & Session("IDSTORE") & "')"

        'oDB.ConectaDBConnString(connstringSAP)
        'dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Listas", connstringWEB)

        'Dim cuenta As Integer = 0

        'For Each Drow As DataRow In dt.Rows

        '    Lista.Items.Add(New Ext.Net.ListItem(Drow("ListName"), Drow("ListNum")))

        '    If cuenta = 0 Then
        '        Lista.SelectedIndex = 0
        '    End If

        '    cuenta = cuenta + 1

        'Next

        squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & Value & "') and Almacen=" & Session("IDSTORE")
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)

        Dim ctemp As Double

        If dt.Rows.Count = 0 Then
            ctemp = 0
        Else
            ctemp = dt.Rows.Item(0).Item("Cantidad")
        End If

        For Each oVenta In CurrentData

            If oVenta.CantidadTienda > 0 Then

                If oVenta.Linea = Value Then
                    ctemp = ctemp - CDbl(oVenta.CantidadTienda)
                End If

            End If

        Next


        ExistenciaTienda.Text = ctemp



        squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & Value & "') and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "EB", connstringWEB)

        If dt.Rows.Count = 0 Then
            ctemp = 0
        Else
            ctemp = dt.Rows.Item(0).Item("Cantidad")
        End If

        For Each oVenta In CurrentData

            If oVenta.CantidadBodega > 0 Then

                If oVenta.Linea = Value Then
                    ctemp = ctemp - CDbl(oVenta.CantidadBodega)
                End If

            End If

        Next

        ExistenciaBodega.Text = ctemp

        CantidadesTienda.Render()
        CantidadesBodega.Render()
        Combo.Render()
        AJuego.Render()

        Lista.Render()
        PUIVA.Render()
        Monto.Render()
        Descuento.Render()
        Total.Render()

        ChangePrice()

    End Sub

    <DirectMethod()> _
    Public Sub ObtenerArticulos()

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct ItemCode as code, ItemName as name from OITM where (QryGroup" & ConfigurationManager.AppSettings("PropiedadVenta") & "='Y' or QryGroup" & ConfigurationManager.AppSettings("PropiedadDescontinuados") & "='Y')"

        If tfArticulo.SelectedItem.Index <> -1 Then
            squery = squery & " and QryGroup" & tfArticulo.SelectedItem.Value & "='Y'"
        End If


        If cbLinea.SelectedItem.Index <> -1 Then
            squery = squery & " and U_BXP_MARCA='" & cbLinea.SelectedItem.Value & "'"
        End If

        If tfMedida.SelectedItem.Index <> -1 Then
            squery = squery & " and U_BXP_MEDIDA='" & tfMedida.SelectedItem.Value & "'"
        End If

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)

        If dt.Rows.Count <> 0 Then

            For Each drow As DataRow In dt.Rows

                Modelo.Items.Add(New Ext.Net.ListItem(drow("Name").ToString(), drow("Code")))

            Next

            'Modelo.Text = ""
            'Modelo.Render()
            'Combo.Render()
            'Lista.Render()
            'CantidadesTienda.Render()
            'CantidadesBodega.Render()
            'PrecioUnitario.Render()
            'IVA.Render()
            'Monto.Render()
            'Descuento.Render()
            'Total.Render()


        Else

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "No existen Modelos para esta Medida '" & DirectCast(Ext.Net.ExtNet.GetCtl("cbLinea"), ComboBox).SelectedItem.Text & "'"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            UserForm.Reset()

        End If



    End Sub

    <DirectMethod()> _
    Public Sub AgregaLineaVenta()

        Dim Value As String
        Dim dt As New DataTable
        Dim squery As String
        Dim Id As String
        Dim pArticulo As String
        Dim pLinea As String
        Dim pMedida As String
        Dim pModelo As String
        Dim pCantidadTienda As Double
        Dim pCantidadBodega As Double
        Dim pLista As String
        Dim pPrecioUnitario As Double
        Dim pIVA As Double
        Dim pMonto As Double
        Dim pDescuento As Double
        Dim pTotal As Double

        pArticulo = DirectCast(Ext.Net.ExtNet.GetCtl("tfArticulo"), ComboBox).SelectedItem.Text
        pModelo = DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Text

        If CantidadTienda.Text = "" Then
            pCantidadTienda = 0
        Else
            pCantidadTienda = CantidadTienda.Text
        End If

        If CantidadBodega.Text = "" Then

            pCantidadBodega = 0
        Else

            pCantidadBodega = CantidadBodega.Text

        End If


        If pCantidadBodega <= 0 And pCantidadTienda <= 0 Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "La cantidad a vender debe ser mayor a 0"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If


        If pCantidadBodega > 0 And pCantidadTienda > 0 Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Solo puede seleccionar una cantidad y sea en Cantidad Bodega o Cantidad Tienda"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If

        If esCombo.Value = True Then

            If AlmJuego.SelectedItem.Value = "" Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Debe seleccionar el origen del box incluido en el combo"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                Exit Sub

            Else

                squery = "select (select ItmsGrpNam from OITB where oitb.ItmsGrpCod=oitm.ItmsGrpCod) as [Articulo],ItemName,itemcode,(select Price from ITM1 where ITM1.ItemCode=oitm.ItemCode and PriceList=(select listnum from OPLN where ListNum='" & ConfigurationManager.AppSettings("ListaBox") & "')) as Precio,(select U_BoxesXJuego from OITM A where A.ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "') as [Cantidad] from oitm where itemcode= (select cast(U_juego as varchar) from OITM where ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "')"

                oDB.ConectaDBConnString(connstringSAP)
                dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Combo", connstringSAP)

                ChangeABOX()

                If AlmJuego.SelectedItem.Value = "T" Then

                    If (dt.Rows.Item(0).Item("Cantidad") * (pCantidadTienda + pCantidadBodega)) > CDbl(CantidadBox.Text) Then

                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "No hay existencia suficiente en la tienda para surtir el box"
                        nconf.Title = "Error"
                        msg.Configure(nconf)
                        msg.Show()

                        Exit Sub

                    End If


                End If

            End If


        End If



        pLista = DirectCast(Ext.Net.ExtNet.GetCtl("Lista"), ComboBox).SelectedItem.Text

        'Precio unitario / IVA / Monto
        squery = "select isnull(price,0) as price from itm1 where itemcode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "' and pricelist='" & Lista.SelectedItem.Value & "'"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)

        Dim dPrecioConDescuento As Double

        If dt.Rows.Count <> 0 Then
            DirectCast(Ext.Net.ExtNet.GetCtl("taObservaciones"), Ext.Net.Hidden).Text = DirectCast(Ext.Net.ExtNet.GetCtl("Lista"), Ext.Net.ComboBox).SelectedItem.Text

            squery = "select isnull(price,0) as price from itm1 where itemcode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "' and pricelist='" & Lista.SelectedItem.Value & "'"
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "R", connstringSAP)
            If dt.Rows.Count <> 0 Then

                'Dim dPrecioUnitario As Double = dt.Rows.Item(0).Item("price") - (dt.Rows.Item(0).Item("price") * (Session("IVA") / 100))
                Dim dPrecioUnitario As Double = dt.Rows.Item(0).Item("price") / (1 + (Session("IVA") / 100))

                pPrecioUnitario = dPrecioUnitario

                Dim desc As Double

                If Descuento.Text = "" Then
                    desc = 0
                Else
                    desc = Descuento.Text
                End If

                dPrecioConDescuento = dPrecioUnitario * (1 - (desc / 100))
                'Dim dIvaUnitario = (dt.Rows.Item(0).Item("price") * (Session("IVA") / 100)) * (1 - (desc / 100))
                Dim dIvaUnitario = (dt.Rows.Item(0).Item("price") * (1 - (desc / 100)) - dPrecioConDescuento)

                Dim dCantidad As Double
                Dim cantidad As Double
                Dim cTienda As Double
                Dim cBodega As Double

                DirectCast(Ext.Net.ExtNet.GetCtl("PrecioUnitario"), Ext.Net.TextField).Text = FormatCurrency(dPrecioUnitario, 2)

                If CantidadTienda.Text = "" Then
                    cTienda = 0
                Else
                    cTienda = CantidadTienda.Text
                End If

                If CantidadBodega.Text = "" Then
                    cBodega = 0
                Else
                    cBodega = CantidadBodega.Text
                End If

                cantidad = cTienda + cBodega

                Dim dIVATotal As Double = dIvaUnitario * cantidad

                pIVA = dIVATotal

                IVA.Text = FormatCurrency(dIVATotal, 2)

                Dim dMonto As Double = dPrecioUnitario * cantidad + dIVATotal

                pMonto = dMonto

                Monto.Text = FormatCurrency(dMonto, 2)

                Dim dTotal = dPrecioConDescuento * cantidad + dIVATotal

                Total.Text = FormatCurrency(dTotal, 2)

                pTotal = dTotal

                pDescuento = (pMonto - pIVA) * (desc / 100)

            End If

        End If

        If Total.Text = "" Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Debe seleccioar una Lista de precios"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub


        End If

        pTotal = Total.Text

        If pModelo = "" Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Debe seleccioar un modelo"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If

        If pCantidadBodega <= 0 And pCantidadTienda <= 0 Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "La cantidad a vender debe ser mayor a 0"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If

        If pLista = "" Then

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Debe seleccionar una lista"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            Exit Sub

        End If


        If pArticulo = "" Then

            squery = "select (select ItmsGrpNam from OITB where oitb.ItmsGrpCod=oitm.ItmsGrpCod) as [Articulo],ItemName,itemcode,(select Price from ITM1 where ITM1.ItemCode=oitm.ItemCode and PriceList=(select listnum from OPLN where ListNum='" & ConfigurationManager.AppSettings("ListaBox") & "')) as Precio,(select U_BoxesXJuego from OITM A where A.ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "') as [Cantidad] from oitm where itemcode= (select cast(U_juego as varchar) from OITM where ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "')"

            oDB.ConectaDBConnString(connstringSAP)
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Combo", connstringSAP)

        End If

        Dim AlmacenBox As String

        If esCombo.Value = "False" Then
            Session("TestPersons").Add(New Venta(Session("TestPersons").count + 1, pArticulo, DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value, pMedida, pModelo, pCantidadTienda, pCantidadBodega, pLista, pPrecioUnitario, pIVA, pMonto, pDescuento, pTotal, "", "", "", dPrecioConDescuento * (pCantidadTienda + pCantidadBodega)))
        Else
            Session("TestPersons").Add(New Venta(Session("TestPersons").count + 1, pArticulo, DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value, pMedida, pModelo, pCantidadTienda, pCantidadBodega, pLista, pPrecioUnitario, pIVA, pMonto, pDescuento, pTotal, "(JGO)", AlmacenBox, "", dPrecioConDescuento * (pCantidadTienda + pCantidadBodega)))
        End If



        If esCombo.Value = "True" Then

            squery = "select (select ItmsGrpNam from OITB where oitb.ItmsGrpCod=oitm.ItmsGrpCod) as [Articulo],ItemName,itemcode,(select Price / " & (1 + (Session("IVA") / 100)) & " from ITM1 where ITM1.ItemCode=oitm.ItemCode and PriceList=(select listnum from OPLN where ListNum='" & ConfigurationManager.AppSettings("ListaBox") & "')) as Precio,(select U_BoxesXJuego from OITM A where A.ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "') as [Cantidad] from oitm where itemcode= (select cast(U_juego as varchar) from OITM where ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "')"

            oDB.ConectaDBConnString(connstringSAP)
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Combo", connstringSAP)

            AlmacenBox = ""

            'If Combo.Value = "False" Then

            '  Session("TestPersons").Add(New Venta(Session("TestPersons").count + 1, dt.Rows.Item(0).Item("Articulo"), dt.Rows.Item(0).Item("itemcode"), pMedida, dt.Rows.Item(0).Item("ItemName"), pCantidadTienda * dt.Rows.Item(0).Item("Cantidad"), CDbl(pCantidadBodega) * dt.Rows.Item(0).Item("Cantidad"), pLista, dt.Rows.Item(0).Item("Precio"), 0, dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), 0, "", "", "", ""))

            ' Else

            Dim cantidad As Double = (pCantidadBodega + pCantidadTienda) * dt.Rows.Item(0).Item("Cantidad")

            If AlmJuego.SelectedItem.Value = "B" Then
                Session("TestPersons").Add(New Venta(Session("TestPersons").count + 1, dt.Rows.Item(0).Item("Articulo"), dt.Rows.Item(0).Item("itemcode"), pMedida, dt.Rows.Item(0).Item("ItemName"), 0, cantidad, pLista, dt.Rows.Item(0).Item("Precio"), 0, dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), 0, "(JGO)", AlmacenBox, "", 0))
            Else
                Session("TestPersons").Add(New Venta(Session("TestPersons").count + 1, dt.Rows.Item(0).Item("Articulo"), dt.Rows.Item(0).Item("itemcode"), pMedida, dt.Rows.Item(0).Item("ItemName"), cantidad, 0, pLista, dt.Rows.Item(0).Item("Precio"), 0, dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), dt.Rows.Item(0).Item("Precio") * (CDbl(pCantidadBodega) + CDbl(pCantidadTienda)) * dt.Rows.Item(0).Item("Cantidad"), 0, "(JGO)", AlmacenBox, "", 0))
            End If

            'End If

        End If

        If Boolean.TryParse(UseConfirmation.Text, False) Then
            Me.Store1.SuspendScripting()
            Me.Store1.ResumeScripting()
        End If

        Store1.CommitChanges()

        Response.Redirect("Facturacion.aspx")


    End Sub


    <DirectMethod()> _
    Public Sub AplicaCombo()

        Dim Value As String

        Value = DirectCast(Ext.Net.ExtNet.GetCtl("esCombo"), Checkbox).Value

        If Value = "" Then
            Exit Sub
        End If

        If Value = "False" Then
            Lista.Items.Clear()
            cargaListas(True)
            artCombo.Text = ""
            CantidadBox.Text = ""
            AlmJuego.Disabled = True
            Exit Sub
        End If

        Dim dt As New DataTable
        Dim squery As String

        squery = "select ItemName,itemcode from oitm where itemcode= (select cast(U_juego as varchar) from OITM where ItemCode='" & DirectCast(Ext.Net.ExtNet.GetCtl("Modelo"), ComboBox).SelectedItem.Value & "')"

        oDB.ConectaDBConnString(connstringSAP)
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Combo", connstringSAP)

        If dt.Rows.Count = 0 Then

            esCombo.Checked = False
            artCombo.Text = ""

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "No se encontro informacion del combo"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()
            Exit Sub

        End If

        If dt.Rows.Item(0).Item(0) = "" Then

            esCombo.Checked = False
            artCombo.Text = ""

            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "El articulo del combo ya no existe en la base de datos"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()
            Exit Sub

        End If

        Dim pArtCombo As String = dt.Rows.Item(0).Item(0)
        Dim pArtCode As String = dt.Rows.Item(0).Item(1)
        Dim ctienda As Double
        Dim cbodega As Double

        If CantidadTienda.Text = "" Then
            ctienda = 0
        Else
            ctienda = CantidadTienda.Text
        End If

        If CantidadBodega.Text = "" Then
            cbodega = 0
        Else
            cbodega = CantidadBodega.Text
        End If

        If AlmJuego.Disabled = True Then

            'CantidadBox.Text = ""
            AlmJuego.Disabled = False
            'AJuego.Render()

        End If




        If ctienda > 0 Then

            squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & pArtCode & "') and Almacen=" & Session("IDSTORE")
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)

            Dim ctemp As Double

            If dt.Rows.Count = 0 Then
                ctemp = 0
            Else
                ctemp = dt.Rows.Item(0).Item("Cantidad")
            End If

            For Each oVenta In CurrentData

                If oVenta.CantidadBodega > 0 Then

                    If oVenta.Linea = pArtCode Then
                        ctemp = ctemp - CDbl(oVenta.CantidadTienda)
                    End If

                End If

            Next

            'If ctemp < ctienda Then

            '    esCombo.Checked = False
            '    artCombo.Text = ""

            '    Dim msg As New Ext.Net.Notification
            '    Dim nconf As New Ext.Net.NotificationConfig
            '    nconf.IconCls = "icon-exclamation"
            '    nconf.Html = "El articulo del combo '" & pArtCombo & "' no tiene existencias suficientes en la tienda"
            '    nconf.Title = "Error"
            '    msg.Configure(nconf)
            '    msg.Show()
            '    Exit Sub

            'End If

        End If


        If cbodega > 0 Then

            squery = "select * from Inventarios where IDArticulo=(select IDArticulo from Articulos where ArticuloSBO='" & pArtCode & _
            "') and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "EB", connstringWEB)

            Dim ctemp As Double

            If dt.Rows.Count = 0 Then
                ctemp = 0
            Else
                ctemp = dt.Rows.Item(0).Item("Cantidad")
            End If


            For Each oVenta In CurrentData

                If oVenta.CantidadTienda > 0 Then

                    If oVenta.Linea = pArtCode Then
                        ctemp = ctemp - CDbl(oVenta.CantidadBodega)
                    End If

                End If

            Next


            If ctemp < cbodega Then

                'esCombo.Checked = False
                'artCombo.Text = ""

                'Dim msg As New Ext.Net.Notification
                'Dim nconf As New Ext.Net.NotificationConfig
                'nconf.IconCls = "icon-exclamation"
                'nconf.Html = "El articulo del combo '" & pArtCombo & "' no tiene existencias suficientes en la bodega"
                'nconf.Title = "Error"
                'msg.Configure(nconf)
                'msg.Show()
                'Exit Sub

            End If

        End If

        artCombo.Text = pArtCombo

        squery = "select count(*) from StoreListGlobal where ListID=" & ConfigurationManager.AppSettings("ListaJuegoFin")
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)


        If dt.Rows.Item(0).Item(0) = 1 Then

            squery = "select count(*) from StoreListGlobal where ListID=" & ConfigurationManager.AppSettings("ListaJuego")
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringWEB)

            If dt.Rows.Item(0).Item(0) = 1 Then
                GoTo normal
            End If

            squery = "select ListName from OPLN where ListNum=" & ConfigurationManager.AppSettings("ListaJuegoFin")
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringSAP)

            CantidadesTienda.Render()
            CantidadesBodega.Render()
            Combo.Render()
            AJuego.Render()

            Lista.Items.Clear()

            Lista.AddItem(dt.Rows.Item(0).Item(0), ConfigurationManager.AppSettings("ListaJuegoFin"))

        Else

normal:

            squery = "select ListName from OPLN where ListNum=" & ConfigurationManager.AppSettings("ListaJuego")
            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ET", connstringSAP)

            CantidadesTienda.Render()
            CantidadesBodega.Render()
            Combo.Render()
            AJuego.Render()

            Lista.Items.Clear()

            Lista.AddItem(dt.Rows.Item(0).Item(0), ConfigurationManager.AppSettings("ListaJuego"))

        End If




        Lista.SelectedItem.Index = 0
        Lista.Disabled = False
        Lista.Enabled = True
        Lista.Render()
        'PrecioUnitario.Render()
        'IVA.Render()
        PUIVA.Render()
        Monto.Render()
        Descuento.Render()
        Total.Render()


    End Sub

    Public Function addVenta() As Boolean

        Dim squery As String

        Try

            squery = "DECLARE @intErrorCode INT" & vbNewLine & vbNewLine

            squery = squery & " begin transaction" & vbNewLine & vbNewLine

            squery = squery & " DECLARE @Tienda as int" & vbNewLine
            squery = squery & " DECLARE @TipoFolio as int" & vbNewLine
            squery = squery & " DECLARE @Folio as int" & vbNewLine
            squery = squery & " DECLARE @Prefijo as varchar(20)" & vbNewLine
            squery = squery & " DECLARE @Sufijo as varchar(20)" & vbNewLine
            squery = squery & " DECLARE @UltimaVenta as int" & vbNewLine
            squery = squery & " DECLARE @LineasAInsertar as int" & vbNewLine
            squery = squery & " DECLARE @LineasInsertadas as int" & vbNewLine & vbNewLine

            squery = squery & " DECLARE @ICCS as VARCHAR(200)=''" & vbNewLine
            squery = squery & " DECLARE @IMEIS as VARCHAR(200)=''" & vbNewLine
            squery = squery & " DECLARE @Inventario as int" & vbNewLine
            squery = squery & " DECLARE @ERROR as VARCHAR(200)" & vbNewLine & vbNewLine

            squery = squery & " SET @ERROR = 'Ocurrio un error'" & vbNewLine

            Dim STipoCliente As String = ""

            STipoCliente = "C"

            Dim sCurrentFolio As String

            'If cmbFolioType.SelectedIndex = 0 Then
            sCurrentFolio = "1"
            'Else
            'sCurrentFolio = "3"
            'End If

            squery = squery & " SET @TipoFolio = " & sCurrentFolio & vbNewLine
            squery = squery & " SET @Tienda = " & Session("IDSTORE") & vbNewLine
            squery = squery & " SET @Folio= (select (currentfolio + 1) as [NuevoFolio] from storeFolios where AdminStoreID=@Tienda and AdminFolioType=@TipoFolio)" & vbNewLine
            squery = squery & " SET @Prefijo = LTRIM(RTRIM((select prefijo from storeFolios where AdminStoreID=@Tienda and AdminFolioType=@TipoFolio)))" & vbNewLine
            squery = squery & " SET @Sufijo = LTRIM(RTRIM((select NoAprobacion from storeFolios where AdminStoreID=@Tienda and AdminFolioType=@TipoFolio)))" & vbNewLine
            'squery = squery & " SET @LineasAInsertar = 2" & vbNewLine

            squery = squery & " print @Folio" & vbNewLine
            squery = squery & " print @Prefijo" & vbNewLine


            'Captura de encabezado----------------------------------------------------------------------------------
            squery = squery & "Insert into ventasencabezado (IDCliente,IDUser,IDStore,Fecha,Folio,Prefijo,SUFIJO,TipoVenta,StatusCierre,StatusVenta,Comentarios) values ("
            squery = squery & "'" & DirectCast(FrmCliente.Items.Item(0), Ext.Net.TextField).Text & "'"
            squery = squery & ",'" & Session("AdminUserID") & "'"
            squery = squery & ",@Tienda"
            squery = squery & ",getdate()"

            STipoCliente = "C"

            squery = squery & ",@Folio"
            squery = squery & ",@Prefijo"
            squery = squery & ",@Sufijo"
            squery = squery & ",'VM'"

            squery = squery & ",'O','O',UPPER('" & Comentario.Text & "'))" & vbNewLine & vbNewLine
            squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine

            squery = squery & " SET @UltimaVenta = (select MAX(ve.ID) from VentasEncabezado ve)" & vbNewLine
            squery = squery & " Print @UltimaVenta" & vbNewLine

            squery = squery & "Update storeFolios set currentfolio=@Folio where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=" & sCurrentFolio & "" & vbNewLine
            squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine

            Dim sQueryEfectivo As String = ""
            Dim dEfectivo As Double

            For Each dPago As Pago In CurrentDataPago
                If dPago.FormaPago <> "Efectivo" Then
                    squery = squery & " insert into VentasPagos (IDVenta,AdminStoreID,TipoVenta,FormaPago,Monto,Fecha,StatusPago,NoCuenta,Prefijo,Folio) values (@UltimaVenta,@Tienda,'VT','" & dPago.FormaPago & "'," & dPago.MontoPagado & ",getdate(),'O','" & dPago.NoCuenta & "',(select Prefijo from StoreFolios  where AdminStoreID=@Tienda and AdminFolioType=2) ,(select CurrentFolio + 1 from StoreFolios  where AdminStoreID=@Tienda and AdminFolioType=2))" & vbNewLine
                    squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine
                    squery = squery & " Update  StoreFolios set CurrentFolio = CurrentFolio + 1 where AdminStoreID=@Tienda and AdminFolioType=2" & vbNewLine
                    squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine
                Else
                    dEfectivo = dEfectivo + dPago.MontoPagado
                End If
            Next

            If dEfectivo > 0 Then
                squery = squery & "insert into VentasPagos (IDVenta,AdminStoreID,TipoVenta,FormaPago,Monto,Fecha,StatusPago,NoCuenta,Prefijo,Folio) values (@UltimaVenta,@Tienda,'VT','Efectivo'," & dEfectivo - DirectCast(GridPanel1.BottomBar.Item(0).Items.Item(8), TextField).Text & ",getdate(),'O','',(select Prefijo from StoreFolios  where AdminStoreID=@Tienda and AdminFolioType=2) ,(select CurrentFolio + 1 from StoreFolios  where AdminStoreID=@Tienda and AdminFolioType=2))" & vbNewLine
                squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine
                squery = squery & " Update  StoreFolios set CurrentFolio = CurrentFolio + 1 where AdminStoreID=@Tienda and AdminFolioType=2" & vbNewLine
                squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine
            End If

            squery = squery & vbNewLine

            'Captura de detalle y actualiza Inventarios/Series-------------------------------------------------------------------------

            Dim subtotal4Decimales As String
            Dim subtotal2Decimales As String

            Dim impuesto2Decimales As String
            Dim impuesto4Decimales As String

            Dim totalLinea2Decimales As String

            Dim rowCount As Integer = 1
            Dim lineasAInsertar As Integer

            For Each oVenta In CurrentData

tagCheck:

                squery = squery & "Insert into ventasdetalle ("
                'IDVenta,IDLinea,IDArticulo,Lista,PrecioUnitario,IVA,Observaciones,TotalLinea,IMEI,ICC,DialNumber,Cantidad,StatusLinea,Referencia,ReferenciaDN,IDStore,Fecha
                squery = squery & "IDVenta,"
                squery = squery & "IDLinea,"
                squery = squery & "IDArticulo,"
                squery = squery & "Juego,"
                squery = squery & "Lista,"
                squery = squery & "PrecioUnitario,"
                squery = squery & "IVA,"
                squery = squery & "Observaciones,"
                squery = squery & "Descuento,"
                squery = squery & "TotalLinea,"
                squery = squery & "Cantidad,"
                squery = squery & "StatusLinea,"
                squery = squery & "IDStore,"
                squery = squery & "Fecha"
                squery = squery & ") values("
                squery = squery & "@UltimaVenta" 'IDVenta
                squery = squery & ",'" & rowCount & "'" 'IDLinea
                squery = squery & ",'" & getItemID(oVenta.Linea) & "'" 'IDArticulo
                squery = squery & ",'" & oVenta.Juego & "'" 'Juego
                squery = squery & ",'" & oVenta.Lista & "'" 'Lista
                squery = squery & ",'" & FormatCurrency(oVenta.PrecioUnitario, 4) * 1 & "'" 'PrecioUnitario
                squery = squery & ",'" & oVenta.IVA & "'" 'IVA
                squery = squery & ",'" & "v2" & "'" 'Observaciones
                squery = squery & ",'" & oVenta.Descuento & "'" 'Descuento
                squery = squery & ",'" & oVenta.Total & "'" 'TotalLinea

                If oVenta.CantidadTienda > 0 Then
                    squery = squery & ",'" & oVenta.CantidadTienda & "'" 'Cantidad
                Else
                    squery = squery & ",'" & oVenta.CantidadBodega & "'" 'Cantidad
                End If

                squery = squery & ",'O'" 'StatusLinea

                If oVenta.CantidadTienda > 0 Then
                    squery = squery & "," & Session("IDSTORE") 'IDStore
                Else
                    squery = squery & ",(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")"
                End If

                'squery = squery & ",@Tienda" 'IDStore

                squery = squery & ",getdate()" 'Fecha
                squery = squery & ")" & vbNewLine & vbNewLine
                squery = squery & " SELECT @intErrorCode = @@ERROR IF (@intErrorCode <> 0) GOTO PROBLEM" & vbNewLine

                lineasAInsertar = lineasAInsertar + 1

                If cmbFolioType.SelectedItem.Value = "2" Then


                    If oVenta.CantidadTienda > 0 Then
                        squery = squery & " select @Inventario=COUNT(*) from Inventarios where IDArticulo='" & getItemID(oVenta.Linea) & "' and Almacen='" & Session("IDSTORE") & "'" & vbNewLine
                    Else
                        squery = squery & " select @Inventario=COUNT(*) from Inventarios where IDArticulo='" & getItemID(oVenta.Linea) & "' and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")" & vbNewLine
                    End If

                    squery = squery & " if @Inventario = 0 " & vbNewLine

                    If oVenta.CantidadTienda > 0 Then
                        squery = squery & " insert into Inventarios (IDArticulo,Cantidad,Almacen) values ('" & getItemID(oVenta.Linea) & "',0,'" & Session("IDSTORE") & "')" & vbNewLine & vbNewLine
                    Else
                        squery = squery & " insert into Inventarios (IDArticulo,Cantidad,Almacen) values ('" & getItemID(oVenta.Linea) & "',0,(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & "))" & vbNewLine & vbNewLine
                    End If

                    'If oVenta.CantidadTienda > 0 Then
                    '    squery = squery & "update inventarios set cantidad=cantidad -" & oVenta.CantidadTienda & " where IDArticulo='" & getItemID(oVenta.Linea) & "' and Almacen='" & Session("IDSTORE") & "'" & vbNewLine
                    'Else
                    '    squery = squery & "update inventarios set cantidad=cantidad -" & oVenta.CantidadBodega & " where IDArticulo='" & getItemID(oVenta.Linea) & "' and Almacen=(select whsconsigid from AdminStore where AdminStoreID=" & Session("IDSTORE") & ")" & vbNewLine
                    'End If

                End If

                rowCount = rowCount + 1

            Next

            squery = squery & " SET @LineasAInsertar = " & lineasAInsertar & vbNewLine

            squery = squery & " SET @LineasInsertadas = (select COUNT(*) from VentasDetalle where IDVenta=@UltimaVenta)" & vbNewLine
            squery = squery & " PRINT @LineasInsertadas" & vbNewLine & vbNewLine

            squery = squery & " IF (@LineasAInsertar <> @LineasInsertadas) GOTO PROBLEM" & vbNewLine

            squery = squery & " IF (" & DirectCast(FrmCliente.Items.Item(0), Ext.Net.TextField).Text & " = 70) GOTO PROBLEM" & vbNewLine

            squery = squery & " Print 'Transaccion Exitosa'" & vbNewLine & vbNewLine

            'squery = squery & " ROLLBACK transaction" & vbNewLine
            squery = squery & " commit transaction" & vbNewLine & vbNewLine

            squery = squery & " PROBLEM:" & vbNewLine
            squery = squery & " IF (@intErrorCode <> 0) or (@LineasAInsertar <> @LineasInsertadas) OR (@IMEIS <> '') OR (@ICCS<>'') OR (@intErrorCode<>0) or (" & DirectCast(FrmCliente.Items.Item(0), Ext.Net.TextField).Text & " = 70) BEGIN" & vbNewLine
            squery = squery & " Print 'Ocurrio un error'" & vbNewLine
            squery = squery & " ROLLBACK transaction" & vbNewLine
            squery = squery & "  RAISERROR(@ERROR,18,1)" & vbNewLine
            squery = squery & " End" & vbNewLine & vbNewLine


tag:
            Dim sError As String = ""

            If oDB.EjecutaQry(squery, CommandType.Text, connstringWEB, sError) = 2 Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"

                If sError.Contains("EL IMEI") Or sError.Contains("EL ICC") Then
                    nconf.Html = sError
                Else
                    nconf.Html = "Se encontro un problema al procesar la venta, por favor seleccione nuevamente al cliente e intente de nuevo <br> Si el problema persiste por favor cancele la venta y vuélvalo a intentar<BR>" & sError
                End If

                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()
                Return False
                Exit Function

            End If

            Session("IDPRINTVENTA") = getLastSale(Session("AdminUserID"))

            Return True

        Catch ex As Exception

        End Try

    End Function

    Public Function omitirFactura() As Boolean

        For Each ov As Venta In CurrentData

            If ov.Medida = ConfigurationManager.AppSettings("Lista") Then
                Return True
            End If

        Next

        For Each ov As Venta In CurrentData

            If ov.CantidadBodega.Contains("TEMM") Then
                Return True
            End If

        Next

        If Session("CurrentClient").TipoCliente = "M" Then
            Return True
        End If

        Return False

    End Function

    Public Function getCurrentFolio(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select (currentfolio + 1) as [NuevoFolio] from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "UltimoFolio", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NuevoFolio")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getPrefix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select prefijo from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Prefijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("prefijo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function


    Public Function getSufix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select NoAprobacion from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Sufijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NoAprobacion")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getItemID(ByVal Item As String) As String

        Dim sQuery As String
        Dim dt As New DataTable

retry:

        sQuery = "select IDArticulo from articulos where ArticuloSBO = '" & Item & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "IDArticulo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("IDArticulo")

        Next

        GoTo retry

        Return 0

        dt = Nothing
        GC.Collect()

    End Function


    Public Function getItemIDForSim(ByVal ICC As String) As String

        Dim sQuery As String
        Dim dt As New DataTable

retry:

        sQuery = "select IDArticulo"
        sQuery = sQuery & " from Articulos A,Series S "
        sQuery = sQuery & " where S.ArticuloSBO = A.ArticuloSBO"
        sQuery = sQuery & " and ICC='" & ICC & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "IDArticulo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("IDArticulo")

        Next

        GoTo retry

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getLastSale(ByVal user As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from ventasencabezado where iduser=" & user

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Sub StorePagos_RefreshData(ByVal sender As Object, ByVal e As Ext.Net.StoreReadDataEventArgs) Handles StorePagos.ReadData
        StorePagos.DataBind()
    End Sub

    Private Sub btnFindClient_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnFindClient.DirectClick

        'Dim sf As StringFilter = DirectCast(GridFilters1.Filters(1), StringFilter)

        'If Session("SelCount") < 2 Then

        'If Session("SelCount") = 0 Then
        'sf.SetValue("")
        'sf.SetActive(True)

        'sf = New StringFilter
        'sf = DirectCast(GridFilters1.Filters(2), StringFilter)
        'sf.SetValue("")
        'sf.SetActive(True)

        'BindDataCliente(DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Text, DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text)
        'DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).ClearSelections()
        'DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).UpdateSelection()
        'End If

        'Dim sQuery As String
        'Dim dt As New DataTable

        'sQuery = "Select top 1 * from clientes where tipoCliente='C'" & vbNewLine

        'If DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Text <> "" Or DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text <> "" Then
        '    sQuery = sQuery & " and Nombre like '" & DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Text & "%'" & _
        '    " or RFC like '" & DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text & "%'"
        'End If

        'dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Busqueda", connstringWEB)

        'If dt.Rows.Count <> 0 Then

        '    For Each Drow As DataRow In dt.Rows

        '        DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).SelectedRows.Add(New SelectedRow(Drow("ID").ToString))
        '        DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).UpdateSelection()
        '        Session("SelCount") = Session("SelCount") + 1

        '        'Pregunta para jonh, esto para que lo hacias? hace que resulte imposible seleccionar al cliente que uno desea despues de usar el buscar
        '        'btnFindClient.FireEvent("Click")

        '    Next

        'End If

        'sf = New StringFilter
        'sf = DirectCast(GridFilters1.Filters(1), StringFilter)
        'sf.SetValue(DirectCast(Ext.Net.ExtNet.GetCtl("tfFindNombre"), Ext.Net.TextField).Text)
        'sf.SetActive(True)

        'sf = New StringFilter
        'sf = DirectCast(GridFilters1.Filters(2), StringFilter)
        'sf.SetValue(DirectCast(Ext.Net.ExtNet.GetCtl("ftFindRFC"), Ext.Net.TextField).Text)
        'sf.SetActive(True)

        'Else
        'Session("SelCount") = 0
        'btnFindClient.Disabled = True
        'End If

        'Session("bChanging") = False


        Me.BindDataCliente(tfFindNombre.Text, ftFindRFC.Text)

        If tfFindNombre.Text <> "" Or ftFindRFC.Text <> "" Then
            Try
                If Session("clientes").count = 0 Then
tag:
                    tfFindNombre.Text = ""
                    'ftFindRFC.Text = ""
                    btnCleanClient.FireEvent("Click")
                    Button2.FireEvent("Click")
                End If
            Catch ex As Exception

            End Try

            If ftFindRFC.Text <> "" Then


                If ftFindRFC.Text <> "" Then


                    Dim squery As String

                    squery = "insert into Log_Clientes (Accion,RFC,Tienda,fecha,usuario) values ('Se busco el RFC','" & ftFindRFC.Text & "','" & Session("IDSTORE") & "',GETDATE()," & Session("AdminUserID") & ")" & vbNewLine

                    oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

                End If

            End If

        End If

    End Sub

    Private Sub btnCleanClient_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCleanClient.DirectClick

        actCod_PROVINCIAmunicipio.Clear()
        actCod_CIUDAD.Clear()
        actCod_COMUNAcolonia.Clear()

        actCod_PROVINCIAmunicipio.Text = ""
        actCod_CIUDAD.Text = ""
        actCod_COMUNAcolonia.Text = ""

        tfColonia.Text = ""
        tfDelMun.Text = ""
        actCALLE.Text = ""
        actNUMEXT.Text = ""
        actNUMINT.Text = ""
        tfCP.Text = ""
        tfTelCasa.Text = ""
        tfTelOfc.Text = ""
        tfCorreo.Text = ""
        cmbTipoCliente.Text = ""
        actFCHNACIMIENTO.Text = ""

        Dim squery As String
        Dim dt As New DataTable


        squery = "select * from ge_regiones order by DES_REGION"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Cliente", connstringWEB)
        tfColonia.Render()
        tfDelMun.Render()

        For Each row As DataRow In dt.Rows
            Dim item As New Ext.Net.ListItem
            item.Value = row("COD_REGION")
            item.Text = row("DES_REGION")
            actCod_REGIONestado.Items.Add(item)
        Next

        actCod_CIUDAD.Render()
        actCod_COMUNAcolonia.Render()
        tfColonia.Render()
        tfDelMun.Render()
        actCod_REGIONestado.Render()
        'actCALLE.Render()
        'actNUMEXT.Render()
        'actNUMINT.Render()
        tfCP.Render()
        tfTelCasa.Render()
        tfTelOfc.Render()
        tfCorreo.Render()
        cmbTipoCliente.Render()
        actFCHNACIMIENTO.Render()

        If Session("SelCount") < 2 Then

            DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).ClearSelection()
            DirectCast(Ext.Net.ExtNet.GetCtl("RowSelectionModel1"), Ext.Net.RowSelectionModel).UpdateSelection()
            Session("SelCount") = Session("SelCount") + 1
            btnCleanClient.FireEvent("Click")
            'btnFindClient.Disabled = False

        Else

            Session("SelCount") = 0

        End If

    End Sub

    Private Sub Button3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button3.Click

    End Sub

    Private Sub Button3_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Button3.DirectClick


        'If cmbFolioType.SelectedItem.Value = "" Then
        '    cmbFolioType.SetValue("3")
        '    'Dim msg As New Ext.Net.Notification
        '    'Dim nconf As New Ext.Net.NotificationConfig
        '    'nconf.IconCls = "icon-exclamation"
        '    'nconf.Html = "Debe seleccionar un tipo de documento"
        '    'nconf.Title = "Error"
        '    'msg.Configure(nconf)
        '    'msg.Show()
        '    'Exit Sub
        'End If

        If DirectCast(FrmCliente.Items.Item(0), Ext.Net.TextField).Text = "" Then
            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "Debe seleccionar un cliente para continuar"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()
            Exit Sub
        End If

        If CurrentData.Count = 0 Then
            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "No se pude crear una factura/nota sin articulos"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()
            Exit Sub
        End If

        Dim restante As Double = 0

        restante = ObtenRestante()

        'If restante > 0 Then
        '    Dim msg As New Ext.Net.Notification
        '    Dim nconf As New Ext.Net.NotificationConfig
        '    nconf.IconCls = "icon-exclamation"
        '    nconf.Html = "No se pude crear una factura/nota sin saldar el adeudo antes"
        '    nconf.Title = "Error"
        '    msg.Configure(nconf)
        '    msg.Show()
        '    Exit Sub
        'End If

        If addVenta() = True Then


            'If IsNothing(Session("CurrentClient")) = False Then

            '    If Session("CurrentClient").RFC <> "" And omitirFactura() = False Then

            '        Dim ge As New GeneraFE
            '        ge.CreaFacturaXML(Session("IDPRINTVENTA"))

            '    End If

            'End If

            InsertaLog(Tipos.VE, Session("IDPRINTVENTA"), Session("IDSTORE"), Session("WHSID"))

            If cmbFolioType.SelectedItem.Value = "2" Then

                Dim sQuery As String
                Dim bError As Boolean = False
                Dim dt As DataTable

                sQuery = "select "
                sQuery = sQuery & " (select ItemName from " & Session("SAPDB") & ".oitm where itemcode=(select ArticuloSBO from Articulos where IDArticulo=VentasDetalle.IDArticulo)) as [Articulo]"
                sQuery = sQuery & " ,sum(Cantidad) as [Cantidad]"
                sQuery = sQuery & " ,IDStore"
                sQuery = sQuery & " ,(select Cantidad from Inventarios where IDArticulo=VentasDetalle.IDArticulo and Almacen=VentasDetalle.IDStore) as Existencia"
                sQuery = sQuery & " from VentasDetalle where IDVenta = " & Session("IDPRINTVENTA")
                sQuery = sQuery & " group by IDStore,IDArticulo"

                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                For Each row As DataRow In dt.Rows

                    If row("Cantidad") > row("Existencia") Then

                        bError = True

                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "Esta venta no puede facturarse debido a que hay existencia insuficiente del articulo " & row("Articulo") & "<br> Existencia : " & row("Existencia")
                        nconf.Title = "Error"
                        msg.Configure(nconf)
                        msg.Show()

                    End If

                Next

                If bError = False Then

                    GeneraArchivoFactura(Session("IDPRINTVENTA"), ConfigurationManager.AppSettings("DirectorioFacturasTXT"), Session("SAPDB"), ConfigurationManager.AppSettings("FTP"), ConfigurationManager.AppSettings("FTPUSR"), ConfigurationManager.AppSettings("FTPPASS"))

                    'Dim sQuery As String
                    Dim sError As String

                    sQuery = "select 'FACTURA_' + Prefijo + RIGHT('0000' + convert(varchar,Folio),5) + '_' + Prefijo from VentasEncabezado where ID=" & Session("IDPRINTVENTA")

                    'Dim dt As DataTable

                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "TMP", connstringWEB)

                    sQuery = "update ventasencabezado set FechaCFDI=getdate(),Facturado=1,Archivo_FE='" & ConfigurationManager.AppSettings("DirectorioFacturasTXT") & dt.Rows.Item(0).Item(0) & "' where id=" & Session("IDPRINTVENTA")
                    oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB, sError)

                End If

            End If

            CurrentData.Clear()
            CurrentDataPago.Clear()
            Session("POSTBACK") = "Facturacion"
            Session("CurrentClient") = Nothing
            Response.Redirect("../printControl/imprimeventa.aspx")

        End If

    End Sub

    Private Sub btnCancelarFactura_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles btnCancelarFactura.DirectClick

        CurrentData.Clear()
        CurrentDataPago.Clear()
        Session("CurrentClient") = Nothing
        Response.Redirect("Facturacion.aspx")

    End Sub


    Private Sub LimpiarArticulo_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles LimpiarArticulo.DirectClick


    End Sub

    <DirectMethod()> _
    Public Sub OnSelectedLine()

        'Session("bChanging") = True

        Me.BindDataCliente(tfFindNombre.Text, ftFindRFC.Text)

        Dim sm As RowSelectionModel = TryCast(Me.GridCliente.SelectionModel.Primary, RowSelectionModel)

        If sm.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        For Each row As SelectedRow In sm.SelectedRows

            Session("CurrentClient") = DirectCast(CurrentDataClientes(tfFindNombre.Text, ftFindRFC.Text), List(Of Cliente))(CInt(row.RowIndex))
            setClientData()

            tfColonia.Render()
            tfDelMun.Render()
            'tfCP.Render()

        Next



    End Sub

    Private Sub Button13_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button13.Click
        Updatecliente(Session("CurrentClient"))
    End Sub

    Private Sub Button13_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Button13.DirectClick
        Updatecliente(Session("CurrentClient"))
    End Sub

    <DirectMethod()> _
    Public Sub OnSelecteEstado(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        Session("bChanging") = True

        squery = "select * from  ge_provincias where  cod_region='" & Value & "' order by DES_PROVINCIA"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Municipos", connstringWEB)

        If actCod_PROVINCIAmunicipio.Items.Count <> 0 Then
            actCod_PROVINCIAmunicipio.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            actCod_PROVINCIAmunicipio.Items.Add(New Ext.Net.ListItem(row("DES_PROVINCIA").ToString(), row("COD_PROVINCIA")))
        Next

        actCod_PROVINCIAmunicipio.Render()
        actCod_CIUDAD.Render()
        actCod_COMUNAcolonia.Render()
        tfColonia.Render()
        'actCALLE.Render()
        'actNUMEXT.Render()
        'actNUMINT.Render()
        tfCP.Render()
        tfTelCasa.Render()
        tfTelOfc.Render()
        tfCorreo.Render()
        cmbTipoCliente.Render()
        actFCHNACIMIENTO.Render()


    End Sub

    <DirectMethod()> _
    Public Sub OnSelectMunicipio(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        Session("bChanging") = True

        squery = "select * from  dbo.ge_ciudades where cod_region='" & actCod_REGIONestado.SelectedItem.Value & "' and cod_provincia='" & Value & "'"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Ciudades", connstringWEB)

        If actCod_CIUDAD.Items.Count <> 0 Then
            actCod_CIUDAD.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            actCod_CIUDAD.Items.Add(New Ext.Net.ListItem(row("DES_CIUDAD").ToString(), row("COD_CIUDAD")))
        Next

        squery = "select * from  dbo.ge_comunas where cod_region='" & actCod_REGIONestado.SelectedItem.Value & "' and cod_provincia='" & Value & "'"
        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Comuna", connstringWEB)

        If actCod_COMUNAcolonia.Items.Count <> 0 Then
            actCod_COMUNAcolonia.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            actCod_COMUNAcolonia.Items.Add(New Ext.Net.ListItem(row("DES_COMUNA").ToString(), row("COD_COMUNA")))
        Next

        actCod_CIUDAD.Render()
        actCod_COMUNAcolonia.Render()
        tfColonia.Render()
        'actCALLE.Render()
        'actNUMEXT.Render()
        'actNUMINT.Render()
        tfCP.Render()
        tfTelCasa.Render()
        tfTelOfc.Render()
        tfCorreo.Render()
        cmbTipoCliente.Render()
        actFCHNACIMIENTO.Render()


    End Sub

    <DirectMethod()> _
    Public Sub OnSelectCredTarjeta(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        If Value = "Si" Then
            squery = "select * from ec_credito"
            institucionCredTarjeta.FieldLabel = "Banco"
        Else
            squery = "select * from ec_credito"
            institucionCredTarjeta.FieldLabel = "Departamental"
        End If


        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Bancos", connstringWEB)

        If institucionCredTarjeta.Items.Count <> 0 Then
            institucionCredTarjeta.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            institucionCredTarjeta.Items.Add(New Ext.Net.ListItem(row("DESCRIPCION").ToString(), row("CVE_IDENTIFICADOR")))
        Next

        institucionCredTarjeta.Render()
        Label8.Render()
        credHipotecario.Render()
        institucionCredHipotecario.Render()
        Label9.Render()
        ccredAutomotriz.Render()
        credAutomotriz.Render()


    End Sub

    <DirectMethod()> _
    Public Sub OnSelectinstitucionCredHipotecario(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        If Value = "Si" Then
            squery = "select * from ec_hipotecario"
            institucionCredHipotecario.Hidden = False
        Else
            institucionCredHipotecario.Hidden = True
            Exit Sub
        End If


        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ec_hipotecario", connstringWEB)

        If institucionCredHipotecario.Items.Count <> 0 Then
            institucionCredHipotecario.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            institucionCredHipotecario.Items.Add(New Ext.Net.ListItem(row("NOMBRE").ToString(), row("CVE_INSTITUCION")))
        Next

        institucionCredHipotecario.Render()
        Label9.Render()
        ccredAutomotriz.Render()
        credAutomotriz.Render()


    End Sub

    <DirectMethod()> _
    Public Sub OnSelectCredAutomotriz(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        If Value = "Si" Then
            squery = "select * from ec_automotriz"
            credAutomotriz.Hidden = False
        Else
            credAutomotriz.Hidden = True
            Exit Sub
        End If


        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "ec_hipotecario", connstringWEB)

        If credAutomotriz.Items.Count <> 0 Then
            credAutomotriz.Items.Clear()
        End If

        For Each row As DataRow In dt.Rows
            credAutomotriz.Items.Add(New Ext.Net.ListItem(row("NOMBRE").ToString(), row("CVE_INSTITUCION")))
        Next

        credAutomotriz.Render()


    End Sub


End Class