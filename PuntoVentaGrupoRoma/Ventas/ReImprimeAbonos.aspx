﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReImprimeAbonos.aspx.vb" Inherits="PuntoVentaGrupoRoma.ReImprimeAbonos" MasterPageFile="~/admin.master" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <br />
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
    <ext:Hidden ID="UseConfirmation" runat="server" Text="false" />
    
    <style type="text/css">                
       
        .x-grid-custom .x-grid3-row TD{                        
            height: 50px;
        } 
        
         .x-grid-custom .x-grid3-row-alt{
           background-color: #DAE2E8;           
        }
       
    </style>
    
    <ext:XScript ID="XScript1" runat="server">
    
        <script type="text/javascript">
            Ext.data.Connection.override({ timeout: 1200000 });
            Ext.Ajax.timeout = 12000000;
</script> <script type="text/javascript">
            function ValidaCantidad(value) {
                if (value == "") { value = this.gridEditor.record.data.Cantidad }
                if (parseFloat(value) > parseFloat(this.gridEditor.record.data.Cantidad)) {
                    return "Cantidad Incorrecta";
                }
                return true;
            }

            //in PrepareCommand we can modify command
            var prepareCommand = function (grid, command, record, row) {
                if (command.command == 'Factura' && record.get("Facturado") == '') {
                    command.hidden = true;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

                if (command.command == 'Factura' && record.get("Restante") != 0) {
                    command.hidden = true;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

                if (command.command == 'CFactura' && record.get("Facturado") == '' && record.get("Devolucion") == 0) {
                    command.hidden = false;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

                if (command.command == 'CFactura' && record.get("Restante") != 0) {
                    command.hidden = true;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

                if (command.command == 'NCInterna' && record.get("Devolucion") == 1 && record.get("NCE") == 0) {
                    command.hidden = false;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

                if (command.command == 'NCFiscal' && record.get("Devolucion") == 1 && record.get("NCE") == 1) {
                    command.hidden = false;
                    command.hideMode = 'display'; //you can try 'visibility' also
                    //command.text = 'Generar Factura';
                }

            };

        </script> </ext:XScript>

    
   
     
     <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="true"
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None" AutoLoad="false">
            <Proxy>
                <ext:AjaxProxy Url="ClientesReImprimeAbonos.ashx" >
                    <ActionMethods READ="POST" />
                </ext:AjaxProxy>   
            </Proxy> 
            <Model>
                <ext:Model runat="server" Root="clientes" TotalProperty="Reimprimir" >
                    <Fields>
                        <ext:ModelField Name="cCliente" />
                        <ext:ModelField Name="RFC"/> 
                        <ext:ModelField Name="cIdCliente"/>                        
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
   
    <ext:GridPanel 
            ID="gpDevoluciones" 
            runat="server"
            Icon="Table" 
            Title="Re impresion comprobantes"  
            Height="500"   
            AutoExpandColumn = "Reimprimir" 
            ClicksToEdit="1"    
            AutoDataBind="true"
            EnableViewState="true"
            StripeRows = "true"    
            Cls="x-grid-custom" DisableSelection="true">   
            <Store>
                <ext:Store
                    ID="StoreDevoluciones"
                    runat="server"
                    AutoSave="true"
                    ShowWarningOnFailure="false"
                    SkipIdForNewRecords="false"
                    RefreshAfterSaving="None"
                    OnBeforeStoreChanged="HandleChanges">
                    <Model>
                        <ext:Model runat="server" IDProperty="Id">
                            <Fields>
                                <ext:ModelField Name="Id" />
                                <ext:ModelField Name="venta" />
                                <ext:ModelField Name="abono" />
                                <ext:ModelField Name="fechaventa" />
                                <ext:ModelField Name="fecharecibo" />
                                <ext:ModelField Name="montopagado" />
                                <ext:ModelField Name="Facturado" />
                                <ext:ModelField Name="Reimprimir" />
                                <ext:ModelField Name="Restante" Type="Float" />
                                <ext:ModelField Name="Devolucion" />
                                <ext:ModelField Name="NCE" />
                            </Fields>
                        </ext:Model>
                    </Model>

                    <Listeners>
                        <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
                    </Listeners>
                </ext:Store>
            
            </Store>                    
            <ColumnModel>
                <Columns>
                                    
                    <ext:Column runat="server" Header="Id" DataIndex="Id" Hidden="true"  />
                    
                    <ext:Column runat="server" Header="Venta" DataIndex="venta" Width="30">                        
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Abono" DataIndex="abono" Width="30">                        
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Fecha Venta" DataIndex="fechaventa" Width="30">                        
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Fecha Recibo" DataIndex="fecharecibo" Width="30">                                                
                    </ext:Column>
                    
                     <ext:Column runat="server" Header="Monto Pagado" DataIndex="montopagado" Width="25">                        
                        <Renderer Format="UsMoney" />
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Factura" DataIndex="Facturado" Hidden="true">                                                
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Restante" DataIndex="Restante" Hidden="true">                                                
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Devolucion" DataIndex="Devolucion" Hidden="true">                                                
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="NCE" DataIndex="NCE" Hidden="true">                                                
                    </ext:Column>
                                       
                   <ext:Column runat="server" Header="Imprimir" Align="Left" DataIndex="Reimprimir">
                        <Commands>                            
                            <ext:ImageCommand Icon="Table" CommandName="Pedido" Text="Pedido" />                            
                            <ext:ImageCommand Icon="Money" CommandName="Abono" Text="Abono" />
                            <ext:ImageCommand Icon="PageWhiteAcrobat" CommandName="Factura"  Text="  Ver Factura" HideMode="Display" />
                            <ext:ImageCommand Icon="PageWhiteAcrobat" CommandName="CFactura" Text="Crear Factura" Hidden="true" HideMode="Display" />                            
                            <ext:ImageCommand Icon="TableDelete" CommandName="NCInterna" Text="NC Interna" Hidden="true" />
                            <ext:ImageCommand Icon="PageWhiteAcrobat" CommandName="NCFiscal" Text="NC Fiscal" Hidden="true" />
                        </Commands>            
                        <PrepareCommand Fn="prepareCommand" />
                    </ext:Column>                   
                  
                    <ext:CommandColumn runat="server" Align="Center" OverOnly="true" Width="25">
                        <Listeners>
                            <Command Handler="Ext.net.DirectMethods.Reimprime(record.data.Id,command);" />
                        </Listeners>
                    </ext:CommandColumn>
                                                                                           
                </Columns>
            </ColumnModel>
            
            
            
                             <Features>
                                <ext:GridFilters runat="server" ID="GridFilters1">
                                    <Filters>  
                                    <ext:StringFilter DataIndex="Articulo"  /> 

                                    </Filters>
                                </ext:GridFilters>
                            </Features>
    
            <View>
                <ext:GridView ID="gvDevoluciones" runat="server" ForceFit="true">                    
                </ext:GridView>
            </View>
            
            <TopBar>
                <ext:Toolbar ID="Toolbar1" runat="server" Layout="Container" >
                    <Items>
                       <%-- <ext:TableLayout runat="server" Columns="1">                        
                            <Cells>
                            <ext:Cell>--%>
                            
                            <ext:Toolbar runat="server" Flat="true">
                                <Items>
                                                                              
                                <ext:Label runat="server" Text="Cliente: "></ext:Label>  
                                
                                <ext:ComboBox
                                runat="server" 
                                ID="cmbCliente"
                                StoreID="StoreClientes"
                                DisplayField="cCliente" 
                                ValueField="cIdCliente"                                                        
                                TypeAhead="false"
                                LoadingText="Searching..."                 
                                PageSize="10"
                                HideTrigger="true"
                                ItemSelector="div.search-item"        
                                AnchorHorizontal="100%"
                                MinChars="1" 
                                EmptyText="Seleccione Cliente"
                                AllowBlank="false"                                                       
                                Width="250"
                                >
                                <ListConfig>
                                    <ItemTpl ID="Template1" runat="server">
                                       <Html>
					                       
						                      <div class="search-item">
							                     <h3><span>{cCliente}</span></h3>
							                     {RFC} 
						                      </div>
					                       
				                       </Html>
                                    </ItemTpl>
                                    </ListConfig>
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetPeriods(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox>
                                
                                <ext:Label ID="lblPeriodo" runat="server" Text="Periodo:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbFecha"
                                EmptyText="Seleccione Fecha"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <%--<Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>--%>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetDays(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblDia" runat="server" Text="Dia:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbDia" 
                                EmptyText="Seleccione un dia"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblFolio" runat="server" Text="Folio:" Visible="True"></ext:Label>                       
                                
                                <ext:ComboBox
                                runat="server" 
                                ID="cmbFolio"
                                EmptyText="Seleccione Folio" Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.DoBind(el.value,false);}"/>
                                    </Listeners>
                                </ext:ComboBox>                                           
                                
                                <ext:Button runat="server" ID="btnClean" Text="Nuevo Filto" Flat="false" Icon="Erase">                                
                                <Listeners>
                                        <Click Fn="function(el) {Ext.net.DirectMethods.Clean();}"/>
                                    </Listeners>
                                </ext:Button>    
                            <%--    </ext:Cell>
                                
                            </cells>                        
                        </ext:tablelayout> --%>
                        </Items>
                        </ext:Toolbar>
                        
                        <ext:Toolbar runat="server" Flat="true">                        
                            <Items>
                                <ext:TextField runat="server" FieldLabel="Pedido" ID="Pedido">
                                </ext:TextField>
                                <ext:Button runat="server" Text="Buscar" Icon="Find" ID="Buscar"></ext:Button>
                            </Items>
                        </ext:Toolbar> 
                        
                        
                    </Items>
                </ext:Toolbar>
            </TopBar>
            
            <BottomBar>
                <ext:Toolbar runat="server" ID="tbBottom" >
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Tipo:" Visible="False"></ext:Label>
                        <ext:ToolbarSpacer Width="10"></ext:ToolbarSpacer> 
                        <ext:ComboBox runat="server" ID="cmbTipo" SelectedIndex="1" Disabled="true" Hidden="true">
                            <Items>
                                <ext:ListItem Text="Cancelacion (Mismo dia)" Value="Cancelacion" /> 
                                <ext:ListItem Text="Nota de credito (Devolucion)" Value="Devolucion" /> 
                            </Items> 
                        </ext:ComboBox> 
                        <ext:ToolbarFill></ext:ToolbarFill> 
                        <ext:Button ID="btnDevolucion" runat="server" Text="Re-Imprimir" Icon="Accept" Hidden="true">

                            <DirectEvents>
                                <Click OnEvent="realizaAbono">
                                    <EventMask ShowMask="true" Msg="Realizando Abono" MinDelay="1000" />
                                </Click>
                            </DirectEvents>

                        </ext:Button>    
                    </Items> 
                </ext:Toolbar> 
            </BottomBar> 
            
           <SelectionModel>
                <ext:CheckboxSelectionModel ID="chkDevolber" runat="server" HideCheckAll="true" Visible="false">
                </ext:CheckboxSelectionModel>
            </SelectionModel>

                       
        </ext:GridPanel>
        
</asp:Content>