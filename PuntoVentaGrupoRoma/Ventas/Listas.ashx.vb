﻿Imports System.Web
Imports System.Web.Services
Imports Ext.Net
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.Xml

<WebService([Namespace]:="http://tempuri.org/")> _
    <WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class Listas
    Implements System.Web.IHttpHandler

    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/json"

        Dim start = 0
        Dim limit = 500
        Dim sort = String.Empty
        Dim dir = String.Empty
        Dim query = String.Empty

        If Not String.IsNullOrEmpty(context.Request("start")) Then
            start = Integer.Parse(context.Request("start"))
        End If

        If Not String.IsNullOrEmpty(context.Request("limit")) Then
            limit = Integer.Parse(context.Request("limit"))
        End If

        If Not String.IsNullOrEmpty(context.Request("sort")) Then
            sort = context.Request("sort")
        End If

        If Not String.IsNullOrEmpty(context.Request("dir")) Then
            dir = context.Request("dir")
        End If

        If Not String.IsNullOrEmpty(context.Request("query")) Then
            query = context.Request("query")
        End If

        Dim listas As Paging(Of Lista) = Lista.ListasPaging(start, limit, sort, dir, query, context.Session("IDSTORE"))

        context.Response.Write(String.Format("{{total:{1},'listas':{0}}}", JSON.Serialize(listas.Data), listas.TotalRecords))

    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property


End Class

Public Class Lista

    Public Sub New(ByVal cIDLista As String, ByVal cNombreLista As String)
        Me.cLista = cIDLista
        Me.NombreLista = cNombreLista
    End Sub

    Public Sub New()
    End Sub

    Public Property cLista() As String
        Get
            Return m_Lista
        End Get
        Set(ByVal value As String)
            m_Lista = value
        End Set
    End Property
    Private m_Lista As String

    Public Property NombreLista() As String
        Get
            Return m_NombreLista
        End Get
        Set(ByVal value As String)
            m_NombreLista = value
        End Set
    End Property
    Private m_NombreLista As String

    Public Shared Function ListasPaging(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal dir As String, ByVal filter As String, ByVal IDSTORE As String) As Paging(Of Lista)

        Dim listas As List(Of Lista) = Lista.TestData(IDSTORE)

        If Not String.IsNullOrEmpty(filter) AndAlso filter <> "*" Then
            listas.RemoveAll(Function(listas__1) Not listas__1.NombreLista.ToLower().StartsWith(filter.ToLower()))
        End If

        If Not String.IsNullOrEmpty(sort) Then
            listas.Sort()
        End If

        If (start + limit) > listas.Count Then
            limit = listas.Count - start
        End If

        Dim rangeListas As List(Of Lista) = If((start < 0 OrElse limit < 0), listas, listas.GetRange(start, limit))

        Return New Paging(Of Lista)(rangeListas, listas.Count)
    End Function

    Public Shared ReadOnly Property TestData(ByVal IDSTORE As String) As List(Of Lista)
        Get

            Dim data As New List(Of Lista)()

            Dim dt As New DataTable
            Dim squery As String
            squery = "select ListID listnum,isnull(ListName,'') listname from storelist where AdminStoreID='" & IDSTORE & "' order by listnum"

            Dim odb As New DBMaster
            odb.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)


            dt = odb.EjecutaQry_Tabla(squery, CommandType.Text, "Listas", ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)

            For Each Drow As DataRow In dt.Rows

                Dim Lista As New Lista()

                Lista.cLista = Drow("listnum")
                Lista.NombreLista = Drow("listname")

                data.Add(Lista)

            Next

            Return data

        End Get
    End Property
End Class
