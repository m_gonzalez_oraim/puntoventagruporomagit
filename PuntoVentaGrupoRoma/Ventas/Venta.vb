﻿Public Class Venta
    Public Sub New(ByVal pId As String, _
                   ByVal pArticulo As String, _
                   ByVal pLinea As String, _
                   ByVal pMedida As String, _
                   ByVal pModelo As String, _
                   ByVal pCantidadesTienda As String, _
                   ByVal pCantidadesBodega As String, _
                   ByVal pLista As String, _
                   ByVal pPrecioUnitario As String, _
                   ByVal pIVA As String, _
                   ByVal pMonto As String, _
                   ByVal pDescuento As String, _
                   ByVal pTotal As String, _
                   ByVal pJuego As String, _
                   ByVal pAlmacenBox As String, _
                   ByVal pCantidadBox As String, _
                   ByVal pSubTotal As String _
                   )
        Id = pId
        Articulo = pArticulo
        Linea = pLinea
        Medida = pMedida
        Modelo = pModelo
        CantidadTienda = pCantidadesTienda
        CantidadBodega = pCantidadesBodega
        Lista = pLista '
        PrecioUnitario = pPrecioUnitario
        IVA = pIVA
        Monto = pMonto
        Descuento = pDescuento
        Total = pTotal
        Juego = pJuego
        AlmacenBox = pAlmacenBox
        CantidadBox = pCantidadBox
        subTotal = pSubTotal

    End Sub

    Private m_subTotal As String
    Public Property subTotal() As String
        Get
            Return m_subTotal
        End Get
        Set(ByVal value As String)
            m_subTotal = value
        End Set
    End Property

    Private m_CantidadBox As String
    Public Property CantidadBox() As String
        Get
            Return m_CantidadBox
        End Get
        Set(ByVal value As String)
            m_CantidadBox = value
        End Set
    End Property

    Private mAlmacenBox As String
    Public Property AlmacenBox() As String
        Get
            Return mAlmacenBox
        End Get
        Set(ByVal value As String)
            mAlmacenBox = value
        End Set
    End Property


    Public Property Total() As Double
        Get
            Return m_Total
        End Get
        Set(ByVal value As Double)
            m_Total = value
        End Set
    End Property
    Private m_Total As Double

    Public Property Descuento() As Double
        Get
            Return m_Cantidad
        End Get
        Set(ByVal value As Double)
            m_Cantidad = value
        End Set
    End Property
    Private m_Cantidad As Double

    Public Property Lista() As String
        Get
            Return m_Lista
        End Get
        Set(ByVal value As String)
            m_Lista = value
        End Set
    End Property
    Private m_Lista As String

    Public Property Juego() As String
        Get
            Return m_Juego
        End Get
        Set(ByVal value As String)
            m_Juego = value
        End Set
    End Property
    Private m_Juego As String

    Public Property PrecioUnitario() As String
        Get
            Return m_PrecioUnitario
        End Get
        Set(ByVal value As String)
            m_PrecioUnitario = value
        End Set
    End Property
    Private m_PrecioUnitario As String

    Public Property IVA() As String
        Get
            Return m_IVA
        End Get
        Set(ByVal value As String)
            m_IVA = value
        End Set
    End Property
    Private m_IVA As String

    Public Property Monto() As String
        Get
            Return m_Monto
        End Get
        Set(ByVal value As String)
            m_Monto = value
        End Set
    End Property
    Private m_Monto As String

    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer

    Public Property Articulo() As String
        Get
            Return m_Articulo
        End Get
        Set(ByVal value As String)
            m_Articulo = value
        End Set
    End Property
    Private m_Articulo As String

    Public Property Linea() As String
        Get
            Return m_DescripcionArticulo
        End Get
        Set(ByVal value As String)
            m_DescripcionArticulo = value
        End Set
    End Property
    Private m_DescripcionArticulo As String

    Public Property Medida() As String
        Get
            Return m_Medida
        End Get
        Set(ByVal value As String)
            m_Medida = value
        End Set
    End Property
    Private m_Medida As String


    Public Property Modelo() As String
        Get
            Return m_Modelo
        End Get
        Set(ByVal value As String)
            m_Modelo = value
        End Set
    End Property
    Private m_Modelo As String

    Public Property CantidadTienda() As String
        Get
            Return m_CantidadesTienda
        End Get
        Set(ByVal value As String)
            m_CantidadesTienda = value
        End Set
    End Property
    Private m_CantidadesTienda As String

    Public Property CantidadBodega() As String
        Get
            Return m_CantidadesBodega
        End Get
        Set(ByVal value As String)
            m_CantidadesBodega = value
        End Set
    End Property
    Private m_CantidadesBodega As String
End Class

Public Class Pago

    Public Sub New(ByVal pId As String, ByVal pFormaPago As String, ByVal pMontoPagado As String, ByVal pNoCuenta As String)
        Id = pId
        FormaPago = pFormaPago '
        MontoPagado = pMontoPagado '
        NoCuenta = pNoCuenta
    End Sub

    Public Property Id() As String
        Get
            Return m_Id
        End Get
        Set(ByVal value As String)
            m_Id = value
        End Set
    End Property
    Private m_Id As Integer


    Public Property FormaPago() As String
        Get
            Return m_FormaPago
        End Get
        Set(ByVal value As String)
            m_FormaPago = value
        End Set
    End Property
    Private m_FormaPago As String

    Public Property MontoPagado() As Double
        Get
            Return m_MontoPagado
        End Get
        Set(ByVal value As Double)
            m_MontoPagado = value
        End Set
    End Property
    Private m_MontoPagado As Double

    Public Property NoCuenta() As String
        Get
            Return m_NoCuenta
        End Get
        Set(ByVal value As String)
            m_NoCuenta = value
        End Set
    End Property
    Private m_NoCuenta As String

End Class
