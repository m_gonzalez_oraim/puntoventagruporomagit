﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="PuntoVentaGrupoRoma.Facturacion2" Codebehind="Facturacion2.aspx.vb" MasterPageFile="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
           
    <ext:XScript ID="XScript1" runat="server">
                                           
    <script type="text/javascript">

        function SeConfirmoCreacionCliente() {
            App.direct.UIFormularioClientesCrear();
        }

        function muestraPreciosFactor(hdnDefinicionFactores) {
            Ext.getCmp("WindowPreciosFactores").show();
            var defFactores = hdnDefinicionFactores.getValue().split("|")
            var numeroFactor = 1
            for(i=0;i<defFactores.length;i+=2)
            {
                Ext.getCmp("PrecioFactor" + numeroFactor).setBoxLabel(defFactores[i+1] +"-"+ defFactores[i]);
                Ext.getCmp("PrecioFactor" + numeroFactor).setValue(defFactores[i]);
                numeroFactor = numeroFactor +1;
            }
        }

        function seleccionarPrecioFactor(hdnDefinicionFactores,numeroFactor, letraFactor) {
            var defFactores = hdnDefinicionFactores.getValue().split("|")
            alert(defFactores[numeroFactor * 2]);
            Ext.getCmp("WindowPreciosFactores").close();
        }

        function ResetearForm(form) {
            form.getForm().reset();
            form.items.items[9].setValue("");
            form.items.items[10].setValue("");
            form.items.items[11].setValue("");
            form.items.items[8].setValue("");
            if (form.items.items[8].store != undefined) {
                form.items.items[8].store.removeAll();
            }
        }

        function InicializarValoresCombo(combo, numeroRegistros) {
            for (i = 0; i < numeroRegistros; i++)
                combo.removeByIndex(0)
        }

        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + '$' + num + '.' + cents);
        }
    
        var validate = function (e) {
            /*
            Properties of 'e' include:
                e.grid - This grid
                e.record - The record being edited
                e.field - The field name being edited
                e.value - The value being set
                e.originalValue - The original value for the field, before the edit.
                e.row - The grid row index
                e.column - The grid column index
            */
            // Call DirectMethod
            e.value = null;
        };

        var MetodoClienteSeleccionado = {
            javaCargarInformacionClienteSeleccionado: function (grid) {
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                });
                App.direct.CargarInformacionClienteSeleccionado(data);
            }
        }

        var MetodoClienteSeleccionado2 = {
            javaSeleccionarCliente: function (grid) {
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                });
                App.direct.SeleccionarClienteDobleClic(data);
            }
        }

        var MetodoArticuloSeleccionado = {
            javaCargarInformacionArticuloSeleccionado: function (grid) {
                var porcentajeImpuesto = Ext.getCmp("hdnPorcentajeImpuesto").getValue();
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                    data.push(item.data.Nombre)
                    data.push(porcentajeImpuesto)
                });
                App.direct.CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(data);
            }
        }

        var MetodoDetalleCambioFactor = {
            ControlarCambioFactor: function (idRegistro) {

                var precioImpuesto = Ext.getCmp("cmbFactor" + idRegistro).getValue();
                Ext.getCmp("txtPrecioImpuesto" + idRegistro).setValue(precioImpuesto);
                Ext.getCmp("hdnPrecioImpuesto" + idRegistro).setValue(precioImpuesto);
                
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var porcentajeImpuesto = Ext.getCmp("hdnPorcentajeImpuesto").getValue();
                var importeLineaImpuestoActual = Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).getValue();
                var importeLineaActual = Ext.getCmp("hdnTotalLinea" + idRegistro).getValue();
                var subTotal = Ext.getCmp("txtSubtotalDocumento").getValue();
                var descuentoGlobal = Ext.getCmp("txtDescuentoGlobal").getValue();
                var subTotal2 = Ext.getCmp("txtSubtotalDescuento").getValue();
                var impuestos = Ext.getCmp("txtIvaDocumento").getValue();
                var total = Ext.getCmp("txtTotalDocumento").getValue();

                App.direct.RecalcularTotalesSeleccionLineaGridCambioPrecio(precioImpuesto, cantidad, sujetoAImpuesto, descuento, porcentajeImpuesto,importeLineaImpuestoActual, importeLineaActual, subTotal, descuentoGlobal, subTotal2, impuestos, total, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecioImpuesto" + idRegistro).setValue(precioImpuesto);
                        Ext.getCmp("txtPrecio" + idRegistro).setValue(result.PrecioSinImpuesto);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("txtSubtotalDocumento").setValue(result.SubTotal);
                        Ext.getCmp("txtSubtotalDescuento").setValue(result.SubTotal2);
                        Ext.getCmp("txtIvaDocumento").setValue(result.Impuestos);
                        Ext.getCmp("txtTotalDocumento").setValue(result.Total);
                        Ext.getCmp("txtMontoCambio").setValue(result.Cambio);
                    }
                });
            }
        }

    
        var MetodoDetalleCambioUnidad = {
            ControlarCambioUnidadMedida: function (idRegistro) {
                var comboUnidad = Ext.getCmp("cmbUnidadMedida" + idRegistro);
                var comboFactores = Ext.getCmp("cmbFactor" + idRegistro);
                var codigoArticulo = Ext.getCmp("txtCodigoArticulo" + idRegistro).getValue();
                var factoresUsuario = Ext.getCmp("hdnFactoresUsuario").getValue();
                var factoresTienda = Ext.getCmp("hdnFactoresTienda").getValue();
                var precioCosto = Ext.getCmp("hdnPrecioCosto" + idRegistro).getValue();
                var numeroFactores = Ext.getCmp("hdnItemsComboFactor" + idRegistro).getValue();
                var factorArticulo = Ext.getCmp("hdnFactorArticulo" + idRegistro).getValue();
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var valorUnidadMedida = comboUnidad.getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = Ext.getCmp("hdnPorcentajeImpuesto").getValue();
                var importeLineaImpuestoActual = Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).getValue();
                var importeLineaActual = Ext.getCmp("hdnTotalLinea" + idRegistro).getValue();
                var subTotal = Ext.getCmp("txtSubtotalDocumento").getValue();
                var descuentoGlobal = Ext.getCmp("txtDescuentoGlobal").getValue();
                var subTotal2 = Ext.getCmp("txtSubtotalDescuento").getValue();
                var impuestos = Ext.getCmp("txtIvaDocumento").getValue();
                var total = Ext.getCmp("txtTotalDocumento").getValue();

                App.direct.ControlarCambioUnidadDeMedidaGrid(codigoArticulo, factorArticulo, factoresUsuario, factoresTienda, valorUnidadMedida, precioCosto, cantidad, descuento, sujetoAImpuesto, porcentajeImpuesto, importeLineaImpuestoActual, importeLineaActual, subTotal, descuentoGlobal, subTotal2, impuestos, total, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecioMinimoAutorizacion" + idRegistro).setValue(result.PrecioMinimoAutorizacion);
                        Ext.getCmp("hdnPrecioMaximoAutorizacion" + idRegistro).setValue(result.PrecioMaximoAutorizacion);
                        InicializarValoresCombo(comboFactores, numeroFactores);
                        for (i = 0; i < result.Factores.length; i++) {
                            comboFactores.insertItem(i, result.Factores[i].Descripcion, result.Factores[i].Precio);
                        }
                        comboFactores.setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtPrecioImpuesto" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtPrecio" + idRegistro).setValue(result.PrecioPrimerFactorSinImpuesto);
                        Ext.getCmp("hdnPrecioImpuesto" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("hdnItemsComboFactor" + idRegistro).setValue(result.Factores.length);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("txtSubtotalDocumento").setValue(result.SubTotal);
                        Ext.getCmp("txtSubtotalDescuento").setValue(result.SubTotal2);
                        Ext.getCmp("txtIvaDocumento").setValue(result.Impuestos);
                        Ext.getCmp("txtTotalDocumento").setValue(result.Total);
                    }
                });
            }
        }



        var MetodoDetalleRecalcularTotales = {
            RecalcularTotalesSeleccionLineaGrid: function (idRegistro) {
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var ultimaCantidad = Ext.getCmp("hdnCantidad" + idRegistro).getValue();
                var descuentoLinea = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var ultimoDescuentoLinea = Ext.getCmp("hdnDescuento" + idRegistro).getValue();

                if (cantidad == ultimaCantidad && descuentoLinea == ultimoDescuentoLinea)
                    return;

                var precio = Ext.getCmp("txtPrecio" + idRegistro).getValue();
                var precioImpuesto = Ext.getCmp("txtPrecioImpuesto" + idRegistro).getValue();
                var importeLineaImpuestoActual = Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).getValue();
                var importeLineaActual = Ext.getCmp("hdnTotalLinea" + idRegistro).getValue();
                var subTotal = Ext.getCmp("txtSubtotalDocumento").getValue();
                var descuentoGlobal = Ext.getCmp("txtDescuentoGlobal").getValue();
                var subTotal2 = Ext.getCmp("txtSubtotalDescuento").getValue();
                var impuestos = Ext.getCmp("txtIvaDocumento").getValue();
                var total = Ext.getCmp("txtTotalDocumento").getValue();

                App.direct.RecalcularTotalesSeleccionLineaGrid(cantidad, precio, precioImpuesto, descuentoLinea, importeLineaImpuestoActual, importeLineaActual, subTotal, descuentoGlobal, subTotal2, impuestos, total,  {
                    success: function (result) {
                        Ext.getCmp("hdnCantidad" + idRegistro).setValue(cantidad);
                        Ext.getCmp("hdnDescuento" + idRegistro).setValue(descuentoLinea);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("txtSubtotalDocumento").setValue(result.SubTotal);
                        Ext.getCmp("txtSubtotalDescuento").setValue(result.SubTotal2);
                        Ext.getCmp("txtIvaDocumento").setValue(result.Impuestos);
                        Ext.getCmp("txtTotalDocumento").setValue(result.Total);
                    }
                });
            }
        }

        var MetodoDetalleRecalcularTotalesCambioPrecio = {
            RecalcularTotalesSeleccionLineaGrid: function (idRegistro) {
                var precioImpuesto = Ext.getCmp("txtPrecioImpuesto" + idRegistro).getValue();
                var ultimoPrecioImpuesto = Ext.getCmp("hdnPrecioImpuesto" + idRegistro).getValue();

                if (precioImpuesto == ultimoPrecioImpuesto)
                    return;

                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var porcentajeImpuesto = Ext.getCmp("hdnPorcentajeImpuesto").getValue();
                var importeLineaImpuestoActual = Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).getValue();
                var importeLineaActual = Ext.getCmp("hdnTotalLinea" + idRegistro).getValue();
                var subTotal = Ext.getCmp("txtSubtotalDocumento").getValue();
                var descuentoGlobal = Ext.getCmp("txtDescuentoGlobal").getValue();
                var subTotal2 = Ext.getCmp("txtSubtotalDescuento").getValue();
                var impuestos = Ext.getCmp("txtIvaDocumento").getValue();
                var total = Ext.getCmp("txtTotalDocumento").getValue();

                App.direct.RecalcularTotalesSeleccionLineaGridCambioPrecio(precioImpuesto, cantidad, sujetoAImpuesto, descuento, porcentajeImpuesto, importeLineaImpuestoActual, importeLineaActual, subTotal, descuentoGlobal, subTotal2, impuestos, total, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecioImpuesto" + idRegistro).setValue(precioImpuesto);
                        Ext.getCmp("txtPrecio" + idRegistro).setValue(result.PrecioSinImpuesto);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("txtSubtotalDocumento").setValue(result.SubTotal);
                        Ext.getCmp("txtSubtotalDescuento").setValue(result.SubTotal2);
                        Ext.getCmp("txtIvaDocumento").setValue(result.Impuestos);
                        Ext.getCmp("txtTotalDocumento").setValue(result.Total);
                    }
                });
            }
        }

    </script>
    <script type="text/javascript">
        var updateRecord = function (form) {
            if (form.record == null) {
                return;
            }

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }
            form.getForm().updateRecord(form.record);
        };

        function add(a, b) {
            return (a + b).toFixed(2);
        }
        function subtract(a, b) {
            return (b - a).toFixed(2);
        }

        function IsDecimal(data) {
            var sFullNumber = data;
            var ValidChars = "0123456789.";
            var Validn = "0123456789";
            var IsDotPres = false;
            var Char;
            Char = sFullNumber.charAt(0);
            if (Validn.indexOf(Char) == -1) {
                alert("Please enter proper value.");
                data.select();
                return false;
            }
            else {
                for (i = 0; i < sFullNumber.length; i++) {
                    Char = sFullNumber.charAt(i);
                    if (Char == '.') {
                        if (IsDotPres == false)
                            IsDotPres = true;
                        else {
                            alert("Please remove extra '.' or spaces.");
                            data.select();
                            return false;
                        }
                    }
                    if (ValidChars.indexOf(Char) == -1) {
                        alert("Please check once again you entered proper value are not.");
                        data.select();
                        return false;
                    }
                }
            }
            return true;
        }

        var quitarPago = function (grid, txtTotalDocumento, txtMontoPagoActual, txtCambio) {
            if (!grid.getSelectionModel().hasSelection()) {
                return;
            }

            var row = grid.getSelectionModel().getSelection()[0];
            var montoPago = row.get("MontoPago");
            App.direct.ActualizarMontoPago(txtTotalDocumento.getValue(), txtMontoPagoActual.getValue(), montoPago, {
                success: function (result) {
                    txtMontoPagoActual.setValue(result.TotalPago);
                    txtCambio.setValue(result.Cambio);
                    grid.deleteSelected();
                }
            });            
        }

        var agregarPago = function (form, grid,txtTotalDocumento,txtMontoPagoActual,txtCambio) {
            if (!form.getForm().isValid()) {

                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Datos incompletos para la forma de pago seleccionada",
                    title: "Error"
                });

                return false;
            }
            var valores = form.getForm().getFieldValues(false, "dataIndex");
            
            alert(valores);
            App.direct.ActualizarMontoPago(txtTotalDocumento.getValue(),txtMontoPagoActual.getValue(),  0, {
                success: function (result) {
                    txtMontoPagoActual.setValue(result.TotalPago);
                    txtCambio.setValue(result.Cambio);
                    grid.insertRecord(0, valores);
                    grid.getView().refresh();
                    form.getForm().reset();
                }
            });
        };

    </script>
    <script type="text/javascript">
        var updateClient = function (form) {
            if (form.record == null) {
                return;
            }

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }

            form.getForm().updateClient(form.record);
        };

        var addCliente = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            //grid.insertRecord(grid.store.data.length + 1, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);
        };

        var prepareaddCliente = function (form, grid) {

            if ((form.items.items[0].getValue() <= 777051040) && (form.items.items[0].getValue() != "")) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("Ya esta registrado el Cliente No. " + form.items.items[0].getValue()),
                    title: "Advertencia"
                });
                form.getForm().reset();
            }


            if (form.items.items[0].getValue() > 777051050 && form.items.items[0].getValue() !== "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("No se encontro el Cliente No. " + form.items.items[0].getValue()),
                    title: "Advertencia"
                });
                form.getForm().reset();
            }

            //var values = form.getForm().getFieldValues();  
            if (form.items.items[0].getValue() == "") {
                form.getForm().reset();
            }
            else {
                var randomnumber = Math.floor(Math.random() * 9) * 1000000
                var div = form.items.items[0].getValue() / randomnumber;
                //form.items.items[1].setValue("1000");
                //form.items.items[2].setValue("1000");
                //form.items.items[3].setValue("Pendiente");
                form.items.items[2].setValue('Lista de precios 01');
                form.items.items[1].setValue('Cliente ' + form.items.items[0].getValue());
                form.items.items[3].setValue(div);
                form.items.items[4].setValue(div * .16);
                form.items.items[5].setValue("Ninguna");
            }

        };

        var addCliente2 = function (form, grid) {

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            //var values = form.getForm().getFieldValues();  
            var values = form.getForm().getFieldValues(false, "dataIndex")
            //var i = 0
            //for(var field in form.getForm().getValues())  
            //{   
            //alert(field+':'+values[field]);  
            //i=i+1;
            //values[field] = "1";
            //var obj = form.getForm().elements[field];
            //alert(obj);  
            //}            

            //grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
        };
    </script>
    
    </ext:XScript>
        <ext:Store
            ID="StoreVendedores"
            runat="server">
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="Descripcion"  />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store
            ID="StoreArticulos"
            runat="server"
            RemotePaging="true"
            RemoteSort="true"
            PageSize="20">
            <Proxy>
                <ext:PageProxy DirectFn="App.direct.ControlPaginacionArticulos" />
            </Proxy>
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="Nombre" />
                        <ext:ModelField Name="Existencia" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false">
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="NombreCompleto"  />
                        <ext:ModelField Name="Nombre"  />
                        <ext:ModelField Name="ApPaterno"  />
                        <ext:ModelField Name="ApMaterno"  />
                        <ext:ModelField Name="NombreComercial" />
                        <ext:ModelField Name="RFC" />
                        <ext:ModelField Name="CalleNumero"  />
                        <ext:ModelField Name="Calle"  />
                        <ext:ModelField Name="NumeroExterior"  />
                        <ext:ModelField Name="NumeroInterior"  />
                        <ext:ModelField Name="Colonia"  />
                        <ext:ModelField Name="CodigoPostal"  />
                        <ext:ModelField Name="Municipio"  />
                        <ext:ModelField Name="Estado"  />
                        <ext:ModelField Name="TelCasa"  />
                        <ext:ModelField Name="TelOficina" />
                        <ext:ModelField Name="Correo"  />
                        <ext:ModelField Name="FechaNacimiento"  />
                    </Fields>
                </ext:Model>
            </Model>            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>        
    <ext:Panel runat="server" Flex="1" Layout="AnchorLayout" Width="1350" Height="35">
        <Items>
            <ext:FormPanel runat="server" Width="1350"  Layout="HBoxLayout" Frame="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cmbVendedor" FieldLabel="Vendedor" DisplayField="Descripcion" ValueField="Codigo" StoreID="StoreVendedores" />                        
                    <ext:Label runat="server" Width="40" />
                    <ext:ComboBox runat="server" ID="cmbComprobante" FieldLabel="Comprobante">
                        <Items>
                            <ext:ListItem Value="T" Text="Ticket" />
                            <ext:ListItem Value="F" Text="Factura" />
                        </Items>
                    </ext:ComboBox>
                    <ext:Label runat="server" Width="40" />
                    <ext:DateField runat="server" ID="txtFecha" FieldLabel="Fecha" LabelAlign="Left" />
                </Items>
            </ext:FormPanel>
        </Items>

    </ext:Panel>
    <ext:Panel ID="BorderLayoutMain" runat="server" Flex="1" Layout="BorderLayout" Width="1350" Height="442" Border="false" >
        <Items>
        <ext:Panel 
            ID="pnlWest" 
            runat="server" 
            Layout="Border"
            Region="West"
            AutoScroll="true"
            Width="300">
            <Items>
                <ext:FormPanel 
                ID="FrmClienteVenta" 
                runat="server"
                Layout="AnchorLayout"
                Width="350"  
                Region="North"
                LabelAlign="Right"
                Frame="true">
                    <Items>            
                        <ext:Hidden 
                            ID="hdnCodigoClienteVenta"
                            runat="server"
                            FieldLabel="CodigoCliente"  />
                        <ext:Container runat="server" Layout="HBoxLayout" PaddingSpec="0 0 4 0">
                            <Items>
                                <ext:TextField 
                                    ID="txtNombreClienteVenta" 
                                    runat="server"
                                    FieldLabel="Cliente"
                                    Enabled="false"
                                    ReadOnly="true"
                                    LabelWidth="60"
                                    Width="260"/>                   
                                <ext:Button 
                                    ID="btnIniciarBusquedaCliente" 
                                    runat="server"
                                    Icon="Find"> 
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaClientes" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Container>
                        <ext:TextField 
                            ID="txtRFCVenta" 
                            runat="server"
                            FieldLabel="RFC"
                            Enabled="false"
                            ReadOnly ="true"
                            LabelWidth="60"
                            Width="260" />
                        <ext:TextArea 
                            ID="txtDireccionVenta" 
                            runat="server"
                            FieldLabel="Direccion"                    
                            Cols="30"
                            LabelWidth="60"
                            Enabled ="false"
                            ReadOnly="true"
                            />
                    </Items>
                <Buttons>
                
                </Buttons>
            </ext:FormPanel>       
            <ext:FormPanel 
                ID="FormSeleccionArticulo" 
                runat="server"
                Layout="AnchorLayout"
                LabelAlign="Right"
                AutoRender ="true" 
                Width="350"
                Region="Center"
                Frame="true">
                    <Items>  
                        <ext:FieldContainer 
                            Layout="HBoxLayout" 
                            runat="server" >
                            <Items>
                                <ext:Label 
                                    runat="server"
                                    Text="Articulo"
                                    Width="65" />
                                <ext:ComboBox
                                    Id="cmbBusquedaArticulo"
                                    LabelSeparator=""
                                    runat="server"  
                                    DisplayField="Nombre"
                                    ValueField="Codigo"
                                    TypeAhead="False"
                                    Width="200"
                                    PageSize="10"
                                    HideBaseTrigger="true"
                                    MinChars="4"
                                    TriggerAction="Query" >
                                        <ListConfig 
                                            LoadingText="Buscando">
                                            <ItemTpl runat="server">
                                                <Html>
                                                    <div>
                                                        <span style="font-weight:bolder">{Codigo}</span>|{Existencia}<br />
                                                        <span style="font-size:x-small;font-style:italic">{Nombre}</span>
                                                    </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store runat="server" AutoLoad="false">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../Ajax/ListadoArticulos.ashx">
                                                        <ActionMethods Read="POST" />

                                                        <Reader>
                                                            <ext:JsonReader Root="Articulos" TotalProperty="total" />
                                                        </Reader>                                                        
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Model>
                                                    <ext:Model runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Codigo" />
                                                            <ext:ModelField Name="Nombre" />
                                                            <ext:ModelField Name="Existencia"  />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    <DirectEvents>
                                        <Select OnEvent="CargarInformacionArticuloSeleccionadoDeBuscador" />
                                    </DirectEvents>
                                    </ext:ComboBox>
                                <ext:Button runat="server" Icon="Find">
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaBusquedaArticulos" />
                                    </DirectEvents>
                                </ext:Button>
                                </Items>
                            </ext:FieldContainer>
                            <ext:Hidden
                                ID="hdnCodigoArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnFactorArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnNombreArticulo"
                                runat="server"/>
                        <ext:Hidden
                            ID="hdnSujImp"
                            runat="server" />
                    <ext:ComboBox
                        ID="cmbUnidadMedida"
                        runat="server"
                        FieldLabel="Uni. Med."
                        LabelWidth="60"
                        AutoRender="true"
                        Width="288"
                        LabelSeparator="">
                        <DirectEvents>
                            <Select OnEvent="ControlarCambioUnidadMedida" />
                        </DirectEvents>
                        </ext:ComboBox>
                    <ext:Hidden 
                        ID="hdnItemsComboUnidadMedida"
                        runat="server"
                        Text="0" />
                    <ext:NumberField
                        ID="txtCantidad"
                        runat="server"
                        FieldLabel="Cantidad" 
                        LabelWidth="60"
                        Width="288"
                        LabelSeparator="" 
                        HideTrigger="true"
                        DecimalPrecision="2">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:Hidden
                        ID="hdnNumeroFactores"
                        runat="server" 
                        Text="0" 
                        ClientIDMode="Static"/>
                    <ext:Hidden
                        id="hdnDefinicionFactores"
                        runat="server"
                        text="" 
                        ClientIDMode="Static" />
                    <ext:TextField
                        ID="txtFactor"
                        runat="server"
                        FieldLabel="Factor"
                        LabelWidth="60"
                        Width="288"
                        Size="1">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ muestraPreciosFactor(#{hdnDefinicionFactores});}" />
                        </Listeners>
                    </ext:TextField>
                    <ext:Hidden
                        ID="hdnFactor"
                        runat="server" />
                    <ext:NumberField
                        ID="txtPrecio"
                        runat="server"
                        FieldLabel="P Base"
                        LabelWidth="60"
                        Width="288"
                        AllowDecimals="true"
                        DecimalPrecision="4"
                        ReadOnly="true"
                        HideTrigger="true">
                    </ext:NumberField>
                    <ext:NumberField
                        ID="txtPrecioImpuesto"
                        ClientIDMode="Static"
                        runat="server"
                        FieldLabel="Precio"
                        LabelWidth="60"
                        Width="288"
                        AllowDecimals="true"
                        DecimalPrecision="4"
                        HideTrigger="true" 
                        EnableKeyEvents="true">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLineaCambioPrecio" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:NumberField
                        ID="txtDescuento"
                        runat="server"
                        FieldLabel="% Desc"
                        LabelWidth="60"
                        Width="288"
                        AllowDecimals="true"
                        DecimalPrecision="2"
                        HideTrigger="true"
                        MinValue="0"
                        MaxValue="100">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:TextArea
                        ID="txtComentariosLinea"
                        runat="server"
                        FieldLabel="Comen."
                        LabelWidth="60"
                        Width="288"
                        Rows="2" />
                    <ext:NumberField
                        ID="txtTotalLineaImpuesto"
                        runat="server"
                        FieldLabel="Total"
                        LabelWidth="60"
                        Width="288" 
                        ReadOnly ="true"
                        DecimalPrecision="2"
                        />
                        <ext:Hidden 
                            ID="hdnPrecioMinimoAutorizacion"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnPrecioMaximoAutorizacion"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnPrecioCosto"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnTotalLinea"
                            runat="server" />
                    <ext:Button 
                        ID="BtnAgregarLinea" 
                        runat="server"
                        Text="+ Linea"
                        Icon="Add"> 
                        <DirectEvents>
                            <Click OnEvent="AgregarLineaVenta" />
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:FormPanel>
        </Items>            
        </ext:Panel>
        
        <ext:Panel ID="pnlCenter" 
            runat="server" 
            AutoScroll="true"
            Layout="BorderLayout"
            Region="Center"
            Width="900">
            <Items>
         <ext:Panel 
            runat="server" 
            Frame="true"
            Layout="TableLayout"
            PaddingSummary="5px 5px 0"
            Width="1100">
             <LayoutConfig>
                 <ext:TableLayoutConfig Columns="3" />
             </LayoutConfig>
            <Items>
                <ext:Panel         
                    runat="server"    
                    Width="1000" 
                    Layout="AnchorLayout"
                    Id="PanelEncabezado"
                    ColSpan="3">
                <Items>
                    <ext:Container runat="server" Width="750" Layout="HBoxLayout">
                        <Items>
                            <ext:Label runat="server" ID="HeaderGridCodigo" Text="Codigo" Width="100" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridNombre" Text="Articulo" Width="200" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridUnidad" Text="Unidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridCantidad" Text="Cantidad" Width="60" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridFactor" Text="Factor" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridPrecio" Text="$ Unitario" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridPrecioImpuesto" Text="$ IVA" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridDescuento" Text="Descuento" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridImporte" Text="Importe" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridComentarios" Text="Comentarios" Width="200" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>
            <ext:Panel         
                    runat="server"    
                    Width="1000" 
                    Height="180"
                    Layout="AnchorLayout"
                    Id="PanelDetalleDocumento"
                    AutoScroll="true"
                    ColSpan="3" >
            </ext:Panel>

            <ext:Panel
                runat="server"
                Boder="false"
                Frame="true"
                Width="300"
                Height="210" 
                Layout="AnchorLayout">
                <Items>
                    <ext:TextArea 
                        ID="txtComentarios" 
                        runat="server" 
                        FieldLabel="Comentarios" 
                        Rows="4"
                        LabelWidth="80"
                        Width="290" />
                </Items>
            </ext:Panel>
            <ext:Panel
                runat="server"
                Border="false"
                Frame="true"
                Width="400"
                Height="210"
                Layout="VBoxLayout">
                <Items>
                    <ext:TextField 
                        runat="server" 
                        FieldLabel="SubTotal" 
                        ID="txtSubtotalDocumento" 
                        ClientIDMode="Static" 
                        ReadOnly="true"
                        LabelWidth="80"
                        Width="200"
                    />
                    <ext:TextField
                        runat="server"
                        FieldLabel="Descuento"
                        ID="txtDescuentoGlobal"
                        ClientIDMode="Static"
                        ReadOnly="true"
                        LabelWidth="80"
                        Width="200" />
                    <ext:TextField
                        runat="server"
                        FieldLabel="Subtotal 2"
                        ID="txtSubtotalDescuento"
                        ClientIDMode="Static"
                        ReadOnly="true"
                        LabelWidth="80"
                        Width="200" />
                    <ext:TextField 
                        runat="server" 
                        FieldLabel="IVA" 
                        ID="txtIvaDocumento" 
                        ClientIDMode="Static" 
                        ReadOnly="true"
                        LabelWidth="80"
                        Width="200"
                    />
                    <ext:TextField 
                        runat="server" 
                        FieldLabel="Total" 
                        ID="txtTotalDocumento" 
                        ClientIDMode="Static" 
                        ReadOnly="true"
                        LabelWidth="80"
                        Width="200"
                    />
                </Items>
            </ext:Panel>
            </Items>
        </ext:Panel>
        
                <ext:Hidden 
                            ID="hdnPorcentajeImpuesto" ClientIDMode="Static"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnListaPrecioCosto"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnListaPrecioVenta"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnFactoresUsuario" ClientIDMode="Static"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnFactoresTienda" ClientIDMode="Static"
                            runat="server" />

            </Items>
        </ext:Panel>
        </Items>
    </ext:Panel>        
        <ext:Window 
            title="Buscar Cliente" 
            runat="server" 
            ID="winClientes" 
            Modal="true" 
            Hidden="True" 
             Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content>   
                <ext:Panel 
                    ID="PanelIzquierda" 
                    runat="server" 
                    Width="300" 
                    Split="true"             
                    DefaultBorder="false"            
                    Layout="Border"
                    Region="West"
                    AutoScroll="true">   
                    <Items>
                        <ext:FieldContainer 
                            runat="server" 
                            FieldLabel="Buscar" 
                            LabelSeparator="" 
                            LabelStyle="font-weight:bolder;text-align:center" 
                            LabelAlign="Top"   
                            Layout="HBoxLayout" 
                            Region="North" 
                            DefaultMargins="0 0 15 0"
                            >
                            <Items>
                                <ext:ComboBox 
                                    ID="cmbTipoBuquedaCliente"
                                    runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Value="N" Text="Nombre" />
                                        <ext:ListItem Value="R" Text="RFC"  />
                                    </Items>
                                </ext:ComboBox>
                                <ext:TextField 
                                    ID="txtValorBusquedaCliente" 
                                    runat="server"
                                    AllowBlank="false"
                                    Enabled ="false" />
                                 <ext:Button 
                                    ID="btnBuscarCliente" 
                                    runat="server" 
                                    Icon="Find" >
                                        <DirectEvents>
                                            <Click OnEvent="BuscarCliente" />
                                        </DirectEvents>
                                    </ext:Button>
                            </Items>
                        </ext:FieldContainer>
                        <ext:Toolbar runat="server">
                            <Items>

                            </Items>
                        </ext:Toolbar>
                        <ext:FormPanel 
                            ID="FormCliente" 
                            runat="server"
                            LabelAlign="Right"
                            ButtonAlign="Center"
                            Layout="AnchorLayout"
                            Region="Center" >
                            <Buttons>           
                                <ext:Button 
                                    ID="btnGuardarCliente" 
                                    runat="server"
                                    Text="Save"
                                    Icon="Disk">
                                    <DirectEvents>
                                        <Click OnEvent="ActualizarCliente" />
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button 
                                    ID="btnCrearCliente" 
                                    runat="server"
                                    Text="Crear"
                                    Icon="ReportAdd">   
                                    <DirectEvents>
                                        <Click OnEvent="CrearCliente" />
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button 
                                    ID="btnSeleccionarCliente" 
                                    runat="server" 
                                    Text="Seleccionar" 
                                    Icon="Accept">
                                    <DirectEvents>
                                        <Click OnEvent="SeleccionarCliente" />
                                    </DirectEvents>
                                </ext:Button>
                            </Buttons>
                    <Items>
                        <ext:Hidden
                            ID="hdnCodigoCliente"
                            runat="server"
                            DataIndex="Codigo" />
                        <ext:TextField 
                            ID="txtNombreCliente" 
                            runat="server"
                            FieldLabel="Nombre"
                            DataIndex="Nombre"
                            AnchorHorizontal="100%"/>
                        <ext:TextField
                            ID="txtApPaternoCliente" 
                            runat="server"
                            FieldLabel="A. Paterno"
                            DataIndex="ApPaterno"
                            AnchorHorizontal="100%" />
                        <ext:TextField 
                            ID="txtApMaternoCliente" 
                            runat="server"
                            FieldLabel="A. Materno"
                            DataIndex="ApMaterno"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNombreComercialCliente"
                            runat="server"
                            FieldLabel="N. Comercial"
                            DataIndex="NombreComercial"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtRFCCliente"
                            runat="server"
                            FieldLabel="RFC"
                            DataIndex="RFC"
                            AnchorHorizontal="100%" />
                         <ext:TextField
                            ID="txtCalleCliente"
                            runat="server"
                            FieldLabel="Calle"
                            DataIndex="Calle"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNumExtCliente"
                            runat="server"
                            FieldLabel="Numero Ext"
                            DataIndex="NumeroExterior"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNumIntCliente" 
                            runat="server"
                            FieldLabel="Numero Int"
                            DataIndex="NumeroInterior"
                            AnchorHorizontal="100%" />
                         <ext:TextField
                            ID="txtColoniaCliente"
                            runat="server"
                            FieldLabel="Colonia"
                            DataIndex="Colonia"                 
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtCPCliente"
                            runat="server"
                            FieldLabel="Codigo Postal"
                            DataIndex="CodigoPostal"                  
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtMunicipioCliente"
                            runat="server"
                            FieldLabel="Municipio"
                            DataIndex="Municipio"                 
                            AnchorHorizontal="100%" />
                        <ext:ComboBox
                            ID="cmbEstadoCliente"
                            runat="server"
                            FieldLabel="Estado"
                            DataIndex="Estado"              
                            AnchorHorizontal="100%">              
                        </ext:ComboBox>
                        <ext:TextField 
                            ID="txtTelCasaCliente" 
                            runat="server"
                            FieldLabel="Tel Casa"
                            DataIndex="TelCasa"
                            AnchorHorizontal="100%"  />
                        <ext:TextField 
                            ID="txtTelOficinaCliente" 
                            runat="server"
                            FieldLabel="Tel Oficina"
                            DataIndex="TelOficina"
                            AnchorHorizontal="100%"  />
                        <ext:TextField 
                            ID="txtCorreoCliente" 
                            runat="server"
                            FieldLabel="Correo"
                            DataIndex="Correo"
                            AnchorHorizontal="100%" />
                         <ext:DateField 
                             ID="txtFechaNacimientoCliente" 
                             runat="server"
                             FieldLabel="F. Nacimiento"                
                             DataIndex="FechaNacimiento"
                             AnchorHorizontal="100%"  />
                    </Items>
                </ext:FormPanel>
            </Items>
            </ext:Panel>        
                    <ext:GridPanel 
                        ID="GridCliente" 
                        runat="server"
                        Icon="Table"
                        Title="Clientes"
                        AutoRender ="true" 
                        StoreID="StoreClientes" 
                        AutoScroll="true" 
                        Layout="FitLayout"
                        Width="680" 
                        Region="West">            
                        <ColumnModel>
                            <Columns>
                                <ext:Column runat="server" Header="Id" DataIndex="Codigo" Hidden="true" />
                                <ext:Column runat="server" Header="Nombre" DataIndex="NombreCompleto" Width="250" />
                                <ext:Column runat="server" DataIndex="Nombre" Hidden="true" />
                                <ext:Column runat="server" DataIndex="ApPaterno" Hidden="true" />
                                <ext:Column runat="server" DataIndex="ApMaterno" Hidden="true" />
                                <ext:Column runat="server" Header="Nombre Comercial" DataIndex="NombreComercial" Width="250" />
                                <ext:Column runat="server" Header="RFC" DataIndex="RFC" Width="80" />
                                <ext:Column runat="server" Header="Calle/Numero" DataIndex="CalleNumero" Width="500" />
                                <ext:Column runat="server" DataIndex="Calle" Hidden="true" />
                                <ext:Column runat="server" DataIndex="NumeroExterior" Hidden="true" />
                                <ext:Column runat="server" DataIndex="NumeroInterior" Hidden="true" />
                                <ext:Column runat="server" Header="Colonia" DataIndex="Colonia" />
                                <ext:Column runat="server" Header="CP" DataIndex="CodigoPostal" Width="30" />
                                <ext:Column runat="server" DataIndex="Municipio" Hidden="true" />
                                <ext:Column runat="server" DataIndex="Estado" Hidden="true" />
                                <ext:Column runat="server" Header="Tel. Casa" DataIndex="TelCasa" />
                                <ext:Column runat="server" DataIndex="TelOficina" Hidden="true" />
                                <ext:Column runat="server" DataIndex="Correo" Hidden="true" />
                                <ext:Column runat="server" DataIndex="FechaNacimiento"  />
                        </Columns>
                    </ColumnModel>
                    <View>
                        <ext:GridView ID="GridView1" runat="server" ForceFit="true">
                        </ext:GridView>
                    </View>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolBar4" runat="server" PageSize="15"></ext:PagingToolbar>
                    </BottomBar> 
                    <Listeners>
                        <ItemDblClick Handler="MetodoClienteSeleccionado2.javaSeleccionarCliente(#{GridCliente});" />
                    </Listeners>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true">                    
                             <Listeners>
                                <Select Handler="MetodoClienteSeleccionado.javaCargarInformacionClienteSeleccionado(#{GridCliente});" />
                             </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                </ext:GridPanel>

    </Content> 
    </ext:Window>
    <ext:Window 
        title="Buscar Articulos" 
            runat="server" 
            ID="winArticulos" 
            Modal="true" 
            Hidden="True" 
            Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content> 
                    <ext:FormPanel 
                        runat="server"
                        ID="FormBusquedaArticulo"
                        Layout="AnchorLayout"
                        LabelAlign="right"
                        AutoRender="true"
                        Region="North"
                        Width="1000"
                        ButtonAlign="Left"
                        PaddingSpec="10 0 10 10"
                        Frame="true">
                        <Items>
                            <ext:Hidden 
                                runat="server"
                                Id="hdnNuevaBusqueda"
                                Text="NO" />
                            <ext:Hidden
                                runat="server"
                                Id="hdnTotalRegistrosNuevaBusqueda" />
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoCodigoBuscado" 
                                Text="|[NONE]|"/>
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoNombreBuscado" />
                            <ext:FieldContainer runat="server">
                                <Items>
                                    <ext:TextField
                                        runat="server"
                                        ID="txtCodigoArticuloBusqueda"
                                        FieldLabel="Codigo"
                                        MarginSpec="0 0 0 8"
                                        LabelWidth="60"    
                                        AnchorHorizontal="200"
                                        DataIndex="CodigoArticulo"/>
                                    </Items>
                            </ext:FieldContainer>
                            <ext:FieldContainer runat="server">
                                <Items>
                            <ext:TextField
                                runat="server"
                                ID="txtNombreArticuloBusqueda"
                                FieldLabel="Nombre"
                                AnchorHorizontal="200"
                                MarginSpec="0 0 0 8"
                                LabelWidth="60"    
                                DataIndex="NombreArticulo" />
                                    </Items>
                                </ext:FieldContainer>
                        </Items>
                        <Buttons>
                            <ext:Button
                                runat="server"
                                ID="btnBuscarArticulo"
                                Text="Buscar"
                                Icon="Find">
                                <DirectEvents>
                                    <Click OnEvent="BuscarArticulos" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
                    <ext:GridPanel
                        runat="server"
                        Id="GridArticulo"
                        Width="1000"
                        Height="380"
                        Layout="BorderLayout"
                        Region="Center"
                        Border="false"
                        StoreID="StoreArticulos">
                        <ColumnModel>
                            <Columns>
                                <ext:Column runat="server" Header="Codigo" DataIndex="Codigo" Width="100" />
                                <ext:Column runat="server" Header="Nombre" DataIndex="Nombre" Width="350" />
                                <ext:Column runat="server" Header="Existencia" DataIndex="Existencia" Width="100" />
                            </Columns>
                        </ColumnModel>
                            <View>
                                <ext:GridView ID="GridView2" runat="server" ForceFit="true">
                                </ext:GridView>
                            </View>
                        <Listeners>
                            <ItemDblClick Handler="MetodoArticuloSeleccionado.javaCargarInformacionArticuloSeleccionado(#{GridArticulo});" />
                        </Listeners>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" />                    
                            </SelectionModel>                
                        <BottomBar>
                            <ext:PagingToolbar
                                runat="server"
                                DisplayInfo="true"
                                DisplayMsg="Articulos {0} - {1} de {2}"
                                EmptyMsg="Sin Articulos " />
                        </BottomBar>
                    </ext:GridPanel>
                </Items>
        </Content>
        </ext:Window>

    <ext:Window 
        title="Formas de Pago" 
            runat="server" 
            ID="WindowPagos" 
            Modal="true" 
            Hidden="True" 
            Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content> 
            <ext:Panel 
                runat="server"
                Layout="VBoxLayout"
                Width="1000">
                <Items>
                    <ext:Container runat="server" Width="750" Layout="HBoxLayout">
                        <Items>
                            <ext:Image runat="server" ID="imgQuitarPago" Width="50" Height="20" PaddingSpec="0 0 5 5" />
                            <ext:Label runat="server" ID="lblFormaPago" Text="Forma de Pago" Width="100" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="lblMonto" Text="Monto" Width="200" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="lblCuenta" Text="Cuenta" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>
            <ext:FormPanel
                ID="FrmFormasPago"
                runat="server"
                Title="Formas de Pago"
                Height="240"
                ButtonAlign="Left"
                LabelAlign="Left">
                <Items>
                    <ext:SelectBox runat="server" ID="cmbFormaPago" FieldLabel="Forma de Pago" DataIndex="FormaPago" /> 
                    <ext:TextField runat="server" ID="txtMonto" FieldLabel="Monto" DataIndex="MontoPagado"></ext:TextField>  
                    <ext:TextField runat="server" ID="txtCuenta" FieldLabel="Terminacion Tarjeta (4 digitos)" DataIndex="NoCuenta" MaxLength="4"></ext:TextField>
                    <ext:Button ID="btnAgregarPago" runat="server" Text="Agregar Pago" Icon="MoneyAdd" ArrowAlign="Right">                   
                        <Listeners>
                            <Click Handler="addPago(#{FormPanel1}, #{grdPagos});" />
                        </Listeners>
                    </ext:Button>
                </Items>
            </ext:FormPanel>
                
        </Items>
        </Content>
        </ext:Window>

    <ext:Window
        title="Precios"
        runat="server"
        ID="WindowPreciosFactores"
        Modal="true"
        Hidden="true"
        Width="200"
        Height="200"
        Layout="BorderLayout"
        Boder="false"
        ClientIDMode="Static">
        <Content>
            <ext:RadioGroup ID="SeleccionPrecioFactor" ClientIDMode="Static" runat="server" Layout="VBoxLayout">
                <Items>
                    <ext:Radio runat="server"  ID="PrecioFactor1"  ClientIDMode="Static" Width="100">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ seleccionarPrecioFactor(#{hdnDefinicionFactores}, 0,'A');}" />
                        </Listeners>
                    </ext:Radio>
                    <ext:Radio runat="server" ID="PrecioFactor2"  ClientIDMode="Static" Width="100">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ seleccionarPrecioFactor(#{hdnDefinicionFactores},1,'B');}" />
                        </Listeners>
                    </ext:Radio>
                    <ext:Radio runat="server" ID="PrecioFactor3" ClientIDMode="Static" Width="100">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ seleccionarPrecioFactor(#{hdnDefinicionFactores},2,'C');}" />
                        </Listeners>
                    </ext:Radio>
                    <ext:Radio runat="server" ID="PrecioFactor4" ClientIDMode="Static" Width="100">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ seleccionarPrecioFactor(#{hdnDefinicionFactores},3,'D');}" />
                        </Listeners>
                    </ext:Radio>
                    <ext:Radio runat="server" ID="PrecioFactor5" ClientIDMode="Static" Width="100">
                        <Listeners>
                            <SpecialKey Handler="if(e.getKey()==13){ seleccionarPrecioFactor(#{hdnDefinicionFactores},4,'E');}" />
                        </Listeners>
                    </ext:Radio>
                </Items>
            </ext:RadioGroup>
        </Content>
    </ext:Window>

</asp:Content>
