﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Cancelaciones.aspx.vb" Inherits="PuntoVentaGrupoRoma.Cancelaciones" MasterPageFile="~/admin.master"%>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    
    <ext:XScript ID="XScript1" runat="server">
    
        <script type="text/javascript">
            Ext.data.Connection.override({ timeout: 1200000 });
            Ext.Ajax.timeout = 12000000;
        </script>
    
        <script type="text/javascript">
            function ValidaCantidad(value) {
                if (value == "") { value = this.gridEditor.record.data.Cantidad }
                if (parseFloat(value) > parseFloat(this.gridEditor.record.data.Cantidad)) {
                    return "Cantidad Incorrecta";
                }
                return true;
            }
        </script>
        
       <%-- <script type="text/javascript">
             function ValidaIMEI(value) {               
                if (value==""){value=this.gridEditor.record.data.IMEI}
                 if (value != this.gridEditor.record.data.IMEI) {                 
                     return "IMEI Incorrecta";
                 }
                 return true;
             }
        </script>--%>
        
    </ext:XScript>
    <ext:Hidden ID="UseConfirmation" runat="server" Text="false" />
     <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None" AutoLoad="false">
            <Proxy>
                <ext:AjaxProxy Url="Clientes.ashx" >
                    <ActionMethods READ="POST" />
                </ext:AjaxProxy>   
            </Proxy> 
            <Model>
                <ext:Model runat="server" Root="clientes" TotalProperty="Total" >
                    <Fields>
                        <ext:ModelField Name="cCliente" />
                        <ext:ModelField Name="RFC"/> 
                        <ext:ModelField Name="cIdCliente"/>                        
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
    
     
    <ext:GridPanel 
            ID="gpDevoluciones" 
            runat="server"
            Icon="Table" 
            Title="Cancelaciones / Notas de credito"  
            Height="500"   
            AutoExpandColumn = "Comentarios" 
            ClicksToEdit="1"    
            AutoDataBind="true"
            EnableViewState="true"        
            >   
            <Store>
                 <ext:Store 
            ID="StoreDevoluciones" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None"
           OnBeforeStoreChanged="HandleChanges"
                     >
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="IDVenta"/>
                        <ext:ModelField Name="IDLinea"/>
                        <ext:ModelField Name="Articulo"/>
                        <ext:ModelField Name="DescripcionArticulo"/>
                        <ext:ModelField Name="Lista"/>
                        <ext:ModelField Name="PU"/>
                        <ext:ModelField Name="IVA"/>
                        <ext:ModelField Name="Monto"/>
                        <ext:ModelField Name="DN"/>
                        <ext:ModelField Name="IMEI"/>
                        <ext:ModelField Name="SIM"/>
                        <ext:ModelField Name="Cantidad"/>
                        <ext:ModelField Name="StatusLinea"/>
                        <ext:ModelField Name="ReferenciaDN"/>
                        <ext:ModelField Name="Referencia"/>
                        <ext:ModelField Name="Observaciones"/>
                        <ext:ModelField Name="Descuento"/>
                        <ext:ModelField Name="Comentarios"/>
                        <ext:ModelField Name="IDStore"/>
                        <ext:ModelField Name="Fecha"/>
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
            
            </Store>                    
            <ColumnModel>
                <Columns>
                                    
                    <ext:Column runat="server" Header="Id" DataIndex="Id" Hidden="true"  />
                    <ext:Column runat="server" Header="Venta" DataIndex="IDVenta" Hidden="true"/>                        
                    <ext:Column runat="server" Header="IDStore" DataIndex="IDStore" Hidden="true"/>                        
                    <ext:Column runat="server" Header="Fecha Venta" DataIndex="Fecha"/>                        
                    <ext:Column runat="server" Header="# Partida" DataIndex="IDLinea" Width="50"/>                        
                    <ext:Column runat="server" Header="Modelo" DataIndex="Articulo" Width="150"/>                        
                    <ext:Column runat="server" Header="Descripcion" DataIndex="DescripcionArticulo" Width="220"/>                        
                    <ext:Column runat="server" Header="Lista" DataIndex="Lista" Hidden="true"/>
                    <ext:Column runat="server" Header="Precio Unitario" DataIndex="PU">                        
                        <Renderer Format="UsMoney" />
                    </ext:Column>
                    <ext:Column runat="server" Header="IVA" DataIndex="IVA">
                        <Renderer Format="UsMoney" />                        
                    </ext:Column>
                     <ext:Column runat="server" Header="Descuento" DataIndex="Descuento">
                        <Renderer Format="UsMoney" />
                    </ext:Column>       
                    <ext:Column runat="server" Header="Monto" DataIndex="Monto">
                        <Renderer Format="UsMoney" />
                    </ext:Column>
                    <ext:Column runat="server" Header="Cantidad Original" DataIndex="Cantidad"/>                                                
                    <ext:Column runat="server" Header="Cantidad Devolucion" DataIndex="Cantidad"/>                        
                    <ext:Column runat="server" Header="Estatus" DataIndex="StatusLinea" Hidden="true"/>
                    <ext:Column runat="server" Header="Estado Producto" DataIndex="Comentarios">                        
                        <Editor>
                            <ext:ComboBox ID="cbEstado" runat="server">
                                <Items>
                                    <ext:ListItem Text="Vendible" Value="Vendible" />
                                    <ext:ListItem Text="No Vendible" Value="No Vendible" />
                                </Items> 
                            </ext:ComboBox>
                        </Editor> 
                    </ext:Column>
                    <ext:Column runat="server" Header="Observaciones" DataIndex="Observaciones">                        
                        <Editor>
                            <ext:TextField runat="server" ID="NumberField1"></ext:TextField> 
                        </Editor> 
                    </ext:Column>
                </Columns>
            </ColumnModel>
            <Features>
                <ext:GridFilters runat="server" ID="GridFilters1">
                    <Filters>  
                        <ext:StringFilter DataIndex="Articulo"  /> 
                    </Filters>
                </ext:GridFilters>
            </Features>           
            <View>
                <ext:GridView ID="gvDevoluciones" runat="server" ForceFit="true" />
            </View>
            <TopBar>
                <ext:Toolbar ID="Toolbar1" runat="server" Layout="Container" >
                    <Items>
                        <ext:Toolbar ID="Toolbar3" runat="server" Flat="true">
                            <Items>                                               
                                <ext:Label ID="Label2" runat="server" Text="Cliente: "></ext:Label>  
                                <ext:ComboBox
                                    runat="server" 
                                    ID="cmbCliente"
                                    StoreID="StoreClientes"
                                    DisplayField="cCliente" 
                                    ValueField="cIdCliente"                                                        
                                    TypeAhead="false"
                                    LoadingText="Searching..."                 
                                    PageSize="10"
                                    HideTrigger="true"
                                    ItemSelector="div.search-item"        
                                    AnchorHorizontal="100%"
                                    MinChars="1" 
                                    EmptyText="Seleccione Cliente"
                                    AllowBlank="false"                                                       
                                    Width="250">
                                    <ListConfig>
                                        <ItemTpl ID="Template1" runat="server">
                                           <Html>
						                      <div class="search-item">
							                     <h3><span>{cCliente}</span></h3>
							                     {RFC} 
						                      </div>
				                           </Html>
                                        </ItemTpl>
                                    </ListConfig>
                                        <Listeners>
                                            <Select Fn="function(el) {Ext.net.DirectMethods.GetPeriods(el.value);}"/>
                                        </Listeners>
                                    </ext:ComboBox>
                                
                                <ext:Label ID="lblPeriodo" runat="server" Text="Periodo:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbFecha"
                                EmptyText="Seleccione Fecha"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <%--<Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>--%>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetDays(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblDia" runat="server" Text="Dia:" Visible="True"></ext:Label>  
                                
                                <ext:ComboBox 
                                runat="server" 
                                ID="cmbDia"
                                EmptyText="Seleccione un dia"                             
                                AutoRender="true" 
                                Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.GetFolios(el.value);}"/>
                                    </Listeners>
                                </ext:ComboBox> 
                                
                                <ext:Label ID="lblFolio" runat="server" Text="Folio:" Visible="True"></ext:Label>                       
                                
                                <ext:ComboBox
                                runat="server" 
                                ID="cmbFolio"
                                EmptyText="Seleccione Folio" Visible="True">
                                    <Listeners>
                                        <Select Fn="function(el) {Ext.net.DirectMethods.DoBind(el.value,false);}"/>
                                    </Listeners>
                                </ext:ComboBox>                                           
                                
                                <ext:Button runat="server" ID="btnClean" Text="Nuevo Filto" Flat="false" Icon="Erase">                                
                                <Listeners>
                                        <Click Fn="function(el) {Ext.net.DirectMethods.Clean();}"/>
                                    </Listeners>
                                </ext:Button>    
                            <%--    </ext:Cell>
                                
                            </cells>                        
                        </ext:tablelayout> --%>
                        </Items>
                        </ext:Toolbar>
                        
                        <ext:Toolbar ID="Toolbar2" runat="server" Flat="true">                        
                            <Items>
                                <ext:TextField runat="server" FieldLabel="Pedido" ID="Pedido">
                                </ext:TextField>
                                <ext:Button runat="server" Text="Buscar" Icon="Find" ID="Buscar"></ext:Button>
                            </Items>
                        </ext:Toolbar> 
                        
                        
                    </Items>
                </ext:Toolbar>
            </TopBar>
            
            <BottomBar>
                <ext:Toolbar runat="server" ID="tbBottom" >
                    <Items>
                        <ext:Label ID="Label1" runat="server" Text="Tipo:" Visible="True"></ext:Label>
                        <ext:ToolbarSpacer Width="10"></ext:ToolbarSpacer> 
                        <ext:ComboBox runat="server" ID="cmbTipo" SelectedIndex="0">
                            <Items>
                                <ext:ListItem Text="Nota de credito interna" Value="Cancelacion" /> 
                                <ext:ListItem Text="Nota de credito fiscal" Value="Devolucion" /> 
                            </Items> 
                        </ext:ComboBox> 
                        <ext:ToolbarFill></ext:ToolbarFill> 
                        <ext:Button ID="btnDevolucion" runat="server" Text="Devolucion" Icon="Accept">

                            <DirectEvents>
                                <Click OnEvent="DoReturns">
                                    <EventMask ShowMask="true" />
                                </Click>
                            </DirectEvents>

                        </ext:Button>    
                    </Items> 
                </ext:Toolbar> 
            </BottomBar> 
            
           <SelectionModel>
                <ext:CheckboxSelectionModel ID="chkDevolber" runat="server" HideCheckAll="true">
                </ext:CheckboxSelectionModel>
            </SelectionModel>
        </ext:GridPanel>
</asp:Content>
