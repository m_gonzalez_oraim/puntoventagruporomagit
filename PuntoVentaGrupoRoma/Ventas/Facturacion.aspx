﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="PuntoVentaGrupoRoma.EsquemaVenta_Facturacion" Codebehind="Facturacion.aspx.vb" MasterPageFile="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">

        <ext:ResourceManager runat="server" />             
           
    <ext:XScript ID="XScript1" runat="server">
                                           
    <script type="text/javascript">

        function ResetearForm(form) {
            form.getForm().reset();
            form.items.items[9].setValue("");
            form.items.items[10].setValue("");
            form.items.items[11].setValue("");
            form.items.items[8].setValue("");
            if (form.items.items[8].store!=undefined){
                form.items.items[8].store.removeAll();
            }
        }

        function formatCurrency(num) {
        num = num.toString().replace(/\$|\,/g,'');
        if(isNaN(num))
        num = "0";
        sign = (num == (num = Math.abs(num)));
        num = Math.floor(num*100+0.50000000001);
        cents = num%100;
        num = Math.floor(num/100).toString();
        if(cents<10)
        cents = "0" + cents;
        for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
        num = num.substring(0,num.length-(4*i+3))+','+
        num.substring(num.length-(4*i+3));
        return (((sign)?'':'-') + '$' + num + '.' + cents);
        }
        
        var validate = function (e) {
            /*
            Properties of 'e' include:
                e.grid - This grid
                e.record - The record being edited
                e.field - The field name being edited
                e.value - The value being set
                e.originalValue - The original value for the field, before the edit.
                e.row - The grid row index
                e.column - The grid column index
            */
            // Call DirectMethod
            e.value = null;          
        };

    </script>
    <script type="text/javascript">
       var updateRecord = function (form) {
            if (form.record == null) {
                return;
            }
            
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
                return false;
            }                       
            form.getForm().updateRecord(form.record);
       };       
                     
       function add(a, b) {
          return (a+b).toFixed(2);
     }
     function subtract(a, b) {
          return (b-a).toFixed(2);
     }
     
     function IsDecimal(data)
{
    var sFullNumber = data;
    var ValidChars = "0123456789.";
    var Validn = "0123456789";
    var IsDotPres=false;
    var Char;
    Char = sFullNumber.charAt(0); 
    if (Validn.indexOf(Char) == -1) 
    {
        alert("Please enter proper value.");
        data.select();
        return false;
    }
    else
    {
        for (i = 0; i < sFullNumber.length; i++) 
        { 
            Char = sFullNumber.charAt(i); 
            if(Char == '.' ) 
            {
                if( IsDotPres == false)
                    IsDotPres = true;
                else
                {
                    alert("Please remove extra '.' or spaces.");
                    data.select();
                    return false;
                }
            }
            if (ValidChars.indexOf(Char) == -1) 
            {
                alert("Please check once again you entered proper value are not.");
                data.select();
                return false;
            }
        }
    }
   return true;
}//end Decimal value check.

       
       var addPago = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
            
                return false;
            }
                      
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();           
            form.getForm().reset();
                        
       };
       
        var addRecord = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
            
                return false;
            }           
            
//            if (form.getForm().getFieldValues(false, "dataIndex").PU.toString()==""){
//                Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar un articulo con precio 0") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });     
//                return false;          
//            }
            
//            if (form.getForm().getFieldValues(false, "dataIndex").PU==0){
//                Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar un articulo con precio 0") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });     
//                return false;          
//            }
            
//            if (form.getForm().getFieldValues(false, "dataIndex").Categoria!="4" && form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)==-1) {
//                    if (form.getForm().getFieldValues(false, "dataIndex").Cantidad.toString().indexOf(".",1)!=-1){
//                           Ext.net.Notification.show({
//                                iconCls  : "icon-exclamation",
//                                html     : ("No puede ingresar decimales en esta categoria de articulo") ,
//                                title    : "Advertencia",
//                                HideDelay : 10000                    
//                            });     
//                            return false;
//                    }
//            }
                     
//            if (grid.topToolbar.items.items[5].getText().substring(0,1) == "M" && (form.getForm().getFieldValues(false, "dataIndex").Categoria=="1")){
//                 Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar equipos con un cliente mostrador") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });
//                return false;
//            } 
            
//            if (grid.topToolbar.items.items[5].getText().substring(0,1) == "M" && form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1){
//                 Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar articulos de la categoria TEMM2 con un cliente mostrador") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });
//                return false;
//            } 
//                     
//            if (grid.topToolbar.items.items[4].getText() == ""){
//                grid.topToolbar.items.items[4].setText(form.getForm().getFieldValues(false, "dataIndex").Categoria)
//            }
//            
//            if (grid.topToolbar.items.items[5].getText().substring(0,1)=="C" && form.getForm().getFieldValues(false, "dataIndex").Categoria != "4") {
//                if (grid.store.reader.jsonData.length >= 10) {
//                        Ext.net.Notification.show({
//                            iconCls  : "icon-exclamation",
//                            html     : ("No puede ingresar mas de 10 lineas en este documento") ,
//                            title    : "Advertencia",
//                            HideDelay : 10000                    
//                        });
//                        return false;
//                }
//                if (form.getForm().getFieldValues(false, "dataIndex").Categoria==4 || form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)==0 || form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)==0) {
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                }
//            }
            
//            if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1) {
//                    if (form.getForm().getFieldValues(false, "dataIndex").Referencia.toString().length==0){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia es obligatorio") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }                    
//                }
//                
//                if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)!=-1){
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                }                        
//                
//                if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1){
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//                }  
            
//            if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)==0){
//                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
//                    Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                    });
//                    return false;                    
//                }
//                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                    Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                    });
//                    return false;                    
//                }
//            }
//            
//            if (form.getForm().getFieldValues(false, "dataIndex").SIM!=""){
//                if (form.getForm().getFieldValues(false, "dataIndex").DN==""){
//                    Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("El campo DN es obligatorio para esta categoria de articulo") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                    });
//                    return false;                    
//                }
//                if (form.getForm().getFieldValues(false, "dataIndex").DN.toString().length!=10){
//                    Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("El campo DN debe ser de 10 caracteres") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                    });
//                    return false;                    
//                }                
//            }
//            
//            if (form.getForm().getFieldValues(false, "dataIndex").Categoria=="4") {
//                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
//                    Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                    });
//                    return false;                    
//                }
//                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
//                        Ext.net.Notification.show({
//                        iconCls  : "icon-exclamation",
//                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
//                        title    : "Advertencia",
//                        HideDelay : 10000                    
//                        });
//                        return false;                    
//                    }
//            }
            
            
//            if (grid.topToolbar.items.items[4].getText() == "4" && form.getForm().getFieldValues(false, "dataIndex").Categoria!="4") {
//                Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar articulos que no sean de la categoria TEMM en este documento") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });
//                return false;
//            }
//            else if (grid.topToolbar.items.items[4].getText() != "4" && form.getForm().getFieldValues(false, "dataIndex").Categoria=="4") {
//                 Ext.net.Notification.show({
//                    iconCls  : "icon-exclamation",
//                    html     : ("No puede ingresar articulos que sean de la categoria TEMM en este documento") ,
//                    title    : "Advertencia",
//                    HideDelay : 10000                    
//                });
//                return false;
//            }

            grid.insertRecord(0, form.getForm().getFieldValues(true, "dataIndex"));
            grid.getView().refresh();
            ResetearForm(form);
            grid.load();            
            grid.render();
            //Ext.net.DirectMethods.actualizaMontos();
       };
       
       var prepareadd = function (form, grid) {
       
            if ((form.items.items[0].getValue()<=777051040) && (form.items.items[0].getValue()!="")) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : ("El articulo no existe en la base de datos, elija una articulo ente el rango 777051041 a 777051050") ,
                    title    : "Advertencia",
                    HideDelay : 10000                    
                });
                form.getForm().reset(); 
                form.items.items[0].setValue("777051041");
            }
            
            
            if (form.items.items[0].getValue()>777051050 && form.items.items[0].getValue()!=="") {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : ("El articulo no existe en la base de datos, elija una articulo ente el rango 777051041 a 777051050") ,
                    title    : "Advertencia",
                    HideDelay : 10000
                });
                form.getForm().reset(); 
                form.items.items[0].setValue("777051041");
            }
                              
            //var values = form.getForm().getFieldValues();  
            if (form.items.items[0].getValue()=="") {
                form.getForm().reset(); 
                form.items.items[0].setValue("777051041");
            }
            else             
            {
            var randomnumber=Math.floor(Math.random()*9) * 1000000
            var div = form.items.items[0].getValue() / randomnumber;
            //form.items.items[1].setValue("1000");
            //form.items.items[2].setValue("1000");
            //form.items.items[3].setValue("Pendiente");
            form.items.items[2].setValue('1');
            form.items.items[3].setValue('Lista de precios 01');
            form.items.items[1].setValue('Articulo ' + form.items.items[0].getValue());
            form.items.items[4].setValue(div);
            form.items.items[5].setValue(div*.16);
            form.items.items[6].setValue("Ninguna");
            }         
                       
            
            
       };
       
       var addRecord2 = function (form, grid) {             
            
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
            
                return false;
            }
            
            //var values = form.getForm().getFieldValues();  
            var values = form.getForm().getFieldValues(false, "dataIndex")
            //var i = 0
            //for(var field in form.getForm().getValues())  
            //{   
            //alert(field+':'+values[field]);  
            //i=i+1;
            //values[field] = "1";
            //var obj = form.getForm().elements[field];
            //alert(obj);  
            //}            
                     
            //grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            ResetearForm(form);
       };
    </script>
    <script type="text/javascript">
       var updateClient = function (form) {
            if (form.record == null) {
                return;
            }
            
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
                return false;
            }
            
            form.getForm().updateClient(form.record);
       };
       
       var addCliente = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
            
                return false;
            }
            
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            //grid.insertRecord(grid.store.data.length + 1, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);
       };
       
       var prepareaddCliente = function (form, grid) {
       
            if ((form.items.items[0].getValue()<=777051040) && (form.items.items[0].getValue()!="")) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : ("Ya esta registrado el Cliente No. " + form.items.items[0].getValue()) ,
                    title    : "Advertencia"
                });
                form.getForm().reset(); 
            }
            
            
            if (form.items.items[0].getValue()>777051050 && form.items.items[0].getValue()!=="") {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : ("No se encontro el Cliente No. " + form.items.items[0].getValue()) ,
                    title    : "Advertencia"
                });
                form.getForm().reset(); 
            }
                              
            //var values = form.getForm().getFieldValues();  
            if (form.items.items[0].getValue()=="") {
                form.getForm().reset(); 
            }
            else             
            {
            var randomnumber=Math.floor(Math.random()*9) * 1000000
            var div = form.items.items[0].getValue() / randomnumber;
            //form.items.items[1].setValue("1000");
            //form.items.items[2].setValue("1000");
            //form.items.items[3].setValue("Pendiente");
            form.items.items[2].setValue('Lista de precios 01');
            form.items.items[1].setValue('Cliente ' + form.items.items[0].getValue());
            form.items.items[3].setValue(div);
            form.items.items[4].setValue(div*.16);
            form.items.items[5].setValue("Ninguna");
            }
            
            
            
       };
       
       var addCliente2 = function (form, grid) {             
            
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls  : "icon-exclamation",
                    html     : "Form is invalid",
                    title    : "Error"
                });
            
                return false;
            }
            
            //var values = form.getForm().getFieldValues();  
            var values = form.getForm().getFieldValues(false, "dataIndex")
            //var i = 0
            //for(var field in form.getForm().getValues())  
            //{   
            //alert(field+':'+values[field]);  
            //i=i+1;
            //values[field] = "1";
            //var obj = form.getForm().elements[field];
            //alert(obj);  
            //}            
                     
            //grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
       };
    </script>
    
    </ext:XScript>
        
        <ext:Store 
            ID="Store1" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            OnBeforeStoreChanged="HandleChanges" 
            SkipIdForNewRecords="false"            
            RefreshAfterSaving="Always">
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="Articulo"  />
                        <ext:ModelField Name="Linea" />
                        <ext:ModelField Name="Medida"  />
                        <ext:ModelField Name="Modelo"  />
                        <ext:ModelField Name="Juego"  />
                        <ext:ModelField Name="CantidadTienda"  />
                        <ext:ModelField Name="CantidadBodega"  />
                        <ext:ModelField Name="Lista"  />
                        <ext:ModelField Name="PrecioUnitario"  />
                        <ext:ModelField Name="IVA" />
                        <ext:ModelField Name="Monto" />
                        <ext:ModelField Name="Descuento" />                        
                        <ext:ModelField Name="Total" />
                        <ext:ModelField Name="AlmacenBox" ></ext:ModelField> 
                        <ext:ModelField Name="CantidadBox" ></ext:ModelField>
                        <ext:ModelField Name="subTotal" ></ext:ModelField>
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
       
        <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            OnBeforeStoreChanged="HandleChangesClientes" 
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None"
            >
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="Nombre"  />
                        <ext:ModelField Name="NombreComercial" />
                        <ext:ModelField Name="RFC" />
                        <ext:ModelField Name="CalleNumero"  />
                        <ext:ModelField Name="Colonia"  />
                        <ext:ModelField Name="CP"  />
                        <ext:ModelField Name="TelCasa"  />
                        <ext:ModelField Name="TelOfc" />
                        <ext:ModelField Name="Correo"  />
                        <ext:ModelField Name="Estado"  />
                        <ext:ModelField Name="actCod_REGIONestado"  />
                        <ext:ModelField Name="actCod_PROVINCIAmunicipio"  />
                        <ext:ModelField Name="actCod_CIUDAD"  />
                        <ext:ModelField Name="actCod_COMUNAcolonia"  />
                        <ext:ModelField Name="DelMun"  />
                        <ext:ModelField Name="IDRef"  />
                        <ext:ModelField Name="TipoCliente"  />
                        <ext:ModelField Name="actFCHNACIMIENTO"  />
                        <ext:ModelField Name="actNOMBRE"  />
                        <ext:ModelField Name="actNOMBRECOMERCIAL"  />
                        <ext:ModelField Name="actAPEPATERNO" />
                        <ext:ModelField Name="actAPEMATERNO"  />
                        <ext:ModelField Name="actCALLE"  />
                        <ext:ModelField Name="actNUMEXT"  />
                        <ext:ModelField Name="actNUMINT" />
                        <ext:ModelField Name="actCIUDAD"  />
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
        
        <ext:Store 
        ID="StorePagos" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            OnBeforeStoreChanged="HandleChangesPagos" 
            SkipIdForNewRecords="false"   
            OnRefreshData="StorePagos_RefreshData"          
            RefreshAfterSaving="Always"
            >
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="FormaPago"  />
                        <ext:ModelField Name="MontoPagado" />                        
                        <ext:ModelField Name="NoCuenta" />
                    </Fields>
                </ext:Model>
            </Model>
            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
        
        <ext:Hidden ID="UseConfirmation" runat="server" Text="false" />
        
        <ext:Panel ID="pnlWest" 
            runat="server" 
            Width="380" 
            Split="true"             
            DefaultBorder="false"            
            AutoScroll="true"
            Layout="AnchorLayout"
            Region="West">   
            <Items>
                <ext:FormPanel 
                ID="FrmCliente" 
                runat="server"
                Icon="User"
                Frame="true"
                LabelAlign="Right"
                Title="Cliente"
                AutoRender ="true" 
                StyleSpec="margin-right: 5px">
                    <Items>            
                        <ext:TextField 
                            ID="tfCliente" 
                            runat="server"
                            FieldLabel="Cliente"                    
                            AllowBlank="false"
                            AnchorHorizontal="90%"
                            Hidden="true" />                    
                        <ext:TextField 
                            ID="tfNombreCliente" 
                            runat="server"
                            FieldLabel="Nombre"
                            AllowBlank="false"
                            Disabled ="true"
                            AnchorHorizontal="90%" />                   
                        <ext:TextField 
                            ID="tfRFCCliente" 
                            runat="server"
                            FieldLabel="RFC"
                            AllowBlank="false"
                            Disabled ="true"
                            AnchorHorizontal="90%" />
                        <ext:TextField 
                            ID="tfTelCasaCliente" 
                            runat="server"
                            FieldLabel="Telefono Casa"
                            AllowBlank="false"
                            Disabled ="true"
                            AnchorHorizontal="90%"
                            Hidden="true" >                   
                        </ext:TextField>
                        <ext:TextArea 
                            ID="tfDireccion" 
                            runat="server"
                            FieldLabel="Direccion"                    
                            AllowBlank="false"
                            AnchorHorizontal="90%"
                            Enabled ="false"
                            Disabled ="true"
                            BoxLabel="CheckBox" />
                        <ext:TextField 
                            ID="tfTipoCliente" 
                            runat="server"
                            FieldLabel="Tipo Cliente"
                            AllowBlank="false"
                            Disabled ="true"
                            AnchorHorizontal="90%" 
                            Hidden = "true" />                   
                    </Items>
                <Buttons>
                    <ext:Button 
                        ID="Button1" 
                        runat="server"
                        Text="Save"
                        Icon="Disk"
                        Disabled = "true"
                        Visible = "false">
                        <Listeners>
                            <Click Handler="updateRecord(#{UserForm});" />
                        </Listeners>
                    </ext:Button>
                    <ext:Button 
                        ID="Button2" 
                        runat="server"
                        Text="Nuevo"
                        Icon="UserAdd" Hidden="true"> 
                        <%-- <Listeners>
                            <Click Handler="addRecord(#{UserForm}, #{GridPanel1});" />
                        </Listeners>--%>
                    </ext:Button>
                    <ext:Button 
                        ID="Button4" 
                        runat="server"
                        Text="Buscar"
                        Icon="Find"> 
                        <%-- <Listeners>
                            <Click Handler="addRecord(#{UserForm}, #{GridPanel1});" />
                        </Listeners>--%>
                    </ext:Button>                
                </Buttons>
            </ext:FormPanel>       
            <ext:FormPanel 
                ID="UserForm" 
                runat="server"
                Icon="ReportEdit"
                LabelAlign="Right"
                Title="Captura de Articulos"
                AutoRender ="true" 
                StyleSpec="margin-top: 5px;margin-right: 5px">                                   
                <Items>
                    <ext:ComboBox 
                    ID="tfArticulo" 
                    runat="server" 
                    FieldLabel="Articulo"
                    DataIndex="Articulo"
                    AnchorHorizontal="100%" >
                        <DirectEvents>                                                
                            <Select OnEvent="CambiaArticulo">
                                <EventMask ShowMask="true" Msg="Obteniendo Lineas" MinDelay="500" />
                            </Select>
                        </DirectEvents>                    
                    </ext:ComboBox>
                    <ext:ComboBox
                        ID="cbLinea" 
                        runat="server"
                        FieldLabel="Linea"
                        DataIndex="Linea"
                        AnchorHorizontal="100%" >
                            <DirectEvents>                        
                                <Select OnEvent="CambiaLinea">
                                <EventMask ShowMask="true" Msg="Obteniendo Medidas" MinDelay="500" />
                            </Select>
                            </DirectEvents>                    
                    </ext:ComboBox>
                    <ext:ComboBox 
                        runat="server" 
                        ID="tfMedida" 
                        DataIndex="Medida" 
                        FieldLabel="Medida"                        
                        Enabled="false">
                        <DirectEvents>                        
                            <Select OnEvent="CambiaMedida">
                                <EventMask ShowMask="true" Msg="Obteniendo Modelos" MinDelay="500" />
                            </Select>
                        </DirectEvents>
                   </ext:ComboBox>                    
                    <ext:ComboBox
                        ID="Modelo" 
                        runat="server" 
                        FieldLabel="Modelo"
                        DataIndex="Modelo"
                        AnchorHorizontal="100%" 
                        AllowBlank="false">
                        <DirectEvents>                                                
                            <Select OnEvent="CambiaModelo">
                                <EventMask ShowMask="true" Msg="Obteniendo Informacion del articulo" MinDelay="500" />
                            </Select>
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:FieldContainer ID="CantidadesTienda" runat="server" FieldLabel="Cantidad Tienda" AnchorHorizontal="95%">
                        <Items>
                            <ext:NumberField 
                                ID="CantidadTienda" 
                                runat="server"
                                FieldLabel="Cantidad Tienda"
                                DataIndex="CantidadTienda"
                                AllowBlank="true" 
                                Width="60" 
                                EmptyText="0">       
                                    <DirectEvents>                                    
                                        <Blur OnEvent="ChangePrice" />
                                    </DirectEvents>             
                            </ext:NumberField> 
                        <ext:NumberField runat="server" ID="ExistenciaTienda" Disabled="true" Enabled="false" Width="60" />
                        </Items> 
                    </ext:FieldContainer>              
                    <ext:FieldContainer ID="CantidadesBodega" runat="server" FieldLabel="Cantidad Bodega" AnchorHorizontal="95%">                
                        <Items>
                            <ext:NumberField 
                                ID="CantidadBodega" 
                                runat="server"
                                DataIndex="CantidadBodega"
                                FieldLabel="Cantidad Bodega"                    
                                AllowBlank="true"                    
                                Width="60" 
                                EmptyText="0">                   
                                    <DirectEvents>                                    
                                        <Blur OnEvent="ChangePrice" />
                                    </DirectEvents>                    
                            </ext:NumberField>
                            <ext:NumberField runat="server" ID="ExistenciaBodega" Disabled="true" Enabled="false" Width="60" />
                    </Items>
                </ext:FieldContainer>
                
                <ext:FieldContainer ID="Combo" runat="server" FieldLabel="Juego" AnchorHorizontal="100%">
                    <Items>
                        <ext:Checkbox ID="esCombo" FieldLabel="Combo" Enabled="true" runat="server">
                            <DirectEvents>                                                                                                            
                                <Change OnEvent="AplicaCombo">
                                        <EventMask ShowMask="true" Msg="Obteniendo Informacion del combo" MinDelay="500" />
                                </Change>
                            </DirectEvents>
                        </ext:Checkbox>                
                        <ext:TextField ID="artCombo" runat="server" Enabled="false" Disabled="true" Width="220">
                        </ext:TextField>
                    </Items> 
                </ext:FieldContainer> 
                <ext:FieldContainer ID="AJuego" runat="server" FieldLabel="Origen" AnchorHorizontal="100%">
                    <Items>
                        <ext:ComboBox ID="AlmJuego" runat="server" Flex="1" Disabled="true" DataIndex="AlmacenBox" SelectedIndex="-1">
                            <Items>
                                <ext:ListItem Text="Bodega" Value="B" />
                                <ext:ListItem Text="Tienda" Value="T" />  
                            </Items> 
                            <DirectEvents>                                    
                                <Select OnEvent="ChangeABOX">                                        
                            </Select>
                        </DirectEvents>                           
                        </ext:ComboBox>
                        <ext:TextField ID="CantidadBox" runat="server" Flex="1" Disabled="true" DataIndex="CantidadBox"></ext:TextField>
                    </Items>
                </ext:FieldContainer>
                <ext:ComboBox 
                    ID="Lista"
                    runat="server" 
                    FieldLabel="Lista de precios"
                    DataIndex="Lista"
                    AnchorHorizontal="100%"
                    EmptyText="Seleccione una lista" 
                    AllowBlank="false" TypeAhead="false">
                        <DirectEvents>                                    
                            <Select OnEvent="ChangePrice">                                        
                            </Select>
                        </DirectEvents>                                                                 
                </ext:ComboBox>
                              
                <ext:FieldContainer ID="PUIVA" runat="server" AnchorHorizontal="100%" FieldLabel="P. Unitario">  
                    <Items>
                        <ext:TextField runat="server"
                            id="PrecioUnitario"
                            FieldLabel="Precio Unitario"
                            DataIndex="PrecioUnitario"
                            AllowBlank="false"                        
                            Enabled ="false"
                            Disabled ="true"
                            Flex="1"
                            BlankText ="0" />
                        <ext:Label ID="Label10" runat="server" Text="IVA: "></ext:Label> 
                        <ext:TextField ID="IVA" 
                            runat="server"
                            FieldLabel="IVA"
                            DataIndex="IVA"
                            AllowBlank="false"
                            Flex="1" 
                            Disabled ="true" />                   
                    </Items>
                </ext:FieldContainer> 
                
                <ext:Hidden 
                    runat="server"
                    id="taObservaciones"
                    FieldLabel="Observaciones"
                    DataIndex="Observaciones"
                    AnchorHorizontal="100%">
                </ext:Hidden>
                    
                <ext:TextField 
                    ID="Monto" 
                    runat="server"
                    FieldLabel="Monto"
                    DataIndex="Monto"
                    AllowBlank="false"
                    AnchorHorizontal="100%"
                    Enabled ="false" 
                    Disabled="true" Hidden="true">                       
                </ext:TextField>           
                <ext:NumberField 
                    ID="Descuento" 
                    runat="server"
                    FieldLabel="% Descuento"
                    DataIndex="Descuento"                        
                    AnchorHorizontal="100%"                         
                    MaxLength="10" MinValue="0" MaxValue="100">
                        <DirectEvents>                                    
                            <Blur OnEvent="ChangePrice" />
                        </DirectEvents>
                </ext:NumberField>    
                <ext:TextField ID="Total" runat="server" FieldLabel="Total" Disabled="true" Enabled="false" AnchorHorizontal="100%" />
                <ext:TextField ID="tfCategoria" 
                    runat="server"
                    FieldLabel="Categoria"
                    DataIndex="Categoria"                        
                    AnchorHorizontal="100%"
                    Hidden="true" />
            </Items>
            <Buttons>
                <ext:Button 
                    runat="server"
                    Text="Save"
                    Icon="Disk"
                    Disabled = "true"
                    Visible = "false">
                        <Listeners>
                            <Click Handler="updateRecord(#{UserForm});" />
                        </Listeners>
                </ext:Button>
                <ext:Button 
                    ID="btnCapturar"
                    runat="server"
                    Text="Capturar"
                    Icon="ReportAdd">
                    <DirectEvents>                          
                        <Click OnEvent="AgregaLineaVenta">                                        
                        </Click>                            
                    </DirectEvents>
                </ext:Button>
                <ext:Button 
                    runat="server"
                    Text="Limpiar" 
                    ID="LimpiarArticulo">
                        <Listeners>
                            <Click Handler="ResetearForm(#{UserForm});Ext.net.DirectMethods.Limpiar();" />
                        </Listeners>
                </ext:Button>
            </Buttons>
        </ext:FormPanel>
        
        <ext:FormPanel 
            ID="frmVentaMasiva"
            runat="server"
            Icon="ReportEdit"
            Frame="true"
            LabelAlign="Right"
            Title="Venta Masiva"
            AutoRender ="true" 
            StyleSpec="margin-top: 5px;margin-right: 5px" hidden="true">
            <Items>
                    <ext:ComboBox 
                        ID="cmbListaVentaMasiva"
                        runat="server" 
                        StoreID="StoreListas"
                        DisplayField="NombreLista" 
                        ValueField="cLista"
                        FieldLabel="Lista de precios"
                        DataIndex="Lista"
                        TypeAhead="false"
                        LoadingText="Searching..."                 
                        PageSize="500"
                        HideTrigger="true"
                        ItemSelector="div.search-item"        
                        AnchorHorizontal="100%"
                        MinChars="1" 
                        EmptyText="'*' para mostrar listas disponibles" 
                        AllowBlank="false"
                        SelectedIndex="0">
                       <ListConfig ID="TemplateListaVentaMasiva" runat="server">
                           <ItemTpl runat="server">
                            <Html>
					           
						          <div class="search-item">
							         <h3><span>{NombreLista}</span></h3>
							         Identificador SAP - {cLista}
						          </div>
					           
				           </Html>
                               </ItemTpl>
                        </ListConfig>                                                                                                                                
                    </ext:ComboBox>
                    <ext:FileUploadField 
                        ID="FileUploadField1" 
                        runat="server" 
                        EmptyText="Cargar Archivo"
                        FieldLabel="Archivo"
                        ButtonText=""
                        AnchorHorizontal = "100%"
                        Icon="Find" >                        
                    </ext:FileUploadField>                                
            </Items>                
        </ext:FormPanel>
        
        
        
        </Items>
            </ext:Panel>
                
                <%-- fin panel west --%>
                
        
        <ext:GridPanel 
            ID="GridPanel1" 
            runat="server"
            Icon="Table"
            Frame="true"
            Title="Articulos Capturados."
            AutoRender ="true" 
            StoreID="Store1"
            Height="240"
            AutoScroll="true"                        
            ForceLayout="true"  
            AutoExpandColumn="Modelo"                       
            >                        
            <ColumnModel>
                <Columns>
                    
                    <ext:Column runat="server" Header="Id" DataIndex="Id" Hidden="true"  />
                    
                    <ext:Column runat="server" Header="Articulo" DataIndex="Articulo" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Modelo" DataIndex="Modelo" Width="300">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Juego" DataIndex="Juego" Width="60">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="C. Tienda" DataIndex="CantidadTienda">                       
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="C. Bodega" DataIndex="CantidadBodega">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Lista" DataIndex="Lista" >                        
                    </ext:Column>
                                       
                    <ext:Column runat="server" Header="P. Unitario" DataIndex="PrecioUnitario">
                        <Renderer Format="UsMoney" />                        
                    </ext:Column>
                    
                     <ext:Column runat="server" Header="Descuento" DataIndex="Descuento">
                        <Renderer Format="UsMoney" />                                                
                    </ext:Column>
                                        
                    <ext:Column runat="server" Header="Subtotal" DataIndex="subTotal">
                        <Renderer Format="UsMoney" />                                                
                    </ext:Column>
                                                          
                    <ext:Column runat="server" Header="IVA" DataIndex="IVA">
                        <Renderer Format="UsMoney" />                        
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Monto" DataIndex="Monto" Hidden="true">
                        <Renderer Format="UsMoney" />                                                
                    </ext:Column>                    
                                      
                    <ext:Column runat="server" Header="Total" DataIndex="Total">
                        <Renderer Format="UsMoney" />                                                
                    </ext:Column>                   
                                                           
                    <ext:CommandColumn runat="server" Hidden="true">
                        <Commands>
                            <ext:GridCommand Text="Reject" ToolTip-Text="Reject row changes" CommandName="reject" Icon="ArrowUndo" />
                        </Commands>
                        <PrepareToolbar Handler="toolbar.items.get(0).setVisible(record.dirty);" />
                    </ext:CommandColumn>
                    
                </Columns>
            </ColumnModel>
                        
            <View>
                <ext:GridView runat="server" ForceFit="true" />
            </View>
            
                       
            <TopBar>
                <ext:Toolbar runat="server" AutoRender="true" ForceLayout="true">
                    <Items>
                                                                       
                        <ext:Button runat="server" Text="Eliminar" Icon="Exclamation" TabTip = "V.5.10.10">
                            <Listeners>
                                <Click Handler="#{GridPanel1}.deleteSelected();ResetearForm(#{UserForm});Ext.net.DirectMethods.actualizaMontos();" />
                                <%--<Click Handler="#{GridPanel1}.deleteSelected();Ext.net.DirectMethods.redirige();" />--%>
                            </Listeners>
                        </ext:Button>
                        
                        <ext:ToolbarSeparator Hidden="true"  />
                        
                        <ext:Button 
                            runat="server" 
                            Text="Guardar Automaticamente"
                            EnableToggle="true"
                            Pressed="true"
                            ToolTip="Se guardaran los cambios de forma automatica." Hidden="true">
                            <Listeners>
                                <Toggle Handler="#{Store1}.autoSave = pressed;#{Store1}.useIdConfirmation = !pressed;#{UseConfirmation}.setValue(!pressed);" />
                            </Listeners>
                        </ext:Button>
                        
                        <ext:ToolbarFill></ext:ToolbarFill> 
                        
                        <ext:Label runat="server" ID="lblDocType" text="" Hidden="true"></ext:Label>    
                        <ext:Label runat="server" ID="lblTipoCliente" text="" Hidden="true"></ext:Label>    
                        
                    </Items>
                </ext:Toolbar>
            </TopBar>
            
            <SelectionModel>
                <ext:RowSelectionModel runat="server" SingleSelect="true" ID="RSVenta">                    
                    <%-- <Listeners>
                        <RowSelect Handler="#{UserForm}.getForm().loadRecord(record);#{UserForm}.record = record;" />
                    </Listeners> --%>
                </ext:RowSelectionModel>
            </SelectionModel>            
                       
            <BottomBar>            
            <%--<ext:Toolbar runat="server" AutoRender="true" ForceLayout="true" >--%>            
            <ext:PagingToolbar ID="PagingToolBar3" runat="server" PageSize="20" HideRefresh="true" HideLabel="true" EmptyMsg=" " DisplayMsg=" " AfterPageText="de {0}" BeforePageText="" AutoHeight="true">
            <Items>            
                <ext:TextField ID="txtTV" runat="server" LabelWidth="60" Width="80" LabelAlign="Top" FieldLabel="Total Venta" Enabled="false" Disabled="true" AutoRender="true">
                </ext:TextField>  
                <ext:ToolbarSeparator AnchorHorizontal="2%" ></ext:ToolbarSeparator>
                <ext:TextField ID="txtMP" runat="server" LabelWidth="40" Width="80" LabelAlign="Top" FieldLabel="Pagado" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%"></ext:TextField>  
                <ext:ToolbarSeparator Hidden="true"></ext:ToolbarSeparator>
                <ext:TextField ID="txtEF" runat="server" LabelWidth="40" Width="80" LabelAlign="Top" FieldLabel="Efectivo" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%" Hidden="true"></ext:TextField>  
                <ext:ToolbarSeparator Hidden="true"></ext:ToolbarSeparator>
                <ext:TextField ID="txtRS" runat="server" LabelWidth="60" Width="80" LabelAlign="Top" FieldLabel="Restante" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%" Hidden="true"></ext:TextField>  
                <ext:ToolbarSeparator></ext:ToolbarSeparator>
                <ext:TextField ID="txtCB" runat="server" LabelWidth="40" Width="80" LabelAlign="Top" FieldLabel="Cambio" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%"></ext:TextField>  
                <ext:ToolbarSeparator></ext:ToolbarSeparator>
                <ext:TextField ID="TextField1" runat="server" LabelWidth="60" Width="80" LabelAlign="Top" FieldLabel="Descuento" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%"></ext:TextField>  
                <ext:ToolbarSeparator></ext:ToolbarSeparator>
                <ext:TextField ID="TextField3" runat="server" LabelWidth="30" Width="80" LabelAlign="Top" FieldLabel="Saldo" Enabled="false" Disabled="true" AutoRender="true" AnchorHorizontal="20%"></ext:TextField>  
            </Items> 
            </ext:PagingToolbar>
            <%--</ext:Toolbar> --%>
            </BottomBar>                       
            
        </ext:GridPanel>
               
        <ext:FormPanel 
            ID="FormPanel1" 
            runat="server" 
            Title="Formas de Pago"
            MonitorValid="true" 
            icon = "MoneyDollar" 
            Height="240"
            ButtonAlign="Left"
            LabelAlign="Left"
            Layout="Column" StyleSpec="margin-top: 10px" AutoScroll="true">
            <Items>
                <ext:Panel ID="Panel1" runat="server" Border="false" Header="false" ColumnWidth=".3" Layout="Form" LabelAlign="Top">
                    <Items>
                        
                        <ext:SelectBox runat="server" ID="cmbFP" FieldLabel="Forma de Pago" DataIndex="FormaPago">                                           
                        </ext:SelectBox>
                        
                            <ext:TextField runat="server" ID="txtMontoP" FieldLabel="Monto" DataIndex="MontoPagado"></ext:TextField>  
                        <ext:TextField runat="server" ID="TextField2" FieldLabel="Terminacion Tarjeta (4 digitos)" DataIndex="NoCuenta" MaxLength="4"></ext:TextField>  
                        
                        <ext:Button ID="Button11" runat="server" Text="Agregar Pago" Icon="MoneyAdd" ArrowAlign="Right">                   
                            <Listeners>
                                <Click Handler="addPago(#{FormPanel1}, #{grdPagos});">                                    
                                </Click>
                            </Listeners>
                        </ext:Button>
                    </Items>
                </ext:Panel>
                <ext:Panel ID="Panel2" runat="server" Border="false" Layout="Form" ColumnWidth=".3" LabelAlign="Top" Height="150" AutoScroll="true">
                    <Items>
                        <ext:GridPanel runat="server" ID="grdPagos" Icon="Money" Frame="False" AutoRender="true" StoreID="StorePagos" Height="150" AutoScroll="true">
                        <ColumnModel>
                            <Columns>
                                <ext:Column runat="server" Header="Id" DataIndex="Id"  Hidden="True" >                                    
                                </ext:Column>
                            </Columns> 
                            <Columns>
                                <ext:Column runat="server" Header="Forma de Pago" DataIndex="FormaPago"  Hidden="False" >                                    
                                </ext:Column>
                            </Columns>
                            <Columns>
                                <ext:Column runat="server" Header="Monto" DataIndex="MontoPagado"  Hidden="False">                                    
                                    <Renderer Format="UsMoney" />
                                    <Editor>
                                        <ext:TextField runat="server" ID="txt24" AllowBlank="true"></ext:TextField> 
                                    </Editor> 
                                </ext:Column>
                            </Columns>       
                            <Columns>
                                <ext:Column runat="server" Header="Cuenta" DataIndex="NoCuenta"  Hidden="False" >                                    
                                </ext:Column>
                            </Columns>                                                     
                        </ColumnModel> 
                        <View>
                            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
                        </View>
                        <SelectionModel>
                            <ext:RowSelectionModel runat="server" SingleSelect="true">                                                    
                            </ext:RowSelectionModel>
                        </SelectionModel>
                        <TopBar>
                            <ext:Toolbar ID="Toolbar2" runat="server" AutoRender="true" ForceLayout="true">
                                <Items>                                                                                   
                                    <ext:Button ID="Button12" runat="server" Text="Eliminar Pago" Icon="Exclamation">
                                        <Listeners>
                                            <Click Handler="#{grdPagos}.deleteSelected();" />
                                        </Listeners>
                                    </ext:Button>
                                </Items>
                            </ext:Toolbar>
                        </TopBar>             
                        </ext:GridPanel>                            
                    </Items>
                </ext:Panel>
                <ext:Panel ID="Panel4" runat="server" Layout="Form" ColumnWidth=".1" LabelAlign="Top" widht="20" >
                        
                </ext:Panel>                              
                <ext:Panel ID="Panel5" runat="server" Layout="Form" ColumnWidth=".3" LabelAlign="Top" MarginsSummary="5 5 5 5" Hidden="false" Collapsed="false">
                    <Items>
                           <ext:ComboBox ID="cmbFolioType" runat="server" FieldLabel="Tipo de Venta" SelectedIndex="0" Disabled="true" Visible="false">
                                <Items>
                                     <ext:ListItem Text="Documento Interno" Value="1" />
                                     <ext:ListItem Text="Factura Electronica" Value="2" />                                 
                                </Items> 
                            </ext:ComboBox>                                              
                            <ext:TextArea ID="Comentario" runat="server" FieldLabel="Comentarios" AnchorHorizontal="100%" Height="47"></ext:TextArea>
                            <ext:Button ID="Button3" runat="server" Text="Procesar Venta" Icon="Table" StyleSpec="margin-top: 5px" AnchorHorizontal="100%" ></ext:Button>                                    
                            <ext:Button ID="btnCancelarFactura" runat="server" Text="Cancelar Venta" Icon="TableDelete" StyleSpec="margin-top: 5px" AnchorHorizontal="100%" ></ext:Button>                                                            
                       </Items>
                </ext:Panel>                
            </Items>     
            </ext:FormPanel>
            <ext:Button ID="Button10" runat="server" Text="Procesar Factura" Icon="Table" StyleSpec="margin-top: 5px" Hidden="true"></ext:Button>              
        </Items>
    </ext:Viewport>
    
    <ext:Window runat="server" ID="winClientes" Hidden="True" AutoRender="true" InitCenter="true">
    <Content>     
            <%-- inicia Viewport --%>
                
                <%-- panel west --%>
                <ext:Panel ID="PanelIzquierda" 
            runat="server" 
            Width="400" 
            Split="true"             
            DefaultBorder="false"            
            Frame="true"
            AutoScroll="true" >   
                <Items>
                
                <ext:FieldContainer ID="CompositeField5" runat="server" FieldLabel="Buscar x RFC">
                    <Items>
                                                   
                        <ext:TextField ID="ftFindRFC" runat="server"
                            FieldLabel="Buscar x RFC"
                            AllowBlank="True"
                            AnchorHorizontal="100%"
                            Enabled ="false"                                   >
                        </ext:TextField>
                        
                        <ext:Button ID="btnFindClient" runat="server" Text="Buscar" Icon="Find"></ext:Button>
                        
                    </Items>
               </ext:FieldContainer>
                
                <ext:TextField ID="tfFindNombre" runat="server"
                    FieldLabel="Buscar x Nombre"                    
                    AllowBlank="True"                    
                    AnchorHorizontal="100%"
                    Enabled="false" Hidden="true">                           
                </ext:TextField>
              
        <ext:FormPanel 
            ID="ClientForm" 
            runat="server"
            Frame="true"
            LabelAlign="Right"
            AutoRender ="true" 
            StyleSpec="margin-top: 10px" ButtonAlign="Center">                                   
            
            <Buttons>           
            
            
                <ext:Button ID="btnSaveClient" 
                    runat="server"
                    Text="Save"
                    Icon="Disk"
                    Disabled = "true"
                    Visible = "false"
                    >
                    <Listeners>
                        <Click Handler="updateClient(#{ClientForm});" />
                    </Listeners>
                </ext:Button>
                
                <ext:Button ID="btnInsertClient" 
                    runat="server"
                    Text="Capturar"
                    Icon="ReportAdd">
                    <Listeners>
                        <Click Handler="addCliente(#{ClientForm}, #{GridCliente});" />
                    </Listeners>
                </ext:Button>
                
               <%-- <ext:Button ID="btnFindClient" 
                    runat="server"
                    Text="Buscar"
                    Icon="Find">                    
                </ext:Button>--%>
                                              
                <ext:Button ID="btnCleanClient" 
                    runat="server"
                    Text="Limpiar">
                    <Listeners>
                        <Click Handler="#{ClientForm}.getForm().reset();" />
                    </Listeners>
                </ext:Button>
                
                <ext:Button ID="Button13" runat="server" Text="Actulizar" Icon="Accept">                    
                </ext:Button>
                
                <ext:Button ID="Button9" runat="server" Text="Seleccionar" Icon="Disk">
                </ext:Button>
            </Buttons>
            
            
            <Items>
                       
                                 
                <ext:TextField ID="tfID" runat="server"
                    FieldLabel="Id"
                    DataIndex="Id"
                    AllowBlank="true"                    
                    AnchorHorizontal="100%"
                    Enabled="false" Hidden="true">
                </ext:TextField>
                              
                <ext:TextField ID="tfNombre" runat="server"
                    FieldLabel="* Nombre"
                    DataIndex="Nombre"
                    AnchorHorizontal="100%"
                    Enabled="true" Visible="false" >                           
                </ext:TextField>
                
                <ext:TextField ID="actNOMBRE" runat="server"
                    FieldLabel="Nombre"
                    DataIndex="actNOMBRE"
                    AnchorHorizontal="100%"                   
                    Enabled="true">
                </ext:TextField>
                
<%--                <ext:CompositeField runat="server" FieldLabel="Apellidos">
                    <Items>--%>
                    
                        <ext:TextField ID="actAPEPATERNO" runat="server"
                            FieldLabel="A. Paterno"
                            DataIndex="actAPEPATERNO"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>
                        
                         <ext:TextField ID="actAPEMATERNO" runat="server"
                            FieldLabel="A. Materno"
                            DataIndex="actAPEMATERNO"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>
                
                    
                   <%-- </Items>                 
                </ext:CompositeField> --%>
                
                
                  <ext:TextField ID="tfIDRef" runat="server"
                    FieldLabel="CURP"
                    DataIndex="IDRef"
                    AnchorHorizontal="100%"                  
                    Enabled="true">
                </ext:TextField>  
                   
                <ext:TextField ID="tfRFC" runat="server"
                    FieldLabel="RFC"
                    DataIndex="RFC"
                    AnchorHorizontal="100%"
                    Disabled = "true"                                   >
                </ext:TextField>
                
                 <ext:TextField ID="actCALLE" runat="server"
                    FieldLabel="Calle"
                    DataIndex="actCALLE"
                    AnchorHorizontal="100%"                   
                    Enabled="true">
                </ext:TextField>
                
              <%--  <ext:CompositeField runat="server" FieldLabel="# Ext / # Int">
                    <Items>--%>
                        <ext:TextField ID="actNUMEXT" runat="server"
                            FieldLabel="Numero Ext"
                            DataIndex="actNUMEXT"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>
                        
                         <ext:TextField ID="actNUMINT" runat="server"
                            FieldLabel="Numero Int"
                            DataIndex="actNUMINT"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>
                   <%-- </Items>                
                </ext:CompositeField> --%>
                
                
                 <ext:TextField ID="tfColonia" runat="server"
                    FieldLabel="* Colonia"
                    DataIndex="Colonia"                    
                    AnchorHorizontal="100%"
                    Enabled="true"                    
                 />
                                                
                <ext:ComboBox ID="actCod_REGIONestado" runat="server"
                    FieldLabel="Estado"
                    DataIndex="actCod_REGIONestado"                    
                    AnchorHorizontal="100%"                    
                    Enabled="true">                                       
                </ext:ComboBox>
                
                <ext:ComboBox ID="actCod_PROVINCIAmunicipio" runat="server"
                    FieldLabel="Municipio"
                    DataIndex="actCod_PROVINCIAmunicipio"                    
                    AnchorHorizontal="100%"                    
                    Enabled="true" Hidden="true">                                                       
                </ext:ComboBox>                
                                
                <ext:ComboBox ID="actCod_CIUDAD" runat="server"
                    FieldLabel="Ciudad"
                    DataIndex="actCod_CIUDAD"
                    AnchorHorizontal="100%"                    
                    Enabled="true" Hidden="true">                                        
                </ext:ComboBox>
                
                <ext:ComboBox ID="actCod_COMUNAcolonia" runat="server"
                    FieldLabel="Colonia"
                    DataIndex="actCod_COMUNAcolonia"                    
                    AnchorHorizontal="100%"                    
                    Enabled="true" Hidden="true">                         
                </ext:ComboBox>
                
               <%-- actCod_PROVINCIAmunicipio--%>
                
                <ext:TextField ID="tfEstado" runat="server"
                   FieldLabel="Estado"
                    DataIndex="Estado"                    
                    AnchorHorizontal="100%"
                    BlankText ="1000"
                    Enabled="true" Hidden = "true">                                          
                </ext:TextField>
                
                              
                <ext:TextField ID="tfCalleNumero" runat="server"
                    FieldLabel="* Calle/Numero"
                    DataIndex="CalleNumero"                    
                    AnchorHorizontal="100%"                    
                   Enabled="true"  Visible="false">                   
                </ext:TextField>
                
                                
                  <ext:TextField ID="tfCP" runat="server"
                    FieldLabel="CP"
                    DataIndex="CP"
                    AnchorHorizontal="100%"                   
                    BlankText ="1000"
                    Enabled="true">
                </ext:TextField>  
                
              
                        
                        <ext:TextField ID="tfTelCasa" runat="server"
                            FieldLabel="Tel Casa"
                            DataIndex="TelCasa"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>   
                        
                        <ext:TextField ID="tfTelOfc" runat="server"
                            FieldLabel="TelOfc"
                            DataIndex="TelOfc"
                            Flex="1"
                            Enabled="true">
                        </ext:TextField>  
                    
                 
                        
                          
                                           
                                              
                <ext:TextField ID="tfCorreo" runat="server"
                    FieldLabel="Correo"
                    DataIndex="Correo"
                    AnchorHorizontal="100%"                   
                    BlankText ="1000"                    
                    Enabled="true">
                </ext:TextField>   
                               
                <ext:TextField ID="tfDelMun" runat="server"
                    FieldLabel="* Del/Mun"
                    DataIndex="DelMun"                    
                    AnchorHorizontal="100%"                   
                    BlankText ="1000"
                    Enabled="true">
                </ext:TextField>                  
                
                <ext:ComboBox ID="cmbTipoCliente" runat="server" FieldLabel="* Tipo Cliente" DataIndex="TipoCliente" AnchorHorizontal="80%" SelectedIndex="1" Hidden="true">
                    <Items>
                        <ext:ListItem Text="Socio" Value="S" />  
                        <ext:ListItem Text="Cliente" Value="C" />  
                    </Items> 
                </ext:ComboBox>                  
                               
                 <ext:DateField ID="actFCHNACIMIENTO" runat="server"
                    FieldLabel="F. Nacimiento"                
                    AnchorHorizontal="80%"                   
                    DataIndex="actFCHNACIMIENTO"
                    Enabled="true">
                </ext:DateField>  
                 
                <ext:TextField ID="actCIUDAD" runat="server"
                    FieldLabel="Ciudad"
                    DataIndex="actCIUDAD"
                    AnchorHorizontal="100%"                   
                    Enabled="true" Hidden="true">
                </ext:TextField>
                 
                 
                    
            </Items>
            
            
        </ext:FormPanel>
        
        </Items>
            </ext:Panel>
            
                            
                <%-- fin panel west --%>
                
                    <%-- centro --%>
                    <ext:Panel ID="PanelCentro" 
                    runat="server"                     
                    IconCls="icono-carro"                           
                    Frame="true" 
                    Border="true"
                    Layout="Fit">
                        <Items>
        
        <ext:GridPanel 
            ID="GridCliente" 
            runat="server"
            Icon="Table"
            Frame="true"
            Title="Clientes Capturados"
            AutoRender ="true" 
            StoreID="StoreClientes" AutoScroll="true" Width="10000" >            
            <Plugins>
                <%--<ext:RowEditor ID="RowEditor2" runat="server" SaveText="Guardar" CancelText="Cancelar"  />--%>
                <%--<ext:GridFilters runat="server" ID="GridFilters1" Local="true">
                        <Filters>
                            <ext:StringFilter DataIndex="Id" />
                            <ext:StringFilter DataIndex="Nombre" />
                            <ext:StringFilter DataIndex="RFC" />
                            <ext:StringFilter DataIndex="CalleNumero" />
                            <ext:StringFilter DataIndex="TelCasa" />
                            <ext:StringFilter DataIndex="Colonia" />
                            <ext:StringFilter DataIndex="CP" />
                            <ext:StringFilter DataIndex="TelCasa" />
                            <ext:StringFilter DataIndex="TelOfc" />
                            <ext:StringFilter DataIndex="Correo" />                            
                            <ext:StringFilter DataIndex="Estado" />
                            <ext:StringFilter DataIndex="DelMun" />
                            <ext:StringFilter DataIndex="IDRef" />
                        </Filters>
                    </ext:GridFilters>--%>
            </Plugins>
            <ColumnModel Width="10000">
                <Columns>
                    <ext:Column runat="server" Header="Id" DataIndex="Id" Hidden="true" />
                                       
                    <ext:Column runat="server" Header="Nombre" DataIndex="Nombre" Width="400">                        
                    </ext:Column>

                    <ext:Column runat="server" Header="Nombre Comercial" DataIndex="Nombre Comercial" Width="400">                        
                    </ext:Column>
                    
                     <ext:Column runat="server" Header="CURP" DataIndex="IDRef" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="RFC" DataIndex="RFC">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Calle/Numero" DataIndex="CalleNumero" Width="500" >
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Colonia" DataIndex="Colonia">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="CP" DataIndex="CP" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Tel. Casa" DataIndex="TelCasa">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Tel. Ofc" DataIndex="TelOfc" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Correo" DataIndex="Correo" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Estado" DataIndex="Estado" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="actCod_REGIONestado" DataIndex="actCod_REGIONestado" Hidden="true">                     
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="actCod_PROVINCIAmunicipio" DataIndex="actCod_PROVINCIAmunicipio" Hidden="true">                     
                    </ext:Column>                   
                                        
                    <ext:Column runat="server" Header="actCod_CIUDAD" DataIndex="actCod_CIUDAD" Hidden="true">                     
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="actCod_COMUNAcolonia" DataIndex="actCod_PROVINCIAmunicipio" Hidden="true">                     
                    </ext:Column>
                    
                    <ext:Column  runat="server" Header="actCod_COMUNAcolonia" DataIndex="actCod_COMUNAcolonia" Hidden="true">                     
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Del/Mun" DataIndex="DelMun" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Tipo Cliente" DataIndex="TipoCliente" Hidden="true">
                    </ext:Column>
                    
                     <ext:Column runat="server" Header="F. Nacimiento" DataIndex="actFCHNACIMIENTO" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Nombre" DataIndex="actNOMBRE" Hidden="true">
                    </ext:Column>

                    <ext:Column runat="server" Header="Nombre" DataIndex="actNOMBRECOMERCIAL" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="A. Paterno" DataIndex="actAPEPATERNO" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="A. Materno" DataIndex="actAPEMATERNO" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Calle" DataIndex="actCALLE" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="# Ext" DataIndex="actNUMEXT" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="# Int" DataIndex="actNUMINT" Hidden="true">
                    </ext:Column>
                    
                    <ext:Column runat="server" Header="Ciudad" DataIndex="actCIUDAD" Hidden="true">
                    </ext:Column>
                                                           
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" ForceFit="true">
                
                </ext:GridView>
            </View>
            
            <BottomBar>
                <ext:PagingToolbar ID="PagingToolBar4" runat="server" PageSize="20"></ext:PagingToolbar>
            </BottomBar> 
            
            <TopBar>
                <ext:Toolbar ID="Toolbar1" runat="server">
                    <Items>
                        <%-- <ext:Button runat="server" Text="Add" Icon="Add">
                            <Listeners>
                                <Click Handler="#{GridCliente}.insertRecord();" />
                            </Listeners>
                        </ext:Button>--%>
                        
                        <ext:Button ID="Button7" runat="server" Text="Eliminar" Icon="Exclamation">
                            <Listeners>
                                <Click Handler="#{GridCliente}.deleteSelected();#{ClientForm}.getForm().reset();" />
                            </Listeners>
                        </ext:Button>
                        
                        <ext:ToolbarSeparator Hidden="true"  />
                        
                        <ext:Button ID="Button8" 
                            runat="server" 
                            Text="Guardar Automaticamente"
                            EnableToggle="true"
                            Pressed="true"
                            ToolTip="Se guardaran los cambios de forma automatica." Hidden="true" >
                            <Listeners>
                                <Toggle Handler="#{StoreClientes}.autoSave = pressed;#{StoreClientes}.useIdConfirmation = !pressed;#{UseConfirmation}.setValue(!pressed);" />
                            </Listeners>
                        </ext:Button>
                    </Items>
                </ext:Toolbar>
            </TopBar>
            
            <SelectionModel>
                <ext:RowSelectionModel ID="RowSelectionModel1" runat="server" SingleSelect="true">                    
                     <Listeners>
                        <Select Handler="#{ClientForm}.getForm().loadRecord(record);#{ClientForm}.record = record;Ext.net.DirectMethods.OnSelectedLine();Ext.net.DirectMethods.OnSelecteEstado(record.get('actCod_REGIONestado'));Ext.net.DirectMethods.OnSelectMunicipio(record.get('actCod_PROVINCIAmunicipio'));" />
                     </Listeners>
                </ext:RowSelectionModel>
            </SelectionModel>
            
            <Buttons>                
            </Buttons>
        </ext:GridPanel>
                      
        <ext:ToolTip 
            ID="ToolTip1" 
            runat="server" 
            Target="={#{GridCliente}.getView().mainBody}"
            Delegate=".x-grid3-row"
            TrackMouse="true">
            <Listeners>
                <Show Handler="var rowIndex = #{GridCliente}.view.findRowIndex(this.triggerElement);this.body.dom.innerHTML = 'Doble click para editar';" />
            </Listeners>
        </ext:ToolTip> 
        
         </Items>
                        
                    </ext:Panel>
                    <%-- panel centro --%>
                </Center>
        <%-- Termina Viewport--%>    
       <%-- </Items>
    </ext:Viewport>--%>
             
    </Content> 
    </ext:Window> 
    
    <ext:Window ID="WinPagos" Visible="true" AutoRender="true" Title="Buro de Credito" runat="server" Icon="Creditcards" Height="650px" Width="800px" AutoScroll="true" Hidden="true" MonitorResize="true" >
    
    <Content>  
    
    <ext:FormPanel 
            ID="FormPanel2" 
            runat="server"            
            Frame="true"
            LabelAlign="Right"            
            AutoRender ="true"             
            StyleSpec="margin-top: 5px;margin-right: 5px;margin-left: 5px" AutoScroll="true">
            
            <Items>            
                               
                <ext:TextArea runat="server" ID="bDatos" FieldLabel="Cliente" Disabled="true" AnchorHorizontal="100%" Height="47" ></ext:TextArea>
                
                <ext:DateField runat="server" ID="bFNaC" FieldLabel="F. Nacimiento" Disabled="true" Hidden="true"></ext:DateField> 
                
                 <ext:FieldContainer ID="CompositeField1" runat="server" FieldLabel="Colonias Alternas" >
                    <Items>
                        <ext:ComboBox ID="coloniaAlterna1" runat="server" />
                        <ext:ComboBox ID="coloniaAlterna2" runat="server" />
                    </Items> 
                 </ext:FieldContainer>                 
                                                             
            </Items> 
            
    </ext:FormPanel>                                          
    
    <ext:FormPanel 
            ID="FormPanel3" 
            runat="server"            
            Frame="true"
            LabelAlign="Right"            
            AutoRender ="true"             
            StyleSpec="margin-top: 5px;margin-right: 5px;margin-left: 5px" AutoScroll="true">
            
            <Items>
            
                <ext:FieldContainer ID="CompositeField3" runat="server" FieldLabel="Domiciliacion" >
                    <Items>
                        <ext:ComboBox ID="domiciliacion" runat="server">
                            <Items>
                                <ext:ListItem Text="Si" Value="True" />                 
                                <ext:ListItem Text="No" Value="False" />
                            </Items> 
                         </ext:ComboBox>
                         <ext:Label ID="Label3" Text="Banco" runat="server"></ext:Label>
                         <ext:ComboBox ID="banco" runat="server" FieldLabel="Banco: " />
                         <ext:Label ID="Label4" Text="No. Tarjeta: " runat="server"></ext:Label>
                         <ext:TextField id="numeroTarjeta" runat="server" FieldLabel="Numero Tarjeta" MaxLength="20"  />
                    </Items>
                </ext:FieldContainer> 
                               
                
                  
                <ext:FieldContainer ID="CompositeField2" runat="server" FieldLabel="Vigencia Tarjeta" >
                    <Items>
                        <ext:ComboBox ID="Mes" runat="server" Width="40">
                            <Items>
                                <ext:ListItem Text="01" Value="01" />
                                <ext:ListItem Text="02" Value="02" />
                                <ext:ListItem Text="03" Value="03" />
                                <ext:ListItem Text="04" Value="04" />
                                <ext:ListItem Text="05" Value="05" />
                                <ext:ListItem Text="06" Value="06" />
                                <ext:ListItem Text="07" Value="07" />
                                <ext:ListItem Text="08" Value="08" />
                                <ext:ListItem Text="09" Value="09" />
                                <ext:ListItem Text="10" Value="10" />
                                <ext:ListItem Text="11" Value="11" />
                                <ext:ListItem Text="12" Value="12" />                                
                            </Items>
                        </ext:ComboBox>
                        <ext:TextField id="Año" runat="server" FieldLabel="Numero Tarjeta" MaxLength="4" Width="40" />
                        <ext:Label ID="Label5" Text="Tipo Tarjeta: " runat="server"></ext:Label>
                        <ext:ComboBox ID="tipoTarjeta" runat="server" FieldLabel="Tipo Tarjeta">
                            <Items>
                                <ext:ListItem Value="PH" Text="PH CELUSTAR" />
                                <ext:ListItem Value="LV" Text="LIVERPOOL" />
                                <ext:ListItem Value="MC" Text="MASTER CARD" />
                                <ext:ListItem Value="VI" Text="VISA" />
                                <ext:ListItem Value="AM" Text="AMERICAN EXPRESS" />                        
                            </Items>
                        </ext:ComboBox>
                        
                        <ext:Label ID="Label6" Text="F. Electronica: " runat="server"></ext:Label>
                         <ext:ComboBox ID="facturaElectronica" runat="server" FieldLabel="F. Electronica" >
                            <Items>
                                <ext:ListItem Text="Si" Value="True" />                 
                                <ext:ListItem Text="No" Value="False" />
                            </Items> 
                         </ext:ComboBox>
                        
                    </Items>
                </ext:FieldContainer>
                 
                 <ext:Label Text="A continuación le realizarán dos preguntas para la validación de la Consulta a Buró, y como respuesta textual se requiere que usted responda con un SI o un NO" runat="server"></ext:Label>
                 
                 <ext:ComboBox ID="pregunta1" runat="server" FieldLabel="¿De acuerdo?" >
                    <Items>
                        <ext:ListItem Text="Si" Value="Si" />                 
                        <ext:ListItem Text="No" Value="No" />
                    </Items> 
                 </ext:ComboBox>
                 
                  <ext:Label ID="Label1" Text="¿Autoriza a Pegaso PCS SA de CV a consultar sus antecedentes crediticios por única ocasión ante las Sociedades de Información Crediticia que estime conveniente, declarando que conoce la naturaleza, alcance y uso que Pegaso PCS SA de CV hará de tal información?" runat="server"></ext:Label>
                 
                 <ext:ComboBox ID="pregunta2" runat="server" FieldLabel="¿De acuerdo?" >
                    <Items>
                        <ext:ListItem Text="Si" Value="Si" />                 
                        <ext:ListItem Text="No" Value="No" />
                    </Items> 
                 </ext:ComboBox>
                 
                  <ext:Label ID="Label2" Text="Pregunta 3" runat="server"></ext:Label>
                 
                 <ext:ComboBox ID="pregunta3" runat="server" FieldLabel="¿De acuerdo?" >
                    <Items>
                        <ext:ListItem Text="Si" Value="Si" />                 
                        <ext:ListItem Text="No" Value="No" />
                    </Items> 
                 </ext:ComboBox>
                 
                <ext:Label ID="Label7" Text="¿Es Usted Titular de una Tarjeta de Crédito Vigente?" runat="server"></ext:Label>
                  
                <ext:FieldContainer ID="CompositeField4" runat="server">
                    <Items>
                        <ext:ComboBox ID="credTarjeta" runat="server">
                            <Items>
                                <ext:ListItem Text="Si" Value="Si" />                 
                                <ext:ListItem Text="No" Value="No" />
                            </Items> 
                            <Listeners>
                                <Select Fn="function(el) {Ext.net.DirectMethods.OnSelectCredTarjeta(el.value);}"/>
                            </Listeners>
                        </ext:ComboBox>                                                
                    </Items> 
                </ext:FieldContainer>
                
                <ext:ComboBox ID="institucionCredTarjeta" runat="server" FieldLabel="Banco" />
                
                
                <ext:Label ID="Label8" Text="Actualmente, ¿Tiene Usted un Crédito Hipotecario?" runat="server"></ext:Label>
                
                <ext:ComboBox ID="credHipotecario" runat="server">
                    <Items>
                        <ext:ListItem Text="Si" Value="Si" />                 
                        <ext:ListItem Text="No" Value="No" />
                    </Items> 
                    <Listeners>
                        <Select Fn="function(el) {Ext.net.DirectMethods.OnSelectinstitucionCredHipotecario(el.value);}"/>
                    </Listeners>
                </ext:ComboBox>                                                                    
                
                <ext:ComboBox ID="institucionCredHipotecario" runat="server" FieldLabel="Credito Hipotecario" Hidden="true"/>
                
                <ext:Label ID="Label9" Text="Actualmente, ¿Tiene Usted un Crédito Automotriz?" runat="server"></ext:Label>
                
                <ext:ComboBox ID="ccredAutomotriz" runat="server">
                    <Items>
                        <ext:ListItem Text="Si" Value="Si" />                 
                        <ext:ListItem Text="No" Value="No" />
                    </Items> 
                    <Listeners>
                        <Select Fn="function(el) {Ext.net.DirectMethods.OnSelectCredAutomotriz(el.value);}"/>
                    </Listeners>
                </ext:ComboBox>                                                                    
                
                <ext:ComboBox ID="credAutomotriz" runat="server" FieldLabel="Credito Automotriz" Hidden="true"/>
                 
            </Items>
            
    </ext:FormPanel>  
    
    <ext:FormPanel 
            ID="FormPanel4" 
            runat="server"            
            Frame="true"
            LabelAlign="Right"            
            AutoRender ="true"             
            StyleSpec="margin-top: 5px;margin-right: 5px;margin-left: 5px">                            
            
            <Items>    
            
                <ext:Button ID="PPago" runat="server" icon="Accept" Text="Procesar" StyleSpec="margin-left: 90%" ></ext:Button>
                            
            </Items> 
            
   </ext:FormPanel>
                      
    </Content>
    
    </ext:Window>  
    
    <ext:Window ID="winCondiciones" Visible="true" AutoRender="true" Title="Condiciones" runat="server" Icon="Book" Height="270px" Width="500px" AutoScroll="true" Hidden="true" MonitorResize="true" >
    
    <Content> 
            
            <ext:FormPanel 
            ID="FormPanel5" 
            runat="server"            
            Frame="true"
            LabelAlign="Right"            
            AutoRender ="true"             
            StyleSpec="margin-top: 5px;margin-right: 5px;margin-left: 5px" AutoScroll="true">
            
                <Items>
                    
                    <ext:TextField ID="conBanco" runat="server" FieldLabel="Banco" Disabled="true" />                         
                    <ext:TextField id="conTarjeta" runat="server" FieldLabel="Numero Tarjeta" MaxLength="20" Disabled="true"  />
                    <ext:TextField id="nombreTitular" runat="server" FieldLabel="Nombre del Titular" MaxLength="20" AnchorHorizontal="100%"  />      
                    <ext:TextField id="codigoSeguridad" runat="server" FieldLabel="Codigo de seguridad" MaxLength="4" Width="40"  />      
                    
                </Items>
            
            </ext:FormPanel>
            
             <ext:FormPanel 
                ID="FormPanel6" 
                runat="server"            
                Frame="true"
                LabelAlign="Right"            
                AutoRender ="true"             
                StyleSpec="margin-top: 5px;margin-right: 5px;margin-left: 5px">                            
            
                <Items>    
                
                    <ext:Button ID="Button16" runat="server" icon="Accept" Text="Procesar" StyleSpec="margin-left: 80%" ></ext:Button>
                                
                </Items> 
            
            </ext:FormPanel>
    </Content> 
    
    </ext:Window> 
    
    
        
</asp:Content>