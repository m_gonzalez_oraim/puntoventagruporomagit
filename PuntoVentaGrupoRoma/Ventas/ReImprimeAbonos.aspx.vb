﻿Imports System.Collections.Generic
Imports Ext.Net
Imports System.Data
Imports System.Net

Partial Public Class ReImprimeAbonos
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private Cliente As String
    Private Fecha As String
    Private Folio As String
    Private Prefijo As String
    Private boton As Boolean = False
    Private IDToFind As String


    Public Class ReimprimeAbono
        Implements SessionState.IReadOnlySessionState

#Region "Constructor Ventas"

        Public Sub New(ByVal pId As String _
                       , ByVal pventa As String _
                       , ByVal pabono As String _
                        , ByVal pfechaventa As String _
                        , ByVal pfecharecibo As String _
                        , ByVal pmontopagado As String _
                        , ByVal pReimprimir As String _
                        , ByVal pFacturado As String _
                        , ByVal pRestante As String _
                        , ByVal pDevolucion As String _
                        , ByVal pNCE As String _
                        )

            Id = pId
            venta = pventa
            abono = pabono
            fechaventa = pfechaventa
            fecharecibo = pfecharecibo
            montopagado = pmontopagado
            Reimprimir = pReimprimir
            Facturado = pFacturado
            Restante = pRestante
            Devolucion = pDevolucion
            NCE = pNCE

        End Sub

#End Region

#Region "Propiedades Ventas"


        Private m_Devolucion As String
        Public Property Devolucion() As String
            Get
                Return m_Devolucion
            End Get
            Set(ByVal value As String)
                m_Devolucion = value
            End Set
        End Property

        Private m_NCE As String
        Public Property NCE() As String
            Get
                Return m_NCE
            End Get
            Set(ByVal value As String)
                m_NCE = value
            End Set
        End Property


        Private m_Restante As String
        Public Property Restante() As String
            Get
                Return m_Restante
            End Get
            Set(ByVal value As String)
                m_Restante = value
            End Set
        End Property

        Public Property abono() As String
            Get
                Return m_abono
            End Get
            Set(ByVal value As String)
                m_abono = value
            End Set
        End Property
        Private m_abono As String

        Public Property Facturado() As String
            Get
                Return m_Facturado
            End Get
            Set(ByVal value As String)
                m_Facturado = value
            End Set
        End Property
        Private m_Facturado As String


        Public Property Reimprimir() As String
            Get
                Return m_Reimprimir
            End Get
            Set(ByVal value As String)
                m_Reimprimir = value
            End Set
        End Property
        Private m_Reimprimir As String

        Public Property fechaventa() As String
            Get
                Return m_fechaventa
            End Get
            Set(ByVal value As String)
                m_fechaventa = value
            End Set
        End Property
        Private m_fechaventa As String

        Public Property venta() As String
            Get
                Return m_venta
            End Get
            Set(ByVal value As String)
                m_venta = value
            End Set
        End Property
        Private m_venta As String


        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer

        Public Property fecharecibo() As String
            Get
                Return m_fecharecibo
            End Get
            Set(ByVal value As String)
                m_fecharecibo = value
            End Set
        End Property
        Private m_fecharecibo As String

        Public Property montopagado() As String
            Get
                Return m_montopagado
            End Get
            Set(ByVal value As String)
                m_montopagado = value
            End Set
        End Property
        Private m_montopagado As String

#End Region

    End Class

#Region "Metodos Ventas"

    Private ReadOnly Property Ventas() As List(Of ReimprimeAbono)

        Get

            Dim oList As New List(Of ReimprimeAbono)()
            Dim dt As New DataTable
            Dim sQuery As String

            sQuery = " select " & vbNewLine
            sQuery = sQuery & " vp.id as id" & vbNewLine
            sQuery = sQuery & " ,isnull(ve.Prefijo + '-' + ve.Folio,'') as [venta]" & vbNewLine
            sQuery = sQuery & " ,isnull(vp.Prefijo + '-' + vp.Folio,'') as [abono]" & vbNewLine
            sQuery = sQuery & " ,ve.Fecha as [fechaventa]" & vbNewLine
            sQuery = sQuery & " ,vp.Fecha as [fecharecibo]" & vbNewLine
            sQuery = sQuery & " ,Monto as  [montopagado]" & vbNewLine
            sQuery = sQuery & " ,'' as [Reimprimir]" & vbNewLine
            sQuery = sQuery & " ,isnull(ve.Archivo_FE,'') as [Facturado]" & vbNewLine
            sQuery = sQuery & " ,(select SUM(TotalLinea) from VentasDetalle where IDVenta = ve.ID) - (select SUM(monto) from VentasPagos where IDVenta=ve.id) as [Restante]" & vbNewLine
            sQuery = sQuery & " ,(select COUNT(*) from DevolucionesEncabezado DE where de.IDVenta=ve.id) as [Devolucion]" & vbNewLine
            sQuery = sQuery & " ,isnull((select case isnull(Archivo_FE,'') when '' then '0' else '1' end from DevolucionesEncabezado DE where de.IDVenta=ve.id),'0') as [NCE]" & vbNewLine
            sQuery = sQuery & " from VentasPagos VP,VentasEncabezado VE" & vbNewLine
            sQuery = sQuery & " where vp.IDVenta = ve.id" & vbNewLine
            sQuery = sQuery & " and ve.idstore='" & Session("IDSTORE") & "'" & vbNewLine

            If boton = False Then

                sQuery = sQuery & " and     (VE.IDCliente = '" & Cliente & "')" & vbNewLine

                If Fecha <> "" Then

                    sQuery = sQuery & " AND (CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar)  = '" & Fecha & "') " & vbNewLine
                    sQuery = sQuery & " AND (VE.Folio = '" & Folio & "') AND (VE.Prefijo = '" & Prefijo & "') " & vbNewLine
                End If

            Else

                sQuery = sQuery & "and ve.Folio = '" & Folio & "'"

            End If

            sQuery = sQuery & " order by vp.idventa desc" & vbNewLine
            'sQuery = sQuery & "  and VE.StatusVenta='O' and ((select isnull(SUM(totallinea),0) from VentasDetalle where IDVenta=ve.ID) - (select isnull(SUM(monto),0) from VentasPagos where IDVenta=ve.ID)) > 0"

            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devoluciones", connstringWEB)

            For Each Drow As DataRow In dt.Rows

                oList.Add(New ReimprimeAbono(Drow("id"), Drow("venta"), Drow("abono"), Drow("fechaventa"), Drow("fecharecibo"), Drow("montopagado"), Drow("Reimprimir"), Drow("Facturado"), Drow("Restante"), Drow("Devolucion"), Drow("NCE")))

            Next

            Return oList

        End Get

    End Property

    Private Shared lockObj As New Object()

    Private ReadOnly Property CurrentData() As List(Of ReimprimeAbono)
        Get
            Dim ventas = Session("Abonos")
            ventas = Me.Ventas
            Session("Abonos") = ventas
            Return DirectCast(ventas, List(Of ReimprimeAbono))
        End Get
    End Property

    Private Function AddAbono(ByVal venta As ReimprimeAbono) As Integer
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            venta.Id = ventas.Count
            ventas.Add(venta)
            Session("Abonos") = ventas
            Return venta.Id
        End SyncLock
    End Function

    Private Sub DeleteAbono(ByVal id As Integer)
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            Dim venta As ReimprimeAbono = Nothing

            For Each p As ReimprimeAbono In ventas
                If p.Id = id Then
                    venta = p
                    Exit For
                End If
            Next

            If venta Is Nothing Then
            End If

            ventas.Remove(venta)

            Session("Abonos") = ventas
        End SyncLock
    End Sub

    Private Sub UpdateAbono(ByVal abono As ReimprimeAbono, ByVal pAbonos As List(Of ReimprimeAbono))
        SyncLock lockObj

            Dim abonos = pAbonos 'Me.CurrentData
            Dim updatingAbono As ReimprimeAbono = Nothing

            For Each p As ReimprimeAbono In abonos
                If p.Id = abono.Id Then
                    updatingAbono = p
                    Exit For
                End If
            Next

            If updatingAbono Is Nothing Then
                Throw New Exception("Venta not found")
            End If


            Session("Abonos") = abonos

        End SyncLock
    End Sub

    Private Sub BindData(ByVal sCliente As String, ByVal sFecha As String, ByVal sFolio As String, ByVal sPrefix As String, Optional ByRef bboton As Boolean = False)
        Cliente = sCliente
        Fecha = sFecha
        Folio = sFolio
        Prefijo = sPrefix

        boton = bboton

        Me.StoreDevoluciones.DataSource = Me.CurrentData
        Me.StoreDevoluciones.DataBind()
    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)

        Dim Abonos As ChangeRecords(Of ReimprimeAbono)
        Abonos = e.DataHandler.BatchObjectData(Of ReimprimeAbono)()

        If Session("bCreated") = False Then

            For Each created As ReimprimeAbono In Abonos.Created

                Session("bCreated") = True

                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddAbono(created)

            Next

        Else

            Session("bCreated") = False

        End If

        For Each deleted As ReimprimeAbono In Abonos.Deleted
            Me.DeleteAbono(deleted.Id)

        Next

        For Each updated As ReimprimeAbono In Abonos.Updated

            Me.UpdateAbono(updated, Session("Abonos"))

        Next
        e.Cancel = True
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Session("STORETYPEID") = 3 Then
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

        If IsNothing(Session("REIMPRIMEFACTUARA")) = False Then

            Response.Write("<script>")
            Response.Write("window.open('" & Session("REIMPRIMEFACTUARA") & "','_blank')")
            Response.Write("</script>")

            Session("REIMPRIMEFACTUARA") = Nothing

        End If

        Dim useConfirmation__1 As Boolean


        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        oDB.ConectaDBConnString(connstringWEB)

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.StoreDevoluciones.SuspendScripting()
            'Me.StoreDevoluciones.UseIdConfirmation = useConfirmation__1
            Me.StoreDevoluciones.ResumeScripting()
        End If


    End Sub

    Protected Sub realizaAbono(ByVal sender As Object, ByVal e As DirectEventArgs)

    End Sub


    <DirectMethod()> _
    Public Sub Reimprime(ByVal IdAbono As String, ByVal tipo As String)

        Dim sQuery As String
        Dim dt As New DataTable

        Select Case tipo
            Case "Pedido"

                sQuery = "select IDVenta from VentasPagos where ID=" & IdAbono
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                Dim idVenta As String = dt.Rows.Item(0).Item(0)

                Session("IDPRINTVENTA") = idVenta
                Session("POSTBACK") = "ReimpresionFacturacion"
                Response.Redirect("../printControl/imprimeventa.aspx")

            Case "NCInterna"

                sQuery = "select IDVenta from VentasPagos where ID=" & IdAbono
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                Dim idVenta As String = dt.Rows.Item(0).Item(0)

                Session("IDPRINTVENTA") = idVenta
                Session("POSTBACK") = "ReimpresionCancelaVenta"
                Response.Redirect("../printControl/imprimeventa.aspx")

            Case "Abono"

                Session("IDPRINTVENTA") = IdAbono
                Session("POSTBACK") = "ReimpresionAbonos"
                Response.Redirect("../printControl/imprimeventa.aspx")

            Case "CFactura"


                Dim sError As Boolean = False

                sQuery = "select "
                sQuery = sQuery & " (select ItemName from " & Session("SAPDB") & ".oitm where itemcode=(select ArticuloSBO from Articulos where IDArticulo=VentasDetalle.IDArticulo)) as [Articulo]"
                sQuery = sQuery & " ,sum(Cantidad) as [Cantidad]"
                sQuery = sQuery & " ,IDStore"
                sQuery = sQuery & " ,isnull((select Cantidad from Inventarios where IDArticulo=VentasDetalle.IDArticulo and Almacen=VentasDetalle.IDStore),0) as Existencia"
                sQuery = sQuery & " from VentasDetalle where IDVenta = (select IDVenta from VentasPagos where ID=" & IdAbono & ")"
                sQuery = sQuery & " group by IDStore,IDArticulo"

                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                For Each row As DataRow In dt.Rows

                    If row("Cantidad") > row("Existencia") Then

                        sError = True

                        Dim msg As New Ext.Net.Notification
                        Dim nconf As New Ext.Net.NotificationConfig
                        nconf.IconCls = "icon-exclamation"
                        nconf.Html = "Esta venta no puede facturarse debido a que hay existencia insuficiente del articulo " & row("Articulo") & "<br> Existencia : " & row("Existencia")
                        nconf.Title = "Error"
                        msg.Configure(nconf)
                        msg.Show()

                    End If

                Next

                If sError = True Then
                    Exit Sub
                End If

                Session("IDABONO") = IdAbono
                Response.Redirect("Cliente.aspx")

            Case "Factura"

                sQuery = "select Archivo_FE,year(fechaCFDI),(select FolderPDF from AdminStore where AdminStoreID=VentasEncabezado.IDStore),month(fechaCFDI),Prefijo + RIGHT('0000' + convert(varchar,Folio),5) from ventasEncabezado where id=(select IDVenta from VentasPagos where ID=" & IdAbono & ")"
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                Dim mes As String = ""

                Select Case dt.Rows.Item(0).Item(3)
                    Case 1
                        mes = "Enero"
                    Case 2
                        mes = "Febrero"
                    Case 3
                        mes = "Marzo"
                    Case 4
                        mes = "Abril"
                    Case 5
                        mes = "Mayo"
                    Case 6
                        mes = "Junio"
                    Case 7
                        mes = "Julio"
                    Case 8
                        mes = "Agosto"
                    Case 9
                        mes = "Septiembre"
                    Case 10
                        mes = "Octubre"
                    Case 11
                        mes = "Noviembre"
                    Case 12
                        mes = "Diciembre"
                End Select

                Dim PDF As String = getpdf(dt.Rows.Item(0).Item(1), Replace(dt.Rows.Item(0).Item(2), " ", "%20"), mes, dt.Rows.Item(0).Item(4))

                If PDF = "" Then

                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "El pdf que esta intentando accesar aun no se a creado,intentelo mas tarde ..."
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()

                Else

                    Session("REIMPRIMEFACTUARA") = "../PDF/" & PDF
                    Response.Redirect("ReImprimeAbonos.aspx")

                End If

            Case "NCFiscal"

                sQuery = "select Archivo_FE,year((select FechaCFDI  from DevolucionesEncabezado where IDVenta=VentasEncabezado.ID)),(select FolderPDF from AdminStore where AdminStoreID=VentasEncabezado.IDStore),month((select FechaCFDI  from DevolucionesEncabezado where IDVenta=VentasEncabezado.ID)),Prefijo + RIGHT('0000' + convert(varchar,Folio),5) from ventasEncabezado where id=(select IDVenta from VentasPagos where ID=" & IdAbono & ")"
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

                Dim mes As String = ""

                Select Case dt.Rows.Item(0).Item(3)
                    Case 1
                        mes = "Enero"
                    Case 2
                        mes = "Febrero"
                    Case 3
                        mes = "Marzo"
                    Case 4
                        mes = "Abril"
                    Case 5
                        mes = "Mayo"
                    Case 6
                        mes = "Junio"
                    Case 7
                        mes = "Julio"
                    Case 8
                        mes = "Agosto"
                    Case 9
                        mes = "Septiembre"
                    Case 10
                        mes = "Octubre"
                    Case 11
                        mes = "Noviembre"
                    Case 12
                        mes = "Diciembre"
                End Select

                Dim PDF As String = getpdf(dt.Rows.Item(0).Item(1), Replace(dt.Rows.Item(0).Item(2), " ", "%20"), mes, dt.Rows.Item(0).Item(4))

                If PDF = "" Then

                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "El pdf que esta intentando accesar aun no se a creado,intentelo mas tarde ..."
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()

                Else

                    Session("REIMPRIMEFACTUARA") = "../PDF/" & PDF
                    Response.Redirect("ReImprimeAbonos.aspx")

                End If



        End Select

    End Sub

    Public Function getpdf(ByVal Año As String, ByVal Tienda As String, ByVal mes As String, ByVal Archivo As String) As String

        Dim ftpAddress = ConfigurationManager.AppSettings("FTP") & "/" & ConfigurationManager.AppSettings("PDFFOLDER") & "/" & Año & "/" & Tienda & "/" & mes
        Dim fwr As Net.FtpWebRequest = Net.FtpWebRequest.Create(ftpAddress)
        fwr.Credentials = New NetworkCredential(ConfigurationManager.AppSettings("FTPUSR"), ConfigurationManager.AppSettings("FTPPASS"))
        fwr.KeepAlive = False
        fwr.Method = WebRequestMethods.Ftp.ListDirectory
        fwr.Proxy = Nothing

        Dim nArchivo As String = ""

        Try
            Dim sr As New IO.StreamReader(fwr.GetResponse().GetResponseStream())
            Dim lst = sr.ReadToEnd().Split(vbNewLine)
            For Each file As String In lst
                file = file.Trim() 'remove any whitespace

                If Mid(file, 1, 8) <> Archivo Then Continue For

                sr.Close()
                fwr.Abort()
                fwr = Nothing

                nArchivo = ConfigurationManager.AppSettings("PDFLOCAL") & file

                If System.IO.File.Exists(nArchivo) = False Then
                    My.Computer.Network.DownloadFile(ftpAddress & "/" & file, nArchivo, ConfigurationManager.AppSettings("FTPUSR"), ConfigurationManager.AppSettings("FTPPASS"))
                End If

                nArchivo = file

                Exit For

            Next

            Return nArchivo


        Catch ex As Exception

tag:

        End Try

    End Function

    Public Function getLastAbono(ByVal idVenta As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from VentasPagos where IDVenta=" & idVenta

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Private Function FindID(ByVal bk As ReimprimeAbono) As Boolean
        If bk.Id = IDToFind Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function omiteNC(ByVal id As String) As Boolean


        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select AdminFolioType from StoreFolios where AdminStoreID=" & Session("IDSTORE") & " and Prefijo=(select prefijo from VentasEncabezado where ID=" & id & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("AdminFolioType") = 4 Then
                Return False
            End If

        Next

        Return True

        dt = Nothing
        GC.Collect()


    End Function

    Public Function getItemID(ByVal Item As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select IDArticulo from articulos where ArticuloSBO = '" & Item & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "IDArticulo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("IDArticulo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getLastDevolucion(ByVal user As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from devolucionesencabezado where iduser=" & user

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getDateFolio(ByVal folio As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as Devolucion from ventasencabezado where folio='" & folio.Split("-")(1) & "' and prefijo='" & folio.Split("-")(0) & "' and CONVERT(varchar(10), fecha,101) = CONVERT(varchar(10), getdate(),101)"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devolucion", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("Devolucion") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getCurrentFolio(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select (currentfolio + 1) as [NuevoFolio] from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "UltimoFolio", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NuevoFolio")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getPrefix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select prefijo from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Prefijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("prefijo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function inventoryExist(ByVal IDArticulo As String, ByVal WHSID As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as exist from Inventarios where IDArticulo=" & IDArticulo & " and Almacen='" & WHSID & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Exist", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("exist") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    <DirectMethod()> _
    Public Sub GetPeriods(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) as Periodo "
        squery = squery & " from ventasEncabezado VE"
        squery = squery & " where VE.IDCliente =" & Value

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Periodos", connstringWEB)

        lblPeriodo.Visible = True
        lblPeriodo.Render()

        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbDia.Text = ""
        cmbDia.Items.Clear()
        cmbDia.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        cmbTipo.Disabled = True
        cmbTipo.SelectedItem.Index = 1

        For Each row As DataRow In dt.Rows
            cmbFecha.Items.Add(New Ext.Net.ListItem(row("Periodo").ToString(), row("Periodo")))
        Next

        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()
        cmbCliente.Disabled = True

        DoBind("C")

    End Sub

    <DirectMethod()> _
    Public Sub GetDays(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "Select distinct"
        squery = squery & " RIGHT('00' + convert(varchar,CAST(DATENAME(day, VE.Fecha) AS varchar)),2) as dia"
        squery = squery & " from ventasencabezado VE, Clientes CL"
        squery = squery & " where VE.IDCliente = CL.ID"
        squery = squery & " and idStore=" & Session("IDSTORE")
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & Value & "'"
        squery = squery & " order by dia"

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Dias", connstringWEB)



        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        cmbTipo.Disabled = True
        cmbTipo.SelectedItem.Index = 1

        For Each row As DataRow In dt.Rows
            cmbDia.Items.Add(New Ext.Net.ListItem(row("dia").ToString(), row("dia")))
        Next

        lblPeriodo.Visible = True
        lblPeriodo.Render()
        cmbFecha.Text = Value
        cmbFecha.Disabled = True
        cmbFecha.Enabled = False
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()

    End Sub

    <DirectMethod()> _
    Public Sub GetFolios(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct VE.folio, VE.Prefijo  + '-' + VE.Folio  as 'FolioPref'" & vbNewLine
        squery = squery & " from ventasencabezado VE, Clientes CL" & vbNewLine
        squery = squery & " where VE.IDCliente = CL.ID" & vbNewLine
        squery = squery & " and idStore=" & Session("IDSTORE") & vbNewLine
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value & vbNewLine
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & cmbFecha.SelectedItem.Value & "'" & vbNewLine
        squery = squery & " and CAST(DATENAME(DAY, VE.Fecha) AS varchar) = " & Value & vbNewLine

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Folios", connstringWEB)

        lblFolio.Visible = True
        lblFolio.Render()

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        For Each row As DataRow In dt.Rows
            cmbFolio.Items.Add(New Ext.Net.ListItem(row("FolioPref").ToString(), row("FolioPref")))
        Next

        Try

            cmbFolio.Render()
            btnClean.Render()

        Catch ex As Exception

tag:

        End Try

    End Sub

    <DirectMethod()> _
    Public Sub Clean()

        cmbCliente.Text = ""
        cmbCliente.Render()
        lblPeriodo.Render()
        cmbFecha.Text = ""
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Text = ""
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Text = ""
        cmbFolio.Render()
        btnClean.Render()
        DoBind("")

    End Sub

    <DirectMethod()> _
    Public Sub DoBind(ByVal Value As String, Optional ByVal botton As Boolean = False)

        Session("Abonos") = Nothing

        If botton = True Then
            Me.BindData("", "", Pedido.Text, "", True)
            Exit Sub
        End If

        If Value = "" Then
            Me.BindData("", "", "", "")
            Exit Sub
        End If

        Dim sm As RowSelectionModel = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
        sm.ClearSelection()
        sm.UpdateSelection()

        If Value <> "C" Then

            If getDateFolio(Value) = True Then
                sm.SelectAll()
                sm.UpdateSelection()
                cmbTipo.SelectedItem.Index = 0
                cmbTipo.Disabled = True
            Else
                cmbTipo.Disabled = True
                cmbTipo.SelectedItem.Index = 1
            End If

        Else

            cmbTipo.Disabled = True
            cmbTipo.SelectedItem.Index = 1

        End If

        If Boolean.TryParse(UseConfirmation.Text, False) Then
            Me.StoreDevoluciones.SuspendScripting()
            Me.StoreDevoluciones.ResumeScripting()
        End If

        If Value <> "C" Then
            Me.BindData(cmbCliente.SelectedItem.Value, cmbFecha.SelectedItem.Value, Value.Split("-")(1), Value.Split("-")(0))
        Else
            Me.BindData(cmbCliente.SelectedItem.Value, "", "", "")
        End If

        chkDevolber.AllowDeselect = True

    End Sub

    Private Sub Buscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Buscar.DirectClick
        DoBind("", True)
    End Sub
End Class
