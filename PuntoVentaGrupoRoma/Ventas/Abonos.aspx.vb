﻿Imports System.Collections.Generic
Imports Ext.Net
Imports System.Data

Public Class Abonos
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private Cliente As String
    Private Fecha As String
    Private Folio As String
    Private Prefijo As String
    Private boton As Boolean = False
    Private IDToFind As String

    Public Class Abono
        Implements SessionState.IReadOnlySessionState

#Region "Constructor Ventas"

        Public Sub New(ByVal pId As String _
                       , ByVal pIDVenta As String _
                       , ByVal pFecha As String _
                        , ByVal pVendedor As String _
                        , ByVal pPagado As String _
                        , ByVal pPorPagar As String _
                        , ByVal pTotal As String _
                        , ByVal pFormaDePago As String _
                        , ByVal pTerminacionTarjeta As String _
                        , ByVal pMonto As String _
                        , ByVal pObservaciones As String _
                        )

            Id = pId
            IDVenta = pIDVenta
            Fecha = pFecha
            Vendedor = pVendedor
            Pagado = pPagado
            PorPagar = pPorPagar
            Total = pTotal
            FormaDePago = pFormaDePago
            TerminacionTarjeta = pTerminacionTarjeta
            Monto = pMonto '
            Observaciones = pObservaciones '

        End Sub

#End Region

#Region "Propiedades Ventas"

        Public Property Fecha() As String
            Get
                Return m_Fecha
            End Get
            Set(ByVal value As String)
                m_Fecha = value
            End Set
        End Property
        Private m_Fecha As String


        Public Property Total() As Double
            Get
                Return m_Total
            End Get
            Set(ByVal value As Double)
                m_Total = value
            End Set
        End Property
        Private m_Total As Double

        Public Property Vendedor() As String
            Get
                Return m_Vendedor
            End Get
            Set(ByVal value As String)
                m_Vendedor = value
            End Set
        End Property
        Private m_Vendedor As String

        Public Property IDVenta() As String
            Get
                Return m_IDVenta
            End Get
            Set(ByVal value As String)
                m_IDVenta = value
            End Set
        End Property
        Private m_IDVenta As String


        Public Property Cantidad() As String
            Get
                Return m_Cantidad
            End Get
            Set(ByVal value As String)
                m_Cantidad = value
            End Set
        End Property
        Private m_Cantidad As String

        Public Property Monto() As String
            Get
                Return m_Monto
            End Get
            Set(ByVal value As String)
                m_Monto = value
            End Set
        End Property
        Private m_Monto As String

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer

        Public Property Pagado() As String
            Get
                Return m_Pagado
            End Get
            Set(ByVal value As String)
                m_Pagado = value
            End Set
        End Property
        Private m_Pagado As String

        Public Property PorPagar() As String
            Get
                Return m_PorPagar
            End Get
            Set(ByVal value As String)
                m_PorPagar = value
            End Set
        End Property
        Private m_PorPagar As String

        Public Property FormaDePago() As String
            Get
                Return m_FormaDePago
            End Get
            Set(ByVal value As String)
                m_FormaDePago = value
            End Set
        End Property
        Private m_FormaDePago As String


        Public Property TerminacionTarjeta() As String
            Get
                Return m_TerminacionTarjeta
            End Get
            Set(ByVal value As String)
                m_TerminacionTarjeta = value
            End Set
        End Property
        Private m_TerminacionTarjeta As String

        Public Property Observaciones() As String
            Get
                Return m_Observaciones
            End Get
            Set(ByVal value As String)
                m_Observaciones = value
            End Set
        End Property
        Private m_Observaciones As String

#End Region

    End Class

#Region "Metodos Ventas"

    Private ReadOnly Property Ventas() As List(Of Abono)

        Get

            Dim oList As New List(Of Abono)()
            Dim dt As New DataTable
            Dim sQuery As String

            sQuery = "set dateformat dmy " & vbNewLine
            sQuery = sQuery & " SELECT     "
            sQuery = sQuery & " VE.ID AS ID"
            sQuery = sQuery & " , ve.Prefijo + '-' + ve.Folio as IDVenta"
            sQuery = sQuery & " ,VE.fecha as [Fecha Venta]"
            sQuery = sQuery & " ,(SELECT AdminUser.FirstName + ' ' + LastName FROM AdminUser WHERE AdminUserID=ve.IDUser) as Venderor"
            sQuery = sQuery & " ,(select isnull(SUM(monto),0) from VentasPagos where IDVenta=ve.ID) as [Pagado]"
            sQuery = sQuery & " ,(select isnull(SUM(totallinea),0) from VentasDetalle where IDVenta=ve.ID) - (select isnull(SUM(monto),0) from VentasPagos where IDVenta=ve.ID) as [Por Pagar]"
            sQuery = sQuery & " ,(select isnull(SUM(totallinea),0) from VentasDetalle where IDVenta=ve.ID) as [Total]"
            sQuery = sQuery & " ,'' as [Forma de Pago]"
            sQuery = sQuery & " ,'' as [Terminacion Tarjeta]"
            sQuery = sQuery & " ,'' as [Monto]"
            sQuery = sQuery & " FROM         VentasEncabezado VE"


            If boton = False Then

                sQuery = sQuery & " WHERE     (VE.IDCliente = '" & Cliente & "')"

                If Fecha <> "" Then
                    sQuery = sQuery & " AND (CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar)  = '" & Fecha & "') "
                    sQuery = sQuery & " AND (VE.Folio = '" & Folio & "') AND (VE.Prefijo = '" & Prefijo & "') "
                End If
                sQuery = sQuery & "  and VE.StatusVenta='O' and ((select isnull(SUM(totallinea),0) from VentasDetalle where IDVenta=ve.ID) - (select isnull(SUM(monto),0) from VentasPagos where IDVenta=ve.ID)) > 0 and ve.idstore=" & Session("IDSTORE")

            Else

                sQuery = sQuery & " where ve.Folio = '" & Folio & "' and ve.idstore=" & Session("IDSTORE")

            End If

            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devoluciones", connstringWEB)

            For Each Drow As DataRow In dt.Rows

                oList.Add(New Abono(Drow("ID"), Drow("IDVenta"), Drow("Fecha Venta"), Drow("Venderor"), Drow("Pagado"), Drow("Por Pagar"), Drow("Total"), Drow("Forma de Pago"), Drow("Terminacion Tarjeta"), Drow("Monto"), ""))

            Next

            Return oList

        End Get

    End Property

    Private Shared lockObj As New Object()

    Private ReadOnly Property CurrentData() As List(Of Abono)
        Get
            Dim ventas = Session("Abonos")
            ventas = Me.Ventas
            Session("Abonos") = ventas
            Return DirectCast(ventas, List(Of Abono))
        End Get
    End Property

    Private Function AddAbono(ByVal venta As Abono) As Integer
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            venta.Id = ventas.Count
            ventas.Add(venta)
            Session("Abonos") = ventas
            Return venta.Id
        End SyncLock
    End Function

    Private Sub DeleteAbono(ByVal id As Integer)
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            Dim venta As Abono = Nothing

            For Each p As Abono In ventas
                If p.Id = id Then
                    venta = p
                    Exit For
                End If
            Next

            If venta Is Nothing Then
            End If

            ventas.Remove(venta)

            Session("Abonos") = ventas
        End SyncLock
    End Sub

    Private Sub UpdateAbono(ByVal abono As Abono, ByVal pAbonos As List(Of Abono))
        SyncLock lockObj

            Dim abonos = pAbonos 'Me.CurrentData
            Dim updatingAbono As Abono = Nothing

            For Each p As Abono In abonos
                If p.Id = abono.Id Then
                    updatingAbono = p
                    Exit For
                End If
            Next

            If updatingAbono Is Nothing Then
                Throw New Exception("Venta not found")
            End If

            Dim montoAbono As Double

            If IsNumeric(abono.Monto) Then
                montoAbono = abono.Monto
            Else
                montoAbono = 0
            End If

            If montoAbono <= CDbl(updatingAbono.PorPagar) Then

                updatingAbono.Monto = abono.Monto
                updatingAbono.FormaDePago = abono.FormaDePago
                updatingAbono.TerminacionTarjeta = abono.TerminacionTarjeta

            Else

                updatingAbono.Monto = 0
                abono.Monto = 0

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Error en el motno"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

            End If

            Session("Abonos") = abonos

        End SyncLock
    End Sub

    Private Sub BindData(ByVal sCliente As String, ByVal sFecha As String, ByVal sFolio As String, ByVal sPrefix As String, Optional ByRef bboton As Boolean = False)
        Cliente = sCliente
        Fecha = sFecha
        Folio = sFolio
        Prefijo = sPrefix

        boton = bboton

        Me.StoreDevoluciones.DataSource = Me.CurrentData
        Me.StoreDevoluciones.DataBind()
    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)
        Dim Abonos As ChangeRecords(Of Abono)
        Abonos = e.DataHandler.BatchObjectData(Of Abono)()

        If Session("bCreated") = False Then

            For Each created As Abono In Abonos.Created

                Session("bCreated") = True

                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddAbono(created)

                'If StoreDevoluciones.UseIdConfirmation Then
                'e.ConfirmationList.ConfirmRecord(tempId.ToString(), newId.ToString())
                'Else
                'StoreDevoluciones.UpdateRecordId(tempId, newId)
                'End If
            Next

        Else

            Session("bCreated") = False

        End If

        For Each deleted As Abono In Abonos.Deleted
            Me.DeleteAbono(deleted.Id)

            'If StoreDevoluciones.UseIdConfirmation Then
            '    e.ConfirmationList.ConfirmRecord(deleted.Id.ToString())
            'End If
        Next

        For Each updated As Abono In Abonos.Updated

            Me.UpdateAbono(updated, Session("Abonos"))

            'If StoreDevoluciones.UseIdConfirmation Then
            '    e.ConfirmationList.ConfirmRecord(updated.Id.ToString())
            'End If

        Next
        e.Cancel = True
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'If Ext.Net.X.IsAjaxRequest = False Then

        'Session("IDSTORE") = 1

        If Session("STORETYPEID") = 3 Then
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

        Dim useConfirmation__1 As Boolean

        'If Ext.Net.X.IsAjaxRequest = False Then

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        oDB.ConectaDBConnString(connstringWEB)

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.StoreDevoluciones.SuspendScripting()
            'Me.StoreDevoluciones.UseIdConfirmation = useConfirmation__1
            Me.StoreDevoluciones.ResumeScripting()
        End If

        'End If

        'End If

        'Me.BindData("5", "Agosto-2010", "2")

    End Sub

    Protected Sub realizaAbono(ByVal sender As Object, ByVal e As DirectEventArgs)


        Dim squery As String
        Dim result As New StringBuilder()

        'StoreDevoluciones.CommitChanges()


        btnDevolucion.Disabled = True
        btnDevolucion.Enabled = False

        result.Append("<b>Selected Rows</b></br /><ul>")
        Dim sm As RowSelectionModel = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
        btnDevolucion.Enabled = False

        If sm.SelectedRows.Count = 0 Then


            Dim msg As New Ext.Net.Notification
            Dim nconf As New Ext.Net.NotificationConfig
            nconf.IconCls = "icon-exclamation"
            nconf.Html = "No a seleccionado ninguna venta para abonar"
            nconf.Title = "Error"
            msg.Configure(nconf)
            msg.Show()

            btnDevolucion.Enabled = True
            btnDevolucion.Disabled = False
            DoBind("C")
            Exit Sub
        End If


        For Each row As SelectedRow In sm.SelectedRows

            IDToFind = row.RecordID
            Dim ndx As Integer = DirectCast(Session("Abonos"), List(Of Abono)).FindIndex(AddressOf FindID)


            If DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).FormaDePago = "" Then


                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Seleccione formade pago de la venta # " & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).IDVenta
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                btnDevolucion.Enabled = True
                btnDevolucion.Disabled = False
                DoBind("C")

                Exit Sub

            End If


            If DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).FormaDePago <> "Efectivo" Then


                If DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).TerminacionTarjeta = "" Then

                    Dim msg As New Ext.Net.Notification
                    Dim nconf As New Ext.Net.NotificationConfig
                    nconf.IconCls = "icon-exclamation"
                    nconf.Html = "Seleccione terminacion de tarjeta/cuenta de la venta # " & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).IDVenta
                    nconf.Title = "Error"
                    msg.Configure(nconf)
                    msg.Show()

                    btnDevolucion.Enabled = True
                    btnDevolucion.Disabled = False
                    DoBind("C")

                    Exit Sub

                End If

            End If

            Dim montoAbono As Double

            If IsNumeric(DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).Monto) Then
                montoAbono = DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).Monto
            Else
                montoAbono = 0
            End If


            If montoAbono > DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).PorPagar Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Monto erroneo de la venta # " & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).IDVenta
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                btnDevolucion.Enabled = True
                btnDevolucion.Disabled = False
                DoBind("C")
                Exit Sub

            End If


            If montoAbono <= 0 Then

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Monto erroneo de la venta # " & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).IDVenta & " el monto debe ser mayor a 0"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

                btnDevolucion.Enabled = True
                btnDevolucion.Disabled = False
                DoBind("C")
                Exit Sub

            End If

        Next

        For Each row As SelectedRow In sm.SelectedRows

            IDToFind = row.RecordID
            Dim ndx As Integer = DirectCast(Session("Abonos"), List(Of Abono)).FindIndex(AddressOf FindID)

            squery = " insert into VentasPagos (IDVenta,AdminStoreID,TipoVenta,FormaPago,Monto,Fecha,StatusPago,NoCuenta,Prefijo,Folio) values (" & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).Id & "," & Session("IDSTORE") & ",'VT','" & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).FormaDePago & "'," & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).Monto & ",getdate(),'O','" & DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).TerminacionTarjeta & "',(select Prefijo from StoreFolios  where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=2) ,(select CurrentFolio + 1 from StoreFolios  where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=2))" & vbNewLine
            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

            squery = " Update  StoreFolios set CurrentFolio = CurrentFolio + 1 where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=2" & vbNewLine
            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

            Session("idAbono") = getLastAbono(DirectCast(Session("Abonos"), List(Of Abono)).Item(ndx).Id)

            Session("IDPRINTVENTA") = Session("idAbono")

            InsertaLog(Tipos.AB, Session("idAbono"), Session("IDSTORE"), Session("WHSID"))

        Next

        Session("POSTBACK") = "Abonos"
        Response.Redirect("../printControl/imprimeventa.aspx")

    End Sub

    Public Function getLastAbono(ByVal idVenta As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from VentasPagos where IDVenta=" & idVenta

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Private Function FindID(ByVal bk As Abono) As Boolean
        If bk.Id = IDToFind Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function omiteNC(ByVal id As String) As Boolean


        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select AdminFolioType from StoreFolios where AdminStoreID=" & Session("IDSTORE") & " and Prefijo=(select prefijo from VentasEncabezado where ID=" & id & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("AdminFolioType") = 4 Then
                Return False
            End If

        Next

        Return True

        dt = Nothing
        GC.Collect()


    End Function

    Public Function getItemID(ByVal Item As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select IDArticulo from articulos where ArticuloSBO = '" & Item & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "IDArticulo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("IDArticulo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getLastDevolucion(ByVal user As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from devolucionesencabezado where iduser=" & user

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getDateFolio(ByVal folio As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as Devolucion from ventasencabezado where folio='" & folio.Split("-")(1) & "' and prefijo='" & folio.Split("-")(0) & "' and CONVERT(varchar(10), fecha,101) = CONVERT(varchar(10), getdate(),101)"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devolucion", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("Devolucion") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getCurrentFolio(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select (currentfolio + 1) as [NuevoFolio] from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "UltimoFolio", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NuevoFolio")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getPrefix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select prefijo from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Prefijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("prefijo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function inventoryExist(ByVal IDArticulo As String, ByVal WHSID As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as exist from Inventarios where IDArticulo=" & IDArticulo & " and Almacen='" & WHSID & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Exist", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("exist") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function



    <DirectMethod()> _
    Public Sub GetPeriods(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) as Periodo "
        squery = squery & " from ventasEncabezado VE"
        squery = squery & " where VE.IDCliente =" & Value

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Periodos", connstringWEB)

        'cmbFolio.Visible = False

        lblPeriodo.Visible = True
        lblPeriodo.Render()

        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbDia.Text = ""
        cmbDia.Items.Clear()
        cmbDia.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        cmbTipo.Disabled = True
        cmbTipo.SelectedItem.Index = 1

        For Each row As DataRow In dt.Rows
            cmbFecha.Items.Add(New Ext.Net.ListItem(row("Periodo").ToString(), row("Periodo")))
        Next

        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()
        cmbCliente.Disabled = True

        DoBind("C")

    End Sub

    <DirectMethod()> _
    Public Sub GetDays(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        'squery = "select distinct CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) as Periodo "
        'squery = squery & " from ventasEncabezado VE"
        'squery = squery & " where VE.IDCliente =" & Value

        squery = "Select distinct"
        squery = squery & " RIGHT('00' + convert(varchar,CAST(DATENAME(day, VE.Fecha) AS varchar)),2) as dia"
        squery = squery & " from ventasencabezado VE, Clientes CL"
        squery = squery & " where VE.IDCliente = CL.ID"
        squery = squery & " and idStore=" & Session("IDSTORE")
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & Value & "'"
        squery = squery & " order by dia"

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Dias", connstringWEB)

        'cmbFolio.Visible = False

        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        cmbTipo.Disabled = True
        cmbTipo.SelectedItem.Index = 1

        For Each row As DataRow In dt.Rows
            cmbDia.Items.Add(New Ext.Net.ListItem(row("dia").ToString(), row("dia")))
        Next

        lblPeriodo.Visible = True
        lblPeriodo.Render()
        cmbFecha.Text = Value
        cmbFecha.Disabled = True
        cmbFecha.Enabled = False
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()

    End Sub

    <DirectMethod()> _
    Public Sub GetFolios(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct VE.folio, VE.Prefijo  + '-' + VE.Folio  as 'FolioPref'" & vbNewLine
        squery = squery & " from ventasencabezado VE, Clientes CL" & vbNewLine
        squery = squery & " where VE.IDCliente = CL.ID" & vbNewLine
        squery = squery & " and idStore=" & Session("IDSTORE") & vbNewLine
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value & vbNewLine
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & cmbFecha.SelectedItem.Value & "'" & vbNewLine
        squery = squery & " and CAST(DATENAME(DAY, VE.Fecha) AS varchar) = " & Value & vbNewLine
        squery = squery & " and (select count(*) from ventasDetalle VDE,ventasEncabezado VEN where VEN.ID=VDE.IDVenta and statusLinea='O' and VEN.Folio=VE.Folio) <> 0"

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Folios", connstringWEB)

        lblFolio.Visible = True
        lblFolio.Render()

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        For Each row As DataRow In dt.Rows
            cmbFolio.Items.Add(New Ext.Net.ListItem(row("FolioPref").ToString(), row("FolioPref")))
        Next

        Try

            cmbFolio.Render()
            btnClean.Render()

        Catch ex As Exception

tag:

        End Try

    End Sub

    <DirectMethod()> _
    Public Sub Clean()

        cmbCliente.Text = ""
        cmbCliente.Render()
        lblPeriodo.Render()
        cmbFecha.Text = ""
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Text = ""
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Text = ""
        cmbFolio.Render()
        btnClean.Render()
        DoBind("")

    End Sub

    <DirectMethod()> _
    Public Sub DoBind(ByVal Value As String, Optional ByVal botton As Boolean = False)

        Session("Abonos") = Nothing

        If botton = True Then
            Me.BindData("", "", Pedido.Text, "", True)
            Exit Sub
        End If

        If Value = "" Then
            Me.BindData("", "", "", "")
            Exit Sub
        End If

        Dim sm As RowSelectionModel = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
        sm.ClearSelection()
        sm.UpdateSelection()

        If Value <> "C" Then

            If getDateFolio(Value) = True Then
                'cmbTipo.Disabled = False
                'Nuevo
                sm.SelectAll()
                sm.UpdateSelection()
                cmbTipo.SelectedItem.Index = 0
                cmbTipo.Disabled = True
            Else
                cmbTipo.Disabled = True
                cmbTipo.SelectedItem.Index = 1
            End If

        Else

            cmbTipo.Disabled = True
            cmbTipo.SelectedItem.Index = 1

        End If

        If Boolean.TryParse(UseConfirmation.Text, False) Then
            Me.StoreDevoluciones.SuspendScripting()
            'Me.StoreDevoluciones.UseIdConfirmation = False
            Me.StoreDevoluciones.ResumeScripting()
        End If

        If Value <> "C" Then
            Me.BindData(cmbCliente.SelectedItem.Value, cmbFecha.SelectedItem.Value, Value.Split("-")(1), Value.Split("-")(0))
        Else
            Me.BindData(cmbCliente.SelectedItem.Value, "", "", "")
        End If

        chkDevolber.AllowDeselect = True

    End Sub

    Private Sub Buscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Buscar.DirectClick
        DoBind("", True)
    End Sub


End Class
