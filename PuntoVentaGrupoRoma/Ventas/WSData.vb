﻿Public Class WSData
    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Public dnDistribuidor As String
    Public tipoActivacion As String
    Public nDo As String
    Public di As String
    Public idpv As String
    Public emailDo As String
    Public rfc As String
    Public codCliente As String
    Public fchNacimiento As String
    Public nombre As String
    Public apePaterno As String
    Public apeMaterno As String
    Public calle As String
    Public numExt As String
    Public numInt As String
    Public telefonoContacto1 As String
    Public telefonoContacto2 As String
    Public emailCliente As String
    Public curp As String
    Public cp As String
    Public colonia As String
    Public codColonia As String
    Public delegacion As String
    Public codDelegacion As String
    Public idRegion As String
    Public ciudad As String
    Public codCiudad As String
    Public estado As String
    Public codEstado As String
    Public codeZonaImpo As String
    Public coloniaAlterna1 As String
    Public coloniaAlterna2 As String
    Public tipoOferta As String
    Public cveProducto As String
    Public numLineas As String
    'Public plazo As String
    Public modVenta As String
    Public formaPago As String
    Public serviciosSup As String
    Public domiciliacion As String
    Public banco As String
    Public numeroTarjeta As String
    Public vigenciaTarjeta As String
    Public tipoTarjeta As String
    Public codigoSeguridad As String
    Public nombreTitular As String
    Public facturaElectronica As String
    Public iva As String
    Public cDiferenciada As String
    Public addenda As String
    Public pregunta1 As String
    Public pregunta2 As String
    Public pregunta3 As String
    Public credTarjeta As String
    Public institucionCredTarjeta As String
    Public digitosCredTarjeta As String
    Public credHipotecario As String
    Public institucionCredHipotecario As String
    Public credAutomotriz As String
    Public institucionCredAutomotriz As String
    'Public codPlan As String
    Public codSS As String

    'Public detalle As New List(Of Detalle)

    '    Public Sub LlenaDatosVenta(ByVal Venta As List(Of Facturacion.Venta))

    '        Dim linea As Integer = 1

    '        detalle.Clear()

    '        For Each oVenta As Facturacion.Venta In Venta

    '            With oVenta

    '                If .IVA <> "" Or .Monto <> "" Or .PrecioUnitario <> "" Then
    '                    numLineas = numLineas + 1

    '                    Dim squery As String
    '                    Dim dtC As New DataTable

    '                    squery = "select * from OPLN where ListNum=" & .Medida
    '                    dtC = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringSAP)
    '                    Dim tPlazo As String = ""
    '                    Dim tcPlan As String = ""

    '                    For Each row As DataRow In dtC.Rows
    '                        tPlazo = row("U_Plazo")
    '                        tcPlan = row("U_codPlan")
    '                    Next

    '                    Dim oTmp As New Detalle(linea, tPlazo, .Monto, .IVA, .PrecioUnitario, "0", tcPlan, "")
    '                    detalle.Add(oTmp)

    '                End If

    '            End With

    '            linea = linea + 1

    '        Next

    '    End Sub

    '    Public Sub LlenaDatosCliente(ByVal cliente As Facturacion.Cliente)

    '        With cliente

    '            rfc = .RFC
    '            codCliente = .Id
    '            fchNacimiento = Replace(.actFCHNACIMIENTO, "/", "")
    '            nombre = .actNOMBRE
    '            apePaterno = .actAPEPATERNO
    '            apeMaterno = .actAPEMATERNO
    '            calle = .actCALLE
    '            numExt = .actNUMEXT
    '            numInt = .actNUMINT
    '            telefonoContacto1 = .TelCasa
    '            telefonoContacto2 = .TelOfc
    '            emailCliente = .Correo
    '            curp = .IDRef
    '            cp = .CP
    '            colonia = .Colonia
    '            codColonia = .actCod_COMUNAcolonia
    '            delegacion = .DelMun
    '            codDelegacion = .actCod_PROVINCIAmunicipio
    '            idRegion = .actCod_REGIONestado
    '            ciudad = .actCIUDAD
    '            codCiudad = .actCod_CIUDAD
    '            estado = .Estado
    '            codEstado = .actCod_REGIONestado
    '            codeZonaImpo = getZona(idRegion, codDelegacion, codCiudad)

    '        End With

    '    End Sub

    '    Public Function DatosCliente() As String

    '        Dim sDCliente As String

    '        sDCliente = "Nombre: " & nombre & " " & apePaterno & " " & apeMaterno & vbNewLine
    '        sDCliente = sDCliente & "RFC: " & rfc & " CURP: " & curp & vbNewLine
    '        sDCliente = sDCliente & "Calle: " & calle & " No. Ext " & numExt & " No. Int " & numInt & vbNewLine
    '        sDCliente = sDCliente & "Colonia: " & colonia & " Municipio/Delegacion: " & delegacion & vbNewLine
    '        sDCliente = sDCliente & "Ciudad: " & ciudad & " Estado: " & estado & " CP: " & cp & vbNewLine
    '        sDCliente = sDCliente & "Tel. Casa: " & telefonoContacto1 & " Tel. Oficina: " & telefonoContacto2 & " email: " & emailCliente

    '        Return (sDCliente)

    '    End Function

    '    Private Function getZona(ByVal cRegion As String, ByVal cProvincia As String, ByVal cCiudad As String) As String

    '        Dim squery As String
    '        Dim dtC As New DataTable

    '        squery = "select * from ge_zonaciudad where COD_REGION='" & cRegion & "' and COD_PROVINCIA='" & cProvincia & "' and COD_CIUDAD='" & cCiudad & "'"
    '        dtC = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "SOCCLI", connstringWEB)

    '        For Each row As DataRow In dtC.Rows
    '            Return row("COD_ZONAIMPO")
    '        Next

    '        Return ("1")

    '    End Function

    '    Public Sub New()

    '        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    '        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

    '    End Sub

    'End Class

    'Public Class Detalle

    '    Public linea As String
    '    Public plazo As String
    '    Public icc As String
    '    Public imei As String
    '    Public dn As String
    '    Public codSS As String
    '    Public codPlan As String
    '    Public cveActivacion As String


    '    Public Sub New(ByVal p_linea As String, ByVal p_plazo As String, ByVal p_icc As String, ByVal p_imei As String, ByVal p_dn As String, ByVal p_codSS As String, ByVal p_codPlan As String, ByVal p_cveActivacion As String)

    '        linea = p_linea
    '        plazo = p_plazo
    '        icc = p_icc
    '        imei = p_imei
    '        dn = p_dn
    '        codSS = p_codSS
    '        codPlan = p_codPlan
    '        cveActivacion = p_cveActivacion

    '    End Sub
End Class
