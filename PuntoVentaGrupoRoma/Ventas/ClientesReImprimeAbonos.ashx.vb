﻿Imports System.Web
Imports System.Web.Services
Imports Ext.Net
Imports System.Collections
Imports System.Collections.Generic
Imports System.Globalization
Imports System.Xml

<WebService([Namespace]:="http://tempuri.org/")> _
    <WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class ClientesReImprimeAbonos
    Implements System.Web.IHttpHandler, SessionState.IReadOnlySessionState


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/json"

        Dim start = 0
        Dim limit = 10
        Dim sort = String.Empty
        Dim dir = String.Empty
        Dim query = String.Empty

        If Not String.IsNullOrEmpty(context.Request("start")) Then
            start = Integer.Parse(context.Request("start"))
        End If

        If Not String.IsNullOrEmpty(context.Request("limit")) Then
            limit = Integer.Parse(context.Request("limit"))
        End If

        If Not String.IsNullOrEmpty(context.Request("sort")) Then
            sort = context.Request("sort")
        End If

        If Not String.IsNullOrEmpty(context.Request("dir")) Then
            dir = context.Request("dir")
        End If

        If Not String.IsNullOrEmpty(context.Request("query")) Then
            query = context.Request("query")
        End If

        Dim clientes As Paging(Of ClienteCancelacion) = ClienteCancelacion.ClientesPaging(start, limit, sort, dir, query, context.Session("IDSTORE"))

        context.Response.Write(String.Format("{{total:{1},'clientes':{0}}}", JSON.Serialize(clientes.Data), clientes.TotalRecords))


    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

    Public Class ClienteCancelacion
        Public Sub New(ByVal cCliente As String, ByVal RFC As String, ByVal cIdCliente As String)
            Me.cCliente = cCliente
            Me.RFC = RFC
            Me.cIdCliente = cIdCliente
        End Sub

        Public Sub New()
        End Sub


        Public Property cIdCliente() As String
            Get
                Return m_cIdCliente
            End Get
            Set(ByVal value As String)
                m_cIdCliente = value
            End Set
        End Property
        Private m_cIdCliente As String

        Public Property cCliente() As String
            Get
                Return m_Cliente
            End Get
            Set(ByVal value As String)
                m_Cliente = value
            End Set
        End Property
        Private m_Cliente As String

        Public Property RFC() As String
            Get
                Return m_RFC
            End Get
            Set(ByVal value As String)
                m_RFC = value
            End Set
        End Property
        Private m_RFC As String

        Public Shared Function ClientesPaging(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal dir As String, ByVal filter As String, ByVal STOREID As String) As Paging(Of ClienteCancelacion)

            Dim clientes As List(Of ClienteCancelacion) = ClienteCancelacion.TestData(STOREID)

            If Not String.IsNullOrEmpty(filter) AndAlso filter <> "*" Then
                clientes.RemoveAll(Function(clientes__1) Not clientes__1.cCliente.ToLower().StartsWith(filter.ToLower()))
            End If

            If Not String.IsNullOrEmpty(sort) Then
                clientes.Sort()
            End If

            If (start + limit) > clientes.Count Then
                limit = clientes.Count - start
            End If

            Dim rangeClientes As List(Of ClienteCancelacion) = If((start < 0 OrElse limit < 0), clientes, clientes.GetRange(start, limit))

            Return New Paging(Of ClienteCancelacion)(rangeClientes, clientes.Count)

        End Function

        Public Shared ReadOnly Property TestData(ByVal STOREID As String) As List(Of ClienteCancelacion)
            Get
                Dim data As New List(Of ClienteCancelacion)()

                Dim dt As New DataTable
                Dim squery As String
                squery = "select distinct CL.Nombre,CL.RFC,CL.ID" & vbNewLine
                squery = squery & " from ventasencabezado VE, Clientes CL" & vbNewLine
                squery = squery & " where VE.IDCliente = CL.ID" & vbNewLine
                squery = squery & " and idStore=" & STOREID

                Dim odb As New DBMaster
                odb.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)

                dt = odb.EjecutaQry_Tabla(squery, CommandType.Text, "Clientes", ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)

                For Each Drow As DataRow In dt.Rows

                    'For Each plantNode As XmlNode In xmlDoc.SelectNodes("catalog/plant")
                    Dim ClienteCancelacion As New ClienteCancelacion()

                    ClienteCancelacion.cCliente = Drow("Nombre") & "- NC: " & Drow("id")  'plantNode.SelectSingleNode("common").InnerText
                    ClienteCancelacion.RFC = Drow("RFC") 'plantNode.SelectSingleNode("botanical").InnerText
                    ClienteCancelacion.cIdCliente = Drow("id")

                    data.Add(ClienteCancelacion)

                Next
                Return data
            End Get
        End Property
    End Class
End Class