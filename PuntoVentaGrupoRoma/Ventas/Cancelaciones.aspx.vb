﻿Imports System.Collections.Generic
Imports Ext.Net
Imports System.Data

Public Class Cancelaciones
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private Cliente As String
    Private Fecha As String
    Private Folio As String
    Private Prefijo As String
    Private boton As Boolean = False
    Private IDToFind As String

    Public Class Venta
        Implements SessionState.IReadOnlySessionState

#Region "Constructor Ventas"

        Public Sub New(ByVal pId As String, _
                       ByVal pIDVenta As String, _
                       ByVal pIDLinea As String, _
                       ByVal pArticulo As String, _
                       ByVal pDescripcionArticulo As String, _
                       ByVal pLista As String, _
                       ByVal pPU As String, _
                       ByVal PIVA As String, _
                       ByVal pObservaciones As String, _
                       ByVal pDescuento As Double, _
                       ByVal pMonto As String, _
                       ByVal pCantidad As String, _
                       ByVal pStatusLinea As String, _
                       ByVal pComentarios As String, _
                       ByVal pIDStore As Integer, _
                       ByVal pFecha As String)

            Id = pId
            IDVenta = pIDVenta
            IDLinea = pIDLinea
            Articulo = pArticulo '
            DescripcionArticulo = pDescripcionArticulo '
            Descuento = pDescuento
            Lista = pLista '
            PU = pPU ' 
            IVA = PIVA '    
            Monto = pMonto '
            Cantidad = pCantidad '
            StatusLinea = pStatusLinea
            Observaciones = pObservaciones '
            Comentarios = pComentarios
            IDStore = pIDStore
            Fecha = pFecha
        End Sub

#End Region

#Region "Propiedades Ventas"

        Public Property Fecha() As String
            Get
                Return m_Fecha
            End Get
            Set(ByVal value As String)
                m_Fecha = value
            End Set
        End Property
        Private m_Fecha As String

        Public Property IDStore() As Integer
            Get
                Return m_IDStore
            End Get
            Set(ByVal value As Integer)
                m_IDStore = value
            End Set
        End Property
        Private m_IDStore As Integer



        Public Property Descuento() As Double
            Get
                Return m_Descuento
            End Get
            Set(ByVal value As Double)
                m_Descuento = value
            End Set
        End Property
        Private m_Descuento As Double

        Public Property Comentarios() As String
            Get
                Return m_Comentarios
            End Get
            Set(ByVal value As String)
                m_Comentarios = value
            End Set
        End Property
        Private m_Comentarios As String

        Public Property IDLinea() As Integer
            Get
                Return m_IDLinea
            End Get
            Set(ByVal value As Integer)
                m_IDLinea = value
            End Set
        End Property
        Private m_IDLinea As Integer

        Public Property IDVenta() As Integer
            Get
                Return m_IDVenta
            End Get
            Set(ByVal value As Integer)
                m_IDVenta = value
            End Set
        End Property
        Private m_IDVenta As Integer

        Public Property StatusLinea() As String
            Get
                Return m_StatusLinea
            End Get
            Set(ByVal value As String)
                m_StatusLinea = value
            End Set
        End Property
        Private m_StatusLinea As String

        Public Property Referencia() As String
            Get
                Return m_Referencia
            End Get
            Set(ByVal value As String)
                m_Referencia = value
            End Set
        End Property
        Private m_Referencia As String

        Public Property ReferenciaDN() As String
            Get
                Return m_ReferenciaDN
            End Get
            Set(ByVal value As String)
                m_ReferenciaDN = value
            End Set
        End Property
        Private m_ReferenciaDN As String

        Public Property Cantidad() As String
            Get
                Return m_Cantidad
            End Get
            Set(ByVal value As String)
                m_Cantidad = value
            End Set
        End Property
        Private m_Cantidad As String

        Public Property Monto() As Double
            Get
                Return m_Monto
            End Get
            Set(ByVal value As Double)
                m_Monto = value
            End Set
        End Property
        Private m_Monto As Double

        Public Property DN() As String
            Get
                Return m_DN
            End Get
            Set(ByVal value As String)
                m_DN = value
            End Set
        End Property
        Private m_DN As String

        Public Property IMEI() As String
            Get
                Return m_IMEI
            End Get
            Set(ByVal value As String)
                m_IMEI = value
            End Set
        End Property
        Private m_IMEI As String

        Public Property SIM() As String
            Get
                Return m_SIM
            End Get
            Set(ByVal value As String)
                m_SIM = value
            End Set
        End Property
        Private m_SIM As String

        Public Property Id() As String
            Get
                Return m_Id
            End Get
            Set(ByVal value As String)
                m_Id = value
            End Set
        End Property
        Private m_Id As Integer

        Public Property Articulo() As String
            Get
                Return m_Articulo
            End Get
            Set(ByVal value As String)
                m_Articulo = value
            End Set
        End Property
        Private m_Articulo As String

        Public Property DescripcionArticulo() As String
            Get
                Return m_DescripcionArticulo
            End Get
            Set(ByVal value As String)
                m_DescripcionArticulo = value
            End Set
        End Property
        Private m_DescripcionArticulo As String

        Public Property Lista() As String
            Get
                Return m_Lista
            End Get
            Set(ByVal value As String)
                m_Lista = value
            End Set
        End Property
        Private m_Lista As String


        Public Property PU() As String
            Get
                Return m_PU
            End Get
            Set(ByVal value As String)
                m_PU = value
            End Set
        End Property
        Private m_PU As String

        Public Property IVA() As String
            Get
                Return m_IVA
            End Get
            Set(ByVal value As String)
                m_IVA = value
            End Set
        End Property
        Private m_IVA As String

        Public Property Observaciones() As String
            Get
                Return m_Observaciones
            End Get
            Set(ByVal value As String)
                m_Observaciones = value
            End Set
        End Property
        Private m_Observaciones As String

#End Region

    End Class

#Region "Metodos Ventas"

    Private ReadOnly Property Ventas() As List(Of Venta)

        Get

            Dim oList As New List(Of Venta)()
            Dim dt As New DataTable
            Dim sQuery As String

            'sQuery = "set dateformat dmy " & vbNewLine
            'sQuery = sQuery & " SELECT     VD.ID AS ID, VD.IDVenta, VD.IDLinea, Articulos.CodigoDeBarras as IDArticulo,Articulos.DescripcionSBO, VD.Lista, VD.PrecioUnitario, VD.IVA, VD.Observaciones, VD.TotalLinea, VD.IMEI, VD.ICC, " & vbNewLine
            'sQuery = sQuery & " VD.DialNumber, VD.Cantidad, VD.StatusLinea, VD.ReferenciaDN,VD.Referencia" & vbNewLine
            'sQuery = sQuery & " ,VE.fecha as [Fecha Venta]" & vbNewLine
            'sQuery = sQuery & " FROM         VentasEncabezado AS VE INNER JOIN" & vbNewLine
            'sQuery = sQuery & " VentasDetalle AS VD ON VE.ID = VD.IDVenta INNER JOIN" & vbNewLine
            'sQuery = sQuery & " Articulos ON VD.IDArticulo = Articulos.IDArticulo" & vbNewLine
            'sQuery = sQuery & " WHERE     (VE.IDCliente = '" & Cliente & "') AND (CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) " & vbNewLine
            'sQuery = sQuery & " = '" & Fecha & "') AND (VE.Folio = " & Folio & ") AND (VE.Prefijo = '" & Prefijo & "') and VD.StatusLinea='O'"

            sQuery = "set dateformat dmy " & vbNewLine
            sQuery = sQuery & " SELECT     VD.ID AS ID, VD.IDVenta, VD.IDLinea, Articulos.ArticuloSBO as IDArticulo,(select ItemName from " & Session("SAPDB") & ".OITM where ItemCode=ArticuloSBO) as DescripcionSBO, VD.Lista, VD.PrecioUnitario, VD.IVA, '' as Observaciones,VD.Descuento, VD.TotalLinea" & vbNewLine
            sQuery = sQuery & " ,VD.Cantidad, VD.StatusLinea,VD.IDStore" & vbNewLine
            sQuery = sQuery & " ,VE.fecha as [Fecha Venta]" & vbNewLine
            sQuery = sQuery & " FROM         VentasEncabezado AS VE INNER JOIN" & vbNewLine
            sQuery = sQuery & " VentasDetalle AS VD ON VE.ID = VD.IDVenta INNER JOIN" & vbNewLine
            sQuery = sQuery & " Articulos ON VD.IDArticulo = Articulos.IDArticulo" & vbNewLine

            If boton = False Then

                sQuery = sQuery & " WHERE     (VE.IDCliente = '" & Cliente & "') AND (CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) " & vbNewLine
                sQuery = sQuery & " = '" & Fecha & "') AND (VE.Folio = '" & Folio & "') AND (VE.Prefijo = '" & Prefijo & "') and VD.StatusLinea='O' and ve.idstore=" & Session("IDSTORE")

            Else

                sQuery = sQuery & " where ve.Folio = '" & Folio & "' and ve.idstore=" & Session("IDSTORE")

            End If

            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devoluciones", connstringWEB)

            For Each Drow As DataRow In dt.Rows

                oList.Add(New Venta(Drow("ID"), Drow("IDVenta"), Drow("IDLinea"), Drow("IDArticulo"), Drow("DescripcionSBO"), Drow("Lista"), Drow("PrecioUnitario"), Drow("IVA"), Drow("Observaciones"), Drow("Descuento"), Drow("TotalLinea"), Drow("Cantidad"), Drow("StatusLinea"), "Vendible", Drow("IDStore"), Drow("Fecha Venta")))

            Next

            Return oList

        End Get

    End Property

    Private Shared lockObj As New Object()

    Private ReadOnly Property CurrentData() As List(Of Venta)
        Get
            Dim ventas = Session("Ventas")
            ventas = Me.Ventas
            Session("Ventas") = ventas
            Return DirectCast(ventas, List(Of Venta))
        End Get
    End Property

    Private Function AddVenta(ByVal venta As Venta) As Integer
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            venta.Id = ventas.Count
            ventas.Add(venta)
            Session("Ventas") = ventas
            Return venta.Id
        End SyncLock
    End Function

    Private Sub DeleteVenta(ByVal id As Integer)
        SyncLock lockObj
            Dim ventas = Me.CurrentData
            Dim venta As Venta = Nothing

            For Each p As Venta In ventas
                If p.Id = id Then
                    venta = p
                    Exit For
                End If
            Next

            If venta Is Nothing Then
            End If

            ventas.Remove(venta)

            Session("Ventas") = ventas
        End SyncLock
    End Sub

    Private Sub UpdateVenta(ByVal venta As Venta, ByVal pVentas As List(Of Venta))
        SyncLock lockObj

            Dim ventas = pVentas 'Me.CurrentData
            Dim updatingVenta As Venta = Nothing

            For Each p As Venta In ventas
                If p.Id = venta.Id Then
                    updatingVenta = p
                    Exit For
                End If
            Next

            If updatingVenta Is Nothing Then
                Throw New Exception("Venta not found")
            End If

            If venta.Cantidad <= updatingVenta.Cantidad Then

                updatingVenta.Cantidad = venta.Cantidad
                updatingVenta.Comentarios = venta.Comentarios

            Else

                venta.Cantidad = updatingVenta.Cantidad
                venta.Comentarios = updatingVenta.Comentarios

                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "Cantidad Errones"
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()

            End If
            'updatingVenta.Lista = venta.Lista
            'updatingVenta.Observaciones = venta.Observaciones
            'updatingVenta.PU = venta.PU
            'updatingVenta.IVA = venta.IVA

            Session("Ventas") = ventas

        End SyncLock
    End Sub

    Private Sub BindData(ByVal sCliente As String, ByVal sFecha As String, ByVal sFolio As String, ByVal sPrefix As String, Optional ByRef bboton As Boolean = False)
        Cliente = sCliente
        Fecha = sFecha
        Folio = sFolio
        Prefijo = sPrefix

        boton = bboton

        Me.StoreDevoluciones.DataSource = Me.CurrentData
        Me.StoreDevoluciones.DataBind()
    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)
        Dim Ventas As ChangeRecords(Of Venta)
        Ventas = e.DataHandler.BatchObjectData(Of Venta)()

        If Session("bCreated") = False Then

            For Each created As Venta In Ventas.Created

                Session("bCreated") = True

                Dim tempId As Integer = created.Id
                Dim newId As Integer = Me.AddVenta(created)

                'If StoreDevoluciones.UseIdConfirmation Then
                '    e.ConfirmationList.ConfirmRecord(tempId.ToString(), newId.ToString())
                'Else
                '    StoreDevoluciones.UpdateRecordId(tempId, newId)
                'End If
            Next

        Else

            Session("bCreated") = False

        End If

        For Each deleted As Venta In Ventas.Deleted
            Me.DeleteVenta(deleted.Id)

            'If StoreDevoluciones.UseIdConfirmation Then
            '    e.ConfirmationList.ConfirmRecord(deleted.Id.ToString())
            'End If
        Next

        For Each updated As Venta In Ventas.Updated

            Me.UpdateVenta(updated, Session("Ventas"))

            'If StoreDevoluciones.UseIdConfirmation Then
            '    e.ConfirmationList.ConfirmRecord(updated.Id.ToString())
            'End If

        Next
        e.Cancel = True
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim useConfirmation__1 As Boolean

        If Session("STORETYPEID") = 3 Then
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        oDB.ConectaDBConnString(connstringWEB)

        If Boolean.TryParse(UseConfirmation.Text, useConfirmation__1) Then
            Me.StoreDevoluciones.SuspendScripting()
            'Me.StoreDevoluciones.UseIdConfirmation = useConfirmation__1
            Me.StoreDevoluciones.ResumeScripting()
        End If

        'End If

        'Me.BindData("5", "Agosto-2010", "2")

    End Sub

    Protected Sub DoReturns(ByVal sender As Object, ByVal e As DirectEventArgs)


        Dim squery As String
        Dim result As New StringBuilder()


        result.Append("<b>Selected Rows</b></br /><ul>")
        Dim sm As RowSelectionModel = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
        btnDevolucion.Enabled = False

        If sm.SelectedRows.Count = 0 Then
            btnDevolucion.Enabled = True
            Exit Sub
        End If

        squery = "Insert into devolucionesencabezado (IDCliente,IDVenta,IDUser,IDStore,Folio,Prefijo,SUFIJO,Fecha,TipoDevolucion,StatusCierre) values ("
        'squery = squery & "'" & cmbCliente.SelectedItem.Value & "'"
        squery = squery & "(select IDCliente from VentasEncabezado where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(sm.SelectedRows.Item(0).RowIndex).IDVenta & ")"
        squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(sm.SelectedRows.Item(0).RowIndex).IDVenta & "'"
        squery = squery & ",'" & Session("AdminUserID") & "'"
        squery = squery & ",'" & Session("IDSTORE") & "'"

        If cmbTipo.SelectedItem.Value = "Devolucion" Then


            squery = squery & ",'" & getCurrentFolio(Session("IDSTORE"), 4) & "'"
            squery = squery & ",'" & getPrefix(Session("IDSTORE"), 4) & "'"
            squery = squery & ",'" & getSufix(Session("IDSTORE"), 4) & "'"


        Else


            squery = squery & ",'" & getCurrentFolio(Session("IDSTORE"), 5) & "'"
            squery = squery & ",'" & getPrefix(Session("IDSTORE"), 5) & "'"
            squery = squery & ",'" & getSufix(Session("IDSTORE"), 5) & "'"


        End If

        squery = squery & ",getdate()"

        If cmbTipo.SelectedItem.Index = 0 Then
            squery = squery & ",'CA'"
        Else
            squery = squery & ",'D'"
        End If

        squery = squery & ",'O')" & vbNewLine

        oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

        If cmbTipo.SelectedItem.Value = "Devolucion" Then
            squery = "Update storeFolios set currentfolio=currentfolio+1 where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=" & 4 & ""
        Else
            squery = "Update storeFolios set currentfolio=currentfolio+1 where AdminStoreID=" & Session("IDSTORE") & " and AdminFolioType=" & 5 & ""
        End If

        oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

        squery = ""

        Dim idVenta = DirectCast(Session("Ventas"), List(Of Venta)).Item(sm.SelectedRows.Item(0).RowIndex).IDVenta

        For Each row As SelectedRow In sm.SelectedRows

            IDToFind = row.RecordID
            Dim ndx As Integer = DirectCast(Session("Ventas"), List(Of Venta)).FindIndex(AddressOf FindID)

            squery = squery & "Insert into devolucionesdetalle (IDDevolucion,IDLinea,IDArticulo,Lista,PrecioUnitario,IVA,Observaciones,Descuento,TotalLinea,Cantidad,StatusLinea,IDStore,Fecha) values("
            squery = squery & "'" & getLastDevolucion(Session("AdminUserID")) & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDLinea & "'"
            squery = squery & ",'" & getItemID(DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Articulo) & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Lista & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).PU & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IVA & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Comentarios & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Descuento & "'"

            'Dim monto As Double
            'monto = CDbl(FormatCurrency(DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).PU * 1.16) * DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Cantidad)
            'squery = squery & ",'" & monto & "'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Monto & "'"

            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Cantidad & "'"
            squery = squery & ",'O'"
            squery = squery & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDStore & "'"
            squery = squery & ",getdate()"
            squery = squery & ")" & vbNewLine

            If cmbTipo.SelectedItem.Index = 0 Then
                squery = squery & "update ventasdetalle set statuslinea='CA' where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Id & vbNewLine
                squery = squery & "update VentasEncabezado set StatusVenta='CA' where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDVenta & vbNewLine
                squery = squery & "update VentasPagos set StatusPago='CA' where idVenta=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDVenta & vbNewLine
            Else
                squery = squery & "update ventasdetalle set statuslinea='D' where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Id & vbNewLine
                squery = squery & "update VentasEncabezado set StatusVenta='D' where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDVenta & vbNewLine
                squery = squery & "update VentasPagos set StatusPago='D' where idVenta=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDVenta & vbNewLine
            End If

            Dim squery2 As String = ""

            squery2 = "select facturado from ventasencabezado where id=" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDVenta

            Dim dt As New DataTable

            dt = oDB.EjecutaQry_Tabla(squery2, CommandType.Text, "FACT", connstringWEB)

            If dt.Rows.Item(0).Item(0) = True Or dt.Rows.Item(0).Item(0) = 1 Then

                If inventoryExist(getItemID(DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Articulo), DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDStore) = True Then
                    squery = squery & "update inventarios set Cantidad=cantidad + " & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Cantidad & " where IDArticulo=" & getItemID(DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Articulo) & " and Almacen='" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDStore & "'"
                Else
                    squery = squery & "insert into inventarios (IDArticulo,Cantidad,Almacen) values (" & getItemID(DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Articulo) & "," & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).Cantidad & ",'" & DirectCast(Session("Ventas"), List(Of Venta)).Item(ndx).IDStore & "')"
                End If

            End If

        Next

        oDB.EjecutaQry(squery, CommandType.Text, connstringWEB)

        If cmbTipo.SelectedItem.Value = "Devolucion" Then

            Session("iddevolucion") = getLastDevolucion(Session("AdminUserID"))

            GeneraArchivoNotaCredito(Session("iddevolucion"), ConfigurationManager.AppSettings("DirectorioFacturasTXT"), Session("SAPDB"), ConfigurationManager.AppSettings("FTP"), ConfigurationManager.AppSettings("FTPUSR"), ConfigurationManager.AppSettings("FTPPASS"))

            Dim sError As String

            squery = "select 'NCREDITO_' + Prefijo + RIGHT('0000' + convert(varchar,Folio),5) + '_' + Prefijo from VentasEncabezado where ID=(select idventa from DevolucionesEncabezado where ID=" & Session("iddevolucion") & ")"

            Dim dt As DataTable

            dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "TMP", connstringWEB)

            squery = "update DevolucionesEncabezado set FechaCFDI=getdate(),Facturado=1,Archivo_FE='" & ConfigurationManager.AppSettings("DirectorioFacturasTXT") & dt.Rows.Item(0).Item(0) & "' where id=" & Session("iddevolucion")
            oDB.EjecutaQry(squery, CommandType.Text, connstringWEB, sError)


            InsertaLog(Tipos.DE, Session("iddevolucion"), Session("IDSTORE"), Session("WHSID"))
            Clean()
            Response.Redirect("Cancelaciones.aspx")

        Else

            Session("iddevolucion") = getLastDevolucion(Session("AdminUserID"))
            InsertaLog(Tipos.DE, Session("iddevolucion"), Session("IDSTORE"), Session("WHSID"))
            Clean()

            Session("IDPRINTVENTA") = idVenta
            Session("POSTBACK") = "CancelaVenta"
            Response.Redirect("../printControl/imprimeventa.aspx")

            'Response.Redirect("Cancelaciones.aspx")
            'End If
        End If


    End Sub

    Private Function FindID(ByVal bk As Venta) As Boolean
        If bk.Id = IDToFind Then
            Return True
        Else
            Return False
        End If
    End Function


    Public Function omiteNC(ByVal id As String) As Boolean


        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select AdminFolioType from StoreFolios where AdminStoreID=" & Session("IDSTORE") & " and Prefijo=(select prefijo from VentasEncabezado where ID=" & id & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("AdminFolioType") = 4 Then
                Return False
            End If

        Next

        Return True

        dt = Nothing
        GC.Collect()


    End Function

    Public Function getItemID(ByVal Item As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select IDArticulo from articulos where ArticuloSBO = '" & Item & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "IDArticulo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("IDArticulo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getLastDevolucion(ByVal user As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select max(id) as LastSale from devolucionesencabezado where iduser=" & user

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "LastSale", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If IsDBNull(Drow("LastSale")) = False Then
                Return Drow("LastSale")
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getFE(ByVal folio As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as Devolucion from ventasencabezado where folio='" & folio.Split("-")(1) & "' and prefijo='" & folio.Split("-")(0) & "' and facturado=1"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Devolucion", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("Devolucion") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getCurrentFolio(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select (currentfolio + 1) as [NuevoFolio] from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "UltimoFolio", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NuevoFolio")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function getPrefix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select prefijo from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Prefijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("prefijo")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function


    Public Function getSufix(ByVal AdminStoreID As String, ByVal AdminFolioType As String) As String

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "(select NoAprobacion from storeFolios where AdminStoreID=" & AdminStoreID & " and AdminFolioType=" & AdminFolioType & ")"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Sufijo", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            Return Drow("NoAprobacion")

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    Public Function inventoryExist(ByVal IDArticulo As String, ByVal WHSID As String) As Boolean

        Dim sQuery As String
        Dim dt As New DataTable


        sQuery = "select count(*) as exist from Inventarios where IDArticulo=" & IDArticulo & " and Almacen='" & WHSID & "'"

        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Exist", connstringWEB)

        For Each Drow As DataRow In dt.Rows

            If Drow("exist") = 0 Then
                Return False
            Else
                Return True
            End If

        Next

        Return 0

        dt = Nothing
        GC.Collect()

    End Function

    <DirectMethod()> _
    Public Sub GetPeriods(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) as Periodo "
        squery = squery & " from ventasEncabezado VE"
        squery = squery & " where VE.IDCliente =" & Value

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Periodos", connstringWEB)

        'cmbFolio.Visible = False

        lblPeriodo.Visible = True
        lblPeriodo.Render()

        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbDia.Text = ""
        cmbDia.Items.Clear()
        cmbDia.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        'cmbTipo.Disabled = True
        'cmbTipo.SelectedIndex = 1

        For Each row As DataRow In dt.Rows
            cmbFecha.Items.Add(New Ext.Net.ListItem(row("Periodo").ToString(), row("Periodo")))
        Next

        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()
        cmbCliente.Disabled = True

    End Sub

    <DirectMethod()> _
    Public Sub GetDays(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        'squery = "select distinct CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) as Periodo "
        'squery = squery & " from ventasEncabezado VE"
        'squery = squery & " where VE.IDCliente =" & Value

        squery = "Select distinct"
        squery = squery & " RIGHT('00' + convert(varchar,CAST(DATENAME(day, VE.Fecha) AS varchar)),2) as dia"
        squery = squery & " from ventasencabezado VE, Clientes CL"
        squery = squery & " where VE.IDCliente = CL.ID"
        squery = squery & " and idStore=" & Session("IDSTORE")
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & Value & "'"
        squery = squery & " order by dia"

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Dias", connstringWEB)

        'cmbFolio.Visible = False

        cmbFecha.Text = ""
        cmbFecha.Items.Clear()
        cmbFecha.Visible = True

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        'cmbTipo.Disabled = True
        'cmbTipo.SelectedIndex = 1

        For Each row As DataRow In dt.Rows
            cmbDia.Items.Add(New Ext.Net.ListItem(row("dia").ToString(), row("dia")))
        Next

        lblPeriodo.Visible = True
        lblPeriodo.Render()
        cmbFecha.Text = Value
        cmbFecha.Disabled = True
        cmbFecha.Enabled = False
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Render()
        btnClean.Render()

    End Sub

    <DirectMethod()> _
    Public Sub GetFolios(ByVal Value As String)

        Dim dt As New DataTable
        Dim squery As String

        squery = "select distinct VE.folio, VE.Prefijo  + '-' + VE.Folio  as 'FolioPref'" & vbNewLine
        squery = squery & " from ventasencabezado VE, Clientes CL" & vbNewLine
        squery = squery & " where VE.IDCliente = CL.ID" & vbNewLine
        squery = squery & " and idStore=" & Session("IDSTORE") & vbNewLine
        squery = squery & " and VE.IDCliente = " & cmbCliente.SelectedItem.Value & vbNewLine
        squery = squery & " and CAST(DATENAME(month, VE.Fecha) AS varchar) + '-' + CAST(CAST(DATEPART(year, VE.Fecha) AS varchar) AS varchar) = '" & cmbFecha.SelectedItem.Value & "'" & vbNewLine
        squery = squery & " and CAST(DATENAME(DAY, VE.Fecha) AS varchar) = " & Value & vbNewLine
        squery = squery & " and (select count(*) from ventasDetalle VDE,ventasEncabezado VEN where VEN.ID=VDE.IDVenta and statusLinea='O' and VEN.Folio=VE.Folio) <> 0"

        dt = oDB.EjecutaQry_Tabla(squery, CommandType.Text, "Folios", connstringWEB)

        lblFolio.Visible = True
        lblFolio.Render()

        cmbFolio.Text = ""
        cmbFolio.Items.Clear()
        cmbFolio.Visible = True

        For Each row As DataRow In dt.Rows
            cmbFolio.Items.Add(New Ext.Net.ListItem(row("FolioPref").ToString(), row("FolioPref")))
        Next

        Try

            cmbFolio.Render()
            btnClean.Render()

        Catch ex As Exception

tag:

        End Try

    End Sub

    <DirectMethod()> _
    Public Sub Clean()

        cmbCliente.Text = ""
        cmbCliente.Render()
        lblPeriodo.Render()
        cmbFecha.Text = ""
        cmbFecha.Render()
        lblDia.Render()
        cmbDia.Text = ""
        cmbDia.Render()
        lblFolio.Render()
        cmbFolio.Text = ""
        cmbFolio.Render()
        btnClean.Render()
        DoBind("")

    End Sub

    <DirectMethod()> _
    Public Sub DoBind(ByVal Value As String, Optional ByVal botton As Boolean = False)

        Session("Ventas") = Nothing

        Dim sm As RowSelectionModel = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)

        If botton = True Then
            Me.BindData("", "", Pedido.Text, "", True)
            sm = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
            sm.SelectAll()
            chkDevolber.AllowDeselect = False
            chkDevolber.ShowHeaderCheckbox = False
            Exit Sub
        End If

        If Value = "" Then
            Me.BindData("", "", "", "")
            sm = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
            sm.SelectAll()
            chkDevolber.AllowDeselect = False
            chkDevolber.ShowHeaderCheckbox = False
            Exit Sub
        End If

        sm = TryCast(Me.gpDevoluciones.SelectionModel.Primary, RowSelectionModel)
        sm.ClearSelection()
        sm.UpdateSelection()

        If getFE(Value) = True Then
            'cmbTipo.Disabled = False
            'Nuevo
            sm.SelectAll()
            sm.UpdateSelection()
            cmbTipo.SelectedItem.Index = 0
            cmbTipo.Disabled = False
            'cmbTipo.Disabled = True
        Else
            'cmbTipo.Disabled = True
            'cmbTipo.SelectedIndex = 1

            sm.SelectAll()
            sm.UpdateSelection()
            cmbTipo.SelectedItem.Index = 0
            cmbTipo.Disabled = True

        End If

        If Boolean.TryParse(UseConfirmation.Text, False) Then
            Me.StoreDevoluciones.SuspendScripting()
            'Me.StoreDevoluciones.UseIdConfirmation = False
            Me.StoreDevoluciones.ResumeScripting()
        End If

        Me.BindData(cmbCliente.SelectedItem.Value, cmbFecha.SelectedItem.Value, Value.Split("-")(1), Value.Split("-")(0))

        If getFE(Value) = True Then
            sm.SelectAll()
            chkDevolber.AllowDeselect = False
            chkDevolber.ShowHeaderCheckbox = False
        Else

            sm.SelectAll()
            chkDevolber.AllowDeselect = False
            chkDevolber.ShowHeaderCheckbox = False
        End If


    End Sub


    Private Sub Buscar_DirectClick(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs) Handles Buscar.DirectClick
        DoBind("", True)
    End Sub

End Class
