﻿Imports Ext.Net

Public Class Facturacion2
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private sDefaultList As String
    Private sDefaultName As String
    Private mensaje As String = ""
    Private winMsg As Ext.Net.Window
    Private oWSData As WSData

    <DirectMethod()> _
    Public Sub AgregarLineaVenta()
        If hdnCodigoArticulo.Value = String.Empty Then
            Ext.Net.X.MessageBox.Alert("Mensaje Sistema", "No se puede agregar la linea si no ha seleccionado el numero de articulo ")
            Return
        End If
        Dim lineaVenta As LineaVenta = New LineaVenta()
        lineaVenta.Id = Guid.NewGuid().ToString()
        lineaVenta.CodigoArticulo = hdnCodigoArticulo.Value
        lineaVenta.FactorArticulo = hdnFactorArticulo.Value
        lineaVenta.NombreArticulo = hdnNombreArticulo.Value
        lineaVenta.SujetoAImpuesto = True
        lineaVenta.Cantidad = txtCantidad.Value
        lineaVenta.Precio = txtPrecio.Value
        lineaVenta.PrecioConImpuesto = txtPrecioImpuesto.Value
        lineaVenta.Descuento = txtDescuento.Value
        lineaVenta.TotalLinea = hdnTotalLinea.Value
        lineaVenta.TotalLineaImpuestos = txtTotalLineaImpuesto.Value
        lineaVenta.PrecioMinimoAutorizacion = hdnPrecioMinimoAutorizacion.Value
        lineaVenta.PrecioMaximoAutorizacion = hdnPrecioMaximoAutorizacion.Value
        lineaVenta.PrecioCosto = hdnPrecioCosto.Value
        lineaVenta.Comentarios = txtComentariosLinea.Value
        lineaVenta.PuedeModificarPrecio = hdnNumeroFactores.Value > 0

        lineaVenta.UnidadDeMedida = New UnidadMedida()
        lineaVenta.UnidadDeMedida.UnidadMedida = cmbUnidadMedida.Text
        lineaVenta.UnidadDeMedida.Factor = cmbUnidadMedida.Value

        Dim controlArticulos As ControlArticulos = New ControlArticulos()
        lineaVenta.UnidadesDeMedida = controlArticulos.UnidadesMedidaArticulo("Venta", lineaVenta.CodigoArticulo)
        lineaVenta.PuedeElegirFactor = hdnNumeroFactores.Value > 0
        If lineaVenta.PuedeElegirFactor Then
            lineaVenta.Factores = New List(Of Factor)()
            Dim defFactores As String() = hdnDefinicionFactores.Value.ToString().Split("|")
            For i As Integer = 0 To defFactores.Count - 1 Step 2
                Dim auxFactor As Factor = New Factor()
                auxFactor.Precio = defFactores(i)
                auxFactor.Nombre = defFactores(i + 1)
                lineaVenta.Factores.Add(auxFactor)
            Next
        End If
        AgregarRegistroDetalleDocumento(lineaVenta)
        inicializarInterfazCapturaLinea()

        recalcularTotalDocumento(lineaVenta.TotalLinea, lineaVenta.TotalLineaImpuestos)
    End Sub

    Private Sub inicializarInterfazCapturaLinea()

        InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)
        cmbUnidadMedida.Value = String.Empty
        cmbUnidadMedida.Enabled = False

        cmbBusquedaArticulo.Value = String.Empty
        hdnCodigoArticulo.Value = String.Empty
        hdnFactorArticulo.Value = String.Empty
        hdnNombreArticulo.Value = String.Empty
        hdnSujImp.Value = False

        txtCantidad.SetRawValue(String.Empty)
        txtCantidad.ReadOnly = True
        txtCantidad.Enabled = False

        txtFactor.Value = String.Empty
        txtFactor.Enabled = False
        hdnFactor.Value = String.Empty
        txtPrecio.SetRawValue(String.Empty)
        txtPrecio.Enabled = False
        txtPrecioImpuesto.SetRawValue(String.Empty)
        txtPrecioImpuesto.ReadOnly = True
        txtDescuento.SetRawValue(String.Empty)
        txtDescuento.ReadOnly = True
        txtDescuento.Enabled = False
        hdnTotalLinea.Value = 0
        txtTotalLineaImpuesto.SetRawValue(String.Empty)
        hdnPrecioMinimoAutorizacion.Value = 0
        hdnPrecioMaximoAutorizacion.Value = 0
        hdnPrecioCosto.Value = 0
        txtComentariosLinea.Value = String.Empty
        BtnAgregarLinea.Enabled = False

        hdnDefinicionFactores.Value = String.Empty
        hdnNumeroFactores.Value = 0

        cmbBusquedaArticulo.Focus()

    End Sub

    <DirectMethod()> _
    Public Sub MostrarVentanaBusquedaArticulos()
        StoreArticulos.Data = Nothing
        StoreArticulos.DataBind()

        txtCodigoArticuloBusqueda.Value = String.Empty
        txtNombreArticuloBusqueda.Value = String.Empty

        hdnUltimoCodigoBuscado.Value = "|[NONE]|"
        hdnUltimoNombreBuscado.Value = Nothing

        winArticulos.Show()
    End Sub

    <DirectMethod()> _
    Public Sub CargarArticuloCodigoBarras()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then

            txtTotalDocumento.Value = 0
            txtDescuentoGlobal.Value = 0
            txtIvaDocumento.Value = 0
            txtSubtotalDescuento.Value = 0

            definicionInterfazFechaCreacionDocumento()
            cargarInformacionEnCombos()
            cargarParametrosVenta()
            cmbVendedor.Focus()
            hdnFactoresTienda.Value = "1,1,1,1,1"

        End If
    End Sub

    Protected Sub definicionInterfazFechaCreacionDocumento()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        txtFecha.Value = controlTienda.FechaAperturaCaja(contextUser.UsuarioPuntoVenta.StoreId)
        txtFecha.Enabled = controlUsuario.PuedeModificarFechaDocumento(contextUser.UsuarioPuntoVenta.UserId)
        txtFecha.ReadOnly = Not txtFecha.Enabled
    End Sub

    Protected Sub cargarParametrosVenta()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim controlUsuario As ControlUsuario = New ControlUsuario()

        Dim listaPrecioCosto As Integer = -1
        Dim listaPrecioVenta As Integer = -1
        Dim porcentajeImpuesto As Decimal
        Dim codigoClienteMostrador As Integer = -1

        controlTienda.ConfiguracionVentaTienda(contextUser.UsuarioPuntoVenta.StoreId, listaPrecioVenta, listaPrecioCosto, porcentajeImpuesto, codigoClienteMostrador)

        hdnListaPrecioCosto.Value = listaPrecioCosto
        hdnListaPrecioVenta.Value = listaPrecioVenta
        hdnPorcentajeImpuesto.Value = porcentajeImpuesto
        hdnFactoresUsuario.Value = controlUsuario.FactoresUsuario(contextUser.UsuarioPuntoVenta.UserId)

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(codigoClienteMostrador)
        cargarInformacionClienteEnFormularioVentas(cliente)


    End Sub

    Protected Sub cargarInformacionEnCombos()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim codigosEstado As Dictionary(Of String, String) = New ControlCatalogos().CodigosEstado()
        For Each estado In codigosEstado
            cmbEstadoCliente.Items.Add(New Ext.Net.ListItem(estado.Value, estado.Key))
        Next

        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim tablaVendedores As System.Data.DataTable = controlTienda.ObtenerVendedoresPuntoDeVenta(contextUser.UsuarioPuntoVenta.StoreId)
        StoreVendedores.Data = tablaVendedores
        StoreVendedores.DataBind()

        cmbComprobante.Value = "T"
        cmbVendedor.Select(0)

    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)

    End Sub


    Protected Sub HandleChangesPagos(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)

    End Sub

#Region "Metodos Control Ventana Clientes"


    <DirectMethod()>
    Public Sub MostrarVentanaClientes()
        cmbTipoBuquedaCliente.Value = "N"
        txtValorBusquedaCliente.Value = String.Empty

        UIFormularioClientesBusqueda()
        StoreClientes.Data = Nothing
        StoreClientes.DataBind()

        winClientes.Show()
    End Sub

    <DirectMethod>
    Public Sub UIFormularioClientesCrear()
        txtNombreCliente.Focus()
        btnGuardarCliente.Hide()
        btnSeleccionarCliente.Hide()
        btnCrearCliente.Show()

        hdnCodigoCliente.Value = String.Empty
        txtNombreCliente.Disabled = False
        txtNombreCliente.Value = String.Empty
        txtApPaternoCliente.Disabled = False
        txtApPaternoCliente.Value = String.Empty
        txtApMaternoCliente.Disabled = False
        txtApMaternoCliente.Value = String.Empty
        txtNombreComercialCliente.Disabled = False
        txtNombreComercialCliente.Value = String.Empty
        txtRFCCliente.Disabled = False
        txtRFCCliente.Value = String.Empty
        txtCalleCliente.Disabled = False
        txtCalleCliente.Value = String.Empty
        txtNumExtCliente.Disabled = False
        txtNumExtCliente.Value = String.Empty
        txtNumIntCliente.Disabled = False
        txtNumIntCliente.Value = String.Empty
        txtColoniaCliente.Disabled = False
        txtColoniaCliente.Value = String.Empty
        txtCPCliente.Disabled = False
        txtCPCliente.Value = String.Empty
        txtMunicipioCliente.Disabled = False
        txtMunicipioCliente.Value = String.Empty
        cmbEstadoCliente.Disabled = False
        cmbEstadoCliente.SelectedItem.Index = -1
        txtTelCasaCliente.Disabled = False
        txtTelCasaCliente.Value = String.Empty
        txtTelOficinaCliente.Disabled = False
        txtTelOficinaCliente.Value = String.Empty
        txtCorreoCliente.Disabled = False
        txtCorreoCliente.Value = String.Empty
        txtFechaNacimientoCliente.Disabled = False
        txtFechaNacimientoCliente.Value = DateTime.MinValue

    End Sub
    <DirectMethod>
    Public Sub CargarInformacionClienteSeleccionado(ByVal codigoCliente As String)
        Dim idCodigoCliente As Integer = codigoCliente.Replace("""", String.Empty).Replace("[", String.Empty).Replace("]", String.Empty)

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(idCodigoCliente)
        mapearDeClienteAFormulario(cliente)
        UIFormularioClientesLectura()
    End Sub

    <DirectMethod>
    Public Sub SeleccionarClienteDobleClic(ByVal codigoCliente As String)

        Dim idCodigoCliente As Integer = codigoCliente.Replace("""", String.Empty).Replace("[", String.Empty).Replace("]", String.Empty)

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(idCodigoCliente)
        cargarInformacionClienteEnFormularioVentas(cliente)
        winClientes.Hide()
    End Sub

    <DirectMethod>
    Public Sub CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(ByVal parametrosRegistroSeleccionado As String)
        Dim valoresArticuloSeleccionado As List(Of String) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of String))(parametrosRegistroSeleccionado)

        Dim codigoArticulo As String = valoresArticuloSeleccionado(0)
        Dim nombreArticulo As String = valoresArticuloSeleccionado(1)
        Dim porcentajeImpuesto As Decimal = valoresArticuloSeleccionado(2)
        cmbBusquedaArticulo.Text = nombreArticulo
        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo

        winArticulos.Close()

        prepararInterfazCapturaLinea(codigoArticulo, porcentajeImpuesto)
        RecalcularTotalesSeleccionLinea()
    End Sub

    Public Sub CargarInformacionArticuloSeleccionadoDeBuscador()
        Dim nombreArticulo As String = cmbBusquedaArticulo.SelectedItem.Text
        Dim codigoArticulo As String = cmbBusquedaArticulo.SelectedItem.Value
        Dim porcentajeImpuesto As Decimal = hdnPorcentajeImpuesto.Value

        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo

        prepararInterfazCapturaLinea(codigoArticulo, porcentajeImpuesto)
        RecalcularTotalesSeleccionLinea()
    End Sub

    <DirectMethod>
    Public Shared Function ActualizarMontoPago(ByVal totalDocumento As Decimal, ByVal montoPagoTotalActual As Decimal, ByVal montoPago As Decimal) As ValoresRetornoPago
        Dim montoPagoTotalNuevo As Decimal = montoPagoTotalActual + montoPago
        Dim cambio As Decimal = montoPagoTotalNuevo - totalDocumento
        If cambio < 0 Then
            cambio = 0
        End If
        Dim retornoPago As ValoresRetornoPago = New ValoresRetornoPago()
        retornoPago.TotalPago = montoPagoTotalNuevo
        retornoPago.Cambio = cambio

        Return retornoPago
    End Function

    <DirectMethod>
    Public Shared Function RecalcularTotalesSeleccionLineaGridCambioPrecio(ByVal precioImpuesto As Decimal, _
        ByVal cantidad As Decimal,
        ByVal sujetoAImpuesto As Boolean,
        ByVal descuento As Decimal,
        ByVal porcentajeImpuesto As Decimal,
        ByVal importeLineaImpuestoActual As Decimal,
        ByVal importeLineaActual As Decimal,
        ByVal subTotal As Decimal,
        ByVal descuentoGlobal As Decimal,
        ByVal subTotal2 As Decimal,
        ByVal impuestos As Decimal,
        ByVal total As Decimal) As ValoresRetornoCalculoTotalLinea

        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim controlArticulo As ControlArticulos = New ControlArticulos()

        Dim precioSinImpuesto = controlArticulo.CalcularPrecioSinImpuesto(precioImpuesto, sujetoAImpuesto, porcentajeImpuesto)

        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLineaPrecioConYSinImpuesto(cantidad, precioSinImpuesto, precioImpuesto, descuento, importeLinea, importeLineaImpuesto)

        Dim importeLineaAgregar As Decimal = importeLinea - importeLineaActual
        Dim importeLineaImpuestosAgregar As Decimal = importeLineaImpuesto - importeLineaImpuestoActual

        controlDocumento.CalcularMontosDocumentoVenta(importeLineaAgregar, importeLineaImpuestosAgregar, descuentoGlobal, subTotal, subTotal2, impuestos, total)

        Dim valoresRetorno As ValoresRetornoCalculoTotalLinea = New ValoresRetornoCalculoTotalLinea()
        valoresRetorno.ImporteLinea = importeLinea
        valoresRetorno.ImporteLineaImpuesto = importeLineaImpuesto
        valoresRetorno.Impuestos = impuestos
        valoresRetorno.PrecioSinImpuesto = precioSinImpuesto
        valoresRetorno.SubTotal = subTotal
        valoresRetorno.SubTotal2 = subTotal2
        valoresRetorno.Total = total

        Return valoresRetorno

    End Function

    <DirectMethod>
    Public Shared Function RecalcularTotalesSeleccionLineaGrid(
        ByVal cantidad As Decimal, _
        ByVal precio As Decimal, _
        ByVal precioImpuesto As Decimal, _
        ByVal descuentoLinea As Decimal,
        ByVal importeLineaImpuestoActual As Decimal,
        ByVal importeLineaActual As Decimal,
        ByVal subTotal As Decimal, _
        ByVal descuentoGlobal As Decimal, _
        ByVal subTotal2 As Decimal, _
        ByVal impuestos As Decimal, _
        ByVal total As Decimal) As ValoresRetornoCalculoTotalLinea

        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLineaPrecioConYSinImpuesto(cantidad, precio, precioImpuesto, descuentoLinea, importeLinea, importeLineaImpuesto)

        Dim importeLineaAgregar As Decimal = importeLinea - importeLineaActual
        Dim importeLineaImpuestosAgregar As Decimal = importeLineaImpuesto - importeLineaImpuestoActual

        controlDocumento.CalcularMontosDocumentoVenta(importeLineaAgregar, importeLineaImpuestosAgregar, descuentoGlobal, subTotal, subTotal2, impuestos, total)

        Dim valoresRetorno As ValoresRetornoCalculoTotalLinea = New ValoresRetornoCalculoTotalLinea()
        valoresRetorno.ImporteLinea = importeLinea
        valoresRetorno.ImporteLineaImpuesto = importeLineaImpuesto
        valoresRetorno.Impuestos = impuestos
        valoresRetorno.PrecioSinImpuesto = precio
        valoresRetorno.SubTotal = subTotal
        valoresRetorno.SubTotal2 = subTotal2
        valoresRetorno.Total = total

        Return valoresRetorno
    End Function


    Private Sub recalcularTotalDocumento(ByVal importeLineaAgregar As Decimal, ByVal importeLineaImpuestoAgregar As Decimal)
        Dim descuentoGlobal As Decimal = txtDescuentoGlobal.Value
        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim subTotal As Decimal = txtSubtotalDocumento.Value
        Dim subTotal2 As Decimal = txtSubtotalDescuento.Value
        Dim impuestos As Decimal = txtIvaDocumento.Value
        Dim total As Decimal = txtTotalDocumento.Value

        controlDocumento.CalcularMontosDocumentoVenta(importeLineaAgregar, importeLineaImpuestoAgregar, descuentoGlobal, subTotal, subTotal2, impuestos, total)

        txtSubtotalDocumento.SetValue(subTotal)
        txtSubtotalDescuento.SetValue(subTotal2)
        txtIvaDocumento.SetValue(impuestos)
        txtTotalDocumento.SetValue(total)
    End Sub



    Public Sub RecalcularTotalesSeleccionLineaCambioPrecio()
        Dim controlDocumento As ControlDocumento = New ControlDocumento()

        Dim cantidad As Decimal = txtCantidad.Value
        Dim precioImpuesto As Decimal = txtPrecioImpuesto.Value
        Dim descuento As Decimal = txtDescuento.Value

        Dim sujetoAImpuesto As Boolean = hdnSujImp.Value
        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim porcentajeImpuesto As Decimal = hdnPorcentajeImpuesto.Value
        txtPrecio.Value = controlArticulo.CalcularPrecioSinImpuesto(txtPrecioImpuesto.Value, sujetoAImpuesto, porcentajeImpuesto)
        Dim precio As Decimal = txtPrecio.Value

        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLineaPrecioConYSinImpuesto(cantidad, precio, precioImpuesto, descuento, importeLinea, importeLineaImpuesto)

        txtTotalLineaImpuesto.Value = importeLineaImpuesto
        hdnTotalLinea.Value = importeLinea


    End Sub

    Public Sub RecalcularTotalesSeleccionLinea()
        Dim controlDocumento As ControlDocumento = New ControlDocumento()

        Dim cantidad As Decimal = txtCantidad.Value
        Dim precioImpuesto As Decimal = txtPrecioImpuesto.Value
        Dim descuento As Decimal = txtDescuento.Value
        Dim precio As Decimal = txtPrecio.Value

        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLineaPrecioConYSinImpuesto(cantidad, precio, precioImpuesto, descuento, importeLinea, importeLineaImpuesto)

        txtTotalLineaImpuesto.Value = importeLineaImpuesto
        hdnTotalLinea.Value = importeLinea

    End Sub

    'nwaps precio
    'Public Sub ActualizarPrecioPorCambioFactor()
    '    txtPrecioImpuesto.Value = cmbFactor.SelectedItem.Value
    '    RecalcularTotalesSeleccionLineaCambioPrecio()
    'End Sub

    Private Sub prepararInterfazCapturaLinea(ByVal codigoArticulo As String, ByVal porcentajeImpuesto As Decimal)

        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controlArticulos As ControlArticulos = New ControlArticulos()
        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim factoresUsuario As List(Of Boolean) = definicionFactoresDeHiddenToList(hdnFactoresUsuario.Value)
        Dim factoresTienda As List(Of Boolean) = definicionFactoresDeHiddenToList(hdnFactoresTienda.Value)

        Dim esArticuloFix As Boolean
        Dim factorArticulo As String = String.Empty
        Dim precioLista As Decimal
        Dim unidadesMedida As List(Of UnidadMedida) = Nothing
        Dim factoresPrecioArticulo As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal
        Dim precioMaximoAutorizacion As Decimal
        Dim precioCosto As Decimal
        Dim puedeElegirFactor As Boolean
        Dim puedeModificarPrecio As Boolean
        Dim sujetoAImpuesto As Boolean

        Dim listaPrecioCosto As Integer = hdnListaPrecioCosto.Value
        Dim listaPrecioVenta As Integer = hdnListaPrecioVenta.Value
        Dim porcentajeIVA As Decimal = hdnPorcentajeImpuesto.Value
        Dim numeroFactorElegir As Integer = -1

        If Not controlArticulos.InformacionVentaArticulo(codigoArticulo, contextUser.UsuarioPuntoVenta.StoreId, factoresUsuario, factoresTienda, listaPrecioCosto, listaPrecioVenta, porcentajeIVA, _
                                                   esArticuloFix, factorArticulo, unidadesMedida, factoresPrecioArticulo, _
                                                  precioLista, precioMinimoAutorizacion, precioMaximoAutorizacion, precioCosto, puedeElegirFactor, puedeModificarPrecio, sujetoAImpuesto, numeroFactorElegir) Then
            cmbUnidadMedida.ReadOnly = True
            InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)
            hdnItemsComboUnidadMedida.Value = 0
            txtCantidad.ReadOnly = True
            txtCantidad.Value = 1
            hdnFactorArticulo.Value = String.Empty
            hdnNumeroFactores.Value = -1
            hdnDefinicionFactores.Value = String.Empty

            txtPrecio.ReadOnly = True
            txtPrecio.Value = 0
            txtComentariosLinea.ReadOnly = True
            txtComentariosLinea.Text = String.Empty
            BtnAgregarLinea.Disabled = True
            MessageBoxExtNet.ShowError("No se pudo obtener la informacion de venta para el articulo seleccionado. Consulte con su administrador")
            Return
        End If

        hdnSujImp.Value = sujetoAImpuesto
        hdnFactorArticulo.Value = factorArticulo
        InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)

        For i As Integer = 0 To unidadesMedida.Count - 1
            cmbUnidadMedida.InsertItem(i, unidadesMedida(i).UnidadMedida, unidadesMedida(i).Factor)
        Next
        cmbUnidadMedida.Select(0)
        hdnItemsComboUnidadMedida.Value = unidadesMedida.Count

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1
        txtComentariosLinea.ReadOnly = False
        txtComentariosLinea.Text = String.Empty
        BtnAgregarLinea.Disabled = False

        If esArticuloFix OrElse Not puedeElegirFactor Then
            hdnDefinicionFactores.Value = String.Empty
            hdnNumeroFactores.Value = -1

            txtPrecio.ReadOnly = True
            txtPrecio.Value = precioLista
            Return
        End If

        Dim defFactores As StringBuilder = New StringBuilder()
        For i As Integer = 0 To factoresPrecioArticulo.Count - 1
            defFactores.Append("|" & factoresPrecioArticulo(i).Precio & "|" & factoresPrecioArticulo(i).Nombre)
        Next
        defFactores.Remove(0, 1)
        hdnNumeroFactores.Value = factoresPrecioArticulo.Count
        hdnDefinicionFactores.Value = defFactores.ToString()

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1
        txtPrecioImpuesto.ReadOnly = False
        txtPrecioImpuesto.Value = factoresPrecioArticulo.Item(0).Precio
        txtPrecio.Value = controlArticulos.CalcularPrecioSinImpuesto(factoresPrecioArticulo.Item(0).Precio, sujetoAImpuesto, porcentajeImpuesto)
        txtComentariosLinea.Disabled = False
        txtComentariosLinea.Value = String.Empty

        hdnPrecioMaximoAutorizacion.Value = precioMaximoAutorizacion
        hdnPrecioMinimoAutorizacion.Value = precioMinimoAutorizacion
        hdnPrecioCosto.Value = precioCosto
    End Sub

    Public Sub InicializarValoresCombo(ByRef combo As Ext.Net.ComboBox, ByVal numeroRegistros As Integer)
        For i As Integer = 0 To numeroRegistros - 1
            combo.RemoveByIndex(0)
        Next
    End Sub

    <DirectMethod>
    Public Shared Function ControlarCambioUnidadDeMedidaGrid(
         ByVal codigoArticulo As String,
        ByVal codigoFactor As String,
        ByVal factoresUsuario As String,
        ByVal factoresTienda As String,
        ByVal factorUnidadMedida As Decimal,
        ByVal precioCosto As Decimal,
        ByVal cantidad As Decimal,
        ByVal descuento As Decimal,
        ByVal sujetoAImpuesto As Boolean,
        ByVal porcentajeImpuesto As Decimal,
        ByVal importeLineaImpuestoActual As Decimal,
        ByVal importeLineaActual As Decimal,
        ByVal descuentoGlobal As Decimal,
        ByVal subTotal As Decimal,
        ByVal subTotal2 As Decimal,
        ByVal impuestos As Decimal,
        ByVal total As Decimal) As ValoresRetornoCambioUnidadMedida

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim factoresPrecio As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal = 0
        Dim precioMaximoAutorizacion As Decimal = 0

        Dim numeroFactorElegir As Integer
        controlArticulo.ActualizarInformacionFactores(codigoArticulo, codigoFactor, definicionFactoresDeHiddenToListShared(factoresUsuario), definicionFactoresDeHiddenToListShared(factoresTienda), factorUnidadMedida, precioCosto, sujetoAImpuesto, porcentajeImpuesto, factoresPrecio, precioMinimoAutorizacion, precioMaximoAutorizacion, numeroFactorElegir)

        Dim valorUnidadMedida As ValoresRetornoCambioUnidadMedida = New ValoresRetornoCambioUnidadMedida()
        valorUnidadMedida.Factores = factoresPrecio
        valorUnidadMedida.PrecioMinimoAutorizacion = precioMinimoAutorizacion
        valorUnidadMedida.PrecioMaximoAutorizacion = precioMaximoAutorizacion
        valorUnidadMedida.PrecioPrimerFactor = valorUnidadMedida.Factores(numeroFactorElegir).Precio
        valorUnidadMedida.PrecioPrimerFactorSinImpuestos = controlArticulo.CalcularPrecioSinImpuesto(valorUnidadMedida.PrecioPrimerFactor, sujetoAImpuesto, porcentajeImpuesto)

        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLineaPrecioConYSinImpuesto(cantidad, valorUnidadMedida.PrecioPrimerFactorSinImpuestos, valorUnidadMedida.PrecioPrimerFactor, descuento, importeLinea, importeLineaImpuesto)

        valorUnidadMedida.ImporteLinea = importeLinea
        valorUnidadMedida.ImporteLineaImpuesto = importeLineaImpuesto

        Dim importeLineaAgregar As Decimal = importeLinea - importeLineaActual
        Dim importeLineaImpuestosAgregar As Decimal = importeLineaImpuesto - importeLineaImpuestoActual

        controlDocumento.CalcularMontosDocumentoVenta(importeLineaAgregar, importeLineaImpuestosAgregar, descuentoGlobal, subTotal, subTotal2, impuestos, total)

        valorUnidadMedida.Impuestos = impuestos
        valorUnidadMedida.SubTotal = subTotal
        valorUnidadMedida.SubTotal2 = subTotal2
        valorUnidadMedida.Total = total

        Return valorUnidadMedida

    End Function

    <DirectMethod>
    Public Sub ControlarCambioUnidadMedida()

        Dim factorUnidadMedida As Decimal = cmbUnidadMedida.Value

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim factoresPrecio As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal = 0
        Dim precioMaximoAutorizacion As Decimal = 0
        Dim numeroFactorElegir As Integer = -1

        Dim defFactores As StringBuilder = New StringBuilder()
        controlArticulo.ActualizarInformacionFactores(hdnCodigoArticulo.Value, hdnFactorArticulo.Value, definicionFactoresDeHiddenToList(hdnFactoresUsuario.Value), definicionFactoresDeHiddenToList(hdnFactoresTienda.Value), factorUnidadMedida, hdnPrecioCosto.Value, hdnSujImp.Value, hdnPorcentajeImpuesto.Value, factoresPrecio, precioMinimoAutorizacion, precioMaximoAutorizacion, numeroFactorElegir)
        For i As Integer = 0 To factoresPrecio.Count - 1
            defFactores.Append(";" & factoresPrecio(i).Precio & ";" & factoresPrecio(i).Descripcion)
        Next
        defFactores.Remove(0, 1)

        txtPrecioImpuesto.Value = factoresPrecio(numeroFactorElegir).Precio
        hdnNumeroFactores.Value = factoresPrecio.Count
        hdnDefinicionFactores.Value = defFactores.ToString()
        hdnPrecioMinimoAutorizacion.Value = precioMinimoAutorizacion
        hdnPrecioMaximoAutorizacion.Value = precioMaximoAutorizacion

        RecalcularTotalesSeleccionLineaCambioPrecio()
    End Sub

    Private Shared Function definicionFactoresDeHiddenToListShared(ByVal definicionFactor As String) As List(Of Boolean)
        Dim permisosFactoresNumero() As String = definicionFactor.Split(",")

        Dim definicionFactores As List(Of Boolean) = New List(Of Boolean)()
        For i As Integer = 0 To permisosFactoresNumero.Length - 1
            definicionFactores.Add(permisosFactoresNumero(i) = "1")
        Next
        Return definicionFactores
    End Function


    Private Function definicionFactoresDeHiddenToList(ByVal definicionFactor As String) As List(Of Boolean)
        Dim permisosFactoresNumero() As String = definicionFactor.Split(",")

        Dim definicionFactores As List(Of Boolean) = New List(Of Boolean)()
        For i As Integer = 0 To permisosFactoresNumero.Length - 1
            definicionFactores.Add(permisosFactoresNumero(i) = "1")
        Next
        Return definicionFactores
    End Function

    Private Sub AgregarRegistroDetalleDocumento(ByVal informacionLineaVenta As LineaVenta)
        Dim idRegistro As String = Guid.NewGuid().ToString()

        Dim registroContainer As Ext.Net.Container = New Container()
        registroContainer.Layout = "HBoxLayout"
        registroContainer.Width = PanelDetalleDocumento.Width
        registroContainer.ID = "contenedor" & idRegistro

        Dim cajaCodigoArticulo As Ext.Net.TextField = New TextField()
        cajaCodigoArticulo.ID = "txtCodigoArticulo" & idRegistro
        cajaCodigoArticulo.ClientIDMode = UI.ClientIDMode.Static
        cajaCodigoArticulo.Width = HeaderGridCodigo.Width
        cajaCodigoArticulo.ReadOnly = True
        cajaCodigoArticulo.Value = informacionLineaVenta.CodigoArticulo

        Dim cajaNombreArticulo As Ext.Net.TextField = New TextField()
        cajaNombreArticulo.ID = "txtNombreArticulo" & idRegistro
        cajaNombreArticulo.ClientIDMode = UI.ClientIDMode.Static
        cajaNombreArticulo.Width = HeaderGridNombre.Width
        cajaNombreArticulo.ReadOnly = True
        cajaNombreArticulo.Value = informacionLineaVenta.NombreArticulo

        Dim comboUnidadMedida As Ext.Net.ComboBox = New ComboBox()
        comboUnidadMedida.ID = "cmbUnidadMedida" & idRegistro
        comboUnidadMedida.ClientIDMode = UI.ClientIDMode.Static
        comboUnidadMedida.Width = HeaderGridUnidad.Width
        comboUnidadMedida.Enabled = True
        For i As Integer = 0 To informacionLineaVenta.UnidadesDeMedida.Count - 1
            comboUnidadMedida.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.UnidadesDeMedida(i).UnidadMedida, informacionLineaVenta.UnidadesDeMedida(i).Factor))
        Next
        comboUnidadMedida.Value = informacionLineaVenta.UnidadDeMedida.Factor
        comboUnidadMedida.Listeners.Select.Handler = "MetodoDetalleCambioUnidad.ControlarCambioUnidadMedida('" & idRegistro & "')"

        Dim cajaCantidad As Ext.Net.NumberField = New NumberField()
        cajaCantidad.ID = "txtCantidad" & idRegistro
        cajaCantidad.ClientIDMode = UI.ClientIDMode.Static
        cajaCantidad.Width = HeaderGridCantidad.Width
        cajaCantidad.Enabled = True
        cajaCantidad.Value = informacionLineaVenta.Cantidad
        cajaCantidad.DecimalPrecision = 2
        cajaCantidad.HideTrigger = True
        cajaCantidad.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        Dim hiddenNumeroFactores As Ext.Net.Hidden = New Hidden()
        hiddenNumeroFactores.ID = "hdnNumeroFactores" & idRegistro
        hiddenNumeroFactores.ClientIDMode = UI.ClientIDMode.Static
        hiddenNumeroFactores.Value = informacionLineaVenta.Factores.Count

        Dim defFactores As StringBuilder = New StringBuilder()
        For i As Integer = 0 To informacionLineaVenta.Factores.Count - 1
            defFactores.Append("|" & informacionLineaVenta.Factores(i).Precio & "|" & informacionLineaVenta.Factores(i).Nombre)
        Next
        If defFactores.Length > 0 Then
            defFactores.Remove(0, 1)
        End If

        Dim hiddenDefinicionFactores As Ext.Net.Hidden = New Hidden()
        hiddenDefinicionFactores.ID = "hdnDefinicionFactores" & idRegistro
        hiddenDefinicionFactores.ClientIDMode = UI.ClientIDMode.Static
        hiddenDefinicionFactores.Value = defFactores.ToString()

        Dim cajaPrecio As Ext.Net.NumberField = New NumberField()
        cajaPrecio.ID = "txtPrecio" & idRegistro
        cajaPrecio.ClientIDMode = UI.ClientIDMode.Static
        cajaPrecio.Width = HeaderGridPrecio.Width
        cajaPrecio.Enabled = False
        cajaPrecio.ReadOnly = True
        cajaPrecio.Value = informacionLineaVenta.Precio
        cajaPrecio.HideTrigger = True
        cajaPrecio.DecimalPrecision = 4

        Dim cajaPrecioImpuesto As Ext.Net.NumberField = New NumberField()
        cajaPrecioImpuesto.ID = "txtPrecioImpuesto" & idRegistro
        cajaPrecioImpuesto.ClientIDMode = UI.ClientIDMode.Static
        cajaPrecioImpuesto.Width = HeaderGridPrecioImpuesto.Width
        cajaPrecioImpuesto.Enabled = True
        cajaPrecioImpuesto.ReadOnly = False
        cajaPrecioImpuesto.Value = informacionLineaVenta.PrecioConImpuesto
        cajaPrecioImpuesto.HideTrigger = True
        cajaPrecioImpuesto.DecimalPrecision = 4
        cajaPrecioImpuesto.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotalesCambioPrecio.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"
        If Not informacionLineaVenta.PuedeModificarPrecio Then
            cajaPrecioImpuesto.Enabled = False
            cajaPrecioImpuesto.ReadOnly = True
        End If

        Dim cajaDescuento As Ext.Net.NumberField = New NumberField()
        cajaDescuento.ID = "txtDescuento" & idRegistro
        cajaDescuento.ClientIDMode = UI.ClientIDMode.Static
        cajaDescuento.Width = HeaderGridDescuento.Width
        cajaDescuento.Value = informacionLineaVenta.Descuento
        cajaDescuento.Enabled = True
        cajaDescuento.HideTrigger = True
        cajaDescuento.DecimalPrecision = 2
        cajaDescuento.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        Dim cajaTotalConImpuesto As Ext.Net.NumberField = New NumberField()
        cajaTotalConImpuesto.ID = "txtTotalLineaConImpuesto" & idRegistro
        cajaTotalConImpuesto.ClientIDMode = UI.ClientIDMode.Static
        cajaTotalConImpuesto.Width = HeaderGridImporte.Width
        cajaTotalConImpuesto.Value = informacionLineaVenta.TotalLineaImpuestos
        cajaTotalConImpuesto.DecimalPrecision = 2
        cajaTotalConImpuesto.HideTrigger = True

        Dim cajaComentarios As Ext.Net.TextField = New TextField()
        cajaComentarios.ID = "txtComentarios" & idRegistro
        cajaComentarios.ClientIDMode = UI.ClientIDMode.Static
        cajaComentarios.Width = HeaderGridComentarios.Width
        cajaComentarios.Value = informacionLineaVenta.Comentarios

        Dim hiddenSujetoAImpuesto As Ext.Net.Hidden = New Hidden()
        hiddenSujetoAImpuesto.ID = "hdnSujImp" & idRegistro
        hiddenSujetoAImpuesto.Value = informacionLineaVenta.SujetoAImpuesto
        hiddenSujetoAImpuesto.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenTotalLinea As Ext.Net.Hidden = New Hidden()
        hiddenTotalLinea.ID = "hdnTotalLinea" & idRegistro
        hiddenTotalLinea.Value = informacionLineaVenta.TotalLinea
        hiddenTotalLinea.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenPrecioMinimoAutorizacion As Ext.Net.Hidden = New Hidden()
        hiddenPrecioMinimoAutorizacion.ID = "hdnPrecioMinimoAutorizacion" & idRegistro
        hiddenPrecioMinimoAutorizacion.Value = informacionLineaVenta.PrecioMinimoAutorizacion
        hiddenPrecioMinimoAutorizacion.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenPrecioMaximoAutorizacion As Ext.Net.Hidden = New Hidden()
        hiddenPrecioMaximoAutorizacion.ID = "hdnPrecioMaximoAutorizacion" & idRegistro
        hiddenPrecioMaximoAutorizacion.Value = informacionLineaVenta.PrecioMaximoAutorizacion

        Dim hiddenPrecioCosto As Ext.Net.Hidden = New Hidden()
        hiddenPrecioCosto.ID = "hdnPrecioCosto" & idRegistro
        hiddenPrecioCosto.Value = informacionLineaVenta.PrecioCosto
        hiddenPrecioCosto.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenUltimaCantidad As Ext.Net.Hidden = New Hidden()
        hiddenUltimaCantidad.ID = "hdnCantidad" & idRegistro
        hiddenUltimaCantidad.Value = informacionLineaVenta.Cantidad
        hiddenUltimaCantidad.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenPrecio As Ext.Net.Hidden = New Hidden()
        hiddenPrecio.ID = "hdnPrecioImpuesto" & idRegistro
        hiddenPrecio.Value = informacionLineaVenta.PrecioConImpuesto
        hiddenPrecio.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenDescuento As Ext.Net.Hidden = New Hidden()
        hiddenDescuento.ID = "hdnDescuento" & idRegistro
        hiddenDescuento.Value = informacionLineaVenta.Descuento
        hiddenDescuento.ClientIDMode = UI.ClientIDMode.Static

        Dim hiddenIDLineas As Ext.Net.Hidden = New Hidden()
        hiddenIDLineas.ID = "hdnIdLinea" & idRegistro
        hiddenIDLineas.Name = "hdnIdLinea"
        hiddenIDLineas.Value = idRegistro
        hiddenIDLineas.ClientIDMode = UI.ClientIDMode.Static


        registroContainer.Items.Add(cajaCodigoArticulo)
        registroContainer.Items.Add(cajaNombreArticulo)
        registroContainer.Items.Add(comboUnidadMedida)
        registroContainer.Items.Add(cajaCantidad)
        registroContainer.Items.Add(hiddenNumeroFactores)
        registroContainer.Items.Add(hiddenDefinicionFactores)
        registroContainer.Items.Add(cajaPrecio)
        registroContainer.Items.Add(cajaPrecioImpuesto)
        registroContainer.Items.Add(cajaDescuento)
        registroContainer.Items.Add(cajaTotalConImpuesto)
        registroContainer.Items.Add(cajaComentarios)
        registroContainer.Items.Add(hiddenSujetoAImpuesto)
        registroContainer.Items.Add(hiddenUltimaCantidad)
        registroContainer.Items.Add(hiddenPrecio)
        registroContainer.Items.Add(hiddenDescuento)
        registroContainer.Items.Add(hiddenTotalLinea)
        registroContainer.Items.Add(hiddenPrecioMinimoAutorizacion)
        registroContainer.Items.Add(hiddenPrecioMaximoAutorizacion)
        registroContainer.Items.Add(hiddenPrecioCosto)
        registroContainer.Items.Add(hiddenIDLineas)
        PanelDetalleDocumento.Items.Add(registroContainer)
        registroContainer.Render()



    End Sub

    Private Sub UIFormularioClientesLectura()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim puedeModificarClientes As Boolean = controlUsuario.TieneAcceso(contextUser.UsuarioPuntoVenta.UserId, "Clientes", enumTipoAccesoModulo.Modificar)

        btnCrearCliente.Hide()
        btnSeleccionarCliente.Show()
        If puedeModificarClientes Then
            btnGuardarCliente.Show()
        Else
            btnGuardarCliente.Hide()
        End If

        txtNombreCliente.Disabled = Not puedeModificarClientes
        txtApPaternoCliente.Disabled = Not puedeModificarClientes
        txtApMaternoCliente.Disabled = Not puedeModificarClientes
        txtNombreComercialCliente.Disabled = Not puedeModificarClientes
        txtRFCCliente.Disabled = Not puedeModificarClientes
        txtCalleCliente.Disabled = Not puedeModificarClientes
        txtNumExtCliente.Disabled = Not puedeModificarClientes
        txtNumIntCliente.Disabled = Not puedeModificarClientes
        txtColoniaCliente.Disabled = Not puedeModificarClientes
        txtCPCliente.Disabled = Not puedeModificarClientes
        txtMunicipioCliente.Disabled = Not puedeModificarClientes
        cmbEstadoCliente.Disabled = Not puedeModificarClientes
        txtTelCasaCliente.Disabled = Not puedeModificarClientes
        txtTelOficinaCliente.Disabled = Not puedeModificarClientes
        txtCorreoCliente.Disabled = Not puedeModificarClientes
        txtFechaNacimientoCliente.Disabled = Not puedeModificarClientes
    End Sub

    Private Sub UIFormularioClientesBusqueda()
        btnGuardarCliente.Hide()
        btnCrearCliente.Hide()
        btnSeleccionarCliente.Hide()
        hdnCodigoCliente.Value = String.Empty
        txtNombreCliente.Disabled = True
        txtNombreCliente.Value = String.Empty
        txtApPaternoCliente.Disabled = True
        txtApPaternoCliente.Value = String.Empty
        txtApMaternoCliente.Disabled = True
        txtApMaternoCliente.Value = String.Empty
        txtNombreComercialCliente.Disabled = True
        txtNombreComercialCliente.Value = String.Empty
        txtRFCCliente.Disabled = True
        txtRFCCliente.Value = String.Empty
        txtCalleCliente.Disabled = True
        txtCalleCliente.Value = String.Empty
        txtNumExtCliente.Disabled = True
        txtNumExtCliente.Value = String.Empty
        txtNumIntCliente.Disabled = True
        txtNumIntCliente.Value = String.Empty
        txtColoniaCliente.Disabled = True
        txtColoniaCliente.Value = String.Empty
        txtCPCliente.Disabled = True
        txtCPCliente.Value = String.Empty
        txtMunicipioCliente.Disabled = True
        txtMunicipioCliente.Value = String.Empty
        cmbEstadoCliente.Disabled = True
        cmbEstadoCliente.SelectedItem.Index = -1
        txtTelCasaCliente.Disabled = True
        txtTelCasaCliente.Value = String.Empty
        txtTelOficinaCliente.Disabled = True
        txtTelOficinaCliente.Value = String.Empty
        txtCorreoCliente.Disabled = True
        txtCorreoCliente.Value = String.Empty
        txtFechaNacimientoCliente.Disabled = True
        txtFechaNacimientoCliente.Value = DateTime.MinValue
    End Sub

    Private Sub mapearDeClienteAFormulario(ByVal cliente As Cliente)
        hdnCodigoCliente.Value = cliente.Codigo
        txtNombreCliente.Value = cliente.Nombre
        txtRFCCliente.Value = cliente.RFC
        txtApPaternoCliente.Value = cliente.ApPaterno
        txtApMaternoCliente.Value = cliente.ApMaterno
        txtNombreComercialCliente.Value = cliente.NombreComercial
        txtCalleCliente.Value = cliente.Calle
        txtNumExtCliente.Value = cliente.NumeroExterior
        txtNumIntCliente.Value = cliente.NumeroInterior
        txtColoniaCliente.Value = cliente.Colonia
        txtCPCliente.Value = cliente.CodigoPostal
        txtMunicipioCliente.Value = cliente.Municipio
        cmbEstadoCliente.Value = cliente.Estado
        txtTelCasaCliente.Value = cliente.TelCasa
        txtTelOficinaCliente.Value = cliente.TelOficina
        txtCorreoCliente.Value = cliente.Correo
        txtFechaNacimientoCliente.Value = cliente.FechaNacimiento
    End Sub

    Private Function mapearDeFormularioACliente() As Cliente
        Return New Cliente(
            hdnCodigoCliente.Value,
            Trim(txtNombreCliente.Value), Trim(txtApPaternoCliente.Value), Trim(txtApMaternoCliente.Value), Trim(txtNombreComercialCliente.Value), Trim(txtRFCCliente.Value).ToUpper(),
            Trim(txtCalleCliente.Value), Trim(txtNumExtCliente.Value), Trim(txtNumIntCliente.Value), Trim(txtColoniaCliente.Value), Trim(txtCPCliente.Value), Trim(txtMunicipioCliente.Value),
            cmbEstadoCliente.Value, Trim(txtTelCasaCliente.Value), Trim(txtTelOficinaCliente.Value), Trim(txtCorreoCliente.Value), txtFechaNacimientoCliente.Value)
    End Function

    Private Function validarInformacionCliente(ByRef defCliente As Cliente, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty
        Return defCliente.ValidarInformacionCapturaCliente(mensajeError)
    End Function

    <DirectMethod>
    Public Sub ActualizarCliente()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controClientes As ControlClientes = New ControlClientes()
        Dim clienteActualizar As Cliente = mapearDeFormularioACliente()
        Dim mensajeError As String = String.Empty

        If Not validarInformacionCliente(clienteActualizar, mensajeError) Then
            MessageBoxExtNet.ShowError(mensajeError)
            Return
        End If

        If controClientes.RFCDuplicado(clienteActualizar.RFC, clienteActualizar.Codigo) Then
            MessageBoxExtNet.ShowError("RFC ya se encuentra dado de alta")
            Return
        End If

        If Not controClientes.ActualizarCliente(clienteActualizar) Then
            MessageBoxExtNet.ShowError("Error al actualizar el cliente consulte con su administrador")
        End If
        controClientes.RegistrarLogCliente("ACTUALIZAR", contextUser.UsuarioPuntoVenta.StoreId, contextUser.UsuarioPuntoVenta.UserId, clienteActualizar)

        MessageBoxExtNet.ShowSuccess("Cliente actualizado correctamente")

    End Sub

    <DirectMethod>
    Public Sub SeleccionarCliente()
        Dim controlCliente As ControlClientes = New ControlClientes()

        Dim cliente As Cliente = controlCliente.ObtenerCliente(hdnCodigoCliente.Value)
        cargarInformacionClienteEnFormularioVentas(cliente)
        winClientes.Hide()
    End Sub

    <DirectMethod>
    Public Sub CrearCliente()
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim controClientes As ControlClientes = New ControlClientes()
        Dim clienteCrear As Cliente = mapearDeFormularioACliente()
        Dim mensajeError As String = String.Empty

        If Not validarInformacionCliente(clienteCrear, mensajeError) Then
            MessageBoxExtNet.ShowError(mensajeError)
            Return
        End If

        If controClientes.RFCDuplicado(clienteCrear.RFC) Then
            MessageBoxExtNet.ShowError("RFC ya se encuentra dado de alta")
            Return
        End If

        If Not controClientes.CrearCliente(clienteCrear) Then
            MessageBoxExtNet.ShowError("Error al crear el cliente consulte con su administrador")
        End If

        controClientes.RegistrarLogCliente("CREAR", contextUser.UsuarioPuntoVenta.StoreId, contextUser.UsuarioPuntoVenta.UserId, clienteCrear)
        MessageBoxExtNet.ShowSuccess("Cliente registrado correctamente")

        cargarInformacionClienteEnFormularioVentas(clienteCrear)
        winClientes.Hide()
    End Sub

    Private Sub cargarInformacionClienteEnFormularioVentas(ByRef cliente As Cliente)
        hdnCodigoClienteVenta.Value = cliente.Codigo
        If cliente.NombreComercial <> String.Empty Then
            txtNombreClienteVenta.Value = cliente.NombreComercial
        Else
            txtNombreClienteVenta.Value = cliente.NombreCompleto
        End If
        txtRFCVenta.Value = cliente.RFC
        txtDireccionVenta.Value = cliente.Direccion
    End Sub

    Public Sub BuscarCliente()

        Dim tipoBusqueda As String = cmbTipoBuquedaCliente.SelectedItem.Value
        Dim valorBusqueda As String = txtValorBusquedaCliente.Value

        If valorBusqueda = String.Empty Then
            MessageBoxExtNet.ShowWarning("Error", "Valor de busqueda no puede ir en blanco")
            Return
        End If

        Dim controlClientes As ControlClientes = New ControlClientes()
        Dim clientesEncontrados As List(Of Cliente) = controlClientes.BuscarCliente(tipoBusqueda, valorBusqueda)

        StoreClientes.DataSource = clientesEncontrados
        StoreClientes.DataBind()
        UIFormularioClientesBusqueda()

        If clientesEncontrados.Count = 0 Then
            Dim messageBox As Ext.Net.MessageBox = New MessageBox()
            messageBox.Confirm("Crear Usuario?", "No se encontraron clientes con el criterio de busqueda seleccionado.Crear?", New JFunction() With {.Fn = "SeConfirmoCreacionCliente"}).Show()
        End If
    End Sub

#End Region

#Region "Metodos Control Ventana Articulos"

    <DirectMethod>
    Public Function ControlPaginacionArticulos(accion As String, extraParams As Dictionary(Of String, Object)) As Object
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        If hdnUltimoCodigoBuscado.Value = "|[NONE]|" Then
            Return Nothing
        End If

        Dim obtenerTotalRegistros As Boolean = False
        If hdnNuevaBusqueda.Value = "YES" Then
            hdnNuevaBusqueda.Value = "NO"
            obtenerTotalRegistros = True
        End If

        Dim codigoBusqueda As String = hdnUltimoCodigoBuscado.Value
        Dim nombreBusqueda As String = hdnUltimoNombreBuscado.Value

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim codigoAlmaceSAP As String = controlTienda.ObtenerCodigoAlmacenSAP(contextUser.UsuarioPuntoVenta.StoreId)
        If codigoAlmaceSAP = Nothing Then
            MessageBoxExtNet.ShowError("Codigo almacen tienda no encontrado")
            Return Nothing
        End If

        Dim parametrosPaginacion As StoreRequestParameters = New StoreRequestParameters(extraParams)
        Dim totalArticulos As Integer = 0

        Dim sortParameter As String = "Codigo"
        Dim sortDirection As String = "ASC"
        If parametrosPaginacion.Sort.Length > 0 Then
            sortParameter = parametrosPaginacion.Sort(0).Property
            sortDirection = parametrosPaginacion.Sort(0).Direction.ToString()
        End If

        Dim pagina As Integer = parametrosPaginacion.Page
        If pagina = 0 Then
            pagina = 1
        End If

        Dim articulosEncontrados As List(Of Articulo) = controlArticulo.BuscarArticulo(((pagina - 1) * parametrosPaginacion.Limit) + 1, pagina * parametrosPaginacion.Limit, sortParameter, sortDirection, codigoBusqueda, nombreBusqueda, "Venta", contextUser.UsuarioPuntoVenta.StoreId, codigoAlmaceSAP, obtenerTotalRegistros, totalArticulos)
        If obtenerTotalRegistros Then
            hdnTotalRegistrosNuevaBusqueda.Value = totalArticulos
        Else
            totalArticulos = hdnTotalRegistrosNuevaBusqueda.Value
        End If
        Return New Paging(Of Articulo)(articulosEncontrados, totalArticulos)
    End Function

    Public Sub BuscarArticulos()

        Dim codigoBusqueda As String = txtCodigoArticuloBusqueda.Value
        Dim nombreBusqueda As String = txtNombreArticuloBusqueda.Value

        If codigoBusqueda Is Nothing Then
            codigoBusqueda = String.Empty
        End If

        If nombreBusqueda Is Nothing Then
            nombreBusqueda = String.Empty
        End If

        hdnUltimoCodigoBuscado.Value = codigoBusqueda
        hdnUltimoNombreBuscado.Value = nombreBusqueda
        hdnNuevaBusqueda.Value = "YES"

        StoreArticulos.LoadProxy()

    End Sub

#End Region


End Class