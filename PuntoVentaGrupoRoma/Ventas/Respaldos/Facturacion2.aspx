﻿<%@ Page Language="VB" AutoEventWireup="false" Inherits="PuntoVentaGrupoRoma.Facturacion2" Codebehind="Facturacion2.aspx.vb" MasterPageFile="~/admin.master" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
           
    <ext:XScript ID="XScript1" runat="server">
                                           
    <script type="text/javascript">
        function ResetearForm(form) {
            form.getForm().reset();
            form.items.items[9].setValue("");
            form.items.items[10].setValue("");
            form.items.items[11].setValue("");
            form.items.items[8].setValue("");
            if (form.items.items[8].store != undefined) {
                form.items.items[8].store.removeAll();
            }
        }

        function InicializarValoresCombo(combo, numeroRegistros) {
            for (i = 0; i < numeroRegistros; i++)
                combo.removeByIndex(0)
        }

        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
                num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + '$' + num + '.' + cents);
        }
    
        var validate = function (e) {
            /*
            Properties of 'e' include:
                e.grid - This grid
                e.record - The record being edited
                e.field - The field name being edited
                e.value - The value being set
                e.originalValue - The original value for the field, before the edit.
                e.row - The grid row index
                e.column - The grid column index
            */
            // Call DirectMethod
            e.value = null;
        };

        var MetodoClienteSeleccionado = {
            javaCargarInformacionClienteSeleccionado: function (grid) {
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                });
                App.direct.CargarInformacionClienteSeleccionado(data);
            }
        }

        var MetodoClienteSeleccionado2 = {
            javaSeleccionarCliente: function (grid) {
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                });
                App.direct.SeleccionarClienteDobleClic(data);
            }
        }

        var MetodoArticuloSeleccionado = {
            javaCargarInformacionArticuloSeleccionado: function (grid) {
                var selModel = grid.getSelectionModel(),
                    selectedRecords = selModel.getSelection(),
                    data = [];
                Ext.each(selectedRecords, function (item) {
                    data.push(item.data.Codigo);
                    data.push(item.data.Nombre)
                });
                App.direct.CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(data);
            }
        }

        var MetodoDetalleCambioFactor = {
            ControlarCambioFactor: function (idRegistro) {
                var precio = Ext.getCmp("cmbFactor" + idRegistro).getValue();
                Ext.getCmp("txtPrecio" + idRegistro).setValue(precio);
                Ext.getCmp("hdnPrecio" + idRegistro).setValue(precio);
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.RecalcularTotalesSeleccionLineaGrid(cantidad, precio, descuento, sujetoAImpuesto, porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                    }
                });
            }
        }

        var MetodoDetalleCambioUnidad = {
            ControlarCambioUnidadMedida: function (idRegistro) {
                var comboUnidad = Ext.getCmp("cmbUnidadMedida" + idRegistro);
                var comboFactores = Ext.getCmp("cmbFactor" + idRegistro);
                var codigoArticulo = Ext.getCmp("txtCodigoArticulo" + idRegistro).getValue();
                var factoresUsuario = "0,0,1,1,0";
                var factoresTienda = "0,0,1,1,0";
                var precioCosto = Ext.getCmp("hdnPrecioCosto" + idRegistro).getValue();
                var numeroFactores = Ext.getCmp("hdnFactores" + idRegistro).getValue();
                var factorArticulo = Ext.getCmp("hdnFactorArticulo" + idRegistro).getValue();
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var valorUnidadMedida = comboUnidad.getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.ControlarCambioUnidadDeMedidaGrid(codigoArticulo,factorArticulo, factoresUsuario, factoresTienda, valorUnidadMedida, precioCosto, cantidad, descuento, sujetoAImpuesto, porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecioMinimoAutorizacion" + idRegistro).setValue(result.PrecioMinimoAutorizacion);
                        Ext.getCmp("hdnPrecioMaximoAutorizacion" + idRegistro).setValue(result.PrecioMaximoAutorizacion);
                        InicializarValoresCombo(comboFactores, numeroFactores);
                        for (i = 0; i < result.Factores.length; i++) {
                            comboFactores.insertItem(i, result.Factores[i].Descripcion, result.Factores[i].Precio);
                        }
                        comboFactores.setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtPrecio" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("hdnPrecio" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("hdnFactores" + idRegistro).setValue(result.Factores.length);
                    }
                });
            }
        }

        var MetodoDetalleRecalcularTotales = {
            RecalcularTotalesSeleccionLineaGrid: function (idRegistro) {
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var ultimaCantidad = Ext.getCmp("hdnCantidad" + idRegistro).getValue();
                var precio = Ext.getCmp("txtPrecio" + idRegistro).getValue();
                var ultimoPrecio = Ext.getCmp("hdnPrecio" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var ultimoDescuento = Ext.getCmp("hdnDescuento" + idRegistro).getValue();

                if (cantidad == ultimaCantidad && precio == ultimoPrecio && descuento == ultimoDescuento)
                    return;

                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.RecalcularTotalesSeleccionLineaGrid(cantidad, precio, descuento, sujetoAImpuesto, porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecio" + idRegistro).setValue(precio);
                        Ext.getCmp("hdnDescuento" + idRegistro).setValue(descuento);
                        Ext.getCmp("hdnCantidad" + idRegistro).setValue(cantidad);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                    }
                });


            }
        }
    </script>
    <script type="text/javascript">
        var updateRecord = function (form) {
            if (form.record == null) {
                return;
            }

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }
            form.getForm().updateRecord(form.record);
        };

        function add(a, b) {
            return (a + b).toFixed(2);
        }
        function subtract(a, b) {
            return (b - a).toFixed(2);
        }

        function IsDecimal(data) {
            var sFullNumber = data;
            var ValidChars = "0123456789.";
            var Validn = "0123456789";
            var IsDotPres = false;
            var Char;
            Char = sFullNumber.charAt(0);
            if (Validn.indexOf(Char) == -1) {
                alert("Please enter proper value.");
                data.select();
                return false;
            }
            else {
                for (i = 0; i < sFullNumber.length; i++) {
                    Char = sFullNumber.charAt(i);
                    if (Char == '.') {
                        if (IsDotPres == false)
                            IsDotPres = true;
                        else {
                            alert("Please remove extra '.' or spaces.");
                            data.select();
                            return false;
                        }
                    }
                    if (ValidChars.indexOf(Char) == -1) {
                        alert("Please check once again you entered proper value are not.");
                        data.select();
                        return false;
                    }
                }
            }
            return true;
        }


        var addPago = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();
            form.getForm().reset();
        };

        var addRecord = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            //            if (form.getForm().getFieldValues(false, "dataIndex").PU.toString()==""){
            //                Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar un articulo con precio 0") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });     
            //                return false;          
            //            }

            //            if (form.getForm().getFieldValues(false, "dataIndex").PU==0){
            //                Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar un articulo con precio 0") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });     
            //                return false;          
            //            }

            //            if (form.getForm().getFieldValues(false, "dataIndex").Categoria!="4" && form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)==-1) {
            //                    if (form.getForm().getFieldValues(false, "dataIndex").Cantidad.toString().indexOf(".",1)!=-1){
            //                           Ext.net.Notification.show({
            //                                iconCls  : "icon-exclamation",
            //                                html     : ("No puede ingresar decimales en esta categoria de articulo") ,
            //                                title    : "Advertencia",
            //                                HideDelay : 10000                    
            //                            });     
            //                            return false;
            //                    }
            //            }

            //            if (grid.topToolbar.items.items[5].getText().substring(0,1) == "M" && (form.getForm().getFieldValues(false, "dataIndex").Categoria=="1")){
            //                 Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar equipos con un cliente mostrador") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });
            //                return false;
            //            } 

            //            if (grid.topToolbar.items.items[5].getText().substring(0,1) == "M" && form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1){
            //                 Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar articulos de la categoria TEMM2 con un cliente mostrador") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });
            //                return false;
            //            } 
            //                     
            //            if (grid.topToolbar.items.items[4].getText() == ""){
            //                grid.topToolbar.items.items[4].setText(form.getForm().getFieldValues(false, "dataIndex").Categoria)
            //            }
            //            
            //            if (grid.topToolbar.items.items[5].getText().substring(0,1)=="C" && form.getForm().getFieldValues(false, "dataIndex").Categoria != "4") {
            //                if (grid.store.reader.jsonData.length >= 10) {
            //                        Ext.net.Notification.show({
            //                            iconCls  : "icon-exclamation",
            //                            html     : ("No puede ingresar mas de 10 lineas en este documento") ,
            //                            title    : "Advertencia",
            //                            HideDelay : 10000                    
            //                        });
            //                        return false;
            //                }
            //                if (form.getForm().getFieldValues(false, "dataIndex").Categoria==4 || form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)==0 || form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)==0) {
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                }
            //            }

            //            if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1) {
            //                    if (form.getForm().getFieldValues(false, "dataIndex").Referencia.toString().length==0){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia es obligatorio") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }                    
            //                }
            //                
            //                if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)!=-1){
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                }                        
            //                
            //                if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TEMM2",0)!=-1){
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                    if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //                }  

            //            if (form.getForm().getFieldValues(false, "dataIndex").DescripcionArticulo.toString().toUpperCase().indexOf("TIEMPO AIRE",0)==0){
            //                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
            //                    Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                    });
            //                    return false;                    
            //                }
            //                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                    Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                    });
            //                    return false;                    
            //                }
            //            }
            //            
            //            if (form.getForm().getFieldValues(false, "dataIndex").SIM!=""){
            //                if (form.getForm().getFieldValues(false, "dataIndex").DN==""){
            //                    Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("El campo DN es obligatorio para esta categoria de articulo") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                    });
            //                    return false;                    
            //                }
            //                if (form.getForm().getFieldValues(false, "dataIndex").DN.toString().length!=10){
            //                    Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("El campo DN debe ser de 10 caracteres") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                    });
            //                    return false;                    
            //                }                
            //            }
            //            
            //            if (form.getForm().getFieldValues(false, "dataIndex").Categoria=="4") {
            //                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN==""){
            //                    Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("El campo referencia DN es obligatorio para esta categoria de articulo") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                    });
            //                    return false;                    
            //                }
            //                if (form.getForm().getFieldValues(false, "dataIndex").ReferenciaDN.toString().length!=10){
            //                        Ext.net.Notification.show({
            //                        iconCls  : "icon-exclamation",
            //                        html     : ("El campo referencia DN debe ser de 10 caracteres") ,
            //                        title    : "Advertencia",
            //                        HideDelay : 10000                    
            //                        });
            //                        return false;                    
            //                    }
            //            }


            //            if (grid.topToolbar.items.items[4].getText() == "4" && form.getForm().getFieldValues(false, "dataIndex").Categoria!="4") {
            //                Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar articulos que no sean de la categoria TEMM en este documento") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });
            //                return false;
            //            }
            //            else if (grid.topToolbar.items.items[4].getText() != "4" && form.getForm().getFieldValues(false, "dataIndex").Categoria=="4") {
            //                 Ext.net.Notification.show({
            //                    iconCls  : "icon-exclamation",
            //                    html     : ("No puede ingresar articulos que sean de la categoria TEMM en este documento") ,
            //                    title    : "Advertencia",
            //                    HideDelay : 10000                    
            //                });
            //                return false;
            //            }

            grid.insertRecord(0, form.getForm().getFieldValues(true, "dataIndex"));
            grid.getView().refresh();
            ResetearForm(form);
            grid.load();
            grid.render();

            //Ext.net.DirectMethods.actualizaMontos();

        };

        var prepareadd = function (form, grid) {

            if ((form.items.items[0].getValue() <= 777051040) && (form.items.items[0].getValue() != "")) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("El articulo no existe en la base de datos, elija una articulo ente el rango 777051041 a 777051050"),
                    title: "Advertencia",
                    HideDelay: 10000
                });
                form.getForm().reset();
                form.items.items[0].setValue("777051041");
            }


            if (form.items.items[0].getValue() > 777051050 && form.items.items[0].getValue() !== "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("El articulo no existe en la base de datos, elija una articulo ente el rango 777051041 a 777051050"),
                    title: "Advertencia",
                    HideDelay: 10000
                });
                form.getForm().reset();
                form.items.items[0].setValue("777051041");
            }

            //var values = form.getForm().getFieldValues();  
            if (form.items.items[0].getValue() == "") {
                form.getForm().reset();
                form.items.items[0].setValue("777051041");
            }
            else {
                var randomnumber = Math.floor(Math.random() * 9) * 1000000
                var div = form.items.items[0].getValue() / randomnumber;
                //form.items.items[1].setValue("1000");
                //form.items.items[2].setValue("1000");
                //form.items.items[3].setValue("Pendiente");
                form.items.items[2].setValue('1');
                form.items.items[3].setValue('Lista de precios 01');
                form.items.items[1].setValue('Articulo ' + form.items.items[0].getValue());
                form.items.items[4].setValue(div);
                form.items.items[5].setValue(div * .16);
                form.items.items[6].setValue("Ninguna");
            }



        };

        var addRecord2 = function (form, grid) {

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            //var values = form.getForm().getFieldValues();  
            var values = form.getForm().getFieldValues(false, "dataIndex")
            //var i = 0
            //for(var field in form.getForm().getValues())  
            //{   
            //alert(field+':'+values[field]);  
            //i=i+1;
            //values[field] = "1";
            //var obj = form.getForm().elements[field];
            //alert(obj);  
            //}            

            //grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            ResetearForm(form);
        };
    </script>
    <script type="text/javascript">
        var updateClient = function (form) {
            if (form.record == null) {
                return;
            }

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }

            form.getForm().updateClient(form.record);
        };

        var addCliente = function (form, grid) {
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            //grid.insertRecord(grid.store.data.length + 1, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
            grid.getView().refresh();
            grid.getSelectionModel().selectRow(0);
        };

        var prepareaddCliente = function (form, grid) {

            if ((form.items.items[0].getValue() <= 777051040) && (form.items.items[0].getValue() != "")) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("Ya esta registrado el Cliente No. " + form.items.items[0].getValue()),
                    title: "Advertencia"
                });
                form.getForm().reset();
            }


            if (form.items.items[0].getValue() > 777051050 && form.items.items[0].getValue() !== "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: ("No se encontro el Cliente No. " + form.items.items[0].getValue()),
                    title: "Advertencia"
                });
                form.getForm().reset();
            }

            //var values = form.getForm().getFieldValues();  
            if (form.items.items[0].getValue() == "") {
                form.getForm().reset();
            }
            else {
                var randomnumber = Math.floor(Math.random() * 9) * 1000000
                var div = form.items.items[0].getValue() / randomnumber;
                //form.items.items[1].setValue("1000");
                //form.items.items[2].setValue("1000");
                //form.items.items[3].setValue("Pendiente");
                form.items.items[2].setValue('Lista de precios 01');
                form.items.items[1].setValue('Cliente ' + form.items.items[0].getValue());
                form.items.items[3].setValue(div);
                form.items.items[4].setValue(div * .16);
                form.items.items[5].setValue("Ninguna");
            }

        };

        var addCliente2 = function (form, grid) {

            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            //var values = form.getForm().getFieldValues();  
            var values = form.getForm().getFieldValues(false, "dataIndex")
            //var i = 0
            //for(var field in form.getForm().getValues())  
            //{   
            //alert(field+':'+values[field]);  
            //i=i+1;
            //values[field] = "1";
            //var obj = form.getForm().elements[field];
            //alert(obj);  
            //}            

            //grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.insertRecord(0, form.getForm().getFieldValues(false, "dataIndex"));
            form.getForm().reset();
        };
    </script>
    
    </ext:XScript>
        <ext:Store
            ID="StoreVendedores"
            runat="server">
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="Descripcion"  />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store
            ID="StoreArticulos"
            runat="server"
            RemotePaging="true"
            RemoteSort="true"
            PageSize="20">
            <Proxy>
                <ext:PageProxy DirectFn="App.direct.ControlPaginacionArticulos" />
            </Proxy>
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="Nombre" />
                        <ext:ModelField Name="Existencia" />
                    </Fields>
                </ext:Model>
            </Model>
        </ext:Store>
        <ext:Store 
            ID="StoreClientes" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false">
            <Model>
                <ext:Model runat="server" IDProperty="Codigo">
                    <Fields>
                        <ext:ModelField Name="Codigo" />
                        <ext:ModelField Name="NombreCompleto"  />
                        <ext:ModelField Name="Nombre"  />
                        <ext:ModelField Name="ApPaterno"  />
                        <ext:ModelField Name="ApMaterno"  />
                        <ext:ModelField Name="NombreComercial" />
                        <ext:ModelField Name="RFC" />
                        <ext:ModelField Name="CalleNumero"  />
                        <ext:ModelField Name="Calle"  />
                        <ext:ModelField Name="NumeroExterior"  />
                        <ext:ModelField Name="NumeroInterior"  />
                        <ext:ModelField Name="Colonia"  />
                        <ext:ModelField Name="CodigoPostal"  />
                        <ext:ModelField Name="Municipio"  />
                        <ext:ModelField Name="Estado"  />
                        <ext:ModelField Name="TelCasa"  />
                        <ext:ModelField Name="TelOficina" />
                        <ext:ModelField Name="Correo"  />
                        <ext:ModelField Name="FechaNacimiento"  />
                    </Fields>
                </ext:Model>
            </Model>            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>        
        <ext:Store 
        ID="StorePagos" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"
            OnBeforeStoreChanged="HandleChangesPagos" 
            SkipIdForNewRecords="false"   
            OnRefreshData="StorePagos_RefreshData"          
            RefreshAfterSaving="Always">
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="FormaPago"  />
                        <ext:ModelField Name="MontoPagado" />                        
                        <ext:ModelField Name="NoCuenta" />
                    </Fields>
                </ext:Model>
            </Model>            
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store>
        <ext:Hidden ID="UseConfirmation" runat="server" Text="false" />
        
    <ext:Panel runat="server" Flex="1" Layout="AnchorLayout" Width="1350" Height="35">
        <Items>
            <ext:FormPanel runat="server" Width="1350"  Layout="HBoxLayout" Frame="true">
                <Items>
                    <ext:ComboBox runat="server" ID="cmbVendedor" FieldLabel="Vendedor" DisplayField="Descripcion" ValueField="Codigo" StoreID="StoreVendedores" />                        
                    <ext:Label runat="server" Width="40" />
                    <ext:ComboBox runat="server" ID="cmbComprobante" FieldLabel="Comprobante">
                        <Items>
                            <ext:ListItem Value="T" Text="Ticket" />
                            <ext:ListItem Value="F" Text="Factura" />
                        </Items>
                    </ext:ComboBox>
                    <ext:Label runat="server" Width="40" />
                    <ext:DateField runat="server" ID="txtFecha" FieldLabel="Fecha" LabelAlign="Left" />
                </Items>
            </ext:FormPanel>
        </Items>

    </ext:Panel>
    <ext:Panel ID="BorderLayoutMain" runat="server" Flex="1" Layout="BorderLayout" Width="1350" Height="442" Border="false" >
        <Items>
        <ext:Panel 
            ID="pnlWest" 
            runat="server" 
            Layout="Border"
            Region="West"
            AutoScroll="true"
            Width="300">
            <Items>
                <ext:FormPanel 
                ID="FrmClienteVenta" 
                runat="server"
                Layout="AnchorLayout"
                Width="350"  
                Region="North"
                LabelAlign="Right"
                Frame="true">
                    <Items>            
                        <ext:Hidden 
                            ID="hdnCodigoClienteVenta"
                            runat="server"
                            FieldLabel="CodigoCliente"  />
                        <ext:Container runat="server" Layout="HBoxLayout" PaddingSpec="0 0 4 0">
                            <Items>
                                <ext:TextField 
                                    ID="txtNombreClienteVenta" 
                                    runat="server"
                                    FieldLabel="Cliente"
                                    Enabled="false"
                                    ReadOnly="true"
                                    LabelWidth="60"
                                    Width="260"/>                   
                                <ext:Button 
                                    ID="btnIniciarBusquedaCliente" 
                                    runat="server"
                                    Icon="Find"> 
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaClientes" />
                                    </DirectEvents>
                                </ext:Button>
                            </Items>
                        </ext:Container>
                        <ext:TextField 
                            ID="txtRFCVenta" 
                            runat="server"
                            FieldLabel="RFC"
                            Enabled="false"
                            ReadOnly ="true"
                            LabelWidth="60"
                            Width="260" />
                        <ext:TextArea 
                            ID="txtDireccionVenta" 
                            runat="server"
                            FieldLabel="Direccion"                    
                            Cols="30"
                            LabelWidth="60"
                            Enabled ="false"
                            ReadOnly="true"
                            />
                    </Items>
                <Buttons>
                
                </Buttons>
            </ext:FormPanel>       
            <ext:FormPanel 
                ID="FormSeleccionArticulo" 
                runat="server"
                Layout="AnchorLayout"
                LabelAlign="Right"
                AutoRender ="true" 
                Width="350"
                Region="Center"
                Frame="true">
                    <Items>  
                        <ext:FieldContainer 
                            Layout="HBoxLayout" 
                            runat="server" >
                            <Items>
                                <ext:Label 
                                    runat="server"
                                    Text="Articulo"
                                    Width="65" />
                                <ext:ComboBox
                                    Id="cmbBusquedaArticulo"
                                    LabelSeparator=""
                                    runat="server"  
                                    DisplayField="Nombre"
                                    ValueField="Codigo"
                                    TypeAhead="False"
                                    Width="200"
                                    PageSize="10"
                                    HideBaseTrigger="true"
                                    MinChars="4"
                                    TriggerAction="Query" >
                                        <ListConfig 
                                            LoadingText="Buscando">
                                            <ItemTpl runat="server">
                                                <Html>
                                                    <div>
                                                        <span style="font-weight:bolder">{Codigo}</span>|{Existencia}<br />
                                                        <span style="font-size:x-small;font-style:italic">{Nombre}</span>
                                                    </div>
                                                </Html>
                                            </ItemTpl>
                                        </ListConfig>
                                        <Store>
                                            <ext:Store runat="server" AutoLoad="false">
                                                <Proxy>
                                                    <ext:AjaxProxy Url="../Ajax/ListadoArticulos.ashx">
                                                        <ActionMethods Read="POST" />

                                                        <Reader>
                                                            <ext:JsonReader Root="Articulos" TotalProperty="total" />
                                                        </Reader>                                                        
                                                    </ext:AjaxProxy>
                                                </Proxy>
                                                <Model>
                                                    <ext:Model runat="server">
                                                        <Fields>
                                                            <ext:ModelField Name="Codigo" />
                                                            <ext:ModelField Name="Nombre" />
                                                            <ext:ModelField Name="Existencia"  />
                                                        </Fields>
                                                    </ext:Model>
                                                </Model>
                                            </ext:Store>
                                        </Store>
                                    <DirectEvents>
                                        <Select OnEvent="CargarInformacionArticuloSeleccionadoDeBuscador" />
                                    </DirectEvents>
                                    </ext:ComboBox>
                                <ext:Button runat="server" Icon="Find">
                                    <DirectEvents>
                                        <Click OnEvent="MostrarVentanaBusquedaArticulos" />
                                    </DirectEvents>
                                </ext:Button>
                                </Items>
                            </ext:FieldContainer>
                            <ext:Hidden
                                ID="hdnCodigoArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnFactorArticulo"
                                runat="server" />
                            <ext:Hidden
                                ID="hdnNombreArticulo"
                                runat="server"/>
                        <ext:Hidden
                            ID="hdnSujImp"
                            runat="server" />
                    <ext:ComboBox
                        ID="cmbUnidadMedida"
                        runat="server"
                        FieldLabel="Uni. Med."
                        LabelWidth="60"
                        AutoRender="true"
                        Width="288"
                        LabelSeparator="">
                        <DirectEvents>
                            <Select OnEvent="ControlarCambioUnidadMedida" />
                        </DirectEvents>
                        </ext:ComboBox>
                    <ext:Hidden 
                        ID="hdnItemsComboUnidadMedida"
                        runat="server"
                        Text="0" />
                    <ext:NumberField
                        ID="txtCantidad"
                        runat="server"
                        FieldLabel="Cantidad" 
                        LabelWidth="60"
                        Width="288"
                        LabelSeparator="" 
                        HideTrigger="true"
                        DecimalPrecision="2">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:ComboBox
                        ID="cmbFactor"
                        runat="server"
                        FieldLabel="Factor"
                        LabelWidth="60" 
                        Width="288"
                        AutoRender="true">
                        <DirectEvents>
                            <Select OnEvent="ActualizarPrecioPorCambioFactor" />
                        </DirectEvents>
                    </ext:ComboBox>
                    <ext:Hidden
                        ID="hdnItemsComboFactor"
                        runat="server" 
                        Text="0"/>
                    <ext:NumberField
                        ID="txtPrecio"
                        runat="server"
                        FieldLabel="Precio"
                        LabelWidth="60"
                        Width="288"
                        AllowDecimals="true"
                        DecimalPrecision="4"
                        HideTrigger="true">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:NumberField
                        ID="txtDescuento"
                        runat="server"
                        FieldLabel="% Desc"
                        LabelWidth="60"
                        Width="288"
                        AllowDecimals="true"
                        DecimalPrecision="2"
                        HideTrigger="true"
                        MinValue="0"
                        MaxValue="100">
                        <DirectEvents>
                            <Blur OnEvent="RecalcularTotalesSeleccionLinea" />
                        </DirectEvents>
                    </ext:NumberField>
                    <ext:TextArea
                        ID="txtComentarios"
                        runat="server"
                        FieldLabel="Detalle"
                        LabelWidth="60"
                        Width="288" />
                    <ext:NumberField
                        ID="txtTotalLineaImpuesto"
                        runat="server"
                        FieldLabel="Total"
                        LabelWidth="60"
                        Width="288" 
                        ReadOnly ="true"
                        DecimalPrecision="2"
                        />
                        <ext:Hidden 
                            ID="hdnPrecioMinimoAutorizacion"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnPrecioMaximoAutorizacion"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnPrecioCosto"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnTotalLinea"
                            runat="server" />
                    <ext:Button 
                        ID="BtnAgregarLinea" 
                        runat="server"
                        Text="+ Linea"
                        Icon="Add"> 
                        <DirectEvents>
                            <Click OnEvent="AgregarLineaVenta" />
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:FormPanel>
        </Items>            
        </ext:Panel>
        
        <ext:Panel ID="pnlCenter" 
            runat="server" 
            AutoScroll="true"
            Layout="BorderLayout"
            Region="Center"
            Width="700">
            <Items>
         <ext:Panel 
            runat="server" 
            Frame="true"
            PaddingSummary="5px 5px 0"
            Width="1000">
            <Items>
                <ext:Panel         
                    runat="server"    
                    Width="800" 
                    Height="300"
                    Layout="AnchorLayout"
                    Id="PanelDetalleDocumento"
                    AutoScroll="true">
                <Items>
                    <ext:Container runat="server" Width="750" Layout="HBoxLayout">
                        <Items>
                            <ext:Label runat="server" ID="HeaderGridCodigo" Text="Codigo" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridNombre" Text="Articulo" Width="140" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridUnidad" Text="Unidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridCantidad" Text="Cantidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridFactor" Text="Factor" Width="90" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridPrecio" Text="Precio" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridDescuento" Text="Descuento" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                            <ext:Label runat="server" ID="HeaderGridImporte" Text="Importe" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                            <ext:Label runat="server" ID="HeaderGridComentarios" Text="Comentarios" Width="100" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        </Items>
                    </ext:Container>
                </Items>
            </ext:Panel>

            </Items>
        </ext:Panel>
        
                <ext:Hidden 
                            ID="hdnPorcentajeImpuesto"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnTipoTienda"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnListaPrecioCosto"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnListaPrecioVenta"
                            runat="server" />
                        <ext:Hidden
                            ID="hdnFactoresUsuario"
                            runat="server" />
                        <ext:Hidden 
                            ID="hdnFactoresTienda"
                            runat="server" />

            </Items>
        </ext:Panel>
        </Items>
    </ext:Panel>        
        <ext:Window 
            title="Buscar Cliente" 
            runat="server" 
            ID="winClientes" 
            Modal="true" 
            Hidden="True" 
             Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content>   
                <ext:Panel 
                    ID="PanelIzquierda" 
                    runat="server" 
                    Width="300" 
                    Split="true"             
                    DefaultBorder="false"            
                    Layout="Border"
                    Region="West"
                    AutoScroll="true">   
                    <Items>
                        <ext:FieldContainer 
                            runat="server" 
                            FieldLabel="Buscar" 
                            LabelSeparator="" 
                            LabelStyle="font-weight:bolder;text-align:center" 
                            LabelAlign="Top"   
                            Layout="HBoxLayout" 
                            Region="North" 
                            DefaultMargins="0 0 15 0"
                            >
                            <Items>
                                <ext:ComboBox 
                                    ID="cmbTipoBuquedaCliente"
                                    runat="server" Width="80">
                                    <Items>
                                        <ext:ListItem Value="N" Text="Nombre" />
                                        <ext:ListItem Value="R" Text="RFC"  />
                                    </Items>
                                </ext:ComboBox>
                                <ext:TextField 
                                    ID="txtValorBusquedaCliente" 
                                    runat="server"
                                    AllowBlank="false"
                                    Enabled ="false" />
                                 <ext:Button 
                                    ID="btnBuscarCliente" 
                                    runat="server" 
                                    Icon="Find" >
                                        <DirectEvents>
                                            <Click OnEvent="BuscarCliente" />
                                        </DirectEvents>
                                    </ext:Button>
                            </Items>
                        </ext:FieldContainer>
                        <ext:Toolbar runat="server">
                            <Items>

                            </Items>
                        </ext:Toolbar>
                        <ext:FormPanel 
                            ID="FormCliente" 
                            runat="server"
                            LabelAlign="Right"
                            ButtonAlign="Center"
                            Layout="AnchorLayout"
                            Region="Center" >
                            <Buttons>           
                                <ext:Button 
                                    ID="btnGuardarCliente" 
                                    runat="server"
                                    Text="Save"
                                    Icon="Disk">
                                    <DirectEvents>
                                        <Click OnEvent="ActualizarCliente" />
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button 
                                    ID="btnCrearCliente" 
                                    runat="server"
                                    Text="Crear"
                                    Icon="ReportAdd">   
                                    <DirectEvents>
                                        <Click OnEvent="CrearCliente" />
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button 
                                    ID="btnSeleccionarCliente" 
                                    runat="server" 
                                    Text="Seleccionar" 
                                    Icon="Accept">
                                    <DirectEvents>
                                        <Click OnEvent="SeleccionarCliente" />
                                    </DirectEvents>
                                </ext:Button>
                            </Buttons>
                    <Items>
                        <ext:TextField 
                            ID="hdnCodigoCliente"
                            runat="server"
                            DataIndex="Codigo" />
                        <ext:TextField 
                            ID="txtNombreCliente" 
                            runat="server"
                            FieldLabel="Nombre"
                            DataIndex="Nombre"
                            AnchorHorizontal="100%"/>
                        <ext:TextField
                            ID="txtApPaternoCliente" 
                            runat="server"
                            FieldLabel="A. Paterno"
                            DataIndex="ApPaterno"
                            AnchorHorizontal="100%" />
                        <ext:TextField 
                            ID="txtApMaternoCliente" 
                            runat="server"
                            FieldLabel="A. Materno"
                            DataIndex="ApMaterno"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNombreComercialCliente"
                            runat="server"
                            FieldLabel="N. Comercial"
                            DataIndex="NombreComercial"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtRFCCliente"
                            runat="server"
                            FieldLabel="RFC"
                            DataIndex="RFC"
                            AnchorHorizontal="100%" />
                         <ext:TextField
                            ID="txtCalleCliente"
                            runat="server"
                            FieldLabel="Calle"
                            DataIndex="Calle"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNumExtCliente"
                            runat="server"
                            FieldLabel="Numero Ext"
                            DataIndex="NumeroExterior"
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtNumIntCliente" 
                            runat="server"
                            FieldLabel="Numero Int"
                            DataIndex="NumeroInterior"
                            AnchorHorizontal="100%" />
                         <ext:TextField
                            ID="txtColoniaCliente"
                            runat="server"
                            FieldLabel="Colonia"
                            DataIndex="Colonia"                 
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtCPCliente"
                            runat="server"
                            FieldLabel="Codigo Postal"
                            DataIndex="CodigoPostal"                  
                            AnchorHorizontal="100%" />
                        <ext:TextField
                            ID="txtMunicipioCliente"
                            runat="server"
                            FieldLabel="Municipio"
                            DataIndex="Municipio"                 
                            AnchorHorizontal="100%" />
                        <ext:ComboBox
                            ID="cmbEstadoCliente"
                            runat="server"
                            FieldLabel="Estado"
                            DataIndex="Estado"              
                            AnchorHorizontal="100%">              
                        </ext:ComboBox>
                        <ext:TextField 
                            ID="txtTelCasaCliente" 
                            runat="server"
                            FieldLabel="Tel Casa"
                            DataIndex="TelCasa"
                            AnchorHorizontal="100%"  />
                        <ext:TextField 
                            ID="txtTelOficinaCliente" 
                            runat="server"
                            FieldLabel="Tel Oficina"
                            DataIndex="TelOficina"
                            AnchorHorizontal="100%"  />
                        <ext:TextField 
                            ID="txtCorreoCliente" 
                            runat="server"
                            FieldLabel="Correo"
                            DataIndex="Correo"
                            AnchorHorizontal="100%" />
                         <ext:DateField 
                             ID="txtFechaNacimientoCliente" 
                             runat="server"
                             FieldLabel="F. Nacimiento"                
                             DataIndex="FechaNacimiento"
                             AnchorHorizontal="100%"  />
                    </Items>
                </ext:FormPanel>
            </Items>
            </ext:Panel>        
    </Content> 
    </ext:Window>
    <ext:Window 
        title="Buscar Articulos" 
            runat="server" 
            ID="winArticulos" 
            Modal="true" 
            Hidden="True" 
            Width="1000" 
            Height="600" 
            Maximizable="false"
            Layout="Border"
            Border="false">
        <Content> 
                    <ext:FormPanel 
                        runat="server"
                        ID="FormBusquedaArticulo"
                        Layout="AnchorLayout"
                        LabelAlign="right"
                        AutoRender="true"
                        Region="North"
                        Width="1000"
                        ButtonAlign="Left"
                        PaddingSpec="10 0 10 10"
                        Frame="true">
                        <Items>
                            <ext:Hidden 
                                runat="server"
                                Id="hdnNuevaBusqueda"
                                Text="NO" />
                            <ext:Hidden
                                runat="server"
                                Id="hdnTotalRegistrosNuevaBusqueda" />
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoCodigoBuscado" 
                                Text="|[NONE]|"/>
                            <ext:Hidden
                                runat="server"
                                ID="hdnUltimoNombreBuscado" />
                            <ext:FieldContainer runat="server">
                                <Items>
                                    <ext:TextField
                                        runat="server"
                                        ID="txtCodigoArticuloBusqueda"
                                        FieldLabel="Codigo"
                                        MarginSpec="0 0 0 8"
                                        LabelWidth="60"    
                                        AnchorHorizontal="200"
                                        DataIndex="CodigoArticulo"/>
                                    </Items>
                            </ext:FieldContainer>
                            <ext:FieldContainer runat="server">
                                <Items>
                            <ext:TextField
                                runat="server"
                                ID="txtNombreArticuloBusqueda"
                                FieldLabel="Nombre"
                                AnchorHorizontal="200"
                                MarginSpec="0 0 0 8"
                                LabelWidth="60"    
                                DataIndex="NombreArticulo" />
                                    </Items>
                                </ext:FieldContainer>
                        </Items>
                        <Buttons>
                            <ext:Button
                                runat="server"
                                ID="btnBuscarArticulo"
                                Text="Buscar"
                                Icon="Find">
                                <DirectEvents>
                                    <Click OnEvent="BuscarArticulos" />
                                </DirectEvents>
                            </ext:Button>
                        </Buttons>
                    </ext:FormPanel>
            <ext:GridPanel
                runat="server"
                Id="GridArticulo"
                Width="1000"
                Height="400"
                Layout="BorderLayout"
                Region="Center"
                Border="false"
                StoreID="StoreArticulos">
                <ColumnModel>
                    <Columns>
                        <ext:Column runat="server" Header="Codigo" DataIndex="Codigo" Width="100" />
                        <ext:Column runat="server" Header="Nombre" DataIndex="Nombre" Width="350" />
                        <ext:Column runat="server" Header="Existencia" DataIndex="Existencia" Width="100" />
                    </Columns>
                </ColumnModel>
                    <View>
                        <ext:GridView ID="GridView2" runat="server" ForceFit="true">
                        </ext:GridView>
                    </View>
                <Listeners>
                    <ItemDblClick Handler="MetodoArticuloSeleccionado.javaCargarInformacionArticuloSeleccionado(#{GridArticulo});" />
                </Listeners>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true" />                    
                    </SelectionModel>                
                <BottomBar>
                    <ext:PagingToolbar
                        runat="server"
                        DisplayInfo="true"
                        DisplayMsg="Articulos {0} - {1} de {2}"
                        EmptyMsg="Sin Articulos " />
                </BottomBar>
            </ext:GridPanel>

        </Content>
        </ext:Window>
</asp:Content>
