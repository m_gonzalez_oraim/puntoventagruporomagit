﻿Imports Ext.Net

Public Class Facturacion2
    Inherits System.Web.UI.Page

    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String
    Private sDefaultList As String
    Private sDefaultName As String
    Private mensaje As String = ""
    Private winMsg As Ext.Net.Window
    Private oWSData As WSData

    Private ReadOnly Property CurrentData() As List(Of Venta)
        Get
            Dim persons = Me.Session("TestPersons")

            If persons Is Nothing Then
                persons = Me.Ventas
                Me.Session("TestPersons") = persons
            End If

            Return DirectCast(persons, List(Of Venta))
        End Get
    End Property

    Private ReadOnly Property Ventas() As List(Of Venta)
        Get
            Return New List(Of Venta)(New Venta() _
                {})
        End Get
    End Property

    <DirectMethod()> _
    Public Sub CambiaArticulo()
    End Sub

    <DirectMethod()> _
    Public Sub CambiaLinea()


    End Sub

    <DirectMethod()> _
    Public Sub CambiaMedida()
    End Sub
    <DirectMethod()> _
    Public Sub CambiaModelo()

    End Sub

    <DirectMethod()> _
    Public Sub ObtenerArticulos()

    End Sub

    <DirectMethod()> _
    Public Sub ChangePrice()

    End Sub


    <DirectMethod()> _
    Public Sub ChangeABOX()

    End Sub

    <DirectMethod()> _
    Public Sub AplicaCombo()
    End Sub

    Public Sub cargaListas(Optional ByVal Cambio As Boolean = False)
    End Sub

    <DirectMethod()> _
    Public Sub AgregarLineaVenta()
        Dim lineaVenta As LineaVenta = New LineaVenta()
        lineaVenta.Id = Guid.NewGuid().ToString()
        lineaVenta.CodigoArticulo = hdnCodigoArticulo.Value
        lineaVenta.FactorArticulo = hdnFactorArticulo.Value
        lineaVenta.NombreArticulo = hdnNombreArticulo.Value
        lineaVenta.SujetoAImpuesto = True
        lineaVenta.Cantidad = txtCantidad.Value
        lineaVenta.Precio = txtPrecio.Value
        lineaVenta.Descuento = txtDescuento.Value
        lineaVenta.TotalLinea = hdnTotalLinea.Value
        lineaVenta.TotalLineaImpuestos = txtTotalLineaImpuesto.Value
        lineaVenta.PrecioMinimoAutorizacion = hdnPrecioMinimoAutorizacion.Value
        lineaVenta.PrecioMaximoAutorizacion = hdnPrecioMaximoAutorizacion.Value
        lineaVenta.PrecioCosto = hdnPrecioCosto.Value
        lineaVenta.Comentarios = txtComentarios.Value

        lineaVenta.Factor = New Factor()
        lineaVenta.Factor.Nombre = cmbFactor.Text
        lineaVenta.Factor.Precio = cmbFactor.Value

        lineaVenta.UnidadDeMedida = New UnidadMedida()
        lineaVenta.UnidadDeMedida.UnidadMedida = cmbUnidadMedida.Text
        lineaVenta.UnidadDeMedida.Factor = cmbUnidadMedida.Value


    End Sub

    <DirectMethod()> _
    Public Sub MostrarVentanaBusquedaArticulos()
        StoreArticulos.Data = Nothing
        StoreArticulos.DataBind()

        txtCodigoArticuloBusqueda.Value = String.Empty
        txtNombreArticuloBusqueda.Value = String.Empty

        hdnUltimoCodigoBuscado.Value = "|[NONE]|"
        hdnUltimoNombreBuscado.Value = Nothing

        winArticulos.Show()
    End Sub

    <DirectMethod()> _
    Public Sub CargarArticuloCodigoBarras()

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            definicionInterfazFechaCreacionDocumento()
            cargarInformacionEnCombos()
            cargarParametrosVenta()
            cmbVendedor.Focus()

        End If
    End Sub

    Private Sub cargarDatosClienteMostrador(ByVal codigoClienteMostrador As String)


    End Sub

    Protected Sub definicionInterfazFechaCreacionDocumento()
        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        txtFecha.Value = controlTienda.FechaAperturaCaja(Session("IDSTORE"))
        txtFecha.Enabled = controlUsuario.PuedeModificarFechaDocumento(Session("AdminUserId"))
        txtFecha.ReadOnly = Not txtFecha.Enabled
    End Sub

    Protected Sub cargarParametrosVenta()
        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim controlUsuario As ControlUsuario = New ControlUsuario()

        Dim listaPrecioCosto As Integer = -1
        Dim listaPrecioVenta As Integer = -1
        Dim porcentajeImpuesto As Decimal
        Dim codigoClienteMostrador As Integer = -1

        controlTienda.ConfiguracionVentaTienda(Session("IDSTORE"), listaPrecioVenta, listaPrecioCosto, porcentajeImpuesto, codigoClienteMostrador)

        hdnListaPrecioCosto.Value = listaPrecioCosto
        hdnListaPrecioVenta.Value = listaPrecioVenta
        hdnPorcentajeImpuesto.Value = porcentajeImpuesto
        hdnFactoresUsuario.Value = controlUsuario.FactoresUsuario(Session("AdminUserId"))

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(codigoClienteMostrador)
        cargarInformacionClienteEnFormularioVentas(cliente)


    End Sub

    Protected Sub cargarInformacionEnCombos()
        Dim codigosEstado As Dictionary(Of String, String) = New ControlCatalogos().CodigosEstado()
        For Each estado In codigosEstado
            cmbEstadoCliente.Items.Add(New Ext.Net.ListItem(estado.Value, estado.Key))
        Next

        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim tablaVendedores As System.Data.DataTable = controlTienda.ObtenerVendedoresPuntoDeVenta(Session("IDSTORE"))
        StoreVendedores.Data = tablaVendedores
        StoreVendedores.DataBind()

        cmbComprobante.Value = "T"
        cmbVendedor.Select(0)

    End Sub

    Protected Sub HandleChanges(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)

    End Sub


    Protected Sub HandleChangesPagos(ByVal sender As Object, ByVal e As BeforeStoreChangedEventArgs)

    End Sub

#Region "Metodos Control Ventana Clientes"


    <DirectMethod()>
    Public Sub MostrarVentanaClientes()
        cmbTipoBuquedaCliente.Value = "N"
        txtValorBusquedaCliente.Value = String.Empty

        UIFormularioClientesBusqueda()
        StoreClientes.Data = Nothing
        StoreClientes.DataBind()

        winClientes.Show()
    End Sub

    <DirectMethod>
    Public Sub UIFormularioClientesCrear()

        btnGuardarCliente.Hide()
        btnSeleccionarCliente.Hide()
        btnCrearCliente.Show()

        hdnCodigoCliente.Value = String.Empty
        txtNombreCliente.Disabled = False
        txtNombreCliente.Value = String.Empty
        txtApPaternoCliente.Disabled = False
        txtApPaternoCliente.Value = String.Empty
        txtApMaternoCliente.Disabled = False
        txtApMaternoCliente.Value = String.Empty
        txtNombreComercialCliente.Disabled = False
        txtNombreComercialCliente.Value = String.Empty
        txtRFCCliente.Disabled = False
        txtRFCCliente.Value = String.Empty
        txtCalleCliente.Disabled = False
        txtCalleCliente.Value = String.Empty
        txtNumExtCliente.Disabled = False
        txtNumExtCliente.Value = String.Empty
        txtNumIntCliente.Disabled = False
        txtNumIntCliente.Value = String.Empty
        txtColoniaCliente.Disabled = False
        txtColoniaCliente.Value = String.Empty
        txtCPCliente.Disabled = False
        txtCPCliente.Value = String.Empty
        txtMunicipioCliente.Disabled = False
        txtMunicipioCliente.Value = String.Empty
        cmbEstadoCliente.Disabled = False
        cmbEstadoCliente.SelectedItem.Index = -1
        txtTelCasaCliente.Disabled = False
        txtTelCasaCliente.Value = String.Empty
        txtTelOficinaCliente.Disabled = False
        txtTelOficinaCliente.Value = String.Empty
        txtCorreoCliente.Disabled = False
        txtCorreoCliente.Value = String.Empty
        txtFechaNacimientoCliente.Disabled = False
        txtFechaNacimientoCliente.Value = DateTime.MinValue

    End Sub
    <DirectMethod>
    Public Sub CargarInformacionClienteSeleccionado(ByVal codigoCliente As String)
        Dim idCodigoCliente As Integer = codigoCliente.Replace("""", String.Empty).Replace("[", String.Empty).Replace("]", String.Empty)

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(idCodigoCliente)
        mapearDeClienteAFormulario(cliente)
        UIFormularioClientesLectura()
    End Sub

    <DirectMethod>
    Public Sub SeleccionarClienteDobleClic(ByVal codigoCliente As String)

        Dim idCodigoCliente As Integer = codigoCliente.Replace("""", String.Empty).Replace("[", String.Empty).Replace("]", String.Empty)

        Dim controlCliente As ControlClientes = New ControlClientes()
        Dim cliente As Cliente = controlCliente.ObtenerCliente(idCodigoCliente)
        cargarInformacionClienteEnFormularioVentas(cliente)
        winClientes.Hide()
    End Sub

    <DirectMethod>
    Public Sub CargarInformacionArticuloSeleccionadoDeVentanaBusqueda(ByVal parametrosRegistroSeleccionado As String)
        Dim valoresArticuloSeleccionado As List(Of String) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of String))(parametrosRegistroSeleccionado)

        Dim codigoArticulo As String = valoresArticuloSeleccionado(0)
        Dim nombreArticulo As String = valoresArticuloSeleccionado(1)
        cmbBusquedaArticulo.Text = nombreArticulo
        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo

        winArticulos.Close()

        prepararInterfazCapturaLinea(codigoArticulo)
        RecalcularTotalesSeleccionLinea()
    End Sub

    Public Sub CargarInformacionArticuloSeleccionadoDeBuscador()
        Dim nombreArticulo As String = cmbBusquedaArticulo.SelectedItem.Text
        Dim codigoArticulo As String = cmbBusquedaArticulo.SelectedItem.Value

        hdnCodigoArticulo.Value = codigoArticulo
        hdnNombreArticulo.Value = nombreArticulo

        prepararInterfazCapturaLinea(codigoArticulo)
        RecalcularTotalesSeleccionLinea()
    End Sub

    <DirectMethod>
    Public Shared Function RecalcularTotalesSeleccionLineaGrid(ByVal cantidad As Decimal, ByVal precio As Decimal,
                                                         ByVal descuento As Decimal, ByVal sujetoAImpuesto As Boolean,
                                                         ByVal porcentajeImpuesto As Decimal
                                                        ) As ValoresRetornoTotalLinea
        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLinea(cantidad, precio, descuento, sujetoAImpuesto, porcentajeImpuesto, importeLinea, importeLineaImpuesto)

        Dim retornoTotales As ValoresRetornoTotalLinea = New ValoresRetornoTotalLinea()
        retornoTotales.ImporteLinea = importeLinea
        retornoTotales.ImporteLineaImpuesto = importeLineaImpuesto

        Return retornoTotales
    End Function

    Public Sub RecalcularTotalesSeleccionLinea()
        Dim controlDocumento As ControlDocumento = New ControlDocumento()

        Dim cantidad As Decimal = txtCantidad.Value
        Dim precio As Decimal = txtPrecio.Value
        Dim sujetoImpuesto As Boolean = hdnSujImp.Value
        Dim descuento As Decimal = txtDescuento.Value
        Dim porcentajeImpuesto As Decimal = hdnPorcentajeImpuesto.Value

        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLinea(cantidad, precio, descuento, sujetoImpuesto, porcentajeImpuesto, importeLinea, importeLineaImpuesto)

        txtTotalLineaImpuesto.Value = importeLineaImpuesto
        hdnTotalLinea.Value = importeLinea
    End Sub

    Public Sub ActualizarPrecioPorCambioFactor()
        txtPrecio.Value = cmbFactor.SelectedItem.Value
        RecalcularTotalesSeleccionLinea()
    End Sub

    Private Sub prepararInterfazCapturaLinea(ByVal codigoArticulo As String)
        Dim controlArticulos As ControlArticulos = New ControlArticulos()
        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim factoresUsuario As List(Of Boolean) = definicionFactoresDeHiddenToList(hdnFactoresUsuario.Value)
        Dim factoresTienda As List(Of Boolean) = definicionFactoresDeHiddenToList(hdnFactoresTienda.Value)

        Dim esArticuloFix As Boolean
        Dim factorArticulo As String = String.Empty
        Dim precioLista As Decimal
        Dim unidadesMedida As List(Of UnidadMedida) = Nothing
        Dim factoresPrecioArticulo As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal
        Dim precioMaximoAutorizacion As Decimal
        Dim precioCosto As Decimal
        Dim puedeElegirFactor As Boolean
        Dim puedeModificarPrecio As Boolean
        Dim sujetoAImpuesto As Boolean

        Dim listaPrecioCosto As Integer = hdnListaPrecioCosto.Value
        Dim listaPrecioVenta As Integer = hdnListaPrecioVenta.Value
        Dim tipoTienda As String = hdnTipoTienda.Value

        If Not controlArticulos.InformacionVentaArticulo(codigoArticulo, Session("IdStore"), factoresUsuario, factoresTienda, listaPrecioCosto, listaPrecioVenta, _
                                                  tipoTienda, esArticuloFix, factorArticulo, precioLista, unidadesMedida, factoresPrecioArticulo, _
                                                  precioMinimoAutorizacion, precioMaximoAutorizacion, precioCosto, puedeElegirFactor, puedeModificarPrecio, sujetoAImpuesto) Then
            cmbUnidadMedida.ReadOnly = True
            InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)
            hdnItemsComboUnidadMedida.Value = 0
            txtCantidad.ReadOnly = True
            txtCantidad.Value = 1
            cmbFactor.ReadOnly = True
            hdnFactorArticulo.Value = String.Empty
            InicializarValoresCombo(cmbFactor, hdnItemsComboFactor.Value)
            hdnItemsComboFactor.Value = 0

            txtPrecio.ReadOnly = True
            txtPrecio.Value = 0
            txtComentarios.ReadOnly = True
            txtComentarios.Text = String.Empty
            BtnAgregarLinea.Disabled = True
            MessageBoxExtNet.ShowError("No se pudo obtener la informacion de venta para el articulo seleccionado. Consulte con su administrador")
            Return
        End If

        hdnSujImp.Value = sujetoAImpuesto
        hdnFactorArticulo.Value = factorArticulo
        InicializarValoresCombo(cmbUnidadMedida, hdnItemsComboUnidadMedida.Value)

        For i As Integer = 0 To unidadesMedida.Count - 1
            cmbUnidadMedida.InsertItem(i, unidadesMedida(i).UnidadMedida, unidadesMedida(i).Factor)
        Next
        cmbUnidadMedida.Select(0)
        hdnItemsComboUnidadMedida.Value = unidadesMedida.Count

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1
        txtComentarios.ReadOnly = False
        txtComentarios.Text = String.Empty
        BtnAgregarLinea.Disabled = False

        If esArticuloFix OrElse Not puedeElegirFactor Then
            cmbFactor.ReadOnly = True
            InicializarValoresCombo(cmbFactor, hdnItemsComboFactor.Value)
            hdnItemsComboFactor.Value = 0
            txtPrecio.ReadOnly = True
            txtPrecio.Value = precioLista
            Return
        End If

        cmbFactor.ReadOnly = False
        InicializarValoresCombo(cmbFactor, hdnItemsComboFactor.Value)
        For i As Integer = 0 To factoresPrecioArticulo.Count - 1
            cmbFactor.InsertItem(i, factoresPrecioArticulo(i).Descripcion, factoresPrecioArticulo(i).Precio)
        Next
        cmbFactor.Select(0)
        hdnItemsComboFactor.Value = factoresPrecioArticulo.Count

        txtCantidad.ReadOnly = False
        txtCantidad.Value = 1
        txtPrecio.ReadOnly = False
        txtPrecio.Value = factoresPrecioArticulo.Item(0).Precio
        txtComentarios.Disabled = False
        txtComentarios.Value = String.Empty

        hdnPrecioMaximoAutorizacion.Value = precioMaximoAutorizacion
        hdnPrecioMinimoAutorizacion.Value = precioMinimoAutorizacion
        hdnPrecioCosto.Value = precioCosto

    End Sub

    Public Sub InicializarValoresCombo(ByRef combo As Ext.Net.ComboBox, ByVal numeroRegistros As Integer)
        For i As Integer = 0 To numeroRegistros - 1
            combo.RemoveByIndex(0)
        Next
    End Sub

    <DirectMethod>
    Public Shared Function ControlarCambioUnidadDeMedidaGrid(ByVal codigoArticulo As String, ByVal codigoFactor As String, ByVal factoresUsuario As String, ByVal factoresTienda As String, ByVal factorUnidadMedida As Decimal, ByVal precioCosto As Decimal, ByVal cantidad As Decimal, ByVal descuento As Decimal, ByVal sujetoAImpuesto As Boolean, ByVal porcentajeImpuesto As Decimal) As ValoresRetornoCambioUnidadMedida
        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim factoresPrecio As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal = 0
        Dim precioMaximoAutorizacion As Decimal = 0

        controlArticulo.ActualizarInformacionFactores(codigoArticulo, codigoFactor, definicionFactoresDeHiddenToListShared(factoresUsuario), definicionFactoresDeHiddenToListShared(factoresTienda), factorUnidadMedida, precioCosto, factoresPrecio, precioMinimoAutorizacion, precioMaximoAutorizacion)

        Dim valorUnidadMedida As ValoresRetornoCambioUnidadMedida = New ValoresRetornoCambioUnidadMedida()
        valorUnidadMedida.Factores = factoresPrecio
        valorUnidadMedida.PrecioMinimoAutorizacion = precioMinimoAutorizacion
        valorUnidadMedida.PrecioMaximoAutorizacion = precioMaximoAutorizacion
        valorUnidadMedida.PrecioPrimerFactor = valorUnidadMedida.Factores(0).Precio

        Dim controlDocumento As ControlDocumento = New ControlDocumento()
        Dim importeLinea As Decimal
        Dim importeLineaImpuesto As Decimal
        controlDocumento.CalcularMontoLinea(cantidad, valorUnidadMedida.PrecioPrimerFactor, descuento, sujetoAImpuesto, porcentajeImpuesto, importeLinea, importeLineaImpuesto)

        valorUnidadMedida.ImporteLinea = importeLinea
        valorUnidadMedida.ImporteLineaImpuesto = importeLineaImpuesto

        Return valorUnidadMedida
    End Function

    <DirectMethod>
    Public Sub ControlarCambioUnidadMedida()

        Dim factorUnidadMedida As Decimal = cmbUnidadMedida.Value

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim factoresPrecio As List(Of Factor) = Nothing
        Dim precioMinimoAutorizacion As Decimal = 0
        Dim precioMaximoAutorizacion As Decimal = 0

        controlArticulo.ActualizarInformacionFactores(hdnCodigoArticulo.Value, hdnFactorArticulo.Value, definicionFactoresDeHiddenToList(hdnFactoresUsuario.Value), definicionFactoresDeHiddenToList(hdnFactoresTienda.Value), factorUnidadMedida, hdnPrecioCosto.Value, factoresPrecio, precioMinimoAutorizacion, precioMaximoAutorizacion)

        InicializarValoresCombo(cmbFactor, hdnItemsComboFactor.Value)
        For i As Integer = 0 To factoresPrecio.Count - 1
            cmbFactor.InsertItem(i, factoresPrecio(i).Descripcion, factoresPrecio(i).Precio)
        Next
        cmbFactor.Select(0)

        txtPrecio.Value = factoresPrecio(0).Precio
        hdnItemsComboFactor.Value = factoresPrecio.Count
        hdnPrecioMinimoAutorizacion.Value = precioMinimoAutorizacion
        hdnPrecioMaximoAutorizacion.Value = precioMaximoAutorizacion

        RecalcularTotalesSeleccionLinea()
    End Sub

    Private Shared Function definicionFactoresDeHiddenToListShared(ByVal definicionFactor As String) As List(Of Boolean)
        Dim permisosFactoresNumero() As String = definicionFactor.Split(",")

        Dim definicionFactores As List(Of Boolean) = New List(Of Boolean)()
        For i As Integer = 0 To permisosFactoresNumero.Length - 1
            definicionFactores.Add(permisosFactoresNumero(i) = "1")
        Next
        Return definicionFactores
    End Function


    Private Function definicionFactoresDeHiddenToList(ByVal definicionFactor As String) As List(Of Boolean)
        Dim permisosFactoresNumero() As String = definicionFactor.Split(",")

        Dim definicionFactores As List(Of Boolean) = New List(Of Boolean)()
        For i As Integer = 0 To permisosFactoresNumero.Length - 1
            definicionFactores.Add(permisosFactoresNumero(i) = "1")
        Next
        Return definicionFactores
    End Function

    Private Sub AgregarRegistroDetalleDocumento(ByVal informacionLineaVenta As LineaVenta)
        Dim idRegistro As String = Guid.NewGuid().ToString()

        Dim registroContainer As Ext.Net.Container = New Container()
        registroContainer.Layout = "HBoxLayout"
        registroContainer.Width = PanelDetalleDocumento.Width
        registroContainer.ID = "contenedor" & idRegistro

        Dim cajaCodigoArticulo As Ext.Net.TextField = New TextField()
        cajaCodigoArticulo.ID = "txtCodigoArticulo" & idRegistro
        cajaCodigoArticulo.Width = HeaderGridCodigo.Width
        cajaCodigoArticulo.ReadOnly = True
        cajaCodigoArticulo.Value = informacionLineaVenta.CodigoArticulo

        Dim cajaNombreArticulo As Ext.Net.TextField = New TextField()
        cajaNombreArticulo.ID = "txtNombreArticulo" & idRegistro
        cajaNombreArticulo.Width = HeaderGridNombre.Width
        cajaNombreArticulo.ReadOnly = True
        cajaNombreArticulo.Value = informacionLineaVenta.NombreArticulo

        Dim comboUnidadMedida As Ext.Net.ComboBox = New ComboBox()
        comboUnidadMedida.ID = "cmbUnidadMedida" & idRegistro
        comboUnidadMedida.Width = HeaderGridUnidad.Width
        comboUnidadMedida.Enabled = True
        For i As Integer = 0 To informacionLineaVenta.UnidadesDeMedida.Count - 1
            comboUnidadMedida.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.UnidadesDeMedida(i).UnidadMedida, informacionLineaVenta.UnidadesDeMedida(i).Factor))
        Next
        comboUnidadMedida.Value = informacionLineaVenta.UnidadDeMedida.Factor
        comboUnidadMedida.Listeners.Select.Handler = "MetodoDetalleCambioUnidad.ControlarCambioUnidadMedida('" & idRegistro & "')"

        Dim cajaCantidad As Ext.Net.NumberField = New NumberField()
        cajaCantidad.ID = "txtCantidad" & idRegistro
        cajaCantidad.Width = HeaderGridCantidad.Width
        cajaCantidad.Enabled = True
        cajaCantidad.Value = informacionLineaVenta.Cantidad
        cajaCantidad.DecimalPrecision = 2
        cajaCantidad.HideTrigger = True
        cajaCantidad.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        Dim comboFactor As Ext.Net.ComboBox = New ComboBox()
        comboFactor.ID = "cmbFactor" & idRegistro
        comboFactor.Width = HeaderGridFactor.Width
        comboFactor.Enabled = True
        For i As Integer = 0 To informacionLineaVenta.Factores.Count - 1
            comboFactor.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.Factores(i).Descripcion, informacionLineaVenta.Factores(i).Precio))
        Next
        comboFactor.Value = informacionLineaVenta.Factor.Precio
        comboFactor.Listeners.Select.Handler = "MetodoDetalleCambioFactor.ControlarCambioFactor('" & idRegistro & "')"
        If Not informacionLineaVenta.PuedeElegirFactor Then
            comboFactor.Enabled = False
            comboFactor.ReadOnly = True
        End If

        Dim hiddenFactores As Ext.Net.Hidden = New Hidden()
        hiddenFactores.ID = "hdnItemsComboFactor" & idRegistro
        hiddenFactores.Value = informacionLineaVenta.Factores.Count

        Dim cajaPrecio As Ext.Net.NumberField = New NumberField()
        cajaPrecio.ID = "txtPrecio" & idRegistro
        cajaPrecio.Width = HeaderGridPrecio.Width
        cajaPrecio.Enabled = True
        cajaPrecio.Value = informacionLineaVenta.Precio
        cajaPrecio.HideTrigger = True
        cajaPrecio.DecimalPrecision = 4
        cajaPrecio.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"
        If Not informacionLineaVenta.PuedeModificarPrecio Then
            cajaPrecio.Enabled = False
            cajaPrecio.ReadOnly = True
        End If

        Dim cajaDescuento As Ext.Net.NumberField = New NumberField()
        cajaDescuento.ID = "txtDescuento" & idRegistro
        cajaDescuento.Width = HeaderGridDescuento.Width
        cajaDescuento.Value = informacionLineaVenta.Descuento
        cajaDescuento.Enabled = True
        cajaDescuento.HideTrigger = True
        cajaDescuento.DecimalPrecision = 2
        cajaDescuento.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        Dim cajaTotalConImpuesto As Ext.Net.NumberField = New NumberField()
        cajaTotalConImpuesto.ID = "txtTotalLineaConImpuesto" & idRegistro
        cajaTotalConImpuesto.Width = HeaderGridImporte.Width
        cajaTotalConImpuesto.Value = informacionLineaVenta.TotalLineaImpuestos
        cajaTotalConImpuesto.DecimalPrecision = 2
        cajaTotalConImpuesto.HideTrigger = True

        Dim cajaComentarios As Ext.Net.TextField = New TextField()
        cajaComentarios.ID = "txtComentarios" & idRegistro
        cajaComentarios.Width = HeaderGridComentarios.Width
        cajaComentarios.Value = informacionLineaVenta.Comentarios

        Dim hiddenSujetoAImpuesto As Ext.Net.Hidden = New Hidden()
        hiddenSujetoAImpuesto.ID = "hdnSujImp" & idRegistro
        hiddenSujetoAImpuesto.Value = informacionLineaVenta.SujetoAImpuesto

        Dim hiddenTotalLinea As Ext.Net.Hidden = New Hidden()
        hiddenTotalLinea.ID = "hdnTotalLinea" & idRegistro
        hiddenTotalLinea.Value = informacionLineaVenta.TotalLinea

        Dim hiddenPrecioMinimoAutorizacion As Ext.Net.Hidden = New Hidden()
        hiddenPrecioMinimoAutorizacion.ID = "hdnPrecioMinimoAutorizacion" & idRegistro
        hiddenPrecioMinimoAutorizacion.Value = informacionLineaVenta.PrecioMinimoAutorizacion

        Dim hiddenPrecioMaximoAutorizacion As Ext.Net.Hidden = New Hidden()
        hiddenPrecioMaximoAutorizacion.ID = "hdnPrecioMaximoAutorizacion" & idRegistro
        hiddenPrecioMaximoAutorizacion.Value = informacionLineaVenta.PrecioMaximoAutorizacion

        Dim hiddenPrecioCosto As Ext.Net.Hidden = New Hidden()
        hiddenPrecioCosto.ID = "hdnPrecioCosto" & idRegistro
        hiddenPrecioCosto.Value = informacionLineaVenta.PrecioCosto

        Dim hiddenUltimaCantidad As Ext.Net.Hidden = New Hidden()
        hiddenUltimaCantidad.ID = "hdnCantidad" & idRegistro
        hiddenUltimaCantidad.Value = informacionLineaVenta.Cantidad

        Dim hiddenPrecio As Ext.Net.Hidden = New Hidden()
        hiddenPrecio.ID = "hdnPrecio" & idRegistro
        hiddenPrecio.Value = informacionLineaVenta.Precio

        Dim hiddenDescuento As Ext.Net.Hidden = New Hidden()
        hiddenDescuento.ID = "hdnDescuento" & idRegistro
        hiddenDescuento.Value = informacionLineaVenta.Descuento


        registroContainer.Items.Add(cajaCodigoArticulo)
        registroContainer.Items.Add(cajaNombreArticulo)
        registroContainer.Items.Add(comboUnidadMedida)
        registroContainer.Items.Add(cajaCantidad)
        registroContainer.Items.Add(comboFactor)
        registroContainer.Items.Add(hiddenFactores)
        registroContainer.Items.Add(cajaPrecio)
        registroContainer.Items.Add(cajaDescuento)
        registroContainer.Items.Add(cajaTotalConImpuesto)
        registroContainer.Items.Add(cajaComentarios)
        registroContainer.Items.Add(hiddenSujetoAImpuesto)
        registroContainer.Items.Add(hiddenUltimaCantidad)
        registroContainer.Items.Add(hiddenPrecio)
        registroContainer.Items.Add(hiddenDescuento)
        registroContainer.Items.Add(hiddenTotalLinea)
        registroContainer.Items.Add(hiddenPrecioMinimoAutorizacion)
        registroContainer.Items.Add(hiddenPrecioMaximoAutorizacion)
        registroContainer.Items.Add(hiddenPrecioCosto)
        PanelDetalleDocumento.Items.Add(registroContainer)
        registroContainer.Render()

    End Sub

    Private Sub UIFormularioClientesLectura()

        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim puedeModificarClientes As Boolean = controlUsuario.TieneAcceso(Session("AdminUserId"), "Clientes", enumTipoAccesoModulo.Modificar)

        btnCrearCliente.Hide()
        btnSeleccionarCliente.Show()
        If puedeModificarClientes Then
            btnGuardarCliente.Show()
        Else
            btnGuardarCliente.Hide()
        End If

        txtNombreCliente.Disabled = Not puedeModificarClientes
        txtApPaternoCliente.Disabled = Not puedeModificarClientes
        txtApMaternoCliente.Disabled = Not puedeModificarClientes
        txtNombreComercialCliente.Disabled = Not puedeModificarClientes
        txtRFCCliente.Disabled = Not puedeModificarClientes
        txtCalleCliente.Disabled = Not puedeModificarClientes
        txtNumExtCliente.Disabled = Not puedeModificarClientes
        txtNumIntCliente.Disabled = Not puedeModificarClientes
        txtColoniaCliente.Disabled = Not puedeModificarClientes
        txtCPCliente.Disabled = Not puedeModificarClientes
        txtMunicipioCliente.Disabled = Not puedeModificarClientes
        cmbEstadoCliente.Disabled = Not puedeModificarClientes
        txtTelCasaCliente.Disabled = Not puedeModificarClientes
        txtTelOficinaCliente.Disabled = Not puedeModificarClientes
        txtCorreoCliente.Disabled = Not puedeModificarClientes
        txtFechaNacimientoCliente.Disabled = Not puedeModificarClientes
    End Sub

    Private Sub UIFormularioClientesBusqueda()
        btnGuardarCliente.Hide()
        btnCrearCliente.Hide()
        btnSeleccionarCliente.Hide()
        hdnCodigoCliente.Value = String.Empty
        txtNombreCliente.Disabled = True
        txtNombreCliente.Value = String.Empty
        txtApPaternoCliente.Disabled = True
        txtApPaternoCliente.Value = String.Empty
        txtApMaternoCliente.Disabled = True
        txtApMaternoCliente.Value = String.Empty
        txtNombreComercialCliente.Disabled = True
        txtNombreComercialCliente.Value = String.Empty
        txtRFCCliente.Disabled = True
        txtRFCCliente.Value = String.Empty
        txtCalleCliente.Disabled = True
        txtCalleCliente.Value = String.Empty
        txtNumExtCliente.Disabled = True
        txtNumExtCliente.Value = String.Empty
        txtNumIntCliente.Disabled = True
        txtNumIntCliente.Value = String.Empty
        txtColoniaCliente.Disabled = True
        txtColoniaCliente.Value = String.Empty
        txtCPCliente.Disabled = True
        txtCPCliente.Value = String.Empty
        txtMunicipioCliente.Disabled = True
        txtMunicipioCliente.Value = String.Empty
        cmbEstadoCliente.Disabled = True
        cmbEstadoCliente.SelectedItem.Index = -1
        txtTelCasaCliente.Disabled = True
        txtTelCasaCliente.Value = String.Empty
        txtTelOficinaCliente.Disabled = True
        txtTelOficinaCliente.Value = String.Empty
        txtCorreoCliente.Disabled = True
        txtCorreoCliente.Value = String.Empty
        txtFechaNacimientoCliente.Disabled = True
        txtFechaNacimientoCliente.Value = DateTime.MinValue
    End Sub

    Private Sub mapearDeClienteAFormulario(ByVal cliente As Cliente)
        hdnCodigoCliente.Value = cliente.Codigo
        txtNombreCliente.Value = cliente.Nombre
        txtRFCCliente.Value = cliente.RFC
        txtApPaternoCliente.Value = cliente.ApPaterno
        txtApMaternoCliente.Value = cliente.ApMaterno
        txtNombreComercialCliente.Value = cliente.NombreComercial
        txtCalleCliente.Value = cliente.Calle
        txtNumExtCliente.Value = cliente.NumeroExterior
        txtNumIntCliente.Value = cliente.NumeroInterior
        txtColoniaCliente.Value = cliente.Colonia
        txtCPCliente.Value = cliente.CodigoPostal
        txtMunicipioCliente.Value = cliente.Municipio
        cmbEstadoCliente.Value = cliente.Estado
        txtTelCasaCliente.Value = cliente.TelCasa
        txtTelOficinaCliente.Value = cliente.TelOficina
        txtCorreoCliente.Value = cliente.Correo
        txtFechaNacimientoCliente.Value = cliente.FechaNacimiento
    End Sub

    Private Function mapearDeFormularioACliente() As Cliente
        Return New Cliente(
            hdnCodigoCliente.Value,
            Trim(txtNombreCliente.Value), Trim(txtApPaternoCliente.Value), Trim(txtApMaternoCliente.Value), Trim(txtNombreComercialCliente.Value), Trim(txtRFCCliente.Value).ToUpper(),
            Trim(txtCalleCliente.Value), Trim(txtNumExtCliente.Value), Trim(txtNumIntCliente.Value), Trim(txtColoniaCliente.Value), Trim(txtCPCliente.Value), Trim(txtMunicipioCliente.Value),
            cmbEstadoCliente.Value, Trim(txtTelCasaCliente.Value), Trim(txtTelOficinaCliente.Value), Trim(txtCorreoCliente.Value), txtFechaNacimientoCliente.Value)
    End Function

    Private Function validarInformacionCliente(ByRef defCliente As Cliente, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty
        Return defCliente.ValidarInformacionCapturaCliente(mensajeError)
    End Function

    <DirectMethod>
    Public Sub ActualizarCliente()
        Dim controClientes As ControlClientes = New ControlClientes()
        Dim clienteActualizar As Cliente = mapearDeFormularioACliente()
        Dim mensajeError As String = String.Empty

        If Not validarInformacionCliente(clienteActualizar, mensajeError) Then
            MessageBoxExtNet.ShowError(mensajeError)
            Return
        End If

        If controClientes.RFCDuplicado(clienteActualizar.RFC, clienteActualizar.Codigo) Then
            MessageBoxExtNet.ShowError("RFC ya se encuentra dado de alta")
            Return
        End If

        If Not controClientes.ActualizarCliente(clienteActualizar) Then
            MessageBoxExtNet.ShowError("Error al actualizar el cliente consulte con su administrador")
        End If
        controClientes.RegistrarLogCliente("ACTUALIZAR", Session("IDSTORE"), Session("AdminUserID"), clienteActualizar)

        MessageBoxExtNet.ShowSuccess("Cliente actualizado correctamente")

    End Sub

    <DirectMethod>
    Public Sub SeleccionarCliente()
        Dim controlCliente As ControlClientes = New ControlClientes()

        Dim cliente As Cliente = controlCliente.ObtenerCliente(hdnCodigoCliente.Value)
        cargarInformacionClienteEnFormularioVentas(cliente)
        winClientes.Hide()
    End Sub

    <DirectMethod>
    Public Sub CrearCliente()
        Dim controClientes As ControlClientes = New ControlClientes()
        Dim clienteCrear As Cliente = mapearDeFormularioACliente()
        Dim mensajeError As String = String.Empty

        If Not validarInformacionCliente(clienteCrear, mensajeError) Then
            MessageBoxExtNet.ShowError(mensajeError)
            Return
        End If

        If controClientes.RFCDuplicado(clienteCrear.RFC) Then
            MessageBoxExtNet.ShowError("RFC ya se encuentra dado de alta")
            Return
        End If

        If Not controClientes.CrearCliente(clienteCrear) Then
            MessageBoxExtNet.ShowError("Error al crear el cliente consulte con su administrador")
        End If

        controClientes.RegistrarLogCliente("CREAR", Session("IDSTORE"), Session("AdminUserID"), clienteCrear)
        MessageBoxExtNet.ShowSuccess("Cliente registrado correctamente")

        cargarInformacionClienteEnFormularioVentas(clienteCrear)
        winClientes.Hide()
    End Sub

    Private Sub cargarInformacionClienteEnFormularioVentas(ByRef cliente As Cliente)
        hdnCodigoClienteVenta.Value = cliente.Codigo
        If cliente.NombreComercial <> String.Empty Then
            txtNombreClienteVenta.Value = cliente.NombreComercial
        Else
            txtNombreClienteVenta.Value = cliente.NombreCompleto
        End If
        txtRFCVenta.Value = cliente.RFC
        txtDireccionVenta.Value = cliente.Direccion
    End Sub

    Public Sub BuscarCliente()

        Dim tipoBusqueda As String = cmbTipoBuquedaCliente.SelectedItem.Value
        Dim valorBusqueda As String = txtValorBusquedaCliente.Value

        If valorBusqueda = String.Empty Then
            MessageBoxExtNet.ShowWarning("Error", "Valor de busqueda no puede ir en blanco")
            Return
        End If

        Dim controlClientes As ControlClientes = New ControlClientes()
        Dim clientesEncontrados As List(Of Cliente) = controlClientes.BuscarCliente(tipoBusqueda, valorBusqueda)

        StoreClientes.DataSource = clientesEncontrados
        StoreClientes.DataBind()
        UIFormularioClientesBusqueda()

        If clientesEncontrados.Count = 0 Then
            Dim messageBox As Ext.Net.MessageBox = New MessageBox()
            messageBox.Confirm("Crear Usuario?", "No se encontraron clientes con el criterio de busqueda seleccionado.Crear?", New JFunction() With {.Fn = "SeConfirmoCreacionCliente"}).Show()
        End If

    End Sub

#End Region

#Region "Metodos Control Ventana Articulos"

    <DirectMethod>
    Public Function ControlPaginacionArticulos(accion As String, extraParams As Dictionary(Of String, Object)) As Object
        If hdnUltimoCodigoBuscado.Value = "|[NONE]|" Then
            Return Nothing
        End If

        Dim obtenerTotalRegistros As Boolean = False
        If hdnNuevaBusqueda.Value = "YES" Then
            hdnNuevaBusqueda.Value = "NO"
            obtenerTotalRegistros = True
        End If

        Dim codigoBusqueda As String = hdnUltimoCodigoBuscado.Value
        Dim nombreBusqueda As String = hdnUltimoNombreBuscado.Value

        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim codigoAlmaceSAP As String = controlTienda.ObtenerCodigoAlmacenSAP(Session("IDSTORE"))
        If codigoAlmaceSAP = Nothing Then
            MessageBoxExtNet.ShowError("Codigo almacen tienda no encontrado")
            Return Nothing
        End If

        Dim parametrosPaginacion As StoreRequestParameters = New StoreRequestParameters(extraParams)
        Dim totalArticulos As Integer = 0

        Dim sortParameter As String = "Codigo"
        Dim sortDirection As String = "ASC"
        If parametrosPaginacion.Sort.Length > 0 Then
            sortParameter = parametrosPaginacion.Sort(0).Property
            sortDirection = parametrosPaginacion.Sort(0).Direction.ToString()
        End If

        Dim pagina As Integer = parametrosPaginacion.Page
        If pagina = 0 Then
            pagina = 1
        End If

        Dim articulosEncontrados As List(Of Articulo) = controlArticulo.BuscarArticulo(((pagina - 1) * parametrosPaginacion.Limit) + 1, pagina * parametrosPaginacion.Limit, sortParameter, sortDirection, codigoBusqueda, nombreBusqueda, "Venta", Session("IDSTORE"), codigoAlmaceSAP, obtenerTotalRegistros, totalArticulos)
        If obtenerTotalRegistros Then
            hdnTotalRegistrosNuevaBusqueda.Value = totalArticulos
        Else
            totalArticulos = hdnTotalRegistrosNuevaBusqueda.Value
        End If
        Return New Paging(Of Articulo)(articulosEncontrados, totalArticulos)
    End Function

    Public Sub BuscarArticulos()

        Dim codigoBusqueda As String = txtCodigoArticuloBusqueda.Value
        Dim nombreBusqueda As String = txtNombreArticuloBusqueda.Value

        If codigoBusqueda Is Nothing Then
            codigoBusqueda = String.Empty
        End If

        If nombreBusqueda Is Nothing Then
            nombreBusqueda = String.Empty
        End If

        hdnUltimoCodigoBuscado.Value = codigoBusqueda
        hdnUltimoNombreBuscado.Value = nombreBusqueda
        hdnNuevaBusqueda.Value = "YES"

        StoreArticulos.LoadProxy()

    End Sub

#End Region


End Class