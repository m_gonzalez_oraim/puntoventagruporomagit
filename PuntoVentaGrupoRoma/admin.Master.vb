﻿Imports System.Data.SqlClient
Imports Ext.Net

Public Class admin
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        If Not Context.User.Identity.IsAuthenticated Then
            Response.Redirect("~/AdminLogin/Login.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

        Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        If Session("Guid") <> principalPV.UsuarioPuntoVenta.Guid Then
            Response.Redirect("~/AdminLogin/Login.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        Else
            Session("Guid") = principalPV.UsuarioPuntoVenta.Guid
        End If

        If principalPV.UsuarioPuntoVenta.StoreId = -1 Then
            If Request.Url.AbsolutePath.ToLower().EndsWith("default.aspx") OrElse
                Request.Url.AbsolutePath.ToLower().EndsWith("adminrole.aspx") OrElse
                Request.Url.AbsolutePath.ToLower().EndsWith("adminstore.aspx") OrElse
                Request.Url.AbsolutePath.ToLower().EndsWith("adminuser.aspx") OrElse
                Request.Url.AbsolutePath.ToLower().EndsWith("marquesina.aspx") Then
                Return
            Else
                Response.Redirect("~/Default.aspx")
            End If
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If Now.DayOfYear < 360 And Now.DayOfYear > 200 Then
        ' Check if logged in if not is Error.aspx and not is Server.Transfer (Executed only when error)
        ''If Right(Request.Url.AbsolutePath, 10).ToLower() <> "error.aspx" AndAlso Page.PreviousPage Is Nothing Then
        ' Check if User Still Logged In
        ''If Not CType(Session.Item("AdminUserID"), Integer) > 0 Then
        ''If Right(Request.Url.AbsolutePath, 10).ToLower() <> "login.aspx" Then
        '                    Response.Redirect("~/AdminLogin/Login.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        ''Response.Redirect("~/AdminLogin/Login.aspx")
        ''End If
        ''End If
        ''End If
        'Else
        'Response.Redirect("~/AdminLogin/Login.aspx")
        'End If

    End Sub
    ''Private Sub CheckUserAccess()
    'Try
    ' Get User and Page URL
    ''Dim AdminUserID As Integer = CType(Session.Item("AdminUserID"), Integer)
    ''Dim CurrentURL As String = Request.AppRelativeCurrentExecutionFilePath

    ' Check if User has Access to Page
    ''Dim Accessdtr As SqlDataReader = Nothing
    ''    p_DBConn.GetQuerydtr("CheckUserAccess " & AdminUserID & ", '" & CurrentURL & "'", Accessdtr)

    ' If no found Raise Exception
    ''    If Accessdtr.HasRows Then

    ' Read Info
    ''        Accessdtr.Read()

    ' Check if User Has permisions Over the Page
    ''        If IsDBNull(Accessdtr.Item("MenuOptionID")) Then
    ' User Has no Access to the page

    ' Close Data Reader
    ''            Accessdtr.Close()

    ' Raise Exception
    ''            Throw New HttpException(403, "Acceso No Permitido.", 403)
    ''        End If
    ''    End If

    ' Close Data Reader
    ''    Accessdtr.Close()
    ''    Accessdtr = Nothing

    'Catch ex As Exception
    '    ' Raise Exception
    '    Throw New HttpException(403, "Access Denied.", 403)

    'End Try

    ''End Sub
    Public Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)
        Dim defMenu As Dictionary(Of String, List(Of OpcionMenu)) = principalPV.UsuarioPuntoVenta.DefinicionMenu()

        For Each menuUsuario As KeyValuePair(Of String, List(Of OpcionMenu)) In defMenu
            Dim subMenuPrincial As MenuItem = New MenuItem()
            subMenuPrincial.Text = menuUsuario.Key

            Dim menuDelSubMenu As Menu = New Menu()
            For i As Integer = 0 To menuUsuario.Value.Count - 1
                Dim accionMenu As MenuItem = New MenuItem()
                accionMenu.Text = menuUsuario.Value(i).Opcion
                accionMenu.Listeners.Click.Handler = "window.location.pathname ='" & menuUsuario.Value(i).Url & "'"
                menuDelSubMenu.Add(accionMenu)
            Next
            subMenuPrincial.Menu.Add(menuDelSubMenu)
            MnuPrincipal.Add(subMenuPrincial)
        Next

        'Dim controlAcceso As ControlAcceso = New ControlAcceso()

        '' Show Menu if Could
        'Try
        '    ' Get Menu Info
        '    If Not Session("AdminUserID") Is Nothing Then

        '        Dim Menudtr As SqlDataReader = Nothing
        '        p_DBConn.GetQuerydtr("MenuOptionByUser " & CType(Session.Item("AdminUserID"), Integer), Menudtr)

        '        ' Menu Vars
        '        Dim TabItem As New MenuItem
        '        Dim OptionItem As MenuItem
        '        Dim TabName As String = "*"

        '        ' Check if Found Data
        '        If Menudtr.HasRows Then
        '            While Menudtr.Read()
        '                If Menudtr.Item("MenuTabName") <> TabName Then
        '                    If TabName <> "*" Then
        '                        ' Add Tab To Menu
        '                        mnuMain.Items.Add(TabItem)
        '                    End If
        '                    TabName = Menudtr.Item("MenuTabName")
        '                    TabItem = New MenuItem(Menudtr.Item("MenuTabName"), Menudtr.Item("MenuTabName"), "", "")
        '                    TabItem.Selectable = False
        '                End If
        '                OptionItem = New MenuItem(Menudtr.Item("OptionName"), Menudtr.Item("OptionName"), "", Menudtr.Item("OptionURL"))
        '                'Bloqueas opciones si no ha realizado el cierre del dia anterior


        '                TabItem.ChildItems.Add(OptionItem)
        '            End While

        '            ' Add Last Tab
        '            mnuMain.Items.Add(TabItem)

        '        End If
        '        Menudtr.Close()

        '        If Session("IDSTORE") Is Nothing Or Session("IDSTORE") < 1 Then
        '            If controlAcceso.EsSuperUsuario("AdminUserID") Then
        '                mnuMain.Enabled = True
        '            Else
        '                mnuMain.Enabled = False
        '            End If
        '        Else
        '            mnuMain.Enabled = True
        '        End If

        '    End If

        'Catch ex As Exception

        'End Try

        '' Set User name
        Dim contextUser As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        lblUserName.Text = contextUser.UsuarioPuntoVenta.UserName & " "
        lblStoreName.Text = contextUser.UsuarioPuntoVenta.StoreName

        '' Making All the pages no-cacheable
        Response.Expires = 0
        Response.ExpiresAbsolute = DateAdd(DateInterval.Day, -1, DateTime.Now())
        Response.AddHeader("pragma", "no-cache")
        Response.AddHeader("cache-control", "private")
        Response.CacheControl = "no-cache"

        '' Set SHORTCUT ICON
        Dim IconLink As New HtmlLink
        IconLink.Href = "~/images/sp.ico"
        IconLink.Attributes.Add("REL", "SHORTCUT ICON")
        Page.Header.Controls.Add(IconLink)
    End Sub

    <DirectMethod>
    Public Sub CerrarSesion()
        Session.RemoveAll()

        Response.Cookies.Remove("PuntoVentaBXP")
        If (Not Request.Cookies("PuntoVentaBXP") Is Nothing) Then
            Dim myCookie As HttpCookie
            myCookie = New HttpCookie("PuntoVentaBXP")
            myCookie.Expires = DateTime.Now.AddDays(-1D)
            Response.Cookies.Add(myCookie)
        End If
        Response.Redirect("~/AdminLogin/Login.aspx")
    End Sub

End Class