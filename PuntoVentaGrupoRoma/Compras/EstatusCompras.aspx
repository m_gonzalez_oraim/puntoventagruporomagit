﻿<%@ Page Language="vb" MasterPageFile="~/admin.master" AutoEventWireup="false" CodeBehind="EstatusCompras.aspx.vb" Inherits="PuntoVentaGrupoRoma.EstatusCompras" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <script type="text/javascript">
        var AfterEdit = function (e) {
            e.grid.store.commitChanges()
        };

        var validate = function (e) {
            e.value = null;
        };
    </script>
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
       <%-- ****************INICIA SQL DATA SOURCES*******************--%>
    <asp:SqlDataSource ID="SqlPurchaseOrderSAP" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DBConn  %>" SelectCommand="">
         <SelectParameters>
            <asp:SessionParameter Name="WHSID" SessionField="WHSID" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlPurchaseOrderDetailSAP" runat="server"  
        ConnectionString="<%$ ConnectionStrings:DBConn %>" SelectCommand="" >
        <SelectParameters>
            <asp:Parameter Name="id" Type="Int32" DefaultValue="0" >
            </asp:Parameter>
        </SelectParameters>
    </asp:SqlDataSource>
 
   <%-- ****************TERMINA SQL DATA SOURCES*******************--%>
    <table style="width: 99%;">
        <tr>
            <td>
                 <ext:Store ID="StoreCmb" runat="server">
                    <Model>
                        <ext:Model runat="server">
                            <Fields>
                                <ext:ModelField Name="AdminStoreID" />
                                <ext:ModelField Name="StoreName" />
                                <ext:ModelField Name="WhsId" /> 
                            </Fields>
                        </ext:Model>
                    </Model>            
                </ext:Store>
    
                <ext:GridPanel ID="gpPurchaseOrder" runat="server" Title="Órdenes de Compra SAP" Collapsed="false"
                    Collapsible="true" Layout="fit"  AutoHeight="true"  Icon="Cart">
                    <Store>
                        <ext:Store ID="StorePurchaseOrderSAP" runat="server" DataSourceID="SqlPurchaseOrderSAP"
                            SerializationMode="Simple">
                            <Model>
                                <ext:Model runat="server" IDProperty="id">
                                    <Fields>                		
                                        <ext:ModelField Name="id" />
                                        <ext:ModelField Name="OCPOS" />
                                        <ext:ModelField Name="TipoCompra" />
                                        <ext:ModelField Name="Fecha" />
                                        <ext:ModelField Name="Importado" />
                                        <ext:ModelField Name="DocSAP" />
                                        <ext:ModelField Name="Total" />                            
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store> 
                    <View>
                        <ext:GridView ID="GridView1" runat="server" AutoFill="true" />
                    </View>
                    <ColumnModel DefaultSortable="True">
                        <Columns>  
                            <ext:Column runat="server" Align="Center" DataIndex="id" Header="id" Hidden="true" >
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="OCPOS" Header="OC. POS / Referencia SAP"  >
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="TipoCompra" Header="Tipo de Compra">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="Fecha" Header="Fecha" >                    
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="Importado" Header="Importado" >
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="DocSAP" Header="No. Doc. SAP" >
                            </ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="Total" Header="Total">
                            </ext:Column>                
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel  runat="server" SingleSelect="true">
                            <Listeners>
                                <Select Handler="#{StorePurchaseOrderDetailSAP}.reload();" Buffer="250" />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolBar1" runat="server" PageSize="6" />
                    </BottomBar>
                </ext:GridPanel>
    
                <ext:GridPanel  ID="gpPurchaseOrderDetail" runat="server"  Title="Detalles"  Layout="fit" 
                 Icon="CartMagnify" AutoExpandColumn="ID"  AutoHeight="true" Collapsible="true" AnimCollapse="true">
                    <Store>
                        <ext:Store runat="server" ID="StorePurchaseOrderDetailSAP" 
                             OnReadData="StorePurchaseOrderDetail_Refresh">
                            <Model>
                                <ext:Model runat="server" IDProperty="Linea">
                                    <Fields>  
                                        <ext:ModelField Name="id" />
                                        <ext:ModelField Name="Linea" />
                                        <ext:ModelField Name="Articulo" />
                                        <ext:ModelField Name="Cantidad" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                            <Parameters>
                                <ext:StoreParameter 
                                    Name="id" 
                                    Value="#{gpPurchaseOrder}.getSelectionModel().hasSelection() ? #{gpPurchaseOrder}.getSelectionModel().getSelection()[0].data.id : -1"
                                    Mode="Raw">
                                </ext:StoreParameter>
                            </Parameters>
                            <Listeners>
                                <Exception Handler="Ext.Msg.alert('Productos - Error de lectura.', e.message || response.statusText);" />
                            </Listeners>
                        </ext:Store>
                    </Store>
                    <View>
                        <ext:GridView ID="GridView2" runat="server" AutoFill="true" />
                    </View>
                    <ColumnModel runat="server">
                        <Columns> 
                            <ext:Column runat="server" Align="Center" DataIndex="ID" Header="Orden de Compra"></ext:Column>
                            <ext:Column runat="server" Align="Left" DataIndex="Linea" Header="# Partida"></ext:Column>
                            <ext:Column runat="server" Align="Left" DataIndex="Articulo" Header="Modelo"></ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="Cantidad" Header="Cantidad"></ext:Column>
                        </Columns>
                    </ColumnModel>         
                    <BottomBar>        
                        <ext:PagingToolbar ID="PagingToolBar2" runat="server" PageSize="6" />           
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel runat="server" SingleSelect="true">
                        </ext:RowSelectionModel>
                    </SelectionModel> 
                    <Listeners>
                        <Expand Handler="#{StorePurchaseOrderDetailSAP}.reload();" />
                        
                    </Listeners>
                </ext:GridPanel>  
            </td>
        </tr>
    </table>
</asp:Content>
