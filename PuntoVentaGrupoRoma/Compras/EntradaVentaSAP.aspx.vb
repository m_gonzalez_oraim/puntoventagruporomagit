﻿Imports Ext.Net
Imports System.IO
Imports System.Xml

Public Class EntradaVentaSAP
    Inherits System.Web.UI.Page

    Private mensaje As String = ""
    Private connstringWEB As String
    Private connstringSAP As String
    Private oDB As New DBMaster

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("AdminUserID") > 0 And Session("IDSTORE") > 0 Then
            If Not Ext.Net.X.IsAjaxRequest Then
                If Session("STORETYPEID") = 3 Then
                    Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
                End If
                Dim CONSTRING As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
                Dim INITIALCATALOG = CONSTRING.Split(";")(1).Split("=").Clone(1) & ".DBO.OITM"

                Dim sQuery As String
                connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

                sQuery = _
               "SELECT" & vbCrLf & _
               "    OINV.DocEntry," & vbCrLf & _
               "    NNM1.SeriesName as Series, " & vbCrLf & _
               "    OINV.DocNum," & vbCrLf & _
               "    CONVERT(VARCHAR, OINV.DocDate, 103) AS FechaContabilizacion, " & vbCrLf & _
               "    CONVERT(VARCHAR, OINV.DocDueDate, 103) AS FechaEntrega," & vbCrLf & _
               "    OINV.NumAtCard," & vbCrLf & _
               "    OINV.CardCode, " & vbCrLf & _
               "    OINV.CardName," & vbCrLf & _
               "    OINV.Comments" & vbCrLf & _
               "FROM    OINV INNER JOIN" & vbCrLf & _
               "        NNM1 ON OINV.Series = NNM1.Series" & vbCrLf & _
               "WHERE     (OINV.DocStatus = 'O') and OINV.CardCode = '" & Session("DEFAULTCUSTOMER") & "' " & vbCrLf & _
               "and OINV.DocEntry in " & vbCrLf & _
               "(" & vbCrLf & _
               "	SELECT  distinct  INV1.DocEntry  " & vbCrLf & _
               "	FROM   INV1 INNER JOIN " & vbCrLf & _
               "			   OITM ON INV1.ItemCode = OITM.ItemCode LEFT JOIN " & vbCrLf & _
               "			   " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.EntradaOCDet EOD ON INV1.DocEntry=EOD.NoDocSBO AND INV1.LineNum=EOD.LineaSBO " & vbCrLf & _
               "	WHERE  inv1.U_TIEPOS = " & Session("IDSTORE") & " AND" & vbCrLf & _
               "	(INV1.OpenQty - ISNULL(EOD.Cantidad,0)) -" & vbCrLf & _
               "	(Select isnull(SUM(COD.Cantidad),0) from " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.CancelacionOCDet COD where COD.NoDocSBO=INV1.DocEntry and COD.LineaSBO=inv1.LineNum) > 0 " & vbCrLf & _
               ")" & vbCrLf & _
               "and OINV.DocEntry in (select distinct DocEntry  from inv1 where inv1.U_TIEPOS = " & Session("IDSTORE") & ")" & vbCrLf & _
               "ORDER BY OINV.CardCode, OINV.DocDate DESC "

                SqlPurchaseOrderSAP.SelectCommand = sQuery
            End If
        Else
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

    End Sub


    Protected Sub StorePurchaseOrderDetail_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim id As String = e.Parameters("DocEntry").ToString
        Dim sQuery As String
        Dim dt As New DataTable
        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        Session("PURCHASEORDERIDDIR") = id
        sQuery = " select CardCode, DocdueDate from OINV where DocEntry ='" & id & "'    "
        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "CheckData", connstringSAP)
        If dt.Rows.Count > 0 Then

            For Each Drow As DataRow In dt.Rows
                Session("PURCHASEORDERFECHA") = Drow.Item("DocdueDate")
                Session("PURCHASEORDERCARDCODE") = Drow.Item("CardCode")
            Next

        End If

        If UpdateDetailGrid(id) Then
            'msgConfig.Buttons = MessageBox.Button.OK
            'msgConfig.Icon = MessageBox.Icon.INFO
            'msgConfig.Title = "RESULTADO"
            'msgConfig.Message = mensaje
        Else
            'msgConfig.Buttons = MessageBox.Button.OK
            'msgConfig.Icon = MessageBox.Icon.INFO
            'msgConfig.Title = "ERROR"
            'msgConfig.Message = mensaje
        End If
        'Ext.Net.X.Msg.Show(msgConfig)

    End Sub


    Private Function UpdateDetailGrid(ByVal id As Integer) As Boolean

        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dtDetail As New DataTable
        Dim newDtDetail As New DataTable
        Dim Data As Object
        Dim Data2 As Object
        Dim sQuery As String
        Dim IMEI As String
        Dim ICC As String
        Dim DN As String
        Dim CODEBARS As String
        Dim DESCRIPCION As String
        Dim TIPOARTICULO As Integer
        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        sQuery = _
        " SELECT    INV1.DocEntry, INV1.LineNum, INV1.ItemCode, INV1.WhsCode, INV1.Dscription, INV1.Quantity, INV1.Price,  " & vbCrLf & _
        "           (INV1.OpenQty - ISNULL(EOD.Cantidad,0)) -" & vbCrLf & _
        "           (Select isnull(SUM(COD.Cantidad),0) from " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.CancelacionOCDet COD where COD.NoDocSBO=INV1.DocEntry and COD.LineaSBO=inv1.LineNum)" & vbCrLf & _
        "           AS OpenQty, INV1.LineStatus, " & vbCrLf & _
        "           (INV1.OpenQty - ISNULL(EOD.Cantidad,0)) - " & vbCrLf & _
        "           (Select isnull(SUM(COD.Cantidad),0) from " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.CancelacionOCDet COD where COD.NoDocSBO=INV1.DocEntry and COD.LineaSBO=inv1.LineNum) AS INGRESAR" & vbCrLf & _
        ",(Select isnull(SUM(COD.Cantidad),0) from " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.CancelacionOCDet COD where COD.NoDocSBO=INV1.DocEntry and COD.LineaSBO=inv1.LineNum) AS QTYCANCELADA" & vbCrLf & _
        "FROM   INV1 INNER JOIN  " & vbCrLf & _
        "           OITM ON INV1.ItemCode = OITM.ItemCode LEFT JOIN " & vbCrLf & _
        "           " & ConfigurationManager.AppSettings("DatabasePV") & ".DBO.EntradaOCDet EOD ON INV1.DocEntry=EOD.NoDocSBO AND INV1.LineNum=EOD.LineaSBO " & vbCrLf & _
        "WHERE     (INV1.DocEntry = '" & id & "') " & vbCrLf & _
        "ORDER BY OITM.CodeBars "
        dtDetail = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "DataGrid", connstringSAP)


        Me.StorePurchaseOrderDetailSAP.DataSource = dtDetail
        Me.StorePurchaseOrderDetailSAP.DataBind()
        'End If
        UpdateDetailGrid = True
        mensaje = counter & " Partidas en la orde de compra."

    End Function

    <DirectMethod()> _
    Protected Sub AfterEdit(ByVal myObj As Object)
        Me.gpPurchaseOrderDetail.Store.Primary.CommitChanges()
    End Sub

    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        Dim grid2Json As String = e.ExtraParams("Grid2")
        Dim grid3Json As String = e.ExtraParams("Grid3")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        Dim grid2Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid2Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim grid2Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid2Json)
        Dim row
        Dim row2
        Dim rowHeader
        'Separar renglones por Saltos de línea  
        Dim INGRESAR As Integer
        Dim OPENQTY As Integer
        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String
        Dim Almacen As String
        Dim numeroEntradas As Integer
        Dim contadorLineaArticulos As Integer = 0
        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
        'Se verifica que el grid con el detalle de la orden de compra contenga datos
        If grid2Data.Length > 0 Then
            If grid1Data.Length = 0 Then
                mensaje = "Debe Seleccionar una Factura"
            Else

                For Each row3 In grid2Data
                    INGRESAR = row3.Item("INGRESAR")
                    OPENQTY = row3.Item("OpenQty")
                    'Revisamos los articulos estan creados en WEB
                    sQuery = "select IdArticulo from articulos " & vbCrLf & _
                             "where ArticuloSBO='" & row3.Item("ItemCode") & "'   "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Articulos", connstringWEB)
                    If dt.Rows.Count = 0 Then
                        'Si no hay registros lo insertamos
                        sQuery = "INSERT INTO Articulos (ArticuloSBO) " & vbCrLf & _
                            "VALUES ('" & row3.Item("ItemCode") & "')"
                        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Articulos", connstringWEB)
                    End If
                    If INGRESAR >= 1 Then
                        numeroEntradas += 1
                    End If
                    If INGRESAR > OPENQTY Then
                        mensaje = "La cantidad a devolver no puede ser mayor que la cantidad abierta. Modelo: " & row3.Item("Dscription")
                        Return False
                    End If
                Next

                If numeroEntradas = 0 Then
                    mensaje = "La cantidad a ingresar debe ser al menos de 1. "
                    Return False
                End If


                Almacen = Session("WHSID")
                Dim DocEntrySBO As Integer
                Dim FechaSBO As String
                Dim FechaEnWeb As String
                Dim SNSBO As String
                Dim IDUsuario As Integer = Session("AdminUserID")
                Dim TipoEntrada As String = "OC"
                Dim IdEntradaWEB As Integer
                counter = 0
                sQueryInsert = ""
                For Each row3 In grid2Data
                    'Se verifica que cada codeBar del ENTRADA este en el grid
                    'Si alguno no esta en el grid se regresa False

                    Dim IDCategoria As Integer = 0
                    counter = counter + 1

                    INGRESAR = row3.Item("INGRESAR")
                    OPENQTY = row3.Item("OpenQty")

                    'Creamos el documento de entrada en web
                    'Creamos primero el encabezado, solo se crea una vez
                    If counter = 1 Then
                        DocEntrySBO = grid1Data(0).Item("DocEntry")
                        FechaSBO = Format(CDate(grid1Data(0).Item("FechaEntrega")), "dd/MM/yyyy")
                        FechaEnWeb = Format(Now(), "dd/MM/yyyy")
                        SNSBO = grid1Data(0).Item("CardCode")
                        sQuery = "set dateformat dmy INSERT INTO EntradasOC (NoDocSBO,FechaSBO,FechaEntWeb,SNSBO,Status, IDUsuario, TipoEntrada) " & vbCrLf & _
                        "VALUES (" & DocEntrySBO & ",'" & FechaSBO & "',GETDATE(),'" & SNSBO & "','O','" & IDUsuario & "','" & TipoEntrada & "')"
                        'oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EntradasOC", connstringWEB)
                        If oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB, mensaje) = 2 Then
                            mensaje = "Se encontro un problema al procesar la entrada por favor intente de nuevo. " & mensaje
                            Return False
                        Else
                            'Obtenemos el id del documento de entrada creado en web
                            sQuery = "SELECT   top 1  IDEntrada " & vbCrLf & _
                            "FROM         EntradasOC order by IDEntrada desc "
                            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EntradasOCLastID", connstringWEB)
                            IdEntradaWEB = dt.Rows(0).Item("IDEntrada")
                        End If
                    End If


                    'buscamos id web del articulo
                    sQuery = "select top 1 IDArticulo from Articulos where ArticuloSBO = '" & row3.Item("ItemCode") & "' "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EntradasOCLastID", connstringWEB)

                    'llenamos insert de partidas
                    sQueryInsert &= "INSERT INTO EntradaOCDet (IDEntrada, NoDocSBO, Linea, Articulo, Cantidad, IDTienda, Status, LineaSBO) " & vbCrLf & _
                        "VALUES ('" & IdEntradaWEB & "','" & DocEntrySBO & "','" & counter & "','" & dt.Rows(0).Item("IDArticulo") & "','" & INGRESAR & _
                        "'," & Session("IDSTORE") & ",'O'," & row3.Item("LineNum") & ")" & vbCrLf

                Next

                oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB, mensaje)

                sQuery = "" & _
                            "SELECT      Articulo, Cantidad  " & vbCrLf & _
                            "FROM         EntradaOCDet " & vbCrLf & _
                            "where IDEntrada = " & IdEntradaWEB & " " & vbCrLf & _
                            " "
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "CheckInv", connstringWEB)
                sQuery = ""
                sQueryInsert = ""
                If dt.Rows.Count > 0 Then
                    For Each drow As DataRow In dt.Rows
                        'inventario tienda
                        sQuery = "SELECT     IDInv, IDArticulo, Cantidad, Almacen FROM Inventarios " & vbCrLf & _
                        "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & Session("IDSTORE") & "') " & vbCrLf
                        dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                        If dt2.Rows.Count > 0 Then
                            'Si encontramos registro actualizamos la cantidad
                            For Each drow2 As DataRow In dt2.Rows
                                sQueryInsert &= "UPDATE Inventarios SET Cantidad = Cantidad + " & drow.Item("Cantidad") & " " & vbCrLf & _
                                 "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & Session("IDSTORE") & "')  " & vbCrLf
                                Exit For
                            Next
                        Else
                            Dim sQueryInsertFirstInventario As String
                            'insertamos el articulo en inventarios con el nuevo almacen
                            sQueryInsertFirstInventario = " Insert into Inventarios (IDArticulo, Cantidad, Almacen) " & vbCrLf & _
                            " values (" & drow.Item("Articulo") & "," & drow.Item("CANTIDAD") & ",'" & Session("IDSTORE") & "' )" & vbCrLf
                            dt = oDB.EjecutaQry_Tabla(sQueryInsertFirstInventario, CommandType.Text, "Inventario", connstringWEB)
                        End If

                        'inventario consignacion
                        sQuery = "SELECT     IDInv, IDArticulo, Cantidad, Almacen FROM Inventarios " & vbCrLf & _
                       "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & Session("WHSCONSIGID") & "') " & vbCrLf
                        dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                        If dt2.Rows.Count > 0 Then
                            'Si encontramos registro actualizamos la cantidad
                            For Each drow2 As DataRow In dt2.Rows
                                sQueryInsert &= "UPDATE Inventarios SET Cantidad = Cantidad - " & drow.Item("Cantidad") & " " & vbCrLf & _
                                 "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & Session("WHSCONSIGID") & "')  " & vbCrLf
                                Exit For
                            Next
                        Else
                            Dim sQueryInsertFirstInventario As String
                            'insertamos el articulo en inventarios con el nuevo almacen
                            sQueryInsertFirstInventario = " Insert into Inventarios (IDArticulo, Cantidad, Almacen) " & vbCrLf & _
                            " values (" & drow.Item("Articulo") & "," & drow.Item("CANTIDAD") * -1 & ",'" & Session("WHSCONSIGID") & "' )" & vbCrLf
                            dt = oDB.EjecutaQry_Tabla(sQueryInsertFirstInventario, CommandType.Text, "Inventario", connstringWEB)
                        End If
                    Next
                    If oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB) = 2 Then
                        mensaje = "Operación realizada con Éxito."
                    Else
                        mensaje = "Operación realizada con Éxito."
                    End If
                End If
                If mensaje = "Operación realizada con Éxito." Then
                    Return True
                End If

            End If
        Else
            mensaje = "Seleccione una Factura antes de crear la entrada"
        End If


    End Function


    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "RESULTADO"
            msgConfig.Message = mensaje
            Me.gpPurchaseOrderDetail.Reload()
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
        End If
        Ext.Net.X.Msg.Show(msgConfig)
    End Sub

End Class