﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml

Public Class Compras
    Inherits System.Web.UI.Page

    Private connstringWEB As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    Private connstringSAP As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
    Private oDB As New DBMaster
    Private sQuery As String
    Private dt As New DataTable
    Private tipoMensaje As String
    Private mensaje As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("AdminUserID") > 0 And Session("IDSTORE") > 0 Then

            If Not Ext.Net.X.IsAjaxRequest Then

                If Session("STORETYPEID") = 3 Then
                    Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
                End If

                Try
                    sQuery = "SELECT  ADS.AdminStoreID, ADS.StoreName,ADS.WhsId " & vbCrLf & _
                            "FROM AdminStore ADS " & vbCrLf & _
                            "     INNER JOIN UserStores US " & vbCrLf & _
                            "     ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                            "     and US.AdminUserID = '" & Session("AdminUserID") & "'  " & vbCrLf & _
                            ""
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringWEB)
                    StoreCmb.DataSource = dt
                    StoreCmb.DataBind()
                    ComboBox1.DataBind()

                    sQuery = "select ItmsTypCod,ItmsGrpNam from OITG WHERE ItmsTypCod IN (1,2,3,4,5,6,7)"
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                    StoreArticulo.DataSource = dt
                    StoreArticulo.DataBind()
                    cmbArticulo.DataBind()
                Catch ex As Exception
                    tipoMensaje = "Error"
                    mensaje = ex.Message
                    Mensajes(tipoMensaje, mensaje)
                End Try
            End If
        Else
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If
    End Sub

    Public Sub CambiaArticulo()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If
            sQuery = "select distinct U_BXP_MARCA as CODE, U_BXP_MARCA as NAME from OITM " & vbCrLf & _
                "where QryGroup" & Value & "='Y' and frozenFor='N' order by U_BXP_MARCA"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreLinea.DataSource = dt
                StoreLinea.DataBind()
                cmbLinea.DataBind()
                'cmbLinea.SetInitValue("")

                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                tipoMensaje = "Error"
                mensaje = "No existen Lineas para esta categoria de articulo '" & DirectCast(Ext.Net.ExtNet.GetCtl("CMBArticulo"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)

            Else
                StoreLinea.DataSource = dt
                StoreLinea.DataBind()
                cmbLinea.DataBind()
                'cmbLinea.SetInitValue(dt.Rows.Item(0).Item(0))
                'cmbLinea.SetInitValue("")

                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

            End If

            ArtName.Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Text
            ' cmbLinea.Render()
        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True

        End Try
    End Sub

    Public Sub CambiaLinea()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If
            sQuery = "select distinct U_BXP_MEDIDA as CODE, U_BXP_MEDIDA as NAME from OITM " & vbCrLf & _
            "where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value & "='Y' and U_BXP_MARCA='" & Value & "'"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""

                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                tipoMensaje = "Error"
                mensaje = "No existen medidas para esta línea '" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)
            Else
                StoreMedida.DataSource = dt
                StoreMedida.DataBind()
                cmbMedida.DataBind()
                'cmbMedida.SetInitValue("")

                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
               ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True

        End Try
    End Sub

    Public Sub CambiaMedida()
        Try
            Dim Value As String
            Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbMedida"), ComboBox).SelectedItem.Value
            If Value = "" Then
                Exit Sub
            End If
            sQuery = "select distinct ItemCode as CODE, ItemName as NAME from OITM " & vbCrLf & _
            "where QryGroup" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbArticulo"), ComboBox).SelectedItem.Value & "='Y' " & vbCrLf & _
            "and U_BXP_MARCA='" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbLinea"), ComboBox).SelectedItem.Value & "' and U_BXP_MEDIDA='" & Value & "'" & vbCrLf & _
            "ORDER BY ITEMNAME "
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
            If dt.Rows.Count = 0 Then
                sQuery = "select TOP 0 'CODE', 'NAME' " & vbCrLf & _
                ""
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringSAP)
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")

                tipoMensaje = "Error"
                mensaje = "No existen modelos para esta Medida '" & DirectCast(Ext.Net.ExtNet.GetCtl("cmbMedida"), ComboBox).SelectedItem.Text & "'"
                Mensajes(tipoMensaje, mensaje)
            Else
                StoreModelo.DataSource = dt
                StoreModelo.DataBind()
                cmbModelo.DataBind()
                'cmbModelo.SetInitValue("")
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        Finally
            btnAgregar.Disabled = True
        End Try
    End Sub

    Public Sub CambiaModelo()
        Try
            Cantidad.Disabled = False
            Cantidad.Text = 1
            btnAgregar.Disabled = False
            ItemName.Value = DirectCast(Ext.Net.ExtNet.GetCtl("cmbModelo"), ComboBox).SelectedItem.Text
        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try
    End Sub

    Private Function Mensajes(ByVal Tittle As String, ByVal Html As String) As Boolean
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-exclamation"
        nconf.Html = Html
        nconf.Title = Tittle
        msg.Configure(nconf)
        msg.Show()
    End Function

    Protected Sub UploadClick(ByVal sender As Object, ByVal e As DirectEventArgs)

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()
        If checkFile(e) Then
            'Habilitamos el botón de Transferencias 
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "RESULTADO"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
            Me.GridPanel1.ClearContent()
            Response.Redirect("../printControl/imprimeventa.aspx")
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
            Ext.Net.X.Msg.Show(msgConfig)
        End If

    End Sub

    Private Function checkFile(ByVal e As DirectEventArgs) As Boolean

        'JSON representation
        Dim grid1Json As String = e.ExtraParams("Grid1")
        'XML representation
        Dim grid1Xml As XmlNode = JSON.DeserializeXmlNode("{records:{record:" & grid1Json & "}}")
        'array of Dictionaries
        Dim grid1Data As Dictionary(Of String, String)() = JSON.Deserialize(Of Dictionary(Of String, String)())(grid1Json)
        Dim row
        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim sQuery As String
        Dim sQueryInsert As String = ""
        Dim sQueryInsert2 As String = ""
        Dim IDTienda As Integer
        Dim IDUsuario As Integer = Session("AdminUserID")
        Dim IdCompraWEB As Integer
        Dim ret As Boolean

        Try
            'Se verifica que el grid con el detalle de la orden de compra contenga datos
            If grid1Data.Length > 0 Then
                If Session("IDSTORE") < 1 Then
                    mensaje = "Seleccione una Tienda antes de Proceder"
                Else

                    IDTienda = Session("IDSTORE")

                    'Revisamos si los articulos estan creados en web
                    For Each row In grid1Data
                        Dim codeBarCount As Integer = 0
                        Dim IDCategoria As Integer = 0
                        Dim ArticuloSBO As String = ""
                        Dim DescripcionSBO As String = ""
                        counter = counter + 1

                        ArticuloSBO = row.item("Modelo")
                        DescripcionSBO = row.item("ItemName")

                        'Revisamos si el artículo esta creado en WEB
                        sQuery = "select IdArticulo from articulos " & vbCrLf & _
                                 "where ArticuloSBO='" & ArticuloSBO & "' "
                        dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Articulos", connstringWEB)
                        If dt.Rows.Count = 0 Then
                            'Si no hay registros lo insertamos
                            sQuery = "INSERT INTO Articulos (ArticuloSBO) " & vbCrLf & _
                                "VALUES ('" & ArticuloSBO & "')"
                            ret = oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB)
                        End If

                        'Creamos el documento de Compra en web
                        'Creamos primero el encabezado, solo se crea una vez
                        If counter = 1 Then
                            For Each rowHeader In grid1Data
                                Dim fechaEnvio As String
                                fechaEnvio = Format(Now(), "dd/MM/yyyy").ToString
                                sQuery = "set dateformat dmy INSERT INTO CompraEncabezado (FechaComWeb, Status, IDUsuario) " & vbCrLf & _
                                "VALUES (getdate(),'O','" & IDUsuario & "')"
                                oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB)
                                'Obtenemos el id del documento de Compra creado en web
                                sQuery = "SELECT   top 1  IDCompra " & vbCrLf & _
                                "FROM         CompraEncabezado order by IDCompra desc "
                                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "ComprasOCLastID", connstringWEB)
                                For Each Drow As DataRow In dt.Rows
                                    IdCompraWEB = Drow.Item("IDCompra")
                                    Exit For
                                Next
                                Exit For
                            Next
                        End If

                        sQueryInsert &= "INSERT INTO CompraDetalle (IDCompra, Linea, Articulo, Cantidad, IDTienda, Status,DefaultList) " & vbCrLf & _
                             "VALUES ('" & IdCompraWEB & "','" & counter & "',(Select IDArticulo from articulos where ArticuloSBO ='" & ArticuloSBO & "'),'" & row.item("Cantidad") & _
                             "','" & IDTienda & "','O','" & Session("DEFAULTLIST") & "')" & vbCrLf


                    Next
                    'Si llegamos a este punto podemos crear el detalle de la Compra
                    ret = oDB.EjecutaQry(sQueryInsert, CommandType.Text, connstringWEB)


                    Dim contadorDetOC As Integer = 0
                    sQuery = "" & _
                            "SELECT      Articulo, Cantidad, IDTienda  " & vbCrLf & _
                            "FROM         CompraDetalle " & vbCrLf & _
                            "where IDCompra = " & IdCompraWEB & " " & vbCrLf & _
                            " "
                    dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "CheckSeries", connstringWEB)
                    sQuery = ""
                    sQueryInsert = ""
                    If dt.Rows.Count > 0 Then
                        For Each drow As DataRow In dt.Rows
                            contadorDetOC += 1
                            sQuery = "SELECT     IDInv, IDArticulo, Cantidad, almacen FROM Inventarios " & vbCrLf & _
                            "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (IDTienda = '" & IDTienda & "') " & vbCrLf
                            dt2 = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Inventarios", connstringWEB)
                            If dt2.Rows.Count > 0 Then
                                'Si encontramos registro actualizamos la cantidad
                                For Each drow2 As DataRow In dt2.Rows
                                    sQueryInsert2 &= "UPDATE Inventarios SET Cantidad = Cantidad + " & drow.Item("Cantidad") & "" & vbCrLf & _
                                     "WHERE     (IDArticulo = '" & drow.Item("Articulo") & "') AND (Almacen = '" & IDTienda & "')  " & vbCrLf
                                    Exit For
                                Next
                            Else
                                'Si no hay registros insertamos uno nuevo 
                                sQueryInsert2 &= "INSERT INTO Inventarios (IDArticulo, Cantidad, Almacen) " & vbCrLf & _
                                "VALUES ('" & drow.Item("Articulo") & "','" & drow.Item("Cantidad") & "','" & IDTienda & "')" & vbCrLf

                            End If
                        Next
                        'ret = oDB.EjecutaQry(sQueryInsert2, CommandType.Text, connstringWEB)
                        mensaje = "Operación realizada con Éxito."
                        InsertaLog(Tipos.CO, IdCompraWEB, Session("IDSTORE"), Session("WHSID"))
                    End If
                End If
                Session("POSTBACK") = "Compras"
                Session("IDPRINTVENTA") = IdCompraWEB
                Return True
            Else
                mensaje = "Debe capturar al menos un modelo para realizar la Compra."
            End If

        Catch ex As Exception
            tipoMensaje = "Error"
            mensaje = ex.Message
            Mensajes(tipoMensaje, mensaje)
        End Try


    End Function

    Protected Sub DisableClick(ByVal sender As Object, ByVal e As DirectEventArgs)
        btnAgregar.Disabled = True
    End Sub

End Class