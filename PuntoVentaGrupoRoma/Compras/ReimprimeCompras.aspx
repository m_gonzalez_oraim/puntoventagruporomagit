﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReimprimeCompras.aspx.vb" Inherits="PuntoVentaGrupoRoma.ReimprimeCompras" MasterPageFile="~/admin.master"%>
<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

     <style type="text/css">                
       
            .x-grid-custom .x-grid3-row TD{                        
                height: 50px;
            } 
        
             .x-grid-custom .x-grid3-row-alt{
               background-color: #DAE2E8;           
            }
       
        </style>

    <ext:GridPanel ID="gridPanel1" IDMode="Explicit" runat="server" AutoHeight="true" Title="Reporte de entradas por Compra" Cls="x-grid-custom" DisableSelection="true"
            Layout="fit" >
        <topbar>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:ComboBox   
                        ID="cmbTiendas" 
                        IDMode="Explicit" 
                        runat="server" 
                        AllowBlank="true" 
                        Width="150" 
                        LabelAlign="Top"
                        LabelWidth="60"  
                        DisplayField="StoreName"
                        FieldLabel="Tienda" 
                        ValueField="WHSID"
                        TypeAhead="true" 
                        Mode="Default" 
                        TriggerAction="All"
                        EmptyText="Filtrar por tienda..."  >
                        <ListConfig>
                            <ItemTpl ID="ctl13" IDMode="Explicit"></ItemTpl>
                        </ListConfig>
                        <Store>
                            <ext:Store ID="storeStores"  runat="server">
                                <Model>
                                    <ext:Model runat="server" IDProperty="AdminStoreID">
                                        <Fields>
                                            <ext:ModelField Name="AdminStoreID"/>
                                            <ext:ModelField Name="StoreName" />
                                            <ext:ModelField Name="WHSID" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Items>
                            <ext:ListItem Text="Todos" Value="-1" />
                        </Items>
                    </ext:ComboBox>
                    <ext:ToolbarSeparator ID="toolbar"></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField1" 
                        runat="server"                    
                        FieldLabel="Fecha Inicio" 
                        LabelAlign="Top"
                        LabelWidth="60"  
                        Vtype="daterange"                 
                        Width="150"  > 
                        <CustomConfig>
                            <ext:ConfigItem Name="endDateField" Value="#{DateField2}" Mode="Value" />
                        </CustomConfig>                          
                    </ext:DateField>
                    <ext:ToolbarSeparator ID="toolbrsep"></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField2" 
                        runat="server"  
                        Vtype="daterange"                  
                        FieldLabel="Fecha Fin" 
                        LabelAlign="Top"
                        LabelWidth="60"                   
                        Width="150"  >   
                        <CustomConfig>
                            <ext:ConfigItem Name="startDateField" Value="#{DateField1}" Mode="Value" />
                        </CustomConfig>                        
                    </ext:DateField> 
                    <ext:ToolbarSeparator ID="ctl16"></ext:ToolbarSeparator>
                    <ext:Button runat="server" ID="Button2" Text="Buscar" Icon="BookMagnify" IconCls="BookMagnify16"
                        Scale="Small" IconAlign="Top" >
                        <DirectEvents>
                            <Click OnEvent="Search_By_WHS">
                                <EventMask ShowMask="true" Msg="Ejecutando Consulta..." MinDelay="3000" />
                            </Click>
                            </DirectEvents> 
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </topbar>
        <columnmodel ID="colmod">
            <Columns>
                <%--<ext:Column  DataIndex="ArticuloSBO" ColumnID="ArticuloSBO" Header="ArticuloSBO">
                </ext:Column>
                <ext:Column DataIndex="IMEI" Header="IMEI " Align="Center" Width="80">
                </ext:Column>
                <ext:Column DataIndex="ICC" Header="ICC" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="DN" Header="DN" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="CodigoDeBarras" Header="CodigoDeBarras" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="DescripcionSBO" Header="DescripcionSBO" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="DescCategoria" Header="DescCategoria" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="Cantidad" Header="Cantidad" Align="Center">
                </ext:Column>
                <ext:Column DataIndex="StoreName" Header="Tienda" Align="Center">
                </ext:Column>--%>
            
            </Columns>
        </columnmodel>      
        <%--<Listeners>                                
            <Command Handler="Ext.net.DirectMethods.Reimprime(record.data.Id,command);" />                
        </Listeners> --%>     
        <View>
            <ext:GridView ID="GridView2" runat="server" ForceFit="true" EnableViewState="true" 
                    AutoDataBind="true"  >
             <%--   <Templates>
                    <Header ID="ctl12" IDMode="Explicit"></Header>
                </Templates>--%>
            </ext:GridView>
        </View>
        <store>
            <ext:Store ID="Store1" IDMode="Explicit" runat="server" AutoLoad="true">
                <%--<Model>
                    <ext:Model runat="server">
                        <Fields>
                            <%--<ext:RecordField Name="ArticuloSBO"> 
                            </ext:RecordField>
                            <ext:RecordField Name="IMEI">
                            </ext:RecordField>
                            <ext:RecordField Name="ICC">
                            </ext:RecordField>
                            <ext:RecordField Name="DN">
                            </ext:RecordField>
                            <ext:RecordField Name="CodigoDeBarras">
                            </ext:RecordField>
                            <ext:RecordField Name="DescripcionSBO">
                            </ext:RecordField>
                            <ext:RecordField Name="DescCategoria">
                            </ext:RecordField>
                            <ext:RecordField Name="Cantidad">
                            </ext:RecordField>
                            <ext:RecordField Name="StoreName">
                            </ext:RecordField>
                        </Fields>
                    </ext:Model>
                </Model> --%>
            </ext:Store>                               
        </store>
        <Plugins>
        <%--<ext:GridFilters runat="server" ID="GridFilters1">
            <Filters> 
            <ext:StringFilter DataIndex="CodigoDeBarras"  /> 
            <ext:StringFilter DataIndex="ArticuloSBO"  /> 
            <ext:StringFilter DataIndex="IMEI"  /> 
            <ext:StringFilter DataIndex="ICC"  /> 
            <ext:StringFilter DataIndex="DN"  /> 
            <ext:ListFilter DataIndex="DescCategoria" Options="Equipos,Tarjetas SIM,Accesorios,TEMM,No Inventariable" />
            </Filters>
            </ext:GridFilters>--%>
        </Plugins>
        <selectionmodel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
            </ext:RowSelectionModel>
        </selectionmodel>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" PageSize="400" ></ext:PagingToolbar>
        </BottomBar>
    </ext:GridPanel>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
</asp:Content>

