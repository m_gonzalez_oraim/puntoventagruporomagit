﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CancelacionOC.aspx.vb" Inherits="PuntoVentaGrupoRoma.CancelacionOC" MasterPageFile="~/admin.master"%>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >


  
    <script type="text/javascript">
   
        var AfterEdit = function (e) {
            e.grid.store.commitChanges()
        };

        var validate = function (e) {
            e.value = null;
        };

        var addRecord = function (form, grid) {
            //            debugger;
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });

                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").CODEBARS == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "El código de barras es obligatorio",
                    title: "Error"
                });
                return false;
            }

            if (!form.getForm().getFieldValues(false, "dataIndex").IMEI == "" && !form.getForm().getFieldValues(false, "dataIndex").ICC == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "IMEI no debe llevar ICC",
                    title: "Error"
                });
                return false;
            }
            if (!form.getForm().getFieldValues(false, "dataIndex").IMEI == "" && !form.getForm().getFieldValues(false, "dataIndex").DN == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "IMEI no debe llevar DN",
                    title: "Error"
                });
                return false;
            }

            if (!form.getForm().getFieldValues(false, "dataIndex").DN == "" && form.getForm().getFieldValues(false, "dataIndex").ICC == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Debe Ingresar el ICC",
                    title: "Error"
                });
                return false;
            }
            else {
                var ICC = form.getForm().getFieldValues(false, "dataIndex").ICC;
                if (!form.getForm().getFieldValues(false, "dataIndex").ICC == "" && ICC.length != 20) {
                    Ext.net.Notification.show({
                        iconCls: "icon-exclamation",
                        html: "Longitud de ICC debe ser 20 caracteres.",
                        title: "Error"
                    });
                    return false;
                }
                else {
                    if (ICC.length == 20 && isNaN(ICC.substring(0, 19) / 1)) {
                        Ext.net.Notification.show({
                            iconCls: "icon-exclamation",
                            html: "Los primeros 19 caracteres del ICC deben ser números.",
                            title: "Error"
                        });
                        return false;
                    }
                    if (ICC.length == 20 && !isNaN(ICC.substring(19, 20) / 1)) {
                        //                    alert("El ultimo digito debe ser Letra");
                        Ext.net.Notification.show({
                            iconCls: "icon-exclamation",
                            html: "El ultimo digito del ICC debe ser Letra." + ICC.substring(19, 20),
                            title: "Error"
                        });
                        return false;
                    }
                }
            }

            if (!form.getForm().getFieldValues(false, "dataIndex").IMEI == "" || !form.getForm().getFieldValues(false, "dataIndex").ICC == "" || !form.getForm().getFieldValues(false, "dataIndex").DN == "") {
                if (form.getForm().getFieldValues(false, "dataIndex").CANTIDAD > 1) {
                    Ext.net.Notification.show({
                        iconCls: "icon-exclamation",
                        html: "La cantidad no puede ser mayor a 1",
                        title: "Error"
                    });
                    return false;
                }
            }

            if (form.getForm().getFieldValues(false, "dataIndex").DN.length > 0 && form.getForm().getFieldValues(false, "dataIndex").DN.length < 10) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "La longitud del DN debe ser de 10.",
                    title: "Error"
                });
                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").TIPOARTICULO == "") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "El artículo no tiene un grupo de artículo asignado. Informe al administrador del sistema.",
                    title: "Error"
                });
                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").IMEI == "" && form.getForm().getFieldValues(false, "dataIndex").TIPOARTICULO == "102") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Los equipos requieren  que se ingrese un IMEI.",
                    title: "Error"
                });
                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").ICC == "" && form.getForm().getFieldValues(false, "dataIndex").TIPOARTICULO == "103") {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Ls SIMS requieren que se ingrese el ICC.",
                    title: "Error"
                });
                return false;
            }

            grid.store.insert(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();
            form.getForm().reset();
        };
    </script>
    
    <ext:ResourceManager ID="ResourceManager1" runat="server">
    </ext:ResourceManager>
       <%-- ****************INICIA SQL DATA SOURCES*******************--%>
    <asp:SqlDataSource ID="SqlPurchaseOrderSAP" runat="server" 
        ConnectionString="<%$ ConnectionStrings:DBConnSAP  %>" SelectCommand="
        SELECT      
			OINV.DocEntry, 
			NNM1.SeriesName as Series, 
			OINV.DocNum, 
			CONVERT(VARCHAR, OINV.DocDate, 103) AS FechaContabilizacion, 
			CONVERT(VARCHAR, OINV.DocDueDate, 103) AS FechaEntrega, 
			OINV.NumAtCard,
			OINV.CardCode, 
			OINV.CardName,  
			OINV.Comments
        FROM         OINV INNER JOIN
                              NNM1 ON OINV.Series = NNM1.Series 
        WHERE     (OINV.DocStatus = 'O') and OINV.CardCode = @DEFAULTCUSTOMER
        ORDER BY OINV.CardCode, OINV.DocDate DESC">
         <SelectParameters>
            <asp:SessionParameter Name="DEFAULTCUSTOMER" SessionField="DEFAULTCUSTOMER" />
        </SelectParameters>
    </asp:SqlDataSource>
    
    <asp:SqlDataSource ID="SqlPurchaseOrderDetailSAP" runat="server"  
        ConnectionString="<%$ ConnectionStrings:DBConnSAP %>" SelectCommand="
        SELECT     
            INV1.DocEntry, 
            INV1.LineNum, 
            INV1.ItemCode,
            INV1.WhsCode,
            INV1.Dscription, 
            INV1.Quantity,              
            INV1.Price, 
            INV1.OpenQty,             
            INV1.LineStatus	    
            FROM         INV1 INNER JOIN
                         OITM ON INV1.ItemCode = OITM.ItemCode 
        WHERE     (INV1.DocEntry = @DocEntry) 
        ORDER BY OITM.CodeBars" >
        <SelectParameters>
            <asp:Parameter Name="DocEntry" Type="Int32" DefaultValue="0" />
        </SelectParameters>
    </asp:SqlDataSource>
    
 
   <%-- ****************TERMINA SQL DATA SOURCES*******************--%>
    <table style="width: 99%;">
        <tr>
            <td>
                <ext:GridPanel ID="gpPurchaseOrder" runat="server" Title="Factura SAP - Devolución de Compra -" Collapsed="false"
                    Collapsible="true" Layout="fit"  AutoHeight="true"  Icon="Cart">
                    <Store>
                        <ext:Store ID="StorePurchaseOrderSAP" runat="server" DataSourceID="SqlPurchaseOrderSAP"
                            SerializationMode="Simple">
                            <Model>
                                <ext:Model runat="server" IDProperty="DocEntry">
                                    <Fields>                		
                                        <ext:ModelField Name="DocEntry" />
                                        <ext:ModelField Name="Series" />
                                        <ext:ModelField Name="DocNum" />
                                        <ext:ModelField Name="FechaContabilizacion" />
                                        <ext:ModelField Name="FechaEntrega" />
                                        <ext:ModelField Name="NumAtCard" />
                                        <ext:ModelField Name="CardCode" />
                                        <ext:ModelField Name="CardName" /> 
                                        <ext:ModelField Name="Comments" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                        </ext:Store>
                    </Store> 
                    <View>
                        <ext:GridView ID="GridView1" runat="server" AutoFill="true" >
                            <%--<Templates>
                                <Header ID="ctl12"></Header>
                            </Templates>--%>
                        </ext:GridView>
                    </View>
                    <ColumnModel Sortable="True" ID="ctl61">
                        <Columns>  
                            <ext:Column runat="server" DataIndex="DocEntry" Hidden="True" ColumnID="DocEntry">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="Series" Header="Serie"  Width="30">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="DocNum" Header="No. Doc."  Width="40">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="FechaContabilizacion" Header="Fecha de Contabilización" width="80">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="FechaEntrega" Header="Fecha de Entrega" width="60">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="NumAtCard" Header="Referencia" width="70">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="CardCode" Header="Proveedor" width="50">
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="CardName" Header="Nombre" width="100">
                            </ext:Column> 
                            <ext:Column runat="server" Align="Center" DataIndex="Comments" Header="Comentarios">
                            </ext:Column>
                        </Columns>
                    </ColumnModel>
                    <SelectionModel>
                        <ext:RowSelectionModel ID="RowSelectionModel1"  runat="server" SingleSelect="true">
                            <Listeners>
                                <Select Handler="#{StorePurchaseOrderDetailSAP}.reload();"  />
                            </Listeners>
                        </ext:RowSelectionModel>
                    </SelectionModel>
                    <BottomBar>
                        <ext:PagingToolbar ID="PagingToolBar1" runat="server" PageSize="6" HideRefresh="true" />
                    </BottomBar>
                </ext:GridPanel>

                <ext:GridPanel  ID="gpPurchaseOrderDetail" runat="server"  Title="Detalles"  Layout="fit" 
                 Icon="CartMagnify" AutoExpandColumn="DocEntry"  AutoHeight="true" Collapsible="true" AnimCollapse="true">
                    <Store>
                        <ext:Store runat="server" ID="StorePurchaseOrderDetailSAP" 
                             OnReadData="StorePurchaseOrderDetail_Refresh">
                            <Model>
                                <ext:Model runat="server" IDProperty="LineNum">
                                    <Fields>  
                                        <ext:ModelField Name="DocEntry" />
                                        <ext:ModelField Name="LineNum" />
                                        <ext:ModelField Name="ItemCode" /> 
                                        <ext:ModelField Name="WhsCode" />
                                        <ext:ModelField Name="Dscription" />
                                        <ext:ModelField Name="Quantity" />
                                        <ext:ModelField Name="Price" />
                                        <ext:ModelField Name="OpenQty" />
                                        <ext:ModelField Name="LineStatus" />
                                        <ext:ModelField Name="INGRESAR" /> 
                                        <ext:ModelField Name="CantidadIngresada" />
                                        <ext:ModelField Name="Comentarios" />
                                    </Fields>
                                </ext:Model>
                            </Model>
                            <Parameters>
                                <ext:StoreParameter 
                                    Name="DocEntry" 
                                    Value="#{gpPurchaseOrder}.getSelectionModel().hasSelection() ? #{gpPurchaseOrder}.getSelectionModel().getSelection()[0].data.DocEntry : -1"
                                    Mode="Raw" 
                                    />
                            </Parameters>
                            <Listeners>
                                <Exception Handler="Ext.Msg.alert('Productos - Error de lectura.', e.message || response.statusText);" />
                            </Listeners>
                        </ext:Store>
                    </Store>
                    <View>
                        <ext:GridView ID="GridView2" runat="server" AutoFill="true" />
                    </View>
                    <ColumnModel runat="server">
                        <Columns> 
                            <ext:Column runat="server" DataIndex="DocEntry" Hidden="True" ColumnID="DocEntry"></ext:Column>
                            <ext:Column runat="server" Align="Center" Hidden="True" DataIndex="LineNum" Header="Linea"></ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="ItemCode" Header="Artículo"></ext:Column> 
                            <ext:Column runat="server" Align="Left" DataIndex="Dscription" Header="Descripción"></ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="Quantity" Header="Cantidad"></ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="OpenQty" Header="Cantidad Abierta"></ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="CantidadIngresada" Header="Cantidad Ingresada"></ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="Price" Header="Precio">
                                <Renderer Format="UsMoney" />
                            </ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="WhsCode" Header="Almacén"></ext:Column>
                            <ext:Column runat="server" Align="Center" DataIndex="LineStatus" Header="Status"></ext:Column>  
                            <ext:Column runat="server" Align="Right" DataIndex="INGRESAR" Header="Cantidad a Devolver" > 
                                <Editor><ext:NumberField AllowDecimals="false" AllowNegative="false" AllowBlank="false" runat="server" ID="NumberField1"></ext:NumberField></Editor>
                            </ext:Column>
                            <ext:Column runat="server" Align="Right" DataIndex="Comentarios" Header="Comentarios" > 
                                <Editor><ext:TextField runat="server" ID="TxtfComentarios" Width="200px"></ext:TextField></Editor>  
                            </ext:Column>
                        </Columns>
                    </ColumnModel>         
                    <BottomBar>        
                        <ext:PagingToolbar ID="PagingToolBar2" runat="server" PageSize="200" />           
                    </BottomBar>
                    <SelectionModel>
                        <ext:RowSelectionModel runat="server" SingleSelect="true">
                        </ext:RowSelectionModel>
                    </SelectionModel> 
                    <Listeners>
                        <Expand Handler="#{StorePurchaseOrderDetailSAP}.reload();" />
                    </Listeners>
                    <TopBar>
                        <ext:Toolbar ID="Toolbar2" runat="server">
                            <Items>
                                <ext:Button ID="Button4" runat="server" Text="Procesar Devolución" Icon="Disk"  Type="Submit" >
                                    <DirectEvents>
                                        <Click 
                                            OnEvent="UploadClick"
                                            Before="Ext.Msg.wait('Procesando Recepción...', 'Cargando...');"
                                    
                                            Failure="Ext.Msg.show({ 
                                                title   : 'Error', 
                                                msg     : 'Error durante la carga...', 
                                                minWidth: 200, 
                                                modal   : true, 
                                                icon    : Ext.Msg.ERROR, 
                                                buttons : Ext.Msg.OK 
                                            });">
                                            <ExtraParams>
                                                <ext:Parameter Name="Grid1" Value="Ext.encode(#{gpPurchaseOrder}.getRowsValues({selectedOnly : true}))" Mode="Raw" /> 
                                                <ext:Parameter Name="Grid2" Value="Ext.encode(#{gpPurchaseOrderDetail}.getRowsValues({selectedOnly : false}))" Mode="Raw" />
                                            </ExtraParams>                            
                                        </Click>
                                    </DirectEvents>
                                </ext:Button>   
                            </Items>
                        </ext:Toolbar>
                    </TopBar> 
                </ext:GridPanel>  
            </td>
        </tr>
    </table>
</asp:Content>