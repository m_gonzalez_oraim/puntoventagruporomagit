﻿Imports Ext.Net
Imports System.IO
Imports System.Xml

Public Class EstatusCompras
    Inherits System.Web.UI.Page

    Private mensaje As String = ""
    Private connstringWEB As String
    Private connstringSAP As String
    Private oDB As New DBMaster

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("AdminUserID") > 0 And Session("IDSTORE") > 0 Then

        Else
            Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
        End If

        If Not Ext.Net.X.IsAjaxRequest Then

            If Session("STORETYPEID") = 3 Then
                Response.Redirect("~/Default.aspx?ReturnUrl=" & Request.Url.AbsolutePath)
            End If

            Dim CONSTRING As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            Dim INITIALCATALOG = CONSTRING.Split(";")(1).Split("=").Clone(1) & ".DBO.OITM"

            Dim SelectCommand As String
            SelectCommand = _
            "select" & _
            "'OCPOS-' + cast(CE.IDCompra as varchar) as id " & _
                            ",'OCPOS-' + cast(CE.IDCompra as varchar) as [OCPOS] " & _
                            ",'Orden de Compra' as [TipoCompra] " & _
                            ",cast(ce.FechaComWeb as date)  as [Fecha] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then 'NO' else 'SI' END as [Importado] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then '-' else (select top 1 cast(docnum as varchar) from " & Session("SAPDB") & ".ORDR where U_FOLPOS='OCPOS-' + cast(CE.IDCompra as varchar)) END as [DocSAP] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then '-' else (select top 1 cast(DocTotal as varchar) from " & Session("SAPDB") & ".ORDR where U_FOLPOS='OCPOS-' + cast(CE.IDCompra as varchar)) END as [Total] " & _
                            "from CompraEncabezado CE,CompraDetalle CD " & _
                            "where ce.IDCompra = cd.IDCompra " & _
                            "and cd.IDTienda=" & Session("IDSTORE") & "  " & _
                            "group by ce.IDCompra,ce.FechaComWeb,ce.Fecha_Importado " & _
                            "union all " & _
                            " select  " & _
                            "Prefijo + '-' + cast(CE.Folio as varchar) as id " & _
                            ",Prefijo + '-' + cast(CE.Folio as varchar) as [OC POS / Referencia SAP] " & _
                            ",'Resurtido' as [Tipo Compra] " & _
                            ",cast(ce.Fecha as date)  as [Fecha] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then 'NO' else 'SI' END as [Importado] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then '-' else (select top 1 cast(docnum as varchar) from " & Session("SAPDB") & ".OQUT where U_FOLPOS=Prefijo + '-' + cast(CE.Folio as varchar)) END as [No. Documento SAP] " & _
                            ",case isnull(ce.Fecha_Importado,'') when '' then '-' else (select top 1 cast(DocTotal as varchar) from " & Session("SAPDB") & ".OQUT where U_FOLPOS=Prefijo + '-' + cast(CE.Folio as varchar)) END as [Total] " & _
                            "from VentasEncabezado CE,VentasDetalle CD " & _
                            "where ce.id=cd.IDVenta " & _
                            "and cd.IDStore <> Ce.IDStore " & _
                            "and cd.StatusLinea = 'O' " & _
                            "and ce.IDStore= " & Session("IDSTORE") & _
                            " group by ce.id,ce.Fecha,ce.Fecha_Importado,Prefijo,Folio,ce.IDStore,cd.StatusLinea "

            SqlPurchaseOrderSAP.SelectCommand = SelectCommand

            SelectCommand = "select 'OCPOS-' + cast(IDCompra as varchar) as ID ,Linea ,(select ItemName from " & Session("SAPDB") & ".OITM where itemcode=(select ArticuloSBO from Articulos where IDArticulo=Articulo)) as [Articulo] ,Cantidad from CompraDetalle where IDCompra=@id"

            SqlPurchaseOrderDetailSAP.SelectCommand = SelectCommand
        Else
        End If
    End Sub


    Protected Sub StorePurchaseOrderDetail_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)

        Dim id As String = e.Parameters("id").ToString
        Session("PURCHASEORDERID") = id
        Dim sQuery As String
        Dim dt As New DataTable
        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        Dim msgConfig As Ext.Net.MessageBoxConfig = New Ext.Net.MessageBoxConfig()

        If UpdateDetailGrid(id) Then
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "RESULTADO"
            msgConfig.Message = mensaje
        Else
            msgConfig.Buttons = MessageBox.Button.OK
            msgConfig.Icon = MessageBox.Icon.INFO
            msgConfig.Title = "ERROR"
            msgConfig.Message = mensaje
        End If


    End Sub

    Private Function UpdateDetailGrid(ByVal id As String) As Boolean

        Dim counter As Integer = 0
        Dim dt As New DataTable
        Dim dtDetail As New DataTable
        Dim newDtDetail As New DataTable
        Dim Data As Object
        Dim Data2 As Object
        Dim sQuery As String
        Dim IMEI As String
        Dim ICC As String
        Dim DN As String
        Dim CODEBARS As String
        Dim DESCRIPCION As String
        Dim TIPOARTICULO As Integer
        connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
        connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

        If id.Split("-")(0) <> "OCPOS" Then

            sQuery = " select "
            sQuery = sQuery & " ve.Prefijo  + '-' + ve.Folio as ID"
            sQuery = sQuery & " ,vd.IDLinea as Linea"
            sQuery = sQuery & " ,(select ItemName from " & Session("SAPDB") & ".OITM where itemcode=(select ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) as [Articulo]"
            sQuery = sQuery & " ,Cantidad "
            sQuery = sQuery & " from"
            sQuery = sQuery & " VentasDetalle VD, VentasEncabezado VE"
            sQuery = sQuery & " where ve.id = vd.IDVenta"
            sQuery = sQuery & " and ve.Prefijo  + '-' + ve.Folio = '" & id & "'"

        Else

            sQuery = "select "
            sQuery = sQuery & " 'OCPOS-' + cast(IDCompra as varchar) as ID"
            sQuery = sQuery & " ,Linea "
            sQuery = sQuery & " ,(select ItemName from " & Session("SAPDB") & ".OITM where itemcode=(select ArticuloSBO from Articulos where IDArticulo=Articulo)) as [Articulo]"
            sQuery = sQuery & " ,Cantidad"
            sQuery = sQuery & " from CompraDetalle where 'OCPOS-' + cast(IDCompra as varchar)='" & id & "'"

        End If

        dtDetail = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "DataGrid", connstringWEB)
        
        If Ext.Net.X.IsAjaxRequest Then
            Me.StorePurchaseOrderDetailSAP.DataSource = dtDetail
            Me.StorePurchaseOrderDetailSAP.DataBind()
        End If
        UpdateDetailGrid = True
        mensaje = counter & " Partidas en la orde de compra."

    End Function

    <DirectMethod()> _
    Protected Sub clickedit()
    End Sub

    <DirectMethod()> _
    Protected Sub AfterEdit(ByVal myObj As Object)
        Me.gpPurchaseOrderDetail.Store.Primary.CommitChanges()
    End Sub

    Private Function VerificarExistenciaArchivos(ByVal sNombreLines As String) As Boolean
        If File.Exists(sNombreLines) Then
            Return True
        End If
    End Function

End Class
