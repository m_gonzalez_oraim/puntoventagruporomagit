﻿    <%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EditableGrid.aspx.vb" Inherits="PuntoVentaGrupoRoma.EditableGrid" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>ComponentColumn Editor - Ext.NET Examples</title>
    <link href="/resources/css/examples.css" rel="stylesheet" />    
    <link href="/css/ExtNetCustomStyles.css" rel="stylesheet" />
</head>
<body>
    <script type="text/javascript">
        function InicializarValoresCombo(combo,numeroRegistros)
        {
            for(i=0;i<numeroRegistros;i++)
                combo.removeByIndex(0)
        }

        var Detalle3 = {
            ControlarCambioFactor: function (idRegistro) {
                var precio = Ext.getCmp("cmbFactor" + idRegistro).getValue();
                Ext.getCmp("txtPrecio" + idRegistro).setValue(precio);
                Ext.getCmp("hdnPrecio" + idRegistro).setValue(precio);
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.RecalcularTotalesSeleccionLineaGrid(cantidad, precio, descuento, sujetoAImpuesto, porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                    }
                });
            }
        }

        var Detalle2 = {
            ControlarCambioUnidadMedida: function (idRegistro) {
                var comboUnidad = Ext.getCmp("cmbUnidadMedida" + idRegistro);
                var comboFactores = Ext.getCmp("cmbFactor" + idRegistro);
                var codigoArticulo = Ext.getCmp("txtCodigoArticulo" + idRegistro).getValue();
                var factoresUsuario = "0,0,1,1,0";
                var factoresTienda = "0,0,1,1,0";
                var precioCosto = Ext.getCmp("hdnPrecioCosto" + idRegistro).getValue();
                var numeroFactores = Ext.getCmp("hdnFactores" + idRegistro).getValue();
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var valorUnidadMedida = comboUnidad.getValue();
                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.ControlarCambioUnidadDeMedidaGrid(codigoArticulo,factoresUsuario,factoresTienda,valorUnidadMedida,precioCosto,cantidad,descuento,sujetoAImpuesto,porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecioMinimoAutorizacion" + idRegistro).setValue(result.PrecioMinimoAutorizacion);
                        Ext.getCmp("hdnPrecioMaximoAutorizacion" + idRegistro).setValue(result.PrecioMaximoAutorizacion);
                        InicializarValoresCombo(comboFactores,numeroFactores);
                        for (i = 0; i < result.Factores.length; i++) {
                            comboFactores.insertItem(i, result.Factores[i].Descripcion, result.Factores[i].Precio);
                        }
                        comboFactores.setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtPrecio" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("hdnPrecio" + idRegistro).setValue(result.PrecioPrimerFactor);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                        Ext.getCmp("hdnFactores" + idRegistro).setValue(result.Factores.length);
                    }
                });
            }
        }

        var Detalle = {
            RecalcularTotalesSeleccionLineaGrid: function (idRegistro) {
                var cantidad = Ext.getCmp("txtCantidad" + idRegistro).getValue();
                var ultimaCantidad = Ext.getCmp("hdnCantidad" + idRegistro).getValue();
                var precio = Ext.getCmp("txtPrecio" + idRegistro).getValue();
                var ultimoPrecio = Ext.getCmp("hdnPrecio" + idRegistro).getValue();
                var descuento = Ext.getCmp("txtDescuento" + idRegistro).getValue();
                var ultimoDescuento = Ext.getCmp("hdnDescuento" + idRegistro).getValue();

                if (cantidad == ultimaCantidad && precio == ultimoPrecio && descuento == ultimoDescuento)
                    return;

                var sujetoAImpuesto = Ext.getCmp("hdnSujImp" + idRegistro).getValue();
                var porcentajeImpuesto = 16;

                App.direct.RecalcularTotalesSeleccionLineaGrid(cantidad, precio, descuento, sujetoAImpuesto, porcentajeImpuesto, {
                    success: function (result) {
                        Ext.getCmp("hdnPrecio" + idRegistro).setValue(precio);
                        Ext.getCmp("hdnDescuento" + idRegistro).setValue(descuento);
                        Ext.getCmp("hdnCantidad" + idRegistro).setValue(cantidad);
                        Ext.getCmp("txtTotalLineaConImpuesto" + idRegistro).setValue(result.ImporteLineaImpuesto);
                        Ext.getCmp("hdnTotalLinea" + idRegistro).setValue(result.ImporteLinea);
                    }
                });


            }
        }
    </script>
    <form runat="server">
        <ext:ResourceManager runat="server" />
        
        <h1>ComponentColumn as Editor</h1>
        <ext:Button
            runat="server">
            <DirectEvents>
                <Click OnEvent="AgregarRegistroVenta">
                </Click>
            </DirectEvents>

        </ext:Button>

        <ext:Panel         
            runat="server"    
            Width="800" 
            Height="300"
            Layout="AnchorLayout"
            Id="panel1"
            AutoScroll="true">
            <Items>
                <ext:Container runat="server" Width="750" Layout="HBoxLayout">
                    <Items>
                        <ext:Label runat="server" ID="HeaderGridCodigo" Text="Codigo" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        <ext:Label runat="server" ID="HeaderGridNombre" Text="Articulo" Width="140" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        <ext:Label runat="server" ID="HeaderGridUnidad" Text="Unidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        <ext:Label runat="server" ID="HeaderGridCantidad" Text="Cantidad" Width="70" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        <ext:Label runat="server" ID="HeaderGridFactor" Text="Factor" Width="90" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        <ext:Label runat="server" ID="HeaderGridPrecio" Text="Precio" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        <ext:Label runat="server" ID="HeaderGridDescuento" Text="Descuento" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid" />
                        <ext:Label runat="server" ID="HeaderGridImporte" Text="Importe" Width="80" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                        <ext:Label runat="server" ID="HeaderGridComentarios" Text="Comentarios" Width="100" Height="20" PaddingSpec="0 0 5 5" Cls="LabelCustomGrid"/>
                    </Items>
                </ext:Container>
            </Items>
        </ext:Panel>

  
    </form>
</body>
</html>