﻿<!--
// GeneralLib.js 
function ConfirmDelete()
{
    return confirm('Are you Sure?');
}


function GetUpperCase(tbControl)
{
    if(tbControl.value!='')
    {
        tbControl.value = tbControl.value.toUpperCase();
    }
}

function ConfirmGenerateLetters(Status)
{    
    if(Status=='OP')
    {
        return true;
    }
    else
    {
        return confirm('If you continue the report can not be edited, are you sure?');
    }    
}

function CleanString(objTB, CharacterToClean) 
{
	// Check if has a custimzed chars to clean
	if (CharacterToClean=="") 
	{
		CharacterToClean="'";
	}

	// Get String To Clean
	var StringToClean = objTB.value;
	
	// Clean for each char in CharacterToClean
	for (i = 0; i < CharacterToClean.length; i++) 
	{
		var Expresion = new RegExp(CharacterToClean.charAt(i),"gm");
		StringToClean= StringToClean.replace(Expresion, '')
	}

	// Return Cleaned String
	objTB.value = StringToClean;
}

function RTrim(TrimString) 
{
	// Cut forward blanks
	var index = (TrimString.length - 1);
	while (index >= 0 )
	{
		if (TrimString.charAt(index) != " ")
		{
			break;
		}
		index--;
	}
	return(TrimString.substr(0, index + 1))
}

function LTrim(TrimString)
{
	// Cut leading blanks
	var index = 0;
	while (index < TrimString.length )
	{
		if (TrimString.charAt(index) != " ")
		{
			break;
		}
		index++;
	}
	return(TrimString.substr(index, TrimString.length ))
}    

//Cut Blanks from Field
function Trim(TrimString)
{
    return (RTrim(LTrim(TrimString)));
}

function IsBlank(Field)
{
	if (Trim (Field) =="")
	{
		return(true);   
	}
	else
	{
		return(false);
	}
}

function ValidateString(StringField, ErrorMessage)
{

	StringField.value = LTrim(StringField.value);

	var StringOK = true;
	
	if (StringField.value == "")
	{
		alert(ErrorMessage);
		StringOK = false;
		if ( (!StringField.disabled) && (StringField.type != 'hidden') )
		{
			StringField.focus();
		}
	}

	return(StringOK);
}

function isInteger(s)
{
      var i;
      var cad;
	  cad = s.value.toString();
      for (i = 0; i < cad.length; i++)
      {
         var c = cad.charAt(i);
         if (isNaN(c)) 
	   {
	    s.value = ""
	    s.focus();
	    alert("El valor '" + cad + "' no es un valor numerico valido.");
	    return false;
	   }
      }
      return true;
}

function ValidateInteger(NumberField, ErrorMessage, MinValue, MaxValue)
{

	// Error Message Must Include Range error.

	var IntegerOK = true;
	
	if(parseFloat(NumberField.value)-parseInt(NumberField.value) != 0)
	{
		alert(ErrorMessage);
		IntegerOK = false;
		NumberField.focus();
	}
	else
	{
		// Change Range
		if ((NumberField.value < MinValue)||(NumberField.value > MaxValue) || (isNaN(parseFloat(NumberField.value))))
		{
			alert(ErrorMessage);
			NumberOK = false;
			NumberField.focus();
		}
	}

	return(IntegerOK);
}


function ValidateInteger2(IntValue, MinValue, MaxValue)
{

	// Error Message Must Include Range error.
	var IntegerOK = true;
	
	if(parseFloat(IntValue)-parseInt(IntValue) != 0)
	{
		// Is not Integer
		IntegerOK = false;
	}
	else
	{
		// Change Range
		if ((IntValue < MinValue)||(IntValue > MaxValue) || (isNaN(parseFloat(IntValue))))
		{
		    // Is not Range
		    IntegerOK = false;
		}
	}

	return(IntegerOK);
}

function validateDate(oSrc, args)
{
    var iDay, iMonth, iYear;
    var arrValues;
    arrValues = args.Value.split("/");
    iMonth = arrValues[0];
    iDay = arrValues[1];
    iYear = arrValues[2];
    
    var testDate = new Date(iYear, iMonth - 1, iDay);
    if ((testDate.getDate() != iDay) || (testDate.getMonth() != iMonth - 1) || (testDate.getFullYear() != iYear))
    {
        args.IsValid = false;
        return;
    }
      
    return true;
}

function validateDDL(oSrc, args)
{
	if (args.Value=="-1")
    {
        args.IsValid = false;
        return;
    }
      
    return true;
}

function CountLetters(Source, Destination, MaxLetters)
{
	//Textos(Source)
	
	numLetters = Source.value.length;
	if(numLetters>MaxLetters)
	{
		alert('El limite de las observaciones es de: ' + MaxLetters + ' caractéres.');
		Source.value = Source.value.substring(0,MaxLetters)
		numLetters = Source.value.length;
	}

	
	Destination.value = numLetters;    
}

//Valida que no inserten caracteras 'raros' en un campo de texto
function Textos(val)   {  
     //alert(val.value);
     var mikExp = /([!¡<>&/¿{}áéíóú°~ñÑ])/;  
     var strPass = val.value;  
     var strLength = strPass.length;  
     var lchar = val.value.charAt((strLength) - 1);  
     if ((lchar.search(mikExp) != -1) || (lchar == '[') || (lchar == ']'))  
     {  
         var tst = val.value.substring(0, (strLength) - 1); val.value = tst;  
     }  
 }
 
function PickDate(BaseURL,DateBox, IniYear, EndYear)
{
	// Define Window Size and Center It
	WindowWidth = 210;
	WindowHeight = 250;
	CenterLeft = (screen.width - WindowWidth)/2;
	CenterTop = (screen.height - WindowHeight)/2;
	
	// Set Window Settings
	var WinSettings = 'location=no,LEFT=' + CenterLeft + ',TOP=' + CenterTop + ',WIDTH=' + WindowWidth + ',HEIGHT=' + WindowHeight;

	//Define Url To Open
	OpenURL = BaseURL + '/Lib/Calendar.aspx?CD=' + DateBox.value + '&DBN=' + DateBox.name + '&NF=0' + '&IY=' + IniYear + '&EY=' + EndYear; 

	// Open Window
	CalendarWindow = window.open(OpenURL,'CalendarWindow', WinSettings);
	CalendarWindow.focus();
	
	// Loose Focus on Box
	DateBox.blur();
	
	// Set Control Reference
	//CalendarWindow.execScript('hfDateBox.value = "' + DateBox.name + '";', 'jscript');
}

function PickDateNoFuture(BaseURL,DateBox, IniYear, EndYear)
{
	// Define Window Size and Center It
	WindowWidth = 210;
	WindowHeight = 250;
	CenterLeft = (screen.width - WindowWidth)/2;
	CenterTop = (screen.height - WindowHeight)/2;
	
	// Set Window Settings
	var WinSettings = 'location=no,LEFT=' + CenterLeft + ',TOP=' + CenterTop + ',WIDTH=' + WindowWidth + ',HEIGHT=' + WindowHeight;

	//Define Url To Open
	OpenURL = BaseURL + '/Lib/Calendar.aspx?CD=' + DateBox.value + '&DBN=' + DateBox.name + '&NF=1' + '&IY=' + IniYear + '&EY=' + EndYear;

	// Open Window
	CalendarWindow = window.open(OpenURL,'CalendarWindow', WinSettings);
	CalendarWindow.focus();
	
	// Loose Focus on Box
	DateBox.blur();
	
	// Set Control Reference
	//CalendarWindow.execScript('hfDateBox.value = "' + DateBox.name + '";', 'jscript');
}

function RefreshMainWindow()
{
	// Suppouse is not OK
		window.opener.RefreshWindow();
		window.close();
}

function client_OnTreeNodeChecked()
{
	var obj = window.event.srcElement;
	var treeNodeFound = false;
	var checkedState;
	if (obj.tagName == "INPUT" && obj.type == "checkbox")
	{
		var treeNode = obj;
		checkedState = treeNode.checked;
		do
		{
			obj = obj.parentElement;
		} while (obj.tagName != "TABLE")
	
		var parentTreeLevel = obj.rows[0].cells.length;
		var parentTreeNode = obj.rows[0].cells[0];
		var tables = obj.parentElement.getElementsByTagName("TABLE");
		var numTables = tables.length
		if (numTables >= 1)
		{
			for (i=0; i<numTables-1; i++)
			{
				if (tables[i] == obj)
				{
					treeNodeFound = true;
				}
				if (treeNodeFound == true)
				{
					var childTreeLevel = tables[i+1].rows[0].cells.length;
					if (childTreeLevel > parentTreeLevel)
					{
						var cell = tables[i+1].rows[0].cells[childTreeLevel - 1];
						var inputs = cell.getElementsByTagName("INPUT");
						inputs[0].checked = checkedState;
					}
					else
					{
						// Level Change, Stop Cascading
						treeNodeFound = false;
					}
				}
			}
			// Check parents
			CheckParents(obj)
		}
	}
}

function CheckParents(obj) 
{ 
	var chk1 = true; 
	//Get the mom. 
	var head1 = obj.parentElement.previousSibling; 
	//no rows, cant do my work. 
	if(obj.rows == null) 
	{
		return;
	} 

	//This is how may rows are at this level. 
	var pTreeLevel1 = obj.rows[0].cells.length; 

	//Are we a parent
	if(head1.tagName == "TABLE") 
	{ 

		//Get the list of rows ahead of us. 
		var tbls = obj.parentElement.getElementsByTagName("TABLE"); 

		//get the count of that list. 
		var tblsCount = tbls.length; 

		//determine if any of the rows underneath are unchecked. 
		for (i=0; i < tblsCount; i++) 
		{ 
			var childTreeLevel = tbls[i].rows[0].cells.length; 
			if (childTreeLevel = pTreeLevel1) 
			{ 
				var chld = tbls[i].getElementsByTagName("INPUT"); 
				if (chld[0].checked == false) 
				{ 
					chk1 = false; 
					break; 
				} 
			}
		} 

		var nd = head1.getElementsByTagName("INPUT"); 
		nd[0].checked = chk1; 

		//do the same for the level above (If is not Top Level)
		if(obj.parentElement.nextSibling)
		{
			CheckParents(obj.parentElement.nextSibling); 
		}
	} 
	else 
	{ 
		return; 
	} 
}


//-->

