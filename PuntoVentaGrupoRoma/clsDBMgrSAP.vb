﻿Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient

Public Class clsDBMgrSAP 
    Private p_DBConn As New SqlConnection
    Public ReadOnly Property DBConn() As SqlConnection
        Get
            Return p_DBConn
        End Get
    End Property
    Public Sub New()
        ' Open DB Connection
        p_DBConn.ConnectionString = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
        p_DBConn.Open()
    End Sub
    Protected Overrides Sub Finalize()
        ' Try to Close the Connection
        Try
            p_DBConn.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub GetQuerydtr(ByVal QueryToRun As String, ByRef LocalReader As SqlDataReader)

        ' Define Command to Execute
        Dim SQLComm As New SqlCommand(QueryToRun, p_DBConn)

        ' Return Resultset
        LocalReader = SQLComm.ExecuteReader

    End Sub
    Public Sub GetQuerydtr(ByVal QueryToRun As String, ByVal Parameters() As SqlParameter, ByRef LocalReader As SqlDataReader)
        ' Define Command to Execute
        Dim SQLComm As New SqlCommand(QueryToRun, p_DBConn)
        SQLComm.CommandType = CommandType.StoredProcedure

        ' Set Passed Parameters
        Dim Parameter As SqlParameter

        For Each Parameter In Parameters
            SQLComm.Parameters.Add(Parameter)
        Next

        ' Return Resultset
        LocalReader = SQLComm.ExecuteReader

    End Sub
    Public Function GetQuerydts(ByVal QueryToRun As String) As DataSet
        Dim LocalDataSet As New DataSet

        ' Define Command to Execute
        Dim SQLAdap As New SqlDataAdapter(QueryToRun, p_DBConn)
        SQLAdap.Fill(LocalDataSet)

        Return LocalDataSet
    End Function
    Public Sub ExecuteCMD(ByVal CMDToRun As String)

        ' Define Command to Execute
        Dim SQLComm As New SqlCommand(CMDToRun, p_DBConn)

        ' Execute, With out Returning Dataset
        SQLComm.ExecuteNonQuery()

    End Sub
    Public Sub ExecuteCMD(ByVal CMDToRun As String, ByVal Parameters() As SqlParameter)

        ' Define Command to Execute
        Dim SQLComm As New SqlCommand(CMDToRun, p_DBConn)
        SQLComm.CommandType = CommandType.StoredProcedure

        ' Set Passed Parameters
        Dim Parameter As SqlParameter
        For Each Parameter In Parameters
            SQLComm.Parameters.Add(Parameter)
        Next

        ' Execute, With out Returning Dataset
        SQLComm.ExecuteNonQuery()

    End Sub
    Public Sub ExecuteCMDReturn(ByVal CMDToRun As String, ByRef Parameters() As SqlParameter)

        ' Define Command to Execute
        Dim SQLComm As New SqlCommand(CMDToRun, p_DBConn)
        SQLComm.CommandType = CommandType.StoredProcedure

        ' Set Passed Parameters
        Dim Parameter As SqlParameter
        For Each Parameter In Parameters
            SQLComm.Parameters.Add(Parameter)
        Next

        ' Execute, With out Returning Dataset
        SQLComm.ExecuteNonQuery()

        ' Return Parameters
        Dim Index As Integer
        For Index = 0 To Parameters.Length - 1
            Parameters(Index) = SQLComm.Parameters(Index)
        Next

    End Sub
End Class
