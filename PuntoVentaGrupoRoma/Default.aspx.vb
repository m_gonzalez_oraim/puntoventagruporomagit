﻿Imports Ext.Net

Public Class _Default
    Inherits System.Web.UI.Page
    Private oDB As New DBMaster
    Private connstringWEB As String
    Private connstringSAP As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Ext.Net.X.IsAjaxRequest Then

            Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)
            Dim controlAccceso As ControlAcceso = New ControlAcceso()
            If controlAccceso.EsSuperUsuario(principalPV.UsuarioPuntoVenta.UserId) AndAlso Not controlAccceso.HayTiendasDadasDeAltaParaSuperUsuario(principalPV.UsuarioPuntoVenta.UserId) Then
                Window1.Hidden = True
                Return
            End If

            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            Dim dt As New DataTable
            Dim sQuery As String
            sQuery = "SELECT  ADS.AdminStoreID, ADS.StoreName,ADS.WhsId " & vbCrLf & _
                      "FROM AdminStore ADS " & vbCrLf & _
                      "     LEFT JOIN UserStores US " & vbCrLf & _
                      "     ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                      "     and US.AdminUserID = '" & principalPV.UsuarioPuntoVenta.UserId & "'  " & vbCrLf & _
                      "Where(US.AdminUserId > 0) and ADS.Status='A' order by ADS.StoreName"
            dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Tiendas", connstringWEB)
            Store1.DataSource = dt
            Store1.DataBind()
            ComboBox1.DataBind()
            If dt.Rows.Count = 0 Then
                Dim msg As New Ext.Net.Notification
                Dim nconf As New Ext.Net.NotificationConfig
                nconf.IconCls = "icon-exclamation"
                nconf.Html = "El usuario no tiene ninguna tienda Activa Asignada, preciona el boton de regreso a la pagina anterior en su navegador."
                nconf.Title = "Error"
                msg.Configure(nconf)
                msg.Show()
                Exit Sub
            Else
                sQuery = "Select top 1 TextoMarquesina from Marquesina"
                dt = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "Marquesina", connstringWEB)
                'lblMarquesina.Text = dt.Rows(0).Item("TextoMarquesina")
            End If
        End If
    End Sub


    <DirectMethod()> _
    Public Sub controlarCambioTienda()

        Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

        Dim idTienda As Integer = ComboBox1.SelectedItem.Value
        Dim nombreTienda As String = ComboBox1.SelectedItem.Text
        Dim usuarioPVIdentity As UsuarioPuntoVentaIdentity = New UsuarioPuntoVentaIdentity(principalPV.UsuarioPuntoVenta.UserId, principalPV.UsuarioPuntoVenta.UserName, principalPV.UsuarioPuntoVenta.Permisos, principalPV.UsuarioPuntoVenta.MenuUsuario, idTienda, nombreTienda, principalPV.UsuarioPuntoVenta.Guid)

        Dim faCookei As HttpCookie = New HttpCookie("PuntoVentaBXP", UsuarioPuntoVentaIdentity.Serialize(usuarioPVIdentity))
        Response.Cookies.Set(faCookei)

        Label1.Visible = True
        Window1.Hidden = True

        Master.lblStoreName.Text = nombreTienda


    End Sub
End Class