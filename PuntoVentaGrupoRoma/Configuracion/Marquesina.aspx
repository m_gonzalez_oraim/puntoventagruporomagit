﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.master" CodeBehind="Marquesina.aspx.vb" Inherits="PuntoVentaGrupoRoma.Marquesina" %>

<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >
        <table>
            <tr valign="top">
                <td>
                    <ext:FormPanel ID="FormPanel1" runat="server" 
                        Title="Ingrese el texto para el mensaje inicial del punto de venta" Padding="5" ButtonAlign="Right" 
                        Width="600" AnchorWidth="50" TitleAlign="Right" Icon="Group">                            
                        <Items>
                            <ext:FieldSet ID="FieldSet1" runat="server" Title="Texto" Collapsible="true" Layout="form">
                                <Items>    
                                    <ext:TextArea ID="MarqueeText"   runat="server" FieldLabel="Mensaje" Width="500PX" CausesValidation="false" /> 
                                </Items>
                            </ext:FieldSet>
                        </Items>
                        <Listeners>
                            <ValidityChange  Handler="#{LoadButton}.setDisabled(!valid);" />
                        </Listeners>
                        <Buttons>
                            <ext:Button ID="btnAgregar" runat="server" Text="Guardar" Icon="TextSignature"> 
                                <DirectEvents>
                                        <Click OnEvent="GuardarClick"></Click>  
                                </DirectEvents>
                            </ext:Button>
                            <ext:Button ID="btnLimpiarCampos" runat="server" Text="Limpiar Campo" Icon="Erase">
                                <Listeners>
                                    <Click Handler="#{FormPanel1}.getForm().reset();" />
                                </Listeners>
                            </ext:Button>             
                        </Buttons>
                      </ext:FormPanel>         
                </td>
            </tr>
        </table>
</asp:Content>