﻿Imports System.Security.Principal
Imports System.Data.SqlClient
Imports Ext.Net
Imports System.IO
Imports System.Xml

Public Class Marquesina
    Inherits System.Web.UI.Page

    Private connstringWEB As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    Private connstringSAP As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
    Private oDB As New DBMaster
    Private sQuery As String
    Private dt As New DataTable
    Private tipoMensaje As String
    Private mensaje As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Function Mensajes(ByVal Tittle As String, ByVal Html As String) As Boolean
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-exclamation"
        nconf.Html = Html
        nconf.Title = Tittle
        nconf.HideDelay = 5000
        msg.Configure(nconf)
        msg.Show()
    End Function


    Protected Sub GuardarClick(ByVal sender As Object, ByVal e As DirectEventArgs)
        If UpdateMarquee(MarqueeText.Text) = True Then
            Mensajes("Éxito", mensaje)
        Else
            Mensajes("Error", mensaje)
        End If


    End Sub

    Private Function UpdateMarquee(ByVal Texto As String) As Boolean
        sQuery = _
        "Delete From Marquesina" & vbCrLf & _
        "INSERT INTO Marquesina (TextoMarquesina) " & vbCrLf & _
        "VALUES ('" & Texto & "')"
        If oDB.EjecutaQry(sQuery, CommandType.Text, connstringWEB, mensaje) = 2 Then
            mensaje = "Se encontro un problema al actualizar el mensaje. " & mensaje
            Return False
        Else
            mensaje = "Mensaje actualizado con éxito"
            Return True
        End If
    End Function
End Class