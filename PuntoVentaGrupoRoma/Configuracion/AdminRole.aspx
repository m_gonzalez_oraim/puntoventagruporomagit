﻿<%@ Page Title="Roles" Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.Master" CodeBehind="AdminRole.aspx.vb" Inherits="PuntoVentaGrupoRoma.AdminRole" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <ext:XScript ID="XScript1" runat="server">
    <script type="text/javascript">
        function editarRol(idRegistro) {
            Ext.Ajax.request({
                url: 'AdminRole.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminRole.aspx'; //the location you want your browser to be  redirected.
                },
                params: { idRegistroEditar: idRegistro  }  // Put your json data here or just enter the JSON object you wannat post
            })
        }

        function interfazCrearRol() {
            Ext.Ajax.request({
                url: 'AdminRole.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminRole.aspx'; //the location you want your browser to be  redirected.
                },
                params: { nuevoRegistro: true}  // Put your json data here or just enter the JSON object you wannat post
            })

        }

        function eliminarRol(idRegistro) {
            var quitarRol;
            Ext.Msg.confirm("Mensaje Sistema", "Eliminar Rol?", function (buttonId) {
                if (buttonId == "yes") {
                    // return true; 
                    quitarRol = true;
                } else {
                    //return false; 
                    quitarRol = false;
                }
                if (quitarRol) {
                    Ext.Ajax.request({
                        url: 'AdminRole.aspx',    // To Which url you wanna POST.
                        success: function () {
                            window.location = 'AdminRole.aspx'; //the location you want your browser to be  redirected.
                        },
                        failure: function (status) {
                            Ext.Msg.alert(status.statusText);
                        },
                        params: { idRegistroEliminar: idRegistro }  // Put your json data here or just enter the JSON object you wannat post
                    });
                }
            });

        }

        function cancelarActualizarRol() {
            window.location = 'AdminRole.aspx';
        }


        function cancelarInsertarRol() {
            window.location= 'AdminRole.aspx';
        }

        var Insertar = {
            insertarRol: function () {
                var forma = Ext.getCmp("FrmRolCrear");
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Rol creado con exito");
                                window.location = 'AdminRole.aspx';
                            }
                            else
                            {
                                Ext.Msg.alert("Error al crear el Rol. Consulte con el administrador");
                            }
                        }
                    });
                }

            }
        }

        var Actualizar = {
            actualizarRol: function () {
                var forma = Ext.getCmp("FrmRolActualizar");
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Rol actualizado con exito");
                            }
                            else
                            {
                                Ext.Msg.alert("Error al actualizar el Rol. Consulte con el administrador")
                            }
                        }
                    });
                }
            }
        }        
</script>
 
    </ext:XScript>
        
    <ext:FormPanel ID="PanelConsultar" runat="server" Width="500" ButtonAlign="Center"  Layout="AnchorLayout"  Border="false">
        <Items>
        <ext:GridPanel 
            ID="GridRol" 
            runat="server"
            Icon="Table"
            Title="Roles"
            AutoRender ="true" 
            AutoScroll="true" 
            Width="450" 
            Region="Center" PaddingSpec="0 0 10 0">
            <Store>
                <ext:Store
                    ID="StoreGridRol" 
                    runat="server" 
                    OnReadData="ObtenerRoles"
                    Enabled="false">
                    <Proxy>
                        <ext:PageProxy>
                            <Reader>
                                <ext:JsonReader Root="data" />
                            </Reader>
                        </ext:PageProxy>
                    </Proxy>
                    <Model>
                        <ext:Model runat="server">
                            <Fields>
                                <ext:ModelField Name="AdminRoleID" />
                                <ext:ModelField Name="RoleName"  />
                                <ext:ModelField Name="StatusName"  />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>           
            <ColumnModel>
                <Columns>
                    <ext:TemplateColumn runat="server" DataIndex="AdminRoleID" Width="80" TemplateString='<a href="#" onclick="eliminarRol({AdminRoleID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.delete.png" /></a>'/>
                    <ext:TemplateColumn runat="server" DataIndex="AdminRoleID" Width="80" TemplateString='<a href="#" onclick="editarRol({AdminRoleID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.edit.png" /></a>'/>
                    <ext:Column runat="server" Header="Rol" DataIndex="RoleName" Width="200" />
                    <ext:Column runat="server" Header="Estado" DataIndex="StatusName" Width="40" />                    
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" ForceFit="true">
                    </ext:GridView>
                </View>
        </ext:GridPanel>
        <ext:Button runat="server" ID="BotonNuevo" Text="Nuevo" OnClientClick="interfazCrearRol()" />
        </Items>
    </ext:FormPanel>
    
    
    <ext:Panel ID="PanelActualizar" runat="server" Width="500">
        <Items>
            <ext:FormPanel ID="FrmRolActualizar"  ClientIDMode="Static" runat="server" LabelAlign="Right" Title="Actualizacion Rol" Width="500">
                <Items>
                    <ext:FieldContainer runat="server" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:TextField ID="txtRoleNameAct" Name="txtRoleNameAct"  runat="server" FieldLabel="Rol" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbStatusAct" Name="cmbStatusAct" runat="server" FieldLabel="Estado" AllowBlank="false" Width="300">
                                <Items>
                                    <ext:ListItem Value="A" Text="Activo" />
                                    <ext:ListItem Value="I" Text="Inactivo" />
                                </Items>
                            </ext:ComboBox>
                            </Items> 
                    </ext:FieldContainer>
                    
                    <ext:Label Text="Permisos del Rol" runat="server" MarginSpec="10 10 10 10" />
                    <ext:FieldContainer ID="fldPermisosAct" runat="server" Layout="VBoxLayout" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:FieldContainer runat="server" Layout="HBoxLayout">
                                <Items>
                                    <ext:Label Text="Permiso"  runat="server" Width="200"/>
                                    <ext:Label Text="Crear"  runat="server" Width="70"/>
                                    <ext:Label Text="Actualizar"  runat="server" Width="70"/>
                                    <ext:Label Text="Consultar"  runat="server" Width="70"/>
                                    <ext:Label Text="Cancelar"  runat="server" Width="70"/>
                                </Items>
                            </ext:FieldContainer>
                        </Items>
                    </ext:FieldContainer>
                    <ext:Hidden runat="server" ID="hdnNumeroPermisosAct" Name="hdnNumeroPermisos"/>
                    <ext:Hidden runat="server" ID="hdnIdRol" Name="hdnIdRol" />
                    <ext:Hidden runat="server" ID="hdnActualizar" Name="hdnActualizar" Text="1"/>

                </Items>
                <Buttons>
                    <ext:Button 
                        Id="btnActualizar"
                        runat="server"
                        Text="Actualizar">
                        <Listeners>
                            <Click Handler="Actualizar.actualizarRol()" />
                        </Listeners>
                    </ext:Button>
                    <ext:Button
                        runat="server"
                        Text="Cancelar" OnClientClick="cancelarActualizarRol()">
                    </ext:Button>
                </Buttons>

            </ext:FormPanel>
        </Items>
    </ext:Panel>

    <ext:Panel ID="PanelCrear" runat="server" Width="500">
        <Items>
            <ext:FormPanel ID="FrmRolCrear"   ClientIDMode="Static" runat="server" LabelAlign="Right" Title="Creacion Rol" Width="500">
                <Items>
                    <ext:FieldContainer runat="server" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:TextField ID="txtRoleNameIns" Name="txtRoleNameIns"  runat="server" FieldLabel="Rol" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbStatusIns" Name="cmbStatusIns" runat="server" FieldLabel="Estado" AllowBlank="false" Width="300">
                                <Items>
                                    <ext:ListItem Value="A" Text="Activo" />
                                    <ext:ListItem Value="I" Text="Inactivo" />
                                </Items>
                            </ext:ComboBox>
                            </Items> 
                    </ext:FieldContainer>
                    
                    <ext:Label Text="Permisos del Rol" runat="server" MarginSpec="10 10 10 10" />
                    <ext:FieldContainer ID="fldPermisosIns" runat="server" Layout="VBoxLayout" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:FieldContainer runat="server" Layout="HBoxLayout">
                                <Items>
                                    <ext:Label Text="Permiso"  runat="server" Width="200"/>
                                    <ext:Label Text="Crear"  runat="server" Width="70"/>
                                    <ext:Label Text="Actualizar"  runat="server" Width="70"/>
                                    <ext:Label Text="Consultar"  runat="server" Width="70"/>
                                    <ext:Label Text="Cancelar"  runat="server" Width="70"/>
                                </Items>
                            </ext:FieldContainer>
                        </Items>
                    </ext:FieldContainer>
                    <ext:Hidden runat="server" ID="hdnNumeroPermisosIns" Name="hdnNumeroPermisos"/>
                    <ext:Hidden runat="server" ID="hdnCrear" Name="hdnCrear" Text="1"/>

                </Items>
                <Buttons>
                    <ext:Button 
                        runat="server"
                        Text="Crear">
                        <Listeners>
                            <Click Handler="Insertar.insertarRol()" />
                        </Listeners>
                    </ext:Button>
                    <ext:Button
                        runat="server"
                        Text="Cancelar" OnClientClick="cancelarInsertarRol()">
                    </ext:Button>
                </Buttons>

            </ext:FormPanel>
        </Items>
    </ext:Panel>

</asp:Content>
