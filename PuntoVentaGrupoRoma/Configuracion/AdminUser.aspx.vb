﻿Imports Ext.Net
Imports System.Net

Public Class AdminUser
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        If Not Ext.Net.X.IsAjaxRequest Then

            Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

            Dim puedeCrear As Boolean = False
            Dim puedeConsultar As Boolean = False
            Dim puedeModificar As Boolean = False
            Dim puedeCancelar As Boolean = False

            Dim controlAcceso As ControlAcceso = New ControlAcceso()

            If Not principalPV.UsuarioPuntoVenta.TienePermiso("Usuarios", puedeCrear, puedeModificar, puedeConsultar, puedeCancelar) Then
                principalPV.UsuarioPuntoVenta.RedireccionaAPaginaNoAcceso(Response)
                Return
            End If

            If Not Session("IdUsuario") Is Nothing Then
                Dim auxIdUsuario As Integer = Session("IdUsuario")
                Session.Remove("IdUsuario")
                interfazModificar(auxIdUsuario, puedeModificar)
                Return
            End If

            If Not Session("CrearUsuarios") Is Nothing Then
                Session.Remove("CrearUsuarios")
                interfazCrear()
                Return
            End If

            If Not Request("hdnAccion") Is Nothing Then
                If Request("hdnAccion") = "I" Then
                    Dim mensajeError As String = String.Empty
                    If Not CrearUsuario(mensajeError) Then
                        Context.Response.StatusCode = HttpStatusCode.InternalServerError
                        Context.Response.ContentType = "text/plain"
                        Context.Response.StatusDescription = mensajeError
                    End If
                    Return
                ElseIf Request("hdnAccion") = "A" Then
                    Dim mensajeError As String = String.Empty
                    If Not ActualizarUsuario(mensajeError) Then
                        Context.Response.StatusCode = HttpStatusCode.InternalServerError
                        Context.Response.ContentType = "text/plain"
                        Context.Response.StatusDescription = mensajeError
                    End If
                    Return
                End If
            End If

            If Not Request("idRegistroEditar") Is Nothing Then
                Dim registroEditar As Integer = Request("idRegistroEditar")
                Session("IdUsuario") = registroEditar
                Return
            End If

            If Not Request("nuevoRegistro") Is Nothing Then
                Session("CrearUsuarios") = True
                Return
            End If

            If Not Request("idRegistroEliminar") Is Nothing Then
                Dim registroQuitar As Integer = Request("idRegistroEliminar")
                Dim mensajeError As String = String.Empty
                If Not EliminarUsuario(registroQuitar, mensajeError) Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = mensajeError
                End If
                Return
            End If

            interfazConsultarCrear(puedeCrear, puedeConsultar, puedeModificar, puedeCancelar)
        End If
    End Sub

    Private Function EliminarUsuario(ByVal idRegistro As Integer, ByRef mensajeError As String)
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Return controlUsuario.EliminarUsuario(idRegistro, mensajeError)
    End Function

    Private Function CrearUsuario(ByRef mensajeError As String) As Boolean
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim usuario As Usuario = Nothing
        controlUsuario.InformacionUsuarioFromRequest(Request, usuario)
        Return controlUsuario.CrearUsuario(usuario, mensajeError)
    End Function

    Private Function ActualizarUsuario(ByRef mensajeError As String) As Boolean
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim usuario As Usuario = Nothing
        controlUsuario.InformacionUsuarioFromRequest(Request, usuario)
        Return controlUsuario.ActualizarUsuario(usuario, mensajeError)
    End Function

    Public Sub ObtenerUsuarios(sender As Object, e As StoreReadDataEventArgs)
        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim store As Store = sender
        store.Data = controlUsuario.ListadoUsuarios()
    End Sub

    Private Sub interfazConsultarCrear(ByVal puedeCrear As Boolean, ByVal puedeConsultar As Boolean, ByVal puedeModificar As Boolean, ByVal puedeEliminar As Boolean)
        If Not puedeConsultar AndAlso Not puedeModificar Then
            GridUsuario.GetStore().Enabled = False
            GridUsuario.Visible = False
            GridUsuario.Hidden = True
        End If

        If Not puedeEliminar Then
            GridUsuario.ColumnModel.Columns(0).Visible = False
        End If

        If Not puedeCrear Then
            BotonNuevo.Visible = False
        End If

        PanelUsuario.Visible = False
        PanelConsultar.Center()
    End Sub


    Private Sub interfazModificar(ByVal idUsuario As Integer, ByVal puedeModificar As Boolean)
        GridUsuario.GetStore().Enabled = False
        PanelConsultar.Visible = False

        PanelUsuario.Visible = True
        hdnAccion.Value = "A"
        btnAccionFormulario.Text = "Actualizar"
        btnAccionFormulario.OnClientClick = "Actualizar.actualizarUsuario"

        txtNTUserDomain.Destroy()
        txtNTUserDomain2.Destroy()
        PanelUsuario.Center()

        Dim controlRol As ControlRol = New ControlRol()
        Dim controlUsuario As ControlUsuario = New ControlUsuario()

        Dim listadoRoles As Dictionary(Of Integer, String) = controlRol.ListadoRolesUsuario(False)
        For Each rol In listadoRoles
            cmbAdminRoleID.Items.Add(New Ext.Net.ListItem(rol.Value, rol.Key))
        Next

        Dim infoUsuario As Usuario = controlUsuario.InformacionUsuario(idUsuario)
        If infoUsuario Is Nothing Then
            Response.Redirect("AdminUser.aspx")
        End If

        infoUsuario.AdminUserID = idUsuario
        txtFirstName.Value = infoUsuario.FirstName
        txtFirstName.ReadOnly = Not puedeModificar
        txtLastName.Value = infoUsuario.LastName
        txtLastName.ReadOnly = Not puedeModificar
        cmbAdminRoleID.Value = infoUsuario.AdminRoleID
        cmbAdminRoleID.ReadOnly = Not puedeModificar
        txtEmployeeNum.Value = infoUsuario.EmployeeNum
        txtEmployeeNum.ReadOnly = Not puedeModificar
        txtPositionEmployee.Value = infoUsuario.PositionEmployee
        txtPositionEmployee.ReadOnly = Not puedeModificar
        chkEsVendedor.Checked = infoUsuario.EsVendedor
        chkEsVendedor.ReadOnly = Not puedeModificar
        txtNTUserAccount.Value = infoUsuario.NTUserAccount
        txtNTUserAccount.ReadOnly = Not puedeModificar
        cmbStatus.Value = infoUsuario.Status
        cmbStatus.ReadOnly = Not puedeModificar
        chkFactoresAll.Checked = infoUsuario.TodosLosFactores
        chkFactoresAll.ReadOnly = Not puedeModificar

        construyeSeccionTiendas(infoUsuario.TiendasAsignadas, puedeModificar)

        hdnIdUsuario.Value = idUsuario
    End Sub

    Private Sub interfazCrear()
        GridUsuario.GetStore().Enabled = False
        PanelConsultar.Visible = False

        PanelUsuario.Center()

        hdnIdUsuario.Destroy()
        hdnAccion.Value = "I"
        btnAccionFormulario.Text = "Crear"
        btnAccionFormulario.OnClientClick = "Insertar.insertarUsuario"

        Dim controlUsuario As ControlUsuario = New ControlUsuario()
        Dim controlRol As ControlRol = New ControlRol()

        Dim listadoRoles As Dictionary(Of Integer, String) = controlRol.ListadoRolesUsuario(True)
        For Each rol In listadoRoles
            cmbAdminRoleID.Items.Add(New Ext.Net.ListItem(rol.Value, rol.Key))
        Next

        txtFirstName.Value = String.Empty
        txtLastName.Value = String.Empty
        cmbAdminRoleID.SelectedItem.Index = 0
        txtEmployeeNum.Value = String.Empty
        txtPositionEmployee.Value = String.Empty
        chkEsVendedor.Checked = False
        txtNTUserAccount.Value = String.Empty
        txtNTUserDomain.Value = String.Empty
        txtNTUserDomain2.Value = String.Empty
        cmbStatus.SelectedItem.Index = 0
        chkFactoresAll.Checked = False

        Dim tiendasAsignar As List(Of TiendaAsignadaUsuario) = controlUsuario.TiendasAsignadasUsuario(-1, True)
        construyeSeccionTiendas(tiendasAsignar, True)

    End Sub

    Private Sub construyeSeccionTiendas(ByRef tiendas As List(Of TiendaAsignadaUsuario), ByVal puedeModificar As Boolean)
        For i As Integer = 0 To tiendas.Count - 1
            construyeLineaTienda(i, tiendas(i), puedeModificar)
        Next
        hdnNumeroTiendas.Value = tiendas.Count
    End Sub

    Private Sub construyeLineaTienda(ByVal numeroPermiso As Integer, ByVal tienda As TiendaAsignadaUsuario, ByVal puedeModificar As Boolean)
        Dim containerPermiso As Ext.Net.FieldContainer = New FieldContainer()
        containerPermiso.Layout = "HBoxLayout"
        Dim labelPermiso As Ext.Net.Label = New Label()
        labelPermiso.Text = tienda.StoreName
        labelPermiso.Width = 300
        Dim checkAcceso As Ext.Net.Checkbox = New Checkbox()
        checkAcceso.Checked = tienda.UserAdminStoreID <> -1
        checkAcceso.Width = 90
        checkAcceso.Name = "chkAcceso" & numeroPermiso
        checkAcceso.ReadOnly = Not puedeModificar OrElse tienda.Status = "I"
        Dim checkTransferencia As Ext.Net.Checkbox = New Checkbox()
        checkTransferencia.Width = 90
        checkTransferencia.Checked = tienda.UserAdminStoreToSendID <> -1
        checkTransferencia.Name = "chkTrans" & numeroPermiso
        checkTransferencia.ReadOnly = Not puedeModificar OrElse tienda.Status = "I"
        Dim hiddenIdTienda As Ext.Net.Hidden = New Hidden()
        hiddenIdTienda.Name = "hdnTienda" & numeroPermiso
        hiddenIdTienda.Value = tienda.AdminStoreID

        containerPermiso.Items.Add(labelPermiso)
        containerPermiso.Items.Add(checkAcceso)
        containerPermiso.Items.Add(checkTransferencia)
        containerPermiso.Items.Add(hiddenIdTienda)

        fldTiendas.Add(containerPermiso)

    End Sub



End Class