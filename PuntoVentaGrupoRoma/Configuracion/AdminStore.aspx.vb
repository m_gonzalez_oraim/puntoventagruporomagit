﻿Imports Ext.Net
Imports System.Net

Public Class AdminStore
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Ext.Net.X.IsAjaxRequest Then
            Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

            Dim puedeCrear As Boolean = False
            Dim puedeConsultar As Boolean = False
            Dim puedeModificar As Boolean = False
            Dim puedeCancelar As Boolean = False

            Dim controlAcceso As ControlAcceso = New ControlAcceso()

            If Not principalPV.UsuarioPuntoVenta.TienePermiso("Tiendas", puedeCrear, puedeModificar, puedeConsultar, puedeCancelar) Then
                principalPV.UsuarioPuntoVenta.RedireccionaAPaginaNoAcceso(Response)
                Return
            End If

            If Not Session("IdTienda") Is Nothing Then
                Dim auxIdTienda As Integer = Session("IdTienda")
                Session.Remove("IdTienda")
                interfazModificar(auxIdTienda, puedeModificar)
                Return
            End If

            If Not Session("CrearTiendas") Is Nothing Then
                Session.Remove("CrearTiendas")
                interfazCrear()
                Return
            End If

            If Not Request("hdnGuardarFactores") Is Nothing AndAlso Request("hdnGuardarFactores") = "1" Then
                Dim idTienda As Integer = Request("hdnStoreFactores")
                Dim listaFactoresDefinidos As List(Of ArticuloFactor) = New List(Of ArticuloFactor)()
                Dim defFactores As Newtonsoft.Json.Linq.JArray = Ext.Net.JSON.Deserialize(Request("hdnDefinicionFactores"))
                Dim auxArticuloFactor As ArticuloFactor
                For i As Integer = 0 To defFactores.Count - 1
                    auxArticuloFactor = New ArticuloFactor()
                    auxArticuloFactor.CodigoArticulo = defFactores(i).Item("CodigoArticulo")
                    auxArticuloFactor.FactorActual = defFactores(i).Item("FactorActual")
                    auxArticuloFactor.FactorNuevo = defFactores(i).Item("FactorNuevo")
                    If auxArticuloFactor.FactorActual <> auxArticuloFactor.FactorNuevo Then
                        listaFactoresDefinidos.Add(auxArticuloFactor)
                    End If
                Next
                Dim controlArticulos As ControlArticulos = New ControlArticulos()
                If Not controlArticulos.GuardarDefinicionFactores(listaFactoresDefinidos, idTienda) Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = "Error al actualizar la definicion de factores. Consulte con su administrador"
                End If
                Return
            End If

            If Not Request("hdnAccion") Is Nothing Then
                If Request("hdnAccion") = "I" Then
                    Dim mensajeError As String = String.Empty
                    If Not CrearTienda(mensajeError) Then
                        Context.Response.StatusCode = HttpStatusCode.InternalServerError
                        Context.Response.ContentType = "text/plain"
                        Context.Response.StatusDescription = mensajeError
                    End If
                    Return
                ElseIf Request("hdnAccion") = "A" Then
                    Dim mensajeError As String = String.Empty
                    If Not ActualizarTienda(mensajeError) Then
                        Context.Response.StatusCode = HttpStatusCode.InternalServerError
                        Context.Response.ContentType = "text/plain"
                        Context.Response.StatusDescription = mensajeError
                    End If
                    Return
                End If
            End If

            If Not Request("idRegistroEditar") Is Nothing Then
                Dim registroEditar As Integer = Request("idRegistroEditar")
                Session("IdTienda") = registroEditar
                Return
            End If

            If Not Request("nuevoRegistro") Is Nothing Then
                Session("CrearTiendas") = True
                Return
            End If

            If Not Request("idRegistroEliminar") Is Nothing Then
                Dim registroQuitar As Integer = Request("idRegistroEliminar")
                Dim mensajeError As String = String.Empty
                If Not EliminarTienda(registroQuitar, mensajeError) Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = mensajeError
                End If
                Return
            End If

            interfazConsultarCrear(puedeCrear, puedeConsultar, puedeModificar, puedeCancelar)
        End If
    End Sub

    Private Function EliminarTienda(ByVal idRegistro As Integer, ByRef mensajeError As String)
        Dim controlTienda As ControlTienda = New ControlTienda()
        Return controlTienda.EliminarTienda(idRegistro, mensajeError)
    End Function

    Private Function CrearTienda(ByRef mensajeError As String) As Boolean
        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim tienda As Tienda = Nothing
        controlTienda.InformacionTiendaFromRequest(Request, tienda)
        Return controlTienda.CrearTienda(tienda, mensajeError)
    End Function

    Private Function ActualizarTienda(ByRef mensajeError As String) As Boolean
        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim tienda As Tienda = Nothing
        controlTienda.InformacionTiendaFromRequest(Request, tienda)
        Return controlTienda.ActualizarTienda(tienda, mensajeError)
    End Function

    Public Sub ObtenerTiendas(sender As Object, e As StoreReadDataEventArgs)
        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim store As Store = sender
        store.Data = controlTienda.ListadoTiendas()
    End Sub

    Private Sub interfazConsultarCrear(ByVal puedeCrear As Boolean, ByVal puedeConsultar As Boolean, ByVal puedeModificar As Boolean, ByVal puedeEliminar As Boolean)
        If Not puedeConsultar AndAlso Not puedeModificar Then
            GridTienda.GetStore().Enabled = False
            GridTienda.Visible = False
            GridTienda.Hidden = True
        End If

        If Not puedeEliminar Then
            GridTienda.ColumnModel.Columns(0).Visible = False
        End If

        If Not puedeCrear Then
            BotonNuevo.Visible = False
        End If

        PanelTienda.Visible = False
        PanelConsultar.Center()
    End Sub


    Private Sub interfazModificar(ByVal idTienda As Integer, ByVal puedeModificar As Boolean)
        GridTienda.GetStore().Enabled = False
        PanelConsultar.Visible = False

        PanelTienda.Visible = True
        hdnAccion.Value = "A"
        btnAccionFormulario.Text = "Actualizar"
        btnAccionFormulario.OnClientClick = "Actualizar.actualizarTienda"

        PanelTienda.Center()

        inicializarCombos()

        Dim controlTienda As ControlTienda = New ControlTienda()

        Dim infoTienda As Tienda = controlTienda.InformacionTienda(idTienda)
        If infoTienda Is Nothing Then
            Response.Redirect("AdminTienda.aspx")
        End If

        infoTienda.AdminStoreID = idTienda

        txtStoreName.Text = infoTienda.StoreName
        txtStoreName.ReadOnly = Not puedeModificar
        cmbStatus.SelectedItem.Value = infoTienda.Status
        cmbStatus.ReadOnly = Not puedeModificar
        txtCalle.Text = infoTienda.Calle
        txtCalle.ReadOnly = Not puedeModificar
        txtNumExt.Text = infoTienda.NumExt
        txtNumExt.ReadOnly = Not puedeModificar
        txtNumInt.Text = infoTienda.NumInt
        txtNumInt.ReadOnly = Not puedeModificar
        txtColonia.Text = infoTienda.Colonia
        txtColonia.ReadOnly = Not puedeModificar
        txtCodigoPostal.Text = infoTienda.CodigoPostal
        txtCodigoPostal.ReadOnly = Not puedeModificar
        txtDelegacion.Text = infoTienda.Delegacion
        txtDelegacion.ReadOnly = Not puedeModificar
        cmbEstado.SelectedItem.Value = infoTienda.EstadoId
        cmbEstado.ReadOnly = Not puedeModificar
        txtTelefono.Value = infoTienda.Telefono
        txtTelefono.ReadOnly = Not puedeModificar
        txtEmailTienda.Value = infoTienda.EmailTienda
        txtEmailTienda.ReadOnly = Not puedeModificar

        cmbWhsID.SelectedItem.Value = infoTienda.WhsID
        cmbWhsID.ReadOnly = Not puedeModificar
        cmbConsigWhsID.SelectedItem.Value = infoTienda.ConsigWhsID
        cmbConsigWhsID.ReadOnly = Not puedeModificar
        cmbTransitWhsID.SelectedItem.Value = infoTienda.TransitWhsID
        cmbTransitWhsID.ReadOnly = Not puedeModificar
        cmbDefaultCustomer.SelectedItem.Value = infoTienda.DefaultCustomer
        cmbDefaultCustomer.ReadOnly = Not puedeModificar
        cmbActIVA.Value = infoTienda.ActIVA
        cmbActIVA.ReadOnly = Not puedeModificar
        cmbDefaultList.SelectedItem.Value = infoTienda.DefaultList
        cmbDefaultList.ReadOnly = Not puedeModificar
        cmbPriceListCost.SelectedItem.Value = infoTienda.PriceListCost
        cmbPriceListCost.ReadOnly = Not puedeModificar

        If Not puedeModificar Then
            btnAccionFormulario.Destroy()
            btnDefFactores.Destroy()
        End If
        hdnIdTienda.Value = idTienda
    End Sub

    Private Sub interfazCrear()
        GridTienda.GetStore().Enabled = False
        PanelConsultar.Visible = False

        PanelTienda.Center()

        hdnIdTienda.Destroy()
        hdnAccion.Value = "I"
        btnAccionFormulario.Text = "Crear"
        btnAccionFormulario.OnClientClick = "Insertar.insertarTienda"

        inicializarCombos()

        Dim controlTienda As ControlTienda = New ControlTienda()

        txtStoreName.Text = String.Empty
        cmbStatus.SelectedItem.Index = 0
        txtCalle.Text = String.Empty
        txtNumExt.Text = String.Empty
        txtNumInt.Text = String.Empty
        txtColonia.Text = String.Empty
        txtCodigoPostal.Text = String.Empty
        txtDelegacion.Text = String.Empty
        cmbEstado.SelectedItem.Index = 0
        txtTelefono.Value = String.Empty
        txtEmailTienda.Value = String.Empty

        cmbWhsID.SelectedItem.Index = 0
        cmbTransitWhsID.SelectedItem.Index = 0
        cmbConsigWhsID.SelectedItem.Index = 0
        cmbDefaultCustomer.SelectedItem.Index = 0
        cmbActIVA.Value = String.Empty
        cmbDefaultList.SelectedItem.Index = 0
        cmbPriceListCost.SelectedItem.Index = 0

        btnDefFactores.Destroy()
    End Sub

    <DirectMethod>
    Public Function BuscarArticulosFactores(ByVal storeId As Integer, ByVal articulo1 As String, ByVal articulo2 As String) As Boolean
        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        Dim tablaResultados As System.Data.DataTable = controlArticulo.CargarArticulosAsignarFactores(storeId, articulo1, articulo2)
        grdArticulos.GetStore().DataSource = tablaResultados
        grdArticulos.GetStore().DataBind()

        If tablaResultados.Rows.Count = 0 Then
            Return False
        End If
        Return True

    End Function

    <DirectMethod>
    Public Sub MostrarVentanaDefinicionFactores(ByVal storeId As Integer)
        hdnStoreFactores.Value = storeId
        hdnGuardarFactores.Value = "1"
        txtArt1.Text = String.Empty
        txtArt2.Text = String.Empty
        grdArticulos.Data = Nothing
        grdArticulos.DataBind()
        winFactores.Show()
    End Sub

    <DirectMethod>
    Public Sub OcultarVentanaDefinicionFactores()
        hdnGuardarFactores.Value = ""
        winFactores.Close()
    End Sub

    Private Sub inicializarCombos()

        Dim controlCatalogosSAP As CatalogosSAP = New CatalogosSAP()
        Dim controlCatalogosPOS As ControlCatalogos = New ControlCatalogos()

        Dim almacenes As Dictionary(Of String, String) = controlCatalogosSAP.AlmacenesSAP()
        Dim impuestos As Dictionary(Of String, String) = controlCatalogosPOS.CodigosImpuesto()
        Dim clientes As Dictionary(Of String, String) = controlCatalogosSAP.ClientesMostradorSAP()
        Dim estados As Dictionary(Of String, String) = controlCatalogosPOS.CodigosEstado()
        Dim listasPrecio As Dictionary(Of String, String) = controlCatalogosSAP.ListasPrecioSAP()

        For Each lista In listasPrecio
            cmbPriceListCost.Items.Add(New Ext.Net.ListItem(lista.Value, lista.Key))
            cmbDefaultList.Items.Add(New Ext.Net.ListItem(lista.Value, lista.Key))
        Next

        For Each estado In estados
            cmbEstado.Items.Add(New Ext.Net.ListItem(estado.Value, estado.Key))
        Next

        For Each impuesto In impuestos
            cmbActIVA.Items.Add(New Ext.Net.ListItem(impuesto.Value, impuesto.Key))
        Next

        For Each cliente In clientes
            cmbDefaultCustomer.Items.Add((New Ext.Net.ListItem(cliente.Value, cliente.Key)))
        Next

        For Each almacen In almacenes
            cmbWhsID.Items.Add(New Ext.Net.ListItem(almacen.Value, almacen.Key))
            cmbConsigWhsID.Items.Add(New Ext.Net.ListItem(almacen.Value, almacen.Key))
            cmbTransitWhsID.Items.Add(New Ext.Net.ListItem(almacen.Value, almacen.Key))
        Next

    End Sub

End Class