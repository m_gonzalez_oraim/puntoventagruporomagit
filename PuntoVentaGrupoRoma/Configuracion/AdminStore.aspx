﻿<%@ Page Title="Roles" Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.Master" CodeBehind="AdminStore.aspx.vb" Inherits="PuntoVentaGrupoRoma.AdminStore" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <ext:XScript ID="XScript1" runat="server">
    <script type="text/javascript">
        function ocultarVentanaFactores() {
            App.direct.OcultarVentanaDefinicionFactores();
        }

        function mostrarVentanaFactores() {
            var idTienda = Ext.getCmp("hdnIdTienda").getValue();
            App.direct.MostrarVentanaDefinicionFactores(idTienda);
        }

        function guardarFactoresArticulo() {
            var store = Ext.getCmp("grdArticulos").store;
            var registrosModificados = store.getModifiedRecords();
            var datosModificados = new Array();

            Ext.each(registrosModificados, function (record) {
                datosModificados.push(record.data);
            });
            if (datosModificados.length == 0){
                Ext.Msg.alert("No se eligieron nuevos factores para los articulos buscados");
                return;
            }
            Ext.getCmp("hdnGuardarFactores").setValue("1");
            Ext.getCmp("hdnDefinicionFactores").setValue(Ext.encode(datosModificados));

            var forma = Ext.getCmp("FrmFactores");
            forma.submit({
                failure: function (a, b) {
                    if (b.response.status == 200) {
                        Ext.Msg.alert("Factores actualizados con exito");
                    }
                    else {
                        Ext.Msg.alert(b.response.statusText);
                    }
                }
            });
        }

        function buscarArticulosFactores() {
            var art1 = Ext.getCmp("txtArt1").getValue();
            var art2 = Ext.getCmp("txtArt2").getValue();
            if(art1=="" && art2=="")
            {
                Ext.Msg.alert("Debe definir algun parametro para realizar la busqueda de articulos");
                return;
            }
            var tienda = Ext.getCmp("hdnStoreFactores").getValue();
            App.direct.BuscarArticulosFactores(tienda, art1, art2, {
                success: function (result) {
                    if (!result) {
                        Ext.Msg.alert("No se encontraron artículos con los parametros de busqueda capturados");
                    }
                },
                failure: function (errorMsg) {
                    Ext.Msg.alert('Failure', errorMsg);
                }
            });
        }

        function editarTienda(idRegistro) {
            Ext.Ajax.request({
                url: 'AdminStore.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminStore.aspx'; //the location you want your browser to be  redirected.
                },
                params: { idRegistroEditar: idRegistro  }  // Put your json data here or just enter the JSON object you wannat post
            })
        }

        function interfazCrearTienda() {
            Ext.Ajax.request({
                url: 'AdminStore.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminStore.aspx'; //the location you want your browser to be  redirected.
                },
                params: { nuevoRegistro: true}  // Put your json data here or just enter the JSON object you wannat post
            })

        }

        function eliminarTienda(idRegistro) {
            var quitarUsuario;
            Ext.Msg.confirm("Mensaje Sistema", "Eliminar Tienda?", function (buttonId) {
                if (buttonId == "yes") {
                    // return true; 
                    quitarUsuario = true;
                } else {
                    //return false; 
                    quitarUsuario = false;
                }
                if (quitarUsuario) {
                    Ext.Ajax.request({
                        url: 'AdminStore.aspx',    // To Which url you wanna POST.
                        success: function () {
                            window.location = 'AdminStore.aspx'; //the location you want your browser to be  redirected.
                        },
                        failure: function (status) {
                            Ext.Msg.alert(status.statusText);
                        },
                        params: { idRegistroEliminar: idRegistro }  // Put your json data here or just enter the JSON object you wannat post
                    });
                }
            });
        }

        function cancelarAccionFormulario() {
            window.location = 'AdminStore.aspx';
        }

        var Insertar = {
            insertarTienda: function () {
                var forma = Ext.getCmp("FrmTienda");
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Tienda creada con exito");
                                window.location = 'AdminStore.aspx';
                            }
                            else
                            {
                                Ext.Msg.alert(b.response.statusText);
                            }
                        }
                    });
                }
            }
        }

        var Actualizar = {
            actualizarTienda: function () {
                var forma = Ext.getCmp("FrmTienda");
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Tienda actualizada con exito");
                            }
                            else
                            {
                                Ext.Msg.alert(b.response.statusText);
                            }
                        }
                    });
                }
            }
        }

        function SeConfirmoCreacionCliente(){

        }

</script>
 
    </ext:XScript>
        
    <ext:FormPanel ID="PanelConsultar" runat="server" Width="600" ButtonAlign="Center"  Layout="AnchorLayout"  Border="false">
        <Items>
        <ext:GridPanel 
            ID="GridTienda" 
            runat="server"
            Icon="Table"
            Title="Tiendas"
            AutoRender ="true" 
            AutoScroll="true" 
            Width="550" 
            Region="Center" PaddingSpec="0 0 10 0">
            <Store>
                <ext:Store
                    ID="StoreGridTienda" 
                    runat="server" 
                    OnReadData="ObtenerTiendas"
                    Enabled="false">
                    <Proxy>
                        <ext:PageProxy>
                            <Reader>
                                <ext:JsonReader Root="data" />
                            </Reader>
                        </ext:PageProxy>
                    </Proxy>
                    <Model>
                        <ext:Model runat="server">
                            <Fields>
                                <ext:ModelField Name="AdminStoreID" />
                                <ext:ModelField Name="StoreName" />
                                <ext:ModelField Name="StatusName"  />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>           
            <ColumnModel>
                <Columns>
                    <ext:TemplateColumn runat="server" DataIndex="AdminStoreID" Width="80" TemplateString='<a href="#" onclick="eliminarTienda({AdminStoreID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.delete.png" /></a>'/>
                    <ext:TemplateColumn runat="server" DataIndex="AdminStoreID" Width="80" TemplateString='<a href="#" onclick="editarTienda({AdminStoreID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.edit.png" /></a>'/>
                    <ext:Column runat="server" Header="Tienda" DataIndex="StoreName" Width="200" />
                    <ext:Column runat="server" Header="Estado" DataIndex="StatusName" Width="40" />                    
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" ForceFit="true">
                    </ext:GridView>
                </View>
        </ext:GridPanel>
        <ext:Button runat="server" ID="BotonNuevo" Text="Nuevo" OnClientClick="interfazCrearTienda()" />
        </Items>
    </ext:FormPanel>
    
    
    <ext:Panel ID="PanelTienda" runat="server" Width="500">
        <Items>
            <ext:FormPanel ID="FrmTienda"  ClientIDMode="Static" runat="server" LabelAlign="Right" Width="500">
                <Items>
                    <ext:FieldContainer runat="server" MarginSpec="10 10 10 10"  >
                        <Items>
                            <ext:TextField ID="txtStoreName"  Name="txtStoreName"  runat="server" FieldLabel="Tienda" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbStatus" Name="cmbStatus" runat="server" FieldLabel="Estado" AllowBlank="false" Width="300">
                                <Items>
                                    <ext:ListItem Value="A" Text="Activo" />
                                    <ext:ListItem Value="I" Text="Inactivo" />
                                </Items>
                            </ext:ComboBox>
                            <ext:TextField ID="txtCalle" Name="txtCalle" runat="server" FieldLabel="Calle" Width="300" />
                            <ext:TextField ID="txtNumExt" Name="txtNumExt" runat="server" FieldLabel="# Exterior" Width="300" />
                            <ext:TextField ID="txtNumInt" Name="txtNumInt" runat="server" FieldLabel="# Interior" Width="300" />
                            <ext:TextField ID="txtColonia" Name="txtColonia" runat="server" FieldLabel="Colonia" Width="300" />
                            <ext:TextField ID="txtCodigoPostal" Name="txtCodigoPostal" runat="server" FieldLabel="Codigo Postal" Width="300" />
                            <ext:TextField ID="txtDelegacion" Name="txtDelegacion" runat="server" FieldLabel="Municipio" Width="300" />
                            <ext:ComboBox ID="cmbEstado" Name="cmbEstado" runat="server" FieldLabel="Estado" Width="300" />
                            <ext:TextField ID="txtTelefono" Name="txtTelefono" runat="server" FieldLabel="Telefono" Width="300" />
                            <ext:TextField ID="txtEmailTienda" Name="txtEmailTienda" runat="server" FieldLabel="Email" Width="300" />
                        </Items>
                    </ext:FieldContainer>
                    <ext:FieldContainer runat="server" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:ComboBox ID="cmbDefaultList" Name="cmbDefaultList" runat="server" FieldLabel="Lista Precios" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbPriceListCost" Name="cmbPriceListCost" runat="server" FieldLabel="Lista Costos" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbWhsID" Name="cmbWhsID" runat="server" FieldLabel="Almacen Venta" AllowBlank ="false" Width="300" />
                            <ext:ComboBox ID="cmbConsigWhsID" Name="cmbConsigWhsID" runat="server" FieldLabel="Almacen Consigna" AllowBlank ="false" Width="300" />
                            <ext:ComboBox ID="cmbTransitWhsID" Name="cmbTransitWhsID" runat="server" FieldLabel="Almacen Transito" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbDefaultCustomer" Name="cmbDefaultCustomer" runat="server" FieldLabel="Cliente Mostrador" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbActIVA" Name="cmbActIVA" runat="server" FieldLabel="Impuesto" AllowBlank="false" Width="300" />
                            
                        </Items> 
                    </ext:FieldContainer>
                    
                    <ext:Hidden runat="server" ClientIDMode="Static" ID="hdnIdTienda" Name="hdnIdTienda" />
                    <ext:Hidden runat="server" ID="hdnAccion" Name="hdnAccion" />
                </Items>
                <Buttons>
                    <ext:Button 
                        runat="server"
                        Id="btnAccionFormulario">
                    </ext:Button>
                    <ext:Button 
                        runat="server"
                        Id="btnDefFactores"
                        Text="Factores" OnClientClick="mostrarVentanaFactores()">
                    </ext:Button>

                    <ext:Button
                        runat="server"
                        Text="Cancelar" OnClientClick="cancelarAccionFormulario()">
                    </ext:Button>
                </Buttons>
            </ext:FormPanel>
        </Items>
    </ext:Panel>

    <ext:Window 
        title="Factores"
        runat="server"
        ID="winFactores"
        Modal="true"
        Hidden="true"
        Width="800"
        Height="500"
        Maximizable="false"
        Layout="HBoxLayout"
        Border="false">
        <Content>
            <ext:FormPanel
                ID="FrmFactores"
                ClientIDMode="Static"
                runat="server"
                Region="Center"
                Layout="TableLayout"
                Border="false"
                Width="800"
                Height="500"
                >
                <LayoutConfig>
                    <ext:TableLayoutConfig Columns="5" />
                </LayoutConfig>
                <Items>
                    <ext:Panel
                        id="Panel1"
                        runat="server"
                        Border="false"
                        Width="100"
                        >
                        <Items>
                          <ext:Label runat="server" Text="Articulo" MarginSpec="5 0 0 10" />
                        </Items>
                    </ext:Panel>
                    <ext:Panel
                        id="Panel2"
                        Border="false"
                        runat="server"
                        Width="200">
                        <Items>
                          <ext:TextField 
                              runat="server"
                              FieldLabel="Desde"
                              LabelWidth="50"
                              ID="txtArt1"
                              ClientIDMode="Static" 
                              MarginSpec="5 0 5 0"/>
                        </Items>
                    </ext:Panel>
                    <ext:Panel
                        id="Panel3"
                        Border="false"
                        runat="server"
                        Html="&nbsp;"
                        Width="50"/>
                    <ext:Panel
                        id="Panel4"
                        Border="false"
                        runat="server"
                        Width="200">
                        <Items>
                          <ext:TextField 
                              runat="server"
                              FieldLabel="Hasta"
                              ID="txtArt2"
                              LabelWidth="50"
                              ClientIDMode="Static" 
                              MarginSpec="5 0 5 0"/>
                        </Items>
                    </ext:Panel>
                    <ext:Panel
                        id="Panel5"
                        border="false"
                        runat="server"
                        Html="&nbsp;"
                        Width="200" />
                    <ext:Panel
                        id="PanelBoton"
                        border="false"
                        ColSpan="4"
                        runat="server" 
                        Layout="FormLayout"
                        ButtonAlign="Right" 
                        Width="550">
                        <Items>
                            <ext:Button ID="btnBuscar" runat="server" Text="Buscar" MarginSpec="5 0 5 500" OnClientClick="buscarArticulosFactores()" />
                        </Items>
                    </ext:Panel>
                    <ext:Panel
                        runat="server"
                        border="false"
                        Html="&nbsp;" />
                    <ext:GridPanel
                        ID="grdArticulos"
                        runat="server"
                        ColSpan="5"
                        Height="360"
                        ClientIDMode="Static">
                        <Store>
                            <ext:Store
                             ID="StoreGridArticulos" 
                            runat="server" >
                                <Model>
                                    <ext:Model runat="server">
                                        <Fields>
                                            <ext:ModelField Name="CodigoArticulo" />
                                            <ext:ModelField Name="NombreArticulo" />
                                            <ext:ModelField Name="FactorActual" />
                                            <ext:ModelField Name="FactorNuevo" />
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <ColumnModel>
                            <Columns>
                                <ext:Column runat="server" Header="Articulo" DataIndex="CodigoArticulo" Width="100" />
                                <ext:Column runat="server" Header="Nombre" DataIndex="NombreArticulo" Width="300" />
                                <ext:TemplateColumn runat="server" Header="Factor Act" DataIndex="FactorActual" Width="120" >
                                    <Template runat="server">
                                        <Html>
                                            <tpl if="FactorActual == ' '">
                                                Ninguno
                                            </tpl>
                                            <tpl if="FactorActual != ' '">
                                                Factor {FactorActual}
                                            </tpl>
                                        </Html>
                                    </Template>
                                </ext:TemplateColumn>                                
                                 <ext:ComponentColumn 
                                    runat="server" 
                                    Editor="true"
                                    Header="Factor Nuevo" 
                                     DataIndex="FactorNuevo" 
                                     Width="120"
                                    Flex="1">
                                    <Component>
                                        <ext:ComboBox runat="server">
                                            <Items>
                                                <ext:ListItem Text="Ninguno" Value=" " />
                                                <ext:ListItem Text="Factor A" Value="A" />
                                                <ext:ListItem Text="Factor B" Value="B" />
                                                <ext:ListItem Text="Factor C" Value="C" />
                                                <ext:ListItem Text="Factor D" Value="D" />
                                                <ext:ListItem Text="Factor E" Value="E" />
                                                <ext:ListItem Text="Factor F" Value="F" />
                                                <ext:ListItem Text="Factor G" Value="G" />
                                                <ext:ListItem Text="Factor H" Value="H" />
                                            </Items>
                                        </ext:ComboBox>
                                    </Component>
                                </ext:ComponentColumn>
                            </Columns>
                        </ColumnModel>
                        <View>
                            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
                        </View>
                    </ext:GridPanel>
                    <ext:Panel 
                        runat="server"
                        ColSpan="4"
                        Html="&nbsp;" 
                        border="false"/>
                    <ext:Panel
                        Id="PanelFinal"
                        runat="server"
                        border="false"
                        MarginSpec="10 0 0 0">
                        <Items>
                            <ext:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="guardarFactoresArticulo()" MarginSpec="0 5 0 80" />
                            <ext:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClientClick="ocultarVentanaFactores()" MarginSpec="0 5 0 5" />
                            <ext:Hidden runat="server" ID="hdnDefinicionFactores" ClientIDMode="Static" />
                            <ext:Hidden runat="server" ID="hdnGuardarFactores" ClientIDMode="Static" />
                            <ext:Hidden runat="server" ID="hdnStoreFactores" ClientIDMode="Static" />
                        </Items>
                    </ext:Panel>
                </Items>
            </ext:FormPanel>

        </Content>
    </ext:Window>

</asp:Content>
