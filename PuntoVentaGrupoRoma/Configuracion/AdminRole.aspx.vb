﻿Imports Ext.Net
Imports System.Net

Public Class AdminRole
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Ext.Net.X.IsAjaxRequest Then

            Dim principalPV As PuntoVentaPrincipal = DirectCast(Context.User, PuntoVentaPrincipal)

            Dim puedeCrear As Boolean = False
            Dim puedeConsultar As Boolean = False
            Dim puedeModificar As Boolean = False
            Dim puedeCancelar As Boolean = False

            If Not principalPV.UsuarioPuntoVenta.TienePermiso("Roles", puedeCrear, puedeModificar, puedeConsultar, puedeCancelar) Then
                principalPV.UsuarioPuntoVenta.RedireccionaAPaginaNoAcceso(Response)
                Return
            End If
            Dim controlAcceso As ControlAcceso = New ControlAcceso()

            If Not Session("IdRol") Is Nothing Then
                Dim auxIdRol As Integer = Session("IdRol")
                Session.Remove("IdRol")

                interfazModificar(auxIdRol, puedeModificar)
                Return
            End If

            If Not Session("CrearRoles") Is Nothing Then
                Session.Remove("CrearRoles")
                interfazCrear()
                Return
            End If

            If Not Request("hdnCrear") Is Nothing Then
                If Not CrearRol() Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = "Error al crear el rol"
                End If
                Return
            End If

            If Not Request("hdnActualizar") Is Nothing Then
                If Not ActualizarRol() Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = "Error al actualizar el rol"
                End If
                Return
            End If

            If Not Request("idRegistroEditar") Is Nothing Then
                Dim registroEditar As Integer = Request("idRegistroEditar")
                Session("IdRol") = registroEditar
                Return
            End If

            If Not Request("nuevoRegistro") Is Nothing Then
                Session("CrearRoles") = True
                Return
            End If


            If Not Request("idRegistroEliminar") Is Nothing Then
                Dim registroQuitar As Integer = Request("idRegistroQuitar")
                Dim mensajeError As String = String.Empty
                If Not EliminarRol(Request("idRegistroQuitar"), mensajeError) Then
                    Context.Response.StatusCode = HttpStatusCode.InternalServerError
                    Context.Response.ContentType = "text/plain"
                    Context.Response.StatusDescription = mensajeError
                End If
                Return
            End If

            interfazConsultarCrear(puedeCrear, puedeConsultar, puedeModificar, puedeCancelar)
        End If
    End Sub

    Private Function EliminarRol(ByVal idRegistro As Integer, ByRef mensajeError As String)
        Dim controlRol As ControlRol = New ControlRol()
        Return controlRol.EliminarRol(idRegistro, mensajeError)
    End Function

    Private Function CrearRol() As Boolean
        Dim controlRol As ControlRol = New ControlRol()
        Dim rol As Rol = Nothing
        Dim permisos As List(Of PermisoRolUsuario) = Nothing
        controlRol.InformacionRolFromRequest(Request, "Ins", rol, permisos)
        Return controlRol.CrearRol(rol, permisos)
    End Function

    Private Function ActualizarRol() As Boolean
        Dim controlRol As ControlRol = New ControlRol()
        Dim rol As Rol = Nothing
        Dim permisos As List(Of PermisoRolUsuario) = Nothing
        controlRol.InformacionRolFromRequest(Request, "Act", rol, permisos)
        Return controlRol.ActualizarRol(rol, permisos)
    End Function

    Public Sub ObtenerRoles(sender As Object, e As StoreReadDataEventArgs)
        Dim controlRol As ControlRol = New ControlRol()
        Dim store As Store = sender
        store.Data = controlRol.ListadoRoles()
    End Sub

    Private Sub interfazConsultarCrear(ByVal puedeCrear As Boolean, ByVal puedeConsultar As Boolean, ByVal puedeModificar As Boolean, ByVal puedeEliminar As Boolean)
        If Not puedeConsultar AndAlso Not puedeModificar Then
            GridRol.GetStore().Enabled = False
            GridRol.Visible = False
            GridRol.Hidden = True
        End If

        If Not puedeEliminar Then
            GridRol.ColumnModel.Columns(0).Visible = False
        End If

        If Not puedeCrear Then
            BotonNuevo.Visible = False
        End If

        PanelActualizar.Hidden = True
        PanelActualizar.Visible = False
        PanelCrear.Hidden = True
        PanelActualizar.Visible = False
        PanelConsultar.Center()

    End Sub

    Private Sub interfazModificar(ByVal idRol As Integer, ByVal puedeModificar As Boolean)
        GridRol.GetStore().Enabled = False
        PanelCrear.Visible = False
        PanelConsultar.Visible = False

        PanelActualizar.Hidden = False
        PanelActualizar.Visible = True
        PanelActualizar.Center()

        Dim controlRol As ControlRol = New ControlRol()
        Dim infoRol As Rol = controlRol.InformacionRol(idRol)
        If infoRol Is Nothing Then
            Response.Redirect("AdminRole.aspx")
        End If
        Dim permisos As List(Of PermisoRolUsuario) = controlRol.PermisosRol(idRol)

        txtRoleNameAct.Value = infoRol.RoleName
        txtRoleNameAct.ReadOnly = Not puedeModificar
        cmbStatusAct.Value = infoRol.Status
        cmbStatusAct.ReadOnly = Not puedeModificar
        hdnIdRol.Value = idRol
        hdnNumeroPermisosAct.Value = permisos.Count
        hdnActualizar.Value = 1
        construyeSeccionPermisos(puedeModificar, permisos, fldPermisosAct)

        If Not puedeModificar Then
            btnActualizar.Destroy()
        End If

    End Sub

    Private Sub interfazCrear()
        GridRol.GetStore().Enabled = False
        PanelActualizar.Visible = False
        PanelConsultar.Visible = False

        PanelCrear.Center()

        Dim controlRol As ControlRol = New ControlRol()
        Dim permisos As List(Of PermisoRolUsuario) = controlRol.PermisosRol(-1)

        txtRoleNameIns.Value = String.Empty
        cmbStatusIns.Value = "A"
        hdnNumeroPermisosIns.Value = permisos.Count
        hdnCrear.Value = 1
        construyeSeccionPermisos(True, permisos, fldPermisosIns)

    End Sub


    Private Sub construyeSeccionPermisos(ByVal puedeModificar As Boolean, ByRef permisos As List(Of PermisoRolUsuario), ByRef fieldContainer As Ext.Net.FieldContainer)
        For i As Integer = 0 To permisos.Count - 1
            construyeLineaPermiso(puedeModificar, i, permisos(i), fieldContainer)
        Next
    End Sub

    Private Sub construyeLineaPermiso(ByVal puedeModificar As Boolean, ByVal numeroPermiso As Integer, ByVal permiso As PermisoRolUsuario, ByRef fieldContainer As Ext.Net.FieldContainer)
        Dim containerPermiso As Ext.Net.FieldContainer = New FieldContainer()
        containerPermiso.Layout = "HBoxLayout"
        Dim labelPermiso As Ext.Net.Label = New Label()
        labelPermiso.Text = permiso.NombrePermiso
        labelPermiso.Width = 200
        Dim checkCrear As Ext.Net.Checkbox = New Checkbox()
        checkCrear.ReadOnly = Not permiso.TieneCrear OrElse Not puedeModificar
        checkCrear.Checked = permiso.Crear AndAlso permiso.TieneCrear
        checkCrear.Width = 70
        checkCrear.Name = "chkCrear" & numeroPermiso
        Dim checkActualizar As Ext.Net.Checkbox = New Checkbox()
        checkActualizar.Width = 70
        checkActualizar.ReadOnly = Not permiso.TieneActualizar OrElse Not puedeModificar
        checkActualizar.Checked = permiso.Actualizar AndAlso permiso.TieneActualizar
        checkActualizar.Name = "chkActualizar" & numeroPermiso
        Dim checkConsultar As Ext.Net.Checkbox = New Checkbox()
        checkConsultar.Width = 70
        checkConsultar.ReadOnly = Not permiso.TieneConsultar OrElse Not puedeModificar
        checkConsultar.Checked = permiso.Consultar AndAlso permiso.TieneConsultar
        checkConsultar.Name = "chkConsultar" & numeroPermiso
        Dim checkCancelar As Ext.Net.Checkbox = New Checkbox()
        checkCancelar.Width = 70
        checkCancelar.ReadOnly = Not permiso.TieneCancelar OrElse Not puedeModificar
        checkCancelar.Checked = permiso.Cancelar AndAlso permiso.TieneCancelar
        checkCancelar.Name = "chkCancelar" & numeroPermiso
        Dim hiddenIdPermiso As Ext.Net.Hidden = New Hidden()
        hiddenIdPermiso.Name = "hdnPermisoID" & numeroPermiso
        hiddenIdPermiso.Value = permiso.PermisoID

        containerPermiso.Items.Add(labelPermiso)
        containerPermiso.Items.Add(checkCrear)
        containerPermiso.Items.Add(checkActualizar)
        containerPermiso.Items.Add(checkConsultar)
        containerPermiso.Items.Add(checkCancelar)
        containerPermiso.Items.Add(hiddenIdPermiso)

        fieldContainer.Add(containerPermiso)

    End Sub



End Class