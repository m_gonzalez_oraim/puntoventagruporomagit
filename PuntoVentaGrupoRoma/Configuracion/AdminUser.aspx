﻿<%@ Page Title="Roles" Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.Master" CodeBehind="AdminUser.aspx.vb" Inherits="PuntoVentaGrupoRoma.AdminUser" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <ext:XScript ID="XScript1" runat="server">
    <script type="text/javascript">
        function editarUsuario(idRegistro) {
            Ext.Ajax.request({
                url: 'AdminUser.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminUser.aspx'; //the location you want your browser to be  redirected.
                },
                params: { idRegistroEditar: idRegistro  }  // Put your json data here or just enter the JSON object you wannat post
            })
        }

        function interfazCrearUsuario() {
            Ext.Ajax.request({
                url: 'AdminUser.aspx',    // To Which url you wanna POST.
                success: function () {
                    window.location = 'AdminUser.aspx'; //the location you want your browser to be  redirected.
                },
                params: { nuevoRegistro: true}  // Put your json data here or just enter the JSON object you wannat post
            })

        }

        function eliminarUsuario(idRegistro) {
            var quitarUsuario;
            Ext.Msg.confirm("Mensaje Sistema", "Eliminar Usuario?", function (buttonId) {
                if (buttonId == "yes") {
                    // return true; 
                    quitarUsuario = true;
                } else {
                    //return false; 
                    quitarUsuario = false;
                }
                if (quitarUsuario) {
                    Ext.Ajax.request({
                        url: 'AdminUser.aspx',    // To Which url you wanna POST.
                        success: function () {
                            window.location = 'AdminUser.aspx'; //the location you want your browser to be  redirected.
                        },
                        failure: function (status) {
                            Ext.Msg.alert(status.statusText);
                        },
                        params: { idRegistroEliminar: idRegistro }  // Put your json data here or just enter the JSON object you wannat post
                    });
                }
            });

        }

        function cancelarAccionFormulario() {
            window.location = 'AdminUser.aspx';
        }

        var Insertar = {
            insertarUsuario: function () {
                var forma = Ext.getCmp("FrmUsuario");
                var contrasenia1 = Ext.getCmp("txtNTUserDomain").getValue();
                var contrasenia2 = Ext.getCmp("txtNTUserDomain2").getValue();

                if(contrasenia1!=contrasenia2)
                {
                    Ext.Msg.alert("Contraseña/Confirmacion de contraseña no corresponden");
                    return false;
                }
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Usuario creado con exito");
                                window.location = 'AdminUser.aspx';
                            }
                            else
                            {
                                Ext.Msg.alert(b.response.statusText);
                            }
                        }
                    });
                }

            }
        }

        var Actualizar = {
            actualizarUsuario: function () {
                var forma = Ext.getCmp("FrmUsuario");
                if (forma.getForm().isValid()) {
                    forma.submit({
                        failure: function (a, b) {
                            if (b.response.status == 200)
                            {
                                Ext.Msg.alert("Usuario actualizado con exito");
                            }
                            else
                            {
                                Ext.Msg.alert(b.response.statusText);
                            }
                        }
                    });
                }
            }
        }        
</script>
 
    </ext:XScript>
        
    <ext:FormPanel ID="PanelConsultar" runat="server" Width="600" ButtonAlign="Center"  Layout="AnchorLayout"  Border="false">
        <Items>
        <ext:GridPanel 
            ID="GridUsuario" 
            runat="server"
            Icon="Table"
            Title="Usuarios"
            AutoRender ="true" 
            AutoScroll="true" 
            Width="550" 
            Region="Center" PaddingSpec="0 0 10 0">
            <Store>
                <ext:Store
                    ID="StoreGridUsuario" 
                    runat="server" 
                    OnReadData="ObtenerUsuarios"
                    Enabled="false">
                    <Proxy>
                        <ext:PageProxy>
                            <Reader>
                                <ext:JsonReader Root="data" />
                            </Reader>
                        </ext:PageProxy>
                    </Proxy>
                    <Model>
                        <ext:Model runat="server">
                            <Fields>
                                <ext:ModelField Name="AdminUserID" />
                                <ext:ModelField Name="FullName" />
                                <ext:ModelField Name="RoleName"  />
                                <ext:ModelField Name="StatusName"  />
                            </Fields>
                        </ext:Model>
                    </Model>
                </ext:Store>
            </Store>           
            <ColumnModel>
                <Columns>
                    <ext:TemplateColumn runat="server" DataIndex="AdminUserID" Width="80" TemplateString='<a href="#" onclick="eliminarUsuario({AdminUserID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.delete.png" /></a>'/>
                    <ext:TemplateColumn runat="server" DataIndex="AdminUserID" Width="80" TemplateString='<a href="#" onclick="editarUsuario({AdminUserID})"><img style="width:20px;height:20px;" src="../Content/Images/appbar.edit.png" /></a>'/>
                    <ext:Column runat="server" Header="Usuario" DataIndex="FullName" Width="200" />
                    <ext:Column runat="server" Header="Rol" DataIndex="RoleName" Width="100" />
                    <ext:Column runat="server" Header="Estado" DataIndex="StatusName" Width="40" />                    
                </Columns>
            </ColumnModel>
            <View>
                <ext:GridView ID="GridView1" runat="server" ForceFit="true">
                    </ext:GridView>
                </View>
        </ext:GridPanel>
        <ext:Button runat="server" ID="BotonNuevo" Text="Nuevo" OnClientClick="interfazCrearUsuario()" />
        </Items>
    </ext:FormPanel>
    
    
    <ext:Panel ID="PanelUsuario" runat="server" Width="500">
        <Items>
            <ext:FormPanel ID="FrmUsuario"  ClientIDMode="Static" runat="server" LabelAlign="Right" Width="500">
                <Items>
                    <ext:FieldContainer runat="server" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:TextField ID="txtFirstName"  Name="txtFirstName"  runat="server" FieldLabel="Nombre" AllowBlank="false" Width="300" />
                            <ext:TextField ID="txtLastName" Name="txtLastName"  runat="server" FieldLabel="Apellido" AllowBlank="false" Width="300" />
                            <ext:ComboBox ID="cmbAdminRoleID" Name="cmbAdminRoleID"  runat="server" FieldLabel="Rol" AllowBlank="false" Width="300" />
                            <ext:TextField ID="txtEmployeeNum" Name="txtEmployeeNum" runat="server" FieldLabel="Empleado #"  Width="300" />
                            <ext:TextField ID="txtPositionEmployee" Name="txtPuesto" runat="server" FieldLabel="Puesto" Width="300" />
                            <ext:Checkbox ID="chkEsVendedor" Name="chkEsVendedor" runat="server" FieldLabel="Es Vendedor" />
                            <ext:TextField ID="txtNTUserAccount" Name="txtNTUserAccount" runat="server" FieldLabel="Usuario" AllowBlank="false" Width="300" />
                            <ext:TextField ID="txtNTUserDomain" ClientIDMode="Static" Name="txtNTUserDomain" runat="server" FieldLabel="Contraseña" AllowBlank="false" Width="300" InputType="Password" />
                            <ext:TextField ID="txtNTUserDomain2" ClientIDMode="Static" Name="txtNTUserDomain2" runat="server" FieldLabel="Confirmacion" AllowBlank="false" Width="300" InputType="Password"  />
                            <ext:ComboBox ID="cmbStatus" Name="cmbStatus" runat="server" FieldLabel="Estado" AllowBlank="false" Width="300">
                                <Items>
                                    <ext:ListItem Value="A" Text="Activo" />
                                    <ext:ListItem Value="I" Text="Inactivo" />
                                </Items>
                            </ext:ComboBox>
                            <ext:Checkbox ID="chkFactoresAll" Name="chkFactoresAll" runat="server" FieldLabel="Venta en todos los factores" Width="300" />
                        </Items> 
                    </ext:FieldContainer>
                    
                    <ext:Hidden runat="server" ID="hdnIdUsuario" Name="hdnIdUsuario" />
                    <ext:Hidden runat="server" ID="hdnAccion" Name="hdnAccion" />
                    <ext:Hidden runat="server" ID="hdnNumeroTiendas" Name="hdnNumeroTiendas" />
                    
                    <ext:FieldContainer  runat="server" Layout="HBoxLayout" >
                        <Items>
                            <ext:Button 
                                runat="server"
                                Id="btnAccionFormulario">
                            </ext:Button>
                            <ext:Button
                                runat="server"
                                Text="Cancelar" OnClientClick="cancelarAccionFormulario()">
                            </ext:Button>
                        </Items>
                    </ext:FieldContainer>

                    <ext:FieldContainer ID="fldTiendas" runat="server" Layout="VBoxLayout" MarginSpec="10 10 10 10" >
                        <Items>
                            <ext:Label Text="Tiendas Asignadas" runat="server" MarginSpec="10 10 10 10" />

                            <ext:FieldContainer runat="server" Layout="HBoxLayout">
                                <Items>
                                    <ext:Label Text="Tiendas"  runat="server" Width="300"/>
                                    <ext:Label Text="Acceso"  runat="server" Width="90"/>
                                    <ext:Label Text="Transferencia"  runat="server" Width="90"/>
                                </Items>
                            </ext:FieldContainer>
                        </Items>
                    </ext:FieldContainer>
                </Items>
            </ext:FormPanel>

        </Items>
    </ext:Panel>


</asp:Content>
