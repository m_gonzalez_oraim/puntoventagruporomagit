﻿Public Class ReporteVentas
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID " & _
                                          "WHERE US.AdminUserID = '" & Session.Item("AdminUserID") & "'")

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"
            cmbUsuarios.SelectedItem.Value = "-1"
            cmbStatus.SelectedItem.Value = "-1"
        End If
    End Sub

    Sub Search_By_WHS()


        Dim daDepostis As DataSet = Nothing
        Dim sQuery As String = GetFilters()

        daDepostis = Master.DBConn.GetQuerydts(squery)

        Store1.DataSource = daDepostis
        Store1.DataBind()
        Store1.LoadPage(20)

    End Sub

    Sub cmbTiendas_BeforeRender(ByVal sender As Object, ByVal e As Ext.Net.DirectEventArgs)



    End Sub


    Public Function GetFilters() As String

        Dim filtroTienda As String = ""
        Dim filtroUsuario As String = ""
        Dim filtroFecha As String = ""
        Dim filtroStatus As String = ""

        If cmbUsuarios.SelectedItem.Value <> "-1" Then
            filtroUsuario = " And VE.IDUSER = " & cmbUsuarios.SelectedItem.Value
        End If

        If cmbTiendas.SelectedItem.Value <> "-1" Then
            filtroTienda = " AND VE.IDStore = " & cmbTiendas.SelectedItem.Value
        End If

        If cmbStatus.SelectedItem.Value <> "-1" Then
            If cmbStatus.SelectedItem.Value = 1 Then
                filtroStatus = " AND vd.StatusLinea='O'"
            Else
                filtroStatus = " AND vd.StatusLinea<>'O'"
            End If
        End If

        If cmbTiendas.SelectedItem.Value = "-1" Then

            filtroTienda = " AND VE.IDStore IN(SELECT  ADS.AdminStoreID " & vbCrLf & _
                            "FROM AdminStore ADS " & vbCrLf & _
                            "LEFT JOIN UserStores US " & vbCrLf & _
                            "ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                            "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ")"
        End If

        If Not DateField1.SelectedValue Is Nothing And Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = " AND (Convert(varchar,VE.Fecha,105) >= '" & Format(DateField1.SelectedValue, "dd-MM-yyyy") & "' AND Convert(varchar,VE.Fecha,105) <= '" & Format(DateField2.SelectedValue, "dd-MM-yyyy") & "') "
        ElseIf Not DateField1.SelectedValue Is Nothing Then
            filtroFecha = " AND Convert(varchar,VE.Fecha,105) >= '" & Format(DateField1.SelectedValue, "dd-MM-yyyy") & "' "
        ElseIf Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = " AND Convert(varchar,VE.Fecha,105) <= '" & Format(DateField2.SelectedValue, "dd-MM-yyyy") & "' "
        End If
        'cambio a formato yyyyMMdd
        If Not DateField1.SelectedValue Is Nothing And Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = " AND ( convert(date,VE.Fecha) between Convert(Date,'" & Format(DateField1.SelectedValue, "yyyyMMdd") & "') AND  Convert(Date,'" & Format(DateField2.SelectedValue, "yyyyMMdd") & "')) "
        ElseIf Not DateField1.SelectedValue Is Nothing Then
            filtroFecha = " AND CAST(VE.Fecha AS DATE) = '" & Format(DateField1.SelectedValue, "yyyyMMdd") & "' "
        ElseIf Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = " AND convert(date,VE.Fecha) = '" & Format(DateField2.SelectedValue, "yyyyMMdd") & "' "
        Else
            'filtroFecha = " AND convert(date,VE.Fecha) = Convert(Date,'" & Format(Now(), "yyyyMMdd") & "') "
        End If

        Dim squery As String

        squery = "select " & vbNewLine
        squery = squery & " ve.ID" & vbNewLine
        squery = squery & " ,cast(ve.Fecha  as DATE) as [Fecha]" & vbNewLine
        squery = squery & " ,(select AdminStore.StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]" & vbNewLine
        'squery = squery & " ,ve.Prefijo + '-' + Ve.Folio as [Venta]" & vbNewLine
        squery = squery & " ,'Pedido' as [Tipo Documento]" & vbNewLine
        squery = squery & " ,ve.Prefijo + '-' + Ve.Folio as [Documento]" & vbNewLine
        squery = squery & " ,case ve.Facturado when 1 then 'SI' else 'NO' end as [Facturado]" & vbNewLine
        squery = squery & " ,(select AdminUser.FirstName + ' ' + AdminUser.LastName from AdminUser where AdminUser.AdminUserID=ve.IDUser) as [Vendedor]" & vbNewLine
        squery = squery & " ,(select Nombre from Clientes where ID=ve.IDCliente) as [Cliente]" & vbNewLine
        squery = squery & " ,case vd.StatusLinea when 'O' then 'Activa' else 'Cancelada' end as [Estatus Venta]" & vbNewLine
        squery = squery & " ,(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo) as [Codigo Articulo]" & vbNewLine

        squery = squery & " ,(select U_BXP_MARCA from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) as [Linea] " & vbNewLine

        squery = squery & " ,(select itemname from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) + ' ' + isnull(vd.Juego,'') as [Articulo]" & vbNewLine
        squery = squery & " ,vd.Lista as [Lista de Precios]" & vbNewLine
        squery = squery & " ,VD.Cantidad as [Cantidad]" & vbNewLine
        squery = squery & " ,case vd.idstore when ve.idstore then 'Tienda' else 'Bodega Consignacion' end as [Origen]" & vbNewLine
        squery = squery & " ,VD.PrecioUnitario as [Precio unitario]" & vbNewLine
        squery = squery & " ,VD.Descuento as [Descuento]" & vbNewLine
        squery = squery & " ,((vd.TotalLinea - IVA)) as [subTotal]"
        squery = squery & " ,Vd.IVA as [IVA]" & vbNewLine
        squery = squery & " ,vd.TotalLinea as [Total Linea]" & vbNewLine
        squery = squery & " ,(select SUM(TotalLinea) from VentasDetalle where IDVenta=vd.IDVenta) as [Total Venta]" & vbNewLine
        squery = squery & " ,(select SUM(monto) from VentasPagos where IDVenta=ve.ID) as [Monto pagado]" & vbNewLine
        squery = squery & " ,((select SUM(TotalLinea) from VentasDetalle where IDVenta=vd.IDVenta) - (select SUM(monto) from VentasPagos where IDVenta=ve.ID)) as [Saldo]" & vbNewLine
        squery = squery & " ,(select isnull(U_PM,0) * vd.Cantidad from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) as [Push Money]" & vbNewLine

        squery = squery & " ,(select a.DefaultCustomer  from AdminStore A where A.AdminStoreID=ve.IDStore) as [Codigo Cliente SAP]"
        squery = squery & " ,(select CardName from " & Session("SAPDB") & ".OCRD where CardCode=(select a.DefaultCustomer  from AdminStore A where A.AdminStoreID=ve.IDStore)) as [Cliente SAP]"

        squery = squery & " from" & vbNewLine
        squery = squery & " ventasEncabezado VE, VentasDetalle VD" & vbNewLine
        squery = squery & " where ve.id = vd.IDVenta" & filtroFecha & filtroTienda & filtroUsuario & filtroStatus & vbNewLine

        squery = squery & " union all" & vbNewLine & vbNewLine


        squery = squery & " select " & vbNewLine
        squery = squery & " ve.ID" & vbNewLine
        squery = squery & " ,cast(ve.Fecha  as DATE) as [Fecha]" & vbNewLine
        squery = squery & " ,(select AdminStore.StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]" & vbNewLine
        squery = squery & " ,case ve.Facturado when 1 then 'NC Fiscal' else 'NC Interna' end as [Tipo]" & vbNewLine
        squery = squery & " ,ve.Prefijo + '-' + Ve.Folio as [Documento]" & vbNewLine
        squery = squery & " ,case ve.Facturado when 1 then 'SI' else 'NO' end as [Facturado]" & vbNewLine
        squery = squery & " ,(select AdminUser.FirstName + ' ' + AdminUser.LastName from AdminUser where AdminUser.AdminUserID=ve.IDUser) as [Vendedor]" & vbNewLine
        squery = squery & " ,(select Nombre from Clientes where ID=ve.IDCliente) as [Cliente]" & vbNewLine
        squery = squery & " ,case vd.StatusLinea when 'O' then 'Activa' else 'Cancelada' end as [Estatus Venta]" & vbNewLine
        squery = squery & " ,(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo) as [Codigo Articulo]" & vbNewLine

        squery = squery & " ,(select U_BXP_MARCA from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) as [Linea] " & vbNewLine

        squery = squery & " ,(select itemname from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) + ' ' + isnull((select Juego from VentasDetalle where IDVenta=ve.IDVenta and IDLinea=vd.IDLinea),'') as [Articulo]" & vbNewLine
        squery = squery & " ,vd.Lista as [Lista de Precios]" & vbNewLine
        squery = squery & " ,VD.Cantidad as [Cantidad]" & vbNewLine
        squery = squery & " ,case vd.idstore when ve.idstore then 'Tienda' else 'Bodega Consignacion' end as [Origen]" & vbNewLine
        squery = squery & " ,VD.PrecioUnitario as [Precio unitario]" & vbNewLine
        squery = squery & " ,VD.Descuento as [Descuento]" & vbNewLine
        squery = squery & " ,((vd.TotalLinea - IVA)) as [subTotal] ,Vd.IVA as [IVA]" & vbNewLine
        squery = squery & " ,vd.TotalLinea as [Total Linea]" & vbNewLine
        squery = squery & " ,(select SUM(TotalLinea) from VentasDetalle where IDVenta=vd.IDDevolucion) as [Total Venta]" & vbNewLine
        squery = squery & " ,(select SUM(monto) from VentasPagos where IDVenta=ve.ID) as [Monto pagado]" & vbNewLine
        squery = squery & " ,((select SUM(TotalLinea) from VentasDetalle where IDVenta=vd.IDDevolucion) - (select SUM(monto) from VentasPagos where IDVenta=ve.ID)) as [Saldo]" & vbNewLine
        squery = squery & " ,(select isnull(U_PM,0) * vd.Cantidad from " & Session("SAPDB") & ".oitm where itemcode=(select Articulos.ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) as [Push Money]" & vbNewLine

        squery = squery & " ,(select a.DefaultCustomer  from AdminStore A where A.AdminStoreID=ve.IDStore) as [Codigo Cliente SAP]" & vbNewLine
        squery = squery & " ,(select CardName from " & Session("SAPDB") & ".OCRD where CardCode=(select a.DefaultCustomer  from AdminStore A where A.AdminStoreID=ve.IDStore)) as [Cliente SAP]" & vbNewLine

        squery = squery & " from" & vbNewLine
        squery = squery & " DevolucionesEncabezado VE, DevolucionesDetalle VD" & vbNewLine
        squery = squery & " where ve.id = vd.IDDevolucion " & filtroFecha & filtroTienda & filtroUsuario & vbNewLine
        squery = squery & " order by tienda,Documento" & vbNewLine




        Return squery

    End Function

    Sub toXLS()

        Dim ds As DataSet = Nothing
        Dim sQuery As String

        sQuery = GetFilters()

        ds = Master.DBConn.GetQuerydts(squery)


        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try

        'Prompt the user to open or save the file.
        '        If blnOpen Then
        '            Response.Write("<script>")
        '            Response.Write("window.open('~/ReporteInventarios/" & strUniqueFn & "','_blank')")
        '            Response.Write("</script>")
        '        End If
        '5:      If strUniqueFn <> "" Then 'get absolute path of the file
        '6:          Dim path As String = Server.MapPath(strUniqueFn) 'get file object as FileInfo
        '7:          Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
        '8:          If file.Exists Then 'set appropriate headers
        '9:              Response.Clear()
        '10:             Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '11:             Response.AddHeader("Content-Length", file.Length.ToString())
        '12:             Response.ContentType = "application/octet-stream"
        '13:             Response.WriteFile(file.FullName)
        '14:             Response.End() 'if file does not exist
        '15:         Else
        '16:             Response.Write("This file does not exist.")
        '17:         End If 'nothing in the URL as HTTP GET
        '18:     Else
        '19:         Response.Write("Please provide a file to download.")
        '20:     End If
        Response.Redirect(strUniqueFn)
    End Sub




    Sub Store_Selected()
        Dim daUsers As DataSet = Nothing
        daUsers = Master.DBConn.GetQuerydts("SELECT    AU.FirstName + ' ' +  AU.LastName as Nombre, AU.AdminUserID " & vbCrLf & _
                                            "FROM AdminUser AU " & vbCrLf & _
                                            "JOIN UserStores US ON AU.AdminUserID = US.AdminUserID " & vbCrLf & _
                                            "WHERE US.AdminStoreID = " & cmbTiendas.SelectedItem.Value)

        storeUsuarios.DataSource = daUsers
        storeUsuarios.DataBind()

        cmbUsuarios.SelectedItem.Value = "-1"
    End Sub


    Function getFormasPago(ByVal IDStore As Integer) As String

        Dim drPagos As SqlClient.SqlDataReader = Nothing
        Dim strPagos As String = "N/A."

        Master.DBConn.GetQuerydtr("SELECT formapago,monto FROM  ventasPagos WHERE idVenta  = " & IDStore, drPagos)

        If drPagos.HasRows Then
            strPagos = ""
            While drPagos.Read
                strPagos = strPagos & drPagos("formapago") & " = $ " & drPagos("monto") & ", "

            End While

        End If
        drPagos.Close()

        Return strPagos.Substring(0, Trim(strPagos).Length - 1)
    End Function

End Class