﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReporteVentas.aspx.vb" Inherits="PuntoVentaGrupoRoma.ReporteVentas" MasterPageFile="~/admin.master"%>

<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

  <script type="text/javascript">
      Ext.data.Connection.override({
          timeout: 1200000
      });
      Ext.Ajax.timeout = 1200000;
      Ext.net.DirectEvent.timeout = 1200000;
    </script>
         
         
    <ext:GridPanel ID="gridPanel1" IDMode="Explicit" runat="server" AutoHeight="true" Title="Ventas"
        Layout="fit" >
 
        <topbar>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:ComboBox   ID="cmbTiendas" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    DisplayField="StoreName"
                                    ValueField="AdminStoreID"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    FieldLabel="Tienda" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por tienda..."  >
                                    <DirectEvents>
                                        <Select OnEvent="Store_Selected"></Select>
                                    </DirectEvents>
                                    <Store>
                                        <ext:Store ID="storeStores" IDMode="Explicit" runat="server">
                                            <model >
                                                <ext:Model runat="server" IDProperty="AdminStoreID">
                                                    <Fields>
                                                        <ext:ModelField Name="AdminStoreID">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="StoreName">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todos" Value="-1" />
                                    </Items>
                     </ext:ComboBox>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                     <ext:ComboBox   ID="cmbUsuarios" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    FieldLabel="Vendedor" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    DisplayField="Nombre"
                                    ValueField="AdminUserID"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por usuario..">
                                    <Store>
                                        <ext:Store ID="storeUsuarios" IDMode="Explicit" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="AdminUserID">
                                                    <Fields>
                                                        <ext:ModelField Name="AdminUserID">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="Nombre">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todos" Value="-1" />
                                    </Items>
                     </ext:ComboBox>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                     <ext:ComboBox   ID="cmbStatus" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    FieldLabel="Estatus Venta" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    DisplayField="Nombre"
                                    ValueField="Code"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por estatus..">
                                    <Store>
                                        <ext:Store ID="storeStatus" IDMode="Explicit" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="Code">
                                                    <Fields>
                                                        <ext:ModelField Name="Code">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="Name">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todos" Value="-1" />
                                        <ext:ListItem Text="Activa" Value="1" />
                                        <ext:ListItem Text="Cancelada" Value="0" />
                                    </Items>
                     </ext:ComboBox>
                    <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField1" 
                        runat="server"                    
                        FieldLabel="Fecha Inicio" 
                        LabelAlign="Top"
                        LabelWidth="60"  
                        Vtype="daterange"                 
                        Width="150"  > 
                        <CustomConfig>
                        <ext:ConfigItem Name="endDateField" Value="#{DateField2}" Mode="Value" />
                    </CustomConfig>                          
                    </ext:DateField>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField2" 
                        runat="server"  
                        Vtype="daterange"                  
                        FieldLabel="Fecha Fin" 
                        LabelAlign="Top"
                        LabelWidth="60"                   
                        Width="150"  >   
                        <CustomConfig>
                        <ext:ConfigItem Name="startDateField" Value="#{DateField1}" Mode="Value" />
                    </CustomConfig>                        
                    </ext:DateField> 
                    <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:Button runat="server" ID="Button2" Text="Buscar..." Icon="BookMagnify" IconCls="BookMagnify16"
                        Scale="Small" IconAlign="Top" >
                        <DirectEvents>
                            <Click OnEvent="Search_By_WHS">
                                <EventMask ShowMask="true" Msg="Ejecutando Consulta..." MinDelay="3000" />
                            </Click>
                         </DirectEvents> 
                    </ext:Button> 
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>                   
                    <ext:Button ID="btnExcel" runat="server" Text="Excel"   Icon="PageExcel" IconAlign="Top"    >
                           <DirectEvents>
                            <Click OnEvent="toXLS">
                              <%--  <EventMask ShowMask="true" Msg="Exportando..." MinDelay="3000" />--%>
                            </Click>
                         </DirectEvents> 
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </topbar>
        <columnmodel>
                                <Columns>
                                    <ext:Column runat="server" DataIndex="ID" Header="ID" Align="Center" Hidden="true">
                                    </ext:Column>
                                    <ext:DateColumn runat="server" DataIndex="Fecha" Header="Fecha" Format="dd/MM/yyyy" >
                                    </ext:DateColumn>
                                    <ext:Column runat="server" DataIndex="Tienda" Header="Tienda" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Tipo Documento" Header="Tipo Documento" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Documento" Header="Documento" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Facturado" Header="Facturado" Align="Center">
                                    </ext:Column>                                    
                                    <ext:Column  runat="server" DataIndex="Vendedor" Header="Vendedor">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Cliente" Header="Cliente" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Estatus Venta" Header="Estatus Venta" Align="Center">
                                    </ext:Column>
                                     <ext:Column runat="server" DataIndex="Linea" Header="Linea" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Articulo" Header="Articulo" Align="Center">
                                    </ext:Column>                      
                                    <ext:Column runat="server" DataIndex="Codigo Articulo" Header="Codigo Articulo" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Lista de Precios" Header="Lista de Precios" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Cantidad" Header="Cantidad" Align="Center">
                                    </ext:Column>                                    
                                     <ext:Column runat="server" DataIndex="Origen" Header="Origen" Align="Center">
                                    </ext:Column>    
                                    <ext:Column runat="server" DataIndex="Precio unitario" Header="Precio unitario" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Descuento" Header="Descuento" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="subTotal" Header="Sub Total" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="IVA" Header="IVA" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Total Linea" Header="Total Linea" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>                                                                        
                                    <ext:Column runat="server" DataIndex="Total Venta" Header="Total Venta" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Monto pagado" Header="Monto pagado" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column> 
                                    <ext:Column runat="server" DataIndex="Saldo" Header="Saldo" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>                                     
                                    <ext:Column runat="server" DataIndex="Push Money" Header="Push Money" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>       
                                    <ext:Column runat="server" DataIndex="Codigo Cliente SAP" Header="Codigo Cliente SAP" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Cliente SAP" Header="Cliente SAP" Align="Center">
                                    </ext:Column>                                                                  
                                </Columns>
                            </columnmodel>
                            <View>
                            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
                        </View>
        <store>
                                <ext:Store ID="Store1" IDMode="Explicit" runat="server" AutoLoad="true">
                                    <Model>
                                        <ext:Model>
                                            <Fields>
                                                <ext:ModelField Name="ID">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Fecha" Type="Date" >
                                                </ext:ModelField>
                                                <ext:ModelField Name="Tienda">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Tipo Documento">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Documento">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Facturado">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Vendedor">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Cliente">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Estatus Venta">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Codigo Articulo">
                                                </ext:ModelField>
                                                
                                                 <ext:ModelField Name="Linea">
                                                </ext:ModelField>
                                                
                                                <ext:ModelField Name="Articulo">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Lista de Precios">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Cantidad">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Origen">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Precio unitario">
                                                </ext:ModelField> 
                                                <ext:ModelField Name="Descuento">
                                                </ext:ModelField>
                                                <ext:ModelField Name="subTotal">
                                                </ext:ModelField>
                                                <ext:ModelField Name="IVA">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Total Linea">
                                                </ext:ModelField>                                                 
                                                <ext:ModelField Name="Total Venta">
                                                </ext:ModelField> 
                                                <ext:ModelField Name="Monto pagado">
                                                </ext:ModelField> 
                                                <ext:ModelField Name="Saldo">
                                                </ext:ModelField> 
                                                <ext:ModelField Name="Push Money">
                                                </ext:ModelField>                         
                                                <ext:ModelField Name="Codigo Cliente SAP">
                                                </ext:ModelField>                         
                                                <ext:ModelField Name="Cliente SAP">
                                                </ext:ModelField>                         
                                                
                                                                      
                                            </Fields>
                                        </ext:Model>
                                    </Model> 
                                </ext:Store>                               
                            </store>
                            <Features>
                                <ext:GridFilters runat="server" ID="GridFilters1">
                                    <Filters>
                                        
                                        <ext:DateFilter DataIndex="Fecha" AfterText="Despues de:" BeforeText="Antes de:" OnText="El día:">
                                            <DatePickerOptions runat="server" TodayText="Hoy" >
                                            </DatePickerOptions>
                                        </ext:DateFilter>
                                        <ext:ListFilter DataIndex="DescCategoria" Options="Equipos,Tarjetas SIM,Accesorios,TEMM,No Inventariable" />
                                        <ext:ListFilter DataIndex="StoreTypeName" Options="Principal,Tienda,Cambaceo,Socios" />
                                        <ext:StringFilter DataIndex="TipoVenta" />

                                        
                                    </Filters>
                                </ext:GridFilters>
                            </Features>
                            
                            
        <selectionmodel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
                                </ext:RowSelectionModel>
                            </selectionmodel>
                            <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" ></ext:PagingToolbar>
                            </BottomBar>
    </ext:GridPanel>
    <br />
    <br />
</asp:Content>