﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReporteKardex.aspx.vb" Inherits="PuntoVentaGrupoRoma.ReporteKardex" MasterPageFile="~/admin.master"  %>

<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

  <script type="text/javascript">
      Ext.data.Connection.override({
          timeout: 1200000
      });
      Ext.Ajax.timeout = 1200000;
      Ext.net.DirectEvent.timeout = 1200000;
    </script>
         
         
    <ext:GridPanel ID="gridPanel1" IDMode="Explicit" runat="server" AutoHeight="true" Title="Ventas"
        Layout="fit" >
        
        <topbar>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:ComboBox   
                        ID="cmbTiendas" 
                        IDMode="Explicit" 
                        runat="server" 
                        AllowBlank="true" 
                        Width="300" 
                        DisplayField="StoreName"
                        ValueField="AdminStoreID"
                        TypeAhead="true" 
                        Mode="Default" 
                        FieldLabel="Tienda" 
                        LabelAlign="Top"
                        LabelWidth="60" 
                        TriggerAction="All"
                        EmptyText="Filtrar por tienda..."  >
                            <Store>
                                <ext:Store ID="storeStores" IDMode="Explicit" runat="server">
                                    <Model>
                                        <ext:Model runat="server" IDProperty="AdminStoreID">
                                            <Fields>
                                                <ext:ModelField Name="AdminStoreID">
                                                </ext:ModelField>
                                                <ext:ModelField Name="StoreName">
                                                </ext:ModelField>
                                            </Fields>
                                        </ext:Model>
                                    </Model>
                                </ext:Store>
                            </Store>
                            <Items>
                                <ext:ListItem Text="Todos" Value="-1" />
                            </Items>
                    </ext:ComboBox>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator> 
                    <ext:Button runat="server" ID="Button2" Text="Buscar..." Icon="BookMagnify" IconCls="BookMagnify16"
                        Scale="Small" IconAlign="Top" >
                        <DirectEvents>
                            <Click OnEvent="Search_By_WHS">
                                <EventMask ShowMask="true" Msg="Ejecutando Consulta..." MinDelay="3000" />
                            </Click>
                         </DirectEvents> 
                    </ext:Button> 
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>                   
                    <ext:Button ID="btnExcel" runat="server" Text="Excel"   Icon="PageExcel" IconAlign="Top"    >
                           <DirectEvents>
                            <Click OnEvent="toXLS">
                              <%--  <EventMask ShowMask="true" Msg="Exportando..." MinDelay="3000" />--%>
                            </Click>
                         </DirectEvents> 
                    </ext:Button>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField1" 
                        runat="server"                    
                        FieldLabel="Fecha Inicio" 
                        LabelAlign="Top"
                        LabelWidth="60"  
                        Vtype="daterange"                 
                        Width="150"  > 
                        <CustomConfig>
                        <ext:ConfigItem Name="endDateField" Value="#{DateField2}" Mode="Value" />
                    </CustomConfig>                          
                    </ext:DateField>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField2" 
                        runat="server"  
                        Vtype="daterange"                  
                        FieldLabel="Fecha Fin" 
                        LabelAlign="Top"
                        LabelWidth="60"                   
                        Width="150"  >   
                        <CustomConfig>
                        <ext:ConfigItem Name="startDateField" Value="#{DateField1}" Mode="Value" />
                    </CustomConfig>                        
                    </ext:DateField> 
                </Items>
            </ext:Toolbar>
        </topbar>
        <columnmodel>
            <Columns>
                <ext:Column runat="server" DataIndex="TIPO MOV" Header="TIPO MOV" Align="Left">
                </ext:Column>
                <ext:DateColumn runat="server" DataIndex="FECHA MOVIMIENTO" Header="FECHA MOVIMIENTO" Format="dd/MM/yyyy"  Align="Left">
                </ext:DateColumn>
                <ext:Column runat="server" DataIndex="ID MOVIMIENTO" Header="ID MOVIMIENTO" Align="Left">
                </ext:Column>
                <ext:Column runat="server" DataIndex="TIENDA" Header="TIENDA" Align="Left">
                </ext:Column>
                <ext:Column  runat="server" DataIndex="CANTIDAD"  Header="CANTIDAD" Align="Right">
                </ext:Column>
                <ext:Column runat="server" DataIndex="DESC ARTÍCULO" Header="DESC ARTÍCULO " Align="Left" >
                </ext:Column>
                <ext:Column runat="server" DataIndex="ID_ARTICULO" Header="ID_ARTICULO" Align="Left">
                </ext:Column>
                <ext:Column runat="server" DataIndex="ID_STORE" Header="ID_STORE" Align="Left">
                </ext:Column>
                <ext:Column runat="server" DataIndex="Comentarios" Header="COMENTARIOS" Align="Left">
                </ext:Column>
                <ext:Column runat="server" DataIndex="CANTIDAD ACUMULADA" Header="CANTIDAD ACUMULADA" Align="Right">
                </ext:Column> 
                <ext:Column runat="server" DataIndex="INVENTARIO ACTUAL" Header="INVENTARIO ACTUAL" Align="Right">
                </ext:Column>
                <ext:Column runat="server" DataIndex="REFERENCIA" Header="REFERENCIA" Align="Right">
                </ext:Column>
            </Columns>
        </columnmodel>
        <View>
            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
        </View>
        <store>
            <ext:Store ID="Store1" IDMode="Explicit" runat="server" AutoLoad="true">
                <Model>
                    <ext:Model runat="server">
                        <Fields>
                            <ext:ModelField Name="TIPO MOV">
                            </ext:ModelField>
                            <ext:ModelField Name="FECHA MOVIMIENTO" Type="Date" >
                            </ext:ModelField>
                            <ext:ModelField Name="ID MOVIMIENTO">
                            </ext:ModelField>
                            <ext:ModelField Name="TIENDA">
                            </ext:ModelField>
                            <ext:ModelField Name="CANTIDAD">
                            </ext:ModelField>
                            <ext:ModelField Name="DESC ARTÍCULO">
                            </ext:ModelField>
                            <ext:ModelField Name="ID_ARTICULO">
                            </ext:ModelField>
                            <ext:ModelField Name="ID_STORE">
                            </ext:ModelField>
                            <ext:ModelField Name="Comentarios">
                            </ext:ModelField> 
                            <ext:ModelField Name="CANTIDAD ACUMULADA">
                            </ext:ModelField> 
                            <ext:ModelField Name="INVENTARIO ACTUAL">
                            </ext:ModelField> 
                            <ext:ModelField Name="REFERENCIA">
                            </ext:ModelField> 
                        </Fields>
                    </ext:Model>
                </Model> 
            </ext:Store>                               
        </store>
         
        <Features>
            <ext:GridFilters runat="server" ID="GridFilters1">
                <Filters> 
                    <ext:DateFilter DataIndex="FECHA MOVIMIENTO" AfterText="Despues de:" BeforeText="Antes de:" OnText="El día:">
                        <DatePickerOptions runat="server" TodayText="Hoy" >
                        </DatePickerOptions>
                    </ext:DateFilter>
                    <ext:ListFilter DataIndex="DescCategoria" Options="Equipos,Tarjetas SIM,Accesorios,TEMM,No Inventariable" />
                    <ext:ListFilter DataIndex="StoreTypeName" Options="Principal,Tienda,Cambaceo,Socios" />
                    <ext:StringFilter DataIndex="TIPO MOV" /> 
                    <ext:StringFilter DataIndex="TIENDA" /> 
                    <ext:StringFilter DataIndex="DESC ARTÍCULO" /> 
                    <ext:StringFilter DataIndex="ID_ARTICULO" /> 
                    <ext:StringFilter DataIndex="ID_STORE" /> 
                    <ext:StringFilter DataIndex="Comentarios" /> 
                    <ext:StringFilter DataIndex="REFERENCIA" /> 
                </Filters>
            </ext:GridFilters>
        </Features>                            
                            
        <selectionmodel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
            </ext:RowSelectionModel>
        </selectionmodel>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server" ></ext:PagingToolbar>
        </BottomBar>
    </ext:GridPanel>
    <br />
    <br />
</asp:Content>