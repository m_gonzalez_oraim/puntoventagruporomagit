﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ReporteIngresos.aspx.vb" Inherits="PuntoVentaGrupoRoma.ReporteIngresos" MasterPageFile="~/admin.master" %>

<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <ext:ResourceManager ID="ResourceManager1" runat="server" />

  <script type="text/javascript">
      Ext.data.Connection.override({
          timeout: 1200000
      });
      Ext.Ajax.timeout = 1200000;
      Ext.net.DirectEvent.timeout = 1200000;
    </script>
         
         
    <ext:GridPanel ID="gridPanel1" IDMode="Explicit" runat="server" AutoHeight="true" Title="Ingresos"
        Layout="fit" >
        <topbar>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:ComboBox   ID="cmbTiendas" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    DisplayField="StoreName"
                                    ValueField="AdminStoreID"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    FieldLabel="Tienda" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por tienda..."  >
                                    <DirectEvents>
                                        <Select OnEvent="Store_Selected"></Select>
                                    </DirectEvents>
                                    <Store>
                                        <ext:Store ID="storeStores" IDMode="Explicit" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="AdminStoreID">
                                                    <Fields>
                                                        <ext:ModelField Name="AdminStoreID">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="StoreName">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todos" Value="-1" />
                                    </Items>
                     </ext:ComboBox>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                     <ext:ComboBox   ID="cmbUsuarios" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    FieldLabel="Vendedor" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    DisplayField="Nombre"
                                    ValueField="AdminUserID"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por usuario..">
                                    <Store>
                                        <ext:Store ID="storeUsuarios" IDMode="Explicit" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="AdminUserID">
                                                    <Fields>
                                                        <ext:ModelField Name="AdminUserID">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="Nombre">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todos" Value="-1" />
                                    </Items>
                     </ext:ComboBox>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                     <ext:ComboBox   ID="cmbStatus" 
                                    IDMode="Explicit" 
                                    runat="server" 
                                    AllowBlank="true" 
                                    Width="150" 
                                    FieldLabel="Forma de Pago" 
                                    LabelAlign="Top"
                                    LabelWidth="60" 
                                    DisplayField="Nombre"
                                    ValueField="Code"
                                    TypeAhead="true" 
                                    Mode="Default" 
                                    TriggerAction="All"
                                    EmptyText="Filtrar por estatus..">
                                    <Store>
                                        <ext:Store ID="storeStatus" IDMode="Explicit" runat="server">
                                            <Model>
                                                <ext:Model runat="server" IDProperty="Code">
                                                    <Fields>
                                                        <ext:ModelField Name="Code">
                                                        </ext:ModelField>
                                                        <ext:ModelField Name="Name">
                                                        </ext:ModelField>
                                                    </Fields>
                                                </ext:Model>
                                            </Model>
                                        </ext:Store>
                                    </Store>
                                    <Items>
                                        <ext:ListItem Text="Todas" Value="-1" />
                                        <ext:ListItem Text="Efectivo" Value="Efectivo" />
                                        <ext:ListItem Text="Tarjeta de Crédito" Value="Tarjeta de Crédito" />
                                        <ext:ListItem Text="Tarjeta de Débito" Value="Tarjeta de Débito" />
                                        <ext:ListItem Text="American Express" Value="American Express" />
                                        <ext:ListItem Text="Deposito / Cheque" Value="Deposito / Cheque" />
                                        <ext:ListItem Text="Nota de crédito" Value="Nota de crédito" />
                                        <ext:ListItem Text="Otros" Value="Otros" />
                                    </Items>
                     </ext:ComboBox>
                    <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField1" 
                        runat="server"                    
                        FieldLabel="Fecha Inicio" 
                        LabelAlign="Top"
                        LabelWidth="60"  
                        Vtype="daterange"                 
                        Width="150"  > 
                        <CustomConfig>
                        <ext:ConfigItem Name="endDateField" Value="#{DateField2}" Mode="Value" />
                    </CustomConfig>                          
                    </ext:DateField>
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:DateField 
                        ID="DateField2" 
                        runat="server"  
                        Vtype="daterange"                  
                        FieldLabel="Fecha Fin" 
                        LabelAlign="Top"
                        LabelWidth="60"                   
                        Width="150"  >   
                        <CustomConfig>
                        <ext:ConfigItem Name="startDateField" Value="#{DateField1}" Mode="Value" />
                    </CustomConfig>                        
                    </ext:DateField> 
                    <ext:ToolbarSeparator></ext:ToolbarSeparator>
                    <ext:Button runat="server" ID="Button2" Text="Buscar..." Icon="BookMagnify" IconCls="BookMagnify16"
                        Scale="Small" IconAlign="Top" >
                        <DirectEvents>
                            <Click OnEvent="Search_By_WHS">
                                <EventMask ShowMask="true" Msg="Ejecutando Consulta..." MinDelay="3000" />
                            </Click>
                         </DirectEvents> 
                    </ext:Button> 
                     <ext:ToolbarSeparator></ext:ToolbarSeparator>                   
                    <ext:Button ID="btnExcel" runat="server" Text="Excel"   Icon="PageExcel" IconAlign="Top"    >
                           <DirectEvents>
                            <Click OnEvent="toXLS">
                              <%--  <EventMask ShowMask="true" Msg="Exportando..." MinDelay="3000" />--%>
                            </Click>
                         </DirectEvents> 
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </topbar>
        <columnmodel>
                                <Columns>
                                    <ext:DateColumn runat="server" DataIndex="Fecha" Header="Fecha" Format="dd/MM/yyyy" >
                                    </ext:DateColumn>
                                    <ext:Column runat="server" DataIndex="Tienda" Header="Tienda" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Venta" Header="Venta" Align="Center">
                                    </ext:Column>
                                    <ext:Column  runat="server" DataIndex="Vendedor" Header="Vendedor">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Cliente" Header="Cliente" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Monto" Header="Monto" Align="Center">
                                        <Renderer Format="UsMoney" /> 
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Forma de Pago" Header="Forma de Pago" Align="Center">
                                    </ext:Column>
                                    <ext:Column runat="server" DataIndex="Recibo" Header="Recibo" Align="Center">
                                    </ext:Column>                                    
                                </Columns>
                            </columnmodel>
                            <View>
                            <ext:GridView ID="GridView2" runat="server" ForceFit="true" />
                        </View>
                            <store>
                                <ext:Store ID="Store1" IDMode="Explicit" runat="server" AutoLoad="true">
                                    <Model>
                                        <ext:Model runat="server">
                                            <Fields>
                                                <ext:ModelField Name="ID">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Fecha" Type="Date" >
                                                </ext:ModelField>
                                                <ext:ModelField Name="Tienda">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Venta">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Vendedor">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Cliente">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Monto">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Forma de Pago">
                                                </ext:ModelField>
                                                <ext:ModelField Name="Recibo">
                                                </ext:ModelField>                                                
                                            </Fields>
                                        </ext:Model>
                                    </Model> 
                                </ext:Store>                               
                            </store>
                            <Features>
                                <ext:GridFilters runat="server" ID="GridFilters1">
                                    <Filters>
                                        
                                        <ext:DateFilter DataIndex="Fecha" AfterText="Despues de:" BeforeText="Antes de:" OnText="El día:">
                                            <DatePickerOptions runat="server" TodayText="Hoy" >
                                            </DatePickerOptions>
                                        </ext:DateFilter>
                                        <ext:ListFilter DataIndex="DescCategoria" Options="Equipos,Tarjetas SIM,Accesorios,TEMM,No Inventariable" />
                                        <ext:ListFilter DataIndex="StoreTypeName" Options="Principal,Tienda,Cambaceo,Socios" />
                                        <ext:StringFilter DataIndex="TipoVenta" />
                                    </Filters>
                                </ext:GridFilters>
                            </Features>
                            <selectionmodel>
                                <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
                                </ext:RowSelectionModel>
                            </selectionmodel>
                            <BottomBar>
                            <ext:PagingToolbar ID="PagingToolbar1" runat="server" ></ext:PagingToolbar>
                            </BottomBar>
    </ext:GridPanel>
    <br />
    <br />
</asp:Content>