﻿Public Class ReporteKardex
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID " & _
                                          "WHERE US.AdminUserID = '" & Session.Item("AdminUserID") & "'")

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"

        End If
    End Sub

    Sub Search_By_WHS()


        Dim daDepostis As DataSet = Nothing
        Dim filtroTienda As String = ""
        Dim filtroUsuario As String = ""
        Dim filtroFecha As String = ""
        Dim filtroConsigna As String = ""

        If cmbTiendas.SelectedItem.Value <> "-1" Then
            filtroTienda = " AND ADS.AdminStoreID = " & cmbTiendas.SelectedItem.Value
            filtroConsigna = " AND ADS.AdminStoreID in (Select ads1.AdminStoreID from AdminStore ADS1 where ads1.whsConsigID = " & cmbTiendas.SelectedItem.Value & ")"
        End If

        If cmbTiendas.SelectedItem.Value = "-1" Then
            filtroTienda = " AND ADS.AdminStoreID IN(SELECT  ADS2.AdminStoreID " & vbCrLf & _
                            "FROM AdminStore ADS2 " & vbCrLf & _
                            "LEFT JOIN UserStores US " & vbCrLf & _
                            "ON US.AdminStoreID = ADS2.AdminStoreID " & vbCrLf & _
                            "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ")"
            filtroConsigna = " "
        End If

        'cambio a formato yyyyMMdd
        If Not DateField1.SelectedValue Is Nothing And Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = "  ( convert(date,[FECHA MOVIMIENTO]) between Convert(Date,'" & Format(DateField1.SelectedValue, "yyyyMMdd") & "') AND  Convert(Date,'" & Format(DateField2.SelectedValue, "yyyyMMdd") & "')) "
        ElseIf Not DateField1.SelectedValue Is Nothing Then
            filtroFecha = "  [FECHA MOVIMIENTO] = '" & Format(DateField1.SelectedValue, "yyyyMMdd") & "' "
        ElseIf Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = "  convert(date,[FECHA MOVIMIENTO]) = '" & Format(DateField2.SelectedValue, "yyyyMMdd") & "' "
        Else
            filtroFecha = "  convert(date,[FECHA MOVIMIENTO]) = Convert(Date,'" & Format(Now(), "yyyyMMdd") & "') "
        End If

        Dim squery As String
        squery = " " & _
        "drop table #tempInv " & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA DIRECTA' AS 'TIPO MOV'" & vbCrLf & _
        ",EDE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EDE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EDD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EDD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",EDD.Comentarios" & vbCrLf & _
        "into #tempInv" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaDirectaDetalle EDD INNER JOIN" & vbCrLf & _
        "                      EntradaDirectaEncabezado EDE ON EDD.IDEntrada = EDE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EDD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EDD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT   " & vbCrLf & _
        "'VENTA' AS 'TIPO MOV'  " & vbCrLf & _
        ",VEE.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",VEE.ID AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",VED.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",VED.IDArticulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      VENTASDETALLE VED INNER JOIN" & vbCrLf & _
        "                      VENTASENCABEZADO VEE ON VED.IDVENTA = VEE.ID ON " & vbCrLf & _
        "                      ADS.AdminStoreID = VED.IDSTORE INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON VED.IDArticulo = ART.IDArticulo" & vbCrLf & _
        "WHERE VEE.Facturado=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'DEVOLUCION' AS 'TIPO MOV'" & vbCrLf & _
        ",DED.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",DEE.ID AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",DED.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",DED.IDArticulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      DEVOLUCIONESDETALLE DED INNER JOIN" & vbCrLf & _
        "                      DEVOLUCIONESENCABEZADO DEE ON DEE.ID = DED.IDDevolucion ON " & vbCrLf & _
        "                      ADS.AdminStoreID = DED.IDSTORE INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON DED.IDArticulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      VentasEncabezado VEE ON DEE.IDVenta = VEE.ID" & vbCrLf & _
        "WHERE VEE.Facturado=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'ENVIO PENDIENTE' AS 'TIPO MOV'" & vbCrLf & _
        ",EVE.FechaEnvio AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EVE.IDENVIO AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EVD.CantidadPendiente * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EVD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EnviosDetalle EVD INNER JOIN" & vbCrLf & _
        "                      EnviosEncabezado EVE ON EVD.IDEnvio = EVE.IDEnvio ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EVD.TiendaOrigen INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EVD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE  EVD.CantidadPendiente>0  " & filtroTienda & vbCrLf & _
        " UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'ENVIO PROCESADO' AS 'TIPO MOV'" & vbCrLf & _
        ",REC.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",REC.IDRecepcion AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",REC.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",REC.Articulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         Recepciones REC INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON REC.Articulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      AdminStore ADS ON REC.AlmacenOrigen = ADS.AdminStoreID" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT   " & vbCrLf & _
        "'RECEPCION' AS 'TIPO MOV'  " & vbCrLf & _
        ",REC.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",REC.IDRecepcion AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",REC.Cantidad  AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",REC.Articulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         Recepciones REC INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON REC.Articulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      AdminStore ADS ON REC.AlmacenDestino = ADS.AdminStoreID" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA OC' AS 'TIPO MOV'" & vbCrLf & _
        ",EOE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EOE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EOD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EOD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaOCDet EOD INNER JOIN" & vbCrLf & _
        "                      EntradasOC EOE ON EOD.IDEntrada = EOE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EOD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EOD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA CONS X OC' AS 'TIPO MOV'" & vbCrLf & _
        ",EOE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EOE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",(Select ads1.StoreName from AdminStore ADS1 where ads1.AdminStoreID  = ADS.whsConsigID) AS 'TIENDA'" & vbCrLf & _
        ",(EOD.Cantidad *-1) AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EOD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.whsConsigID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaOCDet EOD INNER JOIN" & vbCrLf & _
        "                      EntradasOC EOE ON EOD.IDEntrada = EOE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EOD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EOD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroConsigna & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA TRAS CONSIGNA' AS 'TIPO MOV'" & vbCrLf & _
        ",ETE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",ETE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",ETD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",ETD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaTransDet ETD INNER JOIN" & vbCrLf & _
        "                      EntradaTrans ETE ON ETD.IDEntrada = ETE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = ETD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON ETD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA DIRECTA' AS 'TIPO MOV'" & vbCrLf & _
        ",SAE.FechaSalWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",SAE.IDSALIDA AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",SAD.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",SAD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",SAD.Comentarios " & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      SalidaDetalle SAD INNER JOIN" & vbCrLf & _
        "                      SalidaEncabezado SAE ON SAD.IDSalida = SAE.IDSalida ON " & vbCrLf & _
        "                      ADS.AdminStoreID = SAD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON SAD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA TRAS CONSIGNA' AS 'TIPO MOV'" & vbCrLf & _
        ",STE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",STE.IDSALIDA AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",STD.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",STD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      SalidaTransDet STD INNER JOIN" & vbCrLf & _
        "                      SalidaTrans STE ON STD.IDSalida = STE.IDSalida ON " & vbCrLf & _
        "                      ADS.AdminStoreID = STD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON STD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "" & vbCrLf & _
        ""

        Dim squery2 As String

        squery2 = "SELECT " & vbCrLf & _
        "T0.*, " & vbCrLf & _
        "(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) AS 'CANTIDAD ACUMULADA'" & vbCrLf & _
        "FROM #tempInv T0" & vbCrLf & _
        "WHERE " & filtroFecha & vbCrLf & _
        "ORDER BY [FECHA MOVIMIENTO]" & vbCrLf & _
        "" & vbCrLf & _
        "" & vbCrLf & _
        ""

        squery2 = "SELECT " & vbCrLf & _
     "T0.*, " & vbCrLf & _
     "(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) AS 'CANTIDAD ACUMULADA'" & vbCrLf & _
     ",CASE WHEN " & vbCrLf & _
     "	(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) =" & vbCrLf & _
     "	(SELECT INV.Cantidad FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO]) THEN " & vbCrLf & _
     "		(SELECT convert(varchar(20),convert(int,INV.Cantidad)) FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO])" & vbCrLf & _
     "	WHEN (SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) <0 THEN" & vbCrLf & _
     "		(SELECT convert(varchar(20),convert(int,INV.Cantidad)) FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO])" & vbCrLf & _
     "	ELSE " & vbCrLf & _
     "		'' " & vbCrLf & _
     "END  AS 'INVENTARIO ACTUAL'" & vbCrLf & _
     ",CASE [TIPO MOV] " & vbCrLf & _
     "	WHEN 'VENTA' THEN (SELECT VE.SUFIJO + '-' + VE.Folio FROM VentasEncabezado VE WHERE VE.ID=[ID MOVIMIENTO]) " & vbCrLf & _
     "	WHEN 'DEVOLUCION' THEN (SELECT DE.PREFIJO + '-' + DE.Folio FROM DevolucionesEncabezado DE WHERE DE.ID=[ID MOVIMIENTO]) " & vbCrLf & _
     "	ELSE '--'" & vbCrLf & _
     "END AS REFERENCIA" & vbCrLf & _
     "FROM #tempInv T0" & vbCrLf & _
     "WHERE " & filtroFecha & vbCrLf & _
     "ORDER BY [ID_STORE], [FECHA MOVIMIENTO]  " & vbCrLf & _
     "" & vbCrLf & _
     "" & vbCrLf & _
     ""


        daDepostis = Master.DBConn.GetQuerydts(squery)
        daDepostis = Master.DBConn.GetQuerydts(squery2)

        Store1.DataSource = daDepostis
        Store1.DataBind()
        Store1.LoadPage(100)
        'PagingToolbar1.SetPageSize(100)
    End Sub





    Sub toXLS()

        Dim ds As DataSet = Nothing
        Dim filtroTienda As String = ""
        Dim filtroUsuario As String = ""
        Dim filtroFecha As String = ""
        Dim filtroConsigna As String = ""

        If cmbTiendas.SelectedItem.Value <> "-1" Then
            filtroTienda = " AND ADS.AdminStoreID = " & cmbTiendas.SelectedItem.Value
            filtroConsigna = " AND ADS.AdminStoreID in (Select ads1.AdminStoreID from AdminStore ADS1 where ads1.whsConsigID = " & cmbTiendas.SelectedItem.Value & ")"
        End If

        If cmbTiendas.SelectedItem.Value = "-1" Then
            filtroTienda = " AND ADS.AdminStoreID IN(SELECT  ADS2.AdminStoreID " & vbCrLf & _
                            "FROM AdminStore ADS2 " & vbCrLf & _
                            "LEFT JOIN UserStores US " & vbCrLf & _
                            "ON US.AdminStoreID = ADS2.AdminStoreID " & vbCrLf & _
                            "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ")"
            filtroConsigna = " "
        End If

        'cambio a formato yyyyMMdd
        If Not DateField1.SelectedValue Is Nothing And Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = "  ( convert(date,[FECHA MOVIMIENTO]) between Convert(Date,'" & Format(DateField1.SelectedValue, "yyyyMMdd") & "') AND  Convert(Date,'" & Format(DateField2.SelectedValue, "yyyyMMdd") & "')) "
        ElseIf Not DateField1.SelectedValue Is Nothing Then
            filtroFecha = "  [FECHA MOVIMIENTO] = '" & Format(DateField1.SelectedValue, "yyyyMMdd") & "' "
        ElseIf Not DateField2.SelectedValue Is Nothing Then
            filtroFecha = "  convert(date,[FECHA MOVIMIENTO]) = '" & Format(DateField2.SelectedValue, "yyyyMMdd") & "' "
        Else
            filtroFecha = "  convert(date,[FECHA MOVIMIENTO]) = Convert(Date,'" & Format(Now(), "yyyyMMdd") & "') "
        End If

        Dim squery As String
        squery = " " & _
        "drop table #tempInv " & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA DIRECTA' AS 'TIPO MOV'" & vbCrLf & _
        ",EDE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EDE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EDD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EDD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",EDD.Comentarios" & vbCrLf & _
        "into #tempInv" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaDirectaDetalle EDD INNER JOIN" & vbCrLf & _
        "                      EntradaDirectaEncabezado EDE ON EDD.IDEntrada = EDE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EDD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EDD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT   " & vbCrLf & _
        "'VENTA' AS 'TIPO MOV'  " & vbCrLf & _
        ",VEE.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",VEE.ID AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",VED.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",VED.IDArticulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      VENTASDETALLE VED INNER JOIN" & vbCrLf & _
        "                      VENTASENCABEZADO VEE ON VED.IDVENTA = VEE.ID ON " & vbCrLf & _
        "                      ADS.AdminStoreID = VED.IDSTORE INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON VED.IDArticulo = ART.IDArticulo" & vbCrLf & _
        "WHERE VEE.Facturado=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'DEVOLUCION' AS 'TIPO MOV'" & vbCrLf & _
        ",DED.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",DEE.ID AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",DED.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",DED.IDArticulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE'" & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      DEVOLUCIONESDETALLE DED INNER JOIN" & vbCrLf & _
        "                      DEVOLUCIONESENCABEZADO DEE ON DEE.ID = DED.IDDevolucion ON " & vbCrLf & _
        "                      ADS.AdminStoreID = DED.IDSTORE INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON DED.IDArticulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      VentasEncabezado VEE ON DEE.IDVenta = VEE.ID" & vbCrLf & _
        "WHERE VEE.Facturado=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'ENVIO PENDIENTE' AS 'TIPO MOV'" & vbCrLf & _
        ",EVE.FechaEnvio AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EVE.IDENVIO AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EVD.CantidadPendiente * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EVD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EnviosDetalle EVD INNER JOIN" & vbCrLf & _
        "                      EnviosEncabezado EVE ON EVD.IDEnvio = EVE.IDEnvio ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EVD.TiendaOrigen INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EVD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE  EVD.CantidadPendiente>0  " & filtroTienda & vbCrLf & _
        " UNION" & vbCrLf & _
        "SELECT     " & vbCrLf & _
        "'ENVIO PROCESADO' AS 'TIPO MOV'" & vbCrLf & _
        ",REC.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",REC.IDRecepcion AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",REC.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",REC.Articulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         Recepciones REC INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON REC.Articulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      AdminStore ADS ON REC.AlmacenOrigen = ADS.AdminStoreID" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT   " & vbCrLf & _
        "'RECEPCION' AS 'TIPO MOV'  " & vbCrLf & _
        ",REC.Fecha AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",REC.IDRecepcion AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",REC.Cantidad  AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",REC.Articulo AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         Recepciones REC INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON REC.Articulo = ART.IDArticulo INNER JOIN" & vbCrLf & _
        "                      AdminStore ADS ON REC.AlmacenDestino = ADS.AdminStoreID" & vbCrLf & _
        "WHERE 1=1  " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA OC' AS 'TIPO MOV'" & vbCrLf & _
        ",EOE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EOE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",EOD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EOD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaOCDet EOD INNER JOIN" & vbCrLf & _
        "                      EntradasOC EOE ON EOD.IDEntrada = EOE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EOD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EOD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA CONS X OC' AS 'TIPO MOV'" & vbCrLf & _
        ",EOE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",EOE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",(Select ads1.StoreName from AdminStore ADS1 where ads1.AdminStoreID  = ADS.whsConsigID) AS 'TIENDA'" & vbCrLf & _
        ",(EOD.Cantidad *-1) AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",EOD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.whsConsigID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaOCDet EOD INNER JOIN" & vbCrLf & _
        "                      EntradasOC EOE ON EOD.IDEntrada = EOE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = EOD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON EOD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroConsigna & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'ENTRADA TRAS CONSIGNA' AS 'TIPO MOV'" & vbCrLf & _
        ",ETE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",ETE.IDEntrada AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",ETD.Cantidad AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",ETD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      EntradaTransDet ETD INNER JOIN" & vbCrLf & _
        "                      EntradaTrans ETE ON ETD.IDEntrada = ETE.IDEntrada ON " & vbCrLf & _
        "                      ADS.AdminStoreID = ETD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON ETD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA DIRECTA' AS 'TIPO MOV'" & vbCrLf & _
        ",SAE.FechaSalWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",SAE.IDSALIDA AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",SAD.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",SAD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",SAD.Comentarios " & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      SalidaDetalle SAD INNER JOIN" & vbCrLf & _
        "                      SalidaEncabezado SAE ON SAD.IDSalida = SAE.IDSalida ON " & vbCrLf & _
        "                      ADS.AdminStoreID = SAD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON SAD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "UNION" & vbCrLf & _
        "SELECT " & vbCrLf & _
        "'SALIDA TRAS CONSIGNA' AS 'TIPO MOV'" & vbCrLf & _
        ",STE.FechaEntWeb AS 'FECHA MOVIMIENTO'" & vbCrLf & _
        ",STE.IDSALIDA AS 'ID MOVIMIENTO'" & vbCrLf & _
        ",ADS.StoreName AS 'TIENDA'" & vbCrLf & _
        ",STD.Cantidad * -1 AS 'CANTIDAD'" & vbCrLf & _
        ",(SELECT OITM.ITEMNAME FROM " & Session("SAPDB") & ".OITM OITM WHERE OITM.ITEMCODE = ART.ARTICULOSBO) AS 'DESC ARTÍCULO'" & vbCrLf & _
        ",STD.ARTICULO AS 'ID_ARTICULO'" & vbCrLf & _
        ",ADS.AdminStoreID as 'ID_STORE' " & vbCrLf & _
        ",'' AS Comentarios" & vbCrLf & _
        "FROM         AdminStore ADS INNER JOIN" & vbCrLf & _
        "                      SalidaTransDet STD INNER JOIN" & vbCrLf & _
        "                      SalidaTrans STE ON STD.IDSalida = STE.IDSalida ON " & vbCrLf & _
        "                      ADS.AdminStoreID = STD.IDTienda INNER JOIN" & vbCrLf & _
        "                      Articulos ART ON STD.Articulo = ART.IDArticulo" & vbCrLf & _
        "WHERE 1=1 " & filtroTienda & vbCrLf & _
        "" & vbCrLf & _
        ""

        Dim squery2 As String

        squery2 = "SELECT " & vbCrLf & _
        "T0.*, " & vbCrLf & _
        "(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) AS 'CANTIDAD ACUMULADA'" & vbCrLf & _
        "FROM #tempInv T0" & vbCrLf & _
        "WHERE " & filtroFecha & vbCrLf & _
        "ORDER BY [FECHA MOVIMIENTO]" & vbCrLf & _
        "" & vbCrLf & _
        "" & vbCrLf & _
        ""

        squery2 = "SELECT " & vbCrLf & _
     "T0.*, " & vbCrLf & _
     "(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) AS 'CANTIDAD ACUMULADA'" & vbCrLf & _
     ",CASE WHEN " & vbCrLf & _
     "	(SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) =" & vbCrLf & _
     "	(SELECT INV.Cantidad FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO]) THEN " & vbCrLf & _
     "		(SELECT convert(varchar(20),convert(int,INV.Cantidad)) FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO])" & vbCrLf & _
     "	WHEN (SELECT SUM(CANTIDAD) FROM #tempInv T1 WHERE T1.[FECHA MOVIMIENTO]<=T0.[FECHA MOVIMIENTO] AND T1.ID_STORE=T0.ID_STORE AND T0.ID_ARTICULO=T1.ID_ARTICULO) <0 THEN" & vbCrLf & _
     "		(SELECT convert(varchar(20),convert(int,INV.Cantidad)) FROM Inventarios INV WHERE INV.Almacen=[ID_STORE] AND INV.IDArticulo=[ID_ARTICULO])" & vbCrLf & _
     "	ELSE " & vbCrLf & _
     "		'' " & vbCrLf & _
     "END  AS 'INVENTARIO ACTUAL'" & vbCrLf & _
     ",CASE [TIPO MOV] " & vbCrLf & _
     "	WHEN 'VENTA' THEN (SELECT VE.SUFIJO + '-' + VE.Folio FROM VentasEncabezado VE WHERE VE.ID=[ID MOVIMIENTO]) " & vbCrLf & _
     "	WHEN 'DEVOLUCION' THEN (SELECT DE.PREFIJO + '-' + DE.Folio FROM DevolucionesEncabezado DE WHERE DE.ID=[ID MOVIMIENTO]) " & vbCrLf & _
     "	ELSE '--'" & vbCrLf & _
     "END AS REFERENCIA" & vbCrLf & _
     "FROM #tempInv T0" & vbCrLf & _
     "WHERE " & filtroFecha & vbCrLf & _
     "ORDER BY [ID_STORE], [FECHA MOVIMIENTO]  " & vbCrLf & _
     "" & vbCrLf & _
     "" & vbCrLf & _
     ""


        ds = Master.DBConn.GetQuerydts(squery)
        ds = Master.DBConn.GetQuerydts(squery2)



        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try

        'Prompt the user to open or save the file.
        '        If blnOpen Then
        '            Response.Write("<script>")
        '            Response.Write("window.open('~/ReporteInventarios/" & strUniqueFn & "','_blank')")
        '            Response.Write("</script>")
        '        End If
        '5:      If strUniqueFn <> "" Then 'get absolute path of the file
        '6:          Dim path As String = Server.MapPath(strUniqueFn) 'get file object as FileInfo
        '7:          Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
        '8:          If file.Exists Then 'set appropriate headers
        '9:              Response.Clear()
        '10:             Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '11:             Response.AddHeader("Content-Length", file.Length.ToString())
        '12:             Response.ContentType = "application/octet-stream"
        '13:             Response.WriteFile(file.FullName)
        '14:             Response.End() 'if file does not exist
        '15:         Else
        '16:             Response.Write("This file does not exist.")
        '17:         End If 'nothing in the URL as HTTP GET
        '18:     Else
        '19:         Response.Write("Please provide a file to download.")
        '20:     End If
        Response.Redirect(strUniqueFn)
    End Sub
End Class