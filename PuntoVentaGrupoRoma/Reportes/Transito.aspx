﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Transito.aspx.vb" Inherits="PuntoVentaGrupoRoma.Transito" MasterPageFile="~/admin.master"%>
<%@ MasterType VirtualPath="~/admin.master" %>
<%@ Import Namespace="System.Threading" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
<ext:ResourceManager ID="ResourceManager1" runat="server" />

    <ext:GridPanel ID="gridPanel1" IDMode="Explicit" runat="server" AutoHeight="true"
        Title="Reporte de Transferencias" Layout="fit">
        
        <TopBar>
            <ext:Toolbar ID="Toolbar1" runat="server">
                <Items>
                    <ext:ComboBox ID="cmbTiendas" IDMode="Explicit" runat="server" AllowBlank="true"
                        Width="300" DisplayField="StoreName" ValueField="AdminStoreID" TypeAhead="true" Mode="Default"
                        TriggerAction="All" EmptyText="Filtrar por tienda...">
                        <ListConfig IDMode="Explicit"></ListConfig>
                        <Store>
                            <ext:Store ID="storeStores" IDMode="Explicit" runat="server">
                                <Model>
                                    <ext:Model runat="server" IDProperty="AdminStoreID">
                                        <Fields>
                                            <ext:ModelField Name="AdminStoreID">
                                            </ext:ModelField>
                                            <ext:ModelField Name="StoreName">
                                                </ext:ModelField>
                                                <ext:ModelField Name="WHSID">
                                                </ext:ModelField>
                                        </Fields>
                                    </ext:Model>
                                </Model>
                            </ext:Store>
                        </Store>
                        <Items>     
                            <ext:ListItem Text="Todos" Value="-1" />
                        </Items>
                    </ext:ComboBox>
                    <ext:Button runat="server" ID="Button2" Text="Buscar por Tienda" Icon="BookMagnify"
                        IconCls="BookMagnify16" Scale="Small" IconAlign="Top">
                        <DirectEvents>
                            <Click OnEvent="Search_By_WHS">
                                <EventMask ShowMask="true" Msg="Ejecutando Consulta..." MinDelay="1000" />
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                    <ext:Button ID="btnExcel" runat="server" Text="Excel" Icon="PageExcel" IconAlign="Top">
                        <DirectEvents>
                            <Click OnEvent="toXLS">
                                <%--  <EventMask ShowMask="true" Msg="Exportando..." MinDelay="3000" />--%>
                            </Click>
                        </DirectEvents>
                    </ext:Button>
                </Items>
            </ext:Toolbar>
        </TopBar>
        <ColumnModel>
            <Columns>
                <ext:Column runat="server" DataIndex="IDEnvio"  Header = "# de Envio">
                </ext:Column>
                <ext:Column runat="server" DataIndex="ArticuloSBO" ColumnID="ArticuloSBO" Header="ArticuloSBO">
                </ext:Column>
                <ext:Column runat="server" DataIndex="Descripcion" Header="Descripcion">
                </ext:Column> 
                <ext:Column runat="server" DataIndex="Usuario" Header="Usuario">
                </ext:Column>
                <ext:DateColumn runat="server" DataIndex="FechaEnvio" Header="Fecha de Envio">
                </ext:DateColumn>
                <ext:Column runat="server" DataIndex="Cantidad" Header="Cantidad" Align="Center">
                </ext:Column>
                <ext:Column runat="server" DataIndex="TiendaOrigen" Header="Tienda Origen" Align="Center">
                </ext:Column>
                <ext:Column runat="server" DataIndex="TiendaDestino" Header="Tienda Destino" Align="Center">
                </ext:Column> 
                <ext:Column runat="server" DataIndex="FechaDif" Header="Dias en Transito" Align="Center">
                </ext:Column>
                <ext:Column runat="server" DataIndex="Status" Header="Status" Align="Center">
                </ext:Column>
            </Columns>
        </ColumnModel>
        <View>
            <ext:GridView ID="GridView2" runat="server" ForceFit="true" >
<%--<Templates>
<Header ID="ctl24" IDMode="Explicit"></Header>
</Templates>--%>
            </ext:GridView>
        </View>
        <Store>
            <ext:Store ID="Store1" IDMode="Explicit" runat="server" AutoLoad="true">
                <Model>
                    <ext:Model runat="server" >
                        <Fields>
                            <ext:ModelField Name="IDEnvio">
                            </ext:ModelField>
                            <ext:ModelField Name="ArticuloSBO">
                            </ext:ModelField> 
                            <ext:ModelField Name="Descripcion">
                            </ext:ModelField>
                            <ext:ModelField Name="Usuario">
                            </ext:ModelField>
                            <ext:ModelField Name="FechaEnvio" Type="Date">
                            </ext:ModelField>
                            <ext:ModelField Name="Cantidad">
                            </ext:ModelField>
                            <ext:ModelField Name="TiendaOrigen">
                            </ext:ModelField>
                            <ext:ModelField Name="TiendaDestino">
                            </ext:ModelField> 
                            <ext:ModelField Name="FechaDif">
                            </ext:ModelField>
                            <ext:ModelField Name="Status">
                            </ext:ModelField>
                        </Fields>
                    </ext:Model>
                </Model>
            </ext:Store>
        </Store>
        <Features>
            <ext:GridFilters runat="server" ID="GridFilters1">
                <Filters>
                    <ext:DateFilter DataIndex="FechaEnvio" AfterText="Despues de:" BeforeText="Antes de:" OnText="El día:">
                    <DatePickerOptions></DatePickerOptions>
                    </ext:DateFilter> 
                    <ext:ListFilter DataIndex="StatusText" Options="Abierto,Cancelado,Recibido" />
                </Filters>
            </ext:GridFilters>
        </Features>
        <SelectionModel>
            <ext:RowSelectionModel ID="RowSelectionModel2" runat="server" SingleSelect="true">
            </ext:RowSelectionModel>
        </SelectionModel>
        <BottomBar>
            <ext:PagingToolbar ID="PagingToolbar1" runat="server">
            </ext:PagingToolbar>
        </BottomBar>
    </ext:GridPanel>
</asp:Content>
