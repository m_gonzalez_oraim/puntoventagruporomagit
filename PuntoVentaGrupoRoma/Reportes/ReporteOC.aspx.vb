﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.IO.Path
Imports System.Security.Principal
Imports Ext.Net

Public Class ReporteOC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.AdminStoreID as WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID " & _
                                          "WHERE US.AdminUserID = " & Session.Item("AdminUserID"))

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"


            Dim daDepostis As DataSet = Nothing
            Dim squery As String = ""
            Dim strWhere As String = ""
            Dim strWhere2 As String = ""

            If IsNothing(Session("RTVTPOS")) Then
                Exit Sub
            End If

            If Session("RTVTPOS") = False Then
                Exit Sub
            End If

            Dim FormasDePago As String = ""
            Dim FormasDePago2 As String = ""

            If Session("RTVTTIE") = "-1" Then
                strWhere2 = " and EntradaOCDet.IDTIENDA IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                    "FROM AdminStore ADS " & vbCrLf & _
                                    "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                    "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ") "

            Else

                strWhere2 = " and EntradaOCDet.idTienda = '" & Session("RTVTTIE") & "'"

            End If

            strWhere = ""

            If Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString <> "" Then
                strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
            ElseIf Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString = "" Then
                strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' "
            ElseIf Session("RTVTFI").ToString = "" And Session("RTVTFF").ToString <> "" Then
                strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
            End If




            cmbTiendas.SelectedItem.Value = Session("RTVTTIE")


            Session("RTVTPOS") = False
            Session("RTVTTIE") = -1
            Session("RTVTFI") = Nothing
            Session("RTVTFF") = Nothing

            squery = "set dateformat dmy  " & vbCrLf & _
            "SELECT     EntradaOCDet.IDEntrada," & vbCrLf & _
            "(SELECT DocNum FROM  " & Session("SAPDB") & ".OINV AS OINV WHERE (DocEntry = EntradaOCDet.NoDocSBO)) AS DocumentoSAP," & vbCrLf & _
            "(SELECT ItemName FROM " & Session("SAPDB") & ".OITM AS OITM WHERE (ItemCode = Articulos.ArticuloSBO)) AS Artículo, " & vbCrLf & _
            "EntradaOCDet.Cantidad, EntradaOCDet.LineaSBO, EntradasOC.FechaSBO, EntradasOC.FechaEntWeb, EntradasOC.SNSBO,  " & vbCrLf & _
            "(AdminUser.FirstName + ' ' + AdminUser.LastName) as Usuario, AdminStore.StoreName AS Tienda" & vbCrLf & _
            ",inv1.U_FOLPOS as 'FolioPOS' " & vbCrLf & _
            "FROM         EntradaOCDet INNER JOIN" & vbCrLf & _
            "                      EntradasOC ON EntradaOCDet.IDEntrada = EntradasOC.IDEntrada INNER JOIN" & vbCrLf & _
            "                      Articulos ON EntradaOCDet.Articulo = Articulos.IDArticulo INNER JOIN" & vbCrLf & _
            "                      AdminUser ON EntradasOC.IDUsuario = AdminUser.AdminUserID INNER JOIN" & vbCrLf & _
            "                      AdminStore ON EntradaOCDet.idTienda = AdminStore.AdminStoreID " & vbCrLf & _
            "		INNER JOIN   " & Session("SAPDB") & ".inv1 on EntradaOCDet.nodocsbo = inv1.docentry    and inv1.linenum = EntradaOCDet.lineasbo" & vbCrLf & _
            "WHERE 1 = 1 " & strWhere & strWhere2 & vbCrLf & _
            "and storetypeid <> 3" & vbCrLf & _
            "ORDER BY EntradaOCDet.IDEntrada, EntradaOCDet.LineaSBO" & vbCrLf & _
            "" & vbCrLf & _
            "" & vbCrLf & _
            "" & vbCrLf & _
            ""

            daDepostis = Master.DBConn.GetQuerydts(squery)

            For Each dc As DataColumn In daDepostis.Tables.Item(0).Columns

                Store1.Model.Item(0).Fields.Add(dc.ColumnName)

                Dim col As New Ext.Net.Column
                col.DataIndex = dc.ColumnName
                col.Text = dc.ColumnName

                'If col.Header <> "Tienda" Then
                '    col.Renderer.Format = RendererFormat.UsMoney
                'End If

                gridPanel1.ColumnModel.Columns.Add(col)

            Next



            Store1.DataSource = daDepostis
            Store1.DataBind()
            Store1.LoadPage(400)
            'PagingToolbar1.SetPageSize(400)

        End If
    End Sub

    Sub Search_By_WHS()


        Session("RTVTPOS") = True
        Session("RTVTTIE") = cmbTiendas.SelectedItem.Value

        If DateField1.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFI") = DateField1.Value
        Else
            Session("RTVTFI") = ""
        End If


        If DateField2.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFF") = DateField2.Value
        Else
            Session("RTVTFF") = ""
        End If

        Response.Redirect("ReporteOC.aspx")

    End Sub


    Sub toXLS()

        Dim ds As DataSet = Nothing
        Dim daDepostis As DataSet = Nothing
        Dim squery As String = ""
        Dim strWhere As String = ""
        Dim strWhere2 As String = ""

        Session("RTVTTIE") = cmbTiendas.SelectedItem.Value

        If DateField1.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFI") = DateField1.Value
        Else
            Session("RTVTFI") = ""
        End If

        If DateField2.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFF") = DateField2.Value
        Else
            Session("RTVTFF") = ""
        End If

        Dim FormasDePago As String = ""
        Dim FormasDePago2 As String = ""

        If Session("RTVTTIE") = "-1" Then
            strWhere2 = " and EntradaOCDet.IDTIENDA IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                "FROM AdminStore ADS " & vbCrLf & _
                                "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ") "

        Else

            strWhere2 = " and EntradaOCDet.idTienda = '" & Session("RTVTTIE") & "'"

        End If

        strWhere = ""

        If Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString <> "" Then
            strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
        ElseIf Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString = "" Then
            strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' "
        ElseIf Session("RTVTFI").ToString = "" And Session("RTVTFF").ToString <> "" Then
            strWhere = " and cast(EntradasOC.FechaEntWeb as date) between '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
        End If




        cmbTiendas.SelectedItem.Value = Session("RTVTTIE")


        Session("RTVTPOS") = False
        Session("RTVTTIE") = -1
        Session("RTVTFI") = Nothing
        Session("RTVTFF") = Nothing

        squery = "set dateformat dmy  " & vbCrLf & _
          "SELECT     EntradaOCDet.IDEntrada," & vbCrLf & _
          "(SELECT DocNum FROM  " & Session("SAPDB") & ".OINV AS OINV WHERE (DocEntry = EntradaOCDet.NoDocSBO)) AS DocumentoSAP," & vbCrLf & _
          "(SELECT ItemName FROM " & Session("SAPDB") & ".OITM AS OITM WHERE (ItemCode = Articulos.ArticuloSBO)) AS Artículo, " & vbCrLf & _
          "EntradaOCDet.Cantidad, EntradaOCDet.LineaSBO, EntradasOC.FechaSBO, EntradasOC.FechaEntWeb, EntradasOC.SNSBO,  " & vbCrLf & _
          "(AdminUser.FirstName + ' ' + AdminUser.LastName) as Usuario, AdminStore.StoreName AS Tienda" & vbCrLf & _
          ",inv1.U_FOLPOS as 'FolioPOS' " & vbCrLf & _
          "FROM         EntradaOCDet INNER JOIN" & vbCrLf & _
          "                      EntradasOC ON EntradaOCDet.IDEntrada = EntradasOC.IDEntrada INNER JOIN" & vbCrLf & _
          "                      Articulos ON EntradaOCDet.Articulo = Articulos.IDArticulo INNER JOIN" & vbCrLf & _
          "                      AdminUser ON EntradasOC.IDUsuario = AdminUser.AdminUserID INNER JOIN" & vbCrLf & _
          "                      AdminStore ON EntradaOCDet.idTienda = AdminStore.AdminStoreID " & vbCrLf & _
          "		INNER JOIN     DORMIMUNDO_PRODUCTIVA.DBO.inv1 on EntradaOCDet.nodocsbo = inv1.docentry    and inv1.linenum = EntradaOCDet.lineasbo" & vbCrLf & _
          "WHERE 1 = 1 " & strWhere & strWhere2 & vbCrLf & _
          "" & vbCrLf & _
          "ORDER BY EntradaOCDet.IDEntrada, EntradaOCDet.LineaSBO" & vbCrLf & _
          "" & vbCrLf & _
          "" & vbCrLf & _
          "" & vbCrLf & _
          ""

        ds = Master.DBConn.GetQuerydts(squery)

        Session("RTVTTIE") = Nothing
        Session("RTVTFI") = Nothing
        Session("RTVTFF") = Nothing

        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try


        Response.Redirect(strUniqueFn)
    End Sub

    Protected Sub Store1_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim alto As String = ""
    End Sub
End Class