﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.IO.Path
Imports System.Security.Principal
Imports Ext.Net

Public Class ReporteVTTienda
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.AdminStoreID as WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID " & _
                                          "WHERE US.AdminUserID = " & Session.Item("AdminUserID"))

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"


            Dim daDepostis As DataSet = Nothing
            Dim squery As String = ""
            Dim strWhere As String = ""
            Dim strWhere2 As String = ""

            If IsNothing(Session("RTVTPOS")) Then
                Exit Sub
            End If

            If Session("RTVTPOS") = False Then
                Exit Sub
            End If

            Dim FormasDePago As String = ""
            Dim FormasDePago2 As String = ""

            If Session("RTVTTIE") = "-1" Then
                strWhere2 = " and ve.idstore IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                    "FROM AdminStore ADS " & vbCrLf & _
                                    "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                    "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ") "

            Else

                strWhere2 = " and ve.idstore = '" & Session("RTVTTIE") & "'"

            End If

            strWhere = ""

            If Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString <> "" Then
                strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
            ElseIf Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString = "" Then
                strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' "
            ElseIf Session("RTVTFI").ToString = "" And Session("RTVTFF").ToString <> "" Then
                strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
            End If


            squery = "select distinct vp.FormaPago  from VentasPagos vp"

            daDepostis = Master.DBConn.GetQuerydts(squery)
            Dim rows As Integer = daDepostis.Tables.Item(0).Rows.Count
            Dim row As Integer = 0

            For Each DR As DataRow In daDepostis.Tables.Item(0).Rows

                row = row + 1

                If row <> rows Then
                    FormasDePago = FormasDePago & "[" & DR(0) & "],"
                    FormasDePago2 = FormasDePago2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "],"
                Else
                    FormasDePago = FormasDePago & "[" & DR(0) & "]"
                    FormasDePago2 = FormasDePago2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "]"
                End If

            Next

            cmbTiendas.SelectedItem.Value = Session("RTVTTIE")
            'DateField1.Value = Session("RTVTFI")
            'DateField2.Value = Session("RTVTFF")

            Session("RTVTPOS") = False
            Session("RTVTTIE") = -1
            Session("RTVTFI") = Nothing
            Session("RTVTFF") = Nothing

            squery = "set dateformat dmy  " & vbNewLine & vbNewLine

            squery = squery & " DECLARE @ReporteVentas TABLE " & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & "   [Articulo] Varchar(100),  " & vbNewLine
            squery = squery & " [Stock] int,  " & vbNewLine
            squery = squery & " [Almacen] VARCHAR(100)" & vbNewLine
            squery = squery & " )  " & vbNewLine

            squery = squery & " INSERT INTO @ReporteVentas                  " & vbNewLine
            squery = squery & " select "
            squery = squery & " (select StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]"
            squery = squery & " ,vp.Monto "
            squery = squery & " ,vp.FormaPago as [Concepto]"
            squery = squery & " from"
            squery = squery & " VentasPagos vp,VentasEncabezado ve"
            squery = squery & " where vp.IDVenta = ve.ID"
            squery = squery & " " & strWhere & " " & strWhere2
            squery = squery & " union all" & vbNewLine
            squery = squery & " select " & vbNewLine
            squery = squery & " (select StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]" & vbNewLine
            squery = squery & " ,(select isnull(U_PM,0) from " & Session("SAPDB") & ".OITM where ItemCode=(select ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) * vd.Cantidad" & vbNewLine
            squery = squery & " ,'Push Money' As [Concepto]" & vbNewLine
            squery = squery & " from VentasEncabezado Ve,VentasDetalle Vd" & vbNewLine
            squery = squery & " where Ve.ID = vd.IDVenta" & vbNewLine
            squery = squery & " " & strWhere & " " & strWhere2 & vbNewLine & vbNewLine

            squery = squery & " SELECT Articulo as 'Tienda'," & FormasDePago2 & ",isnull([Push Money],0) as [Push Money]" & vbNewLine
            squery = squery & " FROM" & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & " SELECT *  " & vbNewLine
            squery = squery & " FROM @ReporteVentas  " & vbNewLine
            squery = squery & " ) AS SourceTable  " & vbNewLine
            squery = squery & " PIVOT" & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & " SUM([stock])" & vbNewLine
            squery = squery & " FOR [Almacen] IN (" & FormasDePago & ",[Push Money])  " & vbNewLine
            squery = squery & " ) AS PivotTable                    " & vbNewLine
            squery = squery & " order by 1" & vbNewLine

            daDepostis = Master.DBConn.GetQuerydts(squery)

            'If daDepostis.Tables.Item(0).Columns.Count <> 0 Then

            For Each dc As DataColumn In daDepostis.Tables.Item(0).Columns

                Store1.Model.Item(0).Fields.Add(dc.ColumnName)

                Dim col As New Ext.Net.Column
                col.DataIndex = dc.ColumnName
                col.Text = dc.ColumnName

                If col.Text <> "Tienda" Then
                    col.Renderer.Format = RendererFormat.UsMoney
                End If

                gridPanel1.ColumnModel.Columns.Add(col)

            Next

            'End If

            Store1.DataSource = daDepostis
            Store1.DataBind()
            Store1.LoadPage(20)

        End If
    End Sub

    Sub Search_By_WHS()


        Session("RTVTPOS") = True
        Session("RTVTTIE") = cmbTiendas.SelectedItem.Value

        If DateField1.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFI") = DateField1.Value
        Else
            Session("RTVTFI") = ""
        End If


        If DateField2.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFF") = DateField2.Value
        Else
            Session("RTVTFF") = ""
        End If

        Response.Redirect("ReporteVTTienda.aspx")

    End Sub


    Sub toXLS()

        Dim ds As DataSet = Nothing
        Dim daDepostis As DataSet = Nothing
        Dim squery As String = ""
        Dim strWhere As String = ""
        Dim strWhere2 As String = ""

        Session("RTVTTIE") = cmbTiendas.SelectedItem.Value

        If DateField1.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFI") = DateField1.Value
        Else
            Session("RTVTFI") = ""
        End If

        If DateField2.Value.ToString <> "01/01/0001 12:00:00 a.m." Then
            Session("RTVTFF") = DateField2.Value
        Else
            Session("RTVTFF") = ""
        End If

        Dim FormasDePago As String = ""
        Dim FormasDePago2 As String = ""

        If Session("RTVTTIE") = "-1" Then
            strWhere2 = " and ve.idstore IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                "FROM AdminStore ADS " & vbCrLf & _
                                "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ") "

        Else

            strWhere2 = " and ve.idstore = '" & Session("RTVTTIE") & "'"

        End If

        strWhere = ""

        If Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString <> "" Then
            strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
        ElseIf Session("RTVTFI").ToString <> "" And Session("RTVTFF").ToString = "" Then
            strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFI"), "yyyyMMdd") & "' "
        ElseIf Session("RTVTFI").ToString = "" And Session("RTVTFF").ToString <> "" Then
            strWhere = " and cast(ve.Fecha as date) between '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' and '" & Format(Session("RTVTFF"), "yyyyMMdd") & "' "
        End If


        squery = "select distinct vp.FormaPago  from VentasPagos vp"

        daDepostis = Master.DBConn.GetQuerydts(squery)
        Dim rows As Integer = daDepostis.Tables.Item(0).Rows.Count
        Dim row As Integer = 0

        For Each DR As DataRow In daDepostis.Tables.Item(0).Rows

            row = row + 1

            If row <> rows Then
                FormasDePago = FormasDePago & "[" & DR(0) & "],"
                FormasDePago2 = FormasDePago2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "],"
            Else
                FormasDePago = FormasDePago & "[" & DR(0) & "]"
                FormasDePago2 = FormasDePago2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "]"
            End If

        Next

        cmbTiendas.SelectedItem.Value = Session("RTVTTIE")
        'DateField1.Value = Session("RTVTFI")
        'DateField2.Value = Session("RTVTFF")

        Session("RTVTPOS") = False
        Session("RTVTTIE") = -1
        Session("RTVTFI") = Nothing
        Session("RTVTFF") = Nothing

        squery = "set dateformat dmy  " & vbNewLine & vbNewLine

        squery = squery & " DECLARE @ReporteVentas TABLE " & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & "   [Articulo] Varchar(100),  " & vbNewLine
        squery = squery & " [Stock] int,  " & vbNewLine
        squery = squery & " [Almacen] VARCHAR(100)" & vbNewLine
        squery = squery & " )  " & vbNewLine

        squery = squery & " INSERT INTO @ReporteVentas                  " & vbNewLine
        squery = squery & " select "
        squery = squery & " (select StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]"
        squery = squery & " ,vp.Monto "
        squery = squery & " ,vp.FormaPago as [Concepto]"
        squery = squery & " from"
        squery = squery & " VentasPagos vp,VentasEncabezado ve"
        squery = squery & " where vp.IDVenta = ve.ID"
        squery = squery & " " & strWhere & " " & strWhere2
        squery = squery & " union all" & vbNewLine
        squery = squery & " select " & vbNewLine
        squery = squery & " (select StoreName from AdminStore where AdminStoreID=ve.IDStore) as [Tienda]" & vbNewLine
        squery = squery & " ,(select isnull(U_PM,0) from " & Session("SAPDB") & ".OITM where ItemCode=(select ArticuloSBO from Articulos where IDArticulo=vd.IDArticulo)) * vd.Cantidad" & vbNewLine
        squery = squery & " ,'Push Money' As [Concepto]" & vbNewLine
        squery = squery & " from VentasEncabezado Ve,VentasDetalle Vd" & vbNewLine
        squery = squery & " where Ve.ID = vd.IDVenta" & vbNewLine
        squery = squery & " " & strWhere & " " & strWhere2 & vbNewLine & vbNewLine

        squery = squery & " SELECT Articulo as 'Tienda'," & FormasDePago2 & ",isnull([Push Money],0) as [Push Money]" & vbNewLine
        squery = squery & " FROM" & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & " SELECT *  " & vbNewLine
        squery = squery & " FROM @ReporteVentas  " & vbNewLine
        squery = squery & " ) AS SourceTable  " & vbNewLine
        squery = squery & " PIVOT" & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & " SUM([stock])" & vbNewLine
        squery = squery & " FOR [Almacen] IN (" & FormasDePago & ",[Push Money])  " & vbNewLine
        squery = squery & " ) AS PivotTable                    " & vbNewLine
        squery = squery & " order by 1" & vbNewLine

        'daDepostis = Master.DBConn.GetQuerydts(squery)
        ds = Master.DBConn.GetQuerydts(squery)

        Session("RTVTTIE") = Nothing
        Session("RTVTFI") = Nothing
        Session("RTVTFF") = Nothing

        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try

        'Prompt the user to open or save the file.
        '        If blnOpen Then
        '            Response.Write("<script>")
        '            Response.Write("window.open('~/ReporteInventarios/" & strUniqueFn & "','_blank')")
        '            Response.Write("</script>")
        '        End If
        '5:      If strUniqueFn <> "" Then 'get absolute path of the file
        '6:          Dim path As String = Server.MapPath(strUniqueFn) 'get file object as FileInfo
        '7:          Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
        '8:          If file.Exists Then 'set appropriate headers
        '9:              Response.Clear()
        '10:             Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '11:             Response.AddHeader("Content-Length", file.Length.ToString())
        '12:             Response.ContentType = "application/octet-stream"
        '13:             Response.WriteFile(file.FullName)
        '14:             Response.End() 'if file does not exist
        '15:         Else
        '16:             Response.Write("This file does not exist.")
        '17:         End If 'nothing in the URL as HTTP GET
        '18:     Else
        '19:         Response.Write("Please provide a file to download.")
        '20:     End If
        Response.Redirect(strUniqueFn)
    End Sub

    Protected Sub Store1_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim alto As String = ""
    End Sub


End Class