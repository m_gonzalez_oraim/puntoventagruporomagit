﻿Public Class Transito
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID  " & _
                                          "WHERE US.AdminUserID = " & Session.Item("AdminUserID"))

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"
        End If
    End Sub

    Sub Search_By_WHS()


        Dim daDepostis As DataSet = Nothing
        Dim squery As String
        squery = _
        "SELECT ED.IDEnvio, A.ArticuloSBO, oitm.ItemName as Descripcion, AU.FirstName + ' ' + AU.LastName AS Usuario, EE.FechaEnvio, " & vbCrLf & _
        "ED.Cantidad, ads.StoreName as TiendaOrigen, ads2.StoreName as TiendaDestino," & vbCrLf & _
        "CASE ed.Status " & vbCrLf & _
        "	WHEN 'RE' THEN DATEDIFF(day, EE.FechaEnvio, (select top 1 RECE.Fecha from Recepciones RECE where IDEnvio = ee.IDEnvio)) " & vbCrLf & _
        "	WHEN 'C' THEN 0" & vbCrLf & _
        "	WHEN 'O' THEN DATEDIFF(day, EE.FechaEnvio, GETDATE()) " & vbCrLf & _
        "END as FechaDif,  " & vbCrLf & _
        "CASE ed.Status WHEN 'RE' THEN 'Recibido' WHEN 'C' THEN 'Cancelado' WHEN 'O' THEN 'Abierto' END AS Status " & vbCrLf & _
        "FROM EnviosDetalle AS ED LEFT OUTER JOIN" & vbCrLf & _
        "  EnviosEncabezado AS EE ON ED.IDEnvio = EE.IDEnvio INNER JOIN" & vbCrLf & _
        "  Articulos AS A ON ED.Articulo = A.IDArticulo INNER JOIN" & vbCrLf & _
        "  " & Session("SAPDB") & ".OITM OITM on oitm.ItemCode=a.ArticuloSBO inner join" & vbCrLf & _
        "  AdminUser AS AU ON AU.AdminUserID = EE.IDUsuario inner join" & vbCrLf & _
        "  AdminStore as ADS on ads.AdminStoreID = ed.TiendaOrigen  inner join" & vbCrLf & _
        "  AdminStore as ADS2 on ads2.adminstoreID = ed.tiendaDestino" & vbCrLf & _
        "WHERE "

        If cmbTiendas.SelectedItem.Value = "-1" Then
            squery = squery & "(ED.TiendaOrigen IN " & vbCrLf & _
                                "  (SELECT ADS.AdminStoreID " & vbCrLf & _
                                "    FROM  AdminStore AS ADS LEFT OUTER JOIN " & vbCrLf & _
                                " UserStores AS US ON US.AdminStoreToSendID = ADS.AdminStoreID" & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ")) "

        Else
            squery = squery & "ED.TiendaOrigen = '" & cmbTiendas.SelectedItem.Value & "'"
        End If





        daDepostis = Master.DBConn.GetQuerydts(squery)

        Store1.DataSource = daDepostis
        Store1.DataBind()
        Store1.LoadPage(20)
        'PagingToolbar1.SetPageSize(20)
    End Sub


    Sub toXLS()
        Dim ds As DataSet = Nothing
        Dim squery As String
        squery = _
        "SELECT ED.IDEnvio, A.ArticuloSBO, oitm.ItemName as Descripcion, AU.FirstName + ' ' + AU.LastName AS Usuario, EE.FechaEnvio, " & vbCrLf & _
        "ED.Cantidad, ads.StoreName as TiendaOrigen, ads2.StoreName as TiendaDestino," & vbCrLf & _
        "CASE ed.Status " & vbCrLf & _
        "	WHEN 'RE' THEN DATEDIFF(day, EE.FechaEnvio, (select top 1 RECE.Fecha from Recepciones RECE where IDEnvio = ee.IDEnvio)) " & vbCrLf & _
        "	WHEN 'C' THEN 0" & vbCrLf & _
        "	WHEN 'O' THEN DATEDIFF(day, EE.FechaEnvio, GETDATE()) " & vbCrLf & _
        "END as FechaDif,  " & vbCrLf & _
        "CASE ed.Status WHEN 'RE' THEN 'Recibido' WHEN 'C' THEN 'Cancelado' WHEN 'O' THEN 'Abierto' END AS Status " & vbCrLf & _
        "FROM EnviosDetalle AS ED LEFT OUTER JOIN" & vbCrLf & _
        "  EnviosEncabezado AS EE ON ED.IDEnvio = EE.IDEnvio INNER JOIN" & vbCrLf & _
        "  Articulos AS A ON ED.Articulo = A.IDArticulo INNER JOIN" & vbCrLf & _
        "  " & Session("SAPDB") & ".OITM OITM on oitm.ItemCode=a.ArticuloSBO inner join" & vbCrLf & _
        "  AdminUser AS AU ON AU.AdminUserID = EE.IDUsuario inner join" & vbCrLf & _
        "  AdminStore as ADS on ads.AdminStoreID = ed.TiendaOrigen  inner join" & vbCrLf & _
        "  AdminStore as ADS2 on ads2.adminstoreID = ed.tiendaDestino" & vbCrLf & _
        "WHERE "

        If cmbTiendas.SelectedItem.Value = "-1" Then
            squery = squery & "(ED.TiendaOrigen IN " & vbCrLf & _
                                "  (SELECT ADS.AdminStoreID " & vbCrLf & _
                                "    FROM  AdminStore AS ADS LEFT OUTER JOIN " & vbCrLf & _
                                " UserStores AS US ON US.AdminStoreToSendID = ADS.AdminStoreID" & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & ")) "

        Else
            squery = squery & "ED.TiendaOrigen = '" & cmbTiendas.SelectedItem.Value & "'"
        End If






        ds = Master.DBConn.GetQuerydts(squery)

        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try

        'Prompt the user to open or save the file.
        '        If blnOpen Then
        '            Response.Write("<script>")
        '            Response.Write("window.open('~/ReporteInventarios/" & strUniqueFn & "','_blank')")
        '            Response.Write("</script>")
        '        End If
        '5:      If strUniqueFn <> "" Then 'get absolute path of the file
        '6:          Dim path As String = Server.MapPath(strUniqueFn) 'get file object as FileInfo
        '7:          Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
        '8:          If file.Exists Then 'set appropriate headers
        '9:              Response.Clear()
        '10:             Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '11:             Response.AddHeader("Content-Length", file.Length.ToString())
        '12:             Response.ContentType = "application/octet-stream"
        '13:             Response.WriteFile(file.FullName)
        '14:             Response.End() 'if file does not exist
        '15:         Else
        '16:             Response.Write("This file does not exist.")
        '17:         End If 'nothing in the URL as HTTP GET
        '18:     Else
        '19:         Response.Write("Please provide a file to download.")
        '20:     End If
        Response.Redirect(strUniqueFn)
    End Sub

End Class