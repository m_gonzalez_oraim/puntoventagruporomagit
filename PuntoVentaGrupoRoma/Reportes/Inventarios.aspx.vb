﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Web.UI.WebControls
Imports System.IO
Imports System.IO.Path
Imports System.Security.Principal
Imports Ext.Net

Public Class Inventarios
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Ext.Net.X.IsAjaxRequest Then
            Dim daStores As DataSet = Nothing
            daStores = Master.DBConn.GetQuerydts("SELECT  ADS.AdminStoreID, ADS.StoreName, ADS.AdminStoreID as WHSID " & _
                                          "FROM AdminStore ADS " & _
                                          "LEFT JOIN UserStores US " & _
                                          "ON US.AdminStoreID = ADS.AdminStoreID " & _
                                          "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & " and ads.StoreTypeID <> 3")

            storeStores.DataSource = daStores
            storeStores.DataBind()
            cmbTiendas.SelectedItem.Value = "-1"


            Dim daDepostis As DataSet = Nothing
            Dim squery As String = ""
            Dim strWhere As String = ""
            Dim strWhere2 As String = ""

            If IsNothing(Session("RIPOS")) Then
                Exit Sub
            End If

            If Session("RIPOS") = False Then
                Exit Sub
            End If

            Dim tiendas As String = ""

            If Session("RITIE") = "-1" Then
                strWhere2 = " WHERE Inventarios.Almacen IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                    "FROM AdminStore ADS " & vbCrLf & _
                                    "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                    "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & " and ads.StoreTypeID <> 3)"

                squery = "SELECT StoreName " & vbCrLf & _
                                    "FROM AdminStore ADS " & vbCrLf & _
                                    "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                    "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & " and ads.StoreTypeID <> 3 "
            Else

                strWhere2 = " WHERE Inventarios.Almacen = '" & Session("RITIE") & "'"

                squery = "SELECT StoreName " & vbCrLf & _
                                    "FROM AdminStore ADS " & vbCrLf & _
                                    "WHERE AdminStoreID = " & Session("RITIE")

            End If


            daDepostis = Master.DBConn.GetQuerydts(squery)
            Dim rows As Integer = daDepostis.Tables.Item(0).Rows.Count
            Dim row As Integer = 0

            For Each DR As DataRow In daDepostis.Tables.Item(0).Rows

                row = row + 1

                If row <> rows Then
                    tiendas = tiendas & "[" & DR(0) & "],"
                Else
                    tiendas = tiendas & "[" & DR(0) & "]"
                End If

            Next

            Session("RIPOS") = False
            Session("RITIE") = -1

            squery = "set dateformat dmy  " & vbNewLine & vbNewLine

            squery = squery & " DECLARE @ReporteVentas TABLE " & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & "   [Articulo] Varchar(100),  " & vbNewLine
            squery = squery & " [Stock] int,  " & vbNewLine
            squery = squery & " [Almacen] VARCHAR(100)" & vbNewLine
            squery = squery & " )  " & vbNewLine

            squery = squery & " INSERT INTO @ReporteVentas                  " & vbNewLine
            squery = squery & " SELECT     " & vbNewLine
            squery = squery & " (select ItemName from " & Session("SAPDB") & ".OITM where itemcode=articulos.ArticuloSBO) [Nombre Articulo]" & vbNewLine
            squery = squery & " ,Cantidad" & vbNewLine
            squery = squery & " ,(select StoreName from AdminStore where AdminStoreID = Almacen) as [Tienda]" & vbNewLine
            squery = squery & " FROM Inventarios INNER JOIN Articulos ON Inventarios.IDArticulo = Articulos.IDArticulo" & strWhere2 & vbNewLine
            squery = squery & " order by ArticuloSBO,Tienda                  " & vbNewLine & vbNewLine

            squery = squery & " SELECT Articulo as 'Articulo'," & tiendas & vbNewLine
            squery = squery & " FROM" & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & " SELECT *  " & vbNewLine
            squery = squery & " FROM @ReporteVentas  " & vbNewLine
            squery = squery & " ) AS SourceTable  " & vbNewLine
            squery = squery & " PIVOT" & vbNewLine
            squery = squery & " (  " & vbNewLine
            squery = squery & " SUM([stock])" & vbNewLine
            squery = squery & " FOR [Almacen] IN (" & tiendas & ")  " & vbNewLine
            squery = squery & " ) AS PivotTable                    " & vbNewLine
            squery = squery & " order by 1" & vbNewLine

            daDepostis = Master.DBConn.GetQuerydts(squery)

            For Each dc As DataColumn In daDepostis.Tables.Item(0).Columns

                Store1.Model.Item(0).Fields.Add(dc.ColumnName)

                Dim col As New Ext.Net.Column
                col.DataIndex = dc.ColumnName
                col.Text = dc.ColumnName

                gridPanel1.ColumnModel.Columns.Add(col)

            Next

            Store1.DataSource = daDepostis
            Store1.DataBind()
            Store1.LoadPage(20)

        End If
    End Sub

    Sub Search_By_WHS()


        Session("RIPOS") = True
        Session("RITIE") = cmbTiendas.SelectedItem.Value
        Response.Redirect("Inventarios.aspx")

    End Sub


    Sub toXLS()
        Dim ds As DataSet = Nothing
        Dim daDepostis As DataSet = Nothing
        Dim squery As String = ""
        Dim strWhere As String = ""
        Dim strWhere2 As String = ""

        Dim tiendas As String = ""

        If cmbTiendas.SelectedItem.Value = "-1" Then
            strWhere2 = " WHERE Inventarios.Almacen IN (SELECT ADS.AdminStoreID " & vbCrLf & _
                                "FROM AdminStore ADS " & vbCrLf & _
                                "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & " and ads.StoreTypeID <> 3) "

            squery = "SELECT StoreName " & vbCrLf & _
                                "FROM AdminStore ADS " & vbCrLf & _
                                "LEFT JOIN UserStores US ON US.AdminStoreID = ADS.AdminStoreID " & vbCrLf & _
                                "WHERE US.AdminUserID = " & Session.Item("AdminUserID") & " and ads.StoreTypeID <> 3"
        Else

            strWhere2 = " WHERE Inventarios.Almacen = '" & cmbTiendas.SelectedItem.Value & "'"

            squery = "SELECT StoreName " & vbCrLf & _
                                "FROM AdminStore ADS " & vbCrLf & _
                                "WHERE AdminStoreID = " & cmbTiendas.SelectedItem.Value

        End If


        daDepostis = Master.DBConn.GetQuerydts(squery)
        Dim rows As Integer = daDepostis.Tables.Item(0).Rows.Count
        Dim row As Integer = 0
        Dim tiendas2 As String = ""

        For Each DR As DataRow In daDepostis.Tables.Item(0).Rows

            row = row + 1

            If row <> rows Then
                tiendas = tiendas & "[" & DR(0) & "],"
                tiendas2 = tiendas2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "],"
            Else
                tiendas = tiendas & "[" & DR(0) & "]"
                tiendas2 = tiendas2 & "isnull([" & DR(0) & "],0) as [" & DR(0) & "]"
            End If

        Next

        squery = "set dateformat dmy  " & vbNewLine & vbNewLine

        squery = squery & " DECLARE @ReporteVentas TABLE " & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & "   [Articulo] Varchar(100),  " & vbNewLine
        squery = squery & " [Stock] int,  " & vbNewLine
        squery = squery & " [Almacen] VARCHAR(100)" & vbNewLine
        squery = squery & " )  " & vbNewLine

        squery = squery & " INSERT INTO @ReporteVentas                  " & vbNewLine
        squery = squery & " SELECT     " & vbNewLine
        squery = squery & " (select ItemName from " & Session("SAPDB") & ".OITM where itemcode=articulos.ArticuloSBO) [Nombre Articulo]" & vbNewLine
        squery = squery & " ,Cantidad" & vbNewLine
        squery = squery & " ,(select StoreName from AdminStore where AdminStoreID = Almacen) as [Tienda]" & vbNewLine
        squery = squery & " FROM Inventarios INNER JOIN Articulos ON Inventarios.IDArticulo = Articulos.IDArticulo" & strWhere2 & vbNewLine
        squery = squery & " order by ArticuloSBO,Tienda                  " & vbNewLine & vbNewLine

        squery = squery & " SELECT Articulo as 'Articulo'," & tiendas2 & vbNewLine
        squery = squery & " FROM" & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & " SELECT *  " & vbNewLine
        squery = squery & " FROM @ReporteVentas  " & vbNewLine
        squery = squery & " ) AS SourceTable  " & vbNewLine
        squery = squery & " PIVOT" & vbNewLine
        squery = squery & " (  " & vbNewLine
        squery = squery & " SUM([stock])" & vbNewLine
        squery = squery & " FOR [Almacen] IN (" & tiendas & ")  " & vbNewLine
        squery = squery & " ) AS PivotTable                    " & vbNewLine
        squery = squery & " order by 1" & vbNewLine

        'daDepostis = Master.DBConn.GetQuerydts(squery)
        ds = Master.DBConn.GetQuerydts(squery)

        Dim blnOpen As Boolean = False
        Dim strUniqueFn As String = ""

        Try
            ' Get the user id.
            Dim strUser As String = Session("AdminUserID") & Session("STORENAME")
            strUser = strUser.Replace(" ", "")

            ' Get the folder to store files in.
            Dim strFolder As String = Request.MapPath(".")

            ' Create a reference to the folder.
            Dim di As New IO.DirectoryInfo(strFolder)

            ' Create a list of files in the directory.
            Dim fi As IO.FileInfo() = di.GetFiles(strUser & "*.*")


            For i = 0 To fi.Length - 1
                IO.File.Delete(strFolder & "\" & fi(i).Name)
            Next

            ' Get a unique file name.
            strUniqueFn = strUser & _
            IO.Path.GetFileNameWithoutExtension(IO.Path.GetTempFileName()) & ".xls"

            ' Get the full path to the file.
            Dim strPath As String = strFolder & "\" & strUniqueFn



            ' Tweak the dataset so it displays meaningful DataSet and Table Names.
            ds.DataSetName = "My_Report"
            ds.Tables(0).TableName = "Pedidos"


            ' Write the data out as XML with an Excel extension.
            ds.WriteXml(strPath, System.Data.XmlWriteMode.IgnoreSchema)
            blnOpen = True
            'strUniqueFn = strPath
        Catch ex As Exception
            '...

        End Try

        'Prompt the user to open or save the file.
        '        If blnOpen Then
        '            Response.Write("<script>")
        '            Response.Write("window.open('~/ReporteInventarios/" & strUniqueFn & "','_blank')")
        '            Response.Write("</script>")
        '        End If
        '5:      If strUniqueFn <> "" Then 'get absolute path of the file
        '6:          Dim path As String = Server.MapPath(strUniqueFn) 'get file object as FileInfo
        '7:          Dim file As System.IO.FileInfo = New System.IO.FileInfo(path) '-- if the file exists on the server
        '8:          If file.Exists Then 'set appropriate headers
        '9:              Response.Clear()
        '10:             Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '11:             Response.AddHeader("Content-Length", file.Length.ToString())
        '12:             Response.ContentType = "application/octet-stream"
        '13:             Response.WriteFile(file.FullName)
        '14:             Response.End() 'if file does not exist
        '15:         Else
        '16:             Response.Write("This file does not exist.")
        '17:         End If 'nothing in the URL as HTTP GET
        '18:     Else
        '19:         Response.Write("Please provide a file to download.")
        '20:     End If
        Response.Redirect(strUniqueFn)
    End Sub

    Protected Sub Store1_Refresh(ByVal sender As Object, ByVal e As StoreReadDataEventArgs)
        Dim alto As String = ""
    End Sub
End Class