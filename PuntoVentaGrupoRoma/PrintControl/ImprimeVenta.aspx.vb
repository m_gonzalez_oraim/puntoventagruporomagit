﻿Imports Ext.Net
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports CrystalDecisions.Web

Public Class ImprimeVenta
    Inherits System.Web.UI.Page

    Private connstringWEB As String
    Private connstringSAP As String
    Private mensaje As String = ""
    Private oDB As New DBMaster
    Private URL As String = ConfigurationManager.AppSettings("FILES")
    Private EMAIL As String = ConfigurationManager.AppSettings("EMAIL")
    Private PASS As String = ConfigurationManager.AppSettings("EMAILPASS")
    Private SMTP As String = ConfigurationManager.AppSettings("SMPT")
    Private PORT As String = ConfigurationManager.AppSettings("EMAILPORT")
    Private body As String
    Private body2 As String
    Private report As New ReportDocument

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        CrystalReportViewer1.HasExportButton = False

        If Session("POSTBACK") = "" Then
            linkRegresar.PostBackUrl = "../Ventas/Facturacion.aspx"
        ElseIf Session("POSTBACK") = "Facturacion" Then
            linkRegresar.PostBackUrl = "../Facturacion/Facturacion.aspx"
            ReporteVentas()
        ElseIf Session("POSTBACK") = "Abonos" Then
            linkRegresar.PostBackUrl = "../Ventas/Abonos.aspx"
            ReporteAbonos(False)
        ElseIf Session("POSTBACK") = "ReimpresionAbonos" Then
            linkRegresar.PostBackUrl = "../Ventas/ReImprimeAbonos.aspx"
            ReporteAbonos(True)
        ElseIf Session("POSTBACK") = "ReimpresionFacturacion" Then
            linkRegresar.PostBackUrl = "../Ventas/ReImprimeAbonos.aspx"
            ReporteVentas()
        ElseIf Session("POSTBACK") = "Envios" Then
            linkRegresar.PostBackUrl = "../Transferencias/Envios.aspx"
            ReporteEnvios()
            'ElseIf Session("POSTBACK") = "DevCompras" Then
            '    linkRegresar.PostBackUrl = "../CancelacionOC/CancelacionOC.aspx"
            '    ReporteDevCompra()
        ElseIf Session("POSTBACK") = "CancelaVenta" Then
            linkRegresar.PostBackUrl = "../Ventas/Cancelaciones.aspx"
            ReporteCancelaVenta()
        ElseIf Session("POSTBACK") = "ReimpresionCancelaVenta" Then
            linkRegresar.PostBackUrl = "../Ventas/ReImprimeAbonos.aspx"
            ReporteCancelaVenta()
        ElseIf Session("POSTBACK") = "Compras" Then
            linkRegresar.PostBackUrl = "../Compras/Compras.aspx"
            ReporteCompras()
        ElseIf Session("POSTBACK") = "RsCompras" Then
            linkRegresar.PostBackUrl = "../Compras/ReimprimeCompras.aspx"
            ReporteCompras()
        End If

    End Sub

    Private Function ReporteCancelaVenta() As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDVENTA As Integer
        Dim report As New ReportDocument
        Try
            IDVENTA = Session("IDPRINTVENTA")
            'IDVENTA = 70

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/CancelaVentaDormimundo.rpt"))
            report.SetParameterValue(0, IDVENTA)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Private Function ReporteAbonos(ByVal reimpresion As Boolean) As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDABONO As String
        Dim IDVENTA As Integer
        Dim sQuery As String = ""
        Dim MONTOTOTAL As Double = 0

        Try
            'LA SESION DE IDPRINTVENTA EN ESTE CASO TIENE EL VALOR DEL ID DEL ABONO
            IDABONO = Session("IDPRINTVENTA")
            'IDPAGO = 15

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            sQuery = "" & _
            "SELECT     SUM(Monto) as MONTOTOTAL, IDVenta" & vbCrLf & _
            "FROM         VentasPagos" & vbCrLf & _
            "WHERE     (IDVenta = (SELECT TOP 1 IDVENTA FROM VENTASPAGOS WHERE ID = " & IDABONO & "))" & vbCrLf & _
            "and ID <= " & IDABONO & " " & vbCrLf & _
            "GROUP BY IDVenta" & vbCrLf & _
            ""
            dtC = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EXPEDIDOEN", connstringWEB)
            IDVENTA = dtC.Rows.Item(0).Item("IDVENTA")
            MONTOTOTAL = dtC.Rows.Item(0).Item("MONTOTOTAL")

            'report.Load(ConfigurationManager.AppSettings("ReporteFactura"))
            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/AbonoDormimundo.rpt"))
            report.SetParameterValue(0, IDVENTA)
            report.SetParameterValue(1, IDABONO)
            report.SetParameterValue(2, MONTOTOTAL)

            report.SetDatabaseLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"), oDB.Server, oDB.DB)


            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)
            'report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, "sa", "300480")

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Private Function ReporteVentas() As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDVENTA As String
        Dim MONTOABONOS As Double
        Dim MONTOVENTA As Double
        Dim SALDO As Double
        Dim sQuery As String = ""
        Dim report As New ReportDocument
        Try
            IDVENTA = Session("IDPRINTVENTA")
            'IDVENTA = 12

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            sQuery = "SELECT (SELECT SUM(MONTO) FROM VentasPagos WHERE IDVenta= " & IDVENTA & ") AS MONTOABONOS" & vbCrLf & _
            ",(SELECT SUM(TOTALLINEA) FROM VentasDetalle WHERE IDVenta= " & IDVENTA & ") AS MONTOVENTA"
            dtC = oDB.EjecutaQry_Tabla(sQuery, CommandType.Text, "EXPEDIDOEN", connstringWEB)
            MONTOABONOS = dtC.Rows.Item(0).Item("MONTOABONOS")
            MONTOVENTA = dtC.Rows.Item(0).Item("MONTOVENTA")
            SALDO = MONTOVENTA - MONTOABONOS

            'report.Load(ConfigurationManager.AppSettings("ReporteFactura"))
            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/VentaDormimundo.rpt"))
            report.SetParameterValue(0, IDVENTA)
            report.SetParameterValue(1, SALDO)
            report.SetParameterValue(2, MONTOABONOS)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Private Function ReporteEnvios() As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDENVIO As Integer
        Dim report As New ReportDocument
        Try
            IDENVIO = Session("IDPRINTVENTA")
            'IDENVIO = 12

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            'report.Load(ConfigurationManager.AppSettings("ReporteFactura"))
            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/TransferenciaDormimundo.rpt"))
            report.SetParameterValue(0, IDENVIO)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Private Function ReporteDevCompra() As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDDEV As Integer
        Dim report As New ReportDocument
        Try
            IDDEV = Session("IDPRINTVENTA")
            'IDENVIO = 12

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            'report.Load(ConfigurationManager.AppSettings("ReporteFactura"))
            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/DevCompraDormimundo.rpt"))
            report.SetParameterValue(0, IDDEV)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Private Function ReporteCompras() As Boolean
        Dim dtC As New DataTable
        Dim oDB As New DBMaster
        Dim IDCOMPRA As Integer
        Dim report As New ReportDocument
        Try
            IDCOMPRA = Session("IDPRINTVENTA")
            'IDCOMPRA = 2

            connstringSAP = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString
            connstringWEB = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

            'report.Load(ConfigurationManager.AppSettings("ReporteFactura"))
            report.Load(HttpContext.Current.Request.MapPath("../Content/RPTS/ComprasDormimundo.rpt"))
            report.SetParameterValue(0, IDCOMPRA)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConn").ConnectionString)
            report.DataSourceConnections.Item(0).SetConnection(oDB.Server, oDB.DB, False)

            oDB.ConectaDBConnString(ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString)
            report.DataSourceConnections.Item(1).SetConnection(oDB.Server, oDB.DB, False)

            report.DataSourceConnections.Item(0).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))
            report.DataSourceConnections.Item(1).SetLogon(ConfigurationManager.AppSettings("User"), ConfigurationManager.AppSettings("Pass"))

            CrystalReportViewer1.ReportSource = report
        Catch ex As Exception

        End Try
    End Function

    Protected Overrides Sub OnPreRender(ByVal e As EventArgs)
    End Sub
End Class