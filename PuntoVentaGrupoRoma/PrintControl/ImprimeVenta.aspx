﻿<%@ Page Language="vb" MasterPageFile="~/admin.master" AutoEventWireup="false" CodeBehind="ImprimeVenta.aspx.vb" Inherits="PuntoVentaGrupoRoma.ImprimeVenta" %>

<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>
<%@ MasterType VirtualPath="~/admin.master" %> 

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="ContentPlaceHolder1">
    <table > 
        <tr> 
            <td> 
                <cr:crystalreportviewer ID="CrystalReportViewer1" runat="server" 
                    AutoDataBind="True" Height="1039px" 
                    Width="800px"  PrintMode="Pdf" BorderStyle="Groove" BorderWidth="1px" 
                    DisplayGroupTree="False" HasToggleGroupTreeButton="False" HasViewList="False" 
                    ToolbarStyle-BorderColor="White" ToolbarStyle-BorderStyle="Inset" 
                    ToolbarStyle-BorderWidth="1px"  />    
            </td> 
        </tr> 
    </table>
    <asp:LinkButton ID="linkRegresar" runat="server" CssClass="noPrint">Regresar</asp:LinkButton>
</asp:Content>

