﻿Imports Ext.Net

Public Class EditableGrid
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    <DirectMethod>
    Protected Sub AgregarCombo()
        Dim comboBoxTest As ComboBox = New ComboBox()
        Dim guidCombo As String = Guid.NewGuid().ToString()
        comboBoxTest.ID = "cmbTest" & guidCombo
        comboBoxTest.Items.Add(New Ext.Net.ListItem("Uno", 1))
        comboBoxTest.Items.Add(New Ext.Net.ListItem("Dos", 2))
        comboBoxTest.Items.Add(New Ext.Net.ListItem("Tres", 3))


        Panel1.Add(comboBoxTest)
        comboBoxTest.Render()
    End Sub

    <DirectMethod>
    Protected Sub AgregarRegistroVenta()

        Dim guidCombo As String = String.Empty
        Dim objeto As Object = Ext.Net.X.GetCmp("cmbTest" & guidCombo)
        Dim i As Integer = 0
        '    Dim lineaVenta As LineaVenta = New LineaVenta()
        '    lineaVenta.Id = Guid.NewGuid().ToString()
        '    lineaVenta.CodigoArticulo = "25-5-12-C"
        '    lineaVenta.NombreArticulo = "Articulo 12345678"
        '    lineaVenta.SujetoAImpuesto = True

        '    Dim auxUnidadMedida1 As UnidadMedida = New UnidadMedida()
        '    auxUnidadMedida1.UnidadMedida = "Kilo"
        '    auxUnidadMedida1.Factor = 1000
        '    Dim auxUnidadMedida2 As UnidadMedida = New UnidadMedida()
        '    auxUnidadMedida2.UnidadMedida = "Caja"
        '    auxUnidadMedida2.Factor = 10
        '    lineaVenta.UnidadDeMedida = auxUnidadMedida1
        '    lineaVenta.UnidadesDeMedida = New List(Of UnidadMedida)()
        '    lineaVenta.UnidadesDeMedida.Add(auxUnidadMedida1)
        '    lineaVenta.UnidadesDeMedida.Add(auxUnidadMedida2)

        '    lineaVenta.Cantidad = 2
        '    Dim auxFactor1 As Factor = New Factor()
        '    auxFactor1.Precio = 74.44
        '    auxFactor1.Nombre = "Factor 2"
        '    Dim auxFactor2 As Factor = New Factor()
        '    auxFactor2.Precio = 71.73
        '    auxFactor2.Nombre = "Factor 3"

        '    lineaVenta.Factor = auxFactor1
        '    lineaVenta.Factores = New List(Of Factor)()
        '    lineaVenta.Factores.Add(auxFactor1)
        '    lineaVenta.Factores.Add(auxFactor2)

        '    lineaVenta.Precio = 10
        '    lineaVenta.Descuento = 10
        '    lineaVenta.TotalLinea = 1   8
        '    lineaVenta.TotalLineaImpuestos = 20.88
        '    lineaVenta.PrecioMinimoAutorizacion = 4
        '    lineaVenta.PrecioMaximoAutorizacion = 13
        '    lineaVenta.PrecioCosto = 3
        '    lineaVenta.Comentarios = String.Empty

        '    AgregarRegistroPanel(lineaVenta)
        'End Sub

        'Private Sub AgregarRegistroPanel(ByVal informacionLineaVenta As LineaVenta)
        '    Dim idRegistro As String = Guid.NewGuid().ToString()

        '    Dim registroContainer As Ext.Net.Container = New Container()
        '    registroContainer.Layout = "HBoxLayout"
        '    registroContainer.Width = 750
        '    registroContainer.ID = "contenedor" & idRegistro

        '    Dim cajaCodigoArticulo As Ext.Net.TextField = New TextField()
        '    cajaCodigoArticulo.ID = "txtCodigoArticulo" & idRegistro
        '    cajaCodigoArticulo.Width = 80
        '    cajaCodigoArticulo.ReadOnly = True
        '    cajaCodigoArticulo.Value = informacionLineaVenta.CodigoArticulo

        '    Dim cajaNombreArticulo As Ext.Net.TextField = New TextField()
        '    cajaNombreArticulo.ID = "txtNombreArticulo" & idRegistro
        '    cajaNombreArticulo.Width = 140
        '    cajaNombreArticulo.ReadOnly = True
        '    cajaNombreArticulo.Value = informacionLineaVenta.NombreArticulo

        '    Dim comboUnidadMedida As Ext.Net.ComboBox = New ComboBox()
        '    comboUnidadMedida.ID = "cmbUnidadMedida" & idRegistro
        '    comboUnidadMedida.Width = 60
        '    comboUnidadMedida.Enabled = True
        '    For i As Integer = 0 To informacionLineaVenta.UnidadesDeMedida.Count - 1
        '        comboUnidadMedida.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.UnidadesDeMedida(i).UnidadMedida, informacionLineaVenta.UnidadesDeMedida(i).Factor))
        '    Next
        '    comboUnidadMedida.Value = informacionLineaVenta.UnidadDeMedida.Factor
        '    comboUnidadMedida.Listeners.Select.Handler = "MetodoDetalleCambioUnidad.ControlarCambioUnidadMedida('" & idRegistro & "')"

        '    Dim cajaCantidad As Ext.Net.NumberField = New NumberField()
        '    cajaCantidad.ID = "txtCantidad" & idRegistro
        '    cajaCantidad.Width = 70
        '    cajaCantidad.Enabled = True
        '    cajaCantidad.Value = informacionLineaVenta.Cantidad
        '    cajaCantidad.DecimalPrecision = 2
        '    cajaCantidad.HideTrigger = True
        '    cajaCantidad.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        '    Dim comboFactor As Ext.Net.ComboBox = New ComboBox()
        '    comboFactor.ID = "cmbFactor" & idRegistro
        '    comboFactor.Width = 80
        '    comboFactor.Enabled = True
        '    For i As Integer = 0 To informacionLineaVenta.Factores.Count - 1
        '        comboFactor.Items.Add(New Ext.Net.ListItem(informacionLineaVenta.Factores(i).Descripcion, informacionLineaVenta.Factores(i).Precio))
        '    Next
        '    comboFactor.Value = informacionLineaVenta.Factor.Precio
        '    comboFactor.Listeners.Select.Handler = "MetodoDetalleCambioFactor.ControlarCambioFactor('" & idRegistro & "')"
        '    If Not informacionLineaVenta.PuedeElegirFactor Then
        '        comboFactor.Enabled = False
        '        comboFactor.ReadOnly = True
        '    End If

        '    Dim hiddenFactores As Ext.Net.Hidden = New Hidden()
        '    hiddenFactores.ID = "hdnFactores" & idRegistro
        '    hiddenFactores.Value = informacionLineaVenta.Factores.Count

        '    Dim cajaPrecio As Ext.Net.NumberField = New NumberField()
        '    cajaPrecio.ID = "txtPrecio" & idRegistro
        '    cajaPrecio.Width = 70
        '    cajaPrecio.Enabled = True
        '    cajaPrecio.Value = informacionLineaVenta.Precio
        '    cajaPrecio.HideTrigger = True
        '    cajaPrecio.DecimalPrecision = 4
        '    cajaPrecio.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"
        '    If Not informacionLineaVenta.PuedeModificarPrecio Then
        '        cajaPrecio.Enabled = False
        '        cajaPrecio.ReadOnly = True
        '    End If

        '    Dim cajaDescuento As Ext.Net.NumberField = New NumberField()
        '    cajaDescuento.ID = "txtDescuento" & idRegistro
        '    cajaDescuento.Width = 70
        '    cajaDescuento.Value = informacionLineaVenta.Descuento
        '    cajaDescuento.Enabled = True
        '    cajaDescuento.HideTrigger = True
        '    cajaDescuento.DecimalPrecision = 2
        '    cajaDescuento.Listeners.Blur.Handler = "MetodoDetalleRecalcularTotales.RecalcularTotalesSeleccionLineaGrid('" & idRegistro & "')"

        '    Dim cajaTotalConImpuesto As Ext.Net.NumberField = New NumberField()
        '    cajaTotalConImpuesto.ID = "txtTotalLineaConImpuesto" & idRegistro
        '    cajaTotalConImpuesto.Width = 80
        '    cajaTotalConImpuesto.Value = informacionLineaVenta.TotalLineaImpuestos
        '    cajaTotalConImpuesto.DecimalPrecision = 2
        '    cajaTotalConImpuesto.HideTrigger = True

        '    Dim cajaComentarios As Ext.Net.TextField = New TextField()
        '    cajaComentarios.ID = "txtComentarios" & idRegistro
        '    cajaComentarios.Width = 100
        '    cajaComentarios.Value = informacionLineaVenta.Comentarios

        '    Dim hiddenSujetoAImpuesto As Ext.Net.Hidden = New Hidden()
        '    hiddenSujetoAImpuesto.ID = "hdnSujImp" & idRegistro
        '    hiddenSujetoAImpuesto.Value = informacionLineaVenta.SujetoAImpuesto

        '    Dim hiddenTotalLinea As Ext.Net.Hidden = New Hidden()
        '    hiddenTotalLinea.ID = "hdnTotalLinea" & idRegistro
        '    hiddenTotalLinea.Value = informacionLineaVenta.TotalLinea

        '    Dim hiddenPrecioMinimoAutorizacion As Ext.Net.Hidden = New Hidden()
        '    hiddenPrecioMinimoAutorizacion.ID = "hdnPrecioMinimoAutorizacion" & idRegistro
        '    hiddenPrecioMinimoAutorizacion.Value = informacionLineaVenta.PrecioMinimoAutorizacion

        '    Dim hiddenPrecioMaximoAutorizacion As Ext.Net.Hidden = New Hidden()
        '    hiddenPrecioMaximoAutorizacion.ID = "hdnPrecioMaximoAutorizacion" & idRegistro
        '    hiddenPrecioMaximoAutorizacion.Value = informacionLineaVenta.PrecioMaximoAutorizacion

        '    Dim hiddenPrecioCosto As Ext.Net.Hidden = New Hidden()
        '    hiddenPrecioCosto.ID = "hdnPrecioCosto" & idRegistro
        '    hiddenPrecioCosto.Value = informacionLineaVenta.PrecioCosto

        '    Dim hiddenUltimaCantidad As Ext.Net.Hidden = New Hidden()
        '    hiddenUltimaCantidad.ID = "hdnCantidad" & idRegistro
        '    hiddenUltimaCantidad.Value = informacionLineaVenta.Cantidad

        '    Dim hiddenPrecio As Ext.Net.Hidden = New Hidden()
        '    hiddenPrecio.ID = "hdnPrecio" & idRegistro
        '    hiddenPrecio.Value = informacionLineaVenta.Precio

        '    Dim hiddenDescuento As Ext.Net.Hidden = New Hidden()
        '    hiddenDescuento.ID = "hdnDescuento" & idRegistro
        '    hiddenDescuento.Value = informacionLineaVenta.Descuento

        '    registroContainer.Items.Add(cajaCodigoArticulo)
        '    registroContainer.Items.Add(cajaNombreArticulo)
        '    registroContainer.Items.Add(comboUnidadMedida)
        '    registroContainer.Items.Add(cajaCantidad)
        '    registroContainer.Items.Add(comboFactor)
        '    registroContainer.Items.Add(hiddenFactores)
        '    registroContainer.Items.Add(cajaPrecio)
        '    registroContainer.Items.Add(cajaDescuento)
        '    registroContainer.Items.Add(cajaTotalConImpuesto)
        '    registroContainer.Items.Add(cajaComentarios)
        '    registroContainer.Items.Add(hiddenSujetoAImpuesto)
        '    registroContainer.Items.Add(hiddenUltimaCantidad)
        '    registroContainer.Items.Add(hiddenPrecio)
        '    registroContainer.Items.Add(hiddenDescuento)
        '    registroContainer.Items.Add(hiddenTotalLinea)
        '    registroContainer.Items.Add(hiddenPrecioMinimoAutorizacion)
        '    registroContainer.Items.Add(hiddenPrecioMaximoAutorizacion)
        '    registroContainer.Items.Add(hiddenPrecioCosto)
        '    panelDetalleDocumento.Items.Add(registroContainer)
        '    registroContainer.Render()


    End Sub


End Class