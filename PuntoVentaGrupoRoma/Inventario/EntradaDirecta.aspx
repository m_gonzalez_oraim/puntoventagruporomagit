﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/admin.master" CodeBehind="EntradaDirecta.aspx.vb" Inherits="PuntoVentaGrupoRoma.EntradaDirecta" %>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >
    <script type="text/javascript">
        var addRecord = function (form, grid) {
            //            debugger;
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }

            if (form.getForm().getFieldValues(false, "dataIndex").Cantidad < 1) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "La cantidad debe ser mayor a cero",
                    title: "Error"
                });
                return false;
            }



            grid.store.insert(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();
            form.getForm().reset();
        };
    </script>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
    
        <ext:Store ID="StoreCmbPrice" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="Price" />
                        <ext:ModelField Name="PriceList" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreCmb" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="AdminStoreID" />
                        <ext:ModelField Name="StoreName" />
                        <ext:ModelField Name="WhsId" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
         <ext:Store ID="StoreArticulo" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="ItmsTypCod" />
                        <ext:ModelField Name="ItmsGrpNam" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreLinea" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="CODE" />
                        <ext:ModelField Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreMedida" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="CODE" />
                        <ext:ModelField Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreModelo" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:ModelField Name="CODE" />
                        <ext:ModelField Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store 
            ID="Store1" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"  
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None">
            <Model>
                <ext:Model runat="server" IDProperty="Id">
                    <Fields>
                        <ext:ModelField Name="Id" />
                        <ext:ModelField Name="Articulo" />
                        <ext:ModelField Name="Linea" />
                        <ext:ModelField Name="Medida" />
                        <ext:ModelField Name="Modelo" /> 
                        <ext:ModelField Name="ItemName" /> 
                        <ext:ModelField Name="Cantidad" />
                        <ext:ModelField Name="ArtName" /> 
                        <ext:ModelField Name="Comentarios" />
                        <ext:ModelField Name="Price" />
                    </Fields>
                </ext:Model>
            </Model> 
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store> 
        
        
        <table>
            <tr>
                <td>
                    <ext:FormPanel ID="FormPanel1" runat="server" 
                        Title="Ingrese los datos de cada modelo" Padding="5" ButtonAlign="Right" 
                        Width="400" LabelWidth="70" LabelAlign="Right" Icon="CartAdd">                            
                        <Items>
                         <ext:FieldSet ID="FieldSet1" runat="server" Title="Artículos" Collapsible="true" Layout="form">
                            <Items> 
                                <ext:ComboBox 
                                    ID="cmbArticulo" 
                                    runat="server" 
                                    Editable="false"
                                    FieldLabel="Articulo"
                                    EmptyText="Seleccione Artículo..."
                                    DataIndex="Articulo"
                                    AnchorHorizontal="100%"   
                                    StoreID="StoreArticulo" 
                                    Width="150" 
                                    DisplayField="ItmsGrpNam"
                                    ValueField="ItmsTypCod" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    SelectOnFocus="true" >                                    
                                    <ListConfig
                                        ItemSelector="div.list-item">

                                        <ItemTpl ID="Template2" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{ItmsGrpNam}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl>
                                      </ListConfig>      
                                        <DirectEvents>                                                
                                            <Select OnEvent="CambiaArticulo">
                                                <EventMask ShowMask="true" Msg="Obteniendo Lineas" MinDelay="500" />
                                            </Select>
                                        </DirectEvents>                   
                                </ext:ComboBox>
                                  
                                <ext:ComboBox
                                    ID="cmbLinea" 
                                    runat="server"
                                    FieldLabel="Linea"
                                    DataIndex="Linea"
                                    AnchorHorizontal="100%"
                                    StoreID="StoreLinea" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Línea..."
                                    
                                    SelectOnFocus="true" >                                    
                                    <ListConfig
                                        ItemSelector="div.list-item">
                                        <ItemTpl ID="Template3" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{NAME}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl>  
                                     </ListConfig>
                                        <DirectEvents>                        
                                            <Select OnEvent="CambiaLinea">
                                            <EventMask ShowMask="true" Msg="Obteniendo Medidas" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>                    
                                </ext:ComboBox>
                                    
                                <ext:ComboBox runat="server" 
                                        ID="cmbMedida" 
                                        DataIndex="Medida" 
                                        FieldLabel="Medida"                        
                                        Enabled="false"
                                        AnchorHorizontal="100%"
                                    StoreID="StoreMedida" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    QueryMode ="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Medida..."
                                    ItemSelector="div.list-item"
                                    SelectOnFocus="true" >                                    
                                    <ListConfig>
                                        <ItemTpl ID="Template4" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{NAME}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl> 
                                        </ListConfig>
                                         <DirectEvents>                        
                                            <Select OnEvent="CambiaMedida">
                                            <EventMask ShowMask="true" Msg="Obteniendo Modelos" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>
                                </ext:ComboBox>                    
                                   
                                <ext:ComboBox 
                                    runat="server" 
                                        ID="cmbModelo" 
                                        DataIndex="Modelo" 
                                        FieldLabel="Modelo"                        
                                        Enabled="false" 
                                        AnchorHorizontal="100%"                                       
                                    StoreID="StoreModelo" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Modelo..."
                                    
                                    SelectOnFocus="true" > 
                                    <ListConfig
                                        ItemSelector="div.list-item">
                                        <ItemTpl ID="Template5" runat="server">
                                            <Html>
					                            <div class="list-item">
							                            <h3>{NAME}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl> 
                                        </ListConfig>
                                         <DirectEvents>                        
                                            <Select OnEvent="CambiaModelo">
                                            <EventMask ShowMask="true" Msg="Verificando Selección" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>  
                                </ext:ComboBox> 
                               
                               <ext:Hidden DataIndex="ItemName" ID="ItemName" runat="server" />
                               <ext:Hidden DataIndex="ArtName"  ID="ArtName" runat="server" />

                               <ext:TextField ID="Cantidad" DataIndex="Cantidad" Text="1" Disabled="true" runat="server" FieldLabel="Cantidad" AnchorHorizontal="50%" CausesValidation="false" Maskre="/^[0-9]/"/> 
                               <ext:TextArea ID="Comentarios" DataIndex="Comentarios" runat="server" FieldLabel="Comentarios" AnchorHorizontal="80%" CausesValidation="true" Maskre="/^[- .a-zA-Z0-9]/" /> 
                               
                               <ext:ComboBox 
                               runat="server"  
                                        DataIndex="Price"  
                                AnchorHorizontal="100%"        
                                ID="cmbPrice"  
                                StoreID="StoreCmbPrice" 
                                Width="150" 
                                Editable="false"
                                Enabled="false" 
                                DisplayField="Price"
                                ValueField="Price" 
                                TypeAhead="true" 
                                Mode="Default"
                                ForceSelection="true"
                                TriggerAction="All" 
                                Text="Precio"                                 
                                FieldLabel="Precio"
                                EmptyText="Precio..."
                                ItemSelector="div.list-item"
                                SelectOnFocus="true">
                                   <ListConfig>
                                <ItemTpl ID="Template6" runat="server">
                                    <Html> 
						                    <div class="list-item">
							                     <h3>{Price}</h3>
							                     {PriceList} 
						                    </div>
				                    </Html>
                                </ItemTpl>  
                                       </ListConfig>
                                   <ToolTips>
                                        <ext:ToolTip ID="ToolTip3" runat="server" Html="Precio de Entrada" Title="Seleccione el precio de entrada para cada articulo" Target="#{cmbPrice}">
                                        </ext:ToolTip>
                                </ToolTips> 
                                <DirectEvents>                        
                                            <Select OnEvent="CambiaPrecio">
                                            <EventMask ShowMask="true" Msg="Validando Selección" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>                                
                                </ext:ComboBox>
                                                                 
                            </Items>
                            </ext:FieldSet>
                            
                         <ext:FieldSet ID="FieldSet2" 
                            runat="server"
                            Title="Tienda"
                            Collapsible="true"
                            Collapsed="true"
                            visible="false"
                            Layout="form">
                            <ToolTips>
                                    <ext:ToolTip ID="ToolTip2" runat="server" Html="Solo se hace una vez" Title="Seleccione la tienda al terminar de agregar todos los articulos" Target="#{FieldSet2}"></ext:ToolTip>
                                    </ToolTips>  
                            <Items>
                             
                            <ext:ComboBox 
                                ID="ComboBox1" 
                                runat="server"
                                StoreID="StoreCmb" 
                                Width="150" 
                                Editable="false"
                                DisplayField="StoreName"
                                ValueField="AdminStoreID" 
                                TypeAhead="true" 
                                Mode="Default"
                                ForceSelection="true"
                                TriggerAction="All" 
                                Text="Tiendas"                                 
                                FieldLabel="Tienda"
                                EmptyText="Tiendas..."
                                ItemSelector="div.list-item"
                                SelectOnFocus="true">
                                <ListConfig>
                                <ItemTpl ID="Template1" runat="server">
                                    <Html>
						                <div class="list-item">
							                    <h3>{StoreName}</h3>
							                    {WhsId} 
						                </div>
				                    </Html>
                                </ItemTpl>  
                                    </ListConfig>
                                   <ToolTips>
                                    <ext:ToolTip ID="ToolTip1" runat="server" Html="Solo se hace una vez" Title="Seleccione la tienda al terminar de agregar todos los articulos" Target="#{ComboBox1}"></ext:ToolTip>
                                    </ToolTips>                               
                                </ext:ComboBox>
                            </Items>
                         </ext:FieldSet>
                            </Items>
                            <Listeners>
                                <ValidityChange  Handler="#{LoadButton}.setDisabled(!valid);" />
                            </Listeners>
                            <Buttons>
                                <ext:Button ID="btnAgregar" runat="server" Text="Agregar" Disabled="true" Icon="Add">
                                    <Listeners>
                                     <Click Handler="addRecord(#{FormPanel1}, #{GridPanel1});" /> 
                                    </Listeners> 
                                    <DirectEvents>
                                            <Click OnEvent="DisableClick"></Click>  
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnLimpiarCampos" runat="server" Text="Limpiar Campos" Icon="Erase">
                                    <Listeners>
                                        <Click Handler="#{FormPanel1}.getForm().reset();" />
                                    </Listeners>
                                </ext:Button>             
                            </Buttons>
                      </ext:FormPanel> 
                             
                </td>
                <td>   
                        <ext:GridPanel 
                            ID="GridPanel1" 
                            runat="server" 
                            StoreID="Store1" 
                            StripeRows="true" 
                            Height="500"
                            Width="950"
                            TrackOver="true" 
                            AutoExpandColumn="Modelo"
                            Title="Modelos para entrada Directa" 
                            Collapsed="false"
                            Collapsible="true" 
                            Layout="fit"
                            AutoHeight="true"  
                            Icon="Cart">
                        <%--<LoadMask ShowMask="true" />--%>
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column runat="server" Text="IdArtículo" Width="160" DataIndex="Articulo" Hidden="true" />
                                    <ext:Column runat="server" Text="Artículo" Width="160" DataIndex="ArtName" />
                                    <ext:Column runat="server" Text="Línea" Width="160" DataIndex="Linea"/>
                                    <ext:Column runat="server" Text="Medida" Width="60" DataIndex="Medida"/>
                                    <ext:Column runat="server" Text="ItemCode" Width="175" DataIndex="Modelo" Hidden="true"/>
                                    <ext:Column runat="server" Text="Modelo" Width="300" DataIndex="ItemName"/>
                                    <ext:Column runat="server" Text="Cantidad" Width="70" DataIndex="Cantidad" Align="Right"> 
                                        <%--<Editor>
                                            <ext:TextArea ID="TextArea1" Height="19" runat="server" Maskre="/^[0-9]/">
                                            </ext:TextArea>
                                        </Editor>--%>
                                    </ext:Column>  
                                    <ext:Column runat="server" Text="Comentarios" Width="100" DataIndex="Comentarios"/>
                                    <ext:Column runat="server" Text="Precio" Width="100" DataIndex="Price"/>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" >
                                    <Listeners>
                                        <Select Handler="#{FormPanel1}.getForm().loadRecord(record);" />
                                    </Listeners>
                                </ext:RowSelectionModel>
                            </SelectionModel>  
                            <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                <ext:Button ID="btnSave" runat="server" Text="Crear Entrada" Icon="Disk" Type="Submit">
                                        <DirectEvents>
                                            <Click 
                                                OnEvent="UploadClick"
                                                Before="Ext.Msg.wait('Procesando Entrada...', 'Cargando...');"
                                                    
                                                Failure="Ext.Msg.show({ 
                                                    title   : 'Error', 
                                                    msg     : 'Error durante la carga...', 
                                                    minWidth: 200, 
                                                    modal   : true, 
                                                    icon    : Ext.Msg.ERROR, 
                                                    buttons : Ext.Msg.OK 
                                                });">
                                        <ExtraParams>
                                            <ext:Parameter Name="Grid1" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))" Mode="Raw" /> 
                                        </ExtraParams>                                                    
                                                
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>  
                                    <ext:Button ID="LimpiarGrid" runat="server" Text="Limpiar" Icon="Erase">
                                        <Listeners>
                                            <Click Handler="#{GridPanel1}.deleteSelected(); #{GridPanel1}.store.load(#{GridPanel1}.store.lastOptions);" />
                                        </Listeners>
                                    </ext:Button>   
                                    <ext:Button ID="btnBorrarSel" runat="server" Text="Borrar Selección" Icon="Erase">
                                        <Listeners>
                                            <Click Handler="#{GridPanel1}.deleteSelected();" />
                                        </Listeners>
                                    </ext:Button>  
                                </Items>
                                </ext:Toolbar>
                            </TopBar>  
                               <%--<BottomBar>
                                <ext:PagingToolbar ID="PagingToolBar3" runat="server" PageSize="6" />
                            </BottomBar>  --%>                         
                        </ext:GridPanel>                                
                </td>
            </tr>
            
        </table>
        
</asp:Content>