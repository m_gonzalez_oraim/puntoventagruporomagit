﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="SalidaDirecta.aspx.vb" Inherits="PuntoVentaGrupoRoma.SalidaDirecta" MasterPageFile="~/admin.master"%>
<%@ Register Assembly="Ext.Net" Namespace="Ext.Net" TagPrefix="ext" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server" >

    <script type="text/javascript">
        var addRecord = function (form, grid) {
            
            if (!form.getForm().isValid()) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "Form is invalid",
                    title: "Error"
                });
                return false;
            }

            var Cant = form.getForm().getFieldValues(false, "dataIndex").Cantidad;
            var Stock = form.getForm().getFieldValues(false, "dataIndex").Stock;
            Cant = Cant * 1
            Stock = Stock * 1

            if (Stock < 1) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "No cuenta con Stock",
                    title: "Error"
                });
                return false;
            }

            if (Cant < 1) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "La cantidad debe ser mayor a cero",
                    title: "Error"
                });
                return false;
            }

            if (Cant > Stock) {
                Ext.net.Notification.show({
                    iconCls: "icon-exclamation",
                    html: "La cantidad Excede el Stock Cant: " + Cant + " Stock: " + Stock,
                    title: "Error"
                });
                return false;
            }


            grid.store.insert(0, form.getForm().getFieldValues(false, "dataIndex"));
            grid.getView().refresh();
            form.getForm().reset();
        };
    </script>
    <ext:ResourceManager ID="ResourceManager1" runat="server" />
        <ext:Store ID="StoreCmb" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:Modelfield Name="AdminStoreID" />
                        <ext:Modelfield Name="StoreName" />
                        <ext:Modelfield Name="WhsId" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
       <ext:Store ID="StoreArticulo" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:Modelfield Name="ItmsTypCod" />
                        <ext:Modelfield Name="ItmsGrpNam" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreLinea" runat="server">
            <Model>
                <ext:Model runat="server">
                    <Fields>
                        <ext:Modelfield Name="CODE" />
                        <ext:Modelfield Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreMedida" runat="server">
            <Model>
                <ext:Model runat="server" >
                    <Fields>
                        <ext:Modelfield Name="CODE" />
                        <ext:Modelfield Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store ID="StoreModelo" runat="server">
            <Model>
                <ext:Model runat="server" >
                    <Fields>
                        <ext:Modelfield Name="CODE" />
                        <ext:Modelfield Name="NAME" /> 
                    </Fields>
                </ext:Model>
            </Model>            
        </ext:Store>
        
        <ext:Store 
            ID="Store1" 
            runat="server" 
            AutoSave="true" 
            ShowWarningOnFailure="false"  
            SkipIdForNewRecords="false"
            RefreshAfterSaving="None" >
            <Model>
                <ext:Model IDProperty="Id" runat="server" >
                    <Fields>
                        <ext:Modelfield Name="Id" />
                        <ext:Modelfield Name="Articulo"  />
                        <ext:Modelfield Name="Linea" />
                        <ext:Modelfield Name="Medida" />
                        <ext:Modelfield Name="Modelo" /> 
                        <ext:Modelfield Name="ItemName" /> 
                        <ext:Modelfield Name="Cantidad" />
                        <ext:Modelfield Name="ArtName" /> 
                        <ext:Modelfield Name="Comentarios" />
                    </Fields>
                </ext:Model>
            </Model> 
            <Listeners>
                <Exception Handler="
                    Ext.net.Notification.show({
                        iconCls    : 'icon-exclamation', 
                        html       : e.message, 
                        title      : 'EXCEPTION', 
                        autoScroll : true, 
                        hideDelay  : 5000, 
                        width      : 300, 
                        height     : 200
                    });" />
            </Listeners>
        </ext:Store> 
        
        
          <table>
            <tr>
               <td>
                    <ext:FormPanel ID="FormPanel1" runat="server" 
                        Title="Ingrese los datos de cada modelo" Padding="5" ButtonAlign="Right" 
                        Width="400" LabelWidth="70" LabelAlign="Right" Icon="CartAdd">                            
                        <Items>
                         <ext:FieldSet ID="FieldSet1" runat="server" Title="Artículos" Collapsible="true" Layout="form">
                            <Items> 
                                <ext:ComboBox 
                                    ID="cmbArticulo" 
                                    runat="server" 
                                    FieldLabel="Articulo"
                                    DataIndex="Articulo"
                                    AnchorHorizontal="100%"   
                                    StoreID="StoreArticulo" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="ItmsGrpNam"
                                    ValueField="ItmsTypCod" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Artículo..."
                                    ItemSelector="div.list-item"
                                    SelectOnFocus="true" >
                                    <ListConfig>                              
                                        <ItemTpl ID="Template2" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{ItmsGrpNam}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl>  
                                    </ListConfig>
                                        <DirectEvents>                                                
                                            <Select OnEvent="CambiaArticulo">
                                                <EventMask ShowMask="true" Msg="Obteniendo Lineas" MinDelay="500" />
                                            </Select>
                                        </DirectEvents>                    
                                </ext:ComboBox>
                                  
                                <ext:ComboBox
                                    ID="cmbLinea" 
                                    runat="server"
                                    FieldLabel="Linea"
                                    DataIndex="Linea"
                                    AnchorHorizontal="100%"
                                    StoreID="StoreLinea" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Línea..."
                                    ItemSelector="div.list-item"
                                    SelectOnFocus="true" >    
                                    <ListConfig>                                
                                        <ItemTpl ID="Template3" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{NAME}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl>  
                                    </ListConfig>
                                        <DirectEvents>                        
                                            <Select OnEvent="CambiaLinea">
                                            <EventMask ShowMask="true" Msg="Obteniendo Medidas" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>                    
                                </ext:ComboBox>
                                    
                                <ext:ComboBox runat="server" 
                                        ID="cmbMedida" 
                                        DataIndex="Medida" 
                                        FieldLabel="Medida"                        
                                        Enabled="false"
                                        AnchorHorizontal="100%"
                                    StoreID="StoreMedida" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Medida..."
                                    ItemSelector="div.list-item"
                                    SelectOnFocus="true" >  
                                    <ListConfig>                                  
                                        <ItemTpl ID="Template4" runat="server">
                                            <Html>
						                        <div class="list-item">
							                            <h3>{NAME}</h3> 
						                        </div>
				                            </Html>
                                        </ItemTpl> 
                                    </ListConfig>
                                         <DirectEvents>                        
                                            <Select OnEvent="CambiaMedida">
                                            <EventMask ShowMask="true" Msg="Obteniendo Modelos" MinDelay="500" />
                                        </Select>
                                        </DirectEvents>
                                </ext:ComboBox>                    
                                   
                                <ext:ComboBox 
                                    runat="server" 
                                        ID="cmbModelo" 
                                        DataIndex="Modelo" 
                                        FieldLabel="Modelo"                        
                                        Enabled="false" 
                                        AnchorHorizontal="100%"                                       
                                    StoreID="StoreModelo" 
                                    Width="150" 
                                    Editable="false"
                                    DisplayField="NAME"
                                    ValueField="CODE" 
                                    TypeAhead="true" 
                                    Mode="Default"
                                    ForceSelection="true"
                                    TriggerAction="All"                     
                                    EmptyText="Seleccione Modelo..."
                                    ItemSelector="div.list-item"
                                    SelectOnFocus="true" > 
                                    <ListConfig>   
                                         <ItemTpl ID="Template5" runat="server">
                                                <Html>
						                            <div class="list-item">
							                                <h3>{NAME}</h3> 
						                            </div>
				                                </Html>
                                            </ItemTpl> 
                                        </ListConfig>
                                         <DirectEvents>
                                            <Select OnEvent="SelectModel" >
                                                    <ExtraParams>
                                                        <ext:Parameter Name="Grid1" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))" Mode="Raw" /> 
                                                    </ExtraParams>                                                    
                                                
                                            </Select>
                                        </DirectEvents> 
                                </ext:ComboBox> 
                               <ext:Hidden DataIndex="ItemName" ID="ItemName" runat="server" />
                               <ext:Hidden DataIndex="ArtName"  ID="ArtName" runat="server" />
                                        <ext:TextField ID="Stock" DataIndex="Stock" Text="0" FieldLabel="Stock" Disabled="true" runat="server" 
                                        width="60px" CausesValidation="false" Maskre="/^[0-9]/" /> 
                                                                                    
                                        <ext:Label runat="server"  
                                    Text="Escriba la cantidad que desea restar al inventario:" ID="inv1"></ext:Label>
                                        
                                        <ext:TextField ID="Cantidad" DataIndex="Cantidad" Text="1" FieldLabel="Cantidad" Disabled="true" runat="server" 
                                            width="60px" CausesValidation="false" Maskre="/^[0-9]/" />     
                                        
                                        <ext:TextArea ID="Comentarios" DataIndex="Comentarios" runat="server" FieldLabel="Comentarios" AnchorHorizontal="80%" CausesValidation="true" Maskre="/^[- .a-zA-Z0-9]/" />                                                                                    
                            </Items>
                            </ext:FieldSet>
                         <ext:FieldSet ID="FieldSet2" 
                            runat="server"
                            Title="Tienda"
                            Collapsible="true"
                            Collapsed="true"
                            visible="false"
                            Layout="form">
                            <ToolTips>
                                    <ext:ToolTip ID="ToolTip2" runat="server" Html="Solo se hace una vez" Title="Seleccione la tienda al terminar de agregar todos los articulos" Target="#{FieldSet2}"></ext:ToolTip>
                                    </ToolTips>  
                            <Items>
                             
                            <ext:ComboBox 
                                ID="ComboBox1" 
                                runat="server"
                                StoreID="StoreCmb" 
                                Width="150" 
                                Editable="false"
                                DisplayField="StoreName"
                                ValueField="AdminStoreID" 
                                TypeAhead="true" 
                                Mode="Default"
                                ForceSelection="true"
                                TriggerAction="All" 
                                Title="Tiendas"                                 
                                FieldLabel="Tienda"
                                EmptyText="Tiendas..."
                                ItemSelector="div.list-item"
                                SelectOnFocus="true">
                                <ListConfig>
                                    <ItemTpl ID="Template1" runat="server">
                                        <Html>
						                    <div class="list-item">
							                     <h3>{StoreName}</h3>
							                     {WhsId} 
						                    </div>
				                        </Html>
                                    </ItemTpl> 
                                 </ListConfig> 
                                 <ToolTips>
                                    <ext:ToolTip ID="ToolTip1" runat="server" Html="Solo se hace una vez" Title="Seleccione la tienda al terminar de agregar todos los articulos" Target="#{ComboBox1}"></ext:ToolTip>
                                    </ToolTips>                               
                                </ext:ComboBox>
                            </Items>
                         </ext:FieldSet>
                            </Items>
                            <Listeners>
                                <ValidityChange  Handler="#{LoadButton}.setDisabled(!valid);" />
                            </Listeners>
                            <Buttons>
                                <ext:Button ID="btnAgregar" runat="server" Text="Agregar" Disabled="true" Icon="Add">
                                    <Listeners>
                                     <Click Handler="addRecord(#{FormPanel1}, #{GridPanel1});" /> 
                                    </Listeners> 
                                    <DirectEvents>
                                            <Click OnEvent="DisableClick"></Click>  
                                    </DirectEvents>
                                </ext:Button>
                                <ext:Button ID="btnLimpiarCampos" runat="server" Text="Limpiar Campos" Icon="Erase">
                                    <Listeners>
                                        <Click Handler="#{FormPanel1}.getForm().reset();" />
                                    </Listeners>
                                </ext:Button>             
                            </Buttons>
                      </ext:FormPanel> 
                             
                </td>
                <td>   
                                              <ext:GridPanel 
                            ID="GridPanel1" 
                            runat="server" 
                            StoreID="Store1" 
                            StripeRows="true" 
                            Height="500"
                            Width="850"
                            TrackMouseOver="true" 
                            AutoExpandColumn="Modelo"
                            Title="Modelos para salida Directa" 
                            Collapsed="false"
                            Collapsible="true" 
                            Layout="fit"  
                            AutoHeight="true"  
                            Icon="Cart">
                            
          
                            <ColumnModel ID="ColumnModel2" runat="server">
                                <Columns>
                                    <ext:Column runat="server"  Header="IdArtículo" Width="160" DataIndex="Articulo" Hidden="true" AllowBlank="false"/>
                                    <ext:Column runat="server" Header="Artículo" Width="160" DataIndex="ArtName" AllowBlank="false"/>
                                    <ext:Column runat="server" Header="Línea" Width="160" DataIndex="Linea" AllowBlank="false"> 
                                    </ext:Column>
                                    <ext:Column runat="server" Header="Medida" Width="60" DataIndex="Medida" AllowBlank="false"> 
                                    </ext:Column>
                                    <ext:Column  runat="server" Header="ItemCode" Width="175" DataIndex="Modelo" Hidden="true" AllowBlank="false"> 
                                    </ext:Column> 
                                    <ext:Column runat="server" Header="Modelo" Width="300" DataIndex="ItemName" AllowBlank="false"> 
                                    </ext:Column>  
                                    <ext:Column runat="server" Header="Cantidad" Width="70" DataIndex="Cantidad" Align="Right" AllowBlank="false"> 
                                    <Editor><ext:TextArea ID="TextArea1" Height="19" runat="server" Maskre="/^[0-9]/"></ext:TextArea></Editor>
                                    </ext:Column>   
                                    <ext:Column runat="server" Header="Comentarios" Width="100" DataIndex="Comentarios" AllowBlank="false"/>
                                </Columns>
                            </ColumnModel>
                            <SelectionModel>
                                <ext:RowSelectionModel ID="RowSelectionModel3" runat="server" >
                                    <Listeners>
                                        <Select Handler="#{FormPanel1}.getForm().loadRecord(record);" />
                                    </Listeners>
                                </ext:RowSelectionModel>
                            </SelectionModel>  
                            <TopBar>
                                <ext:Toolbar ID="Toolbar1" runat="server">
                                <Items>
                                <ext:Button ID="btnSave" runat="server" Text="Crear Salida" Icon="Disk" Type="Submit">
                                        <DirectEvents>
                                            <Click 
                                                OnEvent="UploadClick"
                                                Before="Ext.Msg.wait('Procesando Salida...', 'Cargando...');"
                                                    
                                                Failure="Ext.Msg.show({ 
                                                    title   : 'Error', 
                                                    msg     : 'Error durante la carga...', 
                                                    minWidth: 200, 
                                                    modal   : true, 
                                                    icon    : Ext.Msg.ERROR, 
                                                    buttons : Ext.Msg.OK 
                                                });">
                                                    <ExtraParams>
                                                        <ext:Parameter Name="Grid1" Value="Ext.encode(#{GridPanel1}.getRowsValues({selectedOnly : false}))" Mode="Raw" /> 
                                                    </ExtraParams>                                                    
                                                
                                            </Click>
                                        </DirectEvents>
                                    </ext:Button>  
                                    <ext:Button ID="LimpiarGrid" runat="server" Text="Limpiar" Icon="Erase">
                                        <Listeners>
                                            <Click Handler="#{GridPanel1}.deleteSelected(); #{GridPanel1}.store.load(#{GridPanel1}.store.lastOptions);" />
                                        </Listeners>
                                    </ext:Button>   
                                    <ext:Button ID="btnBorrarSel" runat="server" Text="Borrar Selección" Icon="Erase">
                                        <Listeners>
                                            <Click Handler="#{GridPanel1}.deleteSelected();" />
                                        </Listeners>
                                    </ext:Button>  
                                </Items>
                                </ext:Toolbar>
                            </TopBar>                        
                        </ext:GridPanel>          
                </td>
            </tr>
        </table>
</asp:Content>