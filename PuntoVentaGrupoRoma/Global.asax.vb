﻿' Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
' visite http://go.microsoft.com/?LinkId=9394802
Imports System.Web.Http
Imports System.Web.Optimization
Imports System.Security.Principal

Public Class MvcApplication
    Inherits System.Web.HttpApplication

    Sub Application_Start()
        AreaRegistration.RegisterAllAreas()

        WebApiConfig.Register(GlobalConfiguration.Configuration)
        FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters)
        RouteConfig.RegisterRoutes(RouteTable.Routes)
        'BundleConfig.RegisterBundles(BundleTable.Bundles)
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs) Handles Me.AuthenticateRequest
        Dim cookieName As String = "PuntoVentaBXP"
        Dim authCookie As HttpCookie = Context.Request.Cookies(cookieName)

        If authCookie Is Nothing Then
            Return
        End If

        Dim usuarioIdentity As UsuarioPuntoVentaIdentity = UsuarioPuntoVentaIdentity.Deserialize(authCookie.Value)
        Dim userIdentity As GenericIdentity = New GenericIdentity(authCookie.Name)
        Dim userPrincipal As PuntoVentaPrincipal = New PuntoVentaPrincipal(userIdentity, usuarioIdentity)
        Context.User = userPrincipal


    End Sub




End Class
