﻿Public Class CadenaConexionSQL
    Public Shared Function BaseDeDatosDeCadenaConexion(ByVal cadenaConexion As String) As String
        Dim elementosConexion() As String = cadenaConexion.Split(";")
        For i As Integer = 0 To elementosConexion.Length - 1
            Dim valoresElemento() As String = elementosConexion(i).Split("=")
            If valoresElemento(0) = "Initial Catalog" Then
                Return valoresElemento(1)
            End If
        Next
        Return Nothing
    End Function
End Class
