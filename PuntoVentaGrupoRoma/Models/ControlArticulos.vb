﻿Imports NLog
Imports Ext.Net
Public Class ControlArticulos

    Shared logger As NLog.Logger = LogManager.GetCurrentClassLogger()

    Private oDb As DBMaster = New DBMaster()
    Private connStringWebPOS As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString
    Private connStringWebSAP As String = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

    Public Function CargarArticulosAsignarFactores(ByVal storeId As Integer, ByVal articulo1 As String, ByVal articulo2 As String) As System.Data.DataTable
        Dim queryBase As String = "SELECT OITM.ItemCode AS CodigoArticulo,OITM.ItemName AS NombreArticulo,ISNULL(Factores.Factor,'') AS FactorActual,ISNULL(Factores.Factor,'') AS FactorNuevo FROM OITM LEFT JOIN {0}.dbo.AdminStoreFactores AS Factores ON Factores.IDArticulo=OITM.ItemCode AND Factores.AdminStoreId={1} WHERE OITM.FrozenFor='N' AND {2}"
        Dim condicion As String = String.Empty
        If articulo1 = String.Empty Then
            condicion = String.Format("ItemCode='{0}'", articulo2)
        ElseIf articulo2 = String.Empty Then
            condicion = String.Format("ItemCode='{0}'", articulo1)
        Else
            condicion = String.Format("ItemCode>='{0}' AND ItemCode<='{1}'", articulo1, articulo2)
        End If

        Dim baseDatosPOS As String = CadenaConexionSQL.BaseDeDatosDeCadenaConexion(connStringWebPOS)
        Dim queryBusqueda As String = String.Format(queryBase, baseDatosPOS, storeId, condicion)

        Dim tablaArticulosFactores As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryBusqueda, CommandType.Text, "Articulos", connStringWebSAP)
        Return tablaArticulosFactores
    End Function

    Public Function BuscarArticuloAjax(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal sortDir As String,
                                       ByVal valorBusqueda As String, ByVal tipoBusqueda As String, ByVal codigoAlmacenPOS As Integer, ByVal almacenSAPTienda As String) As Paging(Of Articulo)
        If tipoBusqueda = "Venta" Then
            Dim articulos As List(Of Articulo) = sqlBuscarArticuloAjax(start, limit, sort, sortDir, valorBusqueda, True, False, False, codigoAlmacenPOS, almacenSAPTienda)
            Return New Paging(Of Articulo)(articulos, articulos.Count)

        ElseIf tipoBusqueda = "Inventario" Then
            Dim articulos As List(Of Articulo) = sqlBuscarArticuloAjax(start, limit, sort, sortDir, valorBusqueda, True, False, False, codigoAlmacenPOS, almacenSAPTienda)
            Return New Paging(Of Articulo)(articulos, articulos.Count)
        End If
        Return Nothing
    End Function

    Public Function GuardarDefinicionFactores(ByVal definicionFactores As List(Of ArticuloFactor), ByVal idTienda As Integer) As Boolean
        Dim queryGuardadoFactores As StringBuilder = New StringBuilder()
        For i As Integer = 0 To definicionFactores.Count - 1
            queryGuardadoFactores.AppendLine(String.Format("DELETE FROM AdminStoreFactores WHERE AdminStoreID={0} AND IDArticulo='{1}'", idTienda, definicionFactores(i).CodigoArticulo))
            queryGuardadoFactores.AppendLine(String.Format("INSERT INTO AdminStoreFactores(AdminStoreId,Factor,IdArticulo) VALUES({0},'{1}','{2}')", idTienda, definicionFactores(i).FactorNuevo, definicionFactores(i).CodigoArticulo))
        Next
        Dim codigoEjecucion As Integer = oDb.EjecutaQry(queryGuardadoFactores.ToString(), CommandType.Text, connStringWebPOS)
        If codigoEjecucion <> 1 Then
            Return False
        End If
        Return True
    End Function

    Private Function sqlBuscarArticuloAjax(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal sortDir As String,
                                           ByVal valorBusqueda As String, ByVal articuloVenta As Boolean, ByVal articuloCompra As Boolean,
                                           ByVal articuloInventario As Boolean, ByVal codigoAlmacenPOS As Integer, ByVal almacenSAPTienda As String) As List(Of Articulo)
        Dim listadoArticulos As List(Of Articulo) = New List(Of Articulo)()

        Dim querySAP As String = "SELECT OITM.ItemCode,OITM.ItemName,(SELECT OnHand FROM OITW WHERE OITW.ItemCode=OITM.ItemCode AND OITW.WhsCode='{0}') FROM OITM " & _
                                 "INNER JOIN " & _
                                 "(" & _
                                 "  SELECT OITM.ItemCode,ROW_NUMBER() OVER ({1}) AS RowNum" & _
                                 "  FROM OITM" & _
                                 " WHERE FrozenFor='N' {2} " & _
                                ") aux ON aux.ItemCode=OITM.ItemCode " & _
                                " WHERE aux.RowNum BETWEEN {3} AND {4} " & _
                                " {1}"

        Dim condicionArticuloSAP As StringBuilder = New StringBuilder()
        If articuloVenta Then
            condicionArticuloSAP.Append(" AND SellItem='Y'")
        End If
        If articuloCompra Then
            condicionArticuloSAP.Append(" AND InvntItem='Y'")
        End If
        If articuloInventario Then
            condicionArticuloSAP.Append(" AND PurchaseItem='Y'")
        End If

        If valorBusqueda <> String.Empty Then
            condicionArticuloSAP.Append(" AND ItemCode LIKE '" & valorBusqueda & "%' OR ItemName LIKE '%" & valorBusqueda & "%'")
        End If

        Dim definicionOrdenamientoSQL As StringBuilder = New StringBuilder()
        If sort = String.Empty Then
            definicionOrdenamientoSQL.Append("ORDER BY ItemName " & sortDir)
        ElseIf sort = "Codigo" Then
            definicionOrdenamientoSQL.Append(" ORDER BY ItemCode " & sortDir)
        ElseIf sort = "Nombre" Then
            definicionOrdenamientoSQL.Append(" ORDER BY ItemName " & sortDir)
        Else
            definicionOrdenamientoSQL.Append(" ORDER BY OnHand" & sortDir)
        End If

        Dim queryEjecucionSAP As String = String.Format(querySAP, almacenSAPTienda, definicionOrdenamientoSQL.ToString(), condicionArticuloSAP.ToString(), start, limit)

        Dim conexionSAP As System.Data.Common.DbConnection = Nothing
        Dim commandSAP As System.Data.Common.DbCommand = Nothing
        Dim readerSAP As System.Data.Common.DbDataReader = Nothing
        Dim conexionPOS As System.Data.Common.DbConnection = Nothing

        Try
            conexionSAP = New System.Data.SqlClient.SqlConnection()
            conexionSAP.ConnectionString = connStringWebSAP
            conexionSAP.Open()
            commandSAP = conexionSAP.CreateCommand()
            commandSAP.CommandText = queryEjecucionSAP
            commandSAP.CommandType = CommandType.Text

            conexionPOS = New System.Data.SqlClient.SqlConnection()
            conexionPOS.ConnectionString = connStringWebPOS
            conexionPOS.Open()

            readerSAP = commandSAP.ExecuteReader()
            Dim articuloEncontrado As Articulo
            Dim existenciaEnPOS As Decimal
            While readerSAP.Read()
                articuloEncontrado = New Articulo()
                articuloEncontrado.Codigo = readerSAP.GetString(0)
                articuloEncontrado.Nombre = readerSAP.GetString(1)
                existenciaEnPOS = ObtenerExistenciaPOS(articuloEncontrado.Codigo, codigoAlmacenPOS, conexionPOS)
                If existenciaEnPOS <> -1 Then
                    articuloEncontrado.Existencia = existenciaEnPOS
                Else
                    articuloEncontrado.Existencia = readerSAP.GetDecimal(2)
                End If

                listadoArticulos.Add(articuloEncontrado)
            End While
            Return listadoArticulos
        Catch ex As Exception
            logger.ErrorException("buscarArticulo", ex)
            Return Nothing
        Finally
            If Not conexionPOS Is Nothing Then
                Try
                    If conexionPOS.State = ConnectionState.Open Then
                        conexionPOS.Close()
                    End If
                Catch
                End Try
                Try
                    conexionPOS.Dispose()
                Catch
                End Try
            End If

            If Not readerSAP Is Nothing Then
                Try
                    readerSAP.Close()
                Catch
                End Try
            End If

            If Not commandSAP Is Nothing Then
                Try
                    commandSAP.Dispose()
                Catch
                End Try
            End If

            If Not conexionSAP Is Nothing Then
                Try
                    If conexionSAP.State = ConnectionState.Open Then
                        conexionSAP.Close()
                    End If
                Catch
                End Try
                Try
                    conexionSAP.Dispose()
                Catch
                End Try
            End If
        End Try
    End Function

    Public Function BuscarArticulo(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal sortDir As String,
                                   ByVal codigoBusqueda As String, ByVal nombreBusqueda As String, ByVal tipoBusqueda As String, ByVal codigoAlmacenPOS As Integer, ByVal almacenSAPTienda As String, ByVal obtenTotalArticulos As Boolean, ByRef totalArticulos As Integer) As List(Of Articulo)
        totalArticulos = 0
        If tipoBusqueda = "Venta" Then
            Return sqlBuscarArticulo(start, limit, sort, sortDir, codigoBusqueda, nombreBusqueda, True, False, False, codigoAlmacenPOS, almacenSAPTienda, obtenTotalArticulos, totalArticulos)
        End If
        ''''''''''''''''''''''''''''''''''''''''''''''''ojo''''''''''''''''
        If tipoBusqueda = "Inventario" Then
            Return sqlBuscarArticulo(start, limit, sort, sortDir, codigoBusqueda, nombreBusqueda, True, False, False, codigoAlmacenPOS, almacenSAPTienda, obtenTotalArticulos, totalArticulos)
        End If
        Return Nothing
    End Function

    Private Function sqlBuscarArticulo(ByVal start As Integer, ByVal limit As Integer, ByVal sort As String, ByVal sortDir As String, ByVal codigoBusqueda As String, ByVal nombreBusqueda As String, ByVal articuloVenta As Boolean, ByVal articuloCompra As Boolean, ByVal articuloInventario As Boolean, ByVal codigoAlmacenPOS As Integer, ByVal almacenSAPTienda As String, ByVal obtenerTotalArticulos As Boolean, ByRef totalArticulos As Integer) As List(Of Articulo)
        Dim listadoArticulos As List(Of Articulo) = New List(Of Articulo)()

        Dim queryTotalRegistrosSAP As String = "SELECT COUNT(*) FROM OITM WHERE FrozenFor='N' {0}"

        Dim querySAP As String = "SELECT OITM.ItemCode,OITM.ItemName,(SELECT OnHand FROM OITW WHERE OITW.ItemCode=OITM.ItemCode AND OITW.WhsCode='{0}') FROM OITM " & _
        "INNER JOIN " & _
        "(" & _
        "  SELECT OITM.ItemCode,ROW_NUMBER() OVER ({1}) AS RowNum" & _
        "  FROM OITM" & _
        " WHERE FrozenFor='N' {2} " & _
        ") aux ON aux.ItemCode=OITM.ItemCode " & _
        " WHERE aux.RowNum BETWEEN {3} AND {4} " & _
        " {1}"

        Dim condicionArticuloSAP As StringBuilder = New StringBuilder()
        If articuloVenta Then
            condicionArticuloSAP.Append(" AND SellItem='Y'")
        End If
        If articuloCompra Then
            condicionArticuloSAP.Append(" AND InvntItem='Y'")
        End If
        If articuloInventario Then
            condicionArticuloSAP.Append(" AND PurchaseItem='Y")
        End If
        If codigoBusqueda <> String.Empty Then
            condicionArticuloSAP.Append(" AND ItemCode LIKE '%" & codigoBusqueda & "%'")
        End If
        If nombreBusqueda <> String.Empty Then
            condicionArticuloSAP.Append(" AND ItemName LIKE '%" & nombreBusqueda & "%'")
        End If

        Dim definicionOrdenamientoSQL As StringBuilder = New StringBuilder()
        If sort = String.Empty Then
            definicionOrdenamientoSQL.Append("ORDER BY ItemName " & sortDir)
        ElseIf sort = "Codigo" Then
            definicionOrdenamientoSQL.Append(" ORDER BY ItemCode " & sortDir)
        ElseIf sort = "Nombre" Then
            definicionOrdenamientoSQL.Append(" ORDER BY ItemName " & sortDir)
        Else
            definicionOrdenamientoSQL.Append(" ORDER BY OnHand " & sortDir)
        End If

        Dim queryEjecucionSAP As String = String.Format(querySAP, almacenSAPTienda, definicionOrdenamientoSQL.ToString(), condicionArticuloSAP.ToString(), start, limit)
        Dim queryEjecucionSAPTotalArticulos As String = String.Format(queryTotalRegistrosSAP, condicionArticuloSAP.ToString())

        Dim conexionSAP As System.Data.Common.DbConnection = Nothing
        Dim commandSAP As System.Data.Common.DbCommand = Nothing
        Dim readerSAP As System.Data.Common.DbDataReader = Nothing
        Dim conexionPOS As System.Data.Common.DbConnection = Nothing

        Try
            conexionSAP = New System.Data.SqlClient.SqlConnection()
            conexionSAP.ConnectionString = connStringWebSAP
            conexionSAP.Open()

            commandSAP = conexionSAP.CreateCommand()
            commandSAP.CommandType = CommandType.Text

            If obtenerTotalArticulos Then
                commandSAP.CommandText = queryEjecucionSAPTotalArticulos
                totalArticulos = commandSAP.ExecuteScalar()
            End If

            commandSAP.CommandText = queryEjecucionSAP

            conexionPOS = New System.Data.SqlClient.SqlConnection()
            conexionPOS.ConnectionString = connStringWebPOS
            conexionPOS.Open()

            readerSAP = commandSAP.ExecuteReader()
            Dim articuloEncontrado As Articulo
            Dim existenciaEnPOS As Decimal
            While readerSAP.Read()
                articuloEncontrado = New Articulo()
                articuloEncontrado.Codigo = readerSAP.GetString(0)
                articuloEncontrado.Nombre = readerSAP.GetString(1)
                existenciaEnPOS = ObtenerExistenciaPOS(articuloEncontrado.Codigo, codigoAlmacenPOS, conexionPOS)
                If existenciaEnPOS <> -1 Then
                    articuloEncontrado.Existencia = existenciaEnPOS
                Else
                    articuloEncontrado.Existencia = readerSAP.GetDecimal(2)
                End If

                listadoArticulos.Add(articuloEncontrado)
            End While
            Return listadoArticulos
        Catch ex As Exception
            logger.ErrorException("BuscarArticulo", ex)
            Return Nothing
        Finally
            If Not conexionPOS Is Nothing Then
                Try
                    If conexionPOS.State = ConnectionState.Open Then
                        conexionPOS.Close()
                    End If
                Catch
                End Try
                Try
                    conexionPOS.Dispose()
                Catch
                End Try
            End If

            If Not readerSAP Is Nothing Then
                Try
                    readerSAP.Close()
                Catch
                End Try
            End If

            If Not commandSAP Is Nothing Then
                Try
                    commandSAP.Dispose()
                Catch
                End Try
            End If

            If Not conexionSAP Is Nothing Then
                Try
                    If conexionSAP.State = ConnectionState.Open Then
                        conexionSAP.Close()
                    End If
                Catch
                End Try
                Try
                    conexionSAP.Dispose()
                Catch
                End Try
            End If
        End Try
    End Function

    Public Function ObtenerExistenciaPOS(ByVal codigoArticulo As String, ByVal codigoTienda As Integer, ByRef conexionPOS As System.Data.Common.DbConnection) As Decimal
        Dim queryExistencia As String =
            "SELECT Cantidad " & _
            "FROM Inventarios " & _
            "INNER JOIN Articulos ON Inventarios.IDArticulo=Articulos.IDArticulo " & _
            "WHERE Articulos.ArticuloBO='" & codigoArticulo & "' AND Almacen=" & codigoTienda

        Dim command As System.Data.Common.DbCommand = Nothing
        Dim reader As System.Data.Common.DbDataReader = Nothing
        Try
            command.CommandType = CommandType.Text
            command.CommandText = queryExistencia

            reader = command.ExecuteReader()
            If Not reader.Read Then
                Return -1
            End If

            Return reader.GetDecimal(0)
        Catch ex As Exception
            logger.ErrorException("ExistenciaPOS", ex)
            Return -1
        Finally
            If Not reader Is Nothing Then
                Try
                    reader.Close()
                Catch ex As Exception
                End Try
                reader = Nothing
            End If
            If Not command Is Nothing Then
                Try
                    command.Dispose()
                Catch ex As Exception
                End Try
            End If
        End Try
    End Function

    Public Function CalcularPrecioSinImpuesto(ByVal precioImpuesto As Decimal, ByVal sujetoImpuesto As Boolean, ByVal porcentajeImpuesto As Decimal)
        If Not sujetoImpuesto Then
            Return precioImpuesto
        Else
            Return Decimal.Round(precioImpuesto / (1 + (porcentajeImpuesto / 100)), 4)
        End If
    End Function

    Public Function CalcularPrecioConImpuesto(ByVal precio As Decimal, ByVal sujetoImpuesto As Boolean, ByVal porcentajeImpuesto As Decimal)

        If Not sujetoImpuesto Then
            Return precio
        Else
            Return Decimal.Round(precio * (1 + (porcentajeImpuesto / 100)), 4)
        End If
    End Function

    Public Function InformacionVentaArticulo(ByVal codigoArticulo As String, ByVal idTienda As Integer, ByVal factoresUsuario As List(Of Boolean), ByVal factoresTienda As List(Of Boolean), ByVal listaPrecioCosto As Integer, ByVal listaPrecioTienda As Integer, ByVal porcentajeIVA As Decimal,
                                            ByRef esArticuloFix As Boolean, ByRef factorArticulo As String, ByRef unidadesMedida As List(Of UnidadMedida), ByRef factoresPrecioArticulo As List(Of Factor), _
                                            ByRef precioListaIVA As Decimal,
                                            ByRef precioMinimoAutorizacion As Decimal, ByRef precioMaximoAutorizacion As Decimal,
                                            ByRef precioCosto As Decimal, ByRef puedeElegirFactor As Boolean, ByRef puedeModificarPrecio As Boolean, ByRef sujetoAImpuesto As Boolean,
                                            ByRef numeroFactorElegir As Integer) As Boolean
        factorArticulo = String.Empty
        unidadesMedida = Nothing
        factoresPrecioArticulo = Nothing
        precioCosto = 0
        precioMinimoAutorizacion = -1
        precioMaximoAutorizacion = -1
        numeroFactorElegir = -1
        precioListaIVA = 0
        puedeModificarPrecio = True
        puedeElegirFactor = False

        sujetoAImpuesto = EsSujetoAImpuesto(codigoArticulo)
        unidadesMedida = Me.UnidadesMedidaArticulo("Venta", codigoArticulo)

        If Not Me.TieneDefinidoFactorArticulo(codigoArticulo, idTienda, factorArticulo) Then
            Dim precioLista As Decimal = obtenerPrecioDeLista(codigoArticulo, listaPrecioTienda)
            precioListaIVA = CalcularPrecioConImpuesto(precioLista, sujetoAImpuesto, porcentajeIVA)
            puedeModificarPrecio = False
            Return True
        End If

        puedeElegirFactor = True
        Dim factoresArticulo As List(Of Decimal) = Nothing
        obtenerFactoresAplicablesArticulo(codigoArticulo, factorArticulo, factoresArticulo)

        If factoresArticulo.Count = 0 Then
            puedeModificarPrecio = False
            Return False
        End If

        precioCosto = obtenerPrecioDeLista(codigoArticulo, listaPrecioCosto)
        Dim factorUnidadMedida As Decimal = unidadesMedida(0).Factor

        factoresPrecioArticulo = New List(Of Factor)()

        precioMinimoAutorizacion = Decimal.MaxValue
        precioMaximoAutorizacion = Decimal.MinValue

        Dim letrasFactores As String() = New String() {"A", "B", "C", "D", "E"}

        For i As Integer = 0 To factoresTienda.Count - 1
            Dim precioFactorArticulo As Factor = New Factor
            precioFactorArticulo.Nombre = letrasFactores(i)
            precioFactorArticulo.Precio = CalcularPrecioConImpuesto(calcularPrecioFactorSegunPrecioCostoYUnidadMedida(precioCosto, factorUnidadMedida, factoresArticulo(i)), sujetoAImpuesto, porcentajeIVA)
            factoresPrecioArticulo.Add(precioFactorArticulo)

        Next

        Dim primerFactorDefUsuario As Integer = 0
        Dim ultimoFactorDefUsuario As Integer = factoresUsuario.Count - 1
        Dim seEncontroPrimerFactor As Boolean = False
        For i As Integer = 0 To factoresUsuario.Count - 1
            If Not seEncontroPrimerFactor AndAlso factoresUsuario(i) Then
                seEncontroPrimerFactor = True
                primerFactorDefUsuario = i
            End If
            If factoresUsuario(i) Then
                ultimoFactorDefUsuario = i
            End If
        Next

        If Not seEncontroPrimerFactor Then
            numeroFactorElegir = -1
            precioMinimoAutorizacion = -2
            precioMaximoAutorizacion = -2
        Else
            numeroFactorElegir = primerFactorDefUsuario
            If primerFactorDefUsuario = 0 Then
                precioMinimoAutorizacion = -1
            Else
                precioMinimoAutorizacion = factoresPrecioArticulo(primerFactorDefUsuario - 1).Precio
            End If

            If ultimoFactorDefUsuario = factoresUsuario.Count - 1 Then
                precioMaximoAutorizacion = -1
            Else
                precioMaximoAutorizacion = factoresPrecioArticulo(ultimoFactorDefUsuario + 1).Precio
            End If
        End If

        Return True
    End Function


    Public Sub ActualizarInformacionFactores(ByVal codigoArticulo As String, ByVal factorArticulo As String, ByVal factoresUsuario As List(Of Boolean),
                                             ByVal factoresTienda As List(Of Boolean), ByVal factorUnidadMedida As Decimal, ByVal precioCosto As Decimal,
                                             ByVal sujetoAImpuesto As Boolean, ByVal porcentajeImpuesto As Decimal,
                                                  ByRef factoresPrecioArticulo As List(Of Factor), ByRef precioMinimoAutorizacion As Decimal, ByRef precioMaximoAutorizacion As Decimal, ByRef numeroFactorSeleccionar As Integer)
        numeroFactorSeleccionar = -1

        Dim factoresArticulo As List(Of Decimal) = Nothing
        obtenerFactoresAplicablesArticulo(codigoArticulo, factorArticulo, factoresArticulo)

        Dim letrasFactores As String() = New String() {"A", "B", "C", "D", "E"}

        factoresPrecioArticulo = New List(Of Factor)()
        For i As Integer = 0 To factoresTienda.Count - 1
            Dim precioFactorArticulo As Factor = New Factor
            precioFactorArticulo.Nombre = letrasFactores(i)
            precioFactorArticulo.Precio = CalcularPrecioConImpuesto(calcularPrecioFactorSegunPrecioCostoYUnidadMedida(precioCosto, factorUnidadMedida, factoresArticulo(i)), sujetoAImpuesto, porcentajeImpuesto)
            factoresPrecioArticulo.Add(precioFactorArticulo)
        Next

        Dim primerFactorDefUsuario As Integer = 0
        Dim ultimoFactorDefUsuario As Integer = factoresUsuario.Count - 1
        Dim seEncontroPrimerFactor As Boolean = False

        For i As Integer = 0 To factoresUsuario.Count - 1
            If Not seEncontroPrimerFactor AndAlso factoresUsuario(i) Then
                seEncontroPrimerFactor = True
                primerFactorDefUsuario = i
            End If
            If factoresUsuario(i) Then
                ultimoFactorDefUsuario = i
            End If
        Next

        If Not seEncontroPrimerFactor Then
            precioMinimoAutorizacion = -2
            precioMaximoAutorizacion = -2
            numeroFactorSeleccionar = -1
        Else
            numeroFactorSeleccionar = primerFactorDefUsuario
            If primerFactorDefUsuario = 0 Then
                precioMinimoAutorizacion = -1
            Else
                precioMinimoAutorizacion = factoresPrecioArticulo(primerFactorDefUsuario - 1).Precio
            End If

            If ultimoFactorDefUsuario = factoresUsuario.Count - 1 Then
                precioMaximoAutorizacion = -1
            Else
                precioMaximoAutorizacion = factoresPrecioArticulo(ultimoFactorDefUsuario + 1).Precio
            End If
        End If
    End Sub

    Private Function calcularPrecioFactorSegunPrecioCostoYUnidadMedida(ByVal precioBase As Decimal, ByVal factorUnidadMedida As Decimal, ByVal factorPrecio As Decimal) As Decimal
        Dim precioBaseSegunUnidadMedida As Decimal = precioBase * factorUnidadMedida
        Return Decimal.Round(((factorPrecio / 100) + 1) * precioBaseSegunUnidadMedida, 4)
    End Function

    Private Sub obtenerFactoresAplicablesArticulo(ByVal codigoArticulo As String, ByVal codigoFactor As String, ByRef defFactores As List(Of Decimal))
        defFactores = New List(Of Decimal)()
        Dim query As String = "SELECT Factor1,Factor2,Factor3,Factor4,Factor5 FROM Factores WHERE Factor= '" & codigoFactor & "'"
        Try
            Dim tablaFactores As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Factor", connStringWebPOS)
            If tablaFactores.Rows.Count <> 1 Then
                Throw New ApplicationException()
            End If

            defFactores.Add(tablaFactores.Rows.Item(0)("Factor1"))
            defFactores.Add(tablaFactores.Rows.Item(0)("Factor2"))
            defFactores.Add(tablaFactores.Rows.Item(0)("Factor3"))
            defFactores.Add(tablaFactores.Rows.Item(0)("Factor4"))
            defFactores.Add(tablaFactores.Rows.Item(0)("Factor5"))

        Catch ex As Exception
            logger.ErrorException("obtenerFactoresAplicablesArticulo", ex)
            Throw ex
        End Try
    End Sub

    Public Function UnidadesMedidaArticulo(ByVal tipoMovimiento As String, ByVal codigoArticulo As String) As List(Of UnidadMedida)
        Dim unidadesMedida As List(Of UnidadMedida) = New List(Of UnidadMedida)()
        Dim defUnidadMedida As UnidadMedida
        Dim query As String

        If tipoMovimiento = "Venta" Then
            query = "SELECT U_UniMed,U_Factor FROM [@UNIMED] WHERE U_CodArt='" & codigoArticulo & "' AND U_Venta='Y'"
        ElseIf tipoMovimiento = "Compra" Then
            query = "SELECT U_UniMed,U_Factor FROM [@UNIMED] WHERE U_CodArt='" & codigoArticulo & "' AND U_Compra='Y'"
        Else
            query = "SELECT U_UniMed,U_Factor FROM [@UNIMED] WHERE U_CodArt='" & codigoArticulo & "' AND U_Invent='Y'"
        End If

        Try
            Dim tablaUnidadMedida As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "UnidadesMedida", connStringWebSAP)
            If tablaUnidadMedida.Rows.Count = 0 Then
                defUnidadMedida = New UnidadMedida()
                defUnidadMedida.UnidadMedida = "Unica"
                defUnidadMedida.Factor = 1
                unidadesMedida.Add(defUnidadMedida)
                Return unidadesMedida
            End If

            For i As Integer = 0 To tablaUnidadMedida.Rows.Count - 1
                defUnidadMedida = New UnidadMedida()
                defUnidadMedida.UnidadMedida = tablaUnidadMedida.Rows(i)("U_UniMed")
                defUnidadMedida.Factor = tablaUnidadMedida.Rows(i)("U_Factor")
                unidadesMedida.Add(defUnidadMedida)
            Next
            Return unidadesMedida
        Catch ex As Exception
            logger.ErrorException("UnidadesMedidaArticulo", ex)
            Throw ex
        End Try

    End Function


    Private Function obtenerPrecioDeLista(ByVal codigoArticulo As String, ByVal listaPrecioFix As Integer) As Decimal
        Dim query As String = "SELECT Price FROM ITM1 WHERE ItemCode='" & codigoArticulo & "' AND PriceList=" & listaPrecioFix
        Try
            Dim tablaPrecioFix As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Precio", connStringWebSAP)
            Return tablaPrecioFix.Rows.Item(0)("Price")
        Catch ex As Exception
            logger.ErrorException("obtenerPrecioFix", ex)
            Throw ex
        End Try
    End Function

    Private Function EsSujetoAImpuesto(ByVal codigoArticulo As String) As Boolean
        Dim query As String = "SELECT VatLiable FROM OITM WHERE ItemCode='" & codigoArticulo & "'"
        Try
            Dim tablaSujetoImpuesto As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "SujImp", connStringWebSAP)
            Return tablaSujetoImpuesto.Rows.Item(0)("VatLiable") = "Y"
        Catch ex As Exception
            logger.ErrorException("EsSujetoAImpuesto", ex)
            Throw ex
        End Try
    End Function

    Private Function TieneDefinidoFactorArticulo(ByVal codigoArticulo As String, ByVal idTienda As Integer, ByRef factor As String) As Boolean
        factor = String.Empty

        Dim query As String = "SELECT Factor FROM AdminStoreFactores WHERE AdminStoreId=" & idTienda & " AND IDArticulo='" & codigoArticulo & "'"
        Try
            Dim tablaFactorDefinido As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Factores", connStringWebPOS)
            If tablaFactorDefinido.Rows.Count = 0 Then
                Return False
            End If
            factor = tablaFactorDefinido.Rows.Item(0)("Factor")
            Return True
        Catch ex As Exception
            logger.ErrorException("EsArticuloFix", ex)
            Throw ex
        End Try
    End Function


End Class
