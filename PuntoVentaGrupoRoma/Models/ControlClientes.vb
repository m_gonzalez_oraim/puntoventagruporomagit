﻿Imports NLog
Public Class ControlClientes
    Shared logger As NLog.Logger = LogManager.GetCurrentClassLogger()

    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb As String = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Function ObtenerCliente(ByVal codigoCliente As Integer) As Cliente
        Dim queryBusqueda As String =
            "SELECT Id,ISNULL(Nombre,'') AS Nombre,ISNULL(actNOMBRE,'') AS actNOMBRE,ISNULL(actAPEPAterno,'') AS actAPEPAterno,ISNULL(actAPEMATERNO,'') AS actAPEMATERNO,ISNULL(NombreComercial,'') AS NombreComercial,ISNULL(RFC,'') AS RFC," & _
            "ISNULL(CalleNumero,'') AS CalleNumero,ISNULL(actCALLE,'') AS actCALLE,ISNULL(actNUMEXT,'') AS actNUMEXT,ISNULL(actNUMINT,'') AS actNUMINT,ISNULL(Colonia,'') AS Colonia,ISNULL(CP,'') AS CP,ISNULL(DelMun,'') AS DelMun,ISNULL(Estado,'') AS Estado," & _
            "ISNULL(TelCasa,'') AS TelCasa,ISNULL(TelOfc,'') AS TelOfc,ISNULL(Correo,'') AS Correo,actFCHNacimiento " & _
            "FROM Clientes WHERE Id=" & codigoCliente

        Dim dataTableClientes As DataTable = oDb.EjecutaQry_Tabla(queryBusqueda, CommandType.Text, "Clientes", connStringWeb)

        Return Cliente.CargarDeDataRow(dataTableClientes.Rows(0))
    End Function

    Public Function BuscarCliente(ByVal tipoParametroBusqueda As String, ByVal valorParametro As String) As List(Of Cliente)
        Dim queryBusqueda As String = _
            "SELECT Id,ISNULL(Nombre,'') AS Nombre,ISNULL(actNOMBRE,'') AS actNOMBRE,ISNULL(actAPEPAterno,'') AS actAPEPAterno,ISNULL(actAPEMATERNO,'') AS actAPEMATERNO,ISNULL(NombreComercial,'') AS NombreComercial,ISNULL(RFC,'') AS RFC," & _
            "ISNULL(CalleNumero,'') AS CalleNumero,ISNULL(actCALLE,'') AS actCALLE,ISNULL(actNUMEXT,'') AS actNUMEXT,ISNULL(actNUMINT,'') AS actNUMINT,ISNULL(Colonia,'') AS Colonia,ISNULL(CP,'') AS CP,ISNULL(DelMun,'') AS DelMun,ISNULL(Estado,'') AS Estado," & _
            "ISNULL(TelCasa,'') AS TelCasa,ISNULL(TelOfc,'') AS TelOfc,ISNULL(Correo,'') AS Correo,actFCHNacimiento " & _
            "FROM Clientes WHERE {0}"

        Dim parametroBusqueda As String = String.Empty
        If tipoParametroBusqueda = "R" Then
            parametroBusqueda = "RFC LIKE '" & valorParametro & "%'"
        ElseIf tipoParametroBusqueda = "N" Then
            parametroBusqueda = "Nombre LIKE '%" & valorParametro & "%' OR NombreComercial LIKE '%" & valorParametro & "%'"
        End If

        Dim queryEjecucionBusqueda As String = String.Format(queryBusqueda, parametroBusqueda)

        Dim listadoClientes As List(Of Cliente) = New List(Of Cliente)()
        Dim dataTableClientes As DataTable = oDb.EjecutaQry_Tabla(queryEjecucionBusqueda, CommandType.Text, "Clientes", connStringWeb)

        For i As Integer = 0 To dataTableClientes.Rows.Count - 1
            listadoClientes.Add(Cliente.CargarDeDataRow(dataTableClientes.Rows(i)))
        Next
        Return listadoClientes
    End Function

    Public Function RegistrarLogCliente(ByVal accion As String, ByVal idTienda As Integer, ByVal usuario As String, ByRef clienteCreado As Cliente)
        Try
            Dim squery As String = "set dateformat dmy insert into Log_Clientes (Accion,fecha,Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,NombreComercial,actCALLE,actNUMEXT,actNUMINT,Tienda,Usuario,IDCliente,Importado,Error) values "
            squery = squery & "('" & accion & "',GETDATE(),'" & Trim(clienteCreado.NombreCompleto) & "'," & _
            ",'" & clienteCreado.RFC.ToUpper() & "')," & _
            "'" & Trim(clienteCreado.Calle) & " #EXT:" & clienteCreado.CalleNumero & "'," & _
            "'" & Trim(clienteCreado.Colonia) & "'," & _
            "'" & Trim(clienteCreado.CodigoPostal) & "'," & _
            "'" & Trim(clienteCreado.TelCasa) & "'," & _
            "'" & Trim(clienteCreado.TelOficina) & "'," & _
            "'" & Trim(clienteCreado.Correo) & "'," & _
            "'" & clienteCreado.Estado & "'," & _
            "'" & clienteCreado.Municipio & "'," & _
            ",'','" & clienteCreado.FechaNacimiento & "','" & clienteCreado.Nombre & "','" & clienteCreado.ApPaterno & "','" & clienteCreado.ApMaterno & "'," & "'" & clienteCreado.NombreComercial & "'," & _
            "'" & clienteCreado.Calle & "','" & clienteCreado.NumeroExterior & "','" & clienteCreado.NumeroInterior & idTienda & "'," & usuario & "," & clienteCreado.Codigo & ")"

            oDb.EjecutaQry(squery, CommandType.Text, connStringWeb)
            Return True
        Catch ex As Exception
            logger.ErrorException("registraLogClienteCreado", ex)
            Return False
        End Try
    End Function

    Public Function CrearCliente(ByRef clienteCrear As Cliente) As Boolean
        Try

            Dim squery As String = "set dateformat dmy Insert into clientes (Nombre,RFC,CalleNumero,Colonia,CP,TelCasa,TelOfc,Correo,Estado,DelMun,actFCHNACIMIENTO,actNOMBRE,actAPEPATERNO,actAPEMATERNO,NombreComercial,actCALLE,actNUMEXT,actNUMINT,Importado,Error) values (" & vbNewLine
            squery = squery & "'" & clienteCrear.NombreCompleto & "',"
            squery = squery & "'" & clienteCrear.RFC & "',"
            squery = squery & "'" & clienteCrear.CalleNumero & "',"
            squery = squery & "'" & Trim(clienteCrear.Colonia) & "',"
            squery = squery & "'" & Trim(clienteCrear.CodigoPostal) & "',"
            squery = squery & "'" & Trim(clienteCrear.TelCasa) & "',"
            squery = squery & "'" & Trim(clienteCrear.TelOficina) & "',"
            squery = squery & "'" & Trim(clienteCrear.Correo) & "',"
            squery = squery & "'" & clienteCrear.Estado & "',"
            squery = squery & "'" & clienteCrear.Municipio & "',"
            squery = squery & "'" & clienteCrear.FechaNacimiento.ToString.Split(" ")(0) & "','" & clienteCrear.Nombre & "','" & clienteCrear.ApPaterno & "','" & clienteCrear.ApMaterno & "','" & clienteCrear.NombreComercial & "','" & clienteCrear.Calle & "','" & clienteCrear.NumeroExterior & "','" & clienteCrear.NumeroInterior & "',0,0"
            squery = squery & ")"

            Dim codigoCliente As Long = 0
            oDb.EjecuteQryObtenAutonumeric(squery, CommandType.Text, connStringWeb, codigoCliente)

            clienteCrear.Codigo = codigoCliente

            Return True
        Catch ex As Exception
            logger.ErrorException("CrearCliente", ex)
            Return False
        End Try

    End Function

    Public Function ActualizarCliente(ByRef clienteActualizar As Cliente) As Boolean
        Try

            Dim squery As String = "SET DATEFORMAT DMY Update clientes set"
            squery = squery & " Nombre='" & clienteActualizar.NombreCompleto & "'" & vbNewLine
            squery = squery & " ,RFC='" & clienteActualizar.RFC & "'" & vbNewLine
            squery = squery & " ,CalleNumero='" & clienteActualizar.CalleNumero & "'" & vbNewLine
            squery = squery & " ,Colonia='" & clienteActualizar.Colonia & "'" & vbNewLine
            squery = squery & " ,CP='" & clienteActualizar.CodigoPostal & "'" & vbNewLine
            squery = squery & " ,TelCasa='" & clienteActualizar.TelCasa & "'" & vbNewLine
            squery = squery & " ,TelOfc='" & clienteActualizar.TelOficina & "'" & vbNewLine
            squery = squery & " ,Correo='" & clienteActualizar.Correo & "'" & vbNewLine
            squery = squery & " ,Estado='" & clienteActualizar.Estado & "'" & vbNewLine
            squery = squery & " ,DelMun='" & clienteActualizar.Municipio & "'" & vbNewLine
            squery = squery & " ,actFCHNACIMIENTO='" & clienteActualizar.FechaNacimiento & "'" & vbNewLine
            squery = squery & " ,actNOMBRE='" & clienteActualizar.Nombre & "'" & vbNewLine
            squery = squery & " ,actAPEPATERNO='" & clienteActualizar.ApPaterno & "'" & vbNewLine
            squery = squery & " ,actAPEMATERNO='" & clienteActualizar.ApMaterno & "'" & vbNewLine
            squery = squery & " ,actCALLE='" & clienteActualizar.Calle & "'" & vbNewLine
            squery = squery & " ,actNUMEXT='" & clienteActualizar.NumeroExterior & "'" & vbNewLine
            squery = squery & " ,actNUMINT='" & clienteActualizar.NumeroInterior & "'" & vbNewLine
            squery = squery & " ,NombreComercial='" & clienteActualizar.NombreComercial & "'" & vbNewLine
            squery = squery & " ,Importado=0, Error=0" & vbNewLine
            squery = squery & " where Id='" & clienteActualizar.Codigo & "'" & vbNewLine & vbNewLine

            oDb.EjecutaQry(squery, CommandType.Text, connStringWeb)

            Return True
        Catch ex As Exception
            logger.ErrorException("CrearCliente", ex)
            Return False
        End Try

    End Function


    Public Function RFCDuplicado(ByVal rfc As String) As Boolean
        Dim queryRFCDuplicado As String = "SELECT COUNT(*) FROM Clientes WHERE RFC='" & rfc & "'"

        Dim dt As DataTable = oDb.EjecutaQry_Tabla(queryRFCDuplicado, CommandType.Text, "Cliente", connStringWeb)
        If dt.Rows.Item(0).Item(0) <> 0 Then
            Return True
        End If
        Return False
    End Function

    Public Function RFCDuplicado(ByVal rfc As String, ByVal codigoClienteIgnorar As Integer) As Boolean
        Dim queryRFCDuplicado As String = "SELECT COUNT(*) FROM Clientes WHERE RFC='" & rfc & "' AND Id<>" & codigoClienteIgnorar

        Dim dt As DataTable = oDb.EjecutaQry_Tabla(queryRFCDuplicado, CommandType.Text, "Cliente", connStringWeb)
        If dt.Rows.Item(0).Item(0) <> 0 Then
            Return True
        End If
        Return False
    End Function

End Class
