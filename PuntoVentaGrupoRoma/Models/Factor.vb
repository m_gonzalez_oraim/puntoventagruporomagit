﻿Public Class Factor

    Public Property Nombre As String
    Public Property Precio As Decimal

    Public ReadOnly Property Descripcion As String
        Get
            Return Nombre & "-" & Precio
        End Get
    End Property

End Class
