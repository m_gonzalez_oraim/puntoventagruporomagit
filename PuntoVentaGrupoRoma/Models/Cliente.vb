﻿Public Class Cliente

    Public Shared Function CargarDeDataRow(ByVal registro As DataRow) As Cliente
        If (registro("actFCHNacimiento") Is DBNull.Value) Then
            registro("actFCHNacimiento") = DateTime.MinValue
        End If

        Return New Cliente(
                registro("Id"), registro("actNOMBRE"), registro("actAPEPAterno"), registro("actAPEMATERNO"), registro("NombreComercial"), registro("RFC"),
                registro("actCALLE"), registro("actNUMEXT"), registro("actNUMINT"), registro("Colonia"), registro("CP"), registro("DelMun"), registro("Estado"),
                registro("TelCasa"), registro("TelOfc"), registro("Correo"), registro("actFCHNacimiento"))
    End Function

    Public Sub New(
                  ByVal p_Codigo As String,
                  ByVal p_Nombre As String,
                  ByVal p_ApPaterno As String,
                  ByVal p_ApMaterno As String,
                  ByVal p_NombreComercial As String,
                  ByVal p_RFC As String,
                  ByVal p_Calle As String,
                  ByVal p_NumeroExterior As String,
                  ByVal p_NumeroInterior As String,
                  ByVal p_Colonia As String,
                  ByVal p_CodigoPostal As String,
                  ByVal p_Municipio As String,
                  ByVal p_Estado As String,
                  ByVal p_TelCasa As String,
                  ByVal p_TelOfc As String,
                  ByVal p_Correo As String,
                  ByVal p_FechaNacimeinto As DateTime)
        Codigo = p_Codigo
        Nombre = p_Nombre
        ApPaterno = p_ApPaterno
        ApMaterno = p_ApMaterno
        NombreComercial = p_NombreComercial
        RFC = p_RFC
        Calle = p_Calle
        NumeroExterior = p_NumeroExterior
        NumeroInterior = p_NumeroInterior
        Colonia = p_Colonia
        CodigoPostal = p_CodigoPostal
        Municipio = p_Municipio
        Estado = p_Estado
        TelCasa = p_TelCasa
        TelOficina = p_TelOfc
        Correo = p_Correo
        FechaNacimiento = p_FechaNacimeinto.ToString("dd-MM-yyyy")
    End Sub

    Public Function ValidarInformacionCapturaCliente(ByRef mensajeError As String) As Boolean
        If Nombre = String.Empty Then
            mensajeError = "Nombre obligatorio"
            Return False
        End If

        If ApPaterno = String.Empty Then
            mensajeError = "Apellido paterno obligatorio"
            Return False
        End If

        If ApMaterno = String.Empty Then
            mensajeError = "Apellido materno obligatorio"
            Return False
        End If

        If RFC = String.Empty Then
            mensajeError = "RFC obligatorio"
            Return False
        End If

        If RFC.Length <> 12 AndAlso RFC.Length <> 13 Then
            mensajeError = "RFC debe tener 12 o 13 caracteres"
            Return False
        End If

        If RFC.Length = 12 AndAlso NombreComercial = String.Empty Then
            mensajeError = "Nombre comercial obligatorio cuando se capturan compañias"
            Return False
        End If

        If Calle = String.Empty Then
            mensajeError = "Calle es obligatorio"
            Return False
        End If

        If NumeroExterior = String.Empty Then
            mensajeError = "Numero exterior es obligatorio"
            Return False
        End If

        If Colonia = String.Empty Then
            mensajeError = "Colonia es obligatorio"
            Return False
        End If

        If CodigoPostal = String.Empty Then
            mensajeError = "Codigo postal es obligatorio"
            Return False
        End If

        If Municipio = String.Empty Then
            mensajeError = "Municipio es oblitagorio"
            Return False
        End If

        If Estado = String.Empty Then
            mensajeError = "Estado es obligatorio"
            Return False
        End If

        Return True
    End Function

    Public Property Codigo As String
    Public Property Nombre As String
    Public Property ApPaterno As String
    Public Property ApMaterno As String
    Public Property NombreComercial As String
    Public Property RFC As String
    Public Property Calle As String
    Public Property NumeroExterior As String
    Public Property NumeroInterior As String
    Public Property Colonia As String
    Public Property CodigoPostal As String
    Public Property Municipio As String
    Public Property Estado As String
    Public Property TelCasa As String
    Public Property TelOficina As String
    Public Property Correo As String
    Public Property FechaNacimiento As String

    Public ReadOnly Property NombreCompleto() As String
        Get
            Return Nombre & " " & ApPaterno & " " & ApMaterno
        End Get
    End Property

    Public ReadOnly Property CalleNumero() As String
        Get
            If NumeroInterior <> String.Empty Then
                Return Calle & " " & NumeroExterior & " INT " & NumeroInterior
            Else
                Return Calle & " " & NumeroExterior & " "
            End If
        End Get
    End Property

    Public ReadOnly Property Direccion() As String
        Get
            Return CalleNumero() & " " & "COL " & Colonia & " CP." & CodigoPostal
        End Get
    End Property

    Public Sub CopiarDe(ByRef cliente As Cliente)
        ApMaterno = cliente.ApMaterno
        ApPaterno = cliente.ApPaterno
        Calle = cliente.Calle
        Codigo = cliente.Codigo
        CodigoPostal = cliente.CodigoPostal
        Colonia = cliente.Colonia
        Correo = cliente.Correo
        Estado = cliente.Estado
        FechaNacimiento = cliente.FechaNacimiento
        Municipio = cliente.Municipio
        Nombre = cliente.Nombre
        NombreComercial = cliente.NombreComercial
        NumeroExterior = cliente.NumeroExterior
        NumeroInterior = cliente.NumeroInterior
        RFC = cliente.RFC
        TelCasa = cliente.TelCasa
        TelOficina = cliente.TelOficina
    End Sub

End Class
