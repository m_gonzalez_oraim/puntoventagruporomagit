﻿Imports System.Security.Cryptography
Imports System.IO

Public Class Crypto
#Region "enums, constants & fields"
    'types of symmetric encyption
    Public Enum CryptoTypes
        encTypeDES = 0
        encTypeRC2
        encTypeRijndael
        encTypeTripleDES
    End Enum

    Private Const CRYPT_DEFAULT_PASSWORD As String = "yourDefaultPassword"
    '"CB06cfE507a1";
    Private Const CRYPT_DEFAULT_METHOD As CryptoTypes = CryptoTypes.encTypeRijndael

    Private mKey As Byte() = {1, 2, 3, 4, 5, 6, _
        7, 8, 9, 10, 11, 12, _
        13, 14, 15, 16, 17, 18, _
        19, 20, 21, 22, 23, 24}
    Private mIV As Byte() = {65, 110, 68, 26, 69, 178, _
        200, 219}
    Private SaltByteArray As Byte() = {&H49, &H76, &H61, &H6E, &H20, &H4D, _
        &H65, &H64, &H76, &H65, &H64, &H65, _
        &H76}
    Private mCryptoType As CryptoTypes = CRYPT_DEFAULT_METHOD
    Private mPassword As String = CRYPT_DEFAULT_PASSWORD
#End Region

#Region "Constructors"

    Public Sub New()
        calculateNewKeyAndIV()
    End Sub

    Public Sub New(CryptoType As CryptoTypes)
        Me.CryptoType = CryptoType
    End Sub
#End Region

#Region "Props"

    ''' <summary>
    '''     type of encryption / decryption used
    ''' </summary>
    Public Property CryptoType() As CryptoTypes
        Get
            Return mCryptoType
        End Get
        Set(value As CryptoTypes)
            If mCryptoType <> value Then
                mCryptoType = value
                calculateNewKeyAndIV()
            End If
        End Set
    End Property

    ''' <summary>
    '''     Passsword Key Property.
    '''     The password key used when encrypting / decrypting
    ''' </summary>
    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(value As String)
            If mPassword <> value Then
                mPassword = value
                calculateNewKeyAndIV()
            End If
        End Set
    End Property
#End Region

#Region "Encryption"

    Public Function Encrypt(inputText As String) As String
        'declare a new encoder
        Dim UTF8Encoder As New UTF8Encoding()
        'get byte representation of string
        Dim inputBytes As Byte() = UTF8Encoder.GetBytes(inputText)

        'convert back to a string
        Return Convert.ToBase64String(EncryptDecrypt(inputBytes, True))
    End Function

    Public Function Encrypt(inputText As String, password As String) As String
        Me.Password = password
        Return Me.Encrypt(inputText)
    End Function

    Public Function Encrypt(inputText As String, password As String, cryptoType As CryptoTypes) As String
        mCryptoType = cryptoType
        Return Me.Encrypt(inputText, password)
    End Function

    Public Function Encrypt(inputText As String, cryptoType As CryptoTypes) As String
        Me.CryptoType = cryptoType
        Return Me.Encrypt(inputText)
    End Function

#End Region

#Region "Decryption"

    Public Function Decrypt(inputText As String) As String
        'declare a new encoder
        Dim UTF8Encoder As New UTF8Encoding()
        'get byte representation of string
        Dim inputBytes As Byte() = Convert.FromBase64String(inputText)

        'convert back to a string
        Return UTF8Encoder.GetString(EncryptDecrypt(inputBytes, False))
    End Function

    Public Function Decrypt(inputText As String, password As String) As String
        Me.Password = password
        Return Decrypt(inputText)
    End Function

    Public Function Decrypt(inputText As String, password As String, cryptoType As CryptoTypes) As String
        mCryptoType = cryptoType
        Return Decrypt(inputText, password)
    End Function

    Public Function Decrypt(inputText As String, cryptoType As CryptoTypes) As String
        Me.CryptoType = cryptoType
        Return Decrypt(inputText)
    End Function
#End Region

#Region "Symmetric Engine"

    Private Function EncryptDecrypt(inputBytes As Byte(), Encrpyt As Boolean) As Byte()
        'get the correct transform
        Dim transform As ICryptoTransform = getCryptoTransform(Encrpyt)

        'memory stream for output
        Dim memStream As New MemoryStream()

        Try
            'setup the cryption - output written to memstream
            Dim cryptStream As New CryptoStream(memStream, transform, CryptoStreamMode.Write)

            'write data to cryption engine
            cryptStream.Write(inputBytes, 0, inputBytes.Length)

            'we are finished
            cryptStream.FlushFinalBlock()

            'get result
            Dim output As Byte() = memStream.ToArray()

            'finished with engine, so close the stream
            cryptStream.Close()

            Return output
        Catch e As Exception
            'throw an error
            Throw New Exception("Error in symmetric engine. Error : " + e.Message, e)
        End Try
    End Function

    Private Function getCryptoTransform(encrypt As Boolean) As ICryptoTransform
        Dim SA As SymmetricAlgorithm = selectAlgorithm()
        SA.Key = mKey
        SA.IV = mIV
        If encrypt Then
            Return SA.CreateEncryptor()
        Else
            Return SA.CreateDecryptor()
        End If
    End Function

    Private Function selectAlgorithm() As SymmetricAlgorithm
        Dim SA As SymmetricAlgorithm
        Select Case mCryptoType
            Case CryptoTypes.encTypeDES
                SA = DES.Create()
                Exit Select
            Case CryptoTypes.encTypeRC2
                SA = RC2.Create()
                Exit Select
            Case CryptoTypes.encTypeRijndael
                SA = Rijndael.Create()
                Exit Select
            Case CryptoTypes.encTypeTripleDES
                SA = TripleDES.Create()
                Exit Select
            Case Else
                SA = TripleDES.Create()
                Exit Select
        End Select
        Return SA
    End Function

    Private Sub calculateNewKeyAndIV()
        'use salt so that key cannot be found with dictionary attack
        Dim pdb As New PasswordDeriveBytes(mPassword, SaltByteArray)
        Dim algo As SymmetricAlgorithm = selectAlgorithm()
        mKey = pdb.GetBytes(algo.KeySize / 8)
        mIV = pdb.GetBytes(algo.BlockSize / 8)
    End Sub

#End Region
End Class

''' <summary>
''' Hashing class. Only static members so no need to create an instance
''' </summary>
Public Class Hashing
#Region "enum, constants and fields"
    'types of hashing available
    Public Enum HashingTypes
        SHA
        SHA256
        SHA384
        SHA512
        MD5
    End Enum
#End Region

#Region "static members"
    Public Shared Function Hash(inputText As [String]) As String
        Return ComputeHash(inputText, HashingTypes.MD5)
    End Function

    Public Shared Function Hash(inputText As [String], hashingType As HashingTypes) As String
        Return ComputeHash(inputText, hashingType)
    End Function

    Public Shared Function isHashEqual(inputText As String, hashText As String) As Boolean
        Return (Hash(inputText) = hashText)
    End Function

    Public Shared Function isHashEqual(inputText As String, hashText As String, hashingType As HashingTypes) As Boolean
        Return (Hash(inputText, hashingType) = hashText)
    End Function
#End Region

#Region "Hashing Engine"

    Private Shared Function ComputeHash(inputText As String, hashingType As HashingTypes) As String
        Dim HA As HashAlgorithm = getHashAlgorithm(hashingType)

        'declare a new encoder
        Dim UTF8Encoder As New UTF8Encoding()
        'get byte representation of input text
        Dim inputBytes As Byte() = UTF8Encoder.GetBytes(inputText)


        'hash the input byte array
        Dim output As Byte() = HA.ComputeHash(inputBytes)

        'convert output byte array to a string
        Return Convert.ToBase64String(output)
    End Function

    Private Shared Function getHashAlgorithm(hashingType As HashingTypes) As HashAlgorithm
        Select Case hashingType
            Case HashingTypes.MD5
                Return New MD5CryptoServiceProvider()
            Case HashingTypes.SHA
                Return New SHA1CryptoServiceProvider()
            Case HashingTypes.SHA256
                Return New SHA256Managed()
            Case HashingTypes.SHA384
                Return New SHA384Managed()
            Case HashingTypes.SHA512
                Return New SHA512Managed()
            Case Else
                Return New MD5CryptoServiceProvider()
        End Select
    End Function
#End Region

End Class