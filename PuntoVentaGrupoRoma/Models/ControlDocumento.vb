﻿Public Class ControlDocumento
    Public Sub CalcularMontosDocumentoVenta(ByVal totalLinea As Decimal, ByVal totalLineaImpuestos As Decimal,
                                       ByVal montoDescuentoGlobal As Decimal, ByRef subtotal As Decimal, ByRef subTotal2 As Decimal, ByRef impuestos As Decimal, ByRef total As Decimal)
        If montoDescuentoGlobal = 0 Then
            subtotal = subtotal + totalLinea
            subTotal2 = subtotal
            total = total + totalLineaImpuestos
            impuestos = total - subtotal
        Else
            subtotal = subtotal + totalLinea
            subTotal2 = subtotal - montoDescuentoGlobal
            total = total + totalLineaImpuestos - montoDescuentoGlobal
            impuestos = total - subTotal2
        End If
    End Sub

    Public Sub CalcularMontoLinea(ByVal cantidad As Decimal, ByVal precio As Decimal, ByVal descuento As Decimal, ByVal sujetoImpuesto As Boolean, ByVal porcentajeImpuesto As Decimal, ByRef importeLinea As Decimal, ByRef importeLineaConImpuesto As Decimal)

        Dim montoDescuentoPrecio As Decimal = precio * descuento / 100
        Dim precioConDescuento As Decimal = precio - montoDescuentoPrecio

        importeLinea = Decimal.Round(precioConDescuento * cantidad, 2)

        If Not sujetoImpuesto Then
            importeLineaConImpuesto = importeLinea
        Else
            importeLineaConImpuesto = Decimal.Round(importeLinea * (1 + (porcentajeImpuesto / 100)), 2)
        End If
    End Sub

    Public Sub CalcularMontoLineaPrecioConYSinImpuesto(ByVal cantidad As Decimal, ByVal precio As Decimal, ByVal precioImpuesto As Decimal, ByVal descuento As Decimal, ByRef importeLinea As Decimal, ByRef importeLineaConImpuesto As Decimal)

        Dim montoDescuentoPrecio As Decimal = precio * descuento / 100D
        Dim precioConDescuento As Decimal = precio - montoDescuentoPrecio

        importeLinea = Decimal.Round(precioConDescuento * cantidad, 2)

        Dim montoDescuentoPrecioImpuesto As Decimal = precioImpuesto * descuento / 100D
        Dim precioConDescuentoImpuesto As Decimal = precioImpuesto - montoDescuentoPrecioImpuesto
        importeLineaConImpuesto = Decimal.Round(precioConDescuentoImpuesto * cantidad, 2)

    End Sub


End Class
