﻿Public Class PuntoVentaPrincipal
    Implements System.Security.Principal.IPrincipal

    Private oUsuarioPuntoVenta As UsuarioPuntoVentaIdentity
    Private genericIdentity As System.Security.Principal.IIdentity

    Public Sub New(ByVal identity As System.Security.Principal.IIdentity, ByVal infoUsuario As UsuarioPuntoVentaIdentity)
        genericIdentity = identity
        oUsuarioPuntoVenta = infoUsuario
    End Sub

    Public ReadOnly Property UsuarioPuntoVenta As UsuarioPuntoVentaIdentity
        Get
            Return oUsuarioPuntoVenta
        End Get
    End Property



    Public ReadOnly Property Identity As System.Security.Principal.IIdentity Implements System.Security.Principal.IPrincipal.Identity
        Get
            Return genericIdentity
        End Get
    End Property

    Public Function IsInRole(role As String) As Boolean Implements System.Security.Principal.IPrincipal.IsInRole
        Return True
    End Function

    Public Function TienePermisoCrear(ByVal modulo As String)
        Dim crear As Boolean = False
        Dim actualizar As Boolean = False
        Dim consultar As Boolean = False
        Dim cancelar As Boolean = False
        If Not oUsuarioPuntoVenta.TienePermiso(modulo, crear, actualizar, consultar, cancelar) Then
            Return False
        End If
        Return crear
    End Function

    Public Function TienePermisoActualizar(ByVal modulo As String)
        Dim crear As Boolean = False
        Dim actualizar As Boolean = False
        Dim consultar As Boolean = False
        Dim cancelar As Boolean = False
        If Not oUsuarioPuntoVenta.TienePermiso(modulo, crear, actualizar, consultar, cancelar) Then
            Return False
        End If
        Return actualizar
    End Function

    Public Function TienePermisoConsultar(ByVal modulo As String)
        Dim crear As Boolean = False
        Dim actualizar As Boolean = False
        Dim consultar As Boolean = False
        Dim cancelar As Boolean = False
        If Not oUsuarioPuntoVenta.TienePermiso(modulo, crear, actualizar, consultar, cancelar) Then
            Return False
        End If
        Return consultar
    End Function

    Public Function TienePermisoCancelar(ByVal modulo As String)
        Dim crear As Boolean = False
        Dim actualizar As Boolean = False
        Dim consultar As Boolean = False
        Dim cancelar As Boolean = False
        If Not oUsuarioPuntoVenta.TienePermiso(modulo, crear, actualizar, consultar, cancelar) Then
            Return False
        End If
        Return cancelar
    End Function

End Class
