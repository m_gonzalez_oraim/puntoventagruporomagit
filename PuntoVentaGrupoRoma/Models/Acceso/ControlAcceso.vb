﻿Imports System.Security.Principal

Public Class ControlAcceso
    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Sub TieneAcceso(ByVal NTUserAccount As String, ByVal NTUserDomain As String,
                           ByRef existente As Boolean, ByRef activo As Boolean,
                           ByRef userId As Integer, ByRef nombre As String,
                           ByRef menu As String, ByRef permisos As String)
        existente = False
        activo = False
        userId = -1
        nombre = String.Empty
        menu = String.Empty
        permisos = String.Empty

        Dim UserQuery As String = "SELECT TOP 1 AdminUserId,NTUserAccount,NTUserDomain,Status,FirstName,LastName,ISNULL(SuperUsuario,'N') AS SuperUsuario " & _
                    "FROM AdminUser " & _
                    " WHERE NTUserAccount = '" & NTUserAccount.ToLower() & "'"

        Dim tablaUsuario As System.Data.DataTable = oDb.EjecutaQry_Tabla(UserQuery, CommandType.Text, "InfoUsuario", connStringWeb)
        If tablaUsuario.Rows.Count <> 1 Then
            Return
        End If

        If tablaUsuario.Rows(0)("NTUserAccount") <> NTUserAccount AndAlso
            tablaUsuario.Rows(0)("NTUserDomain") <> NTUserDomain Then
            Return
        End If

        existente = True
        If tablaUsuario.Rows(0)("Status") <> "A" Then
            Return
        End If

        activo = True
        userId = tablaUsuario.Rows(0)("AdminUserId")
        nombre = tablaUsuario.Rows(0)("FirstName") & " " & tablaUsuario.Rows(0)("LastName")
        menu = MenuUsuario(userId, tablaUsuario.Rows(0)("SuperUsuario") = "Y")
        permisos = PermisosUsuario(userId, tablaUsuario.Rows(0)("SuperUsuario") = "Y")

    End Sub

    Private Function PermisosUsuario(ByVal userId As Integer, ByVal superUsuario As Boolean) As String
        Dim queryPermisos As String
        If superUsuario Then
            queryPermisos = "SELECT NombrePermiso,TieneCrear AS Crear,TieneActualizar AS Actualizar,TieneConsultar AS Consultar,TieneCancelar AS Cancelar FROM Permiso"
        Else
            queryPermisos = "SELECT NombrePermiso,Crear ,Actualizar,Consultar,Cancelar " & _
            "FROM Permiso PE " & _
            "INNER JOIN AdminRolePermiso AS RP ON RP.PermisoId=PE.PermisoId	" & _
            "INNER JOIN AdminUser US ON US.AdminRoleID=RP.AdminRoleId " & _
            "WHERE US.AdminUserId = " & userId
        End If

        Dim tablaPermisos As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryPermisos, CommandType.Text, "Permisos", connStringWeb)

        Dim permisosBuilder As StringBuilder = New StringBuilder()
        For i As Integer = 0 To tablaPermisos.Rows.Count - 1
            permisosBuilder.Append("|" & tablaPermisos.Rows(i)("NombrePermiso") &
                                   IIf(tablaPermisos.Rows(i)("Crear"), "1", "0") &
                                   IIf(tablaPermisos.Rows(i)("Actualizar"), "1", "0") &
                                   IIf(tablaPermisos.Rows(i)("Consultar"), "1", "0") &
                                   IIf(tablaPermisos.Rows(i)("Cancelar"), "1", "0"))
        Next
        Return permisosBuilder.ToString()
    End Function

    Private Function MenuUsuario(ByVal userId As Integer, ByVal superUsuario As Boolean) As String
        Dim queryMenu As String
        If superUsuario Then
            queryMenu = "SELECT MT.MenuTabName, MO.OptionName, MO.OptionURL " & _
                "FROM MenuOption MO " & _
                "INNER JOIN MenuTab MT ON MO.MenuTabID = MT.MenuTabID " & _
                "INNER JOIN Permiso PE ON MO.NombrePermiso=PE.NombrePermiso "
        Else
            queryMenu = "SELECT MT.MenuTabName, MO.OptionName, MO.OptionURL " & _
                "FROM MenuOption MO " & _
                "INNER JOIN MenuTab MT ON MO.MenuTabID = MT.MenuTabID " & _
                "INNER JOIN Permiso PE ON MO.NombrePermiso=PE.NombrePermiso " & _
                "INNER JOIN AdminRolePermiso RP ON RP.PermisoId=PE.PermisoId " & _
                "INNER JOIN AdminUser US ON US.AdminRoleID=RP.AdminRoleId " & _
                "WHERE US.AdminUserId=" & userId & "AND MT.Status='A' AND MO.Status = 'A' AND MO.OptionType = 'M'"
        End If

        Dim tablaMenu As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryMenu, CommandType.Text, "Menu", connStringWeb)

        Dim defMenuUsuario As Dictionary(Of String, StringBuilder) = New Dictionary(Of String, StringBuilder)()
        For i As Integer = 0 To tablaMenu.Rows.Count - 1
            If Not defMenuUsuario.ContainsKey(tablaMenu.Rows(i)("MenuTabName")) Then
                defMenuUsuario.Add(tablaMenu.Rows(i)("MenuTabName"), New StringBuilder())
            End If
            defMenuUsuario(tablaMenu.Rows(i)("MenuTabName")).Append("|" & tablaMenu.Rows(i)("OptionName") & "|" & tablaMenu.Rows(i)("OptionURL"))
        Next

        Dim menuBuilder As StringBuilder = New StringBuilder()
        For Each menu In defMenuUsuario
            menuBuilder.Append("º" & menu.Key & menu.Value.ToString())
        Next
        Return menuBuilder.ToString()
    End Function

    Public Function HayTiendasDadasDeAltaParaSuperUsuario(ByVal usuario As Integer) As Boolean
        Dim query As String = "SELECT COUNT(AdminStoreID) FROM UserStores WHERE AdminUserId=" & usuario

        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "AdminStore", connStringWeb)
        Return resultados.Rows(0)(0) > 0
    End Function

    Public Function EsSuperUsuario(ByVal usuario As Integer) As Boolean
        Dim querySuperUsuario As String = "SELECT AdminUserId,ISNULL(SuperUsuario,'N') AS SuperUsuario FROM AdminUser WHERE AdminUserID=" & usuario
        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(querySuperUsuario, CommandType.Text, "SuperUsuario", connStringWeb)

        If resultados.Rows.Count = 0 Then
            Return False
        End If

        Return resultados.Rows(0)("SuperUsuario") = "Y"
    End Function

    Public Function TienePermisoAcceso(ByVal usuario As Integer, ByVal modulo As String, ByRef accesoCrear As Boolean, ByRef accesoModificar As Boolean, ByRef accesoLectura As Boolean, ByRef accesoCancelar As Boolean)
        accesoCrear = False
        accesoModificar = False
        accesoLectura = False
        accesoCancelar = False

        If EsSuperUsuario(usuario) Then
            accesoCrear = True
            accesoModificar = True
            accesoLectura = True
            accesoCancelar = True
            Return True
        End If

        Dim query As String = String.Format("SELECT  Crear,Actualizar,Consultar,Cancelar from Permiso " & _
        "INNER JOIN AdminRolePermiso ON Permiso.PermisoID=AdminRolePermiso.PermisoID " & _
        "INNER JOIN AdminRole ON AdminRole.AdminRoleID=AdminRolePermiso.AdminRoleID " & _
        "INNER JOIN AdminUser ON AdminUser.AdminRoleID=AdminRole.ADminRoleId " &
        "WHERE NombrePermiso='Roles' AND AdminUser.ADminUserId={0}", usuario)

        Dim acceso As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Permisos", connStringWeb)
        If acceso.Rows.Count = 0 Then
            Return False
        End If

        accesoCrear = acceso.Rows(0)("Crear")
        accesoModificar = acceso.Rows(0)("Actualizar")
        accesoLectura = acceso.Rows(0)("Consultar")
        accesoCancelar = acceso.Rows(0)("Cancelar")

        Return True
    End Function

    Public Sub RedirigirPaginaAccesoNoPermitido(ByRef response As System.Web.HttpResponse)
    End Sub
End Class
