﻿Imports System.IO
Imports System.Xml.Serialization
Imports System.Xml

Public Structure UsuarioPuntoVentaIdentity

    Public Property StoreId As Integer
    Public Property StoreName As String
    Public Property UserId As Integer
    Public Property UserName As String
    Public Property Permisos As String
    Public Property MenuUsuario As String
    Public Property Guid As String

    Private authenticatedValue As Boolean

    Public Shared Function Serialize(ByVal usuario As UsuarioPuntoVentaIdentity) As String
        Dim serializedObject As StringBuilder = New StringBuilder
        Using auxXmlWriter As XmlWriter = XmlWriter.Create(serializedObject)
            Dim x As New XmlSerializer(usuario.GetType)
            x.Serialize(auxXmlWriter, usuario)
            auxXmlWriter.Close()
        End Using


        Dim crypto As Crypto = New Crypto()
        Return crypto.Encrypt(serializedObject.ToString())
    End Function

    Public Shared Function Deserialize(ByVal serialized As String) As UsuarioPuntoVentaIdentity

        Dim crypto As Crypto = New Crypto()
        Dim representacionSerializada As String = crypto.Decrypt(serialized)

        Dim deserializedObject As StringReader = New StringReader(representacionSerializada)
        Using auxXmlReader As XmlReader = XmlReader.Create(deserializedObject)
            Dim usuario As UsuarioPuntoVentaIdentity = Nothing
            Dim x As New XmlSerializer(usuario.GetType)
            usuario = x.Deserialize(auxXmlReader)
            Return usuario
        End Using

    End Function

    Public Sub New(ByVal userId As String, ByVal userName As String, ByVal permisos As String, ByVal menu As String, ByVal guid As String)
        Me.UserId = userId
        Me.UserName = userName
        Me.Permisos = permisos
        Me.MenuUsuario = menu
        Me.StoreId = -1
        Me.StoreName = String.Empty
        Me.Guid = guid
    End Sub
    Public Sub New(ByVal userId As String, ByVal userName As String, ByVal permisos As String, ByVal menu As String, ByVal storeId As Integer, ByVal storeName As String, ByVal guid As String)
        Me.UserId = userId
        Me.UserName = userName
        Me.Permisos = permisos
        Me.MenuUsuario = menu
        Me.StoreId = storeId
        Me.StoreName = storeName
        Me.Guid = guid
    End Sub

    Public Function DefinicionMenu() As Dictionary(Of String, List(Of OpcionMenu))
        If MenuUsuario Is Nothing OrElse MenuUsuario = String.Empty Then
            Return Nothing
        End If

        Dim elementosMenu As String() = MenuUsuario.Split("º")

        Dim defMenu As Dictionary(Of String, List(Of OpcionMenu)) = New Dictionary(Of String, List(Of OpcionMenu))()
        For i As Integer = 1 To elementosMenu.Length - 1
            Dim opcionesMenu As String() = elementosMenu(i).Split("|")
            defMenu.Add(opcionesMenu(0), New List(Of OpcionMenu))
            For j As Integer = 1 To opcionesMenu.Length - 1 Step 2
                defMenu(opcionesMenu(0)).Add(New OpcionMenu(opcionesMenu(j), opcionesMenu(j + 1)))
            Next
        Next
        Return defMenu
    End Function

    Public Sub RedireccionaAPaginaNoAcceso(ByRef response As System.Web.HttpResponse)

    End Sub

    Public Function TienePermiso(ByVal permiso As String, ByRef crear As Boolean, ByRef actualizar As Boolean, ByRef consultar As Boolean, ByRef cancelar As Boolean) As Boolean
        crear = False
        actualizar = False
        consultar = False
        cancelar = False

        If Not Permisos.Contains("|" & permiso) Then
            Return False
        End If
        Dim posicionDefPermiso As Integer = Permisos.IndexOf("|" & permiso)
        Dim defPermiso As String = Permisos.Substring(posicionDefPermiso + permiso.Length + 1, 4)

        crear = defPermiso(0) = "1"
        actualizar = defPermiso(1) = "1"
        consultar = defPermiso(2) = "1"
        cancelar = defPermiso(3) = "1"
        Return True
    End Function
End Structure
