﻿Public Class CatalogosSAP
    Private oDb As DBMaster = New DBMaster()
    Private connStringSap = ConfigurationManager.ConnectionStrings("DBConnSAP").ConnectionString

    Public Function ClientesMostradorSAP() As Dictionary(Of String, String)
        Dim query As String = "SELECT CardCode,CardName FROM OCRD WHERE CardName LIKE '%MOSTRADOR%'"

        Dim dataTableClientes As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Clientes", connStringSap)

        Dim clientes As Dictionary(Of String, String) = New Dictionary(Of String, String)()
        For i As Integer = 0 To dataTableClientes.Rows.Count - 1
            clientes.Add(dataTableClientes.Rows(i)("CardCode"), dataTableClientes.Rows(i)("CardName"))
        Next

        Return clientes

    End Function

    Public Function AlmacenesSAP() As Dictionary(Of String, String)
        Dim query As String = "SELECT WhsCode,WhsName FROM OWHS"

        Dim dataTableAlmacenes As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Almacenes", connStringSap)

        Dim almacenes As Dictionary(Of String, String) = New Dictionary(Of String, String)()
        For i As Integer = 0 To dataTableAlmacenes.Rows.Count - 1
            almacenes.Add(dataTableAlmacenes.Rows(i)("WhsCode"), dataTableAlmacenes.Rows(i)("WhsName"))
        Next

        Return almacenes
    End Function

    Public Function ListasPrecioSAP() As Dictionary(Of String, String)
        Dim query As String = "SELECT ListNum,ListName FROM OPLN"

        Dim dataTablaPrecios As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Precios", connStringSap)

        Dim listasPrecio As Dictionary(Of String, String) = New Dictionary(Of String, String)()
        For i As Integer = 0 To dataTablaPrecios.Rows.Count - 1
            listasPrecio.Add(dataTablaPrecios.Rows(i)("ListNum"), dataTablaPrecios.Rows(i)("ListName"))
        Next

        Return listasPrecio
    End Function

End Class
