﻿Public Class Tienda
    Public Property AdminStoreID As Integer
    Public Property StoreName As String
    Public Property Status As String
    Public Property StatusName As String
    Public Property Telefono As String
    Public Property CodigoPostal As String
    Public Property Delegacion As String
    Public Property EstadoId As String
    Public Property Colonia As String
    Public Property EmailTienda As String
    Public Property NumExt As String
    Public Property NumInt As String
    Public Property Calle As String

    Public Property WhsID As String
    Public Property ConsigWhsID As String
    Public Property TransitWhsID As String
    Public Property DefaultCustomer As String
    Public Property DefaultList As Integer
    Public Property PriceListCost As Integer
    Public Property ActIVA As String

End Class
