﻿Imports NLog

Public Class ControlTienda
    Shared logger As NLog.Logger = LogManager.GetCurrentClassLogger()

    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Function EliminarTienda(ByVal idTienda As Integer, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryTieneUsuariosTienda As String = "SELECT COUNT(*) FROM UserStores WHERE AdminStoreID=" & idTienda
        Dim resultadosTienda As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryTieneUsuariosTienda, CommandType.Text, "Usuario", connStringWeb)
        If resultadosTienda.Rows(0)(0) > 0 Then
            mensajeError = "No se puede eliminar la tienda ya que tiene usuarios asociados"
            Return False
        End If

        Dim queryEliminarTienda As StringBuilder = New StringBuilder()
        queryEliminarTienda.AppendLine("DELETE FROM AdminStore WHERE AdminStoreID=" & idTienda)
        queryEliminarTienda.AppendLine("UPDATE UserStores SET AdminStoreToSendID=null WHERE AdminStoreToSendID=" & idTienda)
        Dim registros As Integer = oDb.EjecutaQry(queryEliminarTienda.ToString(), CommandType.Text, connStringWeb)
        If registros <> 1 Then
            mensajeError = "Error al eliminar el usuario. Consulte con su administrador"
            Return False
        End If
        Return True
    End Function

    Public Function CrearTienda(ByRef tienda As Tienda, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryInsertarTienda As StringBuilder = New StringBuilder()
        queryInsertarTienda.AppendLine("INSERT INTO AdminStore(StoreName,Status,WhsID,DefaultList,PriceListCost, DefaultCustomer,Telefono,CodigoPostal,Delegacion,EstadoId,Colonia,actIVA,emailTienda,NumExt,NumInt,TransitWhsID,ConsigWhsID,Calle) VALUES ( '" & tienda.StoreName & "', '" & tienda.Status & "','" & tienda.WhsID & "','" & tienda.DefaultList & "'," & tienda.PriceListCost & ",'" & tienda.DefaultCustomer & "','" & tienda.Telefono & "','" & tienda.CodigoPostal & "','" & tienda.Delegacion & "','" & tienda.EstadoId & "','" & tienda.Colonia & "','" & tienda.ActIVA & "','" & tienda.EmailTienda & "','" & tienda.NumExt & "','" & tienda.NumInt & "','" & tienda.TransitWhsID & "','" & tienda.ConsigWhsID & "','" & tienda.Calle & "')")
        If oDb.EjecutaQry(queryInsertarTienda.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If
        mensajeError = "Error al crear la tienda. Consulte con el administrador"
        Return False
    End Function

    Public Function ActualizarTienda(ByRef tienda As Tienda, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryActualizarTienda As StringBuilder = New StringBuilder()
        queryActualizarTienda.Append("UPDATE AdminStore SET StoreName='" & tienda.StoreName & "', Status='" & tienda.Status & "', WhsID='" & tienda.WhsID & "',DefaultCustomer= '" & tienda.DefaultCustomer & "', Telefono='" & tienda.Telefono & "',CodigoPostal='" & tienda.CodigoPostal & "',Delegacion='" & tienda.Delegacion & "',EstadoId='" & tienda.EstadoId & "',Colonia='" & tienda.Colonia & "', actIVA='" & tienda.ActIVA & "',emailTienda='" & tienda.EmailTienda & "',NumExt='" & tienda.NumExt & "',NumInt='" & tienda.NumInt & "',TransitWhsID='" & tienda.TransitWhsID & "',ConsigWhsID='" & tienda.ConsigWhsID & "',Calle='" & tienda.Calle & "',DefaultList=" & tienda.DefaultList & ",PriceListCost=" & tienda.PriceListCost & " WHERE AdminStoreId=" & tienda.AdminStoreID)

        If oDb.EjecutaQry(queryActualizarTienda.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If

        Return False
    End Function

    Public Sub InformacionTiendaFromRequest(ByRef request As System.Web.HttpRequest, ByRef tienda As Tienda)
        Dim nombre As String = request("txtStoreName")
        Dim estado As String = request("cmbStatus")
        Dim calle As String = request("txtCalle")
        Dim numExt As String = request("txtNumExt")
        Dim numInt As String = request("txtNumInt")
        Dim colonia As String = request("txtColonia")
        Dim codigoPostal As String = request("txtCodigoPostal")
        Dim delegacion As String = request("txtDelegacion")
        Dim idEstado As String = request("cmbEstado")
        Dim telefono As String = request("txtTelefono")
        Dim email As String = request("txtEmailTienda")

        Dim almacenVenta As String = request("cmbWhsID")
        Dim almacenConsignacion As String = request("cmbConsigWhsID")
        Dim almacenTransito As String = request("cmbTransitWhsID")
        Dim clienteMostrador As String = request("cmbDefaultCustomer")
        Dim codigoImpuesto As String = request("cmbActIVA")
        Dim listaPrecioVenta As Integer = request("cmbDefaultList")
        Dim listaPrecioCosto As Integer = request("cmbPriceListCost")

        tienda = New Tienda()
        tienda.StoreName = nombre
        tienda.Status = estado
        tienda.Calle = calle
        tienda.NumExt = numExt
        tienda.NumInt = numInt
        tienda.Colonia = colonia
        tienda.CodigoPostal = codigoPostal
        tienda.Delegacion = delegacion
        tienda.EstadoId = idEstado
        tienda.Telefono = telefono
        tienda.EmailTienda = email
        tienda.WhsID = almacenVenta
        tienda.ConsigWhsID = almacenConsignacion
        tienda.TransitWhsID = almacenTransito
        tienda.DefaultCustomer = clienteMostrador
        tienda.ActIVA = codigoImpuesto
        tienda.PriceListCost = listaPrecioCosto
        tienda.DefaultList = listaPrecioVenta

        If Not request("hdnIdTienda") Is Nothing Then
            tienda.AdminStoreID = request("hdnIdTienda")
        End If

    End Sub

    Public Function ListadoTiendas() As List(Of Tienda)
        Dim query As String = "SELECT AdminStoreId,StoreName,Status AS StatusName FROM AdminStore"
        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "AdminStore", connStringWeb)

        Dim tiendas As List(Of Tienda) = New List(Of Tienda)()

        For i As Integer = 0 To resultados.Rows.Count - 1
            Dim auxTienda As Tienda = New Tienda()
            auxTienda.AdminStoreID = resultados.Rows(i)("AdminStoreId")
            auxTienda.StoreName = resultados.Rows(i)("StoreName")
            auxTienda.StatusName = resultados.Rows(i)("StatusName")
            tiendas.Add(auxTienda)
        Next

        Return tiendas
    End Function

    Public Function InformacionTienda(ByVal idTienda As Integer) As Tienda
        Dim query As String = "SELECT StoreName,Status,WhsId,DefaultCustomer,Telefono,CodigoPostal,Delegacion,EstadoId,Colonia,actIVA,emailTienda,NumExt,NumInt,TransitWhsID,ConsigWhsID,Calle,DefaultList,PriceListCost FROM AdminStore WHERE AdminStoreID={0}"
        Dim queryEjecucion As String = String.Format(query, idTienda)

        Dim tiendaData As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucion, CommandType.Text, "Tiendas", connStringWeb)
        If tiendaData.Rows.Count <> 1 Then
            Return Nothing
        End If

        Dim tienda As Tienda = New Tienda()
        tienda.StoreName = tiendaData.Rows(0)("StoreName")
        tienda.Status = tiendaData.Rows(0)("Status")
        tienda.WhsID = tiendaData.Rows(0)("WhsId")
        tienda.DefaultCustomer = tiendaData.Rows(0)("DefaultCustomer")
        tienda.Telefono = tiendaData.Rows(0)("Telefono")
        tienda.CodigoPostal = tiendaData.Rows(0)("CodigoPostal")
        tienda.Delegacion = tiendaData.Rows(0)("Delegacion")
        tienda.EstadoId = tiendaData.Rows(0)("EstadoId")
        tienda.Colonia = tiendaData.Rows(0)("Colonia")
        tienda.ActIVA = tiendaData.Rows(0)("actIVA")
        tienda.EmailTienda = tiendaData.Rows(0)("emailTienda")
        tienda.NumExt = tiendaData.Rows(0)("NumExt")
        tienda.NumInt = tiendaData.Rows(0)("NumInt")
        tienda.TransitWhsID = tiendaData.Rows(0)("TransitWhsID")
        tienda.ConsigWhsID = tiendaData.Rows(0)("ConsigWhsID")
        tienda.Calle = tiendaData.Rows(0)("Calle")
        tienda.DefaultList = tiendaData.Rows(0)("DefaultList")
        tienda.PriceListCost = tiendaData.Rows(0)("PriceListCost")

        Return tienda
    End Function


    Public Sub ConfiguracionVentaTienda(ByVal codigoTienda As Integer, ByRef listaPrecioVenta As Integer, ByRef listaPrecioCosto As Integer, ByRef porcentajeImpuesto As Decimal, ByRef clienteMostrador As Integer)
        Dim query As String = "SELECT AdminStore.PriceListCost, AdminStore.DefaultList,IVA.Porcentaje AS PorcImp,ISNULL(Clientes.Id,0) AS DefaultCustomer FROM AdminStore INNER JOIN IVA ON IVA.COD_IVA=AdminStore.actIVA LEFT JOIN Clientes ON Clientes.CardCode=AdminStore.DefaultCustomer  WHERE AdminStoreID = " & codigoTienda
        Try
            Dim tablaDatos As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "ConfiguracionTienda", connStringWeb)
            listaPrecioVenta = tablaDatos.Rows.Item(0)("DefaultList")
            listaPrecioCosto = tablaDatos.Rows.Item(0)("PriceListCost")
            porcentajeImpuesto = tablaDatos.Rows.Item(0)("PorcImp")
            clienteMostrador = tablaDatos.Rows.Item(0)("DefaultCustomer")
        Catch ex As Exception
            logger.ErrorException("ConfiguracionVentaTienda", ex)
            Throw ex
        End Try
    End Sub

    Public Function FechaAperturaCaja(ByVal codigoTienda As Integer) As DateTime
        Return DateTime.Now
    End Function

    Public Function ObtenerVendedoresPuntoDeVenta(ByVal codigoTienda As Integer) As System.Data.DataTable

        Dim queryVendedores As String =
            "SELECT AdminUser.AdminUserId AS Codigo, AdminUser.FirstName + ' '+AdminUser.LastName AS Descripcion " & _
            "FROM AdminUser " & _
            "INNER JOIN UserStores ON UserStores.AdminUserId=AdminUser.AdminUserId " & _
            "WHERE AdminStoreId={0} AND EsVendedor='Y' ORDER BY AdminUser.FirstName,AdminUser.LastName"

        Dim queryEjecucionVendedores As String = String.Format(queryVendedores, codigoTienda)
        Dim tablaVendedores As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucionVendedores, CommandType.Text, "VENDEDORES", connStringWeb)
        Return tablaVendedores
    End Function

    Public Function ObtenerCodigoAlmacenSAP(ByVal codigoTienda As Integer) As String
        Dim query As String = "SELECT WhsId FROM AdminStore WHERE AdminStoreID=" & codigoTienda

        Try
            Dim tablaDatos As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Tienda", connStringWeb)
            Return tablaDatos.Rows(0)("WhsId")
        Catch ex As Exception
            logger.ErrorException("ControlTieda", ex)
            Return Nothing
        End Try
    End Function


End Class
