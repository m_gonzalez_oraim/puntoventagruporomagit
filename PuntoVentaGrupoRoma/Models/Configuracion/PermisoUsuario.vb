﻿Public Class PermisoRolUsuario
    Property PermisoID As Integer
    Property NombrePermiso As String
    Property TieneCrear As Boolean
    Property TieneActualizar As Boolean
    Property TieneConsultar As Boolean
    Property TieneCancelar As Boolean
    Property Crear As Boolean
    Property Actualizar As Boolean
    Property Consultar As Boolean
    Property Cancelar As Boolean


End Class
