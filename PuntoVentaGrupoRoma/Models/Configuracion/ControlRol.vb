﻿Public Class ControlRol
    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Function EliminarRol(ByVal idRol As Integer, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryRolAsignadoAUsuario As String = "SELECT COUNT(*) FROM AdminUser WHERE AdminRoleID=" & idRol
        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryRolAsignadoAUsuario, CommandType.Text, "Rol", connStringWeb)
        If resultados.Rows(0)(0) > 0 Then
            mensajeError = "No se puede eliminar el rol ya que hay usuarios asignados a ese rol"
            Return False
        End If

        Dim queryEliminarRol As String = "DELETE FROM AdminRole WHERE AdminRoleID=" & idRol
        Dim registros As Integer = oDb.EjecutaQry(queryEliminarRol, CommandType.Text, connStringWeb)
        If registros <> 1 Then
            mensajeError = "Error al eliminar el rol. Consulte con su administrador"
            Return False
        End If

        Return True
    End Function

    Public Function CrearRol(ByRef rol As Rol, ByRef permisos As List(Of PermisoRolUsuario)) As Boolean
        Dim queryInsertarRol As StringBuilder = New StringBuilder()
        queryInsertarRol.Append("DECLARE @idRol AS Integer " & Chr(13))
        queryInsertarRol.Append("INSERT INTO AdminRole(RoleName,Status) VALUES ( '" & rol.RoleName & "', '" & rol.Status & "') " & Chr(13))
        queryInsertarRol.Append("SET @idRol=(SELECT @@IDENTITY)" & Chr(13))
        For i As Integer = 0 To permisos.Count - 1
            queryInsertarRol.Append(" INSERT INTO AdminRolePermiso (AdminRoleID,PermisoID,Crear,Actualizar,Consultar,Cancelar) VALUES(@idRol ," & permisos(i).PermisoID & "," & IIf(permisos(i).Crear, 1, 0) & "," & IIf(permisos(i).Actualizar, 1, 0) & "," & IIf(permisos(i).Consultar, 1, 0) & "," & IIf(permisos(i).Cancelar, 1, 0) & ")" & Chr(13))
        Next

        If oDb.EjecutaQry(queryInsertarRol.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If

        Return False

    End Function

    Public Function ActualizarRol(ByRef rol As Rol, ByRef permisos As List(Of PermisoRolUsuario)) As Boolean

        Dim queryActualizarRol As StringBuilder = New StringBuilder()
        queryActualizarRol.Append("DELETE FROM AdminRolePermiso WHERE AdminRoleID=" & rol.AdminRoleID & Chr(13))
        queryActualizarRol.Append("UPDATE AdminRole SET RoleName='" & rol.RoleName & "', Status='" & rol.Status & "' WHERE AdminRoleID=" & rol.AdminRoleID & Chr(13))

        For i As Integer = 0 To permisos.Count - 1
            queryActualizarRol.Append(" INSERT INTO AdminRolePermiso (AdminRoleID,PermisoID,Crear,Actualizar,Consultar,Cancelar) VALUES(" & rol.AdminRoleID & "," & permisos(i).PermisoID & "," & IIf(permisos(i).Crear, 1, 0) & "," & IIf(permisos(i).Actualizar, 1, 0) & "," & IIf(permisos(i).Consultar, 1, 0) & "," & IIf(permisos(i).Cancelar, 1, 0) & ")" & Chr(13))
        Next

        If oDb.EjecutaQry(queryActualizarRol.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If

        Return False
    End Function

    Public Sub InformacionRolFromRequest(ByRef request As System.Web.HttpRequest, ByVal postFijo As String, ByRef rol As Rol, ByRef permisos As List(Of PermisoRolUsuario))
        Dim nombreRol As String = request("txtRoleName" & postFijo)
        Dim estadoRol As String = request("cmbStatus" & postFijo)
        Dim numeroPermisos As Integer = request("hdnNumeroPermisos")

        rol = New Rol()
        rol.RoleName = nombreRol
        rol.Status = estadoRol
        If Not request("hdnIdRol") Is Nothing Then
            rol.AdminRoleID = request("hdnIdRol")
        End If

        permisos = New List(Of PermisoRolUsuario)()
        For i As Integer = 0 To numeroPermisos - 1
            Dim permiso As PermisoRolUsuario = informacionPermisoFromRequest(i, request)
            If Not permiso Is Nothing Then
                permisos.Add(permiso)
            End If
        Next


    End Sub

    Private Function informacionPermisoFromRequest(ByVal numeroPermiso As Integer, ByRef request As System.Web.HttpRequest) As PermisoRolUsuario
        Dim permisoRol As PermisoRolUsuario = New PermisoRolUsuario()
        permisoRol.Crear = False
        permisoRol.Cancelar = False
        permisoRol.Consultar = False
        permisoRol.Actualizar = False

        Dim seDefinioPermisoDeRequest As Boolean = False
        If Not request("chkCrear" & numeroPermiso) Is Nothing Then
            permisoRol.Crear = True
            seDefinioPermisoDeRequest = True
        End If
        If Not request("chkActualizar" & numeroPermiso) Is Nothing Then
            permisoRol.Actualizar = True
            seDefinioPermisoDeRequest = True
        End If
        If Not request("chkConsultar" & numeroPermiso) Is Nothing Then
            permisoRol.Consultar = True
            seDefinioPermisoDeRequest = True
        End If
        If Not request("chkCancelar" & numeroPermiso) Is Nothing Then
            permisoRol.Cancelar = True
            seDefinioPermisoDeRequest = True
        End If
        If Not request("hdnPermisoID" & numeroPermiso) Is Nothing Then
            permisoRol.PermisoID = request("hdnPermisoID" & numeroPermiso)
        Else
            seDefinioPermisoDeRequest = False
        End If

        If Not seDefinioPermisoDeRequest Then
            Return Nothing
        Else
            Return permisoRol
        End If

    End Function

    Public Function ListadoRolesUsuario(ByVal paraNuevo As Boolean) As Dictionary(Of Integer, String)
        Dim query As String
        If paraNuevo Then
            query = "SELECT AdminRoleID,RoleName FROM AdminRole WHERE Status='A'"
        Else
            query = "SELECT AdminRoleID,RoleName FROM AdminRole"
        End If

        Dim listadoRoles As Dictionary(Of Integer, String) = New Dictionary(Of Integer, String)()

        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Roles", connStringWeb)
        For i As Integer = 0 To resultados.Rows.Count - 1
            listadoRoles.Add(resultados.Rows(i)("AdminRoleID"), resultados.Rows(i)("RoleName"))
        Next
        Return listadoRoles

    End Function

    Public Function ListadoRoles() As List(Of Rol)
        Dim query As String = "SELECT AdminRoleID,RoleName,Status AS StatusName FROM AdminRole "
        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "AdminRole", connStringWeb)

        Dim roles As List(Of Rol) = New List(Of Rol)()

        For i As Integer = 0 To resultados.Rows.Count - 1
            Dim auxRol As Rol = New Rol()
            auxRol.AdminRoleID = resultados.Rows(i)("AdminRoleID")
            auxRol.RoleName = resultados.Rows(i)("RoleName")
            auxRol.StatusName = resultados.Rows(i)("StatusName")
            roles.Add(auxRol)
        Next

        Return roles
    End Function

    Public Function InformacionRol(ByVal idRol As Integer) As Rol
        Dim query As String = "SELECT RoleName,Status FROM AdminRole WHERE AdminRoleID={0}"
        Dim queryEjecucion As String = String.Format(query, idRol)

        Dim rolData As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucion, CommandType.Text, "Rol", connStringWeb)
        If rolData.Rows.Count <> 1 Then
            Return Nothing
        End If

        Dim rol As Rol = New Rol()
        rol.AdminRoleID = idRol
        rol.RoleName = rolData.Rows(0)("RoleName")
        rol.Status = rolData.Rows(0)("Status")

        Return rol
    End Function

    Public Function PermisosRol(ByVal idRol As Integer) As List(Of PermisoRolUsuario)
        Dim query As String = _
            "SELECT Permiso.PermisoID, Permiso.NombrePermiso,Permiso.TieneCrear,Permiso.TieneActualizar,Permiso.TieneConsultar,Permiso.TieneCancelar,ISNULL(PermisoRol.Crear,0) AS Crear,ISNULL(PermisoRol.Actualizar,0) AS Actualizar,ISNULL(PermisoRol.Consultar,0) AS Consultar,ISNULL(PermisoRol.Cancelar,0) AS Cancelar " & _
            "FROM Permiso " & _
            "LEFT JOIN AdminRolePermiso AS PermisoRol ON PermisoRol.PermisoId=Permiso.PermisoID AND PermisoRol.AdminRoleID={0}"
        Dim queryEjecucion As String = String.Format(query, idRol)
        Dim tablaPermisos As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucion, CommandType.Text, "Permisos", connStringWeb)

        Dim listaPermisosRol As List(Of PermisoRolUsuario) = New List(Of PermisoRolUsuario)()
        For i As Integer = 0 To tablaPermisos.Rows.Count - 1
            Dim permiso As PermisoRolUsuario = New PermisoRolUsuario()
            permiso.PermisoID = tablaPermisos.Rows(i)("PermisoID")
            permiso.NombrePermiso = tablaPermisos.Rows(i)("NombrePermiso")
            permiso.TieneCrear = tablaPermisos.Rows(i)("TieneCrear")
            permiso.TieneActualizar = tablaPermisos.Rows(i)("TieneActualizar")
            permiso.TieneConsultar = tablaPermisos.Rows(i)("TieneConsultar")
            permiso.TieneCancelar = tablaPermisos.Rows(i)("TieneCancelar")
            permiso.Crear = tablaPermisos.Rows(i)("Crear")
            permiso.Actualizar = tablaPermisos.Rows(i)("Actualizar")
            permiso.Consultar = tablaPermisos.Rows(i)("Consultar")
            permiso.Cancelar = tablaPermisos.Rows(i)("Cancelar")
            listaPermisosRol.Add(permiso)

        Next
        Return listaPermisosRol
    End Function

End Class
