﻿Public Class Usuario
    Public Property AdminUserID As Integer
    Public Property FirstName As String
    Public Property LastName As String
    Public Property RoleName As String
    Public Property AdminRoleID As Integer
    Public Property NTUserAccount As String
    Public Property NTUserDomain As String
    Public Property Status As String
    Public Property StatusName As String
    Public Property EmployeeNum As String
    Public Property PositionEmployee As String
    Public Property EsVendedor As Boolean
    Public Property TiendasAsignadas As List(Of TiendaAsignadaUsuario)
    Public ReadOnly Property FullName As String
        Get
            Return FirstName & " " & LastName
        End Get
    End Property

    Public Property TodosLosFactores As Boolean
        Get
            Return sFactores = "1,1,1,1,1"
        End Get
        Set(value As Boolean)
            If value = True Then
                sFactores = "1,1,1,1,1"
            Else
                sFactores = "0,0,1,1,0"
            End If
        End Set
    End Property

    Private sFactores As String
    Public Property Factores As String
        Get
            Return sFactores
        End Get
        Set(value As String)
            sFactores = value
        End Set
    End Property


End Class
