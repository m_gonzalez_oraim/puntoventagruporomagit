﻿Imports NLog

Public Class ControlUsuario
    Shared logger As NLog.Logger = LogManager.GetCurrentClassLogger()

    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Function EliminarUsuario(ByVal idUsuario As Integer, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryUsuarioSuperUsuario As String = "SELECT ISNULL(SuperUsuario,'N') AS SuperUsuario FROM AdminUser WHERE AdminUserID=" & idUsuario
        Dim resultadosSuperUsuario As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryUsuarioSuperUsuario, CommandType.Text, "Usuario", connStringWeb)
        If resultadosSuperUsuario.Rows(0)(0) = "Y" Then
            mensajeError = "No se puede eliminar el superusuario"
            Return False
        End If

        Dim queryEliminarRol As StringBuilder = New StringBuilder()
        queryEliminarRol.AppendLine("DELETE FROM ADminUser WHERE AdminUserID=" & idUsuario)
        queryEliminarRol.AppendLine("DELETE FROM UserStores WHERE AdminUserID=" & idUsuario)
        Dim registros As Integer = oDb.EjecutaQry(queryEliminarRol.ToString(), CommandType.Text, connStringWeb)
        If registros <> 1 Then
            mensajeError = "Error al eliminar el usuario. Consulte con su administrador"
            Return False
        End If
        Return True
    End Function

    Private Function NombreCuentaUsuarioCumpleRegla(ByVal cuentaUsuario As String) As Boolean
        Return True
    End Function

    Private Function ContraseñaCuentaUsuarioCumpleRegla(ByVal contraseña As String) As Boolean
        Return True
    End Function

    Public Function CrearUsuario(ByRef usuario As Usuario, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryExisteCuentaUsuario As String = "SELECT COUNT(AdminUserID) FROM AdminUser WHERE NTUserAccount='" & usuario.NTUserAccount & "'"
        Dim tablaExisteCuenta As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryExisteCuentaUsuario, CommandType.Text, "Usuarios", connStringWeb)
        If tablaExisteCuenta.Rows(0)(0) > 0 Then
            mensajeError = "Nombre cuenta de usuario ya se encuentra registrada en el sistema"
            Return False
        End If

        If Not NombreCuentaUsuarioCumpleRegla(usuario.NTUserAccount) Then
            mensajeError = "Nombre cuenta de usuario no cumple con las reglas de definicion de cuentas"
            Return False
        End If

        If Not ContraseñaCuentaUsuarioCumpleRegla(usuario.NTUserDomain) Then
            mensajeError = "Contraseña cuenta usuario no cumple con las reglas de definicion de cuentas"
            Return False
        End If

        Dim queryInsertarUsuario As StringBuilder = New StringBuilder()
        queryInsertarUsuario.Append("DECLARE @idUsuario AS Integer " & Chr(13))
        queryInsertarUsuario.Append("INSERT INTO AdminUser(FirstName,LastName,AdminRoleID,NTUserAccount,NTUserDomain,Status,EmployeeNum,PositionEmployee,Factores,EsVendedor) VALUES ( '" & usuario.FirstName & "', '" & usuario.LastName & "'," & usuario.AdminRoleID & ",'" & usuario.NTUserAccount & "','" & usuario.NTUserDomain & "','" & usuario.Status & "','" & usuario.EmployeeNum & "','" & usuario.PositionEmployee & "','" & usuario.Factores & "','" & IIf(usuario.EsVendedor, "Y", "N") & "') " & Chr(13))
        queryInsertarUsuario.Append("SET @idUsuario=(SELECT @@IDENTITY)" & Chr(13))
        For i As Integer = 0 To usuario.TiendasAsignadas.Count - 1
            queryInsertarUsuario.Append(" INSERT INTO UserStores (AdminStoreID,AdminUserID,AdminStoreToSendID) VALUES(" & IIf(usuario.TiendasAsignadas(i).UserAdminStoreID = -1, "NULL", usuario.TiendasAsignadas(i).UserAdminStoreID) & ", @idUsuario ," & IIf(usuario.TiendasAsignadas(i).UserAdminStoreToSendID = -1, "NULL", usuario.TiendasAsignadas(i).UserAdminStoreToSendID) & ")" & Chr(13))
        Next

        If oDb.EjecutaQry(queryInsertarUsuario.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If
        mensajeError = "Error al crear el usuario. Consulte con el administrador"
        Return False
    End Function

    Public Function ActualizarUsuario(ByRef usuario As Usuario, ByRef mensajeError As String) As Boolean
        mensajeError = String.Empty

        Dim queryExisteCuentaUsuario As String = "SELECT COUNT(AdminUserID) FROM AdminUser WHERE NTUserAccount='" & usuario.NTUserAccount & "' AND AdminUserId<>" & usuario.AdminUserID
        Dim tablaExisteCuenta As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryExisteCuentaUsuario, CommandType.Text, "Usuarios", connStringWeb)
        If tablaExisteCuenta.Rows(0)(0) > 0 Then
            mensajeError = "Nombre cuenta de usuario ya se encuentra registrada en el sistema"
            Return False
        End If

        If Not NombreCuentaUsuarioCumpleRegla(usuario.NTUserAccount) Then
            mensajeError = "Nombre cuenta de usuario no cumple con las reglas de definicion de cuentas"
            Return False
        End If

        Dim queryActualizarUsuario As StringBuilder = New StringBuilder()
        queryActualizarUsuario.AppendLine("DELETE FROM UserStores WHERE AdminUserID=" & usuario.AdminUserID & Chr(13))
        queryActualizarUsuario.AppendLine("UPDATE AdminUser SET FirstName='" & usuario.FirstName & "', LastName='" & usuario.LastName & "', AdminRoleID='" & usuario.AdminRoleID & "',NTUserAccount = '" & usuario.NTUserAccount & "', Status='" & usuario.Status & "',EmployeeNum='" & usuario.EmployeeNum & "',PositionEmployee='" & usuario.PositionEmployee & "',Factores='" & usuario.Factores & "',EsVendedor='" & IIf(usuario.EsVendedor, "Y", "N") & "' WHERE AdminUserId=" & usuario.AdminUserID & " " & Chr(13))

        For i As Integer = 0 To usuario.TiendasAsignadas.Count - 1
            queryActualizarUsuario.AppendLine(" INSERT INTO UserStores (AdminStoreID,AdminUserID,AdminStoreToSendID) VALUES(" & IIf(usuario.TiendasAsignadas(i).UserAdminStoreID = -1, "null", usuario.TiendasAsignadas(i).UserAdminStoreID) & ", " & usuario.AdminUserID & "," & IIf(usuario.TiendasAsignadas(i).UserAdminStoreToSendID = -1, "null", usuario.TiendasAsignadas(i).UserAdminStoreToSendID) & ")")
        Next

        If oDb.EjecutaQry(queryActualizarUsuario.ToString(), CommandType.Text, connStringWeb) = 1 Then
            Return True
        End If

        Return False
    End Function

    Public Sub InformacionUsuarioFromRequest(ByRef request As System.Web.HttpRequest, ByRef usuario As Usuario)
        Dim nombreUsuario As String = request("txtFirstName")
        Dim apellidoUsuario As String = request("txtLastName")
        Dim idRol As Integer = request("cmbAdminRoleID")
        Dim numeroEmpleado As String = request("txtEmployeeNum")
        Dim puestoEmpleado As String = request("txtPuesto")
        Dim esVendedor As Boolean = True
        If request("chkEsVendedor") Is Nothing Then
            esVendedor = False
        End If
        Dim usuarioCuenta As String = request("txtNTUserAccount")
        Dim contraseñaCuenta As String = request("txtNTUserDomain")
        Dim estado As String = request("cmbStatus")
        Dim todosLosFactores As Boolean = True
        If request("chkFactoresAll") Is Nothing Then
            todosLosFactores = False
        End If

        Dim numeroTiendas As Integer = request("hdnNumeroTiendas")

        usuario = New Usuario()
        usuario.FirstName = nombreUsuario
        usuario.LastName = apellidoUsuario
        usuario.AdminRoleID = idRol
        usuario.EmployeeNum = numeroEmpleado
        usuario.PositionEmployee = puestoEmpleado
        usuario.EsVendedor = esVendedor
        usuario.NTUserAccount = usuarioCuenta
        usuario.NTUserDomain = contraseñaCuenta
        usuario.Status = estado
        usuario.TodosLosFactores = todosLosFactores
        If Not request("hdnIdUsuario") Is Nothing Then
            usuario.AdminUserID = request("hdnIdUsuario")
        End If

        usuario.TiendasAsignadas = New List(Of TiendaAsignadaUsuario)
        For i As Integer = 0 To numeroTiendas - 1
            Dim tiendaAsignada As TiendaAsignadaUsuario = informacionTiendaFromRequest(i, request)
            If Not tiendaAsignada Is Nothing Then
                usuario.TiendasAsignadas.Add(tiendaAsignada)
            End If
        Next


    End Sub

    Private Function informacionTiendaFromRequest(ByVal numeroTienda As Integer, ByRef request As System.Web.HttpRequest) As TiendaAsignadaUsuario

        Dim tiendaAsignada As TiendaAsignadaUsuario = New TiendaAsignadaUsuario()
        tiendaAsignada.UserAdminStoreID = -1
        tiendaAsignada.UserAdminStoreToSendID = -1
        Dim seSeleccionoTienda As Boolean = False

        If Not request("chkAcceso" & numeroTienda) Is Nothing Then
            tiendaAsignada.UserAdminStoreID = request("hdnTienda" & numeroTienda)
            seSeleccionoTienda = True
        End If
        If Not request("chkTrans" & numeroTienda) Is Nothing Then
            tiendaAsignada.UserAdminStoreToSendID = request("hdnTienda" & numeroTienda)
            seSeleccionoTienda = True
        End If

        If Not seSeleccionoTienda Then
            Return Nothing
        Else
            Return tiendaAsignada
        End If

    End Function

    Public Function ListadoUsuarios() As List(Of Usuario)
        Dim query As String = "SELECT AdminUser.AdminUserId,AdminUser.FirstName,AdminUser.LastName,AdminRole.RoleName, AdminUser.Status AS StatusName FROM AdminUser INNER JOIN AdminRole ON AdminRole.AdminRoleID=AdminUser.AdminRoleID "
        Dim resultados As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "AdminRole", connStringWeb)

        Dim usuarios As List(Of Usuario) = New List(Of Usuario)()

        For i As Integer = 0 To resultados.Rows.Count - 1
            Dim auxUsuario As Usuario = New Usuario()
            auxUsuario.AdminUserID = resultados.Rows(i)("AdminUserId")
            auxUsuario.FirstName = resultados.Rows(i)("FirstName")
            auxUsuario.LastName = resultados.Rows(i)("LastName")
            auxUsuario.RoleName = resultados.Rows(i)("RoleName")
            auxUsuario.StatusName = resultados.Rows(i)("StatusName")
            usuarios.Add(auxUsuario)
        Next

        Return usuarios
    End Function

    Public Function InformacionUsuario(ByVal idUsuario As Integer) As Usuario
        Dim query As String = "SELECT FirstName,LastName,AdminRoleID,NTUserAccount,Status,EmployeeNum,PositionEmployee,Factores,EsVendedor FROM AdminUser WHERE AdminUserID={0}"
        Dim queryEjecucion As String = String.Format(query, idUsuario)

        Dim usuarioData As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucion, CommandType.Text, "Rol", connStringWeb)
        If usuarioData.Rows.Count <> 1 Then
            Return Nothing
        End If

        Dim usuario As Usuario = New Usuario()
        usuario.FirstName = usuarioData.Rows(0)("FirstName")
        usuario.LastName = usuarioData.Rows(0)("LastName")
        usuario.AdminRoleID = usuarioData.Rows(0)("AdminRoleID")
        usuario.NTUserAccount = usuarioData.Rows(0)("NTUserAccount")
        usuario.Status = usuarioData.Rows(0)("Status")
        usuario.EmployeeNum = usuarioData.Rows(0)("EmployeeNum")
        usuario.PositionEmployee = usuarioData.Rows(0)("PositionEmployee")
        usuario.Factores = usuarioData.Rows(0)("Factores")
        usuario.EsVendedor = usuarioData.Rows(0)("EsVendedor") = "Y"

        usuario.TiendasAsignadas = TiendasAsignadasUsuario(idUsuario, False)

        Return usuario
    End Function

    Public Function TiendasAsignadasUsuario(ByVal idUsuario As Integer, ByVal paraCreacion As Boolean) As List(Of TiendaAsignadaUsuario)
        Dim query As String = _
            "SELECT AdminStore.AdminStoreID,AdminStore.StoreName,ISNULL(UserStores.AdminStoreID,-1) AS UserAdminStoreID,ISNULL(UserStores2.AdminStoreToSendID,-1) AS UserAdminStoreToSendID ,AdminStore.Status " & _
            "FROM AdminStore " & _
            "LEFT JOIN UserStores ON AdminStore.AdminStoreID=UserStores.AdminStoreID AND UserStores.AdminUserID={0} " & _
            "LEFT JOIN UserStores AS UserStores2 ON AdminStore.AdminStoreID=UserStores2.AdminStoreToSendID AND UserStores2.AdminUserID={0} "

        If paraCreacion Then
            query = query + " WHERE AdminStore.Status='A' "
        End If

        Dim queryEjecucion As String = String.Format(query, idUsuario)
        Dim tablaTiendas As System.Data.DataTable = oDb.EjecutaQry_Tabla(queryEjecucion, CommandType.Text, "Tiendas", connStringWeb)

        Dim tiendasAsignadasAlUsuario As List(Of TiendaAsignadaUsuario) = New List(Of TiendaAsignadaUsuario)()
        For i As Integer = 0 To tablaTiendas.Rows.Count - 1
            Dim tiendaAsignada As TiendaAsignadaUsuario = New TiendaAsignadaUsuario()
            tiendaAsignada.StoreName = tablaTiendas.Rows(i)("StoreName")
            tiendaAsignada.AdminStoreID = tablaTiendas.Rows(i)("AdminStoreID")
            tiendaAsignada.UserAdminStoreID = tablaTiendas.Rows(i)("UserAdminStoreID")
            tiendaAsignada.UserAdminStoreToSendID = tablaTiendas.Rows(i)("UserAdminStoreToSendID")
            tiendaAsignada.Status = tablaTiendas.Rows(i)("Status")
            tiendasAsignadasAlUsuario.Add(tiendaAsignada)

        Next
        Return tiendasAsignadasAlUsuario
    End Function

    Public Function TieneAcceso(ByVal usuario As String, ByVal modulo As String, ByVal permiso As enumTipoAccesoModulo)
        Return True
    End Function

    Public Function PuedeModificarFechaDocumento(ByVal usuario As String) As Boolean
        Return False
    End Function

    Public Function FactoresUsuario(ByVal usuario As Integer) As String
        Dim query As String = "SELECT Factores FROM AdminUser WHERE AdminUserId=" & usuario
        Try
            Dim tablaFactores As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Factores", connStringWeb)
            Return tablaFactores.Rows(0)("Factores")
        Catch ex As Exception
            logger.ErrorException("FactoresUsuario", ex)
            Throw ex
        End Try
    End Function

End Class
