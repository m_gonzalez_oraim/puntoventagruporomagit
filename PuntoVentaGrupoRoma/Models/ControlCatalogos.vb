﻿Public Class ControlCatalogos
    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Public Function CodigosEstado() As Dictionary(Of String, String)
        Dim queryEstados As String = "SELECT COD_POS AS Codigo,DES_REGION AS Nombre from ge_regiones order by DES_REGION"
        Dim tablaEstados = oDb.EjecutaQry_Tabla(queryEstados, CommandType.Text, "Estados", connStringWeb)

        Dim estados As Dictionary(Of String, String) = New Dictionary(Of String, String)()
        For i As Integer = 0 To tablaEstados.Rows.Count - 1
            estados.Add(tablaEstados.Rows(i)("Codigo"), tablaEstados.Rows(i)("Nombre"))
        Next
        Return estados

    End Function



    Public Function CodigosImpuesto() As Dictionary(Of String, String)
        Dim query As String = "SELECT COD_IVA,PORCENTAJE FROM IVA"

        Dim impuestos As Dictionary(Of String, String) = New Dictionary(Of String, String)()

        Dim tablaImpuestos As System.Data.DataTable = oDb.EjecutaQry_Tabla(query, CommandType.Text, "Impuesto", connStringWeb)
        For i As Integer = 0 To tablaImpuestos.Rows.Count - 1
            impuestos.Add(tablaImpuestos.Rows(i)("COD_IVA"), tablaImpuestos.Rows(i)("PORCENTAJE"))
        Next
        Return impuestos

    End Function

End Class
