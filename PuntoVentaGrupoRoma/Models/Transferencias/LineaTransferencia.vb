﻿Public Class LineaTransferencia

    Property Linea As Integer
    Property CodigoArticulo As String
    Property NombreArticulo As String
    Property CantidadTransferida As Decimal
    Property LineaSolicitudBase As Integer
    Property SolicitudBase As Integer
    Property UnidadMedida As String
    Property FactorUnidadMedida As Integer
    Property Status As Char
    Property ExistenciaTiendaTransfiere As Decimal

End Class
    