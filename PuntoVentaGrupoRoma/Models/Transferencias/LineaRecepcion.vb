﻿Public Class LineaRecepcion

    Property Linea As Integer
    Property CodigoArticulo As String
    Property NombreArticulo As String
    Property CantidadRecibida As Decimal
    Property LineaTransferenciaBase As Integer
    Property TransferenciaBase As Integer
    Property UnidadMedida As String
    Property FactorUnidadMedida As Integer
    Property Status As Char
    Property ExistenciaTiendaRecibe As Decimal

End Class
    