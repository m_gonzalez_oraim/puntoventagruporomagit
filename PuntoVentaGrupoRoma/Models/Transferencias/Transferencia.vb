﻿Public Class Transferencia

    Property Fecha As DateTime
    Property Status As Char
    Property IDUsuario As Integer
    Property Importado As Boolean
    Property FechaImportado As DateTime
    Property CodigoError As Boolean
    Property DetalleError As String
    Property CodigoTiendaSurte As Integer
    Property TransferirAAlmacenTransferencia As Boolean
    Property Lineas As List(Of LineaTransferencia)

End Class
