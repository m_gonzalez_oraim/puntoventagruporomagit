﻿Public Class Solicitud

    Property Fecha As DateTime
    Property Status As Char
    Property IDUsuario As Integer
    Property Importado As Boolean
    Property FechaImportado As DateTime
    Property CodigoError As Boolean
    Property DetalleError As String
    Property CodigoTiendaSolicita As Integer
    Property CodigoTiendaSurte As Integer
    Property Lineas As List(Of LineaSolicitud)

    Private Shared Function LineaSolicitudFromRequest(ByVal idLinea As String, ByRef request As System.Web.HttpRequest, ByVal i As Integer) As LineaSolicitud
        Dim nombreArticulo As String = request("txtNombreArticulo" & idLinea)
        Dim codigoArticulo As String = request("txtCodigoArticulo" & idLinea)
        Dim factor As String = request("cmbUnidadMedida" & idLinea)
        Dim unidadMedida As String = request("hdnUnidadMedida" & idLinea)
        Dim cantidad As Decimal = request("txtCantidad" & idLinea)
        'cambiar el valor de unidad de medida cuando cambie el valor del combo

        Dim linea As LineaSolicitud = New LineaSolicitud()
        linea.Linea = i + 1
        linea.CodigoArticulo = codigoArticulo
        linea.NombreArticulo = nombreArticulo
        linea.Cantidad = cantidad
        linea.UnidadMedida = unidadMedida
        linea.FactorUnidadMedida = factor


        Return linea
    End Function

    Public Shared Function InformacionSolicitudFromRequest(ByVal tiendaSurte As Integer, ByVal idUsuario As Integer, ByRef request As System.Web.HttpRequest) As Solicitud
        Dim fecha As DateTime = DateTime.Now
        Dim tiendaSolicita As Integer = request("hdnStore")
        Dim usuario As Integer = idUsuario

        Dim idsLineas As String = request("hdnIdLinea")
        'Validar si vienen separados por como o por punto y coma
        Dim listadosIds As String() = idsLineas.Split(",")
        Dim lineasSolicitud As List(Of LineaSolicitud) = New List(Of LineaSolicitud)()
        For i As Integer = 0 To listadosIds.Length - 1
            lineasSolicitud.Add(LineaSolicitudFromRequest(listadosIds(i), request, i))
        Next

        Dim solicitud As Solicitud = New Solicitud()
        solicitud.Fecha = fecha
        solicitud.IDUsuario = usuario
        solicitud.CodigoTiendaSolicita = tiendaSolicita
        solicitud.CodigoTiendaSurte = tiendaSurte
        solicitud.Lineas = lineasSolicitud

        Return solicitud
    End Function


End Class
