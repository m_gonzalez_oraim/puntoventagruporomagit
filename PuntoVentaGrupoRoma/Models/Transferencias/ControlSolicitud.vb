﻿Imports System.Globalization
Public Class ControlSolicitud

    Private oDb As DBMaster = New DBMaster()
    Private connStringWeb = ConfigurationManager.ConnectionStrings("DBConn").ConnectionString

    Private ultimoMensajeError As String

    Public Function ObtenerSolicitud(ByVal idSolicitud As Integer) As Solicitud

    End Function

    Public Function CrearSolicitud(ByVal solicitud As Solicitud) As Boolean

        Dim queryInsertarRol As StringBuilder = New StringBuilder()
        Try
            queryInsertarRol.AppendLine("set dateformat dmy")
            queryInsertarRol.AppendLine("DECLARE @idSolicitud INT")
            queryInsertarRol.AppendLine("INSERT INTO SolicitudEncabezado (FechaRegistro, FechaSolicitud, Status, IDUsuario, Importado, FechaImportado, Error, DetalleError, TiendaOrigen ,TiendaDestino)" & vbCrLf & _
                    "VALUES (GETDATE(), '" & solicitud.Fecha.ToString("dd-MM-yyyy") & "','O','" & solicitud.IDUsuario & "', 0, NULL, NULL, NULL, '" & solicitud.CodigoTiendaSurte & "', '" & solicitud.CodigoTiendaSolicita & "')")
            queryInsertarRol.AppendLine("SET @idSolicitud = (SELECT @@IDENTITY)")
            'Obtenemos el id del documento de salida creado en web
            For Each row In solicitud.Lineas
                queryInsertarRol.AppendLine("INSERT INTO SolicitudDetalle (IDSolicitud, Linea, Articulo, NombreArticulo, Cantidad, CantidadEntregada, UnidadMedida, FactorUnidadMedida,	Status) " & vbCrLf & _
                    "VALUES (@idSolicitud," & row.Linea & ",'" & row.CodigoArticulo & "','" & row.NombreArticulo & _
                    "'," & row.Cantidad & ",0,'" & row.UnidadMedida & "'," & row.FactorUnidadMedida & ", 'O')")
            Next

            If oDb.EjecutaQry(queryInsertarRol.ToString(), CommandType.Text, connStringWeb) = 1 Then
                Return True
            End If

            Return False
        Catch ex As Exception
            ultimoMensajeError = ex.Message
            Return False
        End Try
    End Function

    Public Function ActualizarSolicitud(ByVal solicitud As Solicitud) As Boolean

    End Function

    Public Function CancelarSolicitud(ByVal idSolicitud As Integer) As Boolean

    End Function

    Public Function BuscarSolicitud(ByVal tiendaSolicita As String,
                                    ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime,
                                    ByVal tiendaSurteDesde As String, ByVal tiendaSurteHasta As String,
                                    ByVal articuloDesde As String, ByVal articuloHasta As String,
                                    ByVal pagina As Integer, ByVal numeroRegistrosPorPagina As Integer, ByVal columnaOrdenamiento As String,
                                    ByVal ordenAscendente As Boolean) As List(Of Solicitud)



    End Function

    Public Function SePuedeCancelarSolicitud(ByVal idSolicitud As Integer) As Boolean

    End Function

    Public Function SePuedeModificarSolicitud(ByVal idSolicitud As Integer) As Boolean

    End Function

    Public Function InformacionSolicitudArticulo(codigoArticulo As String, ByRef unidadesMedida As List(Of UnidadMedida)) As Boolean
        Dim controlArticulo As ControlArticulos = New ControlArticulos()
        unidadesMedida = Nothing
        unidadesMedida = controlArticulo.UnidadesMedidaArticulo("Inventario", codigoArticulo)

        Return True
    End Function

    Public Function BuscarSolicitud(ByVal tiendaSolicita As Integer,
                                    ByVal fechaDesde As DateTime, ByVal fechaHasta As DateTime,
                                    ByVal tiendaRecibeDesde As String, ByVal tiendaRecibeHasta As String,
                                    ByVal articuloDesde As String, ByVal articuloHasta As String,
                                    ByVal pagina As Integer, ByVal columnaOrdenamiento As String, ByVal ordenAscendente As Boolean,
                                    ByVal numeroRegistrosBusqueda As Integer,
                                    ByVal obtenTotalRegistros As Boolean, ByRef numeroRegistros As Integer) As List(Of Transferencia)
    End Function

End Class
