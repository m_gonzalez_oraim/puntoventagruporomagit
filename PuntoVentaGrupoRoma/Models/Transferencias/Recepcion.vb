﻿Public Class Recepcion

    Property Fecha As DateTime
    Property Status As Char
    Property IDUsuario As Integer
    Property Importado As Boolean
    Property FechaImportado As DateTime
    Property CodigoError As Boolean
    Property DetalleError As String
    Property CodigoTiendaSolicita As Integer
    Property CodigoTiendaSurte As Integer
    Property Lineas As List(Of LineaSolicitud)

End Class
