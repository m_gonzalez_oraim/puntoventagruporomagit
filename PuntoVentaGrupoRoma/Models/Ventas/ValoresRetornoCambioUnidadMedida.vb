﻿Public Class ValoresRetornoCambioUnidadMedida

    Public Property Factores As List(Of Factor)
    Public Property PrecioMinimoAutorizacion As Decimal
    Public Property PrecioMaximoAutorizacion As Decimal
    Public Property PrecioPrimerFactor As Decimal
    Public Property PrecioPrimerFactorSinImpuestos As Decimal
    Public Property ImporteLinea As Decimal
    Public Property ImporteLineaImpuesto As Decimal
    Public Property Impuestos As Decimal
    Public Property PrecioSinImpuesto As Decimal
    Public Property SubTotal As Decimal
    Public Property SubTotal2 As Decimal
    Public Property Total As Decimal
    Public Property Cambio As Decimal

End Class
