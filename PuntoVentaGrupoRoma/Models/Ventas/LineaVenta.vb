﻿Public Class LineaVenta
    Property Id As String
    Property CodigoArticulo As String
    Property FactorArticulo As String
    Property NombreArticulo As String
    Property SujetoAImpuesto As Boolean
    Property UnidadDeMedida As UnidadMedida
    Property UnidadesDeMedida As List(Of UnidadMedida)
    Property Factores As List(Of Factor)
    Property Cantidad As Decimal
    Property Precio As Decimal
    Property PrecioConImpuesto As Decimal
    Property Descuento As Decimal
    Property TotalLinea As Decimal
    Property TotalLineaImpuestos As Decimal
    Property PrecioMinimoAutorizacion As Decimal
    Property PrecioMaximoAutorizacion As Decimal
    Property PrecioCosto As Decimal
    Property Comentarios As String
    Property PuedeElegirFactor As Boolean
    Property PuedeModificarPrecio As Boolean

End Class
