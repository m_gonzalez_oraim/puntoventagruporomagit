﻿Imports System.Data.SqlClient

Public Class DBMaster
#Region "Variables de coneccion SQL"

    Private sServer As String
    Private sDB As String
    Private sUser As String
    Private sPass As String
    Private bTrusted As Boolean

#End Region

#Region "Objetos para interaccion SQL"

    Private BD_SQL As SqlConnection
    Private cmd_SQL As SqlCommand
    Private dta_SQL As SqlDataAdapter
    Private dts_SQL As DataSet
    Public dtc_SQL() As DataTableCollection
    Public int_SQL As Integer

#End Region

#Region "Propiedades"

    Public Property Server()
        Get
            Server = sServer
        End Get
        Set(ByVal value)
            sServer = value
        End Set
    End Property

    Public Property DB()
        Get
            DB = sDB
        End Get
        Set(ByVal value)
            sDB = value
        End Set
    End Property

    Public Property User()
        Get
            User = sUser
        End Get
        Set(ByVal value)
            sUser = value
        End Set
    End Property

    Public Property Pass()
        Get
            Pass = sPass
        End Get
        Set(ByVal value)
            sPass = value
        End Set
    End Property

    Public Property TrustedConection()
        Get
            TrustedConection = bTrusted
        End Get
        Set(ByVal value)
            bTrusted = value
        End Set
    End Property

#End Region

#Region "Enums"

    Public Enum Visible
        NO = 0
        SI = 1
    End Enum

    Public Enum Editable
        NO = 0
        SI = 1
    End Enum

#End Region

    Public Function GetConnString() As String
        ' ******************************************************************************
        ' Crear cadena de Conexión
        ' ******************************************************************************
        Try

            If Me.TrustedConection = False Then
                GetConnString = "Data Source=" & Trim(sServer) & ";Initial Catalog=" & Trim(sDB) & ";" _
                            & "User ID=" & Trim(sUser) & ";Password=" & Trim(sPass) & ";" _
                            & "Persist Security Info=True; Packet Size=4096"
            Else
                GetConnString = "Data Source=" & Trim(sServer) & ";Initial Catalog=" & Trim(sDB) & ";Integrated Security=True"
            End If

        Catch ex As Exception

        End Try

    End Function

    Public Function ConectaDBConnString(ByVal conn As String) As Boolean

        ' ******************************************************************************
        ' Función para realizar conexiones a BD SQL
        ' ******************************************************************************

        Dim strConexion = ""
        ' ******************************************************************************
        ' Crear cadena de Conexión
        ' ******************************************************************************
        strConexion = conn
        ' ******************************************************************************
        ' Crea una nueva conexión
        ' ******************************************************************************
        BD_SQL = New SqlConnection

        Try
            ' ******************************************************************************
            ' Establece la ruta de la BD
            ' ******************************************************************************
            BD_SQL.ConnectionString = strConexion

            ' ******************************************************************************
            ' Abre la conexión
            ' ******************************************************************************
            BD_SQL.Open()

            Me.DB = BD_SQL.Database


            ' ******************************************************************************
            ' Verificar si se realizo la conexion  
            ' ******************************************************************************
            If BD_SQL.State = ConnectionState.Closed Then
                Throw New Exception("No se realizo la conexión a la BD")
            End If

            Me.Server = BD_SQL.ConnectionString.Split(";")(0).Split("=")(1)


        Catch ex As Exception
            ConectaDBConnString = False
            If CStr(Err.Number) <> "-2147217843" Then
                Throw New Exception("Error de conexión a la BD." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description)
            ElseIf CStr(Err.Number) = 5 Then
                Throw New Exception("Error la contraseña a expirado." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description)
            End If
        End Try

        ConectaDBConnString = True


    End Function
    Public Function ConectaBD(ByVal NombreServidor As String, ByVal NombreBaseDatos As String, ByVal Usuario As String, ByVal Password As String, Optional ByVal sErr As String = "") As Boolean
        ' ******************************************************************************
        ' Función para realizar conexiones a BD SQL
        ' ******************************************************************************

        Dim strConexion
        ' ******************************************************************************
        ' Crear cadena de Conexión
        ' ******************************************************************************
        strConexion = "Data Source=" & Trim(NombreServidor) & ";Initial Catalog=" & Trim(NombreBaseDatos) & ";" _
                    & "User ID=" & Trim(Usuario) & ";Password=" & Trim(Password) & ";" _
                    & "Persist Security Info=True; Packet Size=4096"

        ' ******************************************************************************
        ' Crea una nueva conexión
        ' ******************************************************************************
        BD_SQL = New SqlConnection

        Try
            ' ******************************************************************************
            ' Establece la ruta de la BD
            ' ******************************************************************************
            BD_SQL.ConnectionString = strConexion

            ' ******************************************************************************
            ' Abre la conexión
            ' ******************************************************************************
            BD_SQL.Open()

            ' ******************************************************************************
            ' Verificar si se realizo la conexion  
            ' ******************************************************************************
            If BD_SQL.State = ConnectionState.Closed Then
                Throw New Exception("No se realizo la conexión a la BD")
            End If

        Catch ex As Exception
            ConectaBD = False
            If CStr(Err.Number) <> "-2147217843" Then
                sErr = "Error de conexión a la BD." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description
            ElseIf CStr(Err.Number) = 5 Then
                sErr = "Error la contraseña a expirado." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description
            End If
        End Try

        ConectaBD = True
    End Function

    Public Function ConectaBD(Optional ByRef sErr As String = "") As Boolean
        ' ******************************************************************************
        ' Función para realizar conexiones a BD SQL
        ' ******************************************************************************

        Dim strConexion
        ' ******************************************************************************
        ' Crear cadena de Conexión
        ' ******************************************************************************
        If Me.TrustedConection = False Then
            strConexion = "Data Source=" & Trim(sServer) & ";Initial Catalog=" & Trim(sDB) & ";" _
                        & "User ID=" & Trim(sUser) & ";Password=" & Trim(sPass) & ";" _
                        & "Persist Security Info=True; Packet Size=4096"
        Else
            strConexion = "Data Source=" & Trim(sServer) & ";Initial Catalog=" & Trim(sDB) & ";Integrated Security=True"
        End If

        ' ******************************************************************************
        ' Crea una nueva conexión
        ' ******************************************************************************
        BD_SQL = New SqlConnection

        Try
            ' ******************************************************************************
            ' Establece la ruta de la BD
            ' ******************************************************************************
            BD_SQL.ConnectionString = strConexion

            ' ******************************************************************************
            ' Abre la conexión
            ' ******************************************************************************
            BD_SQL.Open()

            ' ******************************************************************************
            ' Verificar si se realizo la conexion  
            ' ******************************************************************************
            If BD_SQL.State = ConnectionState.Closed Then
                Throw New Exception("No se realizo la conexión a la BD")
            End If

        Catch ex As Exception
            ConectaBD = False
            If CStr(Err.Number) <> "-2147217843" Then
                sErr = "Error de conexión a la BD." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description
            ElseIf CStr(Err.Number) = 5 Then
                sErr = "Error la contraseña a expirado." & vbCrLf & "Error: " & CStr(Err.Number) & " " & Err.Description
            End If
        End Try

        ConectaBD = True
    End Function

    Public Function CierraBD() As Boolean
        ' ******************************************************************************
        ' Función para desconexion de BD SQL
        ' ******************************************************************************
        'Cierra la conexión
        If BD_SQL.State = 1 Then
            BD_SQL.Close()
            BD_SQL.Dispose()
            BD_SQL = Nothing
            GC.Collect()
        End If
        CierraBD = True
        Exit Function
    End Function

    Public Function EjecuteQryObtenAutonumeric(ByVal sQuery As String, ByVal sTipoAccion As CommandType, ByVal sConn As String, ByRef identificador As Long) As Integer
        Dim codigoEjecucion As Integer = 0

        Try
            ' ******************************************************************************
            ' Conexion a BD
            ' ******************************************************************************
            Dim sErr As String = ""

            ConectaDBConnString(sConn)

            If sErr <> "" Then
                MsgBox(sErr)
            End If

            If BD_SQL.State <> ConnectionState.Open Then Exit Function

            ' ******************************************************************************
            ' Inicializa comando
            ' ******************************************************************************
            cmd_SQL = New SqlCommand

            cmd_SQL.CommandTimeout = 0
            cmd_SQL.CommandText = sQuery
            cmd_SQL.CommandType = sTipoAccion
            cmd_SQL.Connection = BD_SQL

            ' ******************************************************************************
            ' Si el resultado pasa al Recorset
            ' ******************************************************************************
            cmd_SQL.ExecuteNonQuery()

            Dim query As String = "SELECT @@IDENTITY"
            cmd_SQL.CommandText = query
            identificador = cmd_SQL.ExecuteScalar()
            ' ******************************************************************************
            ' Quita Obj de Memoria
            ' ******************************************************************************
            cmd_SQL.Dispose()
            CierraBD()

            Return 1
        Catch ex As Exception
            Return 2
        End Try

    End Function
    Public Function EjecutaQry(ByVal stNmbSentencia As String, _
                                    ByVal sTipoAccion As CommandType, ByVal sconn As String, Optional ByRef sDError As String = "") As Integer
        ' ******************************************************************************
        ' Ejecuta Sentencias BD SQL que NO Extraen Datos
        ' ******************************************************************************
        EjecutaQry = 0
        Try
            ' ******************************************************************************
            ' Conexion a BD
            ' ******************************************************************************
            Dim sErr As String = ""

            ConectaDBConnString(sconn)

            If sErr <> "" Then
                MsgBox(sErr)
            End If

            If BD_SQL.State <> ConnectionState.Open Then Exit Function

            ' ******************************************************************************
            ' Inicializa comando
            ' ******************************************************************************
            cmd_SQL = New SqlCommand

            cmd_SQL.CommandTimeout = 0
            cmd_SQL.CommandText = stNmbSentencia
            cmd_SQL.CommandType = sTipoAccion
            cmd_SQL.Connection = BD_SQL

            ' ******************************************************************************
            ' Si el resultado pasa al Recorset
            ' ******************************************************************************
            cmd_SQL.ExecuteNonQuery()

            ' ******************************************************************************
            ' Quita Obj de Memoria
            ' ******************************************************************************
            cmd_SQL.Dispose()
            CierraBD()

            EjecutaQry = 1
        Catch ex As Exception
            sDError = ex.Message
            EjecutaQry = 2
        End Try

    End Function

    Public Function EjecutaQry_Tabla(ByVal strSentencia As String, _
                                    ByVal sTipoAccion As CommandType, _
                                    ByVal sNmbTabla As String _
                                    ) As DataTable
        ' ******************************************************************************
        ' Ejecuta Sentencias BD SQL que Extraen Datos
        ' ******************************************************************************

        EjecutaQry_Tabla = New DataTable

        Try
            ' ******************************************************************************
            ' Conexion a BD
            ' ******************************************************************************

            Dim sErr As String = ""

            ConectaBD(sErr)

            If sErr <> "" Then
                MsgBox(sErr)
            End If

            If BD_SQL.State <> ConnectionState.Open Then Exit Function

            ' ******************************************************************************
            ' Inicializa el comando
            ' ******************************************************************************
            cmd_SQL = New SqlCommand

            ' ******************************************************************************
            ' Indicar conexión, Nmb SP e Indicar q se Ejecutará un SP
            ' ******************************************************************************
            cmd_SQL.CommandText = strSentencia
            cmd_SQL.CommandType = sTipoAccion
            cmd_SQL.Connection = BD_SQL
            cmd_SQL.CommandTimeout = 0

            ' **************************************************************************************
            ' Se recuperan los datos del SP de 1 DataAdapter a DataSet
            ' **************************************************************************************
            dta_SQL = New SqlDataAdapter(cmd_SQL)
            dts_SQL = New DataSet
            dta_SQL.Fill(dts_SQL, sNmbTabla)

            EjecutaQry_Tabla = dts_SQL.Tables(sNmbTabla)

            EjecutaQry_Tabla.TableName = sNmbTabla

        Catch ex As Exception

        End Try

        cmd_SQL.Dispose()
        CierraBD()

    End Function

    Public Function EjecutaQry_Tabla(ByVal strSentencia As String, _
                                    ByVal sTipoAccion As CommandType, _
                                    ByVal sNmbTabla As String, ByVal conn As String _
                                    ) As DataTable
        ' ******************************************************************************
        ' Ejecuta Sentencias BD SQL que Extraen Datos
        ' ******************************************************************************

        EjecutaQry_Tabla = New DataTable

        Try
            ' ******************************************************************************
            ' Conexion a BD
            ' ******************************************************************************

            Dim sErr As String = ""

            ConectaDBConnString(conn)

            If sErr <> "" Then
                MsgBox(sErr)
            End If

            If BD_SQL.State <> ConnectionState.Open Then Exit Function

            ' ******************************************************************************
            ' Inicializa el comando
            ' ******************************************************************************
            cmd_SQL = New SqlCommand

            ' ******************************************************************************
            ' Indicar conexión, Nmb SP e Indicar q se Ejecutará un SP
            ' ******************************************************************************
            cmd_SQL.CommandText = strSentencia
            cmd_SQL.CommandType = sTipoAccion
            cmd_SQL.Connection = BD_SQL
            cmd_SQL.CommandTimeout = 0

            ' **************************************************************************************
            ' Se recuperan los datos del SP de 1 DataAdapter a DataSet
            ' **************************************************************************************
            dta_SQL = New SqlDataAdapter(cmd_SQL)
            dts_SQL = New DataSet
            dta_SQL.Fill(dts_SQL, sNmbTabla)

            EjecutaQry_Tabla = dts_SQL.Tables(sNmbTabla)

            EjecutaQry_Tabla.TableName = sNmbTabla

        Catch ex As Exception
            Dim mensajeError As String = ex.Message
        End Try

        cmd_SQL.Dispose()
        CierraBD()

    End Function

    Public Function EjecutaQry_Tabla(ByVal strSentencia As String, _
                                ByVal sTipoAccion As CommandType, _
                                ByVal sNmbTabla As String, ByVal conn As String, _
                                ByVal parametros As List(Of DBParameter)) As DataTable
        ' ******************************************************************************
        ' Ejecuta Sentencias BD SQL que Extraen Datos
        ' ******************************************************************************

        EjecutaQry_Tabla = New DataTable

        Try
            ' ******************************************************************************
            ' Conexion a BD
            ' ******************************************************************************

            Dim sErr As String = ""

            ConectaDBConnString(conn)

            If sErr <> "" Then
                MsgBox(sErr)
            End If

            If BD_SQL.State <> ConnectionState.Open Then Exit Function

            ' ******************************************************************************
            ' Inicializa el comando
            ' ******************************************************************************
            cmd_SQL = New SqlCommand

            ' ******************************************************************************
            ' Indicar conexión, Nmb SP e Indicar q se Ejecutará un SP
            ' ******************************************************************************
            cmd_SQL.CommandText = strSentencia
            cmd_SQL.CommandType = sTipoAccion
            cmd_SQL.Connection = BD_SQL
            cmd_SQL.CommandTimeout = 0

            For i As Integer = 0 To parametros.Count - 1
                Dim parametroSQL As System.Data.SqlClient.SqlParameter = cmd_SQL.CreateParameter()
                parametroSQL.ParameterName = parametros(i).NombreParametro
                parametroSQL.DbType = parametros(i).DbType
                parametroSQL.Value = parametros(i).Value
                If (parametros(i).Size <> Integer.MinValue) Then
                    parametroSQL.Size = parametros(i).Size
                End If

                cmd_SQL.Parameters.Add(parametroSQL)
            Next

            ' **************************************************************************************
            ' Se recuperan los datos del SP de 1 DataAdapter a DataSet
            ' **************************************************************************************
            dta_SQL = New SqlDataAdapter(cmd_SQL)
            dts_SQL = New DataSet
            dta_SQL.Fill(dts_SQL, sNmbTabla)

            EjecutaQry_Tabla = dts_SQL.Tables(sNmbTabla)

            EjecutaQry_Tabla.TableName = sNmbTabla

        Catch ex As Exception

        End Try

        cmd_SQL.Dispose()
        CierraBD()

    End Function

End Class