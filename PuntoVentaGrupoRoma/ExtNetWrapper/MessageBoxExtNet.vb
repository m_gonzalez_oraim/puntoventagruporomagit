﻿Public Class MessageBoxExtNet
    Public Shared Sub ShowWarning(ByVal titulo As String, ByVal mensaje As String)
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-exclamation"
        nconf.Html = mensaje
        nconf.Title = titulo
        msg.Configure(nconf)
        msg.Show()
    End Sub

    Public Shared Sub ShowError(ByVal mensaje As String)
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-error"
        nconf.Html = mensaje
        nconf.Title = "Error"
        msg.Configure(nconf)
        msg.Show()
    End Sub

    Public Shared Sub ShowSuccess(ByVal mensaje As String)
        Dim msg As New Ext.Net.Notification
        Dim nconf As New Ext.Net.NotificationConfig
        nconf.IconCls = "icon-success"
        nconf.Html = mensaje
        nconf.Title = "Aviso"
        msg.Configure(nconf)
        msg.Show()
    End Sub

End Class
