﻿Imports Microsoft.VisualBasic
Public Class clsGlobalLib 
    Public BaseYear As Integer = 2005

    Public Sub AddGlyph(ByVal Grid As GridView, ByVal Item As GridViewRow)
        Dim Glyph As Label = New Label()
        Glyph.EnableTheming = False
        Glyph.Font.Name = "webdings"
        'Glyph.Font.Size = FontUnit.Small
        If Grid.SortDirection = SortDirection.Ascending Then
            Glyph.Text = "5"
        Else
            Glyph.Text = "6"
        End If

        Dim Indice As Integer
        For Indice = 0 To Grid.Columns.Count - 1
            If Grid.Columns(Indice).SortExpression <> "" And _
                Grid.Columns(Indice).SortExpression = Grid.SortExpression Then
                Item.Cells(Indice).Controls.Add(Glyph)
            End If
        Next
    End Sub
    Public Function CheckInvalidChars(ByVal StringToCheck As String, ByVal InvalidChars As String) As Boolean
        ' Define return value
        Dim StringValid As Boolean = True

        ' Check InvalidChars String to start the validation
        Dim indexChar As Integer = 0
        If InvalidChars <> "" Then
            While (indexChar < InvalidChars.Length() And StringValid)
                StringValid = (StringToCheck.IndexOf(InvalidChars.Substring(indexChar, 1)) = -1)
                indexChar += 1
            End While
        End If
        Return StringValid
    End Function
    Public Sub SetJSClearString(ByVal sender As WebControl, ByVal CharacterToClean As String)
        sender.Attributes("onblur") &= "CleanString(this,'" & CharacterToClean & "');"
    End Sub
    Public Function Capitalize(ByVal StringToCapitalize As String) As String
        Dim CapitalizedString As String = ""

        If StringToCapitalize <> "" Then
            CapitalizedString = StringToCapitalize.Substring(0, 1).ToUpper() & StringToCapitalize.Substring(1).ToLower()
        End If

        ' Return Capitalized String
        Return CapitalizedString
    End Function
    Public Function getDayName(ByVal inDay As Integer) As String
        Dim dayNameStr As String = ""
        Select Case inDay
            Case 1 : dayNameStr = "Domingo"
            Case 2 : dayNameStr = "Lunes"
            Case 3 : dayNameStr = "Martes"
            Case 4 : dayNameStr = "Miércoles"
            Case 5 : dayNameStr = "Jueves"
            Case 6 : dayNameStr = "Viernes"
            Case 7 : dayNameStr = "Sábado"
        End Select
        Return dayNameStr
    End Function

    Public Function getMonthNumber(ByVal inMonth As String) As String
        Dim monthNameStr As String = ""
        Select Case inMonth
            Case "ene" : monthNameStr = "01"
            Case "feb" : monthNameStr = "02"
            Case "mar" : monthNameStr = "03"
            Case "abr" : monthNameStr = "04"
            Case "may" : monthNameStr = "05"
            Case "jun" : monthNameStr = "06"
            Case "jul" : monthNameStr = "07"
            Case "ago" : monthNameStr = "08"
            Case "sep" : monthNameStr = "09"
            Case "oct" : monthNameStr = "10"
            Case "nov" : monthNameStr = "11"
            Case "dic" : monthNameStr = "12"
        End Select
        Return monthNameStr
    End Function
End Class
