﻿Public Class DBParameter
    Private sNombreParametro As String
    Public Property NombreParametro As String
        Get
            Return sNombreParametro
        End Get
        Set(value As String)
            sNombreParametro = value
        End Set
    End Property

    Private oDbType As System.Data.DbType
    Public Property DbType As System.Data.DbType
        Get
            Return oDbType
        End Get
        Set(value As System.Data.DbType)
            oDbType = value
        End Set
    End Property

    Private oValue As Object
    Public Property Value As Object
        Get
            Return oValue
        End Get
        Set(value As Object)
            oValue = value
        End Set
    End Property

    Private iSize As Integer = Integer.MinValue
    Public Property Size As Integer
        Get
            Return iSize
        End Get
        Set(value As Integer)
            iSize = value
        End Set
    End Property
End Class
