﻿Imports System.Web
Imports System.Web.Services
Imports Ext.Net

Public Class ListadoArticulos
    Implements System.Web.IHttpHandler, IRequiresSessionState


    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        Dim controlTienda As ControlTienda = New ControlTienda()
        Dim codigoAlmacenSAP As String = controlTienda.ObtenerCodigoAlmacenSAP(context.Session("IDSTORE"))
        Dim controlArticulo As ControlArticulos = New ControlArticulos()

        Dim start As Integer = 0
        Dim limit As Integer = 10
        Dim sort As String = String.Empty
        Dim dir As String = String.Empty
        Dim query As String = String.Empty

        If Not String.IsNullOrEmpty(context.Request("start")) Then
            start = Integer.Parse(context.Request("start"))
        End If

        If Not String.IsNullOrEmpty(context.Request("limit")) Then
            limit = Integer.Parse(context.Request("limit"))
        End If

        If Not String.IsNullOrEmpty(context.Request("sort")) Then
            sort = context.Request("sort")
        End If

        If Not String.IsNullOrEmpty(context.Request("dir")) Then
            dir = context.Request("dir")
        End If

        If Not String.IsNullOrEmpty(context.Request("query")) Then
            query = context.Request("query")
        End If

        Dim articulos As Paging(Of Articulo) = controlArticulo.BuscarArticuloAjax(start, limit, sort, dir, query, "Venta", context.Session("StoreId"), codigoAlmacenSAP)
        Dim jsonSerializado As String = String.Format("{{Total:{1},'Articulos':{0}}}", JSON.Serialize(articulos.Data), articulos.TotalRecords)
        context.Response.Write(jsonSerializado)
    End Sub

    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class